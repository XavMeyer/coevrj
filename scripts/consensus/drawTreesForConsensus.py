#!/usr/bin/python

import sys
import random
import numpy as np
from subprocess import PIPE,Popen

# Read the treefile
treeFileName = sys.argv[1]
treeOutputFileName = sys.argv[2]
N_TREE = int(sys.argv[3])

treeFile = open(treeFileName, 'r')

indices = []
probs = []
trees = []


# Read the trees
for line in treeFile:
	words = line.split()
	index = int(words[0])
	prob = float(words[1])
	tree = words[2]

	indices.append(index)
	probs.append(prob/100.)
	trees.append(tree)

# Random draws and write
treeForConsensusFN = treeOutputFileName
treeFCFile = open(treeForConsensusFN, 'w')

for i in range(0,N_TREE):

	j = 0
	mySum = probs[0]
	myRnd = random.uniform(0., 1.)

	while mySum < myRnd and j+1 < len(probs):
		mySum += probs[j+1]
		j += 1
				
	treeFCFile.write(trees[j] + "\n")
	
	




