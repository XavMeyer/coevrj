#!/usb/bin/python

import sys
import numpy as np
import matplotlib.pyplot as plt
import operator
from numpy import linalg as LA


############## PLOT CONSTANT ####################
#################################################
DPI = 100

LINE_WIDTH=2.5


font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 11}

plt.rc('font', **font)

plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams['lines.linewidth'] = LINE_WIDTH


class ProposalStats:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.times = []
        self.acceptedMoves = 0
        self.totalMoves = 0
        self.acceptanceRate = 0.
        self.scalingDistr = 0.
        self.distanceDistr = 0.
        self.acceptanceRateTS = []
        self.coverageCountTS = []
        self.coverageFreqTS = []

class SplitsStats:
    def __init__(self, nBlock):
        self.freqs = []
        self.splits = []
        self.eVisitCount = []
        self.totalVisitCount = []
        self.blockVisitCount = [[] for x in range(nBlock)]

    def getNSplits(self):
        return len(self.freqs)


class OverallStats:
    def __init__(self):
        self.thinning = 1
        self.scalingDistr = 0.
        self.distanceDistr = 0.
        self.scalingEDistr = 0.
        self.mrBayesErr = 0.
        self.coverageCountTS = []
        self.coverageFreqTS = []
        self.refDistanceTS = []
        self.avgDistanceTS = []
        self.stdDistanceTS = []
        self.maxDistanceTS = []
        self.errorDistanceTS = []


    def computeRelativeErrorDistance(self, nSplits, errorTolerance):
        if len(self.refDistanceTS) == 0 : return []

        splitFrequency = np.zeros(nSplits)
        acceptableError = LA.norm(splitFrequency + errorTolerance)
        return [ (d)/acceptableError for d in self.refDistanceTS ]


def readProposalStats(fileName):
    proposalFile = open(fileName, 'r')

    proposals = {}

    nProposal = int(proposalFile.readline())

    for id in range(nProposal):
        line = next(proposalFile)
        myProp = ProposalStats(id, line.strip())
        line = next(proposalFile)
        myProp.times = [float(t) for t in line.split()]
        line = next(proposalFile)
        words = line.split()
        myProp.acceptedMoves = float(words[0])
        myProp.totalMoves = float(words[1])
        myProp.acceptanceRate = float(words[2])
        line = next(proposalFile)
        words = line.split()
        myProp.scalingDistr = float(words[0])
        myProp.distanceDistr = float(words[1])
        line = next(proposalFile)
        myProp.acceptanceRateTS = [float(ar) for ar in line.split()]
        line = next(proposalFile)
        myProp.coverageCountTS = [float(cnt) for cnt in line.split()]
        line = next(proposalFile)
        myProp.coverageFreqTS = [float(freq) for freq in line.split()]

        proposals[id] = myProp

    return proposals

def readSplitStats(fileName, nProp):
    splitFile = open(fileName, 'r')

    splitsStats = SplitsStats(nProp)

    for line in splitFile:
        words = line.split()

        splitsStats.freqs.append(float(words[1]))
        splitsStats.splits.append(words[2])
        splitsStats.eVisitCount.append(float(words[3]))
        splitsStats.totalVisitCount.append(float(words[4]))
        for i in range(nProp): splitsStats.blockVisitCount[i].append(float(words[5+i]))

    return splitsStats

def readOverallStats(fileName):
    statsFile = open(fileName, 'r')

    overallStats = OverallStats()

    line = next(statsFile)
    overallStats.thinning = int(line)

    line = next(statsFile)
    words = line.split()
    overallStats.scalingDistr = float(words[0])
    overallStats.distanceDistr = float(words[1])
    overallStats.scalingEDistr = float(words[2])
    if len(words) == 4: overallStats.mrBayesErr = float(words[3])

    line = next(statsFile)
    overallStats.coverageCountTS = [float(cnt) for cnt in line.split()]
    line = next(statsFile)
    overallStats.coverageFreqTS = [float(freq) for freq in line.split()]

    line = next(statsFile)
    if line != '':
        overallStats.refDistanceTS = [float(cnt) for cnt in line.split()]
        line = next(statsFile)
        overallStats.avgDistanceTS = [float(freq) for freq in line.split()]
        line = next(statsFile)
        overallStats.stdDistanceTS = [float(cnt) for cnt in line.split()]
        line = next(statsFile)
        overallStats.maxDistanceTS = [float(freq) for freq in line.split()]

    return overallStats


################################## MAIN ###########################

inputFileName = sys.argv[1]

proposals = readProposalStats(inputFileName + '.treeProposalsSummary')
splits = readSplitStats(inputFileName + '.splitSummary', len(proposals))
overall = readOverallStats(inputFileName + '.overallSummary')

# Plot times series
X_WIDTH = 1200
Y_WIDTH = 700

## GENERAL CONVERGENCE AND COVERAGE
fig, ax = plt.subplots(4, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), sharex=False, dpi=DPI, constrained_layout=True)
nTotalSamples = len(overall.coverageCountTS)
nDistanceSamples = len(overall.refDistanceTS)
offset = nTotalSamples - nDistanceSamples

x = [overall.thinning * x for x in range(offset, nTotalSamples) ]
#ax[0].plot(x, overall.refDistanceTS)
ax[0].plot(x, overall.computeRelativeErrorDistance(splits.getNSplits(), 0.01))
ax[0].axhline(1.0, color='k', linestyle='--', linewidth=1.0)
ax[0].set_xlabel('Relative distance to the reference splits\n(Relative to a 1% error per splits)')
ax[1].plot(x, overall.maxDistanceTS)
ax[1].set_xlabel('Max split error')

x = [overall.thinning * x for x in range(nTotalSamples) ]

blCovCntTS = np.array(overall.coverageCountTS)
blCovCntFreq = np.array(overall.coverageFreqTS)
for i in range(len(proposals)):
    blCovCntTS = blCovCntTS - np.array(proposals[i].coverageCountTS)
    blCovCntFreq = blCovCntFreq - np.array(proposals[i].coverageFreqTS)

vecCovCntTS = [ list(blCovCntTS)]
vecCovFreqTS = [ list(blCovCntFreq)]
labels = ['Base']
for i in range(len(proposals)):
    vecCovCntTS.append(proposals[i].coverageCountTS)
    vecCovFreqTS.append(proposals[i].coverageFreqTS)
    labels.append(proposals[i].name)

ax[2].stackplot(x, vecCovCntTS, labels=labels)
ax[2].set_xlabel('Splits coverage normalized count')
ax[2].legend(loc='upper left')

ax[3].stackplot(x, vecCovFreqTS, labels=labels)
ax[3].set_xlabel('Splits coverage freq')

plt.tight_layout()


## Splits visit distribution
fig, ax = plt.subplots(3+len(proposals), 1, figsize=(X_WIDTH/DPI, 1.5*Y_WIDTH/DPI), sharex=True, dpi=DPI, constrained_layout=True)
# define starting offset
offset = 0
for i in range(len(splits.eVisitCount)):
    if splits.eVisitCount[i] > 0.01:
        offset = i
        break
offset = max(0, offset-2)



# plot freqs and expected
x = range(offset, len(splits.eVisitCount))

ax[0].bar(x, splits.freqs[offset:])
ax[0].set_xlabel('Estimated split frequencies')

ax[1].bar(x, splits.eVisitCount[offset:])
ax[1].set_xlabel('Expected from ideal proposal')

# prepare rescaling
sumScalings = 0.
scalings = [0. for i in range(len(proposals))]
for k, v in proposals.iteritems():
    scalings[k] = v.totalMoves
    sumScalings += v.totalMoves

# Plot scaled
y = [v/sumScalings for v in splits.totalVisitCount[offset:]]
ax[2].bar(x, y)
ax[2].set_xlabel('Overall')
for k, v in proposals.iteritems():
    y = [vis/scalings[k] for vis in splits.blockVisitCount[k][offset:]]
    ax[k+3].bar(x, y)
    ax[k+3].set_xlabel(proposals[k].name)

## Test move correlation
keys = proposals.keys()
for i in range(len(keys)):
    for j in range(i, len(keys)):
        k1 = keys[i]
        yI = [vis for vis in splits.blockVisitCount[k1][offset:]]
        yI = yI / np.sum(yI)

        k2 = keys[j]
        yJ = [vis for vis in splits.blockVisitCount[k2][offset:]]
        yJ = yJ / np.sum(yJ)

        distance = LA.norm(yI - yJ)

        print proposals[k1].name, "  --> ", proposals[k2].name, " : ", distance

plt.tight_layout()

## Scalings and distances to ref distribution of visits
X_WIDTH = 800
Y_WIDTH = 400
fig, ax = plt.subplots(1, 2, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI),  dpi=DPI, constrained_layout=True)

names = ['Overall']
scalings = [overall.scalingDistr]
errors = [overall.distanceDistr]
for i in range(len(proposals)):
    names.append(proposals[i].name)
    scalings.append(proposals[i].scalingDistr)
    errors.append(proposals[i].distanceDistr)

#ax[0].set_title("Expected = {:1.2f}".format(overall.scalingEDistr))
ax[0].bar(range(len(scalings)), scalings)
ax[0].set_xticks(range(len(scalings)))
ax[0].set_xticklabels(names, rotation=10)
ax[0].set_ylabel('C-V efficiency')

ax[1].bar(range(len(errors)), errors)
ax[1].set_xticks(range(len(errors)))
ax[1].set_xticklabels(names, rotation=10)
ax[1].set_ylabel('C-V distance')

plt.tight_layout()

## Scalings and distances to ref distribution of visits
X_WIDTH = len(proposals)*400
Y_WIDTH = 600
fig, ax = plt.subplots(3, len(proposals), figsize=(X_WIDTH/DPI, Y_WIDTH/DPI),  dpi=DPI, constrained_layout=True)

x = [overall.thinning * x for x in range(nTotalSamples) ]
for i in range(len(proposals)):
    ax[0,i].set_title(proposals[i].name)
    ax[0,i].plot(x, proposals[i].acceptanceRateTS)
    ax[1,i].plot(x, proposals[i].coverageCountTS)
    ax[2,i].plot(x, proposals[i].coverageFreqTS)
    if i == 0:
        ax[0,i].set_ylabel('accept/rej')
        ax[1,i].set_ylabel('Cov. count')
        ax[2,i].set_ylabel('Cov. freq')



plt.tight_layout()

plt.show()
