#!/usb/bin/python

import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import operator
from numpy import linalg as LA

############## PLOT CONSTANT ####################
#################################################
DPI = 100

LINE_WIDTH=2.5


font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 11}

plt.rc('font', **font)

plt.rcParams['lines.linewidth'] = LINE_WIDTH


def readReferenceFile(refFileName):
    refFile = open(refFileName, 'r')

    splitsDic = {}

    refFile.readline()
    for line in refFile:
        words = line.split()
        splitsDic[words[2]] = [float(words[1]), float(words[3])]

    return splitsDic

def readTreeBipartitions(inputFileName):
    inFile = open(inputFileName + '.allTreeSplits')

    treeSplitDic = {}

    for line in inFile:
        words = line.split()
        hash = long(words[0])
        splits = [int(x) for x in words[1:]]

        treeSplitDic[hash] = splits

    return treeSplitDic

def readBiparitions(inputFileName):
    inFile = open(inputFileName + '.allSplits')

    biparitionsDic = {}

    for line in inFile:
        words = line.split()
        biparitionsDic[int(words[0])] = [float(words[1]), words[2]]

    return biparitionsDic

def readBlocksInfo(inputFileName, blocksIds):
    #print inputFileName[:-2] + '.out'
    inFile = open(inputFileName[:-2] + '.out')

    blockLabels = ['[' + repr(b) + ']' for b in blocksId]

    infoMC3 = []

    reachedSummary = False
    blocksInfo = {}
    for line in inFile:
        words = line.split()

        if len(words) > 0 and words[0] == '[Final]' and words[1] == '[MC3]':
            infoMC3.append(float(words[6]))
            line = next(inFile)
            words = line.split()
            infoMC3.append(float(words[7]))
            infoMC3.append(float(words[8][1:]))
            infoMC3.append(float(words[10][:-1]))

        if not reachedSummary and len(words) > 0 and words[0] != '[Final][Blocks]':
            reachedSummary = True
            continue

        if len(words) > 0 and words[0] in blockLabels:
            id = int(words[0][1:-1])
            # Get name
            name = words[2]
            # Get times (proposal, compute, summed)
            line = next(inFile)
            chunks = line.split('{')
            chunks = chunks[1].split('}')
            timesStr = chunks[0].split(',')
            times = [ float(c) for c in timesStr ]
            # Get aceptance time and more
            line = next(inFile)
            line = next(inFile)
            chunks = line.split('=')
            arInfo = chunks[1].split('/')
            acceptedMoves = int(arInfo[0])
            totalMoves  = int(arInfo[1])
            acceptanceRate = float(chunks[2])

            blocksInfo[id] = [name, times, [acceptedMoves, totalMoves, acceptanceRate]]

    if -5 in blocksId and len(infoMC3) > 0:
        blocksInfo[-5] = ['MC3', [infoMC3[0] for i in range(3)], [infoMC3[2], infoMC3[3], infoMC3[1]]]

    #print blocksInfo
    return blocksInfo


def locateRefSplit(refSplits, splits):

    refFrequency = []
    refError = []
    refSplitPosition = []
    refSplitString = []

    for refK, refV in refSplits.iteritems():
        found = False
        for k, v in splits.iteritems():
            if refK == v[1]:
                refFrequency.append(refV[0])
                refError.append(refV[1]/2.)
                refSplitString.append(refK)
                refSplitPosition.append(long(k))
                found = True
                break

        if not found:
            print refV, "  ", refK


    if len(refSplits) != len(refFrequency):
        print "Some of the true split frequencies were not inferred."

    return [np.array(refFrequency, dtype=np.float_), np.array(refSplitPosition, dtype=np.long), refSplitString, np.array(refError, dtype=np.float_)]

def analyticalVisitCycle(resultFreqSorted):

    expectedCV = []

    for iR in range(0, len(resultFreqSorted)):
        #idSplit = resultFreqSorted[iR][0]
        freq = resultFreqSorted[iR]
        if freq == 1.:
            expectedCV.append(0.)
            continue

        # estimate the expected cycle-vist length
        analExpCV = 0.
        N_ESTIMATE = 1000000;
        for j in range(1, N_ESTIMATE):
            val = 0.
            myQs = [freq, 1-freq]
            for q in myQs:
                val += q*pow(1.-q, j-1)*((1-q)/((1-q)-q))

            analExpCV += val*j

            # Stop when not changing anymore
            if j > 50 and val < 1.e-9:
                break

        expectedCV.append(1./analExpCV)

    return expectedCV


inputFileName = sys.argv[1]
#referenceFileName = sys.argv[2]

offset = int(sys.argv[2])

autostop = False
if offset < 0:
    offset = 50000
    autostop = True
    iterStop = -1
    #outputFileName += '_AS'


thinning = int(sys.argv[3])

if len(sys.argv) >= 5:
    assessSplitDist = True
    refSplitsFile = sys.argv[4]
else:
    assessSplitDist = False

postfix = ''
if len(sys.argv) >= 6:
    postfix = sys.argv[5]

outputFileName = inputFileName + postfix
print "Ouput base name : ", outputFileName

#refSplits = readReferenceFile(referenceFileName)
treeSplits = readTreeBipartitions(inputFileName)
splits = readBiparitions(inputFileName)

#print splits

if assessSplitDist:
    refSplits = readReferenceFile(refSplitsFile)
    [refSplitsFrequency, refSplitsPosition, refSplitString, refSplitsError] = locateRefSplit(refSplits, splits)
    errMrBayes = LA.norm(refSplitsError)
    print "errMrBayes = ", errMrBayes

    distance = []
    avgDist = []
    stdDist = []
    maxDist = []
    refSplitsSum = np.sum(refSplitsFrequency)


inFile = open(inputFileName + '.allTrees')


counter = 0
counts = np.zeros(np.max(splits.keys())+1)
splitFrequency = np.zeros(len(counts))

hash = long(inFile.readline().split()[1])

# Tree freq
treeFreq = {}

# Init visitCnt
visitCnt = np.zeros(len(counts))
visitCnt[treeSplits[hash]] += 1

# for each block contains [acceptedCnt, rejectedCnt, totalCnt, firstVisitCnt]
blocksId = []
blocksStats = {}
blocksStats[-1] = [0, 0, 0, treeSplits[hash], visitCnt]
N_SPLITS = float(len(blocksStats[-1][3]))

# blockStatsThroughTime
blockStatsTT = {}
blockStatsTT[-1] = [[], [], []]

############################## Reading file and collecting stats #############################

doExit = False
for line in inFile:
    #print line
    words = line.split()
    nextIter = int(words[0])
    nextHash = long(words[1])
    blockId = int(words[2])
    accepted = words[3] == "1"

    if not blockId in blocksStats:
        blocksId.append(blockId)
        blocksStats[blockId] = [0, 0, 0, [], np.zeros(len(counts))]
        blockStatsTT[blockId] = [[], [], []]


    if accepted:
        #print "acc : ", words[3]
        blocksStats[blockId][0] += 1
        blocksStats[blockId][2] += 1
    else:
        #print "rej : ", words[3]
        blocksStats[blockId][1] += 1
        blocksStats[blockId][2] += 1
        continue

    nIter = nextIter - counter

    # Get split pos
    if hash in treeSplits and nextHash in treeSplits:
        curSplitPos = treeSplits[hash]
        nextNewSplitPos = treeSplits[nextHash]
    else:
        print "Counter : ", counter, " -- error"

    # Get splits visited uniquely in next iteration
    nextUniqueSplitPos = set(nextNewSplitPos) - set(curSplitPos)

    # get the amount of new visits (first visit to a split)
    countFirstVisits = np.count_nonzero( visitCnt[list(nextUniqueSplitPos)] == 0)
    #blocksStats[blockId][3] += countFirstVisits
    firstVisits = visitCnt[list(nextUniqueSplitPos)] == 0
    if countFirstVisits > 0:
        listSplits = list(nextUniqueSplitPos)
        for id in listSplits:
            if visitCnt[id] == 0:
                    blocksStats[blockId][3].append(id)

    visitCnt[list(nextUniqueSplitPos)] += 1
    blocksStats[blockId][4][list(nextUniqueSplitPos)] += 1

    for i in range(nIter):
        counter += 1

        # Define the split coverage through time using the reference splits frequencies ??
        if counter % thinning == 0:
            sumSplitCnt = 0
            sumSplitFreq = 0.
            for bId in blocksId:
                blockStatsTT[bId][0].append(float(blocksStats[bId][0])/float(blocksStats[bId][2]))
                blockStatsTT[bId][1].append(len(blocksStats[bId][3]))
                sumSplitCnt += len(blocksStats[bId][3])

                sumVFreq = 0.
                if assessSplitDist:
                    for s in blocksStats[bId][3]:
                        if splits[s][1] in refSplits:
                            sumVFreq += refSplits[splits[s][1]][0]/refSplitsSum
                            #print splits[s][0], " vs ", refSplits[splits[s][1]]
                else:
                    for s in blocksStats[bId][3]: sumVFreq += splits[s][0]/N_SPLITS

                blockStatsTT[bId][2].append(sumVFreq)
                sumSplitFreq += sumVFreq

            sumSplitCnt += 0#len(blocksStats[-1][3])
            blockStatsTT[-1][1].append(sumSplitCnt)
            sumVFreq = 0.
            #for s in blocksStats[-1][3]: sumVFreq += splits[s][0]/N_SPLITS
            if assessSplitDist:
                for s in blocksStats[-1][3]:
                    #print s, splits[s][0], splits[s][1]
                    if splits[s][1] in refSplits:
                        sumVFreq += refSplits[splits[s][1]][0]/refSplitsSum
            else:
                for s in blocksStats[-1][3]: sumVFreq += splits[s][0]/N_SPLITS

            sumSplitFreq += sumVFreq
            blockStatsTT[-1][2].append(sumSplitFreq)

        #if counter <= 50000 and counter % 5000 == 0: print counter, '\t', blockStatsTT[-1][2][-1]
        #if counter > 50000 and counter % 50000 == 0: print counter, '\t', blockStatsTT[-1][2][-1]
        if counter % 1000000 == 0: print counter, '\t', blockStatsTT[-1][2][-1]

        if counter > offset:
            # Update freq count
            counts[curSplitPos] += 1.

            if not hash in treeFreq: treeFreq[hash] = 0.
            treeFreq[hash] += 1.

            # Define the split coverage through time using the reference splits frequencies ??
            if assessSplitDist and counter % thinning == 0:
                # Compute distances
                #print refSplitsPosition.astype(np.long)
                splitFrequency = counts[refSplitsPosition.astype(np.long)] / (counter-offset)
                #print splitFrequency
                #print refSplitsFrequency
                distance.append(LA.norm(splitFrequency-refSplitsFrequency))
                diff = np.abs(splitFrequency-refSplitsFrequency)
                vecDist = diff
                avgDist.append(np.mean(vecDist))
                stdDist.append(np.std(vecDist))
                maxDist.append(np.max(vecDist))

                if autostop:
                    splitFrequency = np.zeros(len(refSplitsFrequency))
                    acceptableError = LA.norm(splitFrequency + 1.e-2)
                    #print distance[-1], "\t", acceptableError
                    if iterStop < 0 and distance[-1] < acceptableError:  iterStop = int(1.2*counter)

    hash = nextHash
    if autostop and iterStop > 0 and counter > iterStop: break

splitFrequency = counts / (counter-offset)

############################## expected unique visit to splits #############################
refExpectedCV = analyticalVisitCycle(refSplitsFrequency)

############################## Preparing split ordering #############################
resultFreq = []
scalingEV = 0.
for k, v in splits.iteritems():
    #print visitCnt[k]

    tmpExpCV = 0.
    if splits[k][1] in refSplitString:
        refId = refSplitString.index(splits[k][1])
        tmpExpCV = refExpectedCV[refId]
    elif math.fabs(splitFrequency[k]-0.1) < 0.05 : # we recover this info if freq close to 0.1
        tmpExpCV = analyticalVisitCycle([splitFrequency[k]])[0]
    else:
        tmpExpCV = 0. # This is either a terminal edge or an error in both case CV = 0

    resultFreq.append([k, splitFrequency[k], splits[k][1], tmpExpCV])
    scalingEV += tmpExpCV

resultFreqSorted = sorted(resultFreq, key=operator.itemgetter(1, 2), reverse=True)

############################## Blocks info #############################

sortedBlocksId = sorted(blocksId)
totalFreq = 0.
nSplits = float(len(blocksStats[-1][3]))
for k, v in blocksStats.iteritems():
    sumVFreq = 0.
    for s in v[3]: sumVFreq += splitFrequency[s]/nSplits

    totalFreq += sumVFreq
    if float(v[2]) > 0. :
        print k, float(v[0])/float(v[2]), "\t", float(v[2]),  "\t", len(v[3]), "\t", sumVFreq
    else:
        print k, float(v[0]), "\t", float(v[2]), "\t", len(v[3]), "\t", sumVFreq


############################## Writing splits info #############################
oFile =  file(outputFileName + '.splitSummary', 'w')

lastFreq = resultFreqSorted[0][1]

vCnt = []
eVCnt = []
visitContribution = {}
for bId in blocksId: visitContribution[bId] = []

i=0
while lastFreq > 0.1 and i < len(resultFreq) :
    #print resultFreqSorted[i][3]
    orgId = resultFreqSorted[i][0]
    myStr  =  repr(i) + "\t" + "{:1.6f}".format(resultFreqSorted[i][1]) + "\t"
    myStr += resultFreqSorted[i][2] + "\t"
    myStr += "{:1.6f}".format(resultFreqSorted[i][3]) + "\t"
    myStr += repr(visitCnt[orgId])
    for bId in sortedBlocksId:
        myStr += "\t" + repr(blocksStats[bId][4][orgId])
    myStr +=  "\n"

    oFile.write(myStr)

    lastFreq = resultFreqSorted[i][1]

    vCnt.append(visitCnt[orgId])
    eVCnt.append(resultFreqSorted[i][3])

    for bId in sortedBlocksId:
        visitContribution[bId].append(blocksStats[bId][4][orgId])

    i += 1

N_SPLIT_WITH_FREQ_GREATER_THAN = i

############################## Visit contribution per block #############################
normEVCnt = np.array(eVCnt) / np.sum(eVCnt)

nTotalMoves = 0
blocksDistrStats = {}
for bId in sortedBlocksId:
    #print visitContribution[bId]

    nMoves = blocksStats[bId][2]
    nTotalMoves += nMoves

    tmpArr = np.array(visitContribution[bId])
    scaling = np.sum(tmpArr/float(nMoves))
    tmpArr = normEVCnt - tmpArr / np.sum(tmpArr)
    #print tmpArr
    #print bId, "\t", scaling/len(tmpArr), "\t", LA.norm(tmpArr)
    blocksDistrStats[bId] = [scaling, LA.norm(tmpArr)]
    #ax[i].bar(range(0, len(visitContribution[bId])), visitContribution[bId])

# All moves
tmpArr = np.array(vCnt)
scaling = np.sum(tmpArr/float(nTotalMoves))
tmpArr = normEVCnt - tmpArr / np.sum(tmpArr)
#print tmpArr
overallDistrScaling = scaling
overallDistrDist = LA.norm(tmpArr)

#plt.show()
########################### Write block info #####################################
blocksInfo = readBlocksInfo(inputFileName, blocksId)
oFile = file(outputFileName + '.treeProposalsSummary', 'w')
# number of blocks
oFile.write(repr(len(blocksId)) + '\n')

#blocksInfo[id] = [name, times, [acceptedMoves, totalMoves, acceptanceRate]]
totalSplitVisited = blockStatsTT[-1][1][-1]
for bId in sortedBlocksId:
    oFile.write(blocksInfo[bId][0] + '\n') #blockname
    # blocks time tProposal, tLik, overall
    for t in blocksInfo[bId][1]: oFile.write(repr(t) + '\t')
    oFile.write('\n')
    #Write accepted moves, total proposed moves, acceptance rate
    for t in blocksInfo[bId][2]: oFile.write(repr(t) + '\t')
    oFile.write('\n')
    # Distr stats
    oFile.write(repr(blocksDistrStats[bId][0]) + '\t' + repr(blocksDistrStats[bId][1]) + '\n')
    # Write time series: acceptance rate, (normalized) split through time, split freq through time
    for ar in blockStatsTT[bId][0]: oFile.write(repr(ar) + '\t')
    oFile.write('\n')
    for cnt in blockStatsTT[bId][1]: oFile.write(repr(float(cnt)/totalSplitVisited) + '\t')
    oFile.write('\n')
    for f in blockStatsTT[bId][2]: oFile.write(repr(f) + '\t')
    oFile.write('\n')

########################### Write overall info #####################################
blocksInfo = readBlocksInfo(inputFileName, blocksId)
oFile = file(outputFileName + '.overallSummary', 'w')

# thinning
oFile.write(repr(thinning) + '\n')

# number of blocks
if assessSplitDist:
    oFile.write(repr(overallDistrScaling) + '\t' + repr(overallDistrDist) + '\t' + repr(scalingEV) + '\t' + repr(errMrBayes) + '\n')
else:
    oFile.write(repr(overallDistrScaling) + '\t' + repr(overallDistrDist) + '\t' + repr(scalingEV) + '\t' + '0' + '\n')

# Split coverage through time proporting of splits / freq
for cnt in blockStatsTT[-1][1]: oFile.write(repr(float(cnt)/totalSplitVisited) + '\t')
oFile.write('\n')
for f in blockStatsTT[-1][2]: oFile.write(repr(f) + '\t')
oFile.write('\n')

# Write distance to ref
if assessSplitDist:
    # Distance
    for d in distance: oFile.write(repr(d) + '\t')
    oFile.write('\n')

    ## Avg split distance
    for d in avgDist: oFile.write(repr(d) + '\t')
    oFile.write('\n')
    ## StdDev split distance
    for d in stdDist: oFile.write(repr(d) + '\t')
    oFile.write('\n')

    for d in maxDist: oFile.write(repr(d) + '\t')
    oFile.write('\n')
