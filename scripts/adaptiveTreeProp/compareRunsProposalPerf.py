#!/usb/bin/python

import sys
import numpy as np
import matplotlib.pyplot as plt
import operator
from numpy import linalg as LA
import seaborn as sns

############## PLOT CONSTANT ####################
#################################################
DPI = 100

LINE_WIDTH=2.5


font = {'family' : 'DejaVu Sans',
    'weight' : 'normal',
    'size'   : 11}

plt.rc('font', **font)

plt.rcParams['lines.linewidth'] = LINE_WIDTH


class ProposalStats:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.times = []
        self.acceptedMoves = 0
        self.totalMoves = 0
        self.acceptanceRate = 0.
        self.scalingDistr = 0.
        self.distanceDistr = 0.
        self.acceptanceRateTS = []
        self.coverageCountTS = []
        self.coverageFreqTS = []

class SplitsStats:
    def __init__(self, nBlock):
        self.freqs = []
        self.splits = []
        self.eVisitCount = []
        self.totalVisitCount = []
        self.blockVisitCount = [[] for x in range(nBlock)]

    def getNSplits(self):
        return len(self.freqs)


class OverallStats:
    def __init__(self):
        self.thinning = 1
        self.scalingDistr = 0.
        self.distanceDistr = 0.
        self.scalingEDistr = 0.
        self.mrBayesErr = 0.
        self.coverageCountTS = []
        self.coverageFreqTS = []
        self.refDistanceTS = []
        self.avgDistanceTS = []
        self.stdDistanceTS = []
        self.maxDistanceTS = []
        self.errorDistanceTS = []


    def computeRelativeErrorDistance(self, nSplits, errorTolerance):
        if len(self.refDistanceTS) == 0 : return []

        splitFrequency = np.zeros(nSplits)
        acceptableError = LA.norm(splitFrequency + errorTolerance)
        return [ d/acceptableError for d in self.refDistanceTS ]


def readProposalStats(fileName):
    proposalFile = open(fileName, 'r')

    proposals = {}

    nProposal = int(proposalFile.readline())

    for id in range(nProposal):
        line = next(proposalFile)
        myProp = ProposalStats(id, line.strip())
        line = next(proposalFile)
        myProp.times = [float(t) for t in line.split()]
        line = next(proposalFile)
        words = line.split()
        myProp.acceptedMoves = int(words[0])
        myProp.totalMoves = int(words[1])
        myProp.acceptanceRate = float(words[2])
        line = next(proposalFile)
        words = line.split()
        myProp.scalingDistr = float(words[0])
        myProp.distanceDistr = float(words[1])
        line = next(proposalFile)
        myProp.acceptanceRateTS = [float(ar) for ar in line.split()]
        line = next(proposalFile)
        myProp.coverageCountTS = [float(cnt) for cnt in line.split()]
        line = next(proposalFile)
        myProp.coverageFreqTS = [float(freq) for freq in line.split()]

        proposals[id] = myProp

    return proposals

def readSplitStats(fileName, nProp):
    splitFile = open(fileName, 'r')

    splitsStats = SplitsStats(nProp)

    for line in splitFile:
        words = line.split()

        splitsStats.freqs.append(float(words[1]))
        splitsStats.splits.append(words[2])
        splitsStats.eVisitCount.append(float(words[3]))
        splitsStats.totalVisitCount.append(float(words[4]))
        for i in range(nProp): splitsStats.blockVisitCount[i].append(float(words[5+i]))

    return splitsStats

def readOverallStats(fileName):
    statsFile = open(fileName, 'r')

    overallStats = OverallStats()

    line = next(statsFile)
    overallStats.thinning = int(line)

    line = next(statsFile)
    words = line.split()
    overallStats.scalingDistr = float(words[0])
    overallStats.distanceDistr = float(words[1])
    overallStats.scalingEDistr = float(words[2])
    if len(words) == 4: overallStats.mrBayesErr = float(words[3])

    line = next(statsFile)
    overallStats.coverageCountTS = [float(cnt) for cnt in line.split()]
    line = next(statsFile)
    overallStats.coverageFreqTS = [float(freq) for freq in line.split()]

    line = next(statsFile)
    if line != '':
        overallStats.refDistanceTS = [float(cnt) for cnt in line.split()]
        line = next(statsFile)
        overallStats.avgDistanceTS = [float(freq) for freq in line.split()]
        line = next(statsFile)
        overallStats.stdDistanceTS = [float(cnt) for cnt in line.split()]
        line = next(statsFile)
        overallStats.maxDistanceTS = [float(freq) for freq in line.split()]

    return overallStats


################################## MAIN ###########################

runs = []

for i in range(1, len(sys.argv)):
    inputFileName = sys.argv[i]

    proposals = readProposalStats(inputFileName + '.treeProposalsSummary')
    splits = readSplitStats(inputFileName + '.splitSummary', len(proposals))
    overall = readOverallStats(inputFileName + '.overallSummary')

    runs.append([proposals, splits, overall])

# Plot times series
X_WIDTH = 1200
Y_WIDTH = 700
current_palette = sns.color_palette()
#print current_palette

## GENERAL CONVERGENCE AND COVERAGE
fig, ax = plt.subplots(3, 1, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI), sharex=False, dpi=DPI, constrained_layout=True)

iR = 0
for run in runs:
    [proposals, splits, overall] = run

    nTotalSamples = len(overall.coverageCountTS)
    nDistanceSamples = len(overall.refDistanceTS)
    offset = nTotalSamples - nDistanceSamples

    x = [overall.thinning * x for x in range(offset, nTotalSamples) ]
    #ax[0].plot(x, overall.refDistanceTS)

    refAccErr = 0.01
    splitFrequency = np.zeros(splits.getNSplits())
    acceptableError = LA.norm(splitFrequency + refAccErr)
    acceptableErrorTwice = LA.norm(splitFrequency + 2*refAccErr)
    acceptableErrorHalf = LA.norm(splitFrequency + 0.5*refAccErr)


    distVals = (overall.computeRelativeErrorDistance(splits.getNSplits(), refAccErr))
    ax[0].plot(x, distVals, color=current_palette[iR])
    ax[0].axhline(1.0, color='k', linestyle='--', linewidth=1.0)
    ax[0].axhline(acceptableErrorTwice/acceptableError, color='grey', linestyle='--', linewidth=1.0)
    ax[0].axhline(acceptableErrorHalf/acceptableError, color='grey', linestyle='--', linewidth=1.0)
    ax[0].axhline(overall.mrBayesErr/acceptableError, color='red', linestyle=':', linewidth=1.0)
    ax[0].set_ylim((0,2.5))
    ax[0].set_xlabel('Relative distance to the reference splits\n(Relative to a 1% error per splits)')
    ax[1].plot(x, overall.maxDistanceTS, color=current_palette[iR])
    ax[1].axhline(0.05, color='k', linestyle='--', linewidth=1.0)
    ax[1].set_xlabel('Max split error')

    x = [overall.thinning * x for x in range(nTotalSamples) ]
    ax[2].plot(x, overall.coverageCountTS, color=current_palette[iR % len(current_palette)])
    ax[2].plot(x, overall.coverageFreqTS, color=current_palette[iR % len(current_palette)], linestyle='--')
    ax[2].set_xlabel('Split coverage (-=count, --=freq)')

    iR += 1


## Scalings and distances to ref distribution of visits and AR
X_WIDTH = 1200
Y_WIDTH = 300
fig, ax = plt.subplots(1, 3, figsize=(X_WIDTH/DPI, Y_WIDTH/DPI),  dpi=DPI, constrained_layout=True)

iR = 0
ptOffset = 0.2/(len(runs)-1.)
for run in runs:
    [proposals, splits, overall] = run

    positions = [x-0.1+iR*ptOffset for x in range(1, len(proposals)+2) ]
    #print positions
    names = ['Overall']
    scalings = [overall.scalingDistr]
    errors = [overall.distanceDistr]
    acceptanceRates = []
    for i in range(len(proposals)):
        names.append(proposals[i].name)
        scalings.append(proposals[i].scalingDistr)
        errors.append(proposals[i].distanceDistr)
        acceptanceRates.append(proposals[i].acceptanceRate)

    ax[0].scatter(positions, scalings, color=current_palette[iR], marker='*')
    ax[1].scatter(positions, errors, color=current_palette[iR], marker='*')
    ax[2].scatter(positions[:-1], acceptanceRates, color=current_palette[iR], marker='*')

    iR += 1

positions = range(1, len(proposals)+2)
ax[0].set_xticks(positions)
ax[0].set_xticklabels(names, rotation=10)
ax[0].set_ylabel('C-V efficiency')

ax[1].set_xticks(positions)
ax[1].set_xticklabels(names, rotation=10)
ax[1].set_ylabel('C-V distance')

ax[2].set_xticks(positions[:-1])
ax[2].set_xticklabels(names[1:], rotation=10)
ax[2].set_ylabel('Acc. rates')


#plt.tight_layout()
#plt.savefig(outFile)

plt.show()
