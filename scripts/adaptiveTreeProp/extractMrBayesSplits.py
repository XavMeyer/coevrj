#!/usr/bin/python


import sys
import random
import numpy as np
from subprocess import PIPE,Popen

class Split(object):
  def __init__(self, idx, prob, error, split):
    self.idx = idx
    self.prob = prob
    self.error = error
    self.split = split

  def __lt__(self,other):
    if self.prob > other.prob:
      return True
    elif self.prob == other.prob:
      return self.split > other.split
    else:
      return False

  def __gt__(self,other):
    return not __lt__(other)

mySplits = []

baseName = sys.argv[1]

treeFile = open(baseName+".run1.t", 'r')
partFile = open(baseName+".parts", 'r')
statFile = open(baseName+".tstat", 'r')

# read treeFile
taxa = []

line = treeFile.readline()
while line.strip() != "translate":
  line = treeFile.readline()

line = treeFile.readline()
while len(line.split()) == 2:
  taxa.append(line.split()[1][:-1])
  line = treeFile.readline()

# read partFile
line = partFile.readline()
line = partFile.readline()

permutation = sorted(range(len(taxa)), key=lambda k: taxa[k])

outFile = open(baseName+".taxa", 'w')
for i, t in zip(range(0, len(taxa)), sorted(taxa)):
  outFile.write(repr(i) + "\t" + t + "\n")

splits = {}
for line in partFile:
  #print "the line :" + line
  words = line.split()

  if words[0] == "ID":
    #print "OK!"
    break

  idSplit = int(words[0])

  sortedSplit = [words[1][p] for p in permutation]
  if sortedSplit[0] == '*':
      for i in range(len(sortedSplit)):
          if sortedSplit[i] == '*':
              sortedSplit[i] = '.'
          else:
              sortedSplit[i] = '*'


  splits[idSplit] = "".join(sortedSplit)#words[1]


# read statFile
statFile.readline()
statFile.readline()

for line in statFile:
  words = line.split()
  idProb = int(words[0])
  prob = float(words[2])
  
  mySplits.append(Split(int(words[0]), float(words[2]), float(words[5])-float(words[4]), splits[idProb]))

# sort this
mySplits.sort()

cnt = 0
print "        id	FinalFreq 	Partition 	Error"
for split in mySplits:
  #print split.prob
  formatedProb = "%.4f" % split.prob
  formatedError = "%.4f" % split.error
  print( "\t" + repr(cnt) + "\t" + formatedProb + "\t" + split.split + "\t" + formatedError)
  cnt += 1
