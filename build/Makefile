##########################################################################
## Makefile.
##
## The present Makefile is a pure configuration file, in which
## you can select compilation options. Compilation dependencies
## are managed automatically through the Python library SConstruct.
##
## If you don't have Python, or if compilation doesn't work for other
## reasons, consult the CoevRJ user's guide for instructions on manual
## compilation.
##########################################################################

# USE: multiple arguments are separated by spaces.
#   For example: projectFiles = file1.cpp file2.cpp
#                optimFlags   = -O -finline-functions

# Leading directory of the CoevRJ source code
CoevRJ_Root  = /Users/meyerx/Projets/CoevRJ_Standalone
# Name of source files in current directory to compile and link with CoevRJ
projectFiles = $(CoevRJ_Root)/src/main.cpp
# Name of the folder where CoevRJ is compiled
buildDir = $(CoevRJ_Root)/build

# Path to external source files (other than CoevRJ)
srcPaths =
# Path to external libraries (other than CoevRJ)
libraryPaths = /Users/meyerx/Librairies/boost/lib
# Path to inlude directories (other than CoevRJ)
includePaths = /Users/meyerx/Librairies/eigen /Users/meyerx/Librairies/boost/include
# Dynamic and static libraries (other than CoevRJ)
libraries    = boost_serialization boost_regex pthread

# This is the recommended configuration for CoevRJ.
# Set optimization flags on/off
optimize     = true
# Set debug mode and debug flags on/off
debug        = false
# Set profiling flags on/off
profile      = false

# Compiler to use with MPI parallelism
parallelCXX  = mpicxx
# General compiler flags (e.g. -Wall to turn on all warnings on g++)
compileFlags = -Wall -Wnon-virtual-dtor -Wno-deprecated-declarations -DTIXML_USE_STL
# General linker flags (don't put library includes into this flag)
linkFlags    =
# Compiler flags to use when optimization mode is on
optimFlags   = -O3
# Compiler flags to use when debug mode is on
debugFlags   = -g
# Compiler flags to use when profile mode is on
profileFlags = -pg
# Number of processor employed for compilation
nbProc		= 4


##########################################################################
# All code below this line is just about forwarding the options
# to SConstruct. It is recommended not to modify anything there.
##########################################################################

SCons     = scons -j $(nbProc) -f SConstruct

SConsArgs = CoevRJ_Root=$(CoevRJ_Root) \
						buildDir=$(buildDir) \
            projectFiles="$(projectFiles)" \
            optimize=$(optimize) \
            debug=$(debug) \
            profile=$(profile) \
            MPIparallel=$(MPIparallel) \
            SMPparallel=$(SMPparallel) \
            usePOSIX=$(usePOSIX) \
            serialCXX=$(serialCXX) \
            parallelCXX=$(parallelCXX) \
            compileFlags="$(compileFlags)" \
            linkFlags="$(linkFlags)" \
            optimFlags="$(optimFlags)" \
            debugFlags="$(debugFlags)" \
            profileFlags="$(profileFlags)" \
            srcPaths="$(srcPaths)" \
            libraryPaths="$(libraryPaths)" \
            includePaths="$(includePaths)" \
            libraries="$(libraries)"


compile:
	$(SCons) $(SConsArgs)

clean:
	$(SCons) -c $(SConsArgs)
	#/bin/rm -vf `find $(CoevRJ_Root) -name '*~'`
