# CoevRJ

CoevRJ is a Bayesian method to analyze a nucleotide alignment and jointly estimate:

1. How many and which pairs of sites co-evolve thus differentiating from a background model of independent evolution.
2. The parameters of independent and dependent substitution models.
3. The underlying phylogenetic tree describing the relationships between sequences.  

This method captures the reciprocal effects of the phylogeny, the pairs of coevolving sites and the independent sites while accounting for parameter uncertainties.

## Substition models

### Coevolution

CoevRJ default substitution model is a mixture of
1. GTR+Gamma for sites evolving independently
2. Coev-type substitution model for sites evolving dependently, that is a pairwise or *doublet*-type of substition model (see [Dib et. al 2014 Bioinformatics](http://bioinformatics.oxfordjournals.org/content/30/9/1241), or [Meyer et al. 2019 PNAS](https://doi.org/10.1073/pnas.1813836116)).
To see how to setup configuration files for such analyses see the *Basic* and *Advanced CoevRJ analyses* tutorials linked in the tutorial section.

### Traditional

CoevRJ can also be used with more traditional models, that is under the assumption of independence among sites. Two substitutions models are available (i.e., HKY85 and GTR) with or without rate variation among site (i.e., +Gamma). To see how to setup configuration files for such analyses see the *Standard substitutions models and Adaptive Tree Proposals* tutorials linked in the tutorial section.

## Tutorial

### Installing
* [Installing CoevRJ](documentation/installation.md)

### Basic CoevRJ analyses
* [Using CoevRJ (command line)](documentation/execution.md)
* [CoevRJ outputs and post-processing](documentation/outputs.md)

### Advanced CoevRJ analyses
* [Using CoevRJ XML configuration files](documentation/configXML.md)
* [Using restart files](documentation/restart.md)

### Standard substitutions models and Adaptive Tree Proposals
* [Setting the XML configuration file](documentation/configXML_AdaptiveTM.md)
* [Post-processing the output](documentation/treeProposalPerformance.md)

### Misc
* [Obtaining a consensus tree](documentation/consensus.md)

## Data
### Simulated datasets
Example of simulated datasets can be found in the `data/simulation` folder.
### 16S ribosomal RNA
The alignment, consensus trees obtained with CoevRJ and `restart` files can be found in the `data/16SRNA` folder.
This alignment is stored here for convenience and was published in [Yeang et al., 2007](https://doi.org/10.1093/molbev/msm142).

### Protein-coding gene from eukyarotes
The alignments can be found in the `data/Eukaryotes` folder.
These alignments have been collected from [Ensembl! Release 67](http://may2012.archive.ensembl.org/index.html) and [Selectome](https://selectome.unil.ch). The alignment archived for convenience on this GIT repositiory have been subject to a filtering step where all the fully gapped positions (position with gap accross all species) were filtered out of the alignments.

### Adaptive Tree Proposals
The alignments from TreeBase used for the Adaptive Tree Proposals manuscript can be found in the `data/adaptiveTreeProp/alignments/TreeBase` folder. Some alignments simulated using the GTR+Gamma model can also be found in `data/alignments/adaptiveTreeProp/simulations` folder.

The reference splits frequency can be found found in the `data/adaptiveTreeProp/refSplits`.

## How to cite

Simultaneous Bayesian inference of phylogeny and molecular coevolution  
Xavier Meyer, Linda Dib, Daniele Silvestro, Nicolas Salamin  
Proceedings of the National Academy of Sciences Feb 2019, 201813836  
[DOI: 10.1073/pnas.1813836116](https://doi.org/10.1073/pnas.1813836116)
