//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseManager.cpp
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#include "BaseManager.h"

#include "mpi.h"

namespace Parallel {

BaseManager::BaseManager() : active(false) {

}

BaseManager::~BaseManager() {
}

BaseManager::role_t BaseManager::defineRole(const CommunicationLayer* commLayer) {
	// Are we part of this layer ?
	if(!commLayer->isBelonging()) return None;

	// Are there more than one person on the layer ?
	if(commLayer->getLocalNProc() == 1) return Sequential;

	// Parallel case : Am i the manager ?
	if(commLayer->getMyMasterGlobalRank() == commLayer->getMyGlobakRank()) {
		return Manager; // Yes
	} else {
		return Worker; // No
	}
}

std::string BaseManager::roleToString(role_t myRole) const {
	switch (myRole) {
		case None:
			return "None";
			break;
		case Worker:
			return "Worker";
			break;
		case Manager:
			return "Manager";
			break;
		case Sequential:
			return "Sequential";
			break;
	}

	return "----";
}

void BaseManager::setActive(const bool aActive) {
	active = aActive;
}
bool BaseManager::isActive() const {
	return active;
}

BaseTopology::sharedPtr_t BaseManager::getTopology() {
	return ptrBT;
}

template <>
void BaseManager::sendSerializedObject<BaseManager::STATIC_SIZE>(
		const Utils::Serialize::buffer_t &object, const int dest,
		const int tag, const CommunicationLayer *commL) const {

	MPI_Send(const_cast<char*>(object.data()), object.size(), MPI_CHAR, dest, tag, commL->getLocalCommMPI());
}

template <>
void BaseManager::recvSerializedObject<BaseManager::STATIC_SIZE>(
		Utils::Serialize::buffer_t &object, const int src,
		const int tag, const CommunicationLayer *commL) const {

	MPI_Status status;
	MPI_Recv(object.data(), object.size(), MPI_CHAR, src, tag, commL->getLocalCommMPI(), &status);
}

template <>
void BaseManager::exchangeSerializedObject<BaseManager::STATIC_SIZE>(
		const Utils::Serialize::buffer_t &objectSend, Utils::Serialize::buffer_t &objectRecv,
		const int other, const int tag, const CommunicationLayer *commL) const {

	MPI_Status status;
	MPI_Sendrecv(const_cast<char*>(objectSend.data()), objectSend.size(), MPI_CHAR, other, tag,
				 objectRecv.data(), objectRecv.size(), MPI_CHAR, other, tag,
				 commL->getLocalCommMPI(), &status);
}

template <>
void BaseManager::broadcastSerializedObject<BaseManager::STATIC_SIZE>(
		Utils::Serialize::buffer_t &object, const int root,
		const CommunicationLayer *commL) const {

	MPI_Bcast(object.data(), object.size(), MPI_CHAR, root, commL->getLocalCommMPI());
}

template <>
void BaseManager::sendSerializedObject<BaseManager::DYNAMIC_SIZE>(
		const Utils::Serialize::buffer_t &object, const int dest,
		const int tag, const CommunicationLayer *commL) const {

	MPI_Send(const_cast<char*>(object.data()), object.size(), MPI_CHAR, dest, tag, commL->getLocalCommMPI());
}

template <>
void BaseManager::recvSerializedObject<BaseManager::DYNAMIC_SIZE>(
		Utils::Serialize::buffer_t &object, const int src,
		const int tag, const CommunicationLayer *commL) const {

	MPI_Status status;
	// Check if a message is pending (blocking)
	MPI_Probe(src, tag, commL->getLocalCommMPI(), &status);

	// Create the temporary buffer
	int size = 0;
	MPI_Get_count(&status, MPI_CHAR, &size);
	object.resize(size);
	// Read the message
	MPI_Recv(object.data(), size, MPI_CHAR, src, tag, commL->getLocalCommMPI(), &status);
}

template <>
void BaseManager::exchangeSerializedObject<BaseManager::DYNAMIC_SIZE>(
		const Utils::Serialize::buffer_t &objectSend, Utils::Serialize::buffer_t &objectRecv,
		const int other, const int tag, const CommunicationLayer *commL) const {

	MPI_Status status;

	// Exchange buffer sizes
	long unsigned int mySize = objectSend.size();
	long unsigned int otherSize = 0;
	MPI_Sendrecv(&mySize, 1, MPI_UNSIGNED_LONG, other, tag,
				 &otherSize, 1, MPI_UNSIGNED_LONG, other, tag,
				 commL->getLocalCommMPI(), &status);

	objectRecv.resize(otherSize);
	MPI_Sendrecv(const_cast<char*>(objectSend.data()), objectSend.size(), MPI_CHAR, other, tag,
				 objectRecv.data(), objectRecv.size(), MPI_CHAR, other, tag,
				 commL->getLocalCommMPI(), &status);
}

template <>
void BaseManager::broadcastSerializedObject<BaseManager::DYNAMIC_SIZE>(
		Utils::Serialize::buffer_t &object, const int root,
		const CommunicationLayer *commL) const {

	long unsigned int size = object.size();
	MPI_Bcast(&size, 1, MPI_UNSIGNED_LONG, root, commL->getLocalCommMPI());

	if(object.size() != size) {
		object.resize(size);
	}
	MPI_Bcast(object.data(), object.size(), MPI_CHAR, root, commL->getLocalCommMPI());
}

} /* namespace Parallel */
