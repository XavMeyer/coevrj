//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * LikelihoodProcessor.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef LIKELIHOODPROCESSOR_H_
#define LIKELIHOODPROCESSOR_H_

#include <boost/math/distributions.hpp>
#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <sys/types.h>
#include <iostream>
#include <vector>

#include "DAG/Scheduler/BaseScheduler.h"
#include "Sampler/Samples/Sample.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep
#include "Utils/Uncopyable.h"
#include "math.h"
#include "Utils/MolecularEvolution/Parsimony/FastFitchEvaluator.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeManager.h"


namespace DAG { namespace Scheduler { class BaseScheduler; } }
namespace Sampler { class Sample; }

using namespace std;

namespace StatisticalModel {
namespace Likelihood {

namespace TR = ::MolecularEvolution::TreeReconstruction;

using Sampler::Sample;

class LikelihoodInterface : private Uncopyable {
public:
	typedef boost::shared_ptr< LikelihoodInterface > sharedPtr_t;

public:
	LikelihoodInterface();
	virtual ~LikelihoodInterface();

	virtual double processLikelihood(const Sampler::Sample &sample) = 0;
	virtual string getName(char sep=' ') const = 0;

	virtual size_t stateSize() const;
	virtual void setStateLH(const char* aState);
	virtual void getStateLH(char* aState) const;
	virtual double update(const std::vector<size_t>& pIndices, const Sample& sample);

	virtual double processLikelihoodSpecificPrior(const Sampler::Sample &sample);

	virtual void saveInternalState(const Sampler::Sample &sample, Utils::Serialize::buffer_t &buff){};
	virtual void loadInternalState(const Sampler::Sample &sample, const Utils::Serialize::buffer_t &buff){};

	virtual void initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, const std::vector<std::streamoff>& offsets);
	virtual std::vector<std::streamoff> getCustomizedLogSize() const;
	virtual void writeCustomizedLog(const Sampler::SampleUtils::vecPairItSample_t &iterSamples);

	virtual void doCustomOperations(size_t iteration, const Sampler::Sample &sample);

	virtual DAG::Scheduler::BaseScheduler* getScheduler() const;
	virtual std::vector<DAG::BaseNode*> defineDAGNodeToCompute(const Sample& sample);

	const std::vector<std::string>& getMarkerNames() const;
	const std::vector<double>& getMarkers() const;

	const Sampler::SampleUtils::vecSSMarkers_t& getSubSpaceMarkers() const;

	bool isLog() const;
	bool isUpdatable() const;

	static double getAlphaFromL(double l, double var);
	static double getOptiL(uint nP, uint nPPB, double var);

	TR::Tree::sharedPtr_t getCurrentTree() const;
	TR::TreeManager::sharedPtr_t getTreeManager() const;
	TR::Bipartition::BipartitionMonitor::sharedPtr_t getBipartitionMonitor() const;
	MolecularEvolution::Parsimony::FastFitchEvaluator::sharedPtr_t getFastFitchEvaluator() const;

protected:
	bool isLogLH, isUpdatableLH;
	std::vector<std::string> markerNames;
	std::vector<double> markers;

	Sampler::SampleUtils::vecSSMarkers_t vecSSMarkers;

	TR::Tree::sharedPtr_t curTree;
	TR::TreeManager::sharedPtr_t treeManager;
	TR::Bipartition::BipartitionMonitor::sharedPtr_t bipartitionMonitor;
	MolecularEvolution::Parsimony::FastFitchEvaluator::sharedPtr_t ffEvalPtr;

};

} // namespace Likelihood
} // namespace StatisticalModel

#endif /* LIKELIHOODPROCESSOR_H_ */
