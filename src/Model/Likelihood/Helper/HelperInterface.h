//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * HelperInterface.h
 *
 *  Created on: May 12, 2015
 *      Author: meyerx
 */

#ifndef HELPERINTERFACE_H_
#define HELPERINTERFACE_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>

#include "../Likelihoods.h"
#include "Model/Model.h"
#include "ParameterBlock/Block.h"
#include "ParameterBlock/Blocks.h"
#include "Sampler/ReversibleJump/RJMoveManager.h"
#include "Sampler/Samples/Sample.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"

namespace StatisticalModel { class Parameters; }

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

using namespace ParameterBlock;

class HelperInterface {
public:
	typedef boost::shared_ptr<HelperInterface> sharedPtr_t;

public:
	HelperInterface();
	virtual ~HelperInterface();

	static sharedPtr_t createHelper(LikelihoodInterface *likPtr);

	virtual void defineParameters(Parameters &params) const = 0;
	virtual void defineBlocks(Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, Model &model, Blocks &blocks) const = 0;
	virtual void defineOptiBlocks(Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, Model &model, Blocks &blocks) const;
	virtual void defineRandomBlocks(Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, Model &model, Blocks &blocks) const;
	virtual Sample defineInitSample(const Parameters &params) const = 0;

	virtual void printSample(const Parameters &params, const Sample &sample, const char sep='\t') const;
	std::string sampleToString(const Parameters &params, const Sample &sample, const char sep='\t') const;

	virtual Sampler::RJMCMC::RJMoveManager::sharedPtr_t createRJMoveManager(size_t aSeed, size_t iType, StatisticalModel::Model &model) const;
	virtual std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> createRJInitMoves(StatisticalModel::Model &model) const;
	virtual std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> createRJUpdateMoves(Sampler::Sample &sample, StatisticalModel::Model &model) const;

};

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
#endif /* HELPERINTERFACE_H_ */
