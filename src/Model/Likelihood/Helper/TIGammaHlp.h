//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TIGammaHlp.h
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#ifndef TIGAMMA_HLP_H_
#define TIGAMMA_HLP_H_

#include "../Likelihoods.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "HelperInterface.h"
#include "ParameterBlock/Block.h"
#include "Sampler/Samples/Sample.h"

namespace StatisticalModel { class Parameters; }
namespace StatisticalModel { namespace Likelihood { namespace TreeInference { class Base; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

class TIGammaHlp: public HelperInterface {
public:
	TIGammaHlp(TIGamma::Base *aPtrLik);
	~TIGammaHlp();

	void defineParameters(Parameters &params) const;
	void defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, Model &aModel, Blocks &blocks) const;
	Sample defineInitSample(const Parameters &params) const;
	void printSample(const Parameters &params, const Sample &sample, const char sep) const;


private:

	static const double DEFAULT_P;
	static const bool USE_ST_FREQ, USE_DIRICHLET_PROPOSALS;

	TIGamma::Base *ptrLik;

};

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TIGAMMA_HLP_H_ */
