//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TIGammaHlp.cpp
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#include "TIGammaHlp.h"

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/types.h>

#include "Model/Parameter/ParamDblDef.h"
#include "Model/Parameter/ParamIntDef.h"
#include "Model/Parameter/ParameterType.h"
#include "Model/Parameter/Parameters.h"
#include "Parallel/Manager/MCMCManager.h"
#include "Parallel/Manager/MpiManager.h"
#include "Model/Prior/PriorInterface.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"
#include "cmath"

#include "../../../ParameterBlock/BlockModifier/BlockModifierInterface.h"
using StatisticalModel::PriorInterface;


namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

const double TIGammaHlp::DEFAULT_P = 0.6;
const bool TIGammaHlp::USE_ST_FREQ = true;
const bool TIGammaHlp::USE_DIRICHLET_PROPOSALS = false;

TIGammaHlp::TIGammaHlp(TIGamma::Base *aPtrLik) :
	ptrLik(aPtrLik){

}

TIGammaHlp::~TIGammaHlp() {
}

void TIGammaHlp::defineParameters(Parameters &params) const {

	double treeFreq = 0.4;
	double rateFreq = 0.06;
	double branchFreq = 1.-(treeFreq+rateFreq);

	const bool reflect = true;//ptrLik->getTreeUpdateStrategy() != ptrLik->TREE_LV_STRATEGY;
	const bool reflectBL = true;//ptrLik->getTreeUpdateStrategy() != ptrLik->TREE_LV_STRATEGY;
	RNG *myRNG = Parallel::mpiMgr().getPRNG();

	ParamIntDef pTree(TIGamma::Base::NAME_TREE_PARAMETER, PriorInterface::createNoPrior(), reflect);
	pTree.setFreq(treeFreq/2.);
	pTree.setType(StatisticalModel::TREE_PARAM);
	params.addBaseParameter(pTree);

	size_t nRate = 0;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nRate = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;

		for(size_t iT=0; iT<3; iT++) {
			std::stringstream ssName;
			ssName << "StFreq (" << MolecularEvolution::Definition::Nucleotides::NUCL_BASE[iT] << ")";
			//ParamDblDef pTheta(ssName.str(), PriorInterface::createUniformBoost(myRNG, 0., 100.), reflect);
			ParamDblDef pStFreq(ssName.str(), PriorInterface::createNoPrior(), reflect);
			pStFreq.setFreq(rateFreq/(double)(nRate+3));
			if(USE_ST_FREQ) {
				pStFreq.setMin(0.);
				pStFreq.setMax(500.);
			} else {
				pStFreq.setMin(1.);
				pStFreq.setMax(1.);
			}
			pStFreq.setType(StatisticalModel::RATE_PARAM);
			params.addBaseParameter(pStFreq);
		}

		for(size_t iT=0; iT<nRate; iT++) {
			std::stringstream ssName;
			ssName << "Theta_" << iT << " (" << MolecularEvolution::MatrixUtils::GTR_MF::PARAMETERS_NAME[iT] << ")";
			//ParamDblDef pTheta(ssName.str(), PriorInterface::createUniformBoost(myRNG, 0., 100.), reflect);
			ParamDblDef pTheta(ssName.str(), PriorInterface::createNoPrior(), reflect);
			pTheta.setFreq(rateFreq/(double)(nRate));
			pTheta.setMin(0.);
			if(nRate == 6) {
				pTheta.setMax(1.);
			} else {
				pTheta.setMax(500.);
			}
			pTheta.setType(StatisticalModel::RATE_PARAM);
			params.addBaseParameter(pTheta);
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::HKY85) {

		for(size_t iT=0; iT<3; iT++) {
			std::stringstream ssName;
			ssName << "StFreq (" << MolecularEvolution::Definition::Nucleotides::NUCL_BASE[iT] << ")";
			//ParamDblDef pTheta(ssName.str(), PriorInterface::createUniformBoost(myRNG, 0., 100.), reflect);
			ParamDblDef pStFreq(ssName.str(), PriorInterface::createNoPrior(), reflect);
			pStFreq.setFreq(rateFreq/(double)(nRate+3));
			if(USE_ST_FREQ) {
				pStFreq.setMin(0.);
				pStFreq.setMax(500.);
			} else {
				pStFreq.setMin(1.);
				pStFreq.setMax(1.);
			}
			pStFreq.setType(StatisticalModel::RATE_PARAM);
			params.addBaseParameter(pStFreq);
		}

		nRate = 1;
		ParamDblDef pKappa("Kappa", PriorInterface::createUniformBoost(myRNG, 0., 100.), reflect);
		pKappa.setFreq(rateFreq/(double)(nRate));
		pKappa.setMin(0);
		pKappa.setMax(100.);
		pKappa.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pKappa);
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::defineParameters(Parameters &params) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}

	{ // Gamma - alpha params
		const double GAMMA_RATE_ALPHA_EXPRATE = 1.0;
		std::string alphaName("alpha");
		double min = 1.;
		double max = 1.;
		PriorInterface::sharedPtr_t alphaPrior = PriorInterface::createNoPrior();
		if(ptrLik->getNGamma() > 1) {
			min = 0.;
			max = 500.;
			alphaPrior = PriorInterface::createExponentialBoost(myRNG, GAMMA_RATE_ALPHA_EXPRATE);
		}

		ParamDblDef pAlpha(alphaName, alphaPrior, reflect);
		pAlpha.setMin(min);
		pAlpha.setMax(max);
		pAlpha.setFreq(rateFreq/(double)(nRate+1));
		pAlpha.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pAlpha);
	}

	for(size_t iB=0; iB<ptrLik->getNBranch(); ++iB) {
		std::stringstream ssBranch;
		ssBranch << "BL_" << iB;
		ParamDblDef pBL(ssBranch.str(), PriorInterface::createExponentialBoost(myRNG, 10.), reflectBL);
		pBL.setFreq(branchFreq/(double)ptrLik->getNBranch());
		pBL.setMin(0.);
		pBL.setMax(1000.);
		pBL.setType(StatisticalModel::BRANCH_LENGTH);
		params.addBaseParameter(pBL);
	}


}

void TIGammaHlp::defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, Model &aModel, Blocks &blocks) const {

	bool INDEPENDENT_BL_PROPOSAL = false;
	if(adaptiveType == Block::INDEPENDENT_GAMMA) {
		adaptiveType = Block::PCA_DOUBLE_VARIABLES;
		INDEPENDENT_BL_PROPOSAL = true;
	}

	size_t iParam = 0;
	size_t nBLPerB = blockSize[0];
	div_t divRes = div((int)ptrLik->getNBranch(), (int)nBLPerB);

	size_t nB_BL = divRes.quot;
	if(divRes.rem > (int)((double)nBLPerB*4./5.)) nB_BL++;

	size_t nElemBRate = aModel.getParams().getNBaseParameters() - (ptrLik->getNBranch()+1);


	double epsilonFreq = ptrLik->getEpsilonFreq();
	double powerFreq = ptrLik->getPowerFreq();
	if(ptrLik->getTreeUpdateStrategy() == StatisticalModel::Likelihood::TIGamma::Base::DEFAULT_STRATEGY) {

		double moveFreq = 1.0;

		Block::sharedPtr_t bTreeSTNNI(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeSTNNI->setName("STNNI");
		bTreeSTNNI->addParameter(iParam,
				BlockModifier::createSTNNIBM(ptrLik->getTreeManager(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeSTNNI);

		if(ptrLik->getCurrentTree()->getTerminals().size() > 4) {
			Block::sharedPtr_t bTreeESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
			bTreeESPR->setName("ESPR");
			bTreeESPR->addParameter(iParam,
					BlockModifier::createESPRBM(DEFAULT_P, ptrLik->getTreeManager(), aModel),
					moveFreq / 2.0);
			blocks.addBaseBlock(bTreeESPR);

			Block::sharedPtr_t bTreeETBR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
			bTreeETBR->setName("ETBR");
			bTreeETBR->addParameter(iParam,
					BlockModifier::createETBRBM(DEFAULT_P, ptrLik->getTreeManager(), aModel),
					moveFreq/5.0);
			blocks.addBaseBlock(bTreeETBR);
		}

	} else if(ptrLik->getTreeUpdateStrategy() == StatisticalModel::Likelihood::TIGamma::Base::ADAPTIVE_STRATEGY) {

		double moveFreq = 1.0;

		Block::sharedPtr_t bTreeSTNNI(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeSTNNI->setName("STNNI");
		bTreeSTNNI->addParameter(iParam,
				BlockModifier::createSTNNIBM(ptrLik->getTreeManager(), aModel),
				moveFreq / 10.0);
		blocks.addBaseBlock(bTreeSTNNI);

		Block::sharedPtr_t bTreeESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR->setName("ESPR");
		bTreeESPR->addParameter(iParam,
				BlockModifier::createESPRBM(DEFAULT_P, ptrLik->getTreeManager(), aModel),
				moveFreq / 10.0);
		blocks.addBaseBlock(bTreeESPR);

		Block::sharedPtr_t bTreeESPR1(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR1->setName("A-STNNI");
		bTreeESPR1->addParameter(iParam,
				BlockModifier::createAdaptiveESPRBM(TR::AdaptiveESPR::ONE_STEP, TR::AdaptiveESPR::BOTH_BIASED, epsilonFreq, powerFreq, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeESPR1);

		Block::sharedPtr_t bTreeESPR2(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR2->setName("A-2SPR");
		bTreeESPR2->addParameter(iParam,
				BlockModifier::createAdaptiveESPRBM(TR::AdaptiveESPR::TWO_STEP, TR::AdaptiveESPR::BOTH_BIASED, epsilonFreq, powerFreq, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeESPR2);

		{
		Block::sharedPtr_t bTreeAdaptiveETBR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeAdaptiveETBR->setName("AdaptivePBJ");
		bTreeAdaptiveETBR->addParameter(iParam,
				BlockModifier::createAdaptivePBJBM(TR::AdaptivePBJ::PATH_AND_ADAPTIVE_SPR, DEFAULT_P, epsilonFreq, powerFreq, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeAdaptiveETBR);
		}

	} else if(ptrLik->getTreeUpdateStrategy() == StatisticalModel::Likelihood::TIGamma::Base::GUIDED_STRATEGY) {

		double moveFreq = 1.0;

		Block::sharedPtr_t bTreeSTNNI(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeSTNNI->setName("STNNI");
		bTreeSTNNI->addParameter(iParam,
				BlockModifier::createSTNNIBM(ptrLik->getTreeManager(), aModel),
				moveFreq/10);
		blocks.addBaseBlock(bTreeSTNNI);

		Block::sharedPtr_t bTreeESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR->setName("ESPR");
		bTreeESPR->addParameter(iParam,
				BlockModifier::createESPRBM(DEFAULT_P, ptrLik->getTreeManager(), aModel),
				moveFreq/10);
		blocks.addBaseBlock(bTreeESPR);

		{
		Block::sharedPtr_t bTreeGuidedSTNNI(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeGuidedSTNNI->setName("G-STNNI");
		bTreeGuidedSTNNI->addParameter(iParam,
				BlockModifier::createGuidedSTNNIBM(0, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), ptrLik->getFastFitchEvaluator(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeGuidedSTNNI);
		}

		{
		Block::sharedPtr_t bTreeGuidedESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeGuidedESPR->setName("G-1SPR");
		bTreeGuidedESPR->addParameter(iParam,
				BlockModifier::createGuidedESPRBM(1, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), ptrLik->getFastFitchEvaluator(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeGuidedESPR);
		}

		{
		Block::sharedPtr_t bTreeGuidedESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeGuidedESPR->setName("G-2SPR");
		bTreeGuidedESPR->addParameter(iParam,
				BlockModifier::createGuidedESPRBM(2, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), ptrLik->getFastFitchEvaluator(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeGuidedESPR);
		}

	} else if(ptrLik->getTreeUpdateStrategy() == StatisticalModel::Likelihood::TIGamma::Base::MIXED_STRATEGY) {

		double moveFreq = 1.0;

		Block::sharedPtr_t bTreeSTNNI(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeSTNNI->setName("STNNI");
		bTreeSTNNI->addParameter(iParam,
				BlockModifier::createSTNNIBM(ptrLik->getTreeManager(), aModel),
				moveFreq/3.0);
		blocks.addBaseBlock(bTreeSTNNI);

		Block::sharedPtr_t bTreeESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR->setName("ESPR");
		bTreeESPR->addParameter(iParam,
				BlockModifier::createESPRBM(DEFAULT_P, ptrLik->getTreeManager(), aModel),
				moveFreq/3.0);
		blocks.addBaseBlock(bTreeESPR);

		Block::sharedPtr_t bTreeESPR1(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR1->setName("A-STNNI");
		bTreeESPR1->addParameter(iParam,
				BlockModifier::createAdaptiveESPRBM(TR::AdaptiveESPR::ONE_STEP, TR::AdaptiveESPR::BOTH_BIASED, epsilonFreq, powerFreq, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeESPR1);

		Block::sharedPtr_t bTreeESPR2(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR2->setName("A-2SPR");
		bTreeESPR2->addParameter(iParam,
				BlockModifier::createAdaptiveESPRBM(TR::AdaptiveESPR::TWO_STEP, TR::AdaptiveESPR::BOTH_BIASED, epsilonFreq, powerFreq, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeESPR2);

		{
		Block::sharedPtr_t bTreeAdaptiveETBR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeAdaptiveETBR->setName("A-PBJ");
		bTreeAdaptiveETBR->addParameter(iParam,
				BlockModifier::createAdaptivePBJBM(TR::AdaptivePBJ::PATH_AND_ADAPTIVE_SPR, DEFAULT_P, epsilonFreq, powerFreq, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeAdaptiveETBR);
		}

		{
		Block::sharedPtr_t bTreeGuidedESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeGuidedESPR->setName("G-1SPR");
		bTreeGuidedESPR->addParameter(iParam,
				BlockModifier::createGuidedESPRBM(1, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), ptrLik->getFastFitchEvaluator(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeGuidedESPR);
		}

		{
		Block::sharedPtr_t bTreeGuidedESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeGuidedESPR->setName("G-2SPR");
		bTreeGuidedESPR->addParameter(iParam,
				BlockModifier::createGuidedESPRBM(2, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), ptrLik->getFastFitchEvaluator(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeGuidedESPR);
		}

	} else if(ptrLik->getTreeUpdateStrategy() == StatisticalModel::Likelihood::TIGamma::Base::OPTIMIZED_STRATEGY) {

		double moveFreq = 1.0;

		Block::sharedPtr_t bTreeSTNNI(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeSTNNI->setName("STNNI");
		bTreeSTNNI->addParameter(iParam,
				BlockModifier::createSTNNIBM(ptrLik->getTreeManager(), aModel),
				moveFreq / 15.0);
		blocks.addBaseBlock(bTreeSTNNI);

		Block::sharedPtr_t bTreeESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR->setName("ESPR");
		bTreeESPR->addParameter(iParam,
				BlockModifier::createESPRBM(DEFAULT_P, ptrLik->getTreeManager(), aModel),
				moveFreq / 20.0);
		blocks.addBaseBlock(bTreeESPR);

		Block::sharedPtr_t bTreeESPR1(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR1->setName("A-STNNI");
		bTreeESPR1->addParameter(iParam,
				BlockModifier::createAdaptiveESPRBM(TR::AdaptiveESPR::ONE_STEP, TR::AdaptiveESPR::BOTH_BIASED, epsilonFreq, powerFreq, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel),
				moveFreq);
		blocks.addBaseBlock(bTreeESPR1);

		Block::sharedPtr_t bTreeESPR2(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR2->setName("A-2SPR");
		bTreeESPR2->addParameter(iParam,
				BlockModifier::createAdaptiveESPRBM(TR::AdaptiveESPR::TWO_STEP, TR::AdaptiveESPR::BOTH_BIASED, epsilonFreq, powerFreq, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel),
				moveFreq / 10.0);
		blocks.addBaseBlock(bTreeESPR2);

		{
		Block::sharedPtr_t bTreeAdaptiveETBR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeAdaptiveETBR->setName("A-PBJ");
		bTreeAdaptiveETBR->addParameter(iParam,
				BlockModifier::createAdaptivePBJBM(TR::AdaptivePBJ::PATH_AND_ADAPTIVE_SPR, DEFAULT_P, epsilonFreq, powerFreq, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel),
				moveFreq/5.0);
		blocks.addBaseBlock(bTreeAdaptiveETBR);
		}

		{
		Block::sharedPtr_t bTreeGuidedESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeGuidedESPR->setName("G-2SPR");
		bTreeGuidedESPR->addParameter(iParam,
				BlockModifier::createGuidedESPRBM(2, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), ptrLik->getFastFitchEvaluator(), aModel),
				moveFreq/10.0);
		blocks.addBaseBlock(bTreeGuidedESPR);
		}

	}
	iParam++;


	if(USE_DIRICHLET_PROPOSALS) {

		Block::sharedPtr_t bStFreq(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bStFreq->setName("BlockSTFreq-dirichlet");
		if(USE_ST_FREQ) {
			for(size_t i=0; i<3; ++i){
				bStFreq->addParameter(iParam, aModel.getParams().getParameterFreq(iParam));
				iParam++;
			}
			bStFreq->setBlockModifier(BlockModifier::createCenteredDirichletBM(false, aModel));
			blocks.addBaseBlock(bStFreq);
		} else {
			iParam += 3;
		}

		Block::sharedPtr_t bRate(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bRate->setName("BlockRates-dirichlet");
		// Add kappa or thethas, Px and Wx and gamma
		for(size_t i=0; i<nElemBRate-4; ++i){
			//std::cout << aModel.getParams().getName(iParam) << "\t" << aModel.getParams().getBoundMin(iParam) << std::endl;
			if(aModel.getParams().getBoundMin(iParam) < aModel.getParams().getBoundMax(iParam)) {
				bRate->addParameter(iParam, aModel.getParams().getParameterFreq(iParam));
			}
			iParam++;
		}
		bRate->setBlockModifier(BlockModifier::createCenteredDirichletBM(false, aModel));
		blocks.addBaseBlock(bRate);

		Block::sharedPtr_t bAlpha(new Block(adaptiveType));
		bAlpha->setName("BlockAlpha");
		if(aModel.getParams().getBoundMin(iParam) < aModel.getParams().getBoundMax(iParam)) {
			bAlpha->addParameter(iParam,
					BlockModifier::createGaussianWindow(0.03, aModel),
					aModel.getParams().getParameterFreq(iParam));
		}
		iParam++;
		blocks.addBaseBlock(bAlpha);

	} else {

		Block::sharedPtr_t bRate(new Block(adaptiveType));
		bRate->setName("BlockRatesAndAlpha");
		// Add kappa or thethas, Px and Wx and gamma

		std::vector< std::vector< size_t > > simplexPIndices;
		for(size_t i=0; i<nElemBRate; ++i){
			//std::cout << aModel.getParams().getName(iParam) << "\t" << aModel.getParams().getBoundMin(iParam) << std::endl;
			if(aModel.getParams().getBoundMin(iParam) < aModel.getParams().getBoundMax(iParam)) {
				bRate->addParameter(iParam,
						BlockModifier::createGaussianWindow(0.03, aModel),
						aModel.getParams().getParameterFreq(iParam));
			}

			if(i>=0 && i <=2 && aModel.getParams().getBoundMin(iParam) < aModel.getParams().getBoundMax(iParam)) { // stFreq
				if(i==0){
					std::vector<size_t> tmp(1,iParam);
					simplexPIndices.push_back(tmp);
				} else {
					simplexPIndices.back().push_back(iParam);
				}
			}

			if(i>=3 && i <= (nElemBRate-2) && aModel.getParams().getBoundMin(iParam) < aModel.getParams().getBoundMax(iParam)) { // stFreq
				if(i==3 && nElemBRate-2 != 3){
					std::vector<size_t> tmp(1,iParam);
					simplexPIndices.push_back(tmp);
				} else {
					simplexPIndices.back().push_back(iParam);
				}
			}

			iParam++;
		}
		bRate->setTransformedSimplexIndices(simplexPIndices);

		blocks.addBaseBlock(bRate);
	}

	// Branch length
	if(!INDEPENDENT_BL_PROPOSAL) {
		Block::sharedPtr_t bBL[nB_BL];
		for(size_t iB=0; iB < nB_BL; ++iB){
			bBL[iB].reset(new Block(adaptiveType));
			stringstream ss;
			ss << "BranchLength_" << iB;
			bBL[iB]->setName(ss.str());
		}

		std::vector<size_t> activeParamsIdx;
		for(size_t iBL=iParam; iBL<iParam+ptrLik->getNBranch(); ++iBL){
			activeParamsIdx.push_back(iBL);
		}
		//std::random_shuffle(activeParamsIdx.begin(), activeParamsIdx.end());

		size_t iP = 0;
		for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
			uint idx = iBL/nBLPerB;
			if(idx >= nB_BL) idx = iBL % nB_BL;
			iParam = activeParamsIdx[iP];
			const double l = ptrLik->getOptiL(Parallel::mcmcMgr().getNProposal(), 1, 0.002*0.002)/pow(nBLPerB, 0.75);
			bBL[idx]->addParameter(iParam,
					BlockModifier::createGaussianWindow(l, aModel),
					aModel.getParams().getParameterFreq(iParam));
			++iP;
		}

		for(size_t iB=0; iB < nB_BL; ++iB){
			blocks.addBaseBlock(bBL[iB]);
		}
	} else {

		// Test block
		Block::sharedPtr_t blockAllBL(new Block(ParameterBlock::Block::NOT_ADAPTIVE)); // Fixme Trying adaptive indep gamma for BL

		// Branch length
		std::vector<size_t> activeParamsIdx;
		for(size_t iBL=iParam; iBL<iParam+ptrLik->getNBranch(); ++iBL){
			activeParamsIdx.push_back(iBL);
			blockAllBL->addParameter(iBL, aModel.getParams().getParameterFreq(iBL));
		}
		blockAllBL->setBlockModifier(BlockModifier::createIndependentBranchLengthBM(nBLPerB, ptrLik->getTreeManager(), ptrLik->getBipartitionMonitor(), aModel));
		blocks.addBaseBlock(blockAllBL);
	}
}


Sample TIGammaHlp::defineInitSample(const Parameters &params) const {
	//const double epsilon = 1e-2;
	const RNG* rng = Parallel::mpiMgr().getPRNG();

	Sampler::Sample sample;

	sample.getIntValues().push_back(ptrLik->getCurrentTree()->getHashKey());

	size_t nSubParams;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nSubParams = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;

		if(USE_ST_FREQ) {
			for(size_t iT=0; iT<3; ++iT) {
				sample.getDblValues().push_back(1.-rng->genUniformDbl());		// [0->4] Rand Theta {1..5}
			}
		} else {
			for(size_t iT=0; iT<3; ++iT) {
				sample.getDblValues().push_back(1.0);
			}
		}

		for(size_t iT=0; iT<nSubParams; ++iT) {
			sample.getDblValues().push_back(rng->genUniformDbl());		// [0->4] Rand Theta {1..5}
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::HKY85) {
		nSubParams = 1;

		if(USE_ST_FREQ) {
			for(size_t iT=0; iT<3; ++iT) {
				sample.getDblValues().push_back(1.-rng->genUniformDbl());		// [0->4] Rand Theta {1..5}
			}
		} else {
			for(size_t iT=0; iT<3; ++iT) {
				sample.getDblValues().push_back(1.0);
			}
		}

		sample.getDblValues().push_back(0.5+rng->genUniformDbl());		// [0] Rand Kappa
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::defineBlocks(const std::vector<size_t> &blockSize, Model &aModel, Blocks &blocks) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}

	if(ptrLik->getNGamma() > 1) {
		sample.getDblValues().push_back(0.5+rng->genUniformDbl()); // Random alpha for Gamma rates
	} else {
		sample.getDblValues().push_back(1.0);
	}

	if(!ptrLik->getInitialBranchLenghts().empty()) {
		for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
			sample.getDblValues().push_back(ptrLik->getInitialBranchLenghts()[iBL]/3.0);
		}
	} else {
		for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
			sample.getDblValues().push_back(rng->genExponential(100.));
		}
	}

	return sample;
}

void TIGammaHlp::printSample(const Parameters &params, const Sample &sample, const char sep) const {

	const std::vector<double> &values = sample.getDblValues();

	size_t nNuclParams = 0;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nNuclParams = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF+3;
		std::cout << "StFreq :\t";
		for(size_t iT=0; iT<3; iT++) {
			std::cout << values[iT] << "\t";
		}
		std::cout << std::endl;
		std::cout << "Thetas :\t";
		for(size_t iT=0; iT<5; iT++) {
			std::cout << values[iT] << "\t";
		}
		std::cout << std::endl;
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::HKY85) {
		nNuclParams = 1+3;
		std::cout << "StFreq :\t";
		for(size_t iT=0; iT<3; iT++) {
			std::cout << values[iT] << "\t";
		}
		std::cout << "Kappa :\t" << values[3] << std::endl;
	} else {
		std::cerr << "Error : void PositiveSelectionHlp<>::defineInitNuclSubstSample(const RNG* rng, Sampler::Sample &sample) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by PositiveSelectionHlp"<< std::endl;
		abort();
	}

	size_t iParam = nNuclParams;
	std::cout << "Gamma :\t" << values[iParam] << std::endl;
	iParam++;

	std::cout << "Branch lenghts :\t";
	for(size_t i=iParam; i<values.size(); ++i) {
		std::cout << values[i] << "\t";
	}
	std::cout << endl;

	std::cout << "Topology :\t";
	std::cout << ptrLik->getCurrentTree()->getIdString();
	std::cout << endl;
}

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
