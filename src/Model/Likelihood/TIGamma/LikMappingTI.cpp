/*
 * LikMappingTI.cpp
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#include "LikMappingTI.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

LikMappingTI::LikMappingTI(const size_t aNGamma, const std::string aName) :
		N_GAMMA(aNGamma), name(aName), branchMDAG(N_GAMMA, NULL) {

	branchLength = 0.;

}

LikMappingTI::~LikMappingTI() {
}


void LikMappingTI::addBranchMatrixGammaNode(BranchMatrixGammaNode *node) {
	branchMDAG[node->getIDGamma()] = node;
}

size_t LikMappingTI::getNumberEdgeGammaNode() const {
	return treeStructDAG.size();
}

void LikMappingTI::addEdgeGammaNode(EdgeGammaNode *node) {
	treeStructDAG.push_back(node);
}

EdgeGammaNode* LikMappingTI::getEdgeGammaNode(size_t aIdGamma) {
	return treeStructDAG[aIdGamma];
}


std::vector<EdgeGammaNode*>& LikMappingTI::getEdgeGammaNodes() {
	return treeStructDAG;
}

BranchMatrixGammaNode* LikMappingTI::getBranchMatrixGammaNode(size_t aIdGamma) {
	assert(aIdGamma < N_GAMMA);
	return branchMDAG[aIdGamma];
}


std::vector<BranchMatrixGammaNode *>& LikMappingTI::getBranchMatrixGammaNodes() {
	return branchMDAG;
}

void LikMappingTI::setBranchLength(const double aBL, std::set<DAG::BaseNode*> &updatedNodes) {
	if(branchLength != aBL) {

		//std::cout << "BL " << branchLength << "\t" << aBL << " updated" << std::endl;

		// FIXME
		//std::stringstream ss;
		//ss << "[" << Parallel::mcmcMgr().getRankProposal() << "] upd : " << branchLength << " -> " << aBL << " (" << branchMDAG.size() << " -> " ;
		branchLength = aBL;

		for(size_t i=0; i<branchMDAG.size(); ++i){
			branchMDAG[i]->setBranchLength(branchLength);
			updatedNodes.insert(branchMDAG[i]);
			//ss << branchMDAG[i]->getId() << ", ";
		}
		//ss << std::endl;
		//std::cout << ss.str();
	}
}

double LikMappingTI::getBranchLength() const {
	return branchLength;
}

const std::string& LikMappingTI::getName() const {
	return name;
}

std::string LikMappingTI::getNewickSubstring() const {
	std::stringstream ss;
	ss << name;
	ss << ":" ;
	ss << branchLength;
	return ss.str();
}

std::string LikMappingTI::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << name << " - " << branchLength;
	return ss.str();
}

void LikMappingTI::doAddChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping) {

	LikMappingTI* childTI = dynamic_cast<LikMappingTI*>(childLikMapping.get());
	assert(childTI);

	// Add DAG link
	for(size_t iE=0; iE<getNumberEdgeGammaNode(); ++iE) {
		getEdgeGammaNode(iE)->addChild(childTI->getEdgeGammaNode(iE));
	}
}

void LikMappingTI::doRemoveChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping) {

	LikMappingTI* childTI = dynamic_cast<LikMappingTI*>(childLikMapping.get());
	assert(childTI);

	// Remove DAG link
	for(size_t iE=0; iE<getNumberEdgeGammaNode(); ++iE) {
		EdgeGammaNode *pNode = getEdgeGammaNode(iE);
		assert(pNode != NULL);
		EdgeGammaNode *cNode = childTI->getEdgeGammaNode(iE);
		assert(cNode != NULL);
		pNode->removeChild(cNode);
	}
}

void LikMappingTI::doOnApplyChange() {
	for(size_t iE=0; iE<getNumberEdgeGammaNode(); ++iE) {
		EdgeGammaNode *pNode = getEdgeGammaNode(iE);
		pNode->setDone(); // A bit dirty but necessary
	}
}

void LikMappingTI::doOnUpdate(std::set<DAG::BaseNode*> &updatedNodes) {
	for(size_t iE=0; iE<getNumberEdgeGammaNode(); ++iE){
		ParentGammaNode *pNode = dynamic_cast<ParentGammaNode*>(getEdgeGammaNode(iE));
		if(pNode) {
			pNode->updated();
			updatedNodes.insert(pNode);
		}
	}
}


} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
