//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SampleWrapper.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "SampleWrapper.h"

#include "Model/Likelihood/TIGamma/Nodes/Types.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"
#include <cstdlib>

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

const size_t SampleWrapper::N_KAPPA = 1;
const size_t SampleWrapper::N_THETA = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;

SampleWrapper::SampleWrapper(const size_t aNBranch, const MU::nuclModel_t aNuclModel,
							 const std::vector<long int> &intVals, const std::vector<double> &sample) :
		N_BL(aNBranch), treeH(intVals.front()) {

	// [stFreq (4)] [Nucl. model parameters (1 or 5)] [alpha (1)] [branches remainder]
	size_t iS=0;

	// [Nucl. model parameters (1 or 5)]
	if(aNuclModel == MU::GTR) {

		for(size_t iT=0; iT<3; iT++) {
			stFreq.push_back(sample[iS]);
			++iS;
		}

		for(size_t iT=0; iT<N_THETA; iT++) {
			thetas.push_back(sample[iS]);
			++iS;
		}

	} else if(aNuclModel == MU::HKY85) {

		for(size_t iT=0; iT<3; iT++) {
			stFreq.push_back(sample[iS]);
			++iS;
		}

		K = sample[iS];
		++iS;
	}

	alpha = sample[iS];
	++iS;

	// [branches remainder]
	for(size_t iB=0; iB<N_BL; ++iB) {
		branchLength.push_back(sample[iS]); // DEBUG TRANSFORMED_BL
		++iS;
	}
	assert(iS == sample.size());
}

SampleWrapper::~SampleWrapper() {
}

std::string SampleWrapper::toString() const {
	std::stringstream ss;
	ss.precision(4);
	ss << std::fixed;

	if(thetas.size() > 0 ) {

		ss << "Stationary freq :\t";
		for(size_t iT=0; iT<stFreq.size(); iT++) {
			ss << stFreq[iT] << "\t";
		}
		ss << std::endl;

		ss << "Thetas :\t";
		for(size_t iT=0; iT<N_THETA; ++iT) {
			ss << thetas[iT] << "\t";
		}
		ss << std::endl;
	} else {

		ss << "Stationary freq :\t";
		for(size_t iT=0; iT<stFreq.size(); iT++) {
			ss << stFreq[iT] << "\t";
		}
		ss << std::endl;

		ss << "Kappa :\t" << K << std::endl;
	}

	ss << "Alpha :\t" << alpha << std::endl;

	for(size_t i=0; i<N_BL; ++i) {
		ss << branchLength[i] << "\t";
	}

	return ss.str();
}

size_t SampleWrapper::nuclModelSize(const MU::nuclModel_t aNuclModel) {
	if(aNuclModel == MU::GTR) {
		return N_THETA;
	} else if(aNuclModel == MU::HKY85) {
		return N_KAPPA;
	} else {
		std::cerr << "Error : size_t SampleWrapper::offset(const MU::nuclModel_t aNuclModel);"<< std::endl;
		std::cerr << "Nucleotide model not supported."<< std::endl;
		abort();
		return 0;
	}
}

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
