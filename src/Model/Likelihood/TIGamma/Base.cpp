//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Base.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "Base.h"

#include <assert.h>
#include <math.h>

#include "Nodes/FinalResultNode.h"
#include "SampleWrapper.h"
#include "DAG/Scheduler/BaseScheduler.h"
#include "Parallel/Manager/MpiManager.h"
#include "Sampler/Samples/Sample.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/Parsimony/ParsimonyEvaluator.h"
#include "cmath"
#include "mpi.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { class NewickParser; } }
namespace MolecularEvolution { namespace MatrixUtils { class GTR_MF; } }
namespace MolecularEvolution { namespace MatrixUtils { class HKY85_MF; } }
namespace MolecularEvolution { namespace MatrixUtils { class SingleOmegaMF; } }
namespace MolecularEvolution { namespace TreeReconstruction { class TreeNode; } }

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

class BranchMatrixNode;
class CPVNode;
class CombineSiteNode;
class EdgeNode;
class LeafNode;
class RootNode;

const std::string Base::NAME_TREE_PARAMETER = "TreeHash";
const Base::debug_t Base::doDebug = DISABLE_DEBUG;

Base::Base(const size_t aNThread, const nuclModel_t aNuclModel, const double aNGamma,
		   DL::NewickParser *aNP, const std::string &aFileAlign,
		   treeUpdateStrategy_t aStrat, double aEpsilonFreq, double aPowerFreq) :
				N_GAMMA(aNGamma), nThread(aNThread),
				nuclModel(aNuclModel), fileAlign(aFileAlign),
				epsilonFreq(aEpsilonFreq), powerFreq(aPowerFreq), TREE_UPDATE_STRATEGY(aStrat),
				treeMapper(branchMap), frequencies(TI_NUCL_NSYMBOLS){

	if(doDebug != DISABLE_DEBUG) {
		std::stringstream ss;
		ss << "dbgFile_" << Parallel::mcmcMgr().getRankProposal() << "_" << Parallel::mcmcMgr().getNProposal() << "P.txt";
		dbgFile.open(ss.str().c_str());
	}


	init(aNP);

	MPI_Barrier(MPI_COMM_WORLD);
}

Base::~Base() {
	//rootDAG->deleteChildren();
	//delete rootDAG;

	cleanTreeAndDAG();
	delete scheduler;
}


std::vector<double> Base::defineFrequency(DL::MSA &msa) const {
	return DL::NucleotideFrequencies(msa).getUniform();
}

double Base::defineLogUniformPriorTree(TR::Tree::sharedPtr_t aTree) const {
	size_t N = aTree->getTerminals().size();
	double logPrior = 0.;

	// (2n-5)!/[2^(n-3)*(n-3)!]
	// Number of tree is : (2N-4)! / [ (N-2)! * 2^(N-2) ]
	for(size_t i=(N-3)+1; i<(2*N-5)+1; ++i) {
		logPrior += log(i);
	}
	logPrior -= log(pow(2, N-3));

	return -logPrior;
}

void Base::init(DL::NewickParser *aNP) {

	first = true;
	isLogLH = true;
	isUpdatableLH = true;

	isTreeExternalyImported = false;

	DL::FastaReader fr(fileAlign);
	DL::MSA msa( DL::MSA::NUCLEOTIDE, fr.getAlignments(), false);
	DL::CompressedAlignements compressedAligns(msa, false, false);
	frequencies.set(defineFrequency(msa));

	std::vector<std::string> orderedTaxaNames = msa.getOrderedTaxaNames();
	treeManager.reset(new TR::TreeManager(orderedTaxaNames));

	bipartitionMonitor.reset(new TR::Bipartition::BipartitionMonitor(msa.getOrderedTaxaNames()));

	if(aNP != NULL) {
		curTree.reset(new TR::Tree(aNP->getRoot(), treeManager->getNameMapping()));
	} else {
		curTree.reset(new TR::Tree(true, msa, treeManager->getNameMapping()));
	}

	treeManager->setTree(curTree);
	treeManager->memorizeTree();
	curHash = curTree->getHashKey();

	bipartitionMonitor->init(curTree);

	nSite = compressedAligns.getNSite();

	prepareDAG();
	createTreeAndDAG(fr, msa, compressedAligns);

	// Recover initial branch length if existing
	if(aNP != NULL) {
		const std::map<size_t, size_t> &mapId = curTree->getNodeIdtoNewickMapping();
		assert(!mapId.empty());
		const std::map<size_t, double> &newickBL = aNP->getInitialBLs();
		initialBranchLenghts.resize(branchPtr.size());

		for(size_t iBL=0; iBL<branchPtr.size(); ++iBL) {
			std::map<size_t, size_t>::const_iterator itNewickId = mapId.find(branchPtr[iBL]->getId());
			assert(itNewickId != mapId.end());
			size_t newickId = itNewickId->second;
			std::map<size_t, double>::const_iterator itFind = newickBL.find(newickId);
			assert(itFind != newickBL.end());
			double initBL = itFind->second;
			initialBranchLenghts[iBL] = initBL;
		}
	}



	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << getCompressionReport(msa, compressedAligns);
		std::cout << "**************************************" << std::endl;
	}

	cntErrMsg = 0;
	if(nThread > 1) {
		Eigen::initParallel();
		//scheduler = new DAG::Scheduler::Static::ThreadSafeScheduler(nThread, rootDAG);
		//scheduler = new DAG::Scheduler::PriorityListII::ThreadSafeScheduler(nThread, rootDAG);
		scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR || nuclModel == MolecularEvolution::MatrixUtils::HKY85) {

		for(size_t iT=0; iT<4; ++iT) {
			std::stringstream ss;
			ss << "Norm. " << MD::Nucleotides::NUCL_BASE[iT];// "NormTheta_" << iT;
			markerNames.push_back(ss.str());
		}

		ptrGTRFreqPr.reset(new Utils::Statistics::DirichletPDF(4, 1.));
		markerNames.push_back("PriorNuclFreq");

	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {

		for(size_t iT=0; iT<6; ++iT) {
			std::stringstream ss;
			ss << "Norm. " << MolecularEvolution::MatrixUtils::GTR_MF::PARAMETERS_NAME[iT];// "NormTheta_" << iT;
			markerNames.push_back(ss.str());
		}

		ptrGTRPr.reset(new Utils::Statistics::DirichletPDF(6, 1.));
		markerNames.push_back("PriorGTR");
	}

	markerNames.push_back("Total_BL");

	markers.resize(markerNames.size());

	LOG_UNIFORM_PRIOR_TREE = defineLogUniformPriorTree(curTree);

	if(doDebug == PARSIMONY_DEBUG || TREE_UPDATE_STRATEGY == GUIDED_STRATEGY || TREE_UPDATE_STRATEGY == MIXED_STRATEGY || TREE_UPDATE_STRATEGY == OPTIMIZED_STRATEGY ) {
		ffEvalPtr.reset(new MolecularEvolution::Parsimony::FastFitchEvaluator(DL::MSA::NUCLEOTIDE, fileAlign, treeManager));
	}

}

double Base::processLikelihood(const Sampler::Sample &sample) {

	// Update the values
	std::set<DAG::BaseNode*> updatedNodes;
	SampleWrapper sw(getNBranch(), nuclModel, sample.getIntValues(), sample.getDblValues());
	setSample(sw, updatedNodes);

	/*if(true) {
		manageCustomMarkers(sw);
		curTree->clearUpdatedNodes();
		return 1.0;
	}*/

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();

	// Process the tree
	double sLik = MPI_Wtime(); // DEBUG
	scheduler->process();
	double eLik = MPI_Wtime(); // DEBUG

	manageCustomMarkers(sw);

	double lik = rootDAG->getLikelihood();
	if(doDebug == PARSIMONY_DEBUG || TREE_UPDATE_STRATEGY == GUIDED_STRATEGY || TREE_UPDATE_STRATEGY == MIXED_STRATEGY || TREE_UPDATE_STRATEGY == OPTIMIZED_STRATEGY ) {
		ffEvalPtr->evaluate(curTree); // Init
	}

	if(doDebug == PARSIMONY_DEBUG) {
		double sPars = MPI_Wtime(); // DEBUG
		double parsimony = ffEvalPtr->evaluate(curTree); // DEBUG
		double ePars = MPI_Wtime(); // DEBUG
		dbgFile << std::scientific << lik << ", " << parsimony << ", " << (eLik-sLik) << ", " << (ePars-sPars) << std::endl;
	}

	curTree->clearUpdatedNodes();

	// return the likelihood
	return lik;
}

std::string Base::getName(char sep) const {
	return std::string("TIGamma");
}

size_t Base::getNBranch() const {
	return branchPtr.size();
}

nuclModel_t Base::getNucleotideModel() const {
	return nuclModel;
}


size_t Base::getNGamma() const {
	return N_GAMMA;
}

size_t Base::stateSize() const {
	size_t size = 0;
	return size;
}

void Base::setStateLH(const char* aState) {
	// TODO DO NOT FORGET TO UPDATE THE UPDATEDNODES LIST FOR PARTIAL SCHEDULER UPDATE
}

void Base::getStateLH(char* aState) const {

}

double Base::update(const vector<size_t>& pIndices, const Sample& sample) {

	volatile double sSample, eSample, sDAG, eDAG, sProc, eProc;

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		dbgFile << "Indices : ";
		for(size_t i=0; i<pIndices.size(); ++i) {
			dbgFile << pIndices[i] << ", ";
		}
		dbgFile << std::endl;
		sSample = MPI_Wtime();
	}

	std::set<DAG::BaseNode*> updatedNodes;
	SampleWrapper sw(getNBranch(), nuclModel, sample.getIntValues(), sample.getDblValues());

	setSample(sw, updatedNodes);

	/*if(true) {
		manageCustomMarkers(sw);
		curTree->clearUpdatedNodes();
		return 1.0;
	}*/

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		eSample = MPI_Wtime();
		sDAG = MPI_Wtime();
	}

	// Reset the DAG
	if(!first) { // Partial
		if(!scheduler->resetPartial(updatedNodes) && (!pIndices.empty()) && (pIndices.front() != 0)) { // its possible that the tree is the same
			if(cntErrMsg < 1000) {
				cntErrMsg++;
				if(cntErrMsg == 1000) {
					std::cerr << "[" << Parallel::mcmcMgr().getMyChainMC3() << "][" << Parallel::mcmcMgr().getRankProposal();
					std::cerr <<  "] First pInd = " << pIndices.front();
					std::cerr << " || error with partial update in block having pInd=" << pIndices.front() << std::endl;
				}
			}
		}
	} else { // Full the first time
		scheduler->resetDAG();
		first = false;
	}

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		eDAG = MPI_Wtime();
		sProc = MPI_Wtime();
	}

	/*std::cout << "Before lik" << std::endl;

	if(treeManager->getProposedMove()) {
		std::cout << treeManager->getProposedMove()->getMoveType() << std::endl;
	}*/

	// Process the tree
	double sLik = MPI_Wtime(); // DEBUG
	scheduler->process();
	double eLik = MPI_Wtime(); // DEBUG


	//std::cout << "After lik" << std::endl;


	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		eProc = MPI_Wtime();
	}

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		dbgFile << "Update::setSample : " << eSample -sSample << std::endl;
		dbgFile << "Update::resetPartial : " << eDAG - sDAG << std::endl;
		dbgFile << "Update::process : " << eProc - sProc << std::endl;
		dbgFile << "----------------------------------------------" << std::endl;
	}

	manageCustomMarkers(sw);

	double lik = rootDAG->getLikelihood();


	if(doDebug == PARSIMONY_DEBUG && !curTree->getUpdatedNodes().empty()) {
		double sPars = MPI_Wtime(); // DEBUG
		double parsimony = ffEvalPtr->update(curTree); // DEBUG
		double ePars = MPI_Wtime(); // DEBUG
		dbgFile <<  std::scientific << lik << ", " << parsimony << ", " << (eLik-sLik) << ", " << (ePars-sPars) << std::endl;
	}

	if(doDebug == PARSIMONY_DEBUG || TREE_UPDATE_STRATEGY == GUIDED_STRATEGY || TREE_UPDATE_STRATEGY == MIXED_STRATEGY || TREE_UPDATE_STRATEGY == OPTIMIZED_STRATEGY ) {
		ffEvalPtr->signalUpdatedTreeNodes(curTree->getUpdatedNodes());
	}

	curTree->clearUpdatedNodes();

	//std::cout << lik << std::endl;
	//if(lik == std::numeric_limits<double>::infinity()) getchar();

	// return the likelihood
	return lik;
}


double Base::processLikelihoodSpecificPrior(const Sampler::Sample &sample) {


	SampleWrapper sw(getNBranch(), nuclModel, sample.getIntValues(), sample.getDblValues());

	size_t offset = 0;
	double prior = 0.;
	if(nuclModel == MolecularEvolution::MatrixUtils::GTR || nuclModel == MolecularEvolution::MatrixUtils::HKY85) {
		double stFreqSum = 1.;
		for(size_t iF=0; iF<3; ++iF) stFreqSum += sw.stFreq[iF];

		std::vector<double> nrmStFreq(sw.stFreq);
		nrmStFreq.push_back(1.);

		for(size_t iT=0; iT<nrmStFreq.size(); ++iT) {
			nrmStFreq[iT] /= stFreqSum;
			//markers[offset+iT] = nrmStFreq[iT];
		}
		offset+=4;

		double priorNuclFreq = log(ptrGTRFreqPr->computePDF(nrmStFreq));
		prior += priorNuclFreq;

		markers[offset] = priorNuclFreq;
		offset+=1;
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {

		std::vector<double> nrmThetas(sw.thetas);
		if(sw.thetas.size() == 5) nrmThetas.push_back(1.); // f=1.

		double sum = 0.;
		for(size_t iT=0; iT<nrmThetas.size(); ++iT) {
			sum += nrmThetas[iT];
		}
		for(size_t iT=0; iT<nrmThetas.size(); ++iT) {
			nrmThetas[iT] /= sum;
			//markers[offset+iT] = nrmThetas[iT];
		}
		offset+=nrmThetas.size();

		double priorGTR = log(ptrGTRPr->computePDF(nrmThetas));

		markers[offset] = priorGTR;
		offset+=1;

		prior += priorGTR;
	}

	prior += LOG_UNIFORM_PRIOR_TREE;

	return prior;
}

void Base::saveInternalState(const Sampler::Sample &sample, Utils::Serialize::buffer_t &buff) {
	// Serialize the tree to the other chain (get the good tree)
	TR::Tree::sharedPtr_t aTree = treeManager->getCurrentTree();
	if(sample.getIntParameter(0) != aTree->getHashKey()) {
		aTree = treeManager->getTree(sample.getIntParameter(0));
	}
	// Serialize it
	std::string aString = aTree->getIdString();
	std::copy(aString.begin(), aString.end(), std::back_inserter(buff));
}

void Base::loadInternalState(const Sampler::Sample &sample, const Utils::Serialize::buffer_t &buff) {

	// Get treeString
	std::string aString;
	std::copy(buff.begin(), buff.end(), std::back_inserter(aString));

	// Create tree
	TR::Tree::sharedPtr_t aTree(new TR::Tree(aString));

	// Add it to the pool
	treeManager->setTree(aTree);
	treeManager->memorizeTree();
	isTreeExternalyImported = true;

	if(doDebug == PARSIMONY_DEBUG || TREE_UPDATE_STRATEGY == GUIDED_STRATEGY || TREE_UPDATE_STRATEGY == MIXED_STRATEGY || TREE_UPDATE_STRATEGY == OPTIMIZED_STRATEGY ) {
		ffEvalPtr->signalTreeImport();
	}

}

void Base::doCustomOperations(size_t iteration, const Sampler::Sample &sample) {

	if(iteration > 0 && iteration % 50 == 0) {
		TR::Tree::sharedPtr_t curTree(treeManager->getCurrentTree());
		bipartitionMonitor->registerTreeBipartitions(curTree, sample, false);

	}

}


void Base::initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, const std::vector<std::streamoff>& offsets) {
	if(offsets.empty()) {
		treeManager->initTreeFile(fnPrefix, fnSuffix, 0);
	} else {
		assert(offsets.size() == 1);
		treeManager->initTreeFile(fnPrefix, fnSuffix, offsets.front());
	}
}

std::vector<std::streamoff> Base::getCustomizedLogSize() const {
	return std::vector<std::streamoff>(1, treeManager->getTreeFileLength());

}

void Base::writeCustomizedLog(const Sampler::SampleUtils::vecPairItSample_t &iterSamples) {
	// Keep track of trees hash
	std::vector<long int> treesHash;
	for(size_t iS=0; iS<iterSamples.size(); ++iS) {
		treesHash.push_back(iterSamples[iS].second.getIntParameter(0));
	}

	treeManager->writeTreeString(treesHash);
}

std::vector<DAG::BaseNode*> Base::defineDAGNodeToCompute(const Sample& sample) {
	std::vector<DAG::BaseNode*> mustBeProcessed;

	std::set<DAG::BaseNode*> updatedNodes;
	SampleWrapper sw(getNBranch(), nuclModel, sample.getIntValues(), sample.getDblValues());
	setSample(sw, updatedNodes);

	// Find the nodes to process
	scheduler->findNodeToProcess(rootDAG, mustBeProcessed);
	// Reinit the dag
	scheduler->resetPartial();
	// Fake the computations
	bool doFakeCompute = true;
	scheduler->process(doFakeCompute);

	return mustBeProcessed;
}

size_t Base::getNThread() const {
	return nThread;
}

std::string Base::getAlignFile() const {
	return fileAlign;
}

std::string Base::getCompressionReport(DL::MSA &msa, DL::CompressedAlignements &compressedAligns) const {
	std::stringstream ss;
	ss << "Total number of site : " << msa.getNSite() << std::endl;
	ss << "Number of unique site : " << compressedAligns.getNSite() << std::endl;

	return ss.str();
}

const std::vector<double>& Base::getInitialBranchLenghts() const {
	return initialBranchLenghts;
}

Base::treeUpdateStrategy_t Base::getTreeUpdateStrategy() const {
	return TREE_UPDATE_STRATEGY;
}

void Base::printDAG() const {
	std::cout << "**************************************************************************" << std::endl;
	std::cout << rootDAG->subtreeToString() << std::endl;
	std::cout << "**************************************************************************" << std::endl;
}

double Base::getEpsilonFreq() const {
	return epsilonFreq;
}

double Base::getPowerFreq() const {
	return powerFreq;
}


DAG::Scheduler::BaseScheduler* Base::getScheduler() const {
	return scheduler;
}

void Base::prepareDAG() {

	using MolecularEvolution::MatrixUtils::MatrixFactory;
	using MolecularEvolution::MatrixUtils::GTR_MF;
	using MolecularEvolution::MatrixUtils::HKY85_MF;


	MatrixFactory::sharedPtr_t ptr;
	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		ptr.reset(new GTR_MF());
	} else {
		ptr.reset(new HKY85_MF());
	}

	ptrMatrix = new MatrixGammaNode(frequencies, ptr);
	ptrDiscreteGamma = new DiscreteGammaNode(N_GAMMA, DiscreteGammaNode::MEAN_GAMMA);


	// Root of the DAG
	rootDAG = new FinalResultNode();
}

void Base::createTreeAndDAG(DL::FastaReader &fr, DL::MSA &msa, DL::CompressedAlignements &compressedAligns) {

	// Reset accumulators
	accComp = accComp_t();
	accTotal = accComp_t();

	// Root
	LikMappingTI::sharedPtr_t likMapping(new LikMappingTI(N_GAMMA, treeManager->getTreeNodeName(curTree->getRoot())));
	rootTree = new TRMAP::TreeNode(curTree->getRoot(), likMapping);
	branchMap.insert(std::make_pair(rootTree->getId(), rootTree));
	createTree(curTree->getRoot(), NULL,  rootTree);
	std::sort(branchPtr.begin(), branchPtr.end(), MolecularEvolution::TreeReconstruction::LikelihoodMapper::SortTreeNodePtrById());

	// Decompose the DAG in function of the NB of thread.
	size_t nSubDAG = 1;
	if(isUpdatableLH && nThread > 1){
		size_t tmpVal = ceil((double)nThread/(2.0*N_GAMMA));
		// TODO OPTIMIZE THAT nProc = 2 with nSubDag = 2 -> nThread = 4 nSubDag = ? etc..
		nSubDAG = std::min(tmpVal, nSite);
	}

	//std::cout << "Sequences divided in " << nSubDAG << " for parallel computations with " << nThread << "." << std::endl;

	size_t sizeSubDag = ceil(static_cast<double>(nSite) / static_cast<double>(nSubDAG));
	for(size_t iSD=0; iSD<nSubDAG; ++iSD) {
		// Init subSites
		size_t start = iSD*sizeSubDag;
		size_t end = std::min((iSD+1)*sizeSubDag, nSite);

		std::vector<size_t> subSites;
		for(size_t iSS=start; iSS<end; ++iSS) {
			subSites.push_back(iSS);
		}

		// Create site combiner
		CombineSiteGammaNode* ptrCSN = new CombineSiteGammaNode(N_GAMMA, subSites, compressedAligns);
		cmbNodes.push_back(ptrCSN);

		// Create root(s) and subDAG for class iC
		for(size_t iG=0; iG<N_GAMMA; ++iG) {
			RootGammaNode *ptrRoot = new RootGammaNode(iG, subSites, frequencies);
			createSubDAG(iG, subSites, rootTree, ptrRoot, compressedAligns); // Create sub DAG for class iC
			likMapping->addEdgeGammaNode(ptrRoot);

			// Add root to the CombineSiteNode
			ptrCSN->addChild(ptrRoot);

		}

		// Add this CombineSiteNode to the DAG root
		rootDAG->addChild(ptrCSN);
	}
}


void Base::addBranchMatrixNodes(LikMappingTI::sharedPtr_t likMapping) {

	// Add BranchMatrixNodes for each classes
	for(size_t iG=0; iG<N_GAMMA; ++iG) {
		BranchMatrixGammaNode *ptrBMGTRGN;
		ptrBMGTRGN = new BranchMatrixGammaNode(iG, frequencies);
		ptrBMGTRGN->addChild(ptrDiscreteGamma);
		ptrBMGTRGN->addChild(ptrMatrix);
		likMapping->addBranchMatrixGammaNode(ptrBMGTRGN);
	}
}


/**
 * @param nodeTR a node in the TreeReconstruction tree
 * @param node a node in the TIGamma tree
 */
void Base::createTree(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TRMAP::TreeNode *node) {

	TR::vecTN_t childrenTR = nodeTR->getChildren(parentNodeTR);

	if(childrenTR.size() > 0) { // If node has children, process
		// Set boolean info.
		node->setLeaf(false);

		// For all children in newick, create in PosSel
		for(size_t iC=0; iC<childrenTR.size(); ++iC){
			LikMappingTI::sharedPtr_t likMapping(new LikMappingTI(N_GAMMA, treeManager->getTreeNodeName(childrenTR[iC])));
			TRMAP::TreeNode *childNode = new TRMAP::TreeNode(childrenTR[iC], likMapping);
			branchPtr.push_back(childNode); // Keep pointer to branch for t update
			branchMap.insert(std::make_pair(childNode->getId(), childNode));

			createTree(childrenTR[iC], nodeTR, childNode);
			node->addChild(childNode);
			addBranchMatrixNodes(likMapping); // Add BranchMatrixNode(s)
		}
	} else { // Else it is a leaf node
		node->setLeaf(true);
	}
}

/**
 * This method create one sub-DAG for a set of sites for a given class.
 * For each children node it is doing the following steps :
 * 1) Create the right child DAG node (LeafNode or CPVNode)
 * 2) Register the newly created DAG node(s) to its parent
 * 3) Add the correct DAG dependencies to the child node (BranchMatrixNode)
 *
 * @param idClass the matrix Q class
 * @param subSites the vector of sites.
 * @param node the "parent" in the internal positive sel. tree.
 * @param nodeDAG the "parent" node in the DAG (there can be multiple for foreground branch)
 * @param compressedAligns compressed alignements
 */
void Base::createSubDAG(size_t idGamma, const std::vector<size_t> &subSites, TRMAP::TreeNode *node, DAG::BaseNode *nodeDAG, DL::CompressedAlignements &compressedAligns) {

	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		DAG::BaseNode *newNodeDAG;
		TRMAP::TreeNode *childNode = node->getChild(i);
		LikMappingTI* likMapping = static_cast<LikMappingTI*>(childNode->getLikelihoodMapping().get());

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			LeafGammaNode *newNode;
			newNode = new LeafGammaNode(idGamma, subSites, likMapping->getName(), compressedAligns, frequencies);
			likMapping->addEdgeGammaNode(newNode);
			newNodeDAG = newNode;
		} else { // create CPV element
			CPVGammaNode *newNode;
			newNode = new CPVGammaNode(idGamma, subSites, frequencies);
			likMapping->addEdgeGammaNode(newNode);
			newNodeDAG = newNode;

			// create sub-DAG
			createSubDAG(idGamma, subSites, childNode, newNode, compressedAligns);
		}

		// (2) We add the child node created to the current node (register to father).
		nodeDAG->addChild(newNodeDAG);

		// (3) We add dependencies to the newly created node (BranchMatrixNode)
		BranchMatrixGammaNode *ptrBMN = likMapping->getBranchMatrixGammaNode(idGamma);
		newNodeDAG->addChild(ptrBMN);

	}

}

void Base::setSample(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {

	//std::cout << sw.toString() << std::endl;

	volatile double sTime, eTime;
	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	setTree(sw, updatedNodes);
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setTree : " << eTime-sTime << std::endl;
	}

	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	if(nuclModel == MU::GTR) {
		setThetas(sw, updatedNodes);
	} else if(nuclModel == MU::HKY85) {
		setKappa(sw, updatedNodes);
	} else {
		std::cerr << "Error : void Base::setSample(const SampleWrapper &sw);" << std::endl;
		std::cerr << "Nucleotide model not supported by Light::Base" << std::endl;
		abort();
	}
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setMatrix : " << eTime-sTime << std::endl;
	}

	setAlpha(sw, updatedNodes);

	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	setBranchLength(sw, updatedNodes);
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setBranchLength : " << eTime-sTime << std::endl;
	}

}

void Base::setTree(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {

	volatile double sMPI, eMPI;

	if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sMPI = MPI_Wtime();
	//treePool->updatePool();
	if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) {
		eMPI = MPI_Wtime();
		dbgFile << "SetTree::UpdatePools : " << eMPI - sMPI << std::endl;
	}

	// FIXME std::cout << curTree->getHashKey() << " -> " << sw.treeH << std::endl;

	if(curHash != sw.treeH || curTree->getHashKey() != sw.treeH) {
		//std::cout << "New tree" << std::endl; // DEBUG
		if(TREE_UPDATE_STRATEGY != FIXED_TREE_STRATEGY) {
			volatile double sTree, eTree, sUpd, eUpd, sReinit, eReinit;

			// Find tree
			// FIXME std::cout << "Current tree : " << curTree->getString() << std::endl;
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sTree = MPI_Wtime();
			//std::cout << "Base tree : " << curTree->getIdString() << std::endl; // FIXME
			//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] curTree -> " << curTree->getHashKey() << "\t ";
			//std::cout << " sample -> " << sw.treeH << "\t curHash -> " << curHash << std::endl; // FIXME
			curTree = treeManager->getTree(sw.treeH);
			//std::cout << "New tree : " << curTree->getIdString() << std::endl; // FIXME
			if(curTree == NULL) {
				std::cerr << "[Error] " << sw.treeH << std::endl;
			}
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) eTree = MPI_Wtime();
			// FIXME std::cout << "Next tree : " << curTree->getString() << std::endl;

			// Update tree + DAG
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sUpd = MPI_Wtime();
			if(!isTreeExternalyImported) {
				treeMapper.partialUpdateTreeAndDAG(curTree, updatedNodes);
			} else {
				treeMapper.fullUpdateTreeAndDAG(curTree, rootTree, updatedNodes);
				isTreeExternalyImported = false;
			}
			curHash = curTree->getHashKey();
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) eUpd = MPI_Wtime();

			// Update scheduler
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sReinit = MPI_Wtime();
			scheduler->reinitialize(scheduler->SOFT_REINIT); // only a soft reset
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) eReinit = MPI_Wtime();

			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) {
				dbgFile << "SetTree::getTree : " << eTree - sTree << std::endl;
				dbgFile << "SetTree::UpdateDaG : " << eUpd - sUpd << std::endl;
				dbgFile << "SetTree::ReinitSched : " << eReinit - sReinit << std::endl;
				if(doDebug == TREE_DEBUG) {
					dbgFile << "----------------------------------------------" << std::endl;
				}
			}

		} else {
			assert(false && "[Error] Likelihood strategy is 'FIXED_TREE_STRATEGY' and topological moves are happening.");
		}
	}
}

void Base::setAlpha(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	bool isUpdated = ptrDiscreteGamma->setAlpha(sw.alpha);
	if(isUpdated) {
		updatedNodes.insert(ptrDiscreteGamma);
		//std::cout << "Alpha updated" << std::endl;
	}
}

void Base::setKappa(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	bool isUpdated = ptrMatrix->setKappa(sw.K);

	bool isUpdatedFreq = ptrMatrix->setFrequencies(sw.stFreq);
	if(isUpdated) {
		std::vector<double> stationaryFreq(sw.stFreq);
		stationaryFreq.push_back(1.);
		double tmpSum = 0.;
		for(size_t iF=0; iF<stationaryFreq.size(); ++iF) tmpSum += stationaryFreq[iF];
		for(size_t iF=0; iF<stationaryFreq.size(); ++iF) stationaryFreq[iF] /= tmpSum;

		frequencies.set(stationaryFreq);
	}

	if(isUpdated || isUpdatedFreq) {
		updatedNodes.insert(ptrMatrix);
		//std::cout << "Matrix updated" << std::endl;
	}

}

void Base::setThetas(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {

	bool isUpdated = ptrMatrix->setThetas(sw.thetas);

	bool isUpdatedFreq = ptrMatrix->setFrequencies(sw.stFreq);
	if(isUpdatedFreq) {
		std::vector<double> stationaryFreq(sw.stFreq);
		stationaryFreq.push_back(1.);
		double tmpSum = 0.;
		for(size_t iF=0; iF<stationaryFreq.size(); ++iF) tmpSum += stationaryFreq[iF];
		for(size_t iF=0; iF<stationaryFreq.size(); ++iF) stationaryFreq[iF] /= tmpSum;

		frequencies.set(stationaryFreq);
	}
	if(isUpdated || isUpdatedFreq) {
		updatedNodes.insert(ptrMatrix);
		//std::cout << "Matrix updated" << std::endl;
	}
}


void Base::setBranchLength(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	for(size_t i=0; i<sw.N_BL; ++i) {
		//double tmp = exp(sw.branchLength[i]); // FIXME
		//static_cast<LikMappingTI*>(branchPtr[i]->getLikelihoodMapping().get())->setBranchLength(tmp, updatedNodes);
		static_cast<LikMappingTI*>(branchPtr[i]->getLikelihoodMapping().get())->setBranchLength(sw.branchLength[i], updatedNodes);
	}
}

void Base::cleanTreeAndDAG() {
	rootTree->deleteChildren();
	delete rootTree;
	rootDAG->deleteChildren();
	delete rootDAG;

	branchPtr.clear();
	branchMap.clear();
}

double Base::getTotalTreeLength(const SampleWrapper &sw){
	double sum = 0.;
	for(size_t i=0; i<sw.N_BL; ++i) {
		//sum += exp(sw.branchLength[i]); // FIXME
		sum += sw.branchLength[i]; // FIXME
	}
	return sum;
}

void Base::manageCustomMarkers(const SampleWrapper &sw) {

	size_t offset = 0;

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR || nuclModel == MolecularEvolution::MatrixUtils::HKY85) {
		double stFreqSum = 1.;
		for(size_t iF=0; iF<3; ++iF) stFreqSum += sw.stFreq[iF];

		std::vector<double> nrmStFreq(sw.stFreq);
		nrmStFreq.push_back(1.);

		for(size_t iT=0; iT<nrmStFreq.size(); ++iT) {
			nrmStFreq[iT] /= stFreqSum;
			markers[offset+iT] = nrmStFreq[iT];
		}
		offset+=4;

		offset+=1; // prior
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {

		std::vector<double> nrmThetas(sw.thetas);
		if(sw.thetas.size() == 5) nrmThetas.push_back(1.); // f=1.

		double sum = 0.;
		for(size_t iT=0; iT<nrmThetas.size(); ++iT) {
			sum += nrmThetas[iT];
		}
		for(size_t iT=0; iT<nrmThetas.size(); ++iT) {
			nrmThetas[iT] /= sum;
			markers[offset+iT] = nrmThetas[iT];
		}
		offset+=nrmThetas.size();

		offset+=1; // Prior

	}

	markers.back() = getTotalTreeLength(sw);

}



} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
