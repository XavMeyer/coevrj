/*
 * LikMappingTI.h
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#ifndef MODEL_LIKELIHOOD_TIGAMMA_LIKMAPPINGTI_H_
#define MODEL_LIKELIHOOD_TIGAMMA_LIKMAPPINGTI_H_

#include <boost/shared_ptr.hpp>

#include "Nodes/IncNodes.h"
#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/LikelihoodMapping.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

namespace TR = ::MolecularEvolution::TreeReconstruction;

class LikMappingTI: public TR::LikelihoodMapper::LikelihoodMapping {
public:
	typedef boost::shared_ptr<LikMappingTI> sharedPtr_t;

public:
	LikMappingTI(const size_t aNGamma, const std::string aName);
	~LikMappingTI();

	size_t getNumberEdgeGammaNode() const;
	void addEdgeGammaNode(EdgeGammaNode *node);
	EdgeGammaNode* getEdgeGammaNode(size_t aIdGamma);
	std::vector<EdgeGammaNode *>& getEdgeGammaNodes();

	void addBranchMatrixGammaNode(BranchMatrixGammaNode *node);
	BranchMatrixGammaNode* getBranchMatrixGammaNode(size_t aIdGamma);
	std::vector<BranchMatrixGammaNode *>& getBranchMatrixGammaNodes();

	void setBranchLength(const double aBL, std::set<DAG::BaseNode*> &updatedNodes);
	double getBranchLength() const;

	const std::string& getName() const;

	void doAddChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping);
	void doRemoveChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping);

	void doOnApplyChange();
	void doOnUpdate(std::set<DAG::BaseNode*> &updatedNodes);

	std::string getNewickSubstring() const;
	std::string toString() const;

private:

	typedef std::vector<EdgeGammaNode *> treeStructDAG_t;
	typedef std::vector<BranchMatrixGammaNode *> branchMDAG_t;

	/* Default data */
	const size_t N_GAMMA;
	std::string name;
	double branchLength;

	/* Linkage with DAG elements */
	treeStructDAG_t treeStructDAG;
	branchMDAG_t branchMDAG;
};

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MODEL_LIKELIHOOD_TIGAMMA_LIKMAPPINGTI_H_ */
