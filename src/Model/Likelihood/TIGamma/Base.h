//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Base.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 *
 */
#ifndef BASE_TIGAMMA_H_
#define BASE_TIGAMMA_H_

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <stddef.h>
#include <fstream>
#include <iostream>
#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/stats.hpp>

#include "LikMappingTI.h"
#include "Nodes/IncNodes.h"
#include "SampleWrapper.h"
#include "Model/Likelihood/TIGamma/Nodes/Types.h"
#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriority/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriorityII/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/StaticScheduler/ThreadSafeScheduler.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Utils/Statistics/Dirichlet/DirichletPDF.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/NucleotideFrequencies.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/NewickTree/NewickParser.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleOmegaMF.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/HKY85MF.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeManager.h"
#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/TreeMapper.h"
#include "Utils/MolecularEvolution/Parsimony/ParsimonyEvaluator.h"
#include "Utils/MolecularEvolution/Parsimony/FastFitchEvaluator.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Bipartition/BipartitionMonitor.h"

namespace DAG { namespace Scheduler { class BaseScheduler; } }
namespace MolecularEvolution { namespace DataLoader { class CompressedAlignements; } }
namespace MolecularEvolution { namespace DataLoader { class FastaReader; } }
namespace MolecularEvolution { namespace DataLoader { class NewickParser; } }
namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }
namespace MolecularEvolution { namespace TreeReconstruction { class TreeNode; } }
namespace Sampler { class Sample; }
namespace boost { namespace accumulators { namespace tag { struct mean; } } }


namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {


namespace DL = ::MolecularEvolution::DataLoader;
namespace MD = ::MolecularEvolution::Definition;
namespace TR = ::MolecularEvolution::TreeReconstruction;
namespace TRMAP = ::MolecularEvolution::TreeReconstruction::LikelihoodMapper;


class FinalResultNode;
class MatrixScalingNode;
class SampleWrapper;

typedef MolecularEvolution::MatrixUtils::nuclModel_t nuclModel_t;

class Base : public LikelihoodInterface {
public:
	static const std::string NAME_TREE_PARAMETER;
	enum treeUpdateStrategy_t {FIXED_TREE_STRATEGY=0, DEFAULT_STRATEGY=1, ADAPTIVE_STRATEGY=2,
													MIXED_STRATEGY=3, GUIDED_STRATEGY=4,  OPTIMIZED_STRATEGY=5};

public:
	Base(const size_t aNThread, const nuclModel_t aNuclModel, const double aNGamma,
	     DL::NewickParser *aNP, const std::string &aFileAlign, treeUpdateStrategy_t aStrat,
		 double aEpsilonFreq, double aPowerFreq);
	~Base();

	//! Process the likelihood
	double processLikelihood(const Sampler::Sample &sample);
	std::string getName(char sep) const;
	size_t getNBranch() const;
	nuclModel_t getNucleotideModel() const;
	size_t getNGamma() const;

	size_t stateSize() const;
	void setStateLH(const char* aState);
	void getStateLH(char* aState) const;
	double update(const vector<size_t>& pIndices, const Sample& sample);

	double processLikelihoodSpecificPrior(const Sampler::Sample &sample);

	void saveInternalState(const Sampler::Sample &sample, Utils::Serialize::buffer_t &buff);
	void loadInternalState(const Sampler::Sample &sample, const Utils::Serialize::buffer_t &buff);

	void doCustomOperations(size_t iteration, const Sampler::Sample &sample);

	void initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, const std::vector<std::streamoff>& offsets);
	std::vector<std::streamoff> getCustomizedLogSize() const;
	void writeCustomizedLog(const Sampler::SampleUtils::vecPairItSample_t &iterSamples);

	DAG::Scheduler::BaseScheduler* getScheduler() const;
	std::vector<DAG::BaseNode*> defineDAGNodeToCompute(const Sample& sample);

	size_t getNThread() const;
	std::string getAlignFile() const;

	std::string getCompressionReport(DL::MSA &msa, DL::CompressedAlignements &compressedAligns) const;

	const std::vector<double>& getInitialBranchLenghts() const;

	treeUpdateStrategy_t getTreeUpdateStrategy() const;
	void printDAG() const;

	TR::AdaptiveESPR::bias_t getBiasType() const;
	double getEpsilonFreq() const;
	double getPowerFreq() const;

private:

	enum debug_t {DISABLE_DEBUG=0, GLOBAL_DEBUG=1, TREE_DEBUG=2, UPDATE_DEBUG=3, PARSIMONY_DEBUG=4};
	static const debug_t doDebug;
	std::ofstream dbgFile;

	/* Input data */
	const size_t N_GAMMA;
	double LOG_UNIFORM_PRIOR_TREE;
	bool first, isTreeExternalyImported;
	size_t nThread, nSite;
	const nuclModel_t nuclModel;
	std::string fileAlign;
	double epsilonFreq, powerFreq;

	/* Internal data */
	const treeUpdateStrategy_t TREE_UPDATE_STRATEGY;
	TRMAP::TreeNode *rootTree;
	long int curHash;
	TR::LikelihoodMapper::TreeMapper treeMapper;
	std::vector<TRMAP::TreeNode*> branchPtr;
	std::vector<double> initialBranchLenghts;

	typedef std::map<size_t, TRMAP::TreeNode*> branchMap_t;
	branchMap_t branchMap;
	DL::Utils::Frequencies frequencies;
	std::vector<size_t> labelBranches;
	typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > accComp_t;
	accComp_t accComp, accTotal;

	/* DAG data */
	std::vector<CombineSiteGammaNode*> cmbNodes;
	MatrixGammaNode* ptrMatrix;
	DiscreteGammaNode *ptrDiscreteGamma;
	FinalResultNode *rootDAG;

	/* DAG Scheduler */
	size_t cntErrMsg;
	DAG::Scheduler::BaseScheduler *scheduler;

	// Prior
	Utils::Statistics::DirichletPDF::sharedPtr_t ptrDirichletPr, ptrGTRFreqPr, ptrGTRPr;

	//! Called by constructor to init everything
	void init(DL::NewickParser *aNP);

	//! Define the empiric symbol frequency
	std::vector<double> defineFrequency(DL::MSA &msa) const;

	double defineLogUniformPriorTree(TR::Tree::sharedPtr_t aTree) const;

	//! Prepare DAG elements (matrices, scaling matrix
	void prepareDAG();
	//! Build the internal tree and the DAG
	void createTreeAndDAG(DL::FastaReader &fr, DL::MSA &msa, DL::CompressedAlignements &compressedAligns);

	//! Labelize the internal branches
	void labelizeBranch(const DL::TreeNode &newickNode);

	//! Create recursively the internal tree based on the newick one
	void createTree(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TRMAP::TreeNode *node);
	//! Add the adequate BranchMatrix node to the PS node
	void addBranchMatrixNodes(LikMappingTI::sharedPtr_t likMapping);

	//! Create recursively a sub-DAG (based on the internal Tree)
	void createSubDAG(size_t idClass, const std::vector<size_t> &subSites, TRMAP::TreeNode *node,
			DAG::BaseNode *nodeDAG, DL::CompressedAlignements &compressedAligns);

	// Create Nodes
	// virtual void createMatrixNode();

	void cleanTreeAndDAG();

	// Setters for the samples
	void setSample(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setTree(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setAlpha(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setKappa(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setThetas(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setBranchLength(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);

	double getTotalTreeLength(const SampleWrapper &sw);

	void manageCustomMarkers(const SampleWrapper &sw);

};

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BASE_TIGAMMA_H_ */
