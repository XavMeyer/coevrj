//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Types.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef TYPES_TIG_H_
#define TYPES_TIG_H_

#include <stddef.h>
#include <assert.h>
#include <bitset>

#include "Eigen/Core"
#include "Eigen/Dense"

#include "Utils/MolecularEvolution/Definitions/MolecularDefIncl.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

#define TI_NUCL_NSYMBOLS 			4
#define TI_GAMMA_USE_FLOAT 			0
#if TI_GAMMA_USE_FLOAT

typedef Eigen::Matrix<float, TI_NUCL_NSYMBOLS, TI_NUCL_NSYMBOLS, Eigen::ColMajor> TI_Gamma_EigenSquareMatrix_t;
typedef Eigen::Matrix<float, TI_NUCL_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_Gamma_EigenMatrix_t;

typedef Eigen::MatrixXf TI_EigenMatrixDyn_t;
typedef Eigen::VectorXf TI_EigenVector_t;
typedef Eigen::ArrayXf TI_EigenArray_t;
#define TI_GAMMA_TYPE				float
#else

typedef Eigen::Matrix<double, TI_NUCL_NSYMBOLS, TI_NUCL_NSYMBOLS, Eigen::ColMajor> TI_Gamma_EigenSquareMatrix_t;
typedef Eigen::Matrix<double, TI_NUCL_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_Gamma_EigenMatrix_t;

typedef Eigen::MatrixXd TI_EigenMatrixDyn_t;
typedef Eigen::VectorXd TI_EigenVector_t;
typedef Eigen::ArrayXd TI_EigenArray_t;
#define TI_GAMMA_TYPE				double
#endif

typedef Eigen::VectorXd TI_EigenEIGVector_t;
typedef Eigen::ArrayXd TI_EigenEIGArray_t;
typedef Eigen::Matrix<double, TI_NUCL_NSYMBOLS, TI_NUCL_NSYMBOLS, Eigen::ColMajor> TI_Gamma_EigenEIGMatrix_t;

static const TI_GAMMA_TYPE LOG_OF_2 = log(2.);

typedef ::MolecularEvolution::Definition::bitset16b_t usintBitset_t;
typedef std::pair<bool, usintBitset_t> nuclCodeQuery_t;

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TYPES_TIG_H_ */
