//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixGammaNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef MATRIXGTRNODE_TIG_H_
#define MATRIXGTRNODE_TIG_H_

#include <stddef.h>

#include "Types.h"
#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;
namespace MU = ::MolecularEvolution::MatrixUtils;

class MatrixGammaNode: public DAG::BaseNode {

public:
	MatrixGammaNode(DL_Utils::Frequencies &aFrequencies, MU::MatrixFactory::sharedPtr_t aMF);
	~MatrixGammaNode();

	bool setKappa(const double aK);
	bool setThetas(const std::vector<double> &aThetas);
	bool setFrequencies(const std::vector<double> &aFreq);

	const double getMatrixQScaling() const;
	const TI_Gamma_EigenEIGMatrix_t& getScaledEigenVectors() const;
	const TI_EigenEIGVector_t& getEigenValues() const;

	size_t serializedSize() const;
	void serializeToBuffer(char *buffer) const;
	void serializeFromBuffer(const char *buffer);

	std::string toString() const;

private:

	// Coefficients are [kappa, omega] or [thetas, omega]
	std::vector<double> coefficients, stFrequencies;
	DL_Utils::Frequencies &frequencies;

	double matrixScaling;
	MU::MatrixFactory::sharedPtr_t ptrMF;
	TI_EigenEIGVector_t D;
	TI_Gamma_EigenEIGMatrix_t A, Q, scaledV;

	void doProcessing();
	bool processSignal(BaseNode* aChild);
	void comprEIG();

};

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MATRIXGTRNODE_TIG_H_ */
