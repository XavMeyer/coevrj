//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootGammaNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "RootGammaNode.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

RootGammaNode::RootGammaNode(const size_t aIDGamma, const std::vector<size_t> &aSitePos,
				   DL_Utils::Frequencies &aFrequencies) :
	 			 ParentGammaNode(aIDGamma, aSitePos, aFrequencies) {


	nrm.resize(H.cols());
	H.resize(4,0);
}

RootGammaNode::~RootGammaNode() {
}

const TI_EigenVector_t& RootGammaNode::getLikelihood() const {
	return likelihood;
}

void RootGammaNode::doProcessing() {

	TI_Gamma_EigenMatrix_t G;
	nrm.setZero();

	// Prepare G
	// Check block at first
	for(size_t iE=0; iE<children.size(); ++iE) {
		if(iE == 0) {
			G = children[iE]->getH();
			if(SCALING_TYPE == LOG  && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(children[iE]->getH());
			if (SCALING_TYPE == LOG && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		}
	}


#if TI_GAMMA_USE_FLOAT
	likelihood = frequencies.getEigenFlt().transpose() * G;
#else
	likelihood = frequencies.getEigen().transpose() * G;
#endif

}

bool RootGammaNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void RootGammaNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVGammaNode)){
		CPVGammaNode* cpvNode = dynamic_cast<CPVGammaNode*>(aChild);
		addChild(cpvNode);
	} else if(childtype == typeid(LeafGammaNode)){
		LeafGammaNode* leafNode = dynamic_cast<LeafGammaNode*>(aChild);
		addChild(leafNode);
	}
}

void RootGammaNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVGammaNode)){
		CPVGammaNode* cpvNode = dynamic_cast<CPVGammaNode*>(aChild);
		removeChild(cpvNode);
	} else if(childtype == typeid(LeafGammaNode)){
		LeafGammaNode* leafNode = dynamic_cast<LeafGammaNode*>(aChild);
		removeChild(leafNode);
	}
}

std::string RootGammaNode::toString() const {
	std::stringstream ss;
	ss << "[RootGammaNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
