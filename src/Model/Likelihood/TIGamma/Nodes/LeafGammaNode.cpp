//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafGammaNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "LeafGammaNode.h"

#include "Types.h"
#include "Types.h"
#include "BranchMatrixGammaNode.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { class MSA; } }

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

LeafGammaNode::LeafGammaNode(const size_t aIDGamma, const std::vector<size_t> &aSitePos,
				   	   	   const std::string &aSpecieName, const DL::CompressedAlignements& ca,
				   	   	   DL_Utils::Frequencies &aFrequencies) :
				   	   			   EdgeGammaNode(aIDGamma, aSitePos, aFrequencies), sitePos(aSitePos) {

	specieName = aSpecieName;
	init(aSpecieName, ca, aSitePos);
}

LeafGammaNode::~LeafGammaNode() {
}

/*nuclCodeQuery_t LeafGammaNode::getSiteNucleotides(size_t iSite) {

	for(size_t iS=0; iS<sitePos.size(); ++iS) {
		if(sitePos[iS] == iSite) {
			return std::make_pair(true, usintBitset_t(siteCode[iS]));
		}
	}
	return std::make_pair(false, usintBitset_t(0));
}*/

void LeafGammaNode::init(const std::string &aSpecieName, const DL::CompressedAlignements& ca,
					    const std::vector<size_t> &aSitePos) {

	G.resize(frequencies.size(), 4*4);
	G.setZero();
	for(size_t iC=0; iC<(size_t)G.cols(); ++iC) {
		for(size_t iB=0; iB<4; ++iB) {
			G(iB, iC) = (iC >> iB) & 1;
		}
	}
	for(size_t iS=0; iS<aSitePos.size(); ++iS) {
		DL::Alignment::vecCode_t cdnVals = ca.getSiteCode(aSpecieName, aSitePos[iS]);

		unsigned short int code = 0;
		for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
			code = code | 1 << cdnVals[iCod];
		}
		siteCode.push_back(code);
	}
}

void LeafGammaNode::doProcessing() {

	const TI_Gamma_EigenSquareMatrix_t &Z = bmNode->getZ();
#if TI_GAMMA_USE_FLOAT
	const TI_EigenVector_t &invPi = frequencies.getInvEigenFlt();
#else
	const TI_EigenVector_t &invPi = frequencies.getInvEigen();
#endif


	TI_Gamma_EigenMatrix_t tmp = invPi.asDiagonal() * (Z.selfadjointView<Eigen::Lower>() * G);
	for(size_t iS=0; iS<siteCode.size(); ++iS){
		H.col(iS) = tmp.col(siteCode[iS]);
	}

	/*std::cout << "Leaf : " << specieName << "\t t=" << bmNode->getBranchLength() << "\t NodeId=" << getId() << std::endl;
	std::cout << "Size code : " << siteCode[0] << std::endl;
	std::cout << "G:" << std::endl << G.col(siteCode[0]) << std::endl;
	std::cout << "P:" << std::endl <<  (invPi.asDiagonal() * Z) << std::endl;
	std::cout << "H:" << std::endl <<  H.col(0) << std::endl;
	std::cout << "-----------------------------------------------" << std::endl;
	if(H.col(0).sum() < 4) {
		getchar();
	}*/

	//FIXME std::cout << "Leaf[" << getId() << "] bl = " << bmNode->getBranchLength() << std::endl << H << std::endl;
}

bool LeafGammaNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void LeafGammaNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixGammaNode)){
		bmNode = dynamic_cast<BranchMatrixGammaNode*>(aChild);
		if(bmNode->getIDGamma() != idGamma){
			std::cerr << "void CPVNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild);" << std::endl;
			std::cerr << "Error : Invalid BranchMatrixNode children. Found idGamma = " << bmNode->getIDGamma();
			std::cerr << " while expecting idGamma = " <<  idGamma << std::endl;
			abort();
		}
	}
}

std::string LeafGammaNode::toString() const {
	std::stringstream ss;
	ss << "[LeafGammaNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
