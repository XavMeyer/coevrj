//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafGammaNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef LEAFNODE_TIG_H_
#define LEAFNODE_TIG_H_

#include <stddef.h>
#include <string>
#include <vector>

#include "BranchMatrixGammaNode.h"
#include "EdgeGammaNode.h"
#include "Types.h"
#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"

#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { class CompressedAlignements; } }

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

namespace DL = ::MolecularEvolution::DataLoader;
namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class LeafGammaNode : public EdgeGammaNode {
public:

	LeafGammaNode(const size_t aIDGamma, const std::vector<size_t> &aSitePos,
			const std::string &aSpecieName, const DL::CompressedAlignements& ca,
			DL_Utils::Frequencies &aFrequencies);
	~LeafGammaNode();

	//nuclCodeQuery_t getSiteNucleotides(size_t iSite);

	std::string toString() const;

private:
	std::string specieName;
	std::vector<size_t> sitePos;
	std::vector<short unsigned int> siteCode;
	TI_Gamma_EigenMatrix_t G;

	void init(const std::string &aSpecieName, const DL::CompressedAlignements& ca,
			const std::vector<size_t> &aSitePos);

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* LEAFNODE_TIG_H_ */
