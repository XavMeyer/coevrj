//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FinalResultNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "FinalResultNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

FinalResultNode::FinalResultNode() : DAG::BaseNode(), DAG::LikelihoodNode() {
}

FinalResultNode::~FinalResultNode() {
}


void FinalResultNode::doProcessing() {

	// LOG_LIKELIHOOD
	likelihood = 0.;

	// GTRG likelihoods
	for(size_t iN=0; iN<vecCSGN.size(); ++iN) {
		likelihood += vecCSGN[iN]->getLikelihood(); // Log likelihood already
	}

	//std::cout << likelihood << std::endl;
  	if(likelihood != likelihood) {
		//std::cerr << "Error in TIGamma::FinalResultNode -- Nan likelihood." << std::endl << this->toString() << std::endl;
		likelihood = -std::numeric_limits<double>::infinity();
	} else if (likelihood == std::numeric_limits<double>::infinity()) {
		//std::cerr << "Error in TIGamma::FinalResultNode -- Inf likelihood." << std::endl << this->toString() << std::endl;
		likelihood = -std::numeric_limits<double>::infinity();
	}

}

bool FinalResultNode::processSignal(DAG::BaseNode* aChild) {

	return true;
}

void FinalResultNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CombineSiteGammaNode)){
		CombineSiteGammaNode *node = dynamic_cast<CombineSiteGammaNode*>(aChild);
		vecCSGN.push_back(node);
	}
}


std::string FinalResultNode::toString() const {
	std::stringstream ss;
	ss << "[FinalResultNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
