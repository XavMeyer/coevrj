//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EdgeGammaNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "EdgeGammaNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

const EdgeGammaNode::scaling_t EdgeGammaNode::SCALING_TYPE = LOG;
const float EdgeGammaNode::SCALING_THRESHOLD = 1.; // Scale all with 1.


EdgeGammaNode::EdgeGammaNode(const size_t aIDGamma, const std::vector<size_t> &aSitePos,
				   DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), idGamma(aIDGamma), frequencies(aFrequencies)  {

	bmNode = NULL;
	init(aSitePos);
}

EdgeGammaNode::~EdgeGammaNode() {
}


void EdgeGammaNode::init(const std::vector<size_t> &sitePos) {
	// Create vectors
	H.resize(frequencies.size(), sitePos.size());

	if(SCALING_TYPE == LOG) {
		nrm.resize(0);//nrm.resize(sitePos.size());
	}
}


const TI_Gamma_EigenMatrix_t& EdgeGammaNode::getH() const {
	return H;
}

const TI_EigenVector_t& EdgeGammaNode::getNorm() const {
	return nrm;
}

size_t EdgeGammaNode::getIDGamma() const {
	return idGamma;
}


} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
