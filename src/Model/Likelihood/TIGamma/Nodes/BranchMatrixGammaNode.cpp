//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchMatrixGammaNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "BranchMatrixGammaNode.h"

#include "DiscreteGammaNode.h"
#include "Types.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

BranchMatrixGammaNode::BranchMatrixGammaNode(const size_t aIDGamma, DL_Utils::Frequencies &aFrequencies) :
		BaseNode(), idGamma(aIDGamma), frequencies(aFrequencies),
		Z(frequencies.size(), frequencies.size()) {

	branchLength = 0;
	matrixNode = NULL;
	discreteGammaNode = NULL;

}

BranchMatrixGammaNode::~BranchMatrixGammaNode() {
}

bool BranchMatrixGammaNode::setBranchLength(const double aBL) {
	if(branchLength != aBL) {
		//std::cout << this->getId() << " -- " << branchLength << " -- " << aBL << std::endl;
 		branchLength = aBL;
		BaseNode::updated();
		return true;
	} else {
		return false;
	}
}

double BranchMatrixGammaNode::getBranchLength() const {
	return branchLength;
}

size_t BranchMatrixGammaNode::getIDGamma() const {
	return idGamma;
}

const TI_Gamma_EigenSquareMatrix_t& BranchMatrixGammaNode::getZ() const {
	return Z;
}

double BranchMatrixGammaNode::getScaledBranchLength() const {
	double gammaRate = discreteGammaNode->getRate(idGamma);
	double t = gammaRate*branchLength;
	t /= matrixNode->getMatrixQScaling();
	return t;
}

void BranchMatrixGammaNode::doProcessing() {

	// Shortcuts
	const TI_EigenEIGVector_t &D = matrixNode->getEigenValues();
	const TI_Gamma_EigenEIGMatrix_t &scaledV = matrixNode->getScaledEigenVectors();

	// Process number of substitutions
	double gammaRate = discreteGammaNode->getRate(idGamma);
	double t = gammaRate*branchLength;
	t /= matrixNode->getMatrixQScaling();
	//std::cout << std::scientific << this->getId() << " -- " << discreteGammaNode->getAlpha() << " -- " << gammaRate << " -- " << branchLength << " -- " << t << std::endl;

	// Processing Y
	TI_EigenEIGArray_t dt = (t/2.0) * D.array();
	dt = dt.exp();
	TI_EigenEIGVector_t dt2 = dt.matrix();

	TI_Gamma_EigenEIGMatrix_t Y = scaledV * dt2.asDiagonal();
	//Z.setZero();
#if TI_GAMMA_USE_FLOAT
	TI_Gamma_EigenEIGMatrix_t tmpZ = Y*Y.transpose();
	Z.triangularView<Eigen::Lower>() = tmpZ.cast<float>();
#else
	Z.triangularView<Eigen::Lower>() = Y*Y.transpose();
#endif

	Z.eval();

	/*if(t<1e-20) {
		std::cout << t << std::endl;

		std::cout << dt << std::endl;

		std::cout << Y << std::endl;

		std::cout << Z << std::endl;

		std::cout << "--------------------------" <<  std::endl;
	}*/

}

bool BranchMatrixGammaNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void BranchMatrixGammaNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixGammaNode)){
		matrixNode = dynamic_cast<MatrixGammaNode*>(aChild);
	} else if(childtype == typeid(DiscreteGammaNode)){
		discreteGammaNode = dynamic_cast<DiscreteGammaNode*>(aChild);
	}
}

std::string BranchMatrixGammaNode::toString() const {
	std::stringstream ss;
	ss << "[BranchMatrixGammaNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
