//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FinalResultNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef FINALRESULTNODE_TIGAMMA_H_
#define FINALRESULTNODE_TIGAMMA_H_

#include <stddef.h>
#include <vector>

#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Node/Base/LikelihoodNode.h"
#include "CombineSiteGammaNode.h"


namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

class FinalResultNode: public DAG::BaseNode, public DAG::LikelihoodNode {
public:
	FinalResultNode();
	~FinalResultNode();

	std::string toString() const;

private:
	std::vector< CombineSiteGammaNode* > vecCSGN;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
};

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* FINALRESULTNODE_TIGAMMA_H_ */
