//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchMatrixGammaNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef BRANCHMATRIXGAMMANODE_TI_GAMMA_H_
#define BRANCHMATRIXGAMMANODE_TI_GAMMA_H_

#include <stddef.h>

#include "DiscreteGammaNode.h"
#include "Types.h"
#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"

#include "MatrixGammaNode.h"

#include "Eigen/Dense"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

class DiscreteGammaNode;
class MatrixGTRNode;

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class BranchMatrixGammaNode: public DAG::BaseNode {
public:
	BranchMatrixGammaNode(const size_t aIdGamma, DL_Utils::Frequencies &aFrequencies);
	~BranchMatrixGammaNode();

	bool setBranchLength(const double aBL);
	double getBranchLength() const;
	double getScaledBranchLength() const;

	size_t getIDGamma() const;

	const TI_Gamma_EigenSquareMatrix_t& getZ() const;

	std::string toString() const;

private:

	const size_t idGamma;
	double branchLength;
	DL_Utils::Frequencies &frequencies;

	MatrixGammaNode *matrixNode;
	DiscreteGammaNode *discreteGammaNode;

	TI_Gamma_EigenSquareMatrix_t Z;


	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BRANCHMATRIXGAMMANODE_TI_GAMMA_H_ */
