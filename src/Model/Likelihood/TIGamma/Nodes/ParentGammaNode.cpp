//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ParentGammaNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "ParentGammaNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

class EdgeGammaNode;

ParentGammaNode::ParentGammaNode(const size_t aIDGamma, const std::vector<size_t> &aSitePos,
		   	   	   	   DL_Utils::Frequencies &aFrequencies) :
							   EdgeGammaNode(aIDGamma, aSitePos, aFrequencies) {

}

ParentGammaNode::~ParentGammaNode() {
}

std::string ParentGammaNode::toString() const {
	std::stringstream ss;
	ss << BaseNode::toString();

	return ss.str();
}

void ParentGammaNode::addChild(EdgeGammaNode* childrenNode) {
	children.push_back(childrenNode);
}

bool ParentGammaNode::removeChild(EdgeGammaNode* childNode) {
	std::vector<EdgeGammaNode*>::iterator pos = std::find(children.begin(), children.end(), childNode);
	if (pos != children.end()) {
		children.erase(pos);
		return true;
	}
	return false;
}

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
