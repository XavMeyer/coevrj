//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteGammaNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef COMBINE_SITE_GAMMA_NODE_TIG_H_
#define COMBINE_SITE_GAMMA_NODE_TIG_H_

#include <stddef.h>
#include <vector>

#include "RootGammaNode.h"
#include "Types.h"
#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Node/Base/LikelihoodNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

namespace DL = ::MolecularEvolution::DataLoader;

class CombineSiteGammaNode: public DAG::BaseNode, public DAG::LikelihoodNode {
public:
	CombineSiteGammaNode(const size_t aNGamma, const std::vector<size_t> &aSites,
						 DL::CompressedAlignements &ca);
	~CombineSiteGammaNode();

	std::string toString() const;

private:

	struct SiteMapping {
		size_t site, position;

		SiteMapping(size_t aSite, size_t aPosition) :
			site(aSite), position(aPosition) {
		}
		~SiteMapping(){}

		bool operator== (const SiteMapping &aPLS) const {
			return aPLS.site == site;
		}
	};

	typedef std::vector<SiteMapping> vecSM_t;
	typedef vecSM_t::iterator itVecSM_t;
	vecSM_t sitesMapping;

	static const TI_GAMMA_TYPE MIN_PROBABILITY;
	static const TI_GAMMA_TYPE LOG_MIN, WORST_VAL;
	static const size_t NOT_REPRESENTED;

	const size_t N_GAMMA, nSubSite;
	std::vector<RootGammaNode*> rootNodes;

	std::vector<size_t> sites;
	DL::CompressedAlignements &compressedAligns;
	TI_EigenVector_t allSitesLogLik, siteLogLik;

	TI_EigenArray_t factors;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

	void combineSite();
	void combineSiteWithLogScaling();

};

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COMBINE_SITE_GAMMA_NODE_TIG_H_ */
