//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteGammaNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "CombineSiteGammaNode.h"

#include "Types.h"

#define LOG_PLUS(x,y) (x>y ? x+log(1.+exp(y-x)) : y+log(1.+exp(x-y)))

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

const TI_GAMMA_TYPE CombineSiteGammaNode::MIN_PROBABILITY = 1e-300;
const TI_GAMMA_TYPE CombineSiteGammaNode::LOG_MIN = log(std::numeric_limits<TI_GAMMA_TYPE>::min());
const TI_GAMMA_TYPE CombineSiteGammaNode::WORST_VAL = -std::numeric_limits<TI_GAMMA_TYPE>::max();
const size_t CombineSiteGammaNode::NOT_REPRESENTED = std::numeric_limits<size_t>::max();

CombineSiteGammaNode::CombineSiteGammaNode(const size_t aNGamma, const std::vector<size_t> &aSites,
										   DL::CompressedAlignements &ca) :
		DAG::BaseNode(), N_GAMMA(aNGamma), nSubSite(aSites.size()),
		rootNodes(N_GAMMA, NULL), sites(aSites), compressedAligns(ca), factors(nSubSite, 1) {

	for(size_t i=0; i<nSubSite; ++i) {
		factors(i) = ca.getSiteFactor(sites[i]);
	}

	for(size_t iS=0; iS<sites.size(); ++iS) {
		sitesMapping.push_back(SiteMapping(sites[iS], iS));
	}

	siteLogLik.setZero(sites.size());
	allSitesLogLik.setZero(sites.size());

}

CombineSiteGammaNode::~CombineSiteGammaNode() {
}

void CombineSiteGammaNode::combineSite() {
	// LOG_LIKELIHOOD
	likelihood = 0.;

	if(N_GAMMA == 1) {
		TI_EigenVector_t scaledSiteLik = rootNodes[0]->getLikelihood();
		siteLogLik = scaledSiteLik.array().log()*factors;
		for(size_t iS=0; iS < (size_t)siteLogLik.size(); ++iS) {
			if(siteLogLik(iS) == -std::numeric_limits<TI_GAMMA_TYPE>::infinity()) {
				siteLogLik(iS) = factors(iS)*WORST_VAL;
			} else if(siteLogLik(iS) < MIN_PROBABILITY) {
				siteLogLik(iS) = factors(iS)*WORST_VAL;
			}
		}
		likelihood = siteLogLik.sum();
	} else {
		// Compute new site lik
		TI_EigenVector_t scaledSiteLik = rootNodes[0]->getLikelihood();
		for(size_t iG=1; iG<N_GAMMA; ++iG) {
			scaledSiteLik = scaledSiteLik + rootNodes[iG]->getLikelihood();
		}
		scaledSiteLik = (scaledSiteLik/(double)N_GAMMA);

		siteLogLik = scaledSiteLik.array().log()*factors;

		for(size_t iS=0; iS < (size_t)siteLogLik.size(); ++iS) {
			if(siteLogLik(iS) == -std::numeric_limits<TI_GAMMA_TYPE>::infinity()) {
				siteLogLik(iS) = factors(iS)*WORST_VAL;
			} else if(siteLogLik(iS) < MIN_PROBABILITY) {
				siteLogLik(iS) = factors(iS)*WORST_VAL;
			}
		}

		likelihood = siteLogLik.sum();
	}
}

void CombineSiteGammaNode::combineSiteWithLogScaling() {
	// LOG_LIKELIHOOD
	likelihood = 0.;

	if(N_GAMMA == 1) {
		const TI_EigenVector_t &logNorm = rootNodes[0]->getNorm();
		siteLogLik = (rootNodes[0]->getLikelihood().array().log() + logNorm.array())*factors;
		for(size_t iS=0; iS < (size_t)siteLogLik.size(); ++iS) {
			if(siteLogLik(iS) == -std::numeric_limits<TI_GAMMA_TYPE>::infinity()) {
				siteLogLik(iS) = factors(iS)*WORST_VAL;
			} else if(rootNodes[0]->getLikelihood()(iS) < MIN_PROBABILITY) {
				siteLogLik(iS) = factors(iS)*WORST_VAL;
			}
		}
		likelihood = siteLogLik.sum();
	} else {
		for(size_t iS=0; iS<(size_t)siteLogLik.size(); ++iS) {
			TI_GAMMA_TYPE logLikSite = 0;
			// For each Gamma rate
			for(size_t iG=0; iG<N_GAMMA; ++iG) {
				const TI_EigenVector_t &logNorm = rootNodes[iG]->getNorm();
				const TI_EigenVector_t &subLik = rootNodes[iG]->getLikelihood();

				TI_GAMMA_TYPE tmpSublik = subLik(iS) < MIN_PROBABILITY ?  0. : subLik(iS);
				tmpSublik *= (double) 1./N_GAMMA;
				TI_GAMMA_TYPE tmpLogLik = log(tmpSublik) + logNorm(iS);

				//std::cout << iG+1 << "\t" << iS << "\t" << tmpLogLik << "\t" << subLik(iS) << "\t" << logNorm(iS) << std::endl;

				if(iG == 0) {
					logLikSite = tmpLogLik;
				} else {
					// Safe sum of log value
					TI_GAMMA_TYPE x = logLikSite;
					TI_GAMMA_TYPE y = tmpLogLik;
					if(x<=y) std::swap(x,y);
					logLikSite = x + log(1.+exp(y-x));
				}
			}

			//TI_GAMMA_TYPE factor = compressedAligns.getSiteFactor(sites[iS]);
			likelihood += factors(iS)*logLikSite;
			//std::cout << "Factor " << factors(iS) << "\t" << logLikSite << "\t" << likelihood << std::endl;
			//if(likelihood == std::numeric_limits<double>::infinity()) getchar();
		}
	}
}


void CombineSiteGammaNode::doProcessing() {
	if(EdgeGammaNode::SCALING_TYPE == EdgeGammaNode::LOG) {
		combineSiteWithLogScaling();
	} else {
		combineSite();
	}
}

bool CombineSiteGammaNode::processSignal(BaseNode* aChild) {

	return true;
}

void CombineSiteGammaNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(RootGammaNode)){
		RootGammaNode *node = dynamic_cast<RootGammaNode*>(aChild);
		const size_t iG = node->getIDGamma();
		rootNodes[iG] = node;
	}
}

std::string CombineSiteGammaNode::toString() const {
	std::stringstream ss;
	ss << "[CombineSiteGammaNode] " << BaseNode::toString();
	return ss.str();
}


} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
