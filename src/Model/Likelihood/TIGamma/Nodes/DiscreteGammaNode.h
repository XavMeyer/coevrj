//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DiscreteGammaNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef DISCRETEGAMMANODE_TIG_H_
#define DISCRETEGAMMANODE_TIG_H_

#include <string>
#include <vector>

#include "Types.h"
#include "DAG/Node/Base/BaseNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {


class DiscreteGammaNode : public DAG::BaseNode {
public:
	typedef enum {MEAN_GAMMA, MEDIAN_GAMMA} gammaEstimation_t;

public:

	DiscreteGammaNode(const size_t aNGamma, gammaEstimation_t aGammaEstimation = MEDIAN_GAMMA);
	~DiscreteGammaNode();

	double getAlpha() const;
	bool setAlpha(double aAlpha);

	double getRate(size_t idGamma);
	std::vector<double>& getRates();

	std::string toString() const;

private:
	const gammaEstimation_t GAMMA_ESTIMATION;

	const size_t N_GAMMA;
	double alpha;
	std::vector<double> rates;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

};

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* DISCRETEGAMMANODE_TIG_H_ */
