//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPVGammaNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef CPV_GAMMA_NODE_TIG_H_
#define CPV_GAMMA_NODE_TIG_H_

#include <stddef.h>

#include "LeafGammaNode.h"
#include "ParentGammaNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace TIGamma {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class CPVGammaNode : public ParentGammaNode {
public:

	CPVGammaNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
			DL_Utils::Frequencies &aFrequencies);
	~CPVGammaNode();

	std::string toString() const;

private: // Methods

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
	void doRemoveChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace TIGamma */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* CPV_GAMMA_NODE_TIG_H_ */
