//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevLikelihoodInfo.cpp
 *
 * @date Apr 5, 2017
 * @author meyerx
 * @brief
 */
#include "CoevLikelihoodInfo.h"

#include "Model/Likelihood/CoevRJ/UID/SubSpaceUID.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class MatrixCoevNode;
class RootCoevNode;

CoevLikelihoodInfo::CoevLikelihoodInfo(SubSpaceUID::sharedPtr_t aPtrUIDSubSpace,
		                               const std::vector<MD::idProfile_t> &aVecProfilesId) :
	ptrUIDSubSpace(aPtrUIDSubSpace), pos1(ptrUIDSubSpace->pos1), pos2(ptrUIDSubSpace->pos2),
	vecProfilesId(aVecProfilesId) {

	treeHash = 0;

	rootDAG = NULL;
	ptrMatrixCoev = NULL;

	if(pos1 > pos2) {
		std::swap(pos1, pos2);
	}
}

CoevLikelihoodInfo::~CoevLikelihoodInfo() {
}

std::pair<size_t, size_t> CoevLikelihoodInfo::getPositions() const {
	return std::make_pair(pos1, pos2);
}

const std::vector<MD::idProfile_t>& CoevLikelihoodInfo::getProfilesId() const {
	return vecProfilesId;
}

SubSpaceUID::sharedPtr_t CoevLikelihoodInfo::getUIDSubSpace() const {
	return ptrUIDSubSpace;
}

void CoevLikelihoodInfo::setRootDAG(RootCoevNode *aRootDAG) {
	rootDAG = aRootDAG;
}

RootCoevNode* CoevLikelihoodInfo::getRootDAG() {
	return rootDAG;
}

void CoevLikelihoodInfo::setMatrixCoev(MatrixCoevNode *aMatrixCoev) {
	ptrMatrixCoev = aMatrixCoev;
}

MatrixCoevNode* CoevLikelihoodInfo::getMatrixCoev() {
	return ptrMatrixCoev;
}

void CoevLikelihoodInfo::setLeafNodes(std::vector<LeafCoevNode*>& aLeafNodes) {
	leafNodes = aLeafNodes;
}

std::vector<LeafCoevNode*>& CoevLikelihoodInfo::getLeafNodes() {
	return leafNodes;
}

void CoevLikelihoodInfo::setPInd(const std::vector<size_t>& aPInd) {
	pInd = aPInd;
}

const std::vector<size_t>& CoevLikelihoodInfo::getPInd() const {
	return pInd;
}

void CoevLikelihoodInfo::setTreeHash(long int aTreeHash) {
	treeHash = aTreeHash;
}

long int CoevLikelihoodInfo::getTreeHash() const {
	return treeHash;
}

bool CoevLikelihoodInfo::operator== (const CoevLikelihoodInfo &aCLI) const {
	bool samePosition = (pos1 == aCLI.pos1) && (pos2 == aCLI.pos2);
	return samePosition;
}

std::string CoevLikelihoodInfo::toString() const {
	std::stringstream ss;

	ss << "Label : " << ptrUIDSubSpace->getLabelUID() << " - positions = [ " << pos1 << ", " << pos2 << " ]";
	ss << ", lik = " << rootDAG->getLogLikelihood() << std::endl;
	ss << ", possibleProfiles = [ ";
	for(size_t iV=0; iV<vecProfilesId.size(); ++iV) {
		ss << vecProfilesId[iV];
		if( iV == vecProfilesId.size()-1 ) ss << " ]";
		else ss << " | ";
	}

	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
