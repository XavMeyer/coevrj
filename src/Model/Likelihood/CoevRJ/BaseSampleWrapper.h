//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseSampleWrapper.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef BASESAMPLEWRAPPER_COEVRJ_H_
#define BASESAMPLEWRAPPER_COEVRJ_H_

#include <stddef.h>
#include <iostream>
#include <sstream>
#include <vector>

#include "Nodes/Types.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/Instructions/MultStore.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

namespace MU = ::MolecularEvolution::MatrixUtils;
class Base;

class BaseSampleWrapper {
	friend class Base;
public:
	BaseSampleWrapper(const size_t aNPriorParamValues, const size_t aNBranch,
					  const std::vector<long int> &intVals, const std::vector<double> &dblVals);
	~BaseSampleWrapper();

	std::string toString() const;

private:
	static const size_t N_THETA;

	const size_t N_PRIOR_PARAM_VALUES, N_BL;
	long int treeH;
	double alpha, coevScaling, lambdaPoisson;
	std::vector<double> stFreq, thetas, priorParamValues, branchLength;
};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BASESAMPLEWRAPPER_COEVRJ_H_ */
