//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RestartFileReader.cpp
 *
 * @date Jul 7, 2017
 * @author meyerx
 * @brief
 */

#include "RestartFileReader.h"

#include <iosfwd>
#include <boost/algorithm/string/split.hpp>

#include "Sampler/Samples/Sample.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/InitMoveCoevRJ.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

RestartFileReader::RestartFileReader(Base* aPtrLik) :
	restartFileName(aPtrLik->getRestartFileName()), ptrLik(aPtrLik) {
	readFile();
}

RestartFileReader::~RestartFileReader() {

}

std::vector<double> RestartFileReader::getParametersValue() const {
	return parametersValue;
}

std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> RestartFileReader::getInitMoves() const {
	return initMoves;
}

void RestartFileReader::readFile() {

	Sampler::Sample dummySample;

	std::ifstream iFile(restartFileName.c_str());
	assert(iFile.good() && "The restart file has not been found.");

	std::string line;
	std::vector<string> tokens;

	// Number of params and coev clusters
	getline(iFile, line);
	boost::split(tokens, line, boost::is_any_of("\t"));
	assert(tokens.size() == 2);
	size_t nParams = atol(tokens[0].c_str());
	size_t nCoev = atol(tokens[1].c_str());

	// Parameters
	getline(iFile, line);
	boost::trim(line);
	boost::split(tokens, line, boost::is_any_of("\t"));
	parametersValue.resize(nParams);
	for(size_t iT=0; iT<tokens.size(); ++iT) {
		parametersValue[iT] = atof(tokens[iT].c_str());
		//std::cout << parametersValue[iT] << "\t"; // FIXME DEBUG
	}
	//std::cout << std::endl << "PARAMS END" << std::endl; // FIXME DEBUG

	const std::set<size_t>& segSites = ptrLik->getSegregatingSitesGTR();

	// Coev clusters
	for(size_t iC=0; iC<nCoev; ++iC) {
		assert(iFile.good() && "There is some missing parameters for coev clusters");

		getline(iFile, line);
		//std::cout << line << std::endl; // FIXME DEBUG
		boost::split(tokens, line, boost::is_any_of("\t"));

		size_t pos1 = atol(tokens[0].c_str());
		size_t pos2 = atol(tokens[1].c_str());

		std::string profile = tokens[2];

		std::vector<double> vals(4);
		vals[0] = 0.;//atof(tokens[7].c_str());
		vals[1] = 0.;//atof(tokens[8].c_str());
		vals[2] = 0.;//atof(tokens[9].c_str());
		vals[3] = 0.;//atof(tokens[10].c_str());

		if(segSites.count(pos1) > 0 && segSites.count(pos2) > 0 ) {
			Sampler::RJMCMC::RJMove::sharedPtr_t aMove(new StatisticalModel::Likelihood::CoevRJ::InitMoveCoevRJ(pos1, pos2, profile, vals, dummySample, ptrLik));
			initMoves.push_back(aMove);
		} else if(Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "[RestartFileReader] : ";
			if(segSites.count(pos1) == 0) std::cout << "\t Position = " << pos1 << " is not available";
			if(segSites.count(pos2) == 0) std::cout << "\t Position = " << pos2 << " is not available";
			std::cout << std::endl;
		}
		//std::cout << "Move ok" << std::endl; // FIXME DEBUG
	}
	//std::cout << std::endl << "COEV END" << std::endl; // FIXME DEBUG
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
