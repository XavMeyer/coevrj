//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RestartFileReader.h
 *
 * @date Jul 7, 2017
 * @author meyerx
 * @brief
 */
#ifndef RESTARTFILEREADER_H_
#define RESTARTFILEREADER_H_

#include <string>
#include <vector>

#include "Model/Likelihood/CoevRJ/Base.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class RestartFileReader {
public:
	RestartFileReader(Base* aPtrLik);
	~RestartFileReader();

	std::vector<double> getParametersValue() const;
	std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> getInitMoves() const;

private:

	std::string restartFileName;
	Base* ptrLik;

	std::vector<double> parametersValue;
	std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> initMoves;

	void readFile();

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* RESTARTFILEREADER_H_ */
