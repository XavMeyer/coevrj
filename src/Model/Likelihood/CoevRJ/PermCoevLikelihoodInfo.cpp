//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermCoevLikelihoodInfo.cpp
 *
 * @date Aug 15, 2017
 * @author meyerx
 * @brief
 */
#include "PermCoevLikelihoodInfo.h"

#include "Model/Likelihood/CoevRJ/UID/SubSpaceUID.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class MatrixCoevNode;
class RootCoevNode;

PermCoevLikelihoodInfo::PermCoevLikelihoodInfo(SubSpaceUID::sharedPtr_t aPtrUIDSubSpace,
		                               const std::vector<MD::idProfile_t> &aVecProfilesId) :
	ptrUIDSubSpace(aPtrUIDSubSpace),
	pos1(ptrUIDSubSpace->pos1), pos2(ptrUIDSubSpace->pos2), vecProfilesId(aVecProfilesId) {

	if(pos1 > pos2) {
		std::swap(pos1, pos2);
	}

	currentIdProfile = 0;
	profileSize = 0;
}

PermCoevLikelihoodInfo::~PermCoevLikelihoodInfo() {
}

std::pair<size_t, size_t> PermCoevLikelihoodInfo::getPositions() const {
	return std::make_pair(pos1, pos2);
}

const std::vector<MD::idProfile_t>& PermCoevLikelihoodInfo::getProfilesId() const {
	return vecProfilesId;
}

SubSpaceUID::sharedPtr_t PermCoevLikelihoodInfo::getUIDSubSpace() const {
	return ptrUIDSubSpace;
}

void PermCoevLikelihoodInfo::setPInd(const std::vector<size_t>& aPInd) {
	pInd = aPInd;
}

const std::vector<size_t>& PermCoevLikelihoodInfo::getPInd() const {
	return pInd;
}

void PermCoevLikelihoodInfo::setIdProfile(size_t aIdProfile) {
	currentIdProfile = aIdProfile;
}

size_t PermCoevLikelihoodInfo::getIdProfile() const {
	return currentIdProfile;
}

void PermCoevLikelihoodInfo::setProfileSize(size_t aProfileSize) {
	profileSize = aProfileSize;
}

size_t PermCoevLikelihoodInfo::getProfileSize() const {
	return profileSize;
}

bool PermCoevLikelihoodInfo::operator== (const PermCoevLikelihoodInfo &aCLI) const {
	bool samePosition = (pos1 == aCLI.pos1) && (pos2 == aCLI.pos2);
	return samePosition;
}

std::string PermCoevLikelihoodInfo::toString() const {
	std::stringstream ss;

	ss << "idSubSapce = " << ptrUIDSubSpace->getLabelUID() << ", positions = [ " << pos1 << ", " << pos2 << " ]";
	ss << ", possibleProfiles = [ ";
	for(size_t iV=0; iV<vecProfilesId.size(); ++iV) {
		ss << vecProfilesId[iV];
		if( iV == vecProfilesId.size()-1 ) ss << " ]";
		else ss << " | ";
	}

	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
