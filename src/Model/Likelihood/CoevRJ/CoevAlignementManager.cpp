//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevAlignementManager.cpp
 *
 * @date Apr 20, 2017
 * @author meyerx
 * @brief
 */
#include "CoevAlignementManager.h"

#include <assert.h>
#include <limits>

#include "Parallel/Parallel.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/Definitions/MolecularDefIncl.h"
#include "Utils/MolecularEvolution/Definitions/Nucleotides.h"


namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

const bool CoevAlignementManager::USE_UNIFORM_WHEN_ALL_PROFILE_OBSERVED = false;

const size_t CoevAlignementManager::SCORE_VERSION = 3;
const bool CoevAlignementManager::SKIP_NON_INFORMATIVE_PAIRS = true;

const size_t CoevAlignementManager::MAX_ALIGNEMENT_SIZE_FOR_PRECOMPUT = 20000;
const double CoevAlignementManager::MIN_SCORE = 0.05;
const double CoevAlignementManager::MAX_VARIANCE = 10000;

const double CoevAlignementManager::CONFLICT_TOLERANCE_RATIO = 0.1;
const double CoevAlignementManager::PROFILE_NUMBER_SCORE_WEIGHT = 3.5;
const double CoevAlignementManager::MIN_CONFLICTS_SCORE_WEIGHT = 6.;
const double CoevAlignementManager::PAIR_VARIANCE_SCORE_WEIGHT = 2.;

CoevAlignementManager::CoevAlignementManager(DL::MSA &msa) :
		FULL_PROFILE_ID(constructFullProfileId()),
		N_SITE(msa.getNSite()), N_ORDERED_PAIRS(N_SITE*(N_SITE-1)/2),
		MIN_ACCEPTABLE_CONFLICTS(CONFLICT_TOLERANCE_RATIO*msa.getNAlignments()), // We tolerate a 10% of conflict
		mapSitesCode(constructSiteMap(msa)),
		vecPairsProfilesInfo(constructVecPairsProfilesInfo(msa)) {

	init(msa);

}

CoevAlignementManager::CoevAlignementManager(DL::MSA &msa, const std::string &aCacheFileName) :
		FULL_PROFILE_ID(constructFullProfileId()),
		N_SITE(msa.getNSite()), N_ORDERED_PAIRS(N_SITE*(N_SITE-1)/2),
		MIN_ACCEPTABLE_CONFLICTS(CONFLICT_TOLERANCE_RATIO*msa.getNAlignments()), // We tolerate a 10% of conflict
		cacheFileName(aCacheFileName),
		mapSitesCode(constructSiteMap(msa)),
		vecPairsProfilesInfo(constructVecPairsProfilesInfo(msa)) {

	init(msa);
}


CoevAlignementManager::~CoevAlignementManager() {
}

void CoevAlignementManager::init(DL::MSA &msa) {
	assert(N_SITE < std::numeric_limits<position_t>::max() && "You should change the type for position_t to uint or size_t.");

	// Define the score of a position as the best possible score for a pair including this position
	positionsScore.assign(msa.getNSite(), 0.);
	for(size_t iPPI=0; iPPI<vecPairsProfilesInfo.size(); ++iPPI) {
		std::pair<position_t, position_t> positions = linearToTriangularUpperCoordinate(iPPI);
		size_t pos1 = positions.first;
		size_t pos2 = positions.second;
		score_t score = vecPairsProfilesInfo[iPPI].score;

		positionsScore[pos1] = std::max(positionsScore[pos1], score);
		positionsScore[pos2] = std::max(positionsScore[pos2], score);
	}

	if(Parallel::mpiMgr().isMainProcessor() && N_SITE > MAX_ALIGNEMENT_SIZE_FOR_PRECOMPUT/30) {
		std::cout << "[CoevAlignementManager] Position score constructed." << std::endl;
	}

	// Define the matrix of pairs and score
	positionsToPairsScore.resize(msa.getNSite());
	for(size_t iPTP=0; iPTP<positionsToPairsScore.size(); ++iPTP) {
		std::vector<score_t> tmpScores(msa.getNSite());
		for(size_t iP=0; iP<msa.getNSite(); ++iP) {
			if(iPTP == iP) {
				tmpScores[iP] = 0.;
			} else {
				size_t pos1 = iPTP;
				size_t pos2 = iP;
				if(pos1>pos2) std::swap(pos1, pos2);
				double score = vecPairsProfilesInfo[triangularUpperToLinearCoordinate(std::make_pair(pos1, pos2))].score;
				tmpScores[iP] = score;
			}
		}
		positionsToPairsScore[iPTP] = tmpScores;
	}

	if(Parallel::mpiMgr().isMainProcessor() && N_SITE > MAX_ALIGNEMENT_SIZE_FOR_PRECOMPUT/30) {
		std::cout << "[CoevAlignementManager] Position to pairs score (matrix) constructed." << std::endl;
	}

	//if(N_SITE > 3700)
		//DBG_CHECK_SCORES_FOR_16SRNA();
	//DBG_COMPUTE_SCORES_FOR_16SRNA();
}

CoevAlignementManager::mapSitesCode_t CoevAlignementManager::constructSiteMap(DL::MSA &msa) const {

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[CoevAlignementManager] Precomputing profiles per pairs of position. May take some time..." << std::endl;
		assert(msa.getNSite() < MAX_ALIGNEMENT_SIZE_FOR_PRECOMPUT && "This was not planned for such long sequences.");
	}

	mapSitesCode_t map;

	const std::vector<DL::Alignment>& aligns = msa.getAlignments();
	for(size_t iA=0; iA<aligns.size(); ++iA) {
		std::string name(aligns[iA].getName());

		sitesCode_t sitesCode;
		for(size_t iP=0; iP<msa.getNSite(); ++iP) {
			DL::Alignment::vecCode_t vecCode(aligns[iA].getNucleotideSequence(iP));
			MD::bitset16b_t bitset;
			for(size_t iC=0; iC<vecCode.size(); ++iC){
				bitset[vecCode[iC]] = true;
			}
			sitesCode.push_back(bitset);
		}
		map.insert(std::make_pair(name, sitesCode));
	}

	if(Parallel::mpiMgr().isMainProcessor() && N_SITE > MAX_ALIGNEMENT_SIZE_FOR_PRECOMPUT/30) {
		std::cout << "[CoevAlignementManager] Site map constructed." << std::endl;
	}

	return map;
}

std::vector<MD::idProfile_t> CoevAlignementManager::constructFullProfileId() const {
	std::vector<MD::idProfile_t> fullProfId;
	for(size_t iP=0; iP<MD::getNucleotidesPairProfiles()->NB_POSSIBLE_PROFILES; ++iP) {
		fullProfId.push_back(iP);
	}
	return fullProfId;
}

std::vector<PairProfileInfo> CoevAlignementManager::constructVecPairsProfilesInfo(DL::MSA &msa) const {

	double sTime = MPI_Wtime(); // FIXME
	std::vector<PairProfileInfo> vecAllPPI(N_ORDERED_PAIRS);

	//DBG_COMPUTE_SCORES_FOR_16SRNA();
	//abort();

	const bool useCache = !cacheFileName.empty();
	// If we have to use a cache file
	if(useCache) {
		// Load file if possible
		bool success = loadVecPairsProfilesInfo(vecAllPPI);

		if(success) { // 2.1) yes -> read and use
			return vecAllPPI;
		} // 2.2) no -> construct and write
	}

	double minScore = std::numeric_limits<double>::max();
	double avgTime2 = 0.;
	for(size_t iPos1=0; iPos1<msa.getNSite(); ++iPos1) {
		double sTime2 = MPI_Wtime();
		for(size_t iPos2=iPos1+1; iPos2<msa.getNSite(); ++iPos2) {
			size_t linearCoord = triangularUpperToLinearCoordinate(std::make_pair(iPos1, iPos2));
			// For each pair of positions
			std::pair<position_t, position_t> positions = std::make_pair(iPos1, iPos2);

			// Get possible profiles ID
			MD::bitset16b_t observedPairs = defineObservablePairs(iPos1, iPos2, SKIP_NON_INFORMATIVE_PAIRS);

			if(observedPairs.count() == 16) {
				vecAllPPI[linearCoord].uniform = USE_UNIFORM_WHEN_ALL_PROFILE_OBSERVED;
				vecAllPPI[linearCoord].profilesId = FULL_PROFILE_ID;
			} else {
				vecAllPPI[linearCoord].uniform = false;
				vecAllPPI[linearCoord].profilesId = MD::getNucleotidesPairProfiles()->getPossibleProfilesId(observedPairs);
			}

			// Define the score of the pair as well as the profile conflict count
			definePairProfilesInformation(iPos1, iPos2, vecAllPPI[linearCoord]);
			assert(vecAllPPI[linearCoord].profilesId.size() == vecAllPPI[linearCoord].profileProb.size());
			// If we are empty we have the a score of 0.
			assert(!vecAllPPI[linearCoord].profilesId.empty() || (vecAllPPI[linearCoord].profilesId.empty() && vecAllPPI[linearCoord].score == 0.));

			/*if(vecAllPPI[linearCoord].profilesId.empty()) {
				std::cout << iPos1 << "\t" << iPos2 << "\t" << vecAllPPI[linearCoord].score << "\t" << observedPairs.to_string() << std::endl;
			}*/

			if(vecAllPPI[linearCoord].uniform) {
				vecAllPPI[linearCoord].profilesId.clear();
				vecAllPPI[linearCoord].profileProb.clear();
			}

			if(!vecAllPPI[linearCoord].profilesId.empty() && vecAllPPI[linearCoord].score < minScore) {
				minScore = vecAllPPI[linearCoord].score;
			}

			// DEBUG FIXME
			/*if(vecAllPPI[linearCoord].score > 0. || vecAllPPI[linearCoord].profilesId.size() > 0) {
				if(vecAllPPI[linearCoord].score > 1e-9) std::cout << "G\t";
				if(vecAllPPI[linearCoord].score > 1e-7) std::cout << "GG\t";
				if(vecAllPPI[linearCoord].score > 1e-5) std::cout << "GGG\t";
				std::cout << iPos1 << "\t" << iPos2 << "\tNProfile = " << vecAllPPI[linearCoord].profilesId.size() << "\tscore : " << vecAllPPI[linearCoord].score << std::endl;
				for(size_t i=0; i<vecAllPPI[linearCoord].profilesId.size(); ++i) {
					std::cout << vecAllPPI[linearCoord].profilesId[i] << ", ";
				}
				std::cout << std::endl;
				for(size_t i=0; i<vecAllPPI[linearCoord].profileProb.size(); ++i) {
					std::cout << vecAllPPI[linearCoord].profileProb[i] << ", ";
				}
				std::cout << std::endl << "--------------------------------------------------------------------" << std::endl;
			}*/
			// END DEBUG FIXME
		}
		double eTime2 = MPI_Wtime(); // FIXME
		avgTime2 = ((iPos1)*avgTime2 + (eTime2-sTime2))/(iPos1+1);
		if(((1+iPos1) % (size_t)(N_SITE/10.)) == 0) {
			size_t remPos1 = msa.getNSite()-(iPos1+1);
			std::cout << "[CoevAlignementManager] " << iPos1 << " over " << N_SITE << " positions";
			std::cout << " -- estimated remaining time : " << (int)(avgTime2*remPos1/2.) << " seconds." << std::endl; // FIXME
		}
	}

	//std::cout << "Min score is : " << minScore << "\t new min score is : " << minScore*1.e-5 << std::endl; // FIXME
	for(size_t iP=0; iP<vecAllPPI.size(); ++iP) {
		if(vecAllPPI[iP].profilesId.empty()) {
			vecAllPPI[iP].score = minScore*1.e-5;
		}
	}

	if(Parallel::mpiMgr().isMainProcessor() && N_SITE > MAX_ALIGNEMENT_SIZE_FOR_PRECOMPUT/30) {
		double eTime = MPI_Wtime();
		std::cout << "[CoevAlignementManager] Info on profile pair constructed (vec) in " << eTime-sTime << "seconds." << std::endl;
	}

	if(useCache && Parallel::mpiMgr().isMainProcessor()) { // 2.2 Write the vecPPI after construction [CACHE]
		saveVecPairsProfilesInfo(vecAllPPI);
	}

	return vecAllPPI;
}

void CoevAlignementManager::saveVecPairsProfilesInfo(const std::vector<PairProfileInfo> &toSave) const {
	assert(!cacheFileName.empty());
	std::ofstream oFile(cacheFileName.c_str(), ios::binary);

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout.precision(3);
		double maxSize = (192*2*3)*N_SITE*(N_SITE-1)/(2*1e9);
		std::cout << "[CoevAlignementManager] Warning! Caching score option enabled. File [ " << cacheFileName << " ] (~ " << maxSize << " GB max.) will be saved." << std::endl;
	}

	oFile.write(reinterpret_cast<const char*>(&SCORE_VERSION), sizeof(size_t));

	size_t nbPPI = toSave.size();
	oFile.write(reinterpret_cast<const char*>(&nbPPI), sizeof(size_t));
	for(size_t iP=0; iP<toSave.size(); ++iP) {
		toSave[iP].write(oFile);
	}

}

bool CoevAlignementManager::loadVecPairsProfilesInfo(std::vector<PairProfileInfo> &toLoad) const {
	assert(!cacheFileName.empty());
	std::ifstream iFile(cacheFileName.c_str(), ios::binary);
	if(!iFile.good()) return false;

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[CoevAlignementManager] Caching score option enabled. File [ " << cacheFileName << " ] will be loaded." << std::endl;
	}

	size_t fileScoreVersion;
	iFile.read(reinterpret_cast<char*>(&fileScoreVersion), sizeof(size_t));
	if(fileScoreVersion != SCORE_VERSION) {
		if(Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "[CoevAlignementManager] The file [ " << cacheFileName << " ] was done with an incompatible previous version." << std::endl;
			std::cout << "[CoevAlignementManager] Scores are recomputed and will be saved." << std::endl;
		}
		return false;
	}

	size_t nbPPI;
	iFile.read(reinterpret_cast<char*>(&nbPPI), sizeof(size_t));
	assert(nbPPI == N_ORDERED_PAIRS);
	toLoad.resize(nbPPI);

	for(size_t iP=0; iP<toLoad.size(); ++iP) {
		toLoad[iP].read(iFile);
	}
	return true;
}

MD::bitset16b_t CoevAlignementManager::getSiteCode(const std::string &alignementName, size_t pos) const {

	mapSitesCode_t::const_iterator it = mapSitesCode.find(alignementName);
	assert(it != mapSitesCode.end());
	assert(pos < it->second.size());
	assert(!it->second.empty());

	return it->second[pos];

}

std::pair<MD::bitset16b_t, MD::bitset16b_t> CoevAlignementManager::getPairSitesCode(
		const std::string &alignementName, size_t pos1, size_t pos2) const {

	assert(pos1<pos2);
	MD::bitset16b_t siteCodePos1 = getSiteCode(alignementName, pos1);
	MD::bitset16b_t siteCodePos2 = getSiteCode(alignementName, pos2);

	return std::make_pair(siteCodePos1, siteCodePos2);

}

std::vector<MD::idProfile_t> CoevAlignementManager::getPossibleProfilesId(size_t pos1, size_t pos2) const {

	assert(pos1<pos2);
	if(vecPairsProfilesInfo.empty()) {
		MD::bitset16b_t observedPairs = defineObservablePairs(pos1, pos2, SKIP_NON_INFORMATIVE_PAIRS);
		return MD::getNucleotidesPairProfiles()->getPossibleProfilesId(observedPairs);
	} else {
		size_t linearIndex = triangularUpperToLinearCoordinate(std::make_pair(pos1, pos2));
		if(vecPairsProfilesInfo[linearIndex].uniform) {
			return FULL_PROFILE_ID;
		} else {
			return vecPairsProfilesInfo[linearIndex].profilesId;
		}
	}
}

MD::vecProfiles_t CoevAlignementManager::getPossibleProfiles(size_t pos1, size_t pos2) const {
	if(vecPairsProfilesInfo.empty()) {
		MD::bitset16b_t observedPairs = defineObservablePairs(pos1, pos2, SKIP_NON_INFORMATIVE_PAIRS);
		return MD::getNucleotidesPairProfiles()->getPossibleProfiles(observedPairs);
	} else {
		MD::vecProfiles_t res;
		std::vector<MD::idProfile_t> idProfiles = getPossibleProfilesId(pos1, pos2);
		for(size_t iP=0; iP<idProfiles.size(); ++iP) {
			res.push_back(MD::getNucleotidesPairProfiles()->idToProfile(idProfiles[iP]));
		}
		return res;
	}
}

PairProfileInfo CoevAlignementManager::getPairProfileInfo(size_t pos1, size_t pos2) const {
	assert(pos1<pos2);
	if(vecPairsProfilesInfo.empty()) {
		PairProfileInfo tmp;
		assert(false && "Not supported for extra large alignement for now.");
		return tmp;
	} else {
		size_t linearIndex = triangularUpperToLinearCoordinate(std::make_pair(pos1, pos2));
		return vecPairsProfilesInfo[linearIndex];
	}
}

const std::vector<score_t>& CoevAlignementManager::getPositionsScore() const {
	return positionsScore;
}

const std::vector<score_t>& CoevAlignementManager::getPairsIncludingPositionScore(size_t aPosition) const {
	assert(aPosition < positionsToPairsScore.size());
	return positionsToPairsScore[aPosition];
}

/*void CoevAlignementManager::defineProbablePairs(std::vector< std::pair<position_t, position_t > > &pairs, std::vector<PairProfileInfo> &ppis) const {

	// Get constant
	double nrmConstant = 0.;
	for(size_t iP=0; iP < vecPairsProfilesInfo.size(); ++iP) {
		nrmConstant += vecPairsProfilesInfo[iP].score;
	}

	// Get element that are bigger than uniformProb
	// The test is : score/cst > 1/nbElem ===> prob > unifProb
	// Reduce computation : score > cst*1/nbElem
	double scaledUniformProb = 1000.*nrmConstant/(double)vecPairsProfilesInfo.size();
	for(size_t iP=0; iP < vecPairsProfilesInfo.size(); ++iP) {
		if(vecPairsProfilesInfo[iP].score > scaledUniformProb) {
			pairs.push_back(linearToTriangularUpperCoordinate(iP));
			ppis.push_back(vecPairsProfilesInfo[iP]);
			ppis.back().score /= nrmConstant;
		}
	}

}*/


MD::bitset16b_t CoevAlignementManager::defineObservablePairs(size_t pos1, size_t pos2, bool skipUninformativePairs) const {
	assert(pos1<pos2);
	assert(!mapSitesCode.empty());

	MD::bitset16b_t observedPairs;
	// For each species
	for(mapSitesCode_t::const_iterator it = mapSitesCode.begin(); it != mapSitesCode.end(); ++it) {
		assert(pos1 < it->second.size() && pos2 < it->second.size());

		// Get single position code
		MD::bitset16b_t nuclCodePos1 = it->second[pos1];
		MD::bitset16b_t nuclCodePos2 = it->second[pos2];

		if(skipUninformativePairs && (nuclCodePos1.count() == 4 && nuclCodePos2.count() == 4)) {
			//std::cout << "Skiping non informative : " << it->first << "\t" << pos1 << "\t" << pos2 << std::endl;
			continue;
		}

		// Create paired codes (0:AA...15:GG)
		MD::bitset16b_t pairCode;
		for(size_t iNucl1=0; iNucl1<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl1) {
			for(size_t iNucl2=0; iNucl2<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl2) {
				if(nuclCodePos1[iNucl1] && nuclCodePos2[iNucl2]) { // Nucleotides are present at pos1 and pos2, resp.
					size_t profileCode = iNucl1*MD::getNucleotides()->NB_BASE_NUCL + iNucl2;
					pairCode[profileCode] = true;
				}
			}
		}
		observedPairs |= pairCode;
	}
	return observedPairs;
}

void CoevAlignementManager::definePairProfilesInformation(size_t pos1, size_t pos2, PairProfileInfo &ppi) const {
	assert(pos1<pos2);
	assert(!mapSitesCode.empty());

	if(ppi.profilesId.empty()) {
		ppi.score = 0.;
		return;
	}

	/// Prepare the code for each pair of nucleotides in each profiles
	// For each profiles
	std::vector<  MD::bitset16b_t > profilePairs(ppi.profilesId.size());
	std::vector< std::vector<double> > countInPairProfile(ppi.profilesId.size());
	for(size_t iProf=0; iProf<ppi.profilesId.size(); ++iProf) {
		// Get the pair strings
		MD::profile_t profile = MD::getNucleotidesPairProfiles()->idToProfile(ppi.profilesId[iProf]);
		// For each pairs
		for(size_t iPairs = 0; iPairs < profile.size(); ++iPairs) {
			// Define the code and keep it memorized
			profilePairs[iProf] |= MD::getNucleotidesPairs()->stringToCodedIdPairs(profile[iPairs]);
		}
		countInPairProfile[iProf].assign(profilePairs[iProf].count(), 0);
	}

	/// Count the conflicts and define the variance in pair
	// For each species
	std::vector<double> conflictsCount(ppi.profilesId.size(), 0.);
	for(mapSitesCode_t::const_iterator it = mapSitesCode.begin(); it != mapSitesCode.end(); ++it) {
		assert(pos1 < it->second.size() && pos2 < it->second.size());

		// Get single position code
		MD::bitset16b_t nuclCodePos1 = it->second[pos1];
		MD::bitset16b_t nuclCodePos2 = it->second[pos2];

		double cntNCP1 = nuclCodePos1.count();
		double cntNCP2 = nuclCodePos2.count();
		double weight = 1./((double)cntNCP1*(double)cntNCP2);

		// For each possible pairs --> this is required because of ambiguous nucleotides
		MD::bitset16b_t codedPairs;
		for(size_t iNucl1=0; iNucl1<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl1) {
			for(size_t iNucl2=0; iNucl2<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl2) {
				// If the pair is observable
				size_t pairCode = iNucl1*MD::getNucleotides()->NB_BASE_NUCL + iNucl2;
				codedPairs[pairCode] = nuclCodePos1[iNucl1] && nuclCodePos2[iNucl2];
			}
		}

		// Check if it is present in the profiles and count pair occurrences
		for(size_t iProf=0; iProf<profilePairs.size(); ++iProf) { // Profile loop
			MD::bitset16b_t inProfile = codedPairs & profilePairs[iProf];

			MD::bitset16b_t conflicting = (~inProfile & codedPairs);
			conflictsCount[iProf] += conflicting.count()*weight;

			if(!inProfile.any()) continue;
			size_t iCnt = 0;
			for(size_t iPair=0; iPair < inProfile.size(); ++iPair) {
				if(inProfile[iPair]) {
					countInPairProfile[iProf][iCnt] += weight;
				}

				if(profilePairs[iProf][iPair]) iCnt++;
			}
		}
	}

	computePairProfileScores(ppi, conflictsCount, countInPairProfile);
}

void CoevAlignementManager::computePairProfileScores(PairProfileInfo &ppi, const std::vector<double> conflictsCount,
		                                             const std::vector< std::vector<double> > &countInPairProfile) const {
	/// Compute scores and probabilities
	// Profile prob
	// We want to favorize the element having the smallest amount of conflict
	/* OLD WAY
	// We take the probability as p(profile) = 1+maxConflict-countedConflict(profile)
	// The element having the most conflict will have a probability of 1/totalConflicts, etc.
	double sumConflicts = 0;
	for(size_t iP=0; iP<ppi.profilesId.size(); ++iP) {
		sumConflicts += conflictsCount[iP];
	}
	// Correct the sum
	//std::cout << "Sum of conflicts : " << sumConflicts << std::endl; // FIXME
	sumConflicts = (ppi.profilesId.size()*(1 + mapSitesCode.size()))-sumConflicts;

	// Define and normalize the probabilities
	double minConflict = mapSitesCode.size();
	for(size_t iP=0; iP<ppi.profilesId.size(); ++iP) {
		double profileProba = (double)((mapSitesCode.size()+1)-conflictsCount[iP])/(double)sumConflicts;
		ppi.profileProb.push_back(profileProba);
		minConflict = std::min(minConflict, conflictsCount[iP]);
		assert(profileProba > 0.0);
	}*/

	// NEW WAY
	double sumProb = 0.;
	double minConflict = mapSitesCode.size();
	for(size_t iP=0; iP<ppi.profilesId.size(); ++iP) {
		double scoreProb = scaleWithWithOneMinusLog(std::max(conflictsCount[iP], MIN_ACCEPTABLE_CONFLICTS), MIN_ACCEPTABLE_CONFLICTS, mapSitesCode.size(), MIN_SCORE);
		scoreProb = pow(scoreProb, 1.5);
		ppi.profileProb.push_back(scoreProb);
		sumProb += scoreProb;
		assert(scoreProb > 0.0);

		minConflict = std::min(minConflict, conflictsCount[iP]);
	}

	// Normalize
	for(size_t iP=0; iP<ppi.profileProb.size(); ++iP) {
		ppi.profileProb[iP] /= sumProb;
	}

	//*************************** TEST *********************************/
	/*double sumProb = 0., sumScoreProb = 0.;
	std::vector<double> scoreProb;
	for(size_t iP=0; iP<ppi.profilesId.size(); ++iP) {
		sumProb += ppi.profileProb[iP];
		scoreProb.push_back(scaleWithWithOneMinusLog(std::max(conflictsCount[iP], MIN_ACCEPTABLE_CONFLICTS), MIN_ACCEPTABLE_CONFLICTS, mapSitesCode.size(), MIN_SCORE));
		scoreProb.back() = pow(scoreProb.back(), 2.);
		sumScoreProb += scoreProb.back();
	}

	std::cout << "Sum prob = " << sumProb << std::endl;
	for(size_t iP=0; iP<ppi.profilesId.size(); ++iP) {
		std::cout << std::fixed << ppi.profilesId[iP] << " : " <<  std::fixed <<  conflictsCount[iP];
		std::cout << "\tprob = " <<  std::fixed << ppi.profileProb[iP]/sumProb; // FIXME DEBUG
		std::cout << " vs score : " <<  std::fixed <<  scoreProb[iP]/sumScoreProb << std::endl; // FIXME DEBUG
	}*/
	//*************************** END TEST *********************************/


	/// Score based on the number of observed profile
	const size_t MAX_ESTIMATED_PROFILES = MD::getNucleotidesPairProfiles()->NB_POSSIBLE_PROFILES;
	double profileNumberScore = scaleWithWithOneMinusLog(ppi.profilesId.size(), 1., MAX_ESTIMATED_PROFILES, MIN_SCORE);
	//std::cout << "\t profileNumberScore : " << profileNumberScore; // FIXME
	profileNumberScore = pow(profileNumberScore, PROFILE_NUMBER_SCORE_WEIGHT);
	//std::cout << "\t  power of profileNumberScore : " << profileNumberScore << std::endl; // FIXME

	/// Scores based on the less conflicting profile
	/// a) Less amount of observed conflicts
	double boundedMinConflicts = std::max(minConflict, MIN_ACCEPTABLE_CONFLICTS);
	double minConflictScore = scaleWithWithOneMinusLog(boundedMinConflicts, MIN_ACCEPTABLE_CONFLICTS, mapSitesCode.size(), MIN_SCORE);
	//std::cout << "\t minConflictScore : " << minConflictScore; // FIXME
	minConflictScore = pow(minConflictScore, MIN_CONFLICTS_SCORE_WEIGHT);
	//std::cout << "\t  power of minConflictScore : " << minConflictScore << std::endl; // FIXME

	/// b) Less amount of variance between occurence of pairs in profile (remove conserved sites with few substitution)
	// Define variance
	double pairVarianceScore = 0.;
	for(size_t iProf=0; iProf<countInPairProfile.size(); ++iProf) { // Profile loop
		if(conflictsCount[iProf] == minConflict) {
			boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::variance> > accCount;
			for(size_t iPairs=0; iPairs<countInPairProfile[iProf].size(); ++iPairs) { // Pairs loop
				accCount(countInPairProfile[iProf][iPairs]);
			}
			double pairVariance = boost::accumulators::variance(accCount);
			double boundedPairVariance = std::min(pairVariance, MAX_VARIANCE);
			pairVarianceScore = scaleWithWithOneMinusLog(boundedPairVariance, 0., MAX_VARIANCE, MIN_SCORE);
			break;
		}
	}
	//std::cout << "\t pairVarianceScore : " << pairVarianceScore; // FIXME
	pairVarianceScore = pow(pairVarianceScore, PAIR_VARIANCE_SCORE_WEIGHT);
	//std::cout << "\t  power of pairVarianceScore : " << pairVarianceScore << std::endl; // FIXME

	ppi.score = profileNumberScore*minConflictScore*pairVarianceScore;
	assert(ppi.score > 0.0);
}


double CoevAlignementManager::scaleWithWithOneMinusLog(double value, double minValue, double maxValue, double minScaledValue) const {
	// With m=minValue, M=maxValue, s=minScaledValue, we want:
	// 0. = x*log(y+m) and minScaledValue = x*log(y+M)
	// Wich gives y = exp(0)-m and x=s/log(M+exp(0)-m) ==> exp(0)=1
	double y = 1.-minValue;
	double x = (1.-minScaledValue)/log(maxValue+1-minValue);

	// Then we take 1-x*log(y+value) as scaled result
	return 1.-x*log(y+value);
}


std::pair<position_t, position_t> CoevAlignementManager::linearToTriangularUpperCoordinate(size_t linearPosition) const {
	std::pair<position_t, position_t> triPositions;

	triPositions.first = N_SITE - 2 - floor(sqrt(-8*linearPosition + 4*N_SITE*(N_SITE-1)-7)/2.0 - 0.5);
	triPositions.second = linearPosition + triPositions.first + 1 - N_ORDERED_PAIRS + (N_SITE-triPositions.first)*((N_SITE-triPositions.first)-1)/2;

	assert(triPositions.first < triPositions.second);
	assert(triPositions.first < N_SITE);
	assert(triPositions.second < N_SITE);

	return triPositions;
}

size_t CoevAlignementManager::triangularUpperToLinearCoordinate(std::pair<position_t, position_t> triPositions) const {
	position_t i = triPositions.first;
	position_t j = triPositions.second;
	assert(i < j);
	size_t linearPosition = N_ORDERED_PAIRS - (N_SITE-i)*((N_SITE-i)-1)/2 + j - i - 1;
	assert(linearPosition < N_ORDERED_PAIRS);

	return linearPosition;
}

void CoevAlignementManager::DBG_CHECK_SCORES_FOR_16SRNA() const {
	size_t pos1[29] = {1077, 282, 1157, 766, 276, 1165, 1091, 1076, 619, 681, 765, 1078, 2352, 2498, 2472, 2308, 1830, 1158, 1953, 628, 2241, 2241, 634, 623, 2499, 2516, 1075, 2241, 671};
	size_t pos2[29] = {1121, 608, 1925, 793, 610, 1917, 1116, 1122, 623, 700, 794, 1120, 3198, 2507, 3186, 3210, 1847, 1924, 1972, 662, 2546, 2297, 660, 671, 2506, 2523, 1123, 3163, 673};

	score_t minScore = 1.;
	score_t maxScore = 0.;
	score_t avgScore = 0.;
	for(size_t i=0; i<100000; ++i) {
		minScore = std::min(minScore, vecPairsProfilesInfo[i].score);
		maxScore = std::max(maxScore, vecPairsProfilesInfo[i].score);
		avgScore += vecPairsProfilesInfo[i].score;
	}
	avgScore /= 100000.0;


	std::cout << "[CoevAlignementManager][DBG_CHECK_SCORES_FOR_16SRNA]" << std::endl;
	std::cout << "[CoevAlignementManager][DBG] minScore = " << minScore << "\t maxScore = " << maxScore << "\t avgScore = " << avgScore << std::endl;

	std::cout << "[CoevAlignementManager][DBG] Score for the best 29th with Coev : " << std::endl;
	std::cout << "pos1 \t pos2 \t score" << std::endl;
	for(size_t i=0; i<29; ++i) {
		// Pos are 1 indexed, thus -1
		size_t linCoord = triangularUpperToLinearCoordinate(std::make_pair(pos1[i]-1, pos2[i]-1));
		std::cout << "p1 = " << pos1[i]-1 << "\t p1Score = " << std::scientific << positionsScore[pos1[i]-1];
		std::cout << "\t p2 = " << pos2[i]-1 << "\t p2Score = " << std::scientific << positionsScore[pos2[i]-1];
		std::cout <<"\t score = " << std::scientific << vecPairsProfilesInfo[linCoord].score << std::endl;
		for(size_t iP=0; iP<vecPairsProfilesInfo[linCoord].profilesId.size(); ++iP) {
			MD::profile_t prof = MD::getNucleotidesPairProfiles()->idToProfile(vecPairsProfilesInfo[linCoord].profilesId[iP]);
			std::cout << vecPairsProfilesInfo[linCoord].profilesId[iP] << "\t [ ";
			for(size_t iPair=0; iPair < prof.size(); ++iPair) {
				std::cout << prof[iPair] << ", ";
			}
			std::cout << " ] \t " << vecPairsProfilesInfo[linCoord].profileProb[iP] << std::endl;

		}
		std::cout << std::endl;
		std::cout << "--------------------------------------------------------------------" << std::endl;
	}
}

void CoevAlignementManager::DBG_COMPUTE_SCORES_FOR_16SRNA() const {
	size_t pos1[35] = {1077, 282, 1157, 766, 276, 1165, 1091, 1076, 619, 681, 765, 1078, 2352, 2498, 2472, 2308, 1830, 1158, 1953, 628, 2241, 2241, 634, 623, 2499, 2516, 1075, 2241, 671, 123, 2, 89, 60, 70, 2403};
	size_t pos2[35] = {1121, 608, 1925, 793, 610, 1917, 1116, 1122, 623, 700, 794, 1120, 3198, 2507, 3186, 3210, 1847, 1924, 1972, 662, 2546, 2297, 660, 671, 2506, 2523, 1123, 3163, 673, 3204, 1801, 900, 66, 3490, 3523};

	std::cout <<  "*************************************************************" << std::endl;
	std::cout <<  "******************      OLD POSITIONS      ******************" << std::endl;
	std::cout <<  "*************************************************************" << std::endl;
	for(size_t iP = 0; iP < 35; ++iP) {

		size_t iPos1 = pos1[iP]-1;
		size_t iPos2 = pos2[iP]-1;

		//size_t linearCoord = triangularUpperToLinearCoordinate(std::make_pair(iPos1, iPos2));
		// For each pair of positions
		std::pair<position_t, position_t> positions = std::make_pair(iPos1, iPos2);

		// Get possible profiles ID
		MD::bitset16b_t observedPairs = defineObservablePairs(iPos1, iPos2, SKIP_NON_INFORMATIVE_PAIRS);

		PairProfileInfo ppi;

		if(observedPairs.count() == 16) {
			ppi.uniform = USE_UNIFORM_WHEN_ALL_PROFILE_OBSERVED;
			ppi.profilesId = FULL_PROFILE_ID;
		} else {
			ppi.uniform = false;
			ppi.profilesId = MD::getNucleotidesPairProfiles()->getPossibleProfilesId(observedPairs);
		}

		// Define the score of the pair as well as the profile conflict count
		definePairProfilesInformation(iPos1, iPos2, ppi);
		assert(ppi.profilesId.size() == ppi.profileProb.size());
		// If we are empty we have the a score of 0.
		assert(!ppi.profilesId.empty() || (ppi.profilesId.empty() && ppi.score == 0.));

		/*if(ppi.profilesId.empty()) {
			std::cout << iPos1 << "\t" << iPos2 << "\t" << ppi.score << "\t" << observedPairs.to_string() << std::endl;
		}*/

		std::cout << iPos1 << "\t" << iPos2 << "\tNProfile = " << ppi.profilesId.size() << "\tscore : " << ppi.score << std::endl;
		for(size_t i=0; i<ppi.profilesId.size(); ++i) {
			std::cout << ppi.profilesId[i] << ", ";
		}
		std::cout << std::endl;
		for(size_t i=0; i<ppi.profileProb.size(); ++i) {
			std::cout << ppi.profileProb[i] << ", ";
		}
		std::cout << std::endl << "--------------------------------------------------------------------" << std::endl;
	}


	std::cout <<  "*************************************************************" << std::endl;
	std::cout <<  "******************      NEW POSITIONS      ******************" << std::endl;
	std::cout <<  "*************************************************************" << std::endl;


	std::vector<size_t> newPos1 = boost::assign::list_of(48)(75)(86)(87)(270)(271)(275)(281)(282)(289)(291)(317)(319)(328)(331)(332)(333)(351)(372)(373)(460)(475)(617)(618)(627)(633)(634)(639)(640)(678)(680)(684)(685)(706)(710)(764)(765)(766)(773)(775)(812);
	std::vector<size_t> newPos2 = boost::assign::list_of(2263)(800)(754)(753)(614)(613)(609)(607)(606)(601)(599)(357)(355)(345)(342)(341)(340)(1165)(442)(441)(1183)(511)(673)(672)(661)(659)(655)(650)(649)(701)(699)(693)(692)(728)(724)(793)(792)(791)(787)(785)(858);
	assert(newPos1.size() == newPos2.size());

	for(size_t iP = 0; iP < 35; ++iP) {

		size_t iPos1 = newPos1[iP];
		size_t iPos2 = newPos2[iP];

		//size_t linearCoord = triangularUpperToLinearCoordinate(std::make_pair(iPos1, iPos2));
		// For each pair of positions
		std::pair<position_t, position_t> positions = std::make_pair(iPos1, iPos2);

		// Get possible profiles ID
		MD::bitset16b_t observedPairs = defineObservablePairs(iPos1, iPos2, SKIP_NON_INFORMATIVE_PAIRS);

		PairProfileInfo ppi;

		if(observedPairs.count() == 16) {
			ppi.uniform = USE_UNIFORM_WHEN_ALL_PROFILE_OBSERVED;
			ppi.profilesId = FULL_PROFILE_ID;
		} else {
			ppi.uniform = false;
			ppi.profilesId = MD::getNucleotidesPairProfiles()->getPossibleProfilesId(observedPairs);
		}

		// Define the score of the pair as well as the profile conflict count
		definePairProfilesInformation(iPos1, iPos2, ppi);
		assert(ppi.profilesId.size() == ppi.profileProb.size());
		// If we are empty we have the a score of 0.
		assert(!ppi.profilesId.empty() || (ppi.profilesId.empty() && ppi.score == 0.));

		/*if(ppi.profilesId.empty()) {
			std::cout << iPos1 << "\t" << iPos2 << "\t" << ppi.score << "\t" << observedPairs.to_string() << std::endl;
		}*/

		std::cout << iPos1 << "\t" << iPos2 << "\tNProfile = " << ppi.profilesId.size() << "\tscore : " << ppi.score << std::endl;
		for(size_t i=0; i<ppi.profilesId.size(); ++i) {
			std::cout << ppi.profilesId[i] << ", ";
		}
		std::cout << std::endl;
		for(size_t i=0; i<ppi.profileProb.size(); ++i) {
			std::cout << ppi.profileProb[i] << ", ";
		}
		std::cout << std::endl << "--------------------------------------------------------------------" << std::endl;
	}

	// DEBUG FIXME
	/*if(ppi.score > 0. || ppi.profilesId.size() > 0) {
		if(ppi.score > 1e-9) std::cout << "G\t";
		if(ppi.score > 1e-7) std::cout << "GG\t";
		if(ppi.score > 1e-5) std::cout << "GGG\t";
		std::cout << iPos1 << "\t" << iPos2 << "\tNProfile = " << ppi.profilesId.size() << "\tscore : " << ppi.score << std::endl;

	}*/
	// END DEBUG FIXME
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
