//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevTraceWriter.h
 *
 * @date Apr 19, 2017
 * @author meyerx
 * @brief
 */
#ifndef COEVTRACEWRITER_H_
#define COEVTRACEWRITER_H_

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <stddef.h>
#include <string>
#include <vector>

#include "../PermCoevLikelihoodInfo.h"
#include "Sampler/Samples/Sample.h"
#include "Utils/Code/CheckpointFile.h"
#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/stats.hpp>

#include "Model/Likelihood/CoevRJ/Nodes/Types.h"

namespace boost { namespace accumulators { namespace tag { struct variance; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class CoevTraceWriter {
public:
	enum enumProfileTrace {NUMERICAL, CATEGORICAL};
	typedef enumProfileTrace profileTrace_t;

public:
	CoevTraceWriter(bool areCoevPositionsFixed);
	~CoevTraceWriter();

	void initFile(const std::string &fnPrefix, const std::string &fnSuffix, std::streamoff offset);
	void initCoev(listPCLI_t& coevInformations);

	std::streamoff getFileLength() const;
	void writeSamples(listPCLI_t& coevInformations,
					  const Sampler::SampleUtils::vecPairItSample_t &iterSamples);

	void setNSite(size_t aNSite);

private:

	const bool COEV_POS_FIXED;
	size_t N_SITE;
	profileTrace_t profileTrace;

	std::string coevStringFN, prefixFN, suffixFN;

	Utils::Checkpoint::File::sharedPtr_t oFile;

	void initCoevRJ();
	void initCoevFixed(listPCLI_t& coevInformations);

	void writeSamplesCoevFixed(listPCLI_t& coevInformations,
					          const Sampler::SampleUtils::vecPairItSample_t &iterSamples);

	void writeSamplesCoevRJ(listPCLI_t& coevInformations,
					    const Sampler::SampleUtils::vecPairItSample_t &iterSamples);

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COEVTRACEWRITER_H_ */
