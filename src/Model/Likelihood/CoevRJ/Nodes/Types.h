//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Types.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef TYPES_COEVRJ_H_
#define TYPES_COEVRJ_H_

#include <stddef.h>
#include <assert.h>
#include <bitset>

#include "Eigen/Core"
#include "Eigen/Dense"

#include "Utils/MolecularEvolution/Definitions/MolecularDefIncl.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

#define TI_COEV_NSYMBOLS 			16
#define TI_GTR_NSYMBOLS 			4
#define TI_COEV_USE_FLOAT 			0
#if TI_COEV_USE_FLOAT

typedef Eigen::Matrix<float, TI_COEV_NSYMBOLS, TI_COEV_NSYMBOLS, Eigen::ColMajor> TI_Coev_EigenSquareMatrix_t;
typedef Eigen::Matrix<float, TI_COEV_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_Coev_EigenMatrix_t;

typedef Eigen::Matrix<float, TI_GTR_NSYMBOLS, TI_GTR_NSYMBOLS, Eigen::ColMajor> TI_GTR_EigenSquareMatrix_t;
typedef Eigen::Matrix<float, TI_GTR_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_GTR_EigenMatrix_t;

typedef Eigen::MatrixXf TI_EigenMatrixDyn_t;
typedef Eigen::VectorXf TI_EigenVector_t;
typedef Eigen::ArrayXf TI_EigenArray_t;
#define TI_COEV_TYPE				float
#else
typedef Eigen::Matrix<double, TI_COEV_NSYMBOLS, TI_COEV_NSYMBOLS, Eigen::ColMajor> TI_Coev_EigenSquareMatrix_t;
typedef Eigen::Matrix<double, TI_COEV_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_Coev_EigenMatrix_t;

typedef Eigen::Matrix<double, TI_GTR_NSYMBOLS, TI_GTR_NSYMBOLS, Eigen::ColMajor> TI_GTR_EigenSquareMatrix_t;
typedef Eigen::Matrix<double, TI_GTR_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_GTR_EigenMatrix_t;

typedef Eigen::MatrixXd TI_EigenMatrixDyn_t;
typedef Eigen::VectorXd TI_EigenVector_t;
typedef Eigen::ArrayXd TI_EigenArray_t;
#define TI_COEV_TYPE					double
#endif

typedef Eigen::VectorXd TI_EigenEIGVector_t;
typedef Eigen::ArrayXd TI_EigenEIGArray_t;
typedef Eigen::Matrix<double, TI_COEV_NSYMBOLS, TI_COEV_NSYMBOLS, Eigen::ColMajor> TI_Coev_EigenEIGMatrix_t;
typedef Eigen::Matrix<double, TI_GTR_NSYMBOLS, TI_GTR_NSYMBOLS, Eigen::ColMajor> TI_GTR_EigenEIGMatrix_t;

static const TI_COEV_TYPE LOG_OF_2 = log(2.);

enum scalingType_t {NO_SCALING=0, LOCAL_SCALING=1, GLOBAL_SCALING=2};
typedef enum {NO_SCALING_COEV=0, WITH_SCALING_COEV=1} coevScalingType_t;
typedef enum {GAMMA_PR=0, DIRICHLET_PR=1, GAMMA_HP=2, DIRICHLET_HP=3, GAMMA_NORMAL_HP=4, GAMMA2_HP=5, SINGLE_PARAM=6} coevPriorScalingType_t;

typedef ::MolecularEvolution::Definition::bitset16b_t usintBitset_t;
typedef std::pair<bool, usintBitset_t> nuclCodeQuery_t;

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TYPES_COEVRJ_H_ */
