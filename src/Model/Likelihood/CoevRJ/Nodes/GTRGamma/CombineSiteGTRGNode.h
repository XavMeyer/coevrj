//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteGTRGNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef COMBINESITEGTRGNODE_COEVRJ_H_
#define COMBINESITEGTRGNODE_COEVRJ_H_

#include <stddef.h>
#include <vector>

#include "../Types.h"
#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Node/Base/LikelihoodNode.h"
#include "RootGTRGNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

namespace DL = ::MolecularEvolution::DataLoader;

class CombineSiteGTRGNode: public DAG::BaseNode, public DAG::LikelihoodNode {
public:
	CombineSiteGTRGNode(const size_t aNGamma, const std::vector<size_t> &aSites);
	~CombineSiteGTRGNode();

	std::string toString() const;

	bool enableSites(std::vector<size_t> &sitesToEnable);
	bool disableSites(std::vector<size_t> &sitesToDisable);

	const TI_EigenVector_t& getVectorLogLikGTR() const;

private:

	struct SiteMapping {
		bool represented;
		size_t site, position;

		SiteMapping(bool aReprsented, size_t aSite, size_t aPosition) :
			represented(aReprsented), site(aSite), position(aPosition) {
		}
		~SiteMapping(){}

		void setRepresented(bool aRepr) { represented = aRepr; }
		bool isRepresented() const { return represented;}
		bool operator== (const SiteMapping &aPLS) const {
			return aPLS.site == site;
		}
	};

	typedef std::vector<SiteMapping> vecSM_t;
	typedef vecSM_t::iterator itVecSM_t;
	vecSM_t sitesMapping;

	static const TI_COEV_TYPE MIN_PROBABILITY;
	static const TI_COEV_TYPE LOG_MIN, WORST_VAL;
	static const size_t NOT_REPRESENTED;

	const size_t N_GAMMA, nSubSite;
	std::vector<RootGTRGNode*> rootNodes;

	double hashVal;
	std::vector<size_t> sites;
	TI_EigenVector_t allSitesLogLik, siteLogLik;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

	void combineSite();
	void combineSiteWithLogScaling();

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COMBINESITEGTRGNODE_COEVRJ_H_ */
