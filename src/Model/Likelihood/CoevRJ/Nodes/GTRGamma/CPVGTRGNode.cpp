//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPVGTRGNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "CPVGTRGNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/GTRGamma/../Types.h"
#include "Model/Likelihood/CoevRJ/Nodes/GTRGamma/BranchMatrixGTRGNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/GTRGamma/EdgeGTRGNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

CPVGTRGNode::CPVGTRGNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
				 DL_Utils::Frequencies &aFrequencies) :
		ParentGTRGNode(aIDClass, aSitePos, aFrequencies) {

}

CPVGTRGNode::~CPVGTRGNode() {
}

void CPVGTRGNode::doProcessing() {

	nrm.resize(0);

	TI_GTR_EigenMatrix_t G;

	const TI_GTR_EigenSquareMatrix_t &Z = bmNode->getZ();
#if TI_COEV_USE_FLOAT
	const TI_EigenVector_t &invPi = frequencies.getInvEigenFlt();
#else
	const TI_EigenVector_t &invPi = frequencies.getInvEigen();
#endif

	// Prepare G
	bool first = true;
	// Check block at first
	for(size_t iE=0; iE<children.size(); ++iE) {
		if(iE == 0) {
			G = children[iE]->getH();
			first = false;
			if(SCALING_TYPE == LOG  && children[iE]->getNorm().size() > 0) {
				nrm = children[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(children[iE]->getH());
			if (SCALING_TYPE == LOG && children[iE]->getNorm().size() > 0) {
				if(nrm.size() == 0) {
					nrm = children[iE]->getNorm();
				} else {
					nrm += children[iE]->getNorm();
				}
			}
		}
	}

	// Multiplication
	H = invPi.asDiagonal() * (Z.selfadjointView<Eigen::Lower>() * G);

	if(SCALING_TYPE == LOG) {
		TI_EigenVector_t vecMax = H.colwise().maxCoeff();
		if(SCALING_THRESHOLD >= 1. || vecMax.minCoeff() < SCALING_THRESHOLD) {
			if(nrm.size() == 0) {
				nrm = TI_EigenVector_t::Zero(vecMax.size());
			}
			for(size_t iC=0; iC<(size_t)vecMax.size(); ++iC){
				if(vecMax(iC) > 0 && (SCALING_THRESHOLD >= 1. || vecMax(iC) < SCALING_THRESHOLD)){
					int exponent;
					frexp(vecMax(iC), &exponent);
					nrm(iC) += exponent*LOG_OF_2;
					H.col(iC) /= ldexp((TI_COEV_TYPE)1., exponent); // TODO CHECK IF CORRECT
				}
			}
		}
	}

	/*std::cout << "CPV : t=" << bmNode->getBranchLength() << "\t NodeId=" << getId() << std::endl;
	std::cout << "G:" << std::endl << G.col(0) << std::endl;
	std::cout << "P:" << std::endl <<  (invPi.asDiagonal() * Z) << std::endl;
	std::cout << "H:" << std::endl <<  H.col(0) << std::endl;
	std::cout << "-----------------------------------------------" << std::endl;
	if(H.col(0).sum() < 4) {
		getchar();
	}*/

}

bool CPVGTRGNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void CPVGTRGNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixGTRGNode)){
		bmNode = dynamic_cast<BranchMatrixGTRGNode*>(aChild);
		if(bmNode->getIDGamma() != idGamma){
			std::cerr << "void CPVGTRGNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild);" << std::endl;
			std::cerr << "Error : Invalid BranchMatrixGTRGNode children. Found idGamma = " << bmNode->getIDGamma();
			std::cerr << " while expecting idGamma = " << idGamma << std::endl;
			abort();
		}
	} else if(childtype == typeid(CPVGTRGNode)){
		CPVGTRGNode* cpvNode = dynamic_cast<CPVGTRGNode*>(aChild);
		addChild(cpvNode);
	} else if(childtype == typeid(LeafGTRGNode)){
		LeafGTRGNode* leafNode = dynamic_cast<LeafGTRGNode*>(aChild);
		addChild(leafNode);
	}
}

void CPVGTRGNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVGTRGNode)){
		CPVGTRGNode* cpvNode = dynamic_cast<CPVGTRGNode*>(aChild);
		removeChild(cpvNode);
	} else if(childtype == typeid(LeafGTRGNode)){
		LeafGTRGNode* leafNode = dynamic_cast<LeafGTRGNode*>(aChild);
		removeChild(leafNode);
	}
}

std::string CPVGTRGNode::toString() const {
	std::stringstream ss;
	ss << "[CPVGTRGNode] " << BaseNode::toString();
	return ss.str();
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
