//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchMatrixGTRGNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "BranchMatrixGTRGNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/GTRGamma/../Types.h"
#include "Model/Likelihood/CoevRJ/Nodes/GTRGamma/DiscreteGammaNode.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

BranchMatrixGTRGNode::BranchMatrixGTRGNode(const size_t aIDGamma, const scalingType_t aScalingType,
								   DL_Utils::Frequencies &aFrequencies) :
		BaseNode(), idGamma(aIDGamma), scalingType(aScalingType), frequencies(aFrequencies),
		Z(frequencies.size(), frequencies.size()) {

	branchLength = 0;
	matrixNode = NULL;
	discreteGammaNode = NULL;

}

BranchMatrixGTRGNode::~BranchMatrixGTRGNode() {
}

bool BranchMatrixGTRGNode::setBranchLength(const double aBL) {
	if(branchLength != aBL) {
		branchLength = aBL;
		BaseNode::updated();
		return true;
	} else {
		return false;
	}
}

double BranchMatrixGTRGNode::getBranchLength() const {
	return branchLength;
}

size_t BranchMatrixGTRGNode::getIDGamma() const {
	return idGamma;
}

const TI_GTR_EigenSquareMatrix_t& BranchMatrixGTRGNode::getZ() const {
	return Z;
}

void BranchMatrixGTRGNode::doProcessing() {

	// Shortcuts
	const TI_EigenEIGVector_t &D = matrixNode->getEigenValues();
	const TI_GTR_EigenEIGMatrix_t &scaledV = matrixNode->getScaledEigenVectors();

	// Process number of substitutions
	double gammaRate = discreteGammaNode->getRate(idGamma);
	double t = gammaRate*branchLength;
	if(scalingType == GLOBAL_SCALING) {
		t /= matrixNode->getMatrixQScaling();
	} else if(scalingType == LOCAL_SCALING){
		t /= matrixNode->getMatrixQScaling();
	} else { // NONE
		// Do nothing
	}

	// Processing Y
	TI_EigenEIGArray_t dt = (t/2.0) * D.array();
	dt = dt.exp();
	TI_EigenEIGVector_t dt2 = dt.matrix();

	TI_GTR_EigenEIGMatrix_t Y = scaledV * dt2.asDiagonal();
	//Z.setZero();
#if TI_COEV_USE_FLOAT
	TI_GTR_EigenEIGMatrix_t tmpZ = Y*Y.transpose();
	Z.triangularView<Eigen::Lower>() = tmpZ.cast<float>();
#else
	Z.triangularView<Eigen::Lower>() = Y*Y.transpose();
#endif

	Z.eval();
}

bool BranchMatrixGTRGNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void BranchMatrixGTRGNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixGTRNode)){
		matrixNode = dynamic_cast<MatrixGTRNode*>(aChild);
	} else if(childtype == typeid(DiscreteGammaNode)){
		discreteGammaNode = dynamic_cast<DiscreteGammaNode*>(aChild);
	}
}

std::string BranchMatrixGTRGNode::toString() const {
	std::stringstream ss;
	ss << "[BranchMatrixGTRGNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
