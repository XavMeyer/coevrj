//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ParentGTRGNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef PARENTGTRGNODE_COEVRJ_H_
#define PARENTGTRGNODE_COEVRJ_H_

#include <stddef.h>

#include "EdgeGTRGNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class ParentGTRGNode : public EdgeGTRGNode {
public:
	ParentGTRGNode(const size_t aIDGamma, const std::vector<size_t> &aSitePos,
			   DL_Utils::Frequencies &aFrequencies);
	virtual ~ParentGTRGNode();

	std::string toString() const;

protected: // Variables

	std::vector<EdgeGTRGNode*> children;

protected: // Methods
	void addChild(EdgeGTRGNode* childNode);
	bool removeChild(EdgeGTRGNode* childNode);

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PARENTGTRGNODE_COEVRJ_H_ */
