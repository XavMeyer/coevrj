//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteGTRGNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "CombineSiteGTRGNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/GTRGamma/../Types.h"

#define LOG_PLUS(x,y) (x>y ? x+log(1.+exp(y-x)) : y+log(1.+exp(x-y)))

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

const TI_COEV_TYPE CombineSiteGTRGNode::MIN_PROBABILITY = 1e-300;
const TI_COEV_TYPE CombineSiteGTRGNode::LOG_MIN = log(std::numeric_limits<TI_COEV_TYPE>::min());
const TI_COEV_TYPE CombineSiteGTRGNode::WORST_VAL = -std::numeric_limits<TI_COEV_TYPE>::max();
const size_t CombineSiteGTRGNode::NOT_REPRESENTED = std::numeric_limits<size_t>::max();

CombineSiteGTRGNode::CombineSiteGTRGNode(const size_t aNGamma, const std::vector<size_t> &aSites) :
		DAG::BaseNode(), N_GAMMA(aNGamma), nSubSite(aSites.size()),
		rootNodes(N_GAMMA, NULL), sites(aSites) {

	for(size_t iS=0; iS<sites.size(); ++iS) {
		sitesMapping.push_back(SiteMapping(true, sites[iS], iS));
	}

	siteLogLik.setZero(sites.size());
	allSitesLogLik.setZero(sites.size());

	hashVal = 0.;

}

CombineSiteGTRGNode::~CombineSiteGTRGNode() {
}

bool CombineSiteGTRGNode::enableSites(std::vector<size_t> &sitesToEnable) {
	SiteMapping sm(false, 0, 0);
	for(size_t iS=0; iS<sitesToEnable.size(); ++iS) {
		sm.site = sitesToEnable[iS];
		itVecSM_t it = std::find(sitesMapping.begin(), sitesMapping.end(), sm);

		if(it != sitesMapping.end()) {
			it->setRepresented(true);
			DAG::BaseNode::updated();
		}
	}
	return isUpdated();
}

bool CombineSiteGTRGNode::disableSites(std::vector<size_t> &sitesToDisable) {
	SiteMapping sm(true, 0, 0);
	for(size_t iS=0; iS<sitesToDisable.size(); ++iS) {
		sm.site = sitesToDisable[iS];
		itVecSM_t it = std::find(sitesMapping.begin(), sitesMapping.end(), sm);

		if(it != sitesMapping.end()) {
			it->setRepresented(false);
			DAG::BaseNode::updated();
		}
	}
	return isUpdated();
}

const TI_EigenVector_t& CombineSiteGTRGNode::getVectorLogLikGTR() const {
	return allSitesLogLik;
}

void CombineSiteGTRGNode::combineSite() {
	// LOG_LIKELIHOOD
	likelihood = 0.;

	if(N_GAMMA == 1) {
		TI_EigenVector_t siteLik = rootNodes[0]->getLikelihood();
		siteLogLik = siteLik.array().log();
		for(size_t iS=0; iS < sitesMapping.size(); ++iS) {
			// else we search for his value
			size_t iPos = sitesMapping[iS].position;
			double res = siteLogLik(iPos);
			if(res == -std::numeric_limits<TI_COEV_TYPE>::infinity()) {
				res = WORST_VAL;
			} else if(rootNodes[0]->getLikelihood()(iPos) < MIN_PROBABILITY) {
				res = WORST_VAL;
			}

			allSitesLogLik[sitesMapping[iS].position] = res;

			// If represented, we take this site into account
			if(sitesMapping[iS].isRepresented()) {
				likelihood += res;
			}
		}
	} else {

		// Trying to avoid some useless computations (log, etc.)
		double newHashVal = rootNodes[0]->getLikelihood().sum();
		if(newHashVal != hashVal) {
			hashVal = newHashVal;

			// Compute new site lik
			TI_EigenVector_t siteLik = rootNodes[0]->getLikelihood();
			for(size_t iG=1; iG<N_GAMMA; ++iG) {
				siteLik = siteLik + rootNodes[iG]->getLikelihood();
			}
			siteLik = (siteLik/(double)N_GAMMA);

			siteLogLik = siteLik.array().log();

			for(size_t iS=0; iS < (size_t)siteLogLik.size(); ++iS) {
				size_t iPos = sitesMapping[iS].position;
				double res = siteLogLik(iPos);

				if(res == -std::numeric_limits<TI_COEV_TYPE>::infinity()) {
					res = WORST_VAL;
				}

				allSitesLogLik[sitesMapping[iS].position] = res;
			}
		}


		for(size_t iS=0; iS < sitesMapping.size(); ++iS) {
			// If represented, we take this site into account
			if(sitesMapping[iS].isRepresented()) {
				likelihood += allSitesLogLik[sitesMapping[iS].position];
			}
		}
	}
}

void CombineSiteGTRGNode::combineSiteWithLogScaling() {
	// LOG_LIKELIHOOD
	likelihood = 0.;

	if(N_GAMMA == 1) {
		const TI_EigenVector_t &logNorm = rootNodes[0]->getNorm();
		TI_EigenArray_t siteLogLik = (rootNodes[0]->getLikelihood().array().log() + logNorm.array());
		for(size_t iS=0; iS < sitesMapping.size(); ++iS) {
			// If not represented, we do not take this site into account
			//if(!sitesMapping[iS].isRepresented()) continue;
			// else we search for his value
			size_t iPos = sitesMapping[iS].position;
			double res = siteLogLik(iPos);
			if(res == -std::numeric_limits<TI_COEV_TYPE>::infinity()) {
				res = WORST_VAL;
			} else if(rootNodes[0]->getLikelihood()(iPos) < MIN_PROBABILITY) {
				res = WORST_VAL;
			}
			allSitesLogLik[sitesMapping[iS].position] = res;

			// If represented, we take this site into account
			if(sitesMapping[iS].isRepresented()) {
				likelihood += res;
			}
		}
	} else {
		for(size_t iS=0; iS<sitesMapping.size(); ++iS) {
			// If not represented, we do not take this site into account
			//if(!sitesMapping[iS].isRepresented()) continue;
			// else we search for his value
			size_t iPos = sitesMapping[iS].position;
			TI_COEV_TYPE logLikSite = 0;
			// For each Gamma rate
			for(size_t iG=0; iG<N_GAMMA; ++iG) {
				const TI_EigenVector_t &logNorm = rootNodes[iG]->getNorm();
				const TI_EigenVector_t &subLik = rootNodes[iG]->getLikelihood();

				TI_COEV_TYPE tmpSublik = subLik(iPos) < MIN_PROBABILITY ?  0. : subLik(iS);
				tmpSublik *= (double) 1./N_GAMMA;
				TI_COEV_TYPE tmpLogLik = log(tmpSublik) + logNorm(iS);
				if(iG == 0) {
					logLikSite = tmpLogLik;
				} else {
					// Safe sum of log value
					TI_COEV_TYPE x = logLikSite;
					TI_COEV_TYPE y = tmpLogLik;
					if(x<=y) std::swap(x,y);
					logLikSite = x + log(1.+exp(y-x));
				}
			}

			allSitesLogLik[sitesMapping[iS].position] = logLikSite;

			// If represented, we take this site into account
			if(sitesMapping[iS].isRepresented()) {
				likelihood += logLikSite;
			}
		}
	}
}


void CombineSiteGTRGNode::doProcessing() {
	if(EdgeGTRGNode::SCALING_TYPE == EdgeGTRGNode::LOG) {
		combineSiteWithLogScaling();
	} else {
		combineSite();
	}
}

bool CombineSiteGTRGNode::processSignal(BaseNode* aChild) {

	return true;
}

void CombineSiteGTRGNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(RootGTRGNode)){
		RootGTRGNode *node = dynamic_cast<RootGTRGNode*>(aChild);
		const size_t iG = node->getIDGamma();
		rootNodes[iG] = node;
	}
}

std::string CombineSiteGTRGNode::toString() const {
	std::stringstream ss;
	ss << "[CombineSiteGTRGNode] " << BaseNode::toString();
	return ss.str();
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
