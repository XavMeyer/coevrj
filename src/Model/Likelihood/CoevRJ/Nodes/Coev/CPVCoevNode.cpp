//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPVCoevNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "CPVCoevNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/Coev/../Types.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/BranchMatrixCoevNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/EdgeCoevNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/ParentCoevNode.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

CPVCoevNode::CPVCoevNode(DL_Utils::Frequencies &aFrequencies) :
		        ParentCoevNode(aFrequencies) {

	BaseNode::initAccTime(1e-6);
}

CPVCoevNode::~CPVCoevNode() {
}

void CPVCoevNode::doProcessing() {

	nrm.resize(0);

	TI_EigenVector_t G;

	const TI_Coev_EigenSquareMatrix_t &Z = bmNode->getZ();

	// Prepare G
	// Check block at first
	for(size_t iE=0; iE<children.size(); ++iE) {
		if(iE == 0) {
			G = children[iE]->getH();
			if(SCALING_TYPE == LOG  && children[iE]->getNorm().size() > 0) {
				nrm = children[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(children[iE]->getH());
			if (SCALING_TYPE == LOG && children[iE]->getNorm().size() > 0) {
				if(nrm.size() == 0) {
					nrm = children[iE]->getNorm();
				} else {
					nrm += children[iE]->getNorm();
				}
			}
		}
	}

	// Multiplication
	// The computation here is V*exp(L)*(V^-1)*G
	// We want to avoid the inversion of (V^-1) thus we compute
	// a) Using the LU decomp : V*X=G ==> X=(V^-1)*G
	TI_EigenVector_t X;
	if(bmNode->getEigenVectorLU().isInvertible()) {
		X = bmNode->getEigenVectorLU().solve(G);
	} else {
		X.setZero(frequencies.size());
	}
	// b)  Z*X ==> with Z = V*exp(L) and X = [(V^-1)*G]
	H = Z*X;

	if(SCALING_TYPE == LOG) {
		double vecMax = H.maxCoeff();
		if(SCALING_THRESHOLD >= 1. || vecMax < SCALING_THRESHOLD) {
			if(nrm.size() == 0) {
				nrm = TI_EigenVector_t::Zero(1);
			}
			if(vecMax > 0){
				int exponent;
				frexp(vecMax, &exponent);
				nrm(0) += exponent*LOG_OF_2;
				H /= ldexp((float)1., exponent);
			}
		}
	}
}

bool CPVCoevNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void CPVCoevNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixCoevNode)){
		bmNode = dynamic_cast<BranchMatrixCoevNode*>(aChild);
	} else if(childtype == typeid(CPVCoevNode)){
		CPVCoevNode* cpvNode = dynamic_cast<CPVCoevNode*>(aChild);
		addChild(cpvNode);
	} else if(childtype == typeid(LeafCoevNode)){
		LeafCoevNode* leafNode = dynamic_cast<LeafCoevNode*>(aChild);
		addChild(leafNode);
	}
}

void CPVCoevNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVCoevNode)){
		CPVCoevNode* cpvNode = dynamic_cast<CPVCoevNode*>(aChild);
		removeChild(cpvNode);
	} else if(childtype == typeid(LeafCoevNode)){
		LeafCoevNode* leafNode = dynamic_cast<LeafCoevNode*>(aChild);
		removeChild(leafNode);
	}
}

std::string CPVCoevNode::toString() const {
	std::stringstream ss;
	ss << "[CPVCoevNode] " << BaseNode::toString();
	return ss.str();
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
