//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootCoevNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "RootCoevNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/Coev/../Types.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/EdgeCoevNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/ParentCoevNode.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

RootCoevNode::RootCoevNode(DL_Utils::Frequencies &aFrequencies) :
	 			 ParentCoevNode(aFrequencies) {

	BaseNode::initAccTime(1e-6);

	active = false;
	logLikelihood = 0;
	nrm.resize(1);
	matrixNode = NULL;
}

RootCoevNode::~RootCoevNode() {
}

double RootCoevNode::getLogLikelihood() const {
	return logLikelihood;
}

bool RootCoevNode::isActive() const {
	return active;
}

void RootCoevNode::setActive(bool aActive) {
	active = aActive;
}


void RootCoevNode::doProcessing() {

	// FIXME std::cout << "ROOT [ " << getId() << "] :: H(" << H.rows() << "x" << H.cols() << ") :: G(" << G.rows() << "x" << G.cols() << ")" << std::endl;
	TI_EigenVector_t G;
	nrm.setZero();

	// Prepare G
	// Check block at first
	for(size_t iE=0; iE<children.size(); ++iE) {
		if(iE == 0) {
			G = children[iE]->getH();
			if(SCALING_TYPE == LOG  && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(children[iE]->getH());
			if (SCALING_TYPE == LOG && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		}
	}

#if TI_COEV_USE_FLOAT
	logLikelihood = log(frequencies.getEigenFlt().transpose() * G);
#else
	//likelihood = G.transpose()*frequencies.getEigen();
	Eigen::VectorXd stFreq = matrixNode->getStationaryFrequency();
	logLikelihood = log(stFreq.transpose() * G);
#endif

	if(EdgeCoevNode::SCALING_TYPE == EdgeCoevNode::LOG) {
		logLikelihood += nrm(0);
	}

	/*Eigen::IOFormat CommaInitFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "", ";");
	std::cout << std::scientific << "[TEMP] Root" << " >> G : " << std::endl <<G.format(CommaInitFmt) << std::endl;*/


	//FIXME std::cout << "ROOT [ " << getId() << "] done " << std::endl;

}

bool RootCoevNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void RootCoevNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVCoevNode)){
		CPVCoevNode* cpvNode = dynamic_cast<CPVCoevNode*>(aChild);
		addChild(cpvNode);
	} else if(childtype == typeid(LeafCoevNode)){
		LeafCoevNode* leafNode = dynamic_cast<LeafCoevNode*>(aChild);
		addChild(leafNode);
	} else if(childtype == typeid(MatrixCoevNode)){
		matrixNode = dynamic_cast<MatrixCoevNode*>(aChild);
	}
}

void RootCoevNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVCoevNode)){
		CPVCoevNode* cpvNode = dynamic_cast<CPVCoevNode*>(aChild);
		removeChild(cpvNode);
	} else if(childtype == typeid(LeafCoevNode)){
		LeafCoevNode* leafNode = dynamic_cast<LeafCoevNode*>(aChild);
		removeChild(leafNode);
	} else if(childtype == typeid(MatrixCoevNode)){
		matrixNode = NULL;
	}
}

std::string RootCoevNode::toString() const {
	std::stringstream ss;
	ss << "[RootCoevNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
