//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPVCoevNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef CPVCOEVNODE_COEVRJ_H_
#define CPVCOEVNODE_COEVRJ_H_

#include <stddef.h>

#include "Eigen/Core"
#include "Eigen/Dense"
#include "LeafCoevNode.h"
#include "ParentCoevNode.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class CPVCoevNode : public ParentCoevNode {
public:

	CPVCoevNode(DL_Utils::Frequencies &aFrequencies);
	~CPVCoevNode();

	std::string toString() const;

private: // Methods

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
	void doRemoveChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* CPVCOEVNODE_COEVRJ_H_ */
