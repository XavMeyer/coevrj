//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafCoevNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef LEAFCOEVNODE_COEVRJ_H_
#define LEAFCOEVNODE_COEVRJ_H_

#include <stddef.h>
#include <string>
#include <vector>

#include "../Types.h"
#include "BranchMatrixCoevNode.h"
#include "DAG/Node/Base/BaseNode.h"
#include "EdgeCoevNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/Definitions/Types.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

namespace DL = ::MolecularEvolution::DataLoader;
namespace MD = ::MolecularEvolution::Definition;
namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class LeafCoevNode : public EdgeCoevNode {
public:

	LeafCoevNode(const std::string &aSpecieName, const std::pair<size_t, size_t> &aPairPos,
			     const std::pair<usintBitset_t, usintBitset_t> &aPairNucl,
			     DL_Utils::Frequencies &aFrequencies);
	~LeafCoevNode();

	void init(const std::pair<size_t, size_t> &aPairPos,
			  const std::pair<usintBitset_t, usintBitset_t> &aPairNucl);

	const std::string& getSpecieName();

	std::string toString() const;

private:

	std::string specieName;
	std::vector<short unsigned int> siteCode;
	TI_EigenVector_t G;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* LEAFCOEVNODE_COEVRJ_H_ */
