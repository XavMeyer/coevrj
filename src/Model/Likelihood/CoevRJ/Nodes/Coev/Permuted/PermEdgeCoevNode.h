//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermEdgeCoevNode.h
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#ifndef PERM_EDGECOEVNODE_COEVRJ_H_
#define PERM_EDGECOEVNODE_COEVRJ_H_

#include "../../Types.h"
#include "PermBranchMatrixCoevNode.h"
#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class PermBranchMatrixCoevNode;

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class PermEdgeCoevNode : public DAG::BaseNode {
public:
	PermEdgeCoevNode(DL_Utils::Frequencies &aFrequencies);
	virtual ~PermEdgeCoevNode();

	const TI_Coev_EigenMatrix_t& getH() const;
	const TI_EigenVector_t& getNorm() const;

	PermBranchMatrixCoevNode* getBMCNode();

public:
	enum scaling_t {NONE, LOG};
	static const scaling_t SCALING_TYPE;
	static const float SCALING_THRESHOLD;

protected:

	DL_Utils::Frequencies &frequencies;
	PermBranchMatrixCoevNode *bmNode;
	TI_Coev_EigenMatrix_t H;
	TI_EigenVector_t nrm;

	void init();
};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PERM_EDGECOEVNODE_COEVRJ_H_ */
