//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermBranchMatrixCoevNode.cpp
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#include "PermBranchMatrixCoevNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/Types.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

PermBranchMatrixCoevNode::PermBranchMatrixCoevNode(DL_Utils::Frequencies &aFrequencies) :
		BaseNode(), frequencies(aFrequencies),
		Z(frequencies.size(), frequencies.size()) {

	BaseNode::initAccTime(1e-6);

	branchLength = 0;
	coevScaling = 0.;
	matrixNode = NULL;

}

PermBranchMatrixCoevNode::~PermBranchMatrixCoevNode() {
}

bool PermBranchMatrixCoevNode::setBranchLength(const double aBL) {
	if(branchLength != aBL) {
		branchLength = aBL;
		BaseNode::updated();
		return true;
	} else {
		return false;
	}
}

double PermBranchMatrixCoevNode::getBranchLength() const {
	return branchLength;
}

bool PermBranchMatrixCoevNode::setCoevScaling(const double aV) {
	if(coevScaling != aV) {
		coevScaling = aV;
		BaseNode::updated();
		return true;
	} else {
		return false;
	}
}

const TI_Coev_EigenSquareMatrix_t& PermBranchMatrixCoevNode::getZ() const {
	return Z;
}

const Eigen::FullPivLU<TI_Coev_EigenEIGMatrix_t>& PermBranchMatrixCoevNode::getEigenVectorLU() const {
	return matrixNode->getEigenVectorLU();
}

void PermBranchMatrixCoevNode::doProcessing() {

	// Shortcuts
	const TI_EigenEIGVector_t &D = matrixNode->getEigenValues();
	const TI_Coev_EigenEIGMatrix_t &V = matrixNode->getEigenVectors();
	const TI_Coev_EigenEIGMatrix_t &invV = matrixNode->getInverseEigenVectors();

	// Process number of substitutions
	double t = branchLength;

	double scaledT = t*coevScaling/matrixNode->getMatrixQScaling();

	// Processing Y
	TI_EigenEIGArray_t dt = (scaledT) * D.array();
	dt = dt.exp();
	TI_EigenEIGVector_t dt2 = dt.matrix();

#if TI_COEV_USE_FLOAT
	TI_Coev_EigenEIGMatrix_t tmpZ = (V * dt2.asDiagonal());
	Z = tmpZ.cast<float>();
#else
	Z = (V * dt2.asDiagonal() * invV);
#endif
	Z.eval();

}

bool PermBranchMatrixCoevNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void PermBranchMatrixCoevNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(PermMatrixCoevNode)){
		matrixNode = dynamic_cast<PermMatrixCoevNode*>(aChild);
	}
}

std::string PermBranchMatrixCoevNode::toString() const {
	std::stringstream ss;
	ss << "[PermBranchMatrixCoevNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
