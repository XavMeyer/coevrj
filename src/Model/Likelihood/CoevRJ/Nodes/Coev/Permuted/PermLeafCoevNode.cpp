//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermLeafCoevNode.cpp
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#include "PermLeafCoevNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/Types.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/Permuted/PermBranchMatrixCoevNode.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

PermLeafCoevNode::PermLeafCoevNode(const std::string &aSpecieName,
						   DL_Utils::Frequencies &aFrequencies) :
								   PermEdgeCoevNode(aFrequencies), specieName(aSpecieName) {

	nActivePairs = 0;
	BaseNode::initAccTime(1e-6);
	G.resize(frequencies.size(),0);
}

PermLeafCoevNode::~PermLeafCoevNode() {
}

/*std::vector<size_t> PermLeafCoevNode::preparePermutation(const std::vector< std::string > &aProfile) {
	/// Prepare the permutation (mapping) inf function of the profile
	static const size_t EMPTY_TOKEN = 100;

	std::set<size_t> profileIdx, usedIdx;
	std::vector<size_t> mapping(16, EMPTY_TOKEN);
	for(size_t iP=0; iP<aProfile.size(); ++iP) {
		int iNucl1 = MD::getNucleotides()->getNucleotideIdx(aProfile[iP][0]);
		int iNucl2 = MD::getNucleotides()->getNucleotideIdx(aProfile[iP][1]);

		// First find index of profile pair
		size_t profileCode = iNucl1*MD::getNucleotides()->NB_BASE_NUCL + iNucl2;
		profileIdx.insert(profileCode);
		usedIdx.insert(iP*5);
		// Then create mapping for these index into vector ( in profiles pairs are at positions 0, 5, 10, 15)
		mapping[profileCode] = iP*5;
	}

	// Finally add remaining numbers
	size_t nextFreeId = 0;
	for(size_t iP=0; iP<mapping.size(); ++iP) {
		if(profileIdx.count(iP) == 0) { // It's not a profile
			// Map the index iP
			while(usedIdx.count(nextFreeId) > 0) {
				nextFreeId++;
			}
			// Map iP to the next free id
			mapping[iP] = nextFreeId;
			usedIdx.insert(nextFreeId);
			nextFreeId++;
		} // else it is a profile, and is alredy mapped
	}

	return mapping;
}*/

std::vector<size_t> PermLeafCoevNode::preparePermutation(const std::vector< std::string > &aProfile) {
	/// Prepare the permutation (mapping) in function of the profile

	//std::vector< std::string > defaultProfiles = boost::assign::list_of("AA")("CC")("GG")("TT");

	std::set< char > availablePos1, availablePos2;
	for(size_t iN=0; iN<MD::getNucleotides()->getNucleotideVector().size(); ++iN) {
		availablePos1.insert(MD::getNucleotides()->getNucleotideVector()[iN]);
		availablePos2.insert(MD::getNucleotides()->getNucleotideVector()[iN]);
	}

	//std::cout << "Profile : ";
	std::string pos1NuclOrder, pos2NuclOrder;
	for(size_t iP=0; iP<aProfile.size(); ++iP) {
		pos1NuclOrder.push_back(aProfile[iP][0]);
		availablePos1.erase(aProfile[iP][0]);

		pos2NuclOrder.push_back(aProfile[iP][1]);
		availablePos2.erase(aProfile[iP][1]);

		//std::cout << aProfile[iP] << "\t";
	}
	//std::cout << std::endl;

	while(!availablePos1.empty()) {
		pos1NuclOrder.push_back(*availablePos1.begin());
		availablePos1.erase(availablePos1.begin());
	}

	while(!availablePos2.empty()) {
		pos2NuclOrder.push_back(*availablePos2.begin());
		availablePos2.erase(availablePos2.begin());
	}

	//std::cout << "Order P1 : " << pos1NuclOrder << std::endl;
	//std::cout << "Order P2 : " << pos2NuclOrder << std::endl;

	std::vector<size_t> mapping(16);
	for(size_t iP1=0; iP1<MD::getNucleotides()->NB_BASE_NUCL; ++iP1) {
		int iNucl1 = MD::getNucleotides()->getNucleotideIdx(pos1NuclOrder[iP1]);
		for(size_t iP2=0; iP2<MD::getNucleotides()->NB_BASE_NUCL; ++iP2) {
			int iNucl2 = MD::getNucleotides()->getNucleotideIdx(pos2NuclOrder[iP2]);
			size_t pairCode = iNucl1*MD::getNucleotides()->NB_BASE_NUCL + iNucl2;
			mapping[iP1*MD::getNucleotides()->NB_BASE_NUCL+iP2] = pairCode;
		}
	}

	std::vector<size_t> revMapping(16);
	for(size_t iR=0; iR < revMapping.size(); ++iR) {
		revMapping[mapping[iR]] = iR;
	}

	/*for(size_t iR=0; iR < revMapping.size(); ++iR) {
		if(mapping[iR] != revMapping[iR]) {
			std::cout <<mapping[iR] << "\t" << revMapping[iR] << std::endl;
			getchar();
		}
	}*/

	return revMapping;
}




void PermLeafCoevNode::add(const std::pair<size_t, size_t> &aPairPos,
		 	 	 	 	   const std::pair<usintBitset_t, usintBitset_t> &aPairNucl,
		 	 	 	 	   const std::vector< std::string > &aProfile) {

	/// Memorize the pair
	pairsPos.push_back(aPairPos);

	/// Prepare the permutation (mapping) inf function of the profile
	std::vector<size_t> mapping(preparePermutation(aProfile));

	/*std::cout << "Mapping : ";
	for(size_t i=0; i<mapping.size(); ++i) {
		std::cout << mapping[i] << "\t";
	}
	std::cout << std::endl;*/

	/// Manage the nucleotides pair (with potential gaps)
	// Contains the nucleotide presence as binary |bbbb| : |ACTG| ==> e.g. : |0010| = |  T |
	usintBitset_t nuclCodePos1 = aPairNucl.first;
	usintBitset_t nuclCodePos2 = aPairNucl.second;

	nActivePairs += 1;

    size_t nRows = G.rows();
    size_t nCols = nActivePairs;//G.cols()+1;
    size_t lastColIdx = nCols-1;

	if(nActivePairs > (size_t)G.cols()) {
		G.conservativeResize(nRows, nCols+10);
	}
	G.col(lastColIdx).setZero();

	for(size_t iNucl1=0; iNucl1<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl1) {
		for(size_t iNucl2=0; iNucl2<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl2) {
			if(nuclCodePos1[iNucl1] && nuclCodePos2[iNucl2]) {
				size_t profileCode = iNucl1*MD::getNucleotides()->NB_BASE_NUCL + iNucl2;
				size_t permutedProfileCode = mapping[profileCode];
				G.col(lastColIdx)(permutedProfileCode) = 1.0;
			}
		}
	}

	BaseNode::updated();
}

void PermLeafCoevNode::replace(const std::pair<size_t, size_t> &aPairPos,
	 	   const std::pair<usintBitset_t, usintBitset_t> &aPairNucl,
	 	   const std::vector< std::string > &aProfile) {

	/// Prepare the permutation (mapping) inf function of the profile
	std::vector<size_t> mapping(preparePermutation(aProfile));

	// find the index of the pairs to replace
	std::vector< std::pair<size_t, size_t> >::iterator itFind;
	itFind = std::find(pairsPos.begin(), pairsPos.end(), aPairPos);
	assert(itFind != pairsPos.end());

	// Contains the nucleotide presence as binary |bbbb| : |ACTG| ==> e.g. : |0010| = |  T |
	usintBitset_t nuclCodePos1 = aPairNucl.first;
	usintBitset_t nuclCodePos2 = aPairNucl.second;

	// Index of the pair to replace
	size_t idxToReplace = std::distance(pairsPos.begin(), itFind);
	G.col(idxToReplace).setZero();

	for(size_t iNucl1=0; iNucl1<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl1) {
		for(size_t iNucl2=0; iNucl2<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl2) {
			if(nuclCodePos1[iNucl1] && nuclCodePos2[iNucl2]) {
				size_t profileCode = iNucl1*MD::getNucleotides()->NB_BASE_NUCL + iNucl2;
				size_t permutedProfileCode = mapping[profileCode];
				G.col(idxToReplace)(permutedProfileCode) = 1.0;
			}
		}
	}

	BaseNode::updated();
}

void PermLeafCoevNode::remove(const std::pair<size_t, size_t> &aPairPos) {

	// find the index of the pairs to remove
	std::vector< std::pair<size_t, size_t> >::iterator itFind;
	itFind = std::find(pairsPos.begin(), pairsPos.end(), aPairPos);
	assert(itFind != pairsPos.end());

	// Index of the pair to remove
	size_t idxToRemove = std::distance(pairsPos.begin(), itFind);

	// Remove from pairs positions vector
	pairsPos.erase(itFind);

	// remove from matrix;
    size_t nRows = G.rows();
    size_t nCols = (nActivePairs-1);

	if(idxToRemove < nActivePairs ) {
		G.block(0, idxToRemove, nRows, nCols-idxToRemove) = G.block(0, idxToRemove+1, nRows, nCols-idxToRemove);
	}

	nActivePairs -= 1;

	//G.conservativeResize(nRows, nCols);

	BaseNode::updated();
}

const std::string& PermLeafCoevNode::getSpecieName() {
	return specieName;
}

void PermLeafCoevNode::doProcessing() {

	if(G.cols() == 0) {
		H = G.leftCols(nActivePairs);
		return;
	}

	const TI_Coev_EigenSquareMatrix_t &Z = bmNode->getZ();

	// Multiplication
	H = Z*G.leftCols(nActivePairs);

	/*Eigen::IOFormat CommaInitFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "", ";");
	std::cout << "[PERM] " << specieName << " >> G : " << std::endl << G.format(CommaInitFmt) << std::endl;
	std::cout << "[PERM] " << specieName << " >> H : " << std::endl << H.format(CommaInitFmt) << std::endl;*/

}

bool PermLeafCoevNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void PermLeafCoevNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(PermBranchMatrixCoevNode)){
		bmNode = dynamic_cast<PermBranchMatrixCoevNode*>(aChild);
	}
}

std::string PermLeafCoevNode::toString() const {
	std::stringstream ss;
	ss << "[PermLeafCoevNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
