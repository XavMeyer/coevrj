//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermRootCoevNode.cpp
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#include "PermRootCoevNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/Types.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/Permuted/PermEdgeCoevNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/Permuted/PermParentCoevNode.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

PermRootCoevNode::PermRootCoevNode(DL_Utils::Frequencies &aFrequencies) :
		PermParentCoevNode(aFrequencies) {

	BaseNode::initAccTime(1e-6);

	matrixNode = NULL;

	logLikelihood = 0;
	nrm.resize(H.cols());
	H.resize(16,0);
}

PermRootCoevNode::~PermRootCoevNode() {
}

double PermRootCoevNode::getLogLikelihood() const {
	return logLikelihood;
}

double PermRootCoevNode::getPairLogLikelihood(const std::pair<size_t, size_t> &aPairPos) const {
	std::vector< std::pair<size_t, size_t> >::const_iterator itFind;
	itFind = std::find(pairsPos.begin(), pairsPos.end(), aPairPos);
	assert(itFind != pairsPos.end());

	size_t idx = std::distance(pairsPos.begin(), itFind);

	return siteLogLik(idx);
}

void PermRootCoevNode::add(const std::pair<size_t, size_t> &aPairPos) {
	pairsPos.push_back(aPairPos);
	activePairs.push_back(true);

	BaseNode::updated();
}

void PermRootCoevNode::remove(const std::pair<size_t, size_t> &aPairPos) {
	std::vector< std::pair<size_t, size_t> >::iterator itFind;
	itFind = std::find(pairsPos.begin(), pairsPos.end(), aPairPos);
	assert(itFind != pairsPos.end());

	size_t idx = std::distance(pairsPos.begin(), itFind);
	pairsPos.erase(itFind);
	activePairs.erase(activePairs.begin()+idx);

	BaseNode::updated();
}

void PermRootCoevNode::enable(const std::pair<size_t, size_t> &aPairPos) {
	std::vector< std::pair<size_t, size_t> >::iterator itFind;
	itFind = std::find(pairsPos.begin(), pairsPos.end(), aPairPos);
	assert(itFind != pairsPos.end());

	size_t idx = std::distance(pairsPos.begin(), itFind);
	activePairs[idx] = true;

	BaseNode::updated();
}

void PermRootCoevNode::disable(const std::pair<size_t, size_t> &aPairPos) {
	std::vector< std::pair<size_t, size_t> >::iterator itFind;
	itFind = std::find(pairsPos.begin(), pairsPos.end(), aPairPos);
	assert(itFind != pairsPos.end());

	size_t idx = std::distance(pairsPos.begin(), itFind);
	activePairs[idx] = false;

	BaseNode::updated();
}

bool PermRootCoevNode::isActive() const {
	for(size_t iA=0; iA<activePairs.size(); ++iA) {
		if(activePairs[iA]) return true;
	}
	return false;
}


void PermRootCoevNode::doProcessing() {

	// FIXME std::cout << "ROOT [ " << getId() << "] :: H(" << H.rows() << "x" << H.cols() << ") :: G(" << G.rows() << "x" << G.cols() << ")" << std::endl;
	TI_Coev_EigenMatrix_t G;
	nrm.setZero();

	logLikelihood = 0.;

	if(children.front()->getH().cols() == 0) {
		return;
	}

	// Prepare G
	// Check block at first
	for(size_t iE=0; iE<children.size(); ++iE) {
		if(iE == 0) {
			G = children[iE]->getH();

			if(SCALING_TYPE == LOG  && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(children[iE]->getH());
			if (SCALING_TYPE == LOG && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		}
	}
	const Eigen::VectorXd &stFreq = matrixNode->getStationaryFrequency();
	//std::cout << "ROOT [ " << getId() << "] :: H(" << H.rows() << "x" << H.cols() << ") :: G(" << G.rows() << "x" << G.cols() << ") :: stFreq ( " << stFreq.rows() << " )" << std::endl;
#if TI_COEV_USE_FLOAT
	assert(false);
	likelihood = stFreq.transpose().cast<float>() * G;
#else
	likelihood = stFreq.transpose() * G;
#endif
	siteLogLik = likelihood.array().log();

	for(size_t iL=0; iL<(size_t)siteLogLik.size(); ++iL) {
		if(activePairs[iL]) {
			logLikelihood += siteLogLik[iL];
		}
	}

	if(PermEdgeCoevNode::SCALING_TYPE == PermEdgeCoevNode::LOG) {
		//logLikelihood += nrm.sum();
		for(size_t iL=0; iL<(size_t)nrm.size(); ++iL) {
			if(activePairs[iL]) {
				logLikelihood += nrm[iL];
			}
		}
	}

	/*Eigen::IOFormat CommaInitFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "", ";");
	std::cout << std::scientific << "[PERM] Root" << " >> G : " << std::endl <<G.format(CommaInitFmt) << std::endl;*/
}

bool PermRootCoevNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void PermRootCoevNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(PermCPVCoevNode)){
		PermCPVCoevNode* cpvNode = dynamic_cast<PermCPVCoevNode*>(aChild);
		addChild(cpvNode);
	} else if(childtype == typeid(PermLeafCoevNode)){
		PermLeafCoevNode* leafNode = dynamic_cast<PermLeafCoevNode*>(aChild);
		addChild(leafNode);
	} else if(childtype == typeid(PermMatrixCoevNode)){
		matrixNode = dynamic_cast<PermMatrixCoevNode*>(aChild);
	}
}

void PermRootCoevNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(PermCPVCoevNode)){
		PermCPVCoevNode* cpvNode = dynamic_cast<PermCPVCoevNode*>(aChild);
		bool isRemoved = removeChild(cpvNode);
		assert(isRemoved);
	} else if(childtype == typeid(PermLeafCoevNode)){
		PermLeafCoevNode* leafNode = dynamic_cast<PermLeafCoevNode*>(aChild);
		bool isRemoved = removeChild(leafNode);
		assert(isRemoved);
	} else if(childtype == typeid(PermMatrixCoevNode)){
		matrixNode = NULL;
	}
}

std::string PermRootCoevNode::toString() const {
	std::stringstream ss;
	ss << "[PermRootCoevNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
