//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermutedMatrixCoevNode.h
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#ifndef PERM_MATRIXCOEVNODE_COEVRJ_H_
#define PERM_MATRIXCOEVNODE_COEVRJ_H_

#include <stddef.h>

#include "../../Types.h"
#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"
#include "Eigen/LU"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotidePairModels/COEVMF.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class PermMatrixCoevNode: public DAG::BaseNode {

public:
	PermMatrixCoevNode(size_t aNbPairsInProfile, DL_Utils::Frequencies &aFrequencies);
	~PermMatrixCoevNode();

	bool setParameters(double r1, double r2, double s, double d);

	const TI_EigenEIGVector_t& getStationaryFrequency() const;

	bool isInvertible() const;
	const double getMatrixQScaling() const;
	const TI_Coev_EigenEIGMatrix_t& getEigenVectors() const;
	const TI_Coev_EigenEIGMatrix_t& getInverseEigenVectors() const;
	const TI_EigenEIGVector_t& getEigenValues() const;
	const Eigen::FullPivLU<TI_Coev_EigenEIGMatrix_t>& getEigenVectorLU() const;

	size_t getNbPairsInProfile() const;

	size_t serializedSize() const;
	void serializeToBuffer(char *buffer) const;
	void serializeFromBuffer(const char *buffer);

	std::string toString() const;

private:
	const size_t NB_PAIRS_IN_PROFILE;
	// Coefficients are [kappa, omega] or [thetas, omega]
	bool notInvertible;
	std::vector<double> coefficients;
	DL_Utils::Frequencies &frequencies;

	double matrixScaling;
	MolecularEvolution::MatrixUtils::COEV_MF::sharedPtr_t ptrMF;
	TI_EigenEIGVector_t D, stationaryFreq;
	TI_Coev_EigenEIGMatrix_t Q, V, invV;
	Eigen::FullPivLU<TI_Coev_EigenEIGMatrix_t> lu;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PERM_MATRIXCOEVNODE_COEVRJ_H_ */
