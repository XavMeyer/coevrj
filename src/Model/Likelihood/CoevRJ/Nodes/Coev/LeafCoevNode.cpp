//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafCoevNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "LeafCoevNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/Coev/../Types.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/BranchMatrixCoevNode.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

LeafCoevNode::LeafCoevNode(const std::string &aSpecieName, const std::pair<size_t, size_t> &aPairPos,
						   const std::pair<usintBitset_t, usintBitset_t> &aPairNucl,
						   DL_Utils::Frequencies &aFrequencies) :
								   EdgeCoevNode(aFrequencies), specieName(aSpecieName) {

	init(aPairPos, aPairNucl);
	BaseNode::initAccTime(1e-6);
}

LeafCoevNode::~LeafCoevNode() {
}

void LeafCoevNode::init(const std::pair<size_t, size_t> &aPairPos,
		 	 	 	 	const std::pair<usintBitset_t, usintBitset_t> &aPairNucl) {

	// Contains the nucleotide presence as binary |bbbb| : |ACTG| ==> e.g. : |0010| = |  T |
	usintBitset_t nuclCodePos1 = aPairNucl.first;
	usintBitset_t nuclCodePos2 = aPairNucl.second;

	G.resize(frequencies.size());
	G.setZero();

	for(size_t iNucl1=0; iNucl1<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl1) {
		for(size_t iNucl2=0; iNucl2<MD::getNucleotides()->NB_BASE_NUCL; ++iNucl2) {
			if(nuclCodePos1[iNucl1] && nuclCodePos2[iNucl2]) {
				size_t profileCode = iNucl1*MD::getNucleotides()->NB_BASE_NUCL + iNucl2;
				G(profileCode) = 1.0;
			}
		}
	}
	/*std::cout << aSpecieName << std::endl;
	std::cout << G << std::endl;
	getchar();*/

}

const std::string& LeafCoevNode::getSpecieName() {
	return specieName;
}

void LeafCoevNode::doProcessing() {

	const TI_Coev_EigenSquareMatrix_t &Z = bmNode->getZ();

	// Multiplication
	// The computation here is V*exp(L)*(V^-1)*G
	// We want to avoid the inversion of (V^-1) thus we compute
	// a) Using the LU decomp : V*X=G ==> X=(V^-1)*G
	TI_EigenVector_t X;
	if(bmNode->getEigenVectorLU().isInvertible()) {
		X = bmNode->getEigenVectorLU().solve(G);
	} else {
		X.setZero(frequencies.size());
	}
	// b)  Z*X ==> with Z = V*exp(L) and X = [(V^-1)*G]
	H = Z*X;

	/*Eigen::IOFormat CommaInitFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "", ";");
	std::cout << "[NORMAL] " << specieName << " >> G : " << std::endl << G.format(CommaInitFmt) << std::endl;
	std::cout << "[NORMAL] " << specieName << " >> H : " << std::endl << H.format(CommaInitFmt) << std::endl;*/
}

bool LeafCoevNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void LeafCoevNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixCoevNode)){
		bmNode = dynamic_cast<BranchMatrixCoevNode*>(aChild);
	}
}

std::string LeafCoevNode::toString() const {
	std::stringstream ss;
	ss << "[LeafCoevNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
