//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SwapGTRMoveRJ.h
 *
 * @date May 5, 2017
 * @author meyerx
 * @brief
 */
#ifndef SWAPGTRMOVERJ_H_
#define SWAPGTRMOVERJ_H_

#include "Sampler/ReversibleJump/Moves/RJMove.h"

#include "UtilsCoevRJ.h"
#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/CoevRJ/UID/SubSpaceUID.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Sampler/Proposal/BlockDispatch/BlockDispatcher.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"
#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceInfo.h"


namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class SwapGTRMoveRJ: public Sampler::RJMCMC::RJMove {
public:
	SwapGTRMoveRJ(Sampler::Sample &aCurrentSample, Base* aPtrLik, RNG *aRng);
	~SwapGTRMoveRJ();

	static std::string getStringStats(std::string &prefix);

private:

	static size_t countAccepted, countRejected;

	typedef Sampler::Proposal::BlockDispatcher BlockDispatcher_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::listSSInfo_t listSSInfo_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::sharedPtr_t ptrSSInfo_t;

	enum swapTypeEnum {UNINFORMED_SWAP, INFORMED_GTR_FIRST_SWAP, INFORMED_KEPT_FIRST_SWAP};
	const swapTypeEnum USE_COEV_INFORMATION;

	bool canApply;

	Base* ptrLik;
	RNG* rng;
	UtilsCoevRJ utilsCRJ;

	size_t newIdProfile;
	Sampler::ModelSpecificSample oldMSS;
	std::vector<MD::idProfile_t> newVecProfilesId, oldVecProfilesId;
	SubSpaceUID::sharedPtr_t oldSubSpaceUID, newSubSpaceUID;
	ptrSSInfo_t createdSSInfo, disabledSSInfo;

	// Have to be called first
	bool doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	// Have to be called second
	bool doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	bool doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	void defineMoveWithoutCoevInformation();

	/// First chose the GTR position, then conditionned on that chose the poisition to remove from the pair
	void defineMoveWithCoevInformationV1();

	/// First chose the GTR position, then conditionned on that chose the poisition to remove from the pair
	void defineMoveWithCoevInformationV2();

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* SWAPGTRMOVERJ_H_ */
