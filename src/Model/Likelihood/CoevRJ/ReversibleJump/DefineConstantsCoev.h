//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DefineConstantsCoev.h
 *
 * @date May 25, 2017
 * @author meyerx
 * @brief
 */
#ifndef DEFINECONSTANTSCOEV_H_
#define DEFINECONSTANTSCOEV_H_

#include <boost/assign/list_of.hpp>
#include <vector>

#include "Model/Likelihood/LikelihoodInterface.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"

#define DEFAULT_GAMMA_NORMAL 		0
#define EMPIRICAL_GAMMA_NORMAL 		1
#define GAMMA_NORMAL_HP_TYPE 		DEFAULT_GAMMA_NORMAL

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

namespace Parameters {
	static const size_t GTR_PARAMETER_OFFSET = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF+3;
	// The offset is tree parameter + GTR  ==> then it is alpha (of Gamma rates)
	static const size_t GAMMA_RATE_PARAMETER_OFFSET = 1+GTR_PARAMETER_OFFSET;
	// The offset is tree parameter + GTR + alpha (of Gamma rates) ==> then it is the coevScaling
	static const size_t COEV_SCALING_PARAMETER_OFFSET = 1+GTR_PARAMETER_OFFSET+1;
	// The offset is tree parameter + GTR + alpha (of Gamma rates) + coevScaling  ==> then it is the lambda poisson param
	static const size_t LAMBDA_POISSON_PARAMETERS_OFFSET = 1+GTR_PARAMETER_OFFSET+2;
	// The offset is tree parameter + GTR + alpha (of Gamma rates) + coevScaling + lambdaPoisson ==> then it is the prior params
	static const size_t PRIOR_PARAMETERS_OFFSET = 1+GTR_PARAMETER_OFFSET+3;

	// Single param values
	static const std::vector<std::string> SINGLE_PARAM_NAMES = boost::assign::list_of("R")("S")("D");
	static const size_t SINGLE_NB_PARAMS = SINGLE_PARAM_NAMES.size();

}

namespace Proposals {

	// To have something like [r1=0.04, r2=0.04,  s=0.04, d=0.87, d/s=20]
	// We need to define [MU_R=1.5, MU_S=1.5, MU_D=4.5]
	// We tune it a bit to better fit observed values
#if GAMMA_NORMAL_HP_TYPE == DEFAULT_GAMMA_NORMAL
	static const double GAMMA_NORMAL_MU_R = 0.;
	static const double GAMMA_NORMAL_STD_R = 0.5;
	static const double GAMMA_NORMAL_MU_S = 0.;
	static const double GAMMA_NORMAL_STD_S = 0.5;
	static const double GAMMA_NORMAL_MU_D = 3.5;
	static const double GAMMA_NORMAL_STD_D = 0.5;
#elif GAMMA_NORMAL_HP_TYPE == EMPIRICAL_GAMMA_NORMAL
	// [2.56e-1, 0., 3.13, 3.68e-1, 3.17e-4, 6.62e-1]
	static const double GAMMA_NORMAL_MU_R = 2.56e-1;
	static const double GAMMA_NORMAL_STD_R = sqrt(3.68e-1);
	static const double GAMMA_NORMAL_MU_S = 0.;
	static const double GAMMA_NORMAL_STD_S = sqrt(3.17e-4);
	static const double GAMMA_NORMAL_MU_D = 3.13;
	static const double GAMMA_NORMAL_STD_D = sqrt(6.62e-1);
#endif

	static const double GAMMA_NORMAL_GAUSSIAN_BM_STD_SMALL = LikelihoodInterface::getOptiL(1, 1, 0.10*0.10)/pow(4., 0.75);
	static const double GAMMA_NORMAL_GAUSSIAN_BM_STD_LARGE = LikelihoodInterface::getOptiL(1, 1, 0.20*0.20)/pow(4., 0.75);

} // namespace Proposal

namespace Priors {
	static const double GAMMA_RATE_ALPHA_EXPRATE = 0.005;

	static const double BL_SCALING_ALPHA = 2.;
	static const double BL_SCALING_BETA = 2.;

#if GAMMA_NORMAL_HP_TYPE == DEFAULT_GAMMA_NORMAL
	// To have something like [r1=0.04, r2=0.04,  s=0.04, d=0.0.87, d/s=20]
	// We need to define [MU_R=1.5, MU_S=1.5, MU_D=4.5]
	static const double GAMMA_NORMAL_MU_R = 0.;
	static const double GAMMA_NORMAL_STD_R = 0.5;
	static const double GAMMA_NORMAL_MU_S = 0.;
	static const double GAMMA_NORMAL_STD_S = 0.5;
	static const double GAMMA_NORMAL_MU_D = 3.5;
	static const double GAMMA_NORMAL_STD_D = 0.5;
#elif GAMMA_NORMAL_HP_TYPE == EMPIRICAL_GAMMA_NORMAL
	// [2.56e-1, 0., 3.13, 3.68e-1, 3.17e-4, 6.62e-1]
	static const double GAMMA_NORMAL_MU_R = 2.56e-1;
	static const double GAMMA_NORMAL_STD_R = sqrt(3.68e-1);
	static const double GAMMA_NORMAL_MU_S = 0.;
	static const double GAMMA_NORMAL_STD_S = sqrt(3.17e-4);
	static const double GAMMA_NORMAL_MU_D = 3.13;
	static const double GAMMA_NORMAL_STD_D = sqrt(6.62e-1);
#endif

} // namespace Priors

namespace HyperPriors {
	// parameters for the gamma-poisson hyperprior
	static const double GAMMA_POISSON_ALPHA = 1.;
	static const double GAMMA_POISSON_BETA = 10.0;

} // namespace HyperPriors


} // namespace CoevRJ
} // namespace Likelihood
} // namespace StatisticalModel


#endif /* DEFINECONSTANTSCOEV_H_ */
