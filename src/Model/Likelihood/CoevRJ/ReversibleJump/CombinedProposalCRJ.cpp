//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombinedProposalCRJ.cpp
 *
 * @date May 6, 2017
 * @author meyerx
 * @brief
 */
#include "CombinedProposalCRJ.h"

#include <assert.h>
#include <stddef.h>

#include "Parallel/RNG/RNG.h"
#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/SwapGTRMoveRJ.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/SwapCoevMoveRJ.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/QuartetCoevMoveRJ.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/InformedTransdimMoveCoevRJ.h"



namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

const double CombinedProposalCRJ::RANDOM_TRANSDIM_MOVE_PROBABILITY = 0.0;
const double CombinedProposalCRJ::INFORMED_TRANSDIM_MOVE_PROBABILITY = 1.0;


const std::vector<std::string> CombinedProposalCRJ::MOVES_NAME =
		boost::assign::list_of("FORWARD_RAND")("BACKWARD_RAND")("FORWARD_INF")("BACKWARD_INF")("SWAP_GTR")("SWAP_COEV")("FORWARD_QUARTET")("BACKWARD_QUARTET");

CombinedProposalCRJ::CombinedProposalCRJ(combinedType_t aProposalType, StatisticalModel::Model &aModel) :
		Sampler::RJMCMC::RJMoveProposal(aModel), COMBINED_TYPE(aProposalType) {

	ptrLik = dynamic_cast<Base*>(model.getLikelihood().get());
	assert((ptrLik != NULL) && "This proposal can only be used with CoevRJ::Base likelihood.");

}

CombinedProposalCRJ::~CombinedProposalCRJ() {
}

std::string CombinedProposalCRJ::getStringStats(std::string &prefix) const {
	std::stringstream ss;
	double ratio = cntProposed == 0 ? 0. : (double)cntAccepted/(double)(cntProposed);
	ss << prefix << "[CombinedProposalCRJ] Proposal acceptance ratio = " << ratio;
	ss << " ( " << cntAccepted << " / " << cntProposed << " )" << std::endl;
	//ss << TransdimMoveCoevRJ::getStringStats(prefix);
	ss << InformedTransdimMoveCoevRJ::getStringStats(prefix);
	ss << SwapGTRMoveRJ::getStringStats(prefix);
	ss << SwapCoevMoveRJ::getStringStats(prefix);
	ss << QuartetCoevMoveRJ::getStringStats(prefix);
	return ss.str();
}

Sampler::RJMCMC::RJMove::sharedPtr_t CombinedProposalCRJ::doPropose(Sampler::Sample &aSample) {

	const size_t N_COEV_PAIRS = ptrLik->getNActiveCoevLikelihood();

	// Chose a move
	std::vector<double> probs = defineMoveProbability(N_COEV_PAIRS);
	int idMove = rng->drawFrom(probs);

	//std::cout << "[CombinedProposalCRJ] N_COEV_PAIRS = " << N_COEV_PAIRS;
	//std::cout << "\tMAX_PAIRS = " << ptrLik->getMaxActiveCoevLikelihood();
	//std::cout << "\tMove type : " << MOVES_NAME[idMove] << std::endl; // TODO DEBUG PRINT
	//std::cout << "[CombinedProposalCRJ] Probs :\t"; // TODO DEBUG PRINT
	//for(size_t iP=0; iP<probs.size(); ++iP) std::cout << probs[iP] << "\t"; // TODO DEBUG PRINT
	//std::cout << std::endl; // TODO DEBUG PRINT

	// Define the direction and create the move
	Sampler::RJMCMC::RJMove::sharedPtr_t aMove;
	Sampler::RJMCMC::RJMove::moveDirection_t moveDirection;
	if(idMove == FORWARD_RAND_MOVE_ID) {
		assert(false);
	} else if(idMove == BACKWARD_RAND_MOVE_ID) {
		assert(false);
	} else if(idMove == FORWARD_INFORMED_MOVE_ID) {
		moveDirection = Sampler::RJMCMC::RJMove::FORWARD;
		aMove.reset(new InformedTransdimMoveCoevRJ(moveDirection, aSample, ptrLik, rng));
	} else if(idMove == BACKWARD_INFORMED_MOVE_ID) {
		moveDirection = Sampler::RJMCMC::RJMove::BACKWARD;
		aMove.reset(new InformedTransdimMoveCoevRJ(moveDirection, aSample, ptrLik, rng));
	} else if(idMove == SWAP_GTR_MOVE_ID) {
		moveDirection = Sampler::RJMCMC::RJMove::NOT_TRANSDIM;
		aMove.reset(new SwapGTRMoveRJ(aSample, ptrLik, rng));
	} else if(idMove == SWAP_COEV_MOVE_ID) {
		moveDirection = Sampler::RJMCMC::RJMove::NOT_TRANSDIM;
		aMove.reset(new SwapCoevMoveRJ(aSample, ptrLik, rng));
	} else if(idMove == FORWARD_QUARTET_COEV_MOVE_ID) {
		moveDirection = Sampler::RJMCMC::RJMove::FORWARD;
		aMove.reset(new QuartetCoevMoveRJ(moveDirection, aSample, ptrLik, rng));
	} else if(idMove == BACKWARD_QUARTET_COEV_MOVE_ID) {
		moveDirection = Sampler::RJMCMC::RJMove::BACKWARD;
		aMove.reset(new QuartetCoevMoveRJ(moveDirection, aSample, ptrLik, rng));
	} else {
		assert(false && "Unknown move type.");
	}

	// Define the move probability
	double fwProposalProb, bwProposalProb;
	if(idMove == FORWARD_RAND_MOVE_ID) {
		fwProposalProb = probs[FORWARD_RAND_MOVE_ID];
		std::vector<double> backwardProbs = defineMoveProbability(N_COEV_PAIRS+1);
		bwProposalProb = backwardProbs[BACKWARD_RAND_MOVE_ID];

	} else if(idMove == BACKWARD_RAND_MOVE_ID) {
		std::vector<double> forwardProbs = defineMoveProbability(N_COEV_PAIRS-1);
		fwProposalProb = forwardProbs[FORWARD_RAND_MOVE_ID];
		bwProposalProb = probs[BACKWARD_RAND_MOVE_ID];

		//std::cout << "[CombinedProposalCRJ] Alternate probs :\t"; // TODO DEBUG PRINT
		//for(size_t iP=0; iP<forwardProbs.size(); ++iP) std::cout << forwardProbs[iP] << "\t"; // TODO DEBUG PRINT
		//std::cout << std::endl; // TODO DEBUG PRINT

	} else if(idMove == FORWARD_INFORMED_MOVE_ID) {
		fwProposalProb = probs[FORWARD_INFORMED_MOVE_ID];
		std::vector<double> backwardProbs = defineMoveProbability(N_COEV_PAIRS+1);
		bwProposalProb = backwardProbs[BACKWARD_INFORMED_MOVE_ID];

	} else if(idMove == BACKWARD_INFORMED_MOVE_ID) {
		std::vector<double> forwardProbs = defineMoveProbability(N_COEV_PAIRS-1);
		fwProposalProb = forwardProbs[FORWARD_INFORMED_MOVE_ID];
		bwProposalProb = probs[BACKWARD_INFORMED_MOVE_ID];

	} else if(idMove == FORWARD_QUARTET_COEV_MOVE_ID) {
		fwProposalProb = probs[FORWARD_QUARTET_COEV_MOVE_ID];
		std::vector<double> backwardProbs = defineMoveProbability(N_COEV_PAIRS+1);
		bwProposalProb = backwardProbs[BACKWARD_QUARTET_COEV_MOVE_ID];

	} else if(idMove == BACKWARD_QUARTET_COEV_MOVE_ID) {
		std::vector<double> forwardProbs = defineMoveProbability(N_COEV_PAIRS-1);
		fwProposalProb = forwardProbs[FORWARD_QUARTET_COEV_MOVE_ID];
		bwProposalProb = probs[BACKWARD_QUARTET_COEV_MOVE_ID];

	} else { // Move are symmetrical
		fwProposalProb = probs[idMove];
		bwProposalProb = probs[idMove];
	}
	aMove->setForwardProposalProb(fwProposalProb);
	aMove->setBackwardProposalProb(bwProposalProb);

	return aMove;
}

std::vector<double> CombinedProposalCRJ::defineMoveProbability(const size_t N_COEV_PAIRS) {
	std::vector<double> probs;
	if(COMBINED_TYPE == CONSTANT_CT) {
		probs = defineCstMoveProbability(N_COEV_PAIRS);
	} else if(COMBINED_TYPE == PROPORTIONAL_TO_K_CT) {
		probs = definePropKMoveProbability(N_COEV_PAIRS);
	} else {
		assert(false && "Move probabilities not yet defined.");
	}

	// Normalize probs to be sure
	double sum = 0.;
	for(size_t iP=0; iP<probs.size(); ++iP) sum += probs[iP];
	for(size_t iP=0; iP<probs.size(); ++iP) probs[iP] /= sum;

	//std::cout << "Move prob : "; // FIXME
	//for(size_t iP=0; iP<probs.size(); ++iP) std::cout << MOVES_NAME[iP] << " = " << probs[iP] << "\t"; // FIXME
	//std::cout << std::endl; // FIXME

	return probs;
}

std::vector<double> CombinedProposalCRJ::defineCstMoveProbability(const size_t N_COEV_PAIRS) {
	std::vector<double> probs(MOVES_NAME.size(), 0.);

	const double SWAP_FREQ = 0.5;
	const double QUARTET_FREQ = 0.2;//0.5;

	if(N_COEV_PAIRS == 0) {
		probs[FORWARD_RAND_MOVE_ID] = 1.0*RANDOM_TRANSDIM_MOVE_PROBABILITY; // We can only create coev lik
		probs[FORWARD_INFORMED_MOVE_ID] = 1.0*INFORMED_TRANSDIM_MOVE_PROBABILITY; // We can only create coev lik
	} else if(N_COEV_PAIRS == 1) {
		probs[FORWARD_RAND_MOVE_ID] = (1./3.)*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_RAND_MOVE_ID] = (1./3.)*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[FORWARD_INFORMED_MOVE_ID] = (1./3.)*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_INFORMED_MOVE_ID] = (1./3.)*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[SWAP_GTR_MOVE_ID] = (1./3.)*SWAP_FREQ;
		probs[FORWARD_QUARTET_COEV_MOVE_ID] = (1./3)*QUARTET_FREQ;
		probs[BACKWARD_QUARTET_COEV_MOVE_ID] = 0;
	} else if(N_COEV_PAIRS == ptrLik->getMaxActiveCoevLikelihood()) {
		probs[FORWARD_RAND_MOVE_ID] = 0.;
		probs[BACKWARD_RAND_MOVE_ID] = 0.5*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[FORWARD_INFORMED_MOVE_ID] = 0.;
		probs[BACKWARD_INFORMED_MOVE_ID] = 0.5*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[SWAP_GTR_MOVE_ID] = 0.;
		probs[SWAP_COEV_MOVE_ID] = (1./3)*SWAP_FREQ;
		probs[FORWARD_QUARTET_COEV_MOVE_ID] = 0.;
		probs[BACKWARD_QUARTET_COEV_MOVE_ID] = (1./3)*QUARTET_FREQ;
	} else {
		probs[FORWARD_RAND_MOVE_ID] = 0.25*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_RAND_MOVE_ID] = 0.25*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[FORWARD_INFORMED_MOVE_ID] = 0.25*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_INFORMED_MOVE_ID] = 0.25*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[SWAP_GTR_MOVE_ID] = 0.25*SWAP_FREQ;
		probs[SWAP_COEV_MOVE_ID] = 0.25*SWAP_FREQ;
		probs[FORWARD_QUARTET_COEV_MOVE_ID] = 0.25*QUARTET_FREQ;
		probs[BACKWARD_QUARTET_COEV_MOVE_ID] = 0.25*QUARTET_FREQ;
	}

	return probs;
}

std::vector<double> CombinedProposalCRJ::definePropKMoveProbability(const size_t N_COEV_PAIRS) {
	std::vector<double> probs(MOVES_NAME.size(), 0.); // Create | Delete | swap GTR | swap coev

	size_t MAX_COEV_PAIRS = ptrLik->getMaxActiveCoevLikelihood();
	//double ascendingProb = (double)(N_COEV_PAIRS)/(double)MAX_COEV_PAIRS;
	//double descendingProb = (double)(MAX_COEV_PAIRS - N_COEV_PAIRS)/(double)MAX_COEV_PAIRS;
	const double offset = 0.01;
	double ascendingProb = offset + (1.-2*offset)*((double)(N_COEV_PAIRS)/(double)MAX_COEV_PAIRS);
	double descendingProb = offset + (1.-2*offset)*((double)(MAX_COEV_PAIRS - N_COEV_PAIRS)/(double)MAX_COEV_PAIRS);
	assert(fabs(1.-(ascendingProb+descendingProb)) < 1e-10);

	const double coeffTransdim = 1.0;
	const double coeffSwap = 0.5;
	const double coeffQuartet = 0.2;//0.5;

	if(N_COEV_PAIRS == 0) {

		probs[FORWARD_RAND_MOVE_ID] = coeffTransdim*descendingProb*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_RAND_MOVE_ID] = 0.;
		probs[FORWARD_INFORMED_MOVE_ID] = coeffTransdim*descendingProb*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_INFORMED_MOVE_ID] = 0.;
		probs[SWAP_GTR_MOVE_ID] = 0.;
		probs[SWAP_COEV_MOVE_ID] = 0.;
		probs[FORWARD_QUARTET_COEV_MOVE_ID] = 0.;
		probs[BACKWARD_QUARTET_COEV_MOVE_ID] = 0.;
	} else if(N_COEV_PAIRS == 1) {
		probs[FORWARD_RAND_MOVE_ID] = coeffTransdim*descendingProb*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_RAND_MOVE_ID] = coeffTransdim*ascendingProb*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[FORWARD_INFORMED_MOVE_ID] = coeffTransdim*descendingProb*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_INFORMED_MOVE_ID] = coeffTransdim*ascendingProb*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[SWAP_GTR_MOVE_ID] = coeffSwap;
		probs[SWAP_COEV_MOVE_ID] = 0.; // No coev likelihood pairs
		probs[FORWARD_QUARTET_COEV_MOVE_ID] = coeffQuartet;
		probs[BACKWARD_QUARTET_COEV_MOVE_ID] = 0.;
	} else if(N_COEV_PAIRS == ptrLik->getMaxActiveCoevLikelihood()) {
		probs[FORWARD_RAND_MOVE_ID] = 0.;
		probs[BACKWARD_RAND_MOVE_ID] = coeffTransdim*ascendingProb*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[FORWARD_INFORMED_MOVE_ID] = 0.;
		probs[BACKWARD_INFORMED_MOVE_ID] = coeffTransdim*ascendingProb*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[SWAP_GTR_MOVE_ID] = 0.; // No free GTR
		probs[SWAP_COEV_MOVE_ID] = coeffSwap;
		probs[FORWARD_QUARTET_COEV_MOVE_ID] = 0.;
		probs[BACKWARD_QUARTET_COEV_MOVE_ID] = coeffQuartet;
	} else {
		probs[FORWARD_RAND_MOVE_ID] = coeffTransdim*descendingProb*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_RAND_MOVE_ID] = coeffTransdim*ascendingProb*RANDOM_TRANSDIM_MOVE_PROBABILITY;
		probs[FORWARD_INFORMED_MOVE_ID] = coeffTransdim*descendingProb*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[BACKWARD_INFORMED_MOVE_ID] = coeffTransdim*ascendingProb*INFORMED_TRANSDIM_MOVE_PROBABILITY;
		probs[SWAP_GTR_MOVE_ID] = coeffSwap/2.;
		probs[SWAP_COEV_MOVE_ID] = coeffSwap/2.;
		probs[FORWARD_QUARTET_COEV_MOVE_ID] = coeffQuartet/2.;
		probs[BACKWARD_QUARTET_COEV_MOVE_ID] = coeffQuartet/2.;
	}

	return probs;
}



} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
