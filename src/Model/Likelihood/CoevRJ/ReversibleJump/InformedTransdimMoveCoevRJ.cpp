//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file InformedTransdimMoveCoevRJ.cpp
 *
 * @date May 10, 2017
 * @author meyerx
 * @brief
 */
#include "InformedTransdimMoveCoevRJ.h"

#include <assert.h>
#include <boost/math/distributions/gamma.hpp>
#include <boost/math/distributions/normal.hpp>

#include "Parallel/RNG/RNG.h"
#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/DefineConstantsCoev.h"
#include "Utils/Statistics/Dirichlet/DirichletPDF.h"
#include "Utils/Statistics/Dirichlet/DirichletHyperPrior.h"
#include "Utils/Statistics/ConjugatePrior/GammaNormalConjugatePrior.h"
#include "Utils/Statistics/ConjugatePrior/InverseGammaConjugatePrior.h"

namespace Sampler { class ModelSpecificSample; }
namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class CoevLikelihoodInfo;

const double InformedTransdimMoveCoevRJ::FREQ_INFORMED_MOVES = 0.5;

size_t InformedTransdimMoveCoevRJ::countAcceptedFW = 0;
size_t InformedTransdimMoveCoevRJ::countRejectedFW = 0;
size_t InformedTransdimMoveCoevRJ::countAcceptedBW = 0;
size_t InformedTransdimMoveCoevRJ::countRejectedBW = 0;

InformedTransdimMoveCoevRJ::InformedTransdimMoveCoevRJ(moveDirection_t aMoveDirection, Sampler::Sample &aCurrentSample,
		               Base* aPtrLik, RNG *aRng) :
		Sampler::RJMCMC::RJMove(aMoveDirection, aCurrentSample),
		ptrLik(aPtrLik), rng(aRng),
		utilsCRJ(ptrLik, rng, proposedSample) {

	if(moveDirection == FORWARD) { // Forward
		isWithCoevScalingChange = mustAddCoevScaling();
		defineForwardMove();
	} else { // Backward
		isWithCoevScalingChange = mustRemoveCoevScaling();
		defineBackwardMove();
	}
}

InformedTransdimMoveCoevRJ::~InformedTransdimMoveCoevRJ() {
}

std::string InformedTransdimMoveCoevRJ::getStringStats(std::string &prefix) {
	std::stringstream ss;
	double ratioFW = countAcceptedFW+countRejectedFW == 0 ? 0. : (double)countAcceptedFW/(double)(countAcceptedFW+countRejectedFW);
	ss << prefix << "[InformedTransdimMoveCoevRJ] Forward acceptance ratio = " << ratioFW;
	ss << " ( " << countAcceptedFW << " / " << (countAcceptedFW+countRejectedFW) << " )" << std::endl;

	double ratioBW = countAcceptedBW+countRejectedBW == 0 ? 0. : (double)countAcceptedBW/(double)(countAcceptedBW+countRejectedBW);
	ss << prefix << "[InformedTransdimMoveCoevRJ] Backward acceptance ratio = " << ratioBW;
	ss << " ( " << countAcceptedBW << " / " << (countAcceptedBW+countRejectedBW) << " )" << std::endl;
	return ss.str();
}

void InformedTransdimMoveCoevRJ::defineForwardMove() {

	const std::set<size_t> &segSitesGTR(ptrLik->getSegregatingSitesGTR());
	const size_t N_SEG_SITES_GTR = segSitesGTR.size();

	// Forward move: We first have to find two sites if possible
	if(N_SEG_SITES_GTR < 2) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_GTR < 2 !");
		return;
	}

	/// Draw sites positions
	//double sTime = MPI_Wtime();

	size_t pos1, pos2;
	double pairProb = 1.;
	PairProfileInfo ppi;
	if(rng->genUniformDbl() <= FREQ_INFORMED_MOVES) {
		const boost::dynamic_bitset<> &sitesAvailability = ptrLik->getSitesAvailability();

		UtilsCoevRJ::UnormalizedProbabilities uProb = utilsCRJ.defineProbabilityBySitesAtFirstPosition(sitesAvailability);
		//std::cout << "uProb cst = " << uProb.normalizingConstant << std::endl;
		size_t rndP1 = rng->drawFromLargeUnormalizedVector(uProb.normalizingConstant, uProb.probabilities);

		UtilsCoevRJ::UnormalizedProbabilities uProb1 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(rndP1, sitesAvailability);
		//std::cout << rndP1 << " -- > uProb1 cst = " << uProb1.normalizingConstant << std::endl;
		size_t rndP2 = rng->drawFromLargeUnormalizedVector(uProb1.normalizingConstant, uProb1.probabilities);

		UtilsCoevRJ::UnormalizedProbabilities uProb2 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(rndP2, sitesAvailability);
		//std::cout << rndP2 << " -- > uProb2 cst = " << uProb2.normalizingConstant << std::endl;

		double prob1 = (uProb.probabilities[rndP1]/uProb.normalizingConstant);
		double prob2 = (uProb.probabilities[rndP2]/uProb.normalizingConstant);
		double prob2Given1 = (uProb1.probabilities[rndP2]/uProb1.normalizingConstant);
		double prob1Given2 = (uProb2.probabilities[rndP1]/uProb2.normalizingConstant);

		pos1 = rndP1;
		pos2 = rndP2;
		if(pos1 > pos2) std::swap(pos1, pos2);

		//pairProb = (prob1*prob2Given1+prob2*prob1Given2); // FIXME DEFAULT
		pairProb = FREQ_INFORMED_MOVES*(prob1*prob2Given1+prob2*prob1Given2); // FIXME NEW
		pairProb += (1.0-FREQ_INFORMED_MOVES)*2./(((double)N_SEG_SITES_GTR)*((double)N_SEG_SITES_GTR-1.)); // FIXME NEW

		ppi = ptrLik->getCoevAlignManager()->getPairProfileInfo(pos1, pos2);
		vecProfilesId = utilsCRJ.copyProfileIds(ppi);

		if(vecProfilesId.empty()) {
			/*std::stringstream ss;
			ss << "Empty profile : " << pos1 << "\t" << pos2 << std::endl;
			assert(!vecProfilesId.empty() && ss.str().c_str());*/
			moveState = IMPOSSIBLE;
		}

	} else {
		std::vector<size_t> positions = utilsCRJ.drawSitesPosUniform(2, segSitesGTR);
		pos1 = positions[0];
		pos2 = positions[1];

		// pairProb = 2./(((double)N_SEG_SITES_GTR)*((double)N_SEG_SITES_GTR-1.)); // FIXME DEFAULT

		const boost::dynamic_bitset<> &sitesAvailability = ptrLik->getSitesAvailability();
		UtilsCoevRJ::UnormalizedProbabilities uProb = utilsCRJ.defineProbabilityBySitesAtFirstPosition(sitesAvailability);
		UtilsCoevRJ::UnormalizedProbabilities uProb1 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos1, sitesAvailability);
		UtilsCoevRJ::UnormalizedProbabilities uProb2 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos2, sitesAvailability);

		double prob1 = (uProb.probabilities[pos1]/uProb.normalizingConstant);
		double prob2 = (uProb.probabilities[pos2]/uProb.normalizingConstant);
		double prob2Given1 = (uProb1.probabilities[pos2]/uProb1.normalizingConstant);
		double prob1Given2 = (uProb2.probabilities[pos1]/uProb2.normalizingConstant);

		pairProb = FREQ_INFORMED_MOVES*(prob1*prob2Given1+prob2*prob1Given2); // FIXME NEW
		pairProb += (1.0-FREQ_INFORMED_MOVES)*2./(((double)N_SEG_SITES_GTR)*((double)N_SEG_SITES_GTR-1.)); // FIXME NEW


		ppi = ptrLik->getCoevAlignManager()->getPairProfileInfo(pos1, pos2);
		vecProfilesId = utilsCRJ.copyProfileIds(ppi);

		if(vecProfilesId.empty()) {
			//std::stringstream ss;
			//ss << "Empty profile : " << pos1 << "\t" << pos2 << std::endl;
			//assert(!vecProfilesId.empty() && ss.str().c_str());
			moveState = IMPOSSIBLE;
		}
	}

	/// Get info about the lik
	subSpaceUID.reset(new SubSpaceUID(pos1, pos2));

	/// Define the moves probability
	// Pair probability given score
	fwMoveProb = pairProb;
	// U[#nb of coev +1]
	bwMoveProb = (1./((double)(1.+ptrLik->getNActiveCoevLikelihood())));

	//std::cout << "[InformedTransdimMoveCoevRJ] FW - pos1 = " << pos1 << "\t pos2 = " << pos2 << "\t ppi.score = " << std::scientific << ppi.score << std::endl; // TODO DEBUG PRINT
	//std::cout << "[InformedTransdimMoveCoevRJ] FW - Prob pos1 = " << std::scientific <<  uProb.probabilities[rndP1]/uProb.normalizingConstant << "\t Prob pos2 = " << std::scientific << uProb.probabilities[rndP2]/uProb.normalizingConstant << "\t ppi.score = " << ppi.score << std::endl; // TODO DEBUG PRINT
	//std::cout << "[InformedTransdimMoveCoevRJ] Nb GTR = " << N_SEG_SITES_GTR << "\t Nb active Coev = " << ptrLik->getNActiveCoevLikelihood() << "\t Max active Coev = " << ptrLik->getMaxActiveCoevLikelihood() << std::endl; // TODO DEBUG PRINT

	/// Draws the parameter values
	if(ptrLik->getNActiveCoevLikelihood() == 0) {
		r1 = rng->genNormal(Proposals::GAMMA_NORMAL_MU_R, Proposals::GAMMA_NORMAL_STD_R);
		r2 = r1;
		s = 0.;//rng->genNormal(Proposals::GAMMA_NORMAL_MU_S, Proposals::GAMMA_NORMAL_STD_S);
		d = rng->genNormal(Proposals::GAMMA_NORMAL_MU_D, Proposals::GAMMA_NORMAL_STD_D);
	} else {
		double randR = 0.;//rng->genNormal(0, Proposals::GAMMA_NORMAL_GAUSSIAN_BM_STD_LARGE);
		r1 = currentSample.getDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET)+randR;
		r2 = currentSample.getDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET)+randR;
		s = currentSample.getDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+1);
		double randD = 0.;//rng->genNormal(0, Proposals::GAMMA_NORMAL_GAUSSIAN_BM_STD_LARGE);
		d = currentSample.getDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+2)+randD;
	}

	std::pair<MD::idProfile_t, double> randProf = utilsCRJ.drawProfile(ppi);
	idProfile = randProf.first;

	//if(pos1 == 0 && pos2 == 1) {
		//std::cout << "[InformedTransdimMoveCoevRJ] ratio D/S : " << d/s << std::endl; // TODO DEBUG PRINT
		//std::cout << "[InformedTransdimMoveCoevRJ] FW - idProfile = " << idProfile << "\t params = [ " << r1 << ", " << r2 << ", " << s << ", " << d << " ]" << std::endl; // TODO DEBUG PRINT
	//}

	/// Coev Scaling
	if(isWithCoevScalingChange) { // Going from 0->1 and require scaling
		coevScaling = rng->genGamma(Priors::BL_SCALING_ALPHA, Priors::BL_SCALING_BETA);
	} else {
		coevScaling = 1.;
	}

	/// Define the probabilities
	computeDrawPDF(ppi);
	//std::cout << "[InformedTransdimMoveCoevRJ] FW - fwRandomProb = " << fwRandomProb << "\t bwRandomProb = " << bwRandomProb << std::endl; // TODO DEBUG PRINT

	// Jacobian is 1. because new values are independently drawn from priors
	jacobian = 1.;

}

void InformedTransdimMoveCoevRJ::defineBackwardMove() {

	const size_t N_COEV = ptrLik->getNActiveCoevLikelihood();

	// Forward move: We first have to find two sites if possible
	if(N_COEV < 1) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_COEV < 1!");
		return;
	}

	/// Draw the coev "cluster" to remove
	PermCoevLikelihoodInfo* ptrCLI = utilsCRJ.drawCoevLik();

	/// Get info about the lik
	subSpaceUID = ptrCLI->getUIDSubSpace();
	vecProfilesId = ptrCLI->getProfilesId();

	size_t pos1 = ptrCLI->getPositions().first;
	size_t pos2 = ptrCLI->getPositions().second;

	double pairProb = 1.0;
	PairProfileInfo ppi;
	boost::dynamic_bitset<> sitesAvailability(ptrLik->getSitesAvailability());
	sitesAvailability[pos1] = true;
	sitesAvailability[pos2] = true;

	ppi = ptrLik->getCoevAlignManager()->getPairProfileInfo(pos1, pos2);
	UtilsCoevRJ::UnormalizedProbabilities uProb = utilsCRJ.defineProbabilityBySitesAtFirstPosition(sitesAvailability);
	UtilsCoevRJ::UnormalizedProbabilities uProb1 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos1, sitesAvailability);
	UtilsCoevRJ::UnormalizedProbabilities uProb2 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos2, sitesAvailability);
	double prob1 = (uProb.probabilities[pos1]/uProb.normalizingConstant);
	double prob2 = (uProb.probabilities[pos2]/uProb.normalizingConstant);
	double prob2Given1 = (uProb1.probabilities[pos2]/uProb1.normalizingConstant);
	double prob1Given2 = (uProb2.probabilities[pos1]/uProb2.normalizingConstant);

	// Probability for informed moves
	pairProb = FREQ_INFORMED_MOVES * ( prob1*prob2Given1+prob2*prob1Given2 );


	const std::set<size_t> &segSitesGTR(ptrLik->getSegregatingSitesGTR());
	const size_t N_SEG_SITES_GTR = segSitesGTR.size();
	assert(sitesAvailability.count() == N_SEG_SITES_GTR+2);

	// Probability for non-informed moves
	pairProb += (1. - FREQ_INFORMED_MOVES) * 2./(((double)N_SEG_SITES_GTR+2)*((double)N_SEG_SITES_GTR+1.));

	/// Define the moves probability
	// Pair probability given score
	fwMoveProb = pairProb;
	// U[#nb of coev]
	bwMoveProb = 1./(double)(ptrLik->getNActiveCoevLikelihood());

	//std::cout << "[InformedTransdimMoveCoevRJ] BW - fwMoveProb = " << fwMoveProb << "\t bwMoveProb = " << bwMoveProb << std::endl; // TODO DEBUG PRINT

	/// Get the parameter values
	Sampler::ModelSpecificSample& mss = currentSample.getMSS(subSpaceUID);

	r1 = mss.getDblValues()[0];
	r2 = mss.getDblValues()[1];
	s = mss.getDblValues()[2];
	d = mss.getDblValues()[3];

	idProfile = mss.getIntValues()[0];

	//std::cout << "[InformedTransdimMoveCoevRJ] BW - idProfile = " << idProfile << "\t params = [ " << r1 << ", " << r2 << ", " << s << ", " << d << " ]" << std::endl; // TODO DEBUG PRINT

	double randR = 0.;//rng->genNormal(0, Proposals::GAMMA_NORMAL_GAUSSIAN_BM_STD_LARGE);
	r1 = currentSample.getDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET)+randR;
	r2 = currentSample.getDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET)+randR;
	s = currentSample.getDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+1);
	double randD = 0.;//rng->genNormal(0, Proposals::GAMMA_NORMAL_GAUSSIAN_BM_STD_LARGE);
	d = currentSample.getDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+2)+randD;

	/// Coev Scaling
	if(isWithCoevScalingChange) { // Going from 0->1 and require scaling
		coevScaling = currentSample.getDoubleParameter(Parameters::COEV_SCALING_PARAMETER_OFFSET);
	} else {
		coevScaling = 1.;
	}

	/// Define the probabilities
	computeDrawPDF(ppi);

	//std::cout << "[InformedTransdimMoveCoevRJ] BW - fwRandomProb = " << fwRandomProb << "\t bwRandomProb = " << bwRandomProb << std::endl; // TODO DEBUG PRINT

	// Jacobian is 1. because new values are independently drawn from priors
	jacobian = 1.;

}

// Have to be called first - enable temporary changes
bool InformedTransdimMoveCoevRJ::doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {

	if(moveDirection == FORWARD) {
		// Create Coev Lik -> createdSSInfo is defined here
		createdSSInfo = utilsCRJ.createTempSubSpace(subSpaceUID, model, blockDispatcher);
		utilsCRJ.setValuesMSS(createdSSInfo, idProfile, r1, r2, s, d);
		updatedParameters = createdSSInfo->getParametersId();

		/// Coev scaling set the value to draw
		if(isWithCoevScalingChange) {
			proposedSample.setDoubleParameter(Parameters::COEV_SCALING_PARAMETER_OFFSET, coevScaling);
		}

		if(ptrLik->getNActiveCoevLikelihood() >= 1) {
			proposedSample.setDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET, r1);
			proposedSample.setDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+1, s);
			proposedSample.setDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+2, d);
			//std::cout << "HERE : " <<  r1 << "\t" << s << "\t" << d << std::endl;
		}

		//std::cout << "[InformedTransdimMoveCoevRJ] Try forward move : " << createdSSInfo->getUIDSubSpace()->getLabelUID() << std::endl; // FIXME DEBUG
	} else { // BACKWARD
		// Destroy things -> deletedSSInfo is searched in listSSInfo
		deletedSSInfo = utilsCRJ.disableSubSpace(subSpaceUID, listSSInfo, model, blockDispatcher);
		updatedParameters = deletedSSInfo->getParametersId();

		/// Coev scaling set the value to 0.0
		if(isWithCoevScalingChange) {
			proposedSample.setDoubleParameter(Parameters::COEV_SCALING_PARAMETER_OFFSET, 0.0);
		}

		if(ptrLik->getNActiveCoevLikelihood() == 0) {
			proposedSample.setDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET, 0.);
			proposedSample.setDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+1, 0.);
			proposedSample.setDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+2, 0.);
		} else {
			proposedSample.setDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET, r1);
			proposedSample.setDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+1, s);
			proposedSample.setDoubleParameter(Parameters::PRIOR_PARAMETERS_OFFSET+2, d);
		}

		//std::cout << "[InformedTransdimMoveCoevRJ] Try backward move : " << deletedSSInfo->getUIDSubSpace()->getLabelUID() << std::endl; // FIXME DEBUG
		/*if(isWithCoevScalingChange) { // FIXME DELETE
			return true;
		}*/
	}

	/*if(subSpaceUID->getLabelUID() == "622_670_") {
		std::cout << subSpaceUID->getLabelUID() << std::endl;
		std::cout.precision(4);
		std::cout << (moveDirection == FORWARD ? "[FW]" : "[BW]") << " r1 = " << std::fixed << r1 << "\t r2 = " << r2 << "\t s = " << s << "\t d = " << d << "\t scaling = " << currentSample.getDblValues()[5+1] << std::endl;
		return true;
	}*/

	return false;
}

// Have to be called second - let the choice between removing temporary changes or adapting lik
bool InformedTransdimMoveCoevRJ::doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	assert(moveState == APPLIED);
	// Apply the changes to listSSinfo
	if(moveDirection == FORWARD) {

		//std::cout << "[InformedTransdimMoveCoevRJ] Forward move accepted : " << subSpaceUID->getLabelUID() << std::endl; // FIXME DEBUG

		// Remove temp likelihood and register into permutedLikelihoods
		utilsCRJ.validateTempSubSpace(subSpaceUID);

		// Memorize changes
		listSSInfo.push_back(createdSSInfo);

		/// Coev scaling
		if(isWithCoevScalingChange) {
			ptrSSInfo_t createdBlockHPSSInfo = utilsCRJ.createCoevScalingBlock(model, blockDispatcher);
			listSSInfo.push_back(createdBlockHPSSInfo);
		}

		if(ptrLik->getNActiveCoevLikelihood() == 1) {
			ptrSSInfo_t createdBlockCPInfo = utilsCRJ.createSingleParamBlock(model, blockDispatcher);
			listSSInfo.push_back(createdBlockCPInfo);
		}

		// Keep value, nothing to do

		countAcceptedFW++;
	} else { // BACKWARD
		// Remove deletedSSinfo since the removal is accepted
		//std::cout << "[InformedTransdimMoveCoevRJ] Backward move accepted : " << subSpaceUID->getLabelUID() << std::endl; // FIXME DEBUG
		//std::cout << "[InformedTransdimMoveCoevRJ] Backward move PIND1 : ";
		//for(size_t i=0; i<deletedSSInfo->getParametersId().size(); ++i) std::cout << deletedSSInfo->getParametersId()[i] << "\t"; // FIXME DEBUG
		//std::cout << std::endl;
		utilsCRJ.deleteSubSpaceAfterDisable(subSpaceUID);
		listSSInfo.remove(deletedSSInfo);

		/// Coev scaling, parameter sampling is always active
		if(isWithCoevScalingChange) {
			ptrSSInfo_t deletedBlockHPSSInfo = utilsCRJ.deleteCoevScalingBlock(listSSInfo, blockDispatcher);
			listSSInfo.remove(deletedBlockHPSSInfo);
		}

		if(ptrLik->getNActiveCoevLikelihood() == 0) {
			ptrSSInfo_t deletedBlockParamInfo = utilsCRJ.deleteSingleParamBlock(listSSInfo, blockDispatcher);
			listSSInfo.remove(deletedBlockParamInfo);
		}

		countAcceptedBW++;
	}

	return false;
}

bool InformedTransdimMoveCoevRJ::doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	assert(moveState == APPLIED || moveState == IMPOSSIBLE);

	if(moveState == IMPOSSIBLE) return false;

	if(moveDirection == FORWARD) {
		// Move not kept : we delete it
		deletedSSInfo = utilsCRJ.deleteTempSubSpace(createdSSInfo, model, blockDispatcher);
		updatedParameters = deletedSSInfo->getParametersId();
		//std::cout << "[InformedTransdimMoveCoevRJ] Forward move rejected -> deleted : " << subSpaceUID->getLabelUID() << std::endl; // FIXME DEBUG
		// It has been created, then deleted
		// -> therefore we do not have to update the listSSInfo
		createdSSInfo.reset();
		deletedSSInfo.reset();
		countRejectedFW++;
	} else { // BACKWARD
		// Deletion of the coev cluster rejected : we re add it
		//createSubSpace(model, blockDispatcher);
		createdSSInfo = utilsCRJ.createSubSpaceAfterDisable(subSpaceUID, deletedSSInfo, model, blockDispatcher);
		utilsCRJ.setValuesMSS(createdSSInfo, idProfile, r1, r2, s, d);
		updatedParameters = createdSSInfo->getParametersId();
		//std::cout << "[InformedTransdimMoveCoevRJ] Backward move rejected -> recreate : " << subSpaceUID->getLabelUID() << std::endl; // FIXME DEBUG
		// It has been delete, then re-created
		// The indices of the parameters and block may thus have changed
		// We must signal it to the RJMCMC Handler
		// There we keep  createdSSRessources and deletedSSRessources as such
		assert((createdSSInfo != NULL) && (deletedSSInfo != NULL));
		listSSInfo.remove(deletedSSInfo); 			// Remove the original SSInfo
		listSSInfo.push_back(createdSSInfo); 		// Add the new one
		countRejectedBW++;
	}

	/// Coev scaling, parameter sampling is always active
	// Accept value, nothing to do

	return false;
}

void InformedTransdimMoveCoevRJ::computeDrawPDF(const PairProfileInfo &ppi) {
	fwRandomProb = 1.0;
	bwRandomProb = 1.0;

	if((moveDirection == FORWARD && ptrLik->getNActiveCoevLikelihood() == 0) ||
	   (moveDirection == BACKWARD && ptrLik->getNActiveCoevLikelihood() == 1)) {
		boost::math::normal_distribution<> distNormalR(Proposals::GAMMA_NORMAL_MU_R, Proposals::GAMMA_NORMAL_STD_R);
		fwRandomProb *= boost::math::pdf(distNormalR, r1);
		//fwRandomProb *= boost::math::pdf(distNormalR, r2);
		//boost::math::normal_distribution<> distNormalS(Proposals::GAMMA_NORMAL_MU_S, Proposals::GAMMA_NORMAL_STD_S);
		//fwRandomProb *= boost::math::pdf(distNormalS, s);
		boost::math::normal_distribution<> distNormalD(Proposals::GAMMA_NORMAL_MU_D, Proposals::GAMMA_NORMAL_STD_D);
		fwRandomProb *= boost::math::pdf(distNormalD, d);
	}

	double profileProb = utilsCRJ.getProfileProbability(idProfile, ppi);
	fwRandomProb *= profileProb;

	/** COEV SCALING! **/
	if(isWithCoevScalingChange) { // Going from 0->1 or 1->0
		boost::math::gamma_distribution<> distCoevScaling(Priors::BL_SCALING_ALPHA, Priors::BL_SCALING_BETA);
		fwRandomProb *= boost::math::pdf(distCoevScaling, coevScaling);
	}
}

bool InformedTransdimMoveCoevRJ::mustAddCoevScaling() const {

	assert(moveDirection == FORWARD);
	bool isTheMove = ptrLik->getNActiveCoevLikelihood() == 0; // Going from 0 Coev to 1

	return isTheMove;
}

bool InformedTransdimMoveCoevRJ::mustRemoveCoevScaling() const {
	assert(moveDirection == BACKWARD);
	bool isTheMove = ptrLik->getNActiveCoevLikelihood() == 1; // Going from 1 Coev to 0

	return isTheMove;
}



} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
