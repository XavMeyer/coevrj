//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file InitMoveCoevRJ.h
 *
 * @date Jul 6, 2017
 * @author meyerx
 * @brief
 */
#ifndef INITMOVECOEVRJ_H_
#define INITMOVECOEVRJ_H_

#include "Sampler/ReversibleJump/Moves/RJMove.h"

#include "UtilsCoevRJ.h"
#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/CoevRJ/UID/SubSpaceUID.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Sampler/Proposal/BlockDispatch/BlockDispatcher.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"
#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceInfo.h"
#include "Sampler/Samples/Sample.h"


namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class InitMoveCoevRJ: public Sampler::RJMCMC::RJMove {
public:
	InitMoveCoevRJ(size_t aPos1, size_t aPos2, std::string aProfile, std::vector<double> aValues, Sampler::Sample &aDummySample, Base* aPtrLik);
	~InitMoveCoevRJ();

private:

	typedef Sampler::Proposal::BlockDispatcher BlockDispatcher_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::listSSInfo_t listSSInfo_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::sharedPtr_t ptrSSInfo_t;

	size_t pos1, pos2, idProfile;
	std::string profile;
	std::vector<double> values;

	Base* ptrLik;
	RNG* rng;
	UtilsCoevRJ utilsCRJ;

	std::vector<MD::idProfile_t> vecProfilesId;
	SubSpaceUID::sharedPtr_t subSpaceUID;
	ptrSSInfo_t createdSSInfo, deletedSSInfo;

	bool isWithCoevScalingChange;

	// Have to be called first
	bool doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	// Have to be called second
	bool doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	bool doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	bool mustAddCoevScaling() const;
	bool mustRemoveCoevScaling() const;

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* INITMOVECOEVRJ_H_ */
