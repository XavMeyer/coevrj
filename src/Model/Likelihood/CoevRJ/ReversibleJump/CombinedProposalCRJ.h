//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombinedProposalCRJ.h
 *
 * @date May 6, 2017
 * @author meyerx
 * @brief
 */
#ifndef COMBINEDPROPOSALCRJ_H_
#define COMBINEDPROPOSALCRJ_H_

#include "Sampler/ReversibleJump/Moves/RJMove.h"
#include "Sampler/ReversibleJump/Proposals/RJMoveProposal.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class Base;

class CombinedProposalCRJ: public Sampler::RJMCMC::RJMoveProposal {
public:
	enum COMBINED_TYPE_ENUM {CONSTANT_CT=0, PROPORTIONAL_TO_K_CT=1};
	typedef COMBINED_TYPE_ENUM combinedType_t;
public:
	CombinedProposalCRJ(combinedType_t aProposalType, StatisticalModel::Model &aModel);
	~CombinedProposalCRJ();

	std::string getStringStats(std::string &prefix) const;

private:

	enum MOVES_ID {FORWARD_RAND_MOVE_ID=0, BACKWARD_RAND_MOVE_ID=1,
		           FORWARD_INFORMED_MOVE_ID=2, BACKWARD_INFORMED_MOVE_ID=3,
				   SWAP_GTR_MOVE_ID=4, SWAP_COEV_MOVE_ID=5, FORWARD_QUARTET_COEV_MOVE_ID=6, BACKWARD_QUARTET_COEV_MOVE_ID=7};
	static const std::vector<std::string> MOVES_NAME;
	static const double RANDOM_TRANSDIM_MOVE_PROBABILITY, INFORMED_TRANSDIM_MOVE_PROBABILITY;

	const combinedType_t COMBINED_TYPE;

	Base* ptrLik;

	Sampler::RJMCMC::RJMove::sharedPtr_t doPropose(Sampler::Sample &aSample);

	std::vector<double> defineMoveProbability(const size_t N_COEV_PAIRS);
	std::vector<double> defineCstMoveProbability(const size_t N_COEV_PAIRS);
	std::vector<double> definePropKMoveProbability(const size_t N_COEV_PAIRS);
};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COMBINEDPROPOSALCRJ_H_ */
