//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file UtilsCoevRJ.cpp
 *
 * @date May 5, 2017
 * @author meyerx
 * @brief
 */

#include "UtilsCoevRJ.h"


#include "Parallel/RNG/RNG.h"
#include "DefineConstantsCoev.h"
#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/CoevRJ/UID/HyperPriorUID.h"
#include "Model/Likelihood/CoevRJ/UID/CoevScalingUID.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/DefineConstantsCoev.h"

#include "Model/Likelihood/CoevRJ/UID/CoevParamsUID.h"

namespace Sampler { class ModelSpecificSample; }
namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

const double UtilsCoevRJ::RATE_COEV_PARAM = 0.1;
const std::vector<std::string> UtilsCoevRJ::PARAM_NAMES = boost::assign::list_of("R1")("R2")("S")("D");

UtilsCoevRJ::UtilsCoevRJ(Base* aPtrLik,	RNG* aRng, Sampler::Sample &aProposedSample) :
		ptrLik(aPtrLik), rng(aRng), proposedSample(aProposedSample) {
}

UtilsCoevRJ::~UtilsCoevRJ() {
}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::createTempSubSpace(const SubSpaceUID::sharedPtr_t &subSpaceUID,
													 StatisticalModel::Model &model,
													 BlockDispatcher_t &blockDispatcher) {
	// Create Coev Lik
	// DEBUG_TIME_UTILS_COEV double sTimeTotal = MPI_Wtime(); // FIXME
	UtilsCoevRJ::ptrSSInfo_t createdSSInfo(new Sampler::RJMCMC::RJSubSpaceInfo(subSpaceUID));
	//std::cout << "[UtilsCoevRJ] Create SubSpace = " << subSpaceUID->getLabelUID() << std::endl; // TODO DEBUG PRINT

	// DEBUG_TIME_UTILS_COEV double sTimeLik = MPI_Wtime(); // FIXME
	CoevLikelihoodInfo* newCLI = ptrLik->createTempCoevLikelihood(subSpaceUID);
	// DEBUG_TIME_UTILS_COEV double eTimeLik = MPI_Wtime(); // FIXME
	//std::cout << "[UtilsCoevRJ] Create CLI " << std::endl; // TODO DEBUG PRINT
	// DEBUG_TIME_UTILS_COEV double sTimeRessources = MPI_Wtime(); // FIXME
	createParameters(createdSSInfo, newCLI->getProfilesId(), model);
	//std::cout << "[UtilsCoevRJ] Create parameters " << std::endl; // TODO DEBUG PRINT
	newCLI->setPInd(createdSSInfo->getParametersId());
	//std::cout << "[UtilsCoevRJ] Set pInd " << std::endl; // TODO DEBUG PRINT
	createBlocks(createdSSInfo, newCLI->getPositions(), model, blockDispatcher);
	//std::cout << "[UtilsCoevRJ] Create blocks " << std::endl; // TODO DEBUG PRINT
	createMSS(createdSSInfo);
	//std::cout << "[UtilsCoevRJ] Create MSS " << std::endl; // TODO DEBUG PRINT
	// DEBUG_TIME_UTILS_COEV double eTimeRessources = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double sTimeRegister = MPI_Wtime(); // FIXME
	ptrLik->registerCoevDAG(newCLI, true);
	//std::cout << "[UtilsCoevRJ] registerCoevDAG " << std::endl; // TODO DEBUG PRINT
	// DEBUG_TIME_UTILS_COEV double eTimeRegister = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double eTimeTotal = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpace] Create time total : " << eTimeTotal - sTimeTotal << std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpace] Create time lik : " << eTimeLik - sTimeLik << " --> " << 100*(eTimeLik - sTimeLik)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpace] Create time res : " << eTimeRessources - sTimeRessources << " --> " << 100*(eTimeRessources - sTimeRessources)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpace] Create time reg : " << eTimeRegister - sTimeRegister << " --> " << 100*(eTimeRegister - sTimeRegister)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	return createdSSInfo;
}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::createTempSubSpaceWitoutSample(const SubSpaceUID::sharedPtr_t &subSpaceUID,
													 	 	 	 	 StatisticalModel::Model &model,
													 	 	 	 	 BlockDispatcher_t &blockDispatcher) {
	// Create Coev Lik
	// DEBUG_TIME_UTILS_COEV double sTimeTotal = MPI_Wtime(); // FIXME
	UtilsCoevRJ::ptrSSInfo_t createdSSInfo(new Sampler::RJMCMC::RJSubSpaceInfo(subSpaceUID));
	//std::cout << "[UtilsCoevRJ] Create SubSpace = " << subSpaceUID->getLabelUID() << std::endl; // TODO DEBUG PRINT

	// DEBUG_TIME_UTILS_COEV double sTimeLik = MPI_Wtime(); // FIXME
	CoevLikelihoodInfo* newCLI = ptrLik->createTempCoevLikelihood(subSpaceUID);
	// DEBUG_TIME_UTILS_COEV double eTimeLik = MPI_Wtime(); // FIXME
	//std::cout << "[UtilsCoevRJ] Create CLI " << std::endl; // TODO DEBUG PRINT
	// DEBUG_TIME_UTILS_COEV double sTimeRessources = MPI_Wtime(); // FIXME
	createParameters(createdSSInfo, newCLI->getProfilesId(), model);
	//std::cout << "[UtilsCoevRJ] Create parameters " << std::endl; // TODO DEBUG PRINT
	newCLI->setPInd(createdSSInfo->getParametersId());
	//std::cout << "[UtilsCoevRJ] Set pInd " << std::endl; // TODO DEBUG PRINT
	createBlocks(createdSSInfo, newCLI->getPositions(), model, blockDispatcher);
	//std::cout << "[UtilsCoevRJ] Create blocks " << std::endl; // TODO DEBUG PRINT
	// DEBUG_TIME_UTILS_COEV double eTimeRessources = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double sTimeRegister = MPI_Wtime(); // FIXME
	ptrLik->registerCoevDAG(newCLI, true);
	//std::cout << "[UtilsCoevRJ] registerCoevDAG " << std::endl; // TODO DEBUG PRINT
	// DEBUG_TIME_UTILS_COEV double eTimeRegister = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double eTimeTotal = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpace] Create time total : " << eTimeTotal - sTimeTotal << std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpace] Create time lik : " << eTimeLik - sTimeLik << " --> " << 100*(eTimeLik - sTimeLik)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpace] Create time res : " << eTimeRessources - sTimeRessources << " --> " << 100*(eTimeRessources - sTimeRessources)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpace] Create time reg : " << eTimeRegister - sTimeRegister << " --> " << 100*(eTimeRegister - sTimeRegister)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	return createdSSInfo;
}


UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::deleteTempSubSpace(const SubSpaceUID::sharedPtr_t &subSpaceUID, listSSInfo_t &listSSInfo,
			            	   	   	   	   	   	     StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	//double sTimeTotal = MPI_Wtime(); // FIXME
	// Delete Coev Lik
	listSSInfo_t::iterator it = std::find_if(listSSInfo.begin(), listSSInfo.end(), //...
			Sampler::RJMCMC::HasSSInfoSpecificUID(subSpaceUID));
	assert(it != listSSInfo.end());
	ptrSSInfo_t deletedSSInfo = *it;

	return deleteTempSubSpace(deletedSSInfo, model, blockDispatcher);
}


UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::deleteTempSubSpace(const ptrSSInfo_t &subSpaceInfo,
				            	   	   	   	   	   	 StatisticalModel::Model &model,
				            	   	   	   	   	   	 BlockDispatcher_t &blockDispatcher) {

	// DEBUG_TIME_UTILS_COEV double sTimeTotal = MPI_Wtime(); // FIXME

	CoevLikelihoodInfo* oldCLI = ptrLik->getActiveTempCoevLikelihood(subSpaceInfo->getUIDSubSpace()->getLabelUID());
	//std::cout << "[UtilsCoevRJ] Delete SubSpace = " << subSpaceUID->getLabelUID() << std::endl; // TODO DEBUG PRINT

	// DEBUG_TIME_UTILS_COEV double sTimeRessources = MPI_Wtime(); // FIXME
	deleteMSS(subSpaceInfo);
	deleteBlocks(subSpaceInfo, blockDispatcher);
	deleteParameters(subSpaceInfo, model);
	// DEBUG_TIME_UTILS_COEV double eTimeRessources = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double sTimeRegister = MPI_Wtime(); // FIXME
	ptrLik->unregisterCoevDAG(oldCLI, true);
	// DEBUG_TIME_UTILS_COEV double eTimeRegister = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double sTimeLik = MPI_Wtime(); // FIXME
	ptrLik->removeTempCoevLikelihood(subSpaceInfo->getUIDSubSpace()->getLabelUID());
	// DEBUG_TIME_UTILS_COEV double eTimeLik = MPI_Wtime(); // FIXME
	// DEBUG_TIME_UTILS_COEV double eTimeTotal = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV std::cout << "[deleteSubSpace] Total time : " << eTimeTotal - sTimeTotal << std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[deleteSubSpace] Delete time lik : " << eTimeLik - sTimeLik << " --> " << 100*(eTimeLik - sTimeLik)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[deleteSubSpace] Delete time res : " << eTimeRessources - sTimeRessources << " --> " << 100*(eTimeRessources - sTimeRessources)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[deleteSubSpace] Delete time reg : " << eTimeRegister - sTimeRegister << " --> " << 100*(eTimeRegister - sTimeRegister)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME

	return subSpaceInfo;
}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::disableSubSpace(const SubSpaceUID::sharedPtr_t &subSpaceUID, listSSInfo_t &listSSInfo,
							StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {


	listSSInfo_t::iterator it = std::find_if(listSSInfo.begin(), listSSInfo.end(),
			Sampler::RJMCMC::HasSSInfoSpecificUID(subSpaceUID));
	assert(it != listSSInfo.end());
	ptrSSInfo_t deletedSSInfo = *it;

	return UtilsCoevRJ::disableSubSpace(deletedSSInfo, model, blockDispatcher);
}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::disableSubSpace(const ptrSSInfo_t &subSpaceInfo,
													  StatisticalModel::Model &model,
													  BlockDispatcher_t &blockDispatcher) {

	// DEBUG_TIME_UTILS_COEV double sTimeTotal = MPI_Wtime(); // FIXME
	// We want to temporarily disable an existing/registetred subspace

	// DEBUG_TIME_UTILS_COEV double sTimeLik = MPI_Wtime(); // FIXME
	PermCoevLikelihoodInfo* oldCLI = ptrLik->getActivePermutedCoevLikelihood(subSpaceInfo->getUIDSubSpace()->getLabelUID());
	// DEBUG_TIME_UTILS_COEV double eTimeLik = MPI_Wtime(); // FIXME
	//std::cout << "[UtilsCoevRJ] Disable SubSpace = " << subSpaceInfo->getUIDSubSpace()->getLabelUID() << std::endl; // TODO DEBUG PRINT


	// DEBUG_TIME_UTILS_COEV double sTimeRessources = MPI_Wtime(); // FIXME
	deleteMSS(subSpaceInfo);
	deleteBlocks(subSpaceInfo, blockDispatcher);
	deleteParameters(subSpaceInfo, model);
	// DEBUG_TIME_UTILS_COEV double eTimeRessources = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double sTimeRegister = MPI_Wtime(); // FIXME
	//ptrLik->unregisterCoevDAG(oldCLI, true);
	ptrLik->disablePermutedCoevPair(oldCLI);
	// DEBUG_TIME_UTILS_COEV double eTimeRegister = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double eTimeTotal = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV std::cout << "[disableSubSpace] Total time : " << eTimeTotal - sTimeTotal << std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[disableSubSpace] Get lik  time : " << eTimeLik - sTimeLik << " --> " << 100*(eTimeLik - sTimeLik)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[disableSubSpace] Disable time ress : " << eTimeRessources - sTimeRessources << " --> " << 100*(eTimeRessources - sTimeRessources)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[disableSubSpace] Disable time unreg : " << eTimeRegister - sTimeRegister << " --> " << 100*(eTimeRegister - sTimeRegister)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	return subSpaceInfo;
}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::disableSubSpaceWithoutSample(const SubSpaceUID::sharedPtr_t &subSpaceUID, listSSInfo_t &listSSInfo,
							StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {


	listSSInfo_t::iterator it = std::find_if(listSSInfo.begin(), listSSInfo.end(),
			Sampler::RJMCMC::HasSSInfoSpecificUID(subSpaceUID));
	assert(it != listSSInfo.end());
	ptrSSInfo_t deletedSSInfo = *it;

	return UtilsCoevRJ::disableSubSpaceWithoutSample(deletedSSInfo, model, blockDispatcher);
}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::disableSubSpaceWithoutSample(const ptrSSInfo_t &subSpaceInfo,
													  StatisticalModel::Model &model,
													  BlockDispatcher_t &blockDispatcher) {

	// DEBUG_TIME_UTILS_COEV double sTimeTotal = MPI_Wtime(); // FIXME
	// We want to temporarily disable an existing/registetred subspace

	// DEBUG_TIME_UTILS_COEV double sTimeLik = MPI_Wtime(); // FIXME
	PermCoevLikelihoodInfo* oldCLI = ptrLik->getActivePermutedCoevLikelihood(subSpaceInfo->getUIDSubSpace()->getLabelUID());
	// DEBUG_TIME_UTILS_COEV double eTimeLik = MPI_Wtime(); // FIXME
	//std::cout << "[UtilsCoevRJ] Disable SubSpace NS = " << subSpaceInfo->getUIDSubSpace()->getLabelUID() << std::endl; // TODO DEBUG PRINT


	// DEBUG_TIME_UTILS_COEV double sTimeRessources = MPI_Wtime(); // FIXME
	deleteBlocks(subSpaceInfo, blockDispatcher);
	deleteParameters(subSpaceInfo, model);
	// DEBUG_TIME_UTILS_COEV double eTimeRessources = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double sTimeRegister = MPI_Wtime(); // FIXME
	//ptrLik->unregisterCoevDAG(oldCLI, true);
	ptrLik->disablePermutedCoevPair(oldCLI);
	// DEBUG_TIME_UTILS_COEV double eTimeRegister = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double eTimeTotal = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV std::cout << "[disableSubSpace] Total time : " << eTimeTotal - sTimeTotal << std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[disableSubSpace] Get lik  time : " << eTimeLik - sTimeLik << " --> " << 100*(eTimeLik - sTimeLik)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[disableSubSpace] Disable time ress : " << eTimeRessources - sTimeRessources << " --> " << 100*(eTimeRessources - sTimeRessources)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[disableSubSpace] Disable time unreg : " << eTimeRegister - sTimeRegister << " --> " << 100*(eTimeRegister - sTimeRegister)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	return subSpaceInfo;
}

void UtilsCoevRJ::deleteSubSpaceAfterDisable(const SubSpaceUID::sharedPtr_t &subSpaceUID) {
	// We want to delete the temporarily disable an existing/registered subspace

	//CoevLikelihoodInfo* oldCLI = ptrLik->getInactiveTempCoevLikelihood(subSpaceUID->getLabelUID());
	//std::cout << "[UtilsCosevRJ] Delete SubSpace = " << subSpaceUID->getLabelUID() << std::endl; // TODO DEBUG PRINT

	// DEBUG_TIME_UTILS_COEV double sTimeLik = MPI_Wtime(); // FIXME
	//ptrLik->removeCoevLikelihood(subSpaceUID->getLabelUID());
	ptrLik->removePermutedCoevPair(subSpaceUID->getLabelUID());
	// DEBUG_TIME_UTILS_COEV double eTimeLik = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV std::cout << "[deleteSubSpaceAfterDisable] Delete time lik : " << eTimeLik - sTimeLik << std::endl; // FIXME
}

void UtilsCoevRJ::validateTempSubSpace(const SubSpaceUID::sharedPtr_t &subSpaceUID) {

	CoevLikelihoodInfo* ptrCLI = ptrLik->getActiveTempCoevLikelihood(subSpaceUID->getLabelUID());

	Sampler::ModelSpecificSample &mss = proposedSample.getMSS(subSpaceUID);
	size_t idProfile = mss.getIntParameter(0);

	std::vector<size_t> pInd = ptrCLI->getPInd();

	// Remove from base
	ptrLik->unregisterCoevDAG(ptrCLI, true);
	ptrLik->removeTempCoevLikelihood(subSpaceUID->getLabelUID());

	// Add to PermutedCoevLikelihood
	PermCoevLikelihoodInfo* ptrPCLI = ptrLik->createPermutedCoevPair(subSpaceUID, idProfile);
	ptrPCLI->setPInd(pInd);

}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::createSubSpaceAfterDisable(const SubSpaceUID::sharedPtr_t &subSpaceUID, ptrSSInfo_t &disabledSSInfo,
								  	  	  	  	  	  	  	  	 StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	// Create Coev Lik
	assert(disabledSSInfo != NULL);
	// DEBUG_TIME_UTILS_COEV double sTimeTotal = MPI_Wtime(); // FIXME
	ptrSSInfo_t createdSSInfo(new Sampler::RJMCMC::RJSubSpaceInfo(subSpaceUID));
	createdSSInfo->setParametersId(disabledSSInfo->getParametersId());
	//std::cout << "[UtilsCoevRJ] Create SubSpace = " << subSpaceUID->getLabelUID() << std::endl; // TODO DEBUG PRINT

	// DEBUG_TIME_UTILS_COEV double sTimeLik = MPI_Wtime(); // FIXME
	PermCoevLikelihoodInfo* oldCLI = ptrLik->getInactivePermuedCoevLikelihood(subSpaceUID->getLabelUID());
	// DEBUG_TIME_UTILS_COEV double eTimeLik = MPI_Wtime(); // FIXME
	//std::cout << "[UtilsCoevRJ] Create CLI " << std::endl; // TODO DEBUG PRINT
	// DEBUG_TIME_UTILS_COEV double sTimeRessources = MPI_Wtime(); // FIXME
	createParameters(createdSSInfo, oldCLI->getProfilesId(), model);
	oldCLI->setPInd(createdSSInfo->getParametersId());
	//std::cout << "[UtilsCoevRJ] Create parameters " << std::endl; // TODO DEBUG PRINT
	createBlocks(createdSSInfo, oldCLI->getPositions(), model, blockDispatcher);
	//std::cout << "[UtilsCoevRJ] Create blocks " << std::endl; // TODO DEBUG PRINT
	createMSS(createdSSInfo);
	//std::cout << "[UtilsCoevRJ] Create MSS " << std::endl; // TODO DEBUG PRINT
	// DEBUG_TIME_UTILS_COEV double eTimeRessources = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV double sTimeRegister = MPI_Wtime(); // FIXME
	ptrLik->enablePermutedCoevPair(oldCLI);
	// DEBUG_TIME_UTILS_COEV double eTimeRegister = MPI_Wtime(); // FIXME
	//std::cout << "[UtilsCoevRJ] register DAG MSS " << std::endl; // TODO DEBUG PRINT

	// DEBUG_TIME_UTILS_COEV double eTimeTotal = MPI_Wtime(); // FIXME

	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpaceAfterDisable] Create time total : " << eTimeTotal - sTimeTotal << std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpaceAfterDisable] Recover time lik : " << eTimeLik - sTimeLik << " --> " << 100*(eTimeLik - sTimeLik)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpaceAfterDisable] Create time res : " << eTimeRessources - sTimeRessources << " --> " << 100*(eTimeRessources - sTimeRessources)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	// DEBUG_TIME_UTILS_COEV std::cout << "[createSubSpaceAfterDisable] Create time reg : " << eTimeRegister - sTimeRegister << " --> " << 100*(eTimeRegister - sTimeRegister)/(eTimeTotal - sTimeTotal)<< std::endl; // FIXME
	return createdSSInfo;
}

void UtilsCoevRJ::setValuesMSS(ptrSSInfo_t &createdSSInfo, size_t idProfile, const std::vector<double> &vals) {
	assert(vals.size() == 4);
	setValuesMSS(createdSSInfo, idProfile, vals[0], vals[1], vals[2], vals[3]);
}

void UtilsCoevRJ::setValuesMSS(ptrSSInfo_t &createdSSInfo, size_t idProfile, double r1, double r2, double s, double d) {
	assert(createdSSInfo != NULL);
	Sampler::ModelSpecificSample &mss = proposedSample.getMSS(createdSSInfo->getUIDSubSpace());
	// Draw a random value between [0..#nb of profile]
	mss.setIntParameter(0, idProfile);
	mss.setDoubleParameter(1, r1); 		// r1
	mss.setDoubleParameter(2, r2); 		// r2
	mss.setDoubleParameter(3, s); 		// s
	mss.setDoubleParameter(4, d); 		// d
}

/// Draw the pair of sites randomly
std::vector<size_t> UtilsCoevRJ::drawSitesPosUniform(const size_t N_GTR, std::set<size_t> sitesGTR) {
	assert(N_GTR <= sitesGTR.size());
	std::vector<size_t> positions;

	for(size_t iP=0; iP<N_GTR; ++iP) {
		size_t offsetPos = rng->genUniformInt(0, sitesGTR.size()-1);

		// Get first pos
		std::set<size_t>::iterator it = sitesGTR.begin();
		std::advance(it, offsetPos);
		positions.push_back(*it);

		// Erase it from temporary set
		sitesGTR.erase(it);
	}
	std::sort(positions.begin(), positions.end());

	return positions;
}

UtilsCoevRJ::UnormalizedProbabilities UtilsCoevRJ::defineProbabilityBySitesAtFirstPosition(const boost::dynamic_bitset<> &sitesAvailability) {

	const std::vector<score_t> &pos1Score = ptrLik->getCoevAlignManager()->getPositionsScore();

	UnormalizedProbabilities res;
	res.probabilities = pos1Score;
	res.normalizingConstant = 0.;
	//double sumProbA = 0.;
	for(size_t iP=0; iP<pos1Score.size(); ++iP) {

		// If the site is not available
		if(!sitesAvailability[iP]) {
			res.probabilities[iP] = 0.;
		}
		res.normalizingConstant += res.probabilities[iP]; // ProbIp is already the score
	}

	return res;
}

UtilsCoevRJ::UnormalizedProbabilities UtilsCoevRJ::defineProbabilityBySitesAtSecondPosition(size_t iPos1, const boost::dynamic_bitset<> &sitesAvailability) {

	const std::vector<score_t> &pos2Score = ptrLik->getCoevAlignManager()->getPairsIncludingPositionScore(iPos1);

	UnormalizedProbabilities res;
	res.probabilities = pos2Score;
	res.normalizingConstant = 0.;
	//double sumProbA = 0.;
	for(size_t iP=0; iP<pos2Score.size(); ++iP) {

		// If the site is not available
		if(!sitesAvailability[iP]) {
			res.probabilities[iP] = 0.;
		}
		res.normalizingConstant += res.probabilities[iP]; // ProbIp is already the score
	}

	return res;
}

/// Draw the coev likelihood to suppress
PermCoevLikelihoodInfo* UtilsCoevRJ::drawCoevLik() {
	std::list<PermCoevLikelihoodInfo*> &vecCoevLI = ptrLik->getActivePermCoevLikelihoods();
	size_t offsetCLI = rng->genUniformInt(0, vecCoevLI.size()-1);

	std::list<PermCoevLikelihoodInfo*>::iterator it=vecCoevLI.begin();
	std::advance(it, offsetCLI);
	return (*it);
}

std::vector<MD::idProfile_t> UtilsCoevRJ::copyProfileIds(const PairProfileInfo &ppi) const {
	if(ppi.uniform) {
		return ptrLik->getCoevAlignManager()->FULL_PROFILE_ID;
	} else {
		return ppi.profilesId;
	}
}

std::pair<MD::idProfile_t, double> UtilsCoevRJ::drawProfile(const PairProfileInfo &ppi) const {
	std::pair<MD::idProfile_t, double> result;

	if(ppi.uniform) {
		size_t idProf = rng->genUniformInt(0, MD::getNucleotidesPairProfiles()->NB_POSSIBLE_PROFILES-1);
		result.first = idProf;
		result.second = (1./(double)MD::getNucleotidesPairProfiles()->NB_POSSIBLE_PROFILES);

	} else {

		// return 0 if such profile is not observable
		if(ppi.profileProb.empty()) {
			result.first = 0;
			result.second = 0.;
			return result;
		}

		size_t idRand = rng->drawFrom(ppi.profileProb);
		/*if(idRand >= ppi.profilesId.size()) {
			std::cout << "idRand = " << idRand << "\t PPI size : " << ppi.profilesId.size() << std::endl;
			for(size_t iP=0; iP<ppi.profilesId.size(); ++iP) std::cout << ppi.profilesId[iP] << "\t";
			std::cout << std::endl;
			for(size_t iP=0; iP<ppi.profileProb.size(); ++iP) std::cout << ppi.profileProb[iP] << "\t";
			std::cout << std::endl;
		}*/
		assert(idRand < ppi.profilesId.size()); // FIXME REMOVE WHEN SECURE
		result.first = ppi.profilesId[idRand];
		result.second = ppi.profileProb[idRand];
	}

	return result;
}

score_t UtilsCoevRJ::getProfileProbability(size_t profileId, const PairProfileInfo &ppi) const {

	if(ppi.uniform) {
		return (1./(double)MD::getNucleotidesPairProfiles()->NB_POSSIBLE_PROFILES);
	} else {

		// return 0 if such profile is not observable
		if(ppi.profileProb.empty())	return 0.;

		double profileProb = 0.;
		for(size_t iP=0; iP<ppi.profilesId.size(); ++iP) {
			if(ppi.profilesId[iP] == profileId) {
				profileProb = ppi.profileProb[iP];
				break;
			}
		}
		assert(profileProb != 0.);
		return profileProb;
	}
}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::createCoevScalingBlock(StatisticalModel::Model &model,
        						   BlockDispatcher_t &blockDispatcher) {

	using ParameterBlock::Block;
	using ParameterBlock::BlockModifier;

	CoevScalingUID::sharedPtr_t scalingUID(new CoevScalingUID());
	UtilsCoevRJ::ptrSSInfo_t createdSSInfo(new Sampler::RJMCMC::RJSubSpaceInfo(scalingUID));

	/*{
		Block::sharedPtr_t bCoevGammaScaling(new Block(Block::NOT_ADAPTIVE));
		bCoevGammaScaling->setName("Gamma and Scaling");
		std::vector<size_t> params = boost::assign::list_of(Parameters::GAMMA_RATE_PARAMETER_OFFSET)(Parameters::COEV_SCALING_PARAMETER_OFFSET);

		for(size_t iP=0; iP<params.size(); ++iP) {
			size_t iParam = params[iP];
			if(model.getParams().getBoundMin(iParam) < model.getParams().getBoundMax(iParam)) {
				bCoevGammaScaling->addParameter(iParam,
						BlockModifier::createMultiplierWindow(1.3, model),
						model.getParams().getParameterFreq(iParam));
			}
		}
		size_t nAdded = blockDispatcher.addDynamicBlock(bCoevGammaScaling);
		assert(nAdded > 0);
		createdSSInfo->addBlockId(bCoevGammaScaling->getId());
	}*/

	{
		/* ADD A SECOND BLOCK WITH ONLY SCALING */
		Block::sharedPtr_t bCoevScaling(new Block(Block::NOT_ADAPTIVE));
		bCoevScaling->setName("Scaling");
		std::vector<size_t> params = boost::assign::list_of(Parameters::COEV_SCALING_PARAMETER_OFFSET);

		for(size_t iP=0; iP<params.size(); ++iP) {
			size_t iParam = params[iP];
			if(model.getParams().getBoundMin(iParam) < model.getParams().getBoundMax(iParam)) {
				bCoevScaling->addParameter(iParam,
						BlockModifier::createMultiplierWindow(1.15, model),
						model.getParams().getParameterFreq(iParam));
			}
		}
		size_t nAdded = blockDispatcher.addDynamicBlock(bCoevScaling);
		assert(nAdded > 0);
		createdSSInfo->addBlockId(bCoevScaling->getId());
	}


	{
		/* ADD A SECOND BLOCK WITH ONLY SCALING */
		Block::sharedPtr_t bCoevScaling2(new Block(Block::NOT_ADAPTIVE));
		bCoevScaling2->setName("Scaling and BL");
		std::vector<size_t> params = boost::assign::list_of(Parameters::COEV_SCALING_PARAMETER_OFFSET);
		const size_t BRANCH_LENGTH_OFFSET = Parameters::PRIOR_PARAMETERS_OFFSET + Parameters::SINGLE_NB_PARAMS;
		for(size_t i=0; i<ptrLik->getNBranch(); ++i) {
			params.push_back(i+BRANCH_LENGTH_OFFSET);
		}

		for(size_t iP=0; iP<params.size(); ++iP) {
			size_t iParam = params[iP];
			if(model.getParams().getBoundMin(iParam) < model.getParams().getBoundMax(iParam)) {
				bCoevScaling2->addParameter(iParam,
						//BlockModifier::createMultiplierWindow(1.15, model),
						model.getParams().getParameterFreq(iParam));
			}
		}

		const double STD_SCALING_FACTOR = 0.05;
		const double STD_COEV = STD_SCALING_FACTOR*0.1;
		const double STD_TOTAL_BL = STD_SCALING_FACTOR*0.25;
		const double STD_BL = sqrt((STD_TOTAL_BL*STD_TOTAL_BL)/ptrLik->getNBranch());
		const double CORRELATION_COEF = 0.95;
		std::vector<double> sigma(params.size()*params.size(), 0.);
		for(size_t i=0; i<params.size(); ++i) {
			for(size_t j=0; j<params.size(); ++j) {
				if(i == 0 && j == 0) {
					sigma[i*params.size() + j] = STD_COEV*STD_COEV;
				} else if(i == j) {
					sigma[i*params.size() + j] = STD_BL*STD_BL;
				} else if(i==0 || j==0) {
					sigma[i*params.size() + j] = -1.*CORRELATION_COEF*STD_BL*STD_COEV;
				} else {
					sigma[i*params.size() + j] = CORRELATION_COEF*STD_BL*STD_BL;
				}
			}
		}

		bCoevScaling2->setBlockModifier(BlockModifier::createMVGaussianWindow(params.size(), sigma.data(), model));
		size_t nAdded = blockDispatcher.addDynamicBlock(bCoevScaling2);
		assert(nAdded > 0);
		createdSSInfo->addBlockId(bCoevScaling2->getId());
	}

	return createdSSInfo;
}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::deleteCoevScalingBlock(listSSInfo_t &listSSInfo,
        						   BlockDispatcher_t &blockDispatcher) {
	CoevScalingUID::sharedPtr_t hpUID(new CoevScalingUID());
	listSSInfo_t::iterator it = std::find_if(listSSInfo.begin(), listSSInfo.end(), //...
			Sampler::RJMCMC::HasSSInfoSpecificUID(hpUID));
	assert(it != listSSInfo.end());
	ptrSSInfo_t deletedSSInfo = *it;

	const std::vector<size_t> &blocksId = deletedSSInfo->getBlocksId();
	for(size_t iB=0; iB<blocksId.size(); ++iB) {
		//std::cout << "[UtilsCoevRJ] Deleting block = " << blocksId[iB] << std::endl; // TODO DEBUG PRINT
		size_t nDeleted = blockDispatcher.removeDynamicBlock(blocksId[iB]);
		assert(nDeleted > 0);
	}
	return deletedSSInfo;
}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::createSingleParamBlock(StatisticalModel::Model &model,
            					   	   	   	   	   	   	   	 BlockDispatcher_t &blockDispatcher) {

	using ParameterBlock::Block;
	using ParameterBlock::BlockModifier;

	CoevParamsUID::sharedPtr_t coevparamUID(new CoevParamsUID());
	UtilsCoevRJ::ptrSSInfo_t createdSSInfo(new Sampler::RJMCMC::RJSubSpaceInfo(coevparamUID));

	{	// Rate

		std::vector<size_t> pInd = boost::assign::list_of(Parameters::PRIOR_PARAMETERS_OFFSET)
														 (Parameters::PRIOR_PARAMETERS_OFFSET+1)
														 (Parameters::PRIOR_PARAMETERS_OFFSET+2);

		Block::sharedPtr_t bRateCoev(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		std::stringstream ssNameRateCoev;
		ssNameRateCoev << createdSSInfo->getUIDSubSpace()->getLabelUID() << "_rate";
		bRateCoev->setName(ssNameRateCoev.str());

		for(size_t iP=0; iP<pInd.size(); ++iP) {
			BlockModifier::sharedPtr_t ptrBlockMod;
			ptrBlockMod = BlockModifier::createGaussianWindow(Proposals::GAMMA_NORMAL_GAUSSIAN_BM_STD_SMALL, model);

			if(model.getParams().getBoundMin(pInd[iP]) < model.getParams().getBoundMax(pInd[iP])) {
						bRateCoev->addParameter(pInd[iP],
						ptrBlockMod,
						model.getParams().getParameterFreq(pInd[iP]));
			}
		}

		size_t nAdded = blockDispatcher.addDynamicBlock(bRateCoev);
		assert(nAdded > 0);
		createdSSInfo->addBlockId(bRateCoev->getId());
	}

	/*{
		Block::sharedPtr_t bCoevScaling(new Block(Block::NOT_ADAPTIVE));
		bCoevScaling->setName("Scaling_rate");

		std::vector<size_t> pInd = boost::assign::list_of(Parameters::COEV_SCALING_PARAMETER_OFFSET)
														 (Parameters::PRIOR_PARAMETERS_OFFSET)
														 (Parameters::PRIOR_PARAMETERS_OFFSET+1)
														 (Parameters::PRIOR_PARAMETERS_OFFSET+2);


		for(size_t iP=0; iP<pInd.size(); ++iP) {
			size_t iParam = pInd[iP];
			if(model.getParams().getBoundMin(iParam) < model.getParams().getBoundMax(iParam)) {
				bCoevScaling->addParameter(iParam,
						BlockModifier::createMultiplierWindow(1.1, model),
						model.getParams().getParameterFreq(iParam));
			}
		}
		size_t nAdded = blockDispatcher.addDynamicBlock(bCoevScaling);
		assert(nAdded > 0);
		createdSSInfo->addBlockId(bCoevScaling->getId());
	}*/

	return createdSSInfo;

}

UtilsCoevRJ::ptrSSInfo_t UtilsCoevRJ::deleteSingleParamBlock(listSSInfo_t &listSSInfo,
								   	   	   	   	   	   	   	 BlockDispatcher_t &blockDispatcher) {
	CoevParamsUID::sharedPtr_t coevparamUID(new CoevParamsUID());
	listSSInfo_t::iterator it = std::find_if(listSSInfo.begin(), listSSInfo.end(), //...
			Sampler::RJMCMC::HasSSInfoSpecificUID(coevparamUID));
	assert(it != listSSInfo.end());
	ptrSSInfo_t deletedSSInfo = *it;

	const std::vector<size_t> &blocksId = deletedSSInfo->getBlocksId();
	for(size_t iB=0; iB<blocksId.size(); ++iB) {
		//std::cout << "[UtilsCoevRJ] Deleting block = " << blocksId[iB] << std::endl; // TODO DEBUG PRINT
		size_t nDeleted = blockDispatcher.removeDynamicBlock(blocksId[iB]);
		assert(nDeleted > 0);
	}
	return deletedSSInfo;
}




void UtilsCoevRJ::createParameters(ptrSSInfo_t &createdSSInfo, const std::vector<MD::idProfile_t>& profilesId, StatisticalModel::Model &model) {

	std::vector<size_t> pInd(createdSSInfo->getParametersId());
	bool doRestoreParameters = !pInd.empty();

	std::stringstream ssProfName;
	ssProfName << createdSSInfo->getUIDSubSpace()->getLabelUID() << "_";
	ssProfName << "Prof";
	ParamIntDef pProfile(ssProfName.str(), PriorInterface::createDiscreteUniform(rng, profilesId.size()), true); //FIXME
	//ParamIntDef pProfile(ssProfName.str(), PriorInterface::createDiscreteUniform(rng, MD::getNucleotidesPairProfiles()->NB_POSSIBLE_PROFILES), true); //FIXME
	pProfile.setFreq(RATE_COEV_PARAM);
	pProfile.setType(StatisticalModel::COEV_PARAM);

	if(doRestoreParameters) {
		model.getParams().restoreDynamicParameter(pInd[0], pProfile);
	} else {
		pInd.push_back(model.getParams().addDynamicParameter(pProfile));
	}

	// Gamma parametrization shape=alpha, scale=1/beta
	const bool USE_UNIFORM_PRIORS = false;
	std::vector<PriorInterface::sharedPtr_t> priorsCoev(PARAM_NAMES.size());
	priorsCoev[0] = PriorInterface::createNoPrior();
	priorsCoev[1] = PriorInterface::createNoPrior();
	priorsCoev[2] = PriorInterface::createNoPrior();
	priorsCoev[3] = PriorInterface::createNoPrior();

	// Create parameters
	for(size_t iP=0; iP<PARAM_NAMES.size(); ++iP) {
		std::stringstream ssName;
		ssName << createdSSInfo->getUIDSubSpace()->getLabelUID() << "_";
		ssName << PARAM_NAMES[iP];

		ParamDblDef pCoev(ssName.str(), priorsCoev[iP], true);
		pCoev.setFreq(RATE_COEV_PARAM);
		pCoev.setMin(-100.);
		pCoev.setMax(100.);
		pCoev.setType(StatisticalModel::COEV_PARAM);

		if(doRestoreParameters) {
			model.getParams().restoreDynamicParameter(pInd[iP+1], pCoev);
		} else {
			pInd.push_back(model.getParams().addDynamicParameter(pCoev));
		}
		//std::cout << "Create P[ " << iP+1 << " ] = " << pInd[iP+1] << "\t"; // TODO REMOVE
	}
	//std::cout << std::endl; // TODO REMOVE

	// Save the pInd for later
	if(!doRestoreParameters) {
		createdSSInfo->setParametersId(pInd);
	}
}

void UtilsCoevRJ::deleteParameters(const ptrSSInfo_t &deletedSSInfo, StatisticalModel::Model &model) {
	const std::vector<size_t> &pInd = deletedSSInfo->getParametersId();
	//std::cout << "Delete : " << deletedSSInfo->getUIDSubSpace()->getLabelUID() << std::endl; // TODO REMOVE
	for(size_t iP=0; iP<pInd.size(); ++iP) {
		//std::cout << "Delete P[ " << iP << " ] = " << pInd[iP] << "\t"; // TODO REMOVE
		model.getParams().removeDynamicParameter(pInd[iP]);
	}
	//std::cout << std::endl; // TODO REMOVE
}

void UtilsCoevRJ::createBlocks(ptrSSInfo_t &createdSSInfo, std::pair<size_t, size_t> positions,
							   StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {

	defineCoevBlocksForSingleParam(createdSSInfo, positions, model, blockDispatcher);
}


void UtilsCoevRJ::defineCoevBlocksForSingleParam(ptrSSInfo_t &createdSSInfo, std::pair<size_t, size_t> positions,
		   	   	   	   	   	   	   	   	     	 StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {

	using ParameterBlock::Block;
	using ParameterBlock::BlockModifier;
	const std::vector<size_t> &pInd = createdSSInfo->getParametersId();

	{	// Discrete move
		Block::sharedPtr_t bDiscreteCoev(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		std::stringstream ssNameDiscreteCoev;
		ssNameDiscreteCoev << createdSSInfo->getUIDSubSpace()->getLabelUID() << "_discrete1";
		bDiscreteCoev->setName(ssNameDiscreteCoev.str());

		PairProfileInfo ppi = ptrLik->getCoevAlignManager()->getPairProfileInfo(positions.first, positions.second);

		BlockModifier::sharedPtr_t ptrProfBlockMod;
		if(ppi.uniform) {
			ptrProfBlockMod = BlockModifier::createDiscreteCategoriesBM(ptrLik->getCoevAlignManager()->FULL_PROFILE_ID, model);
		} else {
			ptrProfBlockMod = BlockModifier::createWeightedDiscreteCategoriesBM(ppi.profilesId, ppi.profileProb, model);
		}
		bDiscreteCoev->addParameter(pInd[0], ptrProfBlockMod, model.getParams().getParameterFreq(pInd[0]));

		size_t nAdded = blockDispatcher.addDynamicBlock(bDiscreteCoev);
		assert(nAdded > 0);
		createdSSInfo->addBlockId(bDiscreteCoev->getId());
		//std::cout << "Add block : " << ssNameDiscreteCoev.str() << "\t with ID = " << bDiscreteCoev->getId() << std::endl;
	}
}

void UtilsCoevRJ::deleteBlocks(const ptrSSInfo_t &deletedSSInfo, BlockDispatcher_t &blockDispatcher) {
	const std::vector<size_t> &blocksId = deletedSSInfo->getBlocksId();
	for(size_t iB=0; iB<blocksId.size(); ++iB) {
		//std::cout << "[UtilsCoevRJ] Deleting block = " << blocksId[iB] << std::endl; // TODO DEBUG PRINT
		size_t nDeleted = blockDispatcher.removeDynamicBlock(blocksId[iB]);
		assert(nDeleted > 0);
	}
}

void UtilsCoevRJ::createMSS(ptrSSInfo_t &createdSSInfo) {
	size_t nInt = 1;
	const std::vector<size_t> &pInd = createdSSInfo->getParametersId();
	Sampler::ModelSpecificSample mss(createdSSInfo->getUIDSubSpace(), nInt, pInd);
	proposedSample.registerMSS(mss);
}

void UtilsCoevRJ::deleteMSS(const ptrSSInfo_t &deletedSSInfo) {
	proposedSample.removeMSS(deletedSSInfo->getUIDSubSpace());
	//std::cout << "[UtilsCoevRJ] Delete MSS = " << subSpaceUID->getLabelUID() << std::endl; // TODO DEBUG PRINT
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
