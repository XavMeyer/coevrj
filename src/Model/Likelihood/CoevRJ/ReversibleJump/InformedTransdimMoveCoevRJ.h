//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file InformedTransdimMoveCoevRJ.h
 *
 * @date May 10, 2017
 * @author meyerx
 * @brief
 */
#ifndef INFORMEDTRANSDIMMOVECOEVRJ_H_
#define INFORMEDTRANSDIMMOVECOEVRJ_H_

#include <stddef.h>

#include "UtilsCoevRJ.h"
#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/CoevRJ/UID/SubSpaceUID.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Sampler/Proposal/BlockDispatch/BlockDispatcher.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"
#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceInfo.h"

class RNG;
namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class Base;
class CoevLikelihoodInfo;

class InformedTransdimMoveCoevRJ: public Sampler::RJMCMC::RJMove {
public:
	InformedTransdimMoveCoevRJ(moveDirection_t aMoveDirection, Sampler::Sample &aCurrentSample,
			   Base* aPtrLik, RNG *aRng);
	~InformedTransdimMoveCoevRJ();

	static std::string getStringStats(std::string &prefix);

private:

	typedef Sampler::Proposal::BlockDispatcher BlockDispatcher_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::listSSInfo_t listSSInfo_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::sharedPtr_t ptrSSInfo_t;

	static const double FREQ_INFORMED_MOVES;

	static size_t countAcceptedFW, countRejectedFW, countAcceptedBW, countRejectedBW;

	Base* ptrLik;
	RNG* rng;

	UtilsCoevRJ utilsCRJ;

	double r1, r2, d, s;

	size_t idProfile;
	std::vector<MD::idProfile_t> vecProfilesId;
	SubSpaceUID::sharedPtr_t subSpaceUID;
	ptrSSInfo_t createdSSInfo, deletedSSInfo;

	// For prior parameters (hyperpriors)
	bool isWithCoevScalingChange;
	double coevScaling;
	std::vector<double> priorParamValues;

	// Have to be called first
	bool doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	// Have to be called second
	bool doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	bool doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	/// Define the moves, draw random number, define the probabilities (q(m), q(u))
	/// Set everything that has to be known for the forward move
	void defineForwardMove();
	/// Set everything that has to be known for the backward move
	void defineBackwardMove();

	/// Define q(u')
	void computeDrawPDF(const PairProfileInfo &ppi);

	bool mustAddCoevScaling() const;
	bool mustRemoveCoevScaling() const;

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* INFORMEDTRANSDIMMOVECOEVRJ_H_ */
