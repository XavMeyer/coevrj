//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SwapCoevMoveRJ.cpp
 *
 * @date May 6, 2017
 * @author meyerx
 * @brief
 */
#include "SwapCoevMoveRJ.h"

#include <assert.h>
#include <stddef.h>

#include "Model/Model.h"
#include "Parallel/RNG/RNG.h"
#include "Model/Likelihood/CoevRJ/Base.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

size_t SwapCoevMoveRJ::countAccepted = 0;
size_t SwapCoevMoveRJ::countRejected = 0;

SwapCoevMoveRJ::SwapCoevMoveRJ(Sampler::Sample &aCurrentSample, Base* aPtrLik, RNG *aRng) :
							   Sampler::RJMCMC::RJMove(FORWARD, aCurrentSample),
							   ptrLik(aPtrLik), rng(aRng), utilsCRJ(ptrLik, rng, proposedSample) {

	canApply = true;
	newIdProfile_A = newIdProfile_B = 0;
	defineMove();
}

SwapCoevMoveRJ::~SwapCoevMoveRJ() {
}

std::string SwapCoevMoveRJ::getStringStats(std::string &prefix) {
	std::stringstream ss;
	double ratio = (countAccepted+countRejected) == 0 ? 0. : (double)countAccepted/(double)(countAccepted+countRejected);
	ss << prefix << "[SwapCoevMoveRJ] Move acceptance ratio = " << ratio;
	ss << " ( " << countAccepted << " / " << (countAccepted+countRejected) << " )" << std::endl;
	return ss.str();
}

void SwapCoevMoveRJ::defineMove() {

	/// OLD PAIRS : First we draw two coev cluster
	const size_t N_COEV = ptrLik->getNActiveCoevLikelihood();
	if(N_COEV < 2) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_COEV < 1!");
		return;
	}
	// Draw first
	PermCoevLikelihoodInfo* ptrCLI_A = utilsCRJ.drawCoevLik();
	oldSubSpaceUID_A = ptrCLI_A->getUIDSubSpace();
	oldVecProfilesId_A = ptrCLI_A->getProfilesId();
	oldMSS_A = currentSample.getMSS(oldSubSpaceUID_A);
	PairProfileInfo oldPPI_A = ptrLik->getCoevAlignManager()->getPairProfileInfo(ptrCLI_A->getPositions().first, ptrCLI_A->getPositions().second);
	double oldProfileProb_A = utilsCRJ.getProfileProbability(oldMSS_A.getIntValues()[0], oldPPI_A);

	// Draw second
	PermCoevLikelihoodInfo* ptrCLI_B = utilsCRJ.drawCoevLik();
	while(ptrCLI_B == ptrCLI_A) { // We want a different coev pair
		ptrCLI_B = utilsCRJ.drawCoevLik();
	}
	oldSubSpaceUID_B = ptrCLI_B->getUIDSubSpace();
	oldVecProfilesId_B = ptrCLI_B->getProfilesId();
	oldMSS_B = currentSample.getMSS(oldSubSpaceUID_B);
	PairProfileInfo oldPPI_B = ptrLik->getCoevAlignManager()->getPairProfileInfo(ptrCLI_B->getPositions().first, ptrCLI_B->getPositions().second);
	double oldProfileProb_B = utilsCRJ.getProfileProbability(oldMSS_B.getIntValues()[0], oldPPI_B);

	// Decide how to mix pairs:
	// Is it <A.pos1 with B.pos1> or <A.pos1 with B.pos2>
	bool keepOrder = rng->genUniformInt(0, 1) == 0;
	std::pair<size_t, size_t> positions_A, positions_B;
	if(keepOrder) {
		positions_A = std::make_pair(ptrCLI_A->getPositions().first, ptrCLI_B->getPositions().first);
		positions_B = std::make_pair(ptrCLI_A->getPositions().second, ptrCLI_B->getPositions().second);
	} else {
		positions_A = std::make_pair(ptrCLI_A->getPositions().first, ptrCLI_B->getPositions().second);
		positions_B = std::make_pair(ptrCLI_A->getPositions().second, ptrCLI_B->getPositions().first);
	}
	if(positions_A.first > positions_A.second) std::swap(positions_A.first, positions_A.second);
	if(positions_B.first > positions_B.second) std::swap(positions_B.first, positions_B.second);

	// Define new pair of positions
	newSubSpaceUID_A.reset(new SubSpaceUID(positions_A.first, positions_A.second));
	PairProfileInfo newPPI_A = ptrLik->getCoevAlignManager()->getPairProfileInfo(positions_A.first, positions_A.second);
	newVecProfilesId_A = utilsCRJ.copyProfileIds(newPPI_A);

	newSubSpaceUID_B.reset(new SubSpaceUID(positions_B.first, positions_B.second));
	PairProfileInfo newPPI_B = ptrLik->getCoevAlignManager()->getPairProfileInfo(positions_B.first, positions_B.second);
	newVecProfilesId_B = utilsCRJ.copyProfileIds(newPPI_B);

	// Define the moves probability
	double kPairs = ptrLik->getNActiveCoevLikelihood();

	// Prob of mixing pairs (1/2), prob of changing these two pairs (2/(kPairs)*(kPairs-1))
	fwMoveProb = (1./(2.*kPairs*(kPairs-1)));
	// Prob of reverse move is symmetrical
	bwMoveProb = (1./(2.*kPairs*(kPairs-1)));

	/// [Optional] Draws the parameter values and define the prob
	double newProfileProb_A;
	{ // Draw the new profile for new pair A
		std::pair<MD::idProfile_t, double> randProf = utilsCRJ.drawProfile(newPPI_A);
		newIdProfile_A = randProf.first;
		newProfileProb_A = randProf.second;
		/*PairProfileInfo newPPI_A = ptrLik->getCoevAlignManager()->getPairProfileInfo(positions_A.first, positions_A.second);
		size_t idRand = rng->drawFrom(newPPI_A.profileProb);
		newIdProfile_A = newVecProfilesId_A[idRand];
		newProfileProb_A = newPPI_A.profileProb[idRand];*/
	}

	double newProfileProb_B;
	{ // Draw the new profile for new pair B
		std::pair<MD::idProfile_t, double> randProf = utilsCRJ.drawProfile(newPPI_B);
		newIdProfile_B = randProf.first;
		newProfileProb_B = randProf.second;
		/*PairProfileInfo newPPI_B = ptrLik->getCoevAlignManager()->getPairProfileInfo(positions_B.first, positions_B.second);
		size_t idRand = rng->drawFrom(newPPI_B.profileProb);
		newIdProfile_B = newVecProfilesId_B[idRand];
		newProfileProb_B = newPPI_B.profileProb[idRand];*/
	}

	// Probability of drawing profile are not symmetrical
	// For forward move we draw one over newVecProfilesId_A and newVecProfilesId_B
	//fwRandomProb = 1.0/(newVecProfilesId_A.size()*newVecProfilesId_B.size());
	fwRandomProb = newProfileProb_A*newProfileProb_B;
	// For backward move we draw one over oldVecProfilesId_A and oldVecProfilesId_B
	//bwRandomProb = 1.0/(oldVecProfilesId_A.size()*oldVecProfilesId_B.size());
	bwRandomProb = oldProfileProb_A*oldProfileProb_B;

	// Jacobian is 1. because no change of dimension
	jacobian = 1.;

	canApply = !newVecProfilesId_A.empty() && !newVecProfilesId_B.empty();
	if(!canApply) moveState = IMPOSSIBLE;
}

bool SwapCoevMoveRJ::doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {

	if(canApply) {
		// Disable old pairs
		// A
		disabledSSInfo_A = utilsCRJ.disableSubSpace(oldSubSpaceUID_A, listSSInfo, model, blockDispatcher);
		updatedParameters.insert(updatedParameters.end(), disabledSSInfo_A->getParametersId().begin(), disabledSSInfo_A->getParametersId().end());

		// B
		disabledSSInfo_B = utilsCRJ.disableSubSpace(oldSubSpaceUID_B, listSSInfo, model, blockDispatcher);
		updatedParameters.insert(updatedParameters.end(), disabledSSInfo_B->getParametersId().begin(), disabledSSInfo_B->getParametersId().end());

		// Create two pairs
		// A
		createdSSInfo_A = utilsCRJ.createTempSubSpace(newSubSpaceUID_A, model, blockDispatcher);
		utilsCRJ.setValuesMSS(createdSSInfo_A, newIdProfile_A, oldMSS_A.getDblValues());
		updatedParameters.insert(updatedParameters.end(), createdSSInfo_A->getParametersId().begin(), createdSSInfo_A->getParametersId().end());

		// B
		createdSSInfo_B = utilsCRJ.createTempSubSpace(newSubSpaceUID_B, model, blockDispatcher);
		utilsCRJ.setValuesMSS(createdSSInfo_B, newIdProfile_B, oldMSS_B.getDblValues());
		updatedParameters.insert(updatedParameters.end(), createdSSInfo_B->getParametersId().begin(), createdSSInfo_B->getParametersId().end());
	}
	return false;
}

bool SwapCoevMoveRJ::doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	assert(canApply);

	// Remove temp likelihood and register into permutedLikelihoods
	utilsCRJ.validateTempSubSpace(newSubSpaceUID_A);
	utilsCRJ.validateTempSubSpace(newSubSpaceUID_B);

	// Delete the disabled likelihoods
	utilsCRJ.deleteSubSpaceAfterDisable(oldSubSpaceUID_A);
	utilsCRJ.deleteSubSpaceAfterDisable(oldSubSpaceUID_B);

	// Remove the old ressources
	listSSInfo.remove(disabledSSInfo_A);
	listSSInfo.remove(disabledSSInfo_B);

	// Register the new ones
	listSSInfo.push_back(createdSSInfo_A);
	listSSInfo.push_back(createdSSInfo_B);

	countAccepted++;
	//std::cout << "[SwapCoevMoveRJ] Accepted from : " << oldSubSpaceUID_A->getLabelUID() << " || " << oldSubSpaceUID_B->getLabelUID(); // FIXME DEBUG
	//std::cout << "\t-->\t to : " << newSubSpaceUID_A->getLabelUID() << " || " << newSubSpaceUID_B->getLabelUID() << std::endl; // FIXME DEBUG
	return false;
}

bool SwapCoevMoveRJ::doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	if(canApply) {
		// 1) Delete the two subspaces createds
		ptrSSInfo_t deletedSSInfo_A = utilsCRJ.deleteTempSubSpace(createdSSInfo_A, model, blockDispatcher);
		ptrSSInfo_t deletedSSInfo_B = utilsCRJ.deleteTempSubSpace(createdSSInfo_B, model, blockDispatcher);

		// 2) Enable old ones
		// A
		ptrSSInfo_t reCreatedSSInfo_A = utilsCRJ.createSubSpaceAfterDisable(oldSubSpaceUID_A, disabledSSInfo_A, model, blockDispatcher);
		utilsCRJ.setValuesMSS(reCreatedSSInfo_A, oldMSS_A.getIntValues()[0], oldMSS_A.getDblValues());
		updatedParameters.insert(updatedParameters.end(), reCreatedSSInfo_A->getParametersId().begin(), reCreatedSSInfo_A->getParametersId().end());

		// B
		ptrSSInfo_t reCreatedSSInfo_B = utilsCRJ.createSubSpaceAfterDisable(oldSubSpaceUID_B, disabledSSInfo_B, model, blockDispatcher);
		utilsCRJ.setValuesMSS(reCreatedSSInfo_B, oldMSS_B.getIntValues()[0], oldMSS_B.getDblValues());
		updatedParameters.insert(updatedParameters.end(), reCreatedSSInfo_B->getParametersId().begin(), reCreatedSSInfo_B->getParametersId().end());
		// It has been delete, then re-created
		// The indices of the parameters and block may thus have changed
		// We must signal it to the RJMCMC Handler
		// There we keep  createdSSRessources and deletedSSRessources as such
		listSSInfo.remove(disabledSSInfo_A); 			// Remove the original SSInfo A
		listSSInfo.remove(disabledSSInfo_B); 			// Remove the original SSInfo B
		listSSInfo.push_back(reCreatedSSInfo_A); 		// Add the new one A
		listSSInfo.push_back(reCreatedSSInfo_B); 		// Add the new one B
	}

	countRejected++;
	//std::cout << "[SwapCoevMoveRJ] Rejected from : " << oldSubSpaceUID_A->getLabelUID() << " || " << oldSubSpaceUID_B->getLabelUID(); // FIXME DEBUG
	//std::cout << "\t-->\t to : " << newSubSpaceUID_A->getLabelUID() << " || " << newSubSpaceUID_B->getLabelUID() << std::endl; // FIXME DEBUG
	return false;
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
