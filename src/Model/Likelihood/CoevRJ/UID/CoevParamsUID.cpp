//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevParamsUID.cpp
 *
 * @date Jul 11, 2017
 * @author meyerx
 * @brief
 */
#include "Model/Likelihood/CoevRJ/UID/CoevParamsUID.h"

BOOST_CLASS_EXPORT_GUID(StatisticalModel::Likelihood::CoevRJ::CoevParamsUID, "Model::Likelihood::CoevRJ::CoevParamsUID")

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

CoevParamsUID::CoevParamsUID() : labelUID("PARAMS_COEV") {
}

CoevParamsUID::~CoevParamsUID() {
}

bool CoevParamsUID::operator==(const Sampler::RJMCMC::RJSubSpaceUID &rhs) const {
	const CoevParamsUID *castedRHS = dynamic_cast<const CoevParamsUID*>(&rhs);
	if (castedRHS == NULL) {
		return false;
	}

	return (labelUID == castedRHS->labelUID);
}

std::string CoevParamsUID::getLabelUID() const {
	return labelUID;
}

template<class Archive>
void CoevParamsUID::save(Archive & ar, const unsigned int version) const {
    ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(Sampler::RJMCMC::RJSubSpaceUID);

	ar << BOOST_SERIALIZATION_NVP( labelUID );
}

template<class Archive>
void CoevParamsUID::load(Archive & ar, const unsigned int version) {
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(Sampler::RJMCMC::RJSubSpaceUID);

	ar >> BOOST_SERIALIZATION_NVP( labelUID );
}

template void CoevParamsUID::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void CoevParamsUID::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void CoevParamsUID::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void CoevParamsUID::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
