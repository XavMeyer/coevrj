//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevScalingUID.cpp
 *
 * @date May 25, 2017
 * @author meyerx
 * @brief
 */
#include "CoevScalingUID.h"

#include <sstream>
#include <boost/serialization/export.hpp>
#include <boost/serialization/nvp.hpp>

BOOST_CLASS_EXPORT_GUID(StatisticalModel::Likelihood::CoevRJ::CoevScalingUID, "Model::Likelihood::CoevRJ::CoevScalingUID")

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

CoevScalingUID::CoevScalingUID() : labelUID("SCALING_COEV") {
}

CoevScalingUID::~CoevScalingUID() {
}

bool CoevScalingUID::operator==(const Sampler::RJMCMC::RJSubSpaceUID &rhs) const {
	const CoevScalingUID *castedRHS = dynamic_cast<const CoevScalingUID*>(&rhs);
	if (castedRHS == NULL) {
		return false;
	}

	return (labelUID == castedRHS->labelUID);
}

std::string CoevScalingUID::getLabelUID() const {
	return labelUID;
}

template<class Archive>
void CoevScalingUID::save(Archive & ar, const unsigned int version) const {
    ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(Sampler::RJMCMC::RJSubSpaceUID);

	ar << BOOST_SERIALIZATION_NVP( labelUID );
}

template<class Archive>
void CoevScalingUID::load(Archive & ar, const unsigned int version) {
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(Sampler::RJMCMC::RJSubSpaceUID);

	ar >> BOOST_SERIALIZATION_NVP( labelUID );
}

template void CoevScalingUID::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void CoevScalingUID::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void CoevScalingUID::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void CoevScalingUID::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
