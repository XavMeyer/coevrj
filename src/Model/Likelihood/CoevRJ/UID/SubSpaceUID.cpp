//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SubSpaceUID.cpp
 *
 * @date Apr 18, 2017
 * @author meyerx
 * @brief
 */
#include "SubSpaceUID.h"

#include <sstream>
#include <boost/serialization/export.hpp>
#include <boost/serialization/nvp.hpp>

BOOST_CLASS_EXPORT_GUID(StatisticalModel::Likelihood::CoevRJ::SubSpaceUID, "Model::Likelihood::CoevRJ::SubSpaceUID")

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

SubSpaceUID::SubSpaceUID() :
	pos1(0), pos2(0) {

}

SubSpaceUID::SubSpaceUID(size_t aPos1, size_t aPos2) :
	pos1(aPos1), pos2(aPos2) {
	defineLabelUID();
}

SubSpaceUID::~SubSpaceUID() {
}

bool SubSpaceUID::operator==(const Sampler::RJMCMC::RJSubSpaceUID &rhs) const {
	const SubSpaceUID *castedRHS = dynamic_cast<const SubSpaceUID*>(&rhs);
	if (castedRHS == NULL) {
		return false;
	}

	return (labelUID == castedRHS->labelUID);
}

std::string SubSpaceUID::getLabelUID() const {
	return labelUID;
}

size_t SubSpaceUID::getPos1() const {
	return pos1;
}

size_t SubSpaceUID::getPos2() const {
	return pos2;
}

std::pair<size_t, size_t> SubSpaceUID::getPositions() const {
	return std::make_pair(pos1, pos2);
}

void SubSpaceUID::defineLabelUID() {
	std::stringstream sstr;
	sstr << pos1 << "_" << pos2 << "_" ;
	labelUID = sstr.str();
}


template<class Archive>
void SubSpaceUID::save(Archive & ar, const unsigned int version) const {
    ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(Sampler::RJMCMC::RJSubSpaceUID);

    ar << BOOST_SERIALIZATION_NVP( pos1 );
	ar << BOOST_SERIALIZATION_NVP( pos2 );
	ar << BOOST_SERIALIZATION_NVP( labelUID );
}

template<class Archive>
void SubSpaceUID::load(Archive & ar, const unsigned int version) {
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(Sampler::RJMCMC::RJSubSpaceUID);

    ar >> BOOST_SERIALIZATION_NVP( pos1 );
	ar >> BOOST_SERIALIZATION_NVP( pos2 );
	ar >> BOOST_SERIALIZATION_NVP( labelUID );
}

template void SubSpaceUID::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void SubSpaceUID::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void SubSpaceUID::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void SubSpaceUID::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
