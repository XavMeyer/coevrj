/*
 * LikMappingCoevRJ.cpp
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#include "Model/Likelihood/CoevRJ/LikMappingCoevRJ.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

LikMappingCoevRJ::LikMappingCoevRJ(const size_t aNGamma, const std::string aName) :
		N_GAMMA(aNGamma) {

	name = aName;
	branchLength = 0.;

}

LikMappingCoevRJ::~LikMappingCoevRJ() {

}

void LikMappingCoevRJ::addCoevEdgeNode(EdgeCoevNode *node) {
	treeStructCoev.push_back(node);
}

std::vector<EdgeCoevNode *>& LikMappingCoevRJ::getCoevEdgeNodes() {
	return treeStructCoev;
}

bool LikMappingCoevRJ::removeCoevEdgeNode(EdgeCoevNode *node) {
	size_t pos = treeStructCoev.size();
	for(size_t iN=0; iN<treeStructCoev.size(); ++iN) {
		if(treeStructCoev[iN]->getId() == node->getId()) {
			pos = iN;
			break;
		}
	}

	if(pos == treeStructCoev.size()) {
		return false;
	} else {
		//std::cout << "[TreeNode] id= " << getId() << "\t-> Removing EdgeCoevNode [" << node->getId() << "]" << std::endl; // FIXME
		treeStructCoev.erase(treeStructCoev.begin()+pos);
		return true;
	}
}

bool LikMappingCoevRJ::hasCoevEdgeNode(DAG::BaseNode  *node) {
	size_t pos = treeStructCoev.size();
	for(size_t iN=0; iN<treeStructCoev.size(); ++iN) {
		if(treeStructCoev[iN]->getId() == node->getId()) {
			pos = iN;
			break;
		}
	}
	if(pos == treeStructCoev.size()) {
		return false;
	} else {
		return true;
	}
}

void LikMappingCoevRJ::addCoevBranchMatrixNode(BranchMatrixCoevNode *node) {
	branchMCoev.push_back(node);
}

std::vector<BranchMatrixCoevNode *>& LikMappingCoevRJ::getCoevBranchMatrixNodes() {
	return branchMCoev;
}

bool LikMappingCoevRJ::removeCoevBranchMatrixNode(BranchMatrixCoevNode *node) {
	size_t pos = branchMCoev.size();
	for(size_t iN=0; iN<branchMCoev.size(); ++iN) {
		if(branchMCoev[iN]->getId() == node->getId()) {
			pos = iN;
			break;
		}
	}

	if(pos == branchMCoev.size()) {
		return false;
	} else {
		//std::cout << "[TreeNode] id= " << getId() << "\t-> Removing branchMCoev [" << node->getId() << "]" << std::endl; // FIXME
		branchMCoev.erase(branchMCoev.begin()+pos);
		return true;
	}
}

// Perm coev nodes
// Coev nodes
void LikMappingCoevRJ::addPermCoevEdgeNode(PermEdgeCoevNode *node) {
	treeStructPermCoev.push_back(node);
}

std::vector<PermEdgeCoevNode *>& LikMappingCoevRJ::getPermCoevEdgeNodes() {
	return treeStructPermCoev;
}

void LikMappingCoevRJ::addPermCoevBranchMatrixNode(PermBranchMatrixCoevNode *node) {
	branchMPermCoev.push_back(node);
}

std::vector<PermBranchMatrixCoevNode *>& LikMappingCoevRJ::getPermCoevBranchMatrixNodes() {
	return branchMPermCoev;
}

// GTRG nodes
void LikMappingCoevRJ::addGTRGEdgeNode(EdgeGTRGNode *node) {
	treeStructGTRG.push_back(node);
}

std::vector<EdgeGTRGNode *>& LikMappingCoevRJ::getGTRGEdgeNodes() {
	return treeStructGTRG;
}

void LikMappingCoevRJ::addGTRGBranchMatrixNode(BranchMatrixGTRGNode *node) {
	branchMGTRG.push_back(node);
}

std::vector<BranchMatrixGTRGNode *>& LikMappingCoevRJ::getGTRGBranchMatrixNodes() {
	return branchMGTRG;
}

// Other
void LikMappingCoevRJ::setCoevScaling(const double aCoevScaling, std::set<DAG::BaseNode*> &updatedNodes) {
	for(size_t i=0; i<branchMCoev.size(); ++i){
		bool isUpdated = branchMCoev[i]->setCoevScaling(aCoevScaling);
		if(isUpdated) updatedNodes.insert(branchMCoev[i]);
	}

	for(size_t i=0; i<branchMPermCoev.size(); ++i){
		bool isUpdated = branchMPermCoev[i]->setCoevScaling(aCoevScaling);
		if(isUpdated) updatedNodes.insert(branchMPermCoev[i]);
		//ss << branchMDAG[i]->getId() << ", ";
	}
	//std::cout << "Update coevScaling = " << aCoevScaling << std::endl; // FIXME DEBUG
}

void LikMappingCoevRJ::setBranchLength(const double aBL, std::set<DAG::BaseNode*> &updatedNodes) {
	// FIXME
	//std::stringstream ss;
	//ss << "[" << Parallel::mcmcMgr().getRankProposal() << "] upd : " << branchLength << " -> " << aBL << " (" << branchMDAG.size() << " -> " ;
	branchLength = aBL;

	for(size_t i=0; i<branchMGTRG.size(); ++i){
		bool isUpdated = branchMGTRG[i]->setBranchLength(branchLength);
		if(isUpdated) updatedNodes.insert(branchMGTRG[i]);
		//ss << branchMDAG[i]->getId() << ", ";
	}
	//ss << std::endl;

	for(size_t i=0; i<branchMCoev.size(); ++i){
		bool isUpdated = branchMCoev[i]->setBranchLength(branchLength);
		if(isUpdated) updatedNodes.insert(branchMCoev[i]);
		//ss << branchMDAG[i]->getId() << ", ";
	}

	for(size_t i=0; i<branchMPermCoev.size(); ++i){
		bool isUpdated = branchMPermCoev[i]->setBranchLength(branchLength);
		if(isUpdated) updatedNodes.insert(branchMPermCoev[i]);
		//ss << branchMDAG[i]->getId() << ", ";
	}

	//ss << std::endl;
	//std::cout << ss.str();
}

double LikMappingCoevRJ::getBranchLength() const {
	return branchLength;
}

const std::string& LikMappingCoevRJ::getName() const {
	return name;
}

void LikMappingCoevRJ::doAddChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping) {

	LikMappingCoevRJ* childCoevRJ = dynamic_cast<LikMappingCoevRJ*>(childLikMapping.get());
	assert(childCoevRJ);

	// Add DAG link
	{ // GTR Edge node : Add edge node according to TreeNode
		std::vector<EdgeGTRGNode*>& parentEN = getGTRGEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			std::vector<EdgeGTRGNode*>& childEN = childCoevRJ->getGTRGEdgeNodes();
			parentEN[iE]->addChild(childEN[iE]);
		}
	}

	{ // Coev Edge node : Add edge node according to TreeNode
		std::vector<EdgeCoevNode*>& parentEN = getCoevEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			std::vector<EdgeCoevNode*>& childEN = childCoevRJ->getCoevEdgeNodes();
			parentEN[iE]->addChild(childEN[iE]);
		}
	}

	{ // Coev Edge node : Add edge node according to TreeNode
		std::vector<PermEdgeCoevNode*>& parentEN = getPermCoevEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			std::vector<PermEdgeCoevNode*>& childEN = childCoevRJ->getPermCoevEdgeNodes();
			parentEN[iE]->addChild(childEN[iE]);
		}
	}

}

void LikMappingCoevRJ::doRemoveChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping) {

	LikMappingCoevRJ* childCoevRJ = dynamic_cast<LikMappingCoevRJ*>(childLikMapping.get());

	// Remove DAG link
	{ // Remove GTRG edge node
		std::vector<EdgeGTRGNode*>& parentEN = getGTRGEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			EdgeGTRGNode *pNode = parentEN[iE];
			assert(pNode != NULL);

			std::vector<EdgeGTRGNode*>& childEN = childCoevRJ->getGTRGEdgeNodes();
			EdgeGTRGNode *cNode = childEN[iE];
			assert(cNode != NULL);

			pNode->removeChild(cNode);
		}
	}

	{ // Remove Coev edge node
		std::vector<EdgeCoevNode*>& parentEN = getCoevEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			EdgeCoevNode *pNode = parentEN[iE];
			assert(pNode != NULL);

			std::vector<EdgeCoevNode*>& childEN = childCoevRJ->getCoevEdgeNodes();
			EdgeCoevNode *cNode = childEN[iE];
			assert(cNode != NULL);

			pNode->removeChild(cNode);
		}
	}

	{ // Remove Coev edge node
		std::vector<PermEdgeCoevNode*>& parentEN = getPermCoevEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			PermEdgeCoevNode *pNode = parentEN[iE];
			assert(pNode != NULL);

			std::vector<PermEdgeCoevNode*>& childEN = childCoevRJ->getPermCoevEdgeNodes();
			PermEdgeCoevNode *cNode = childEN[iE];
			assert(cNode != NULL);

			pNode->removeChild(cNode);
		}
	}
}

void LikMappingCoevRJ::doOnApplyChange() {
	{
		std::vector<EdgeGTRGNode*>& edgeNodesGTRG = getGTRGEdgeNodes();
		for(size_t iE=0; iE<edgeNodesGTRG.size(); ++iE) {
			edgeNodesGTRG[iE]->setDone(); // A bit dirty but necessary
		}
	}

	{
		std::vector<EdgeCoevNode*>& edgeNodesCoev = getCoevEdgeNodes();
		for(size_t iE=0; iE<edgeNodesCoev.size(); ++iE) {
			edgeNodesCoev[iE]->setDone(); // A bit dirty but necessary
		}
	}

	{
		std::vector<PermEdgeCoevNode*>& edgeNodesPermCoev = getPermCoevEdgeNodes();
		for(size_t iE=0; iE<edgeNodesPermCoev.size(); ++iE) {
			edgeNodesPermCoev[iE]->setDone(); // A bit dirty but necessary
		}
	}
}

void LikMappingCoevRJ::doOnUpdate(std::set<DAG::BaseNode*> &updatedNodes) {
	{ // GTR
		std::vector<EdgeGTRGNode*>& edgeNodesGTRG = getGTRGEdgeNodes();
		for(size_t iE=0; iE<edgeNodesGTRG.size(); ++iE) {
			ParentGTRGNode *pNode = dynamic_cast<ParentGTRGNode*>(edgeNodesGTRG[iE]);
			if(pNode) {
				pNode->updated();
				updatedNodes.insert(pNode);
			}
		}
	}

	{ // COEV
		std::vector<EdgeCoevNode*>& edgeNodesCoev = getCoevEdgeNodes();
		for(size_t iE=0; iE<edgeNodesCoev.size(); ++iE) {
			ParentCoevNode *pNode = dynamic_cast<ParentCoevNode*>(edgeNodesCoev[iE]);
			if(pNode) {
				pNode->updated();
				updatedNodes.insert(pNode);
			}
		}
	}

	{ // COEV
		std::vector<PermEdgeCoevNode*>& edgeNodesCoev = getPermCoevEdgeNodes();
		for(size_t iE=0; iE<edgeNodesCoev.size(); ++iE) {
			PermParentCoevNode *pNode = dynamic_cast<PermParentCoevNode*>(edgeNodesCoev[iE]);
			if(pNode) {
				pNode->updated();
				updatedNodes.insert(pNode);
			}
		}
	}
}

std::string LikMappingCoevRJ::getNewickSubstring() const {
	std::stringstream ss;
	ss << name;
	ss << ":" ;
	ss << branchLength;
	return ss.str();
}

std::string LikMappingCoevRJ::toString() const {
	stringstream ss;
	ss << name << " - " << branchLength;
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
