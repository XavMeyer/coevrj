//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseSampleWrapper.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "BaseSampleWrapper.h"

#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

const size_t BaseSampleWrapper::N_THETA = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;

BaseSampleWrapper::BaseSampleWrapper(const size_t aNPriorParamValues, const size_t aNBranch,
									 const std::vector<long int> &intVals, const std::vector<double> &dblVals) :
		N_PRIOR_PARAM_VALUES(aNPriorParamValues), N_BL(aNBranch), treeH(intVals.front()) {

	// [Stationary Freq GTR][Nucl. model parameters for GTR (5)] [Alpha parameter for gamma (1)] [branches remainder]
	size_t iS=0;

	for(size_t iT=0; iT<3; iT++) {
		stFreq.push_back(dblVals[iS]);
		++iS;
	}

	// [Nucl. model parameters (1 or 5)]
	for(size_t iT=0; iT<N_THETA; iT++) {
		thetas.push_back(dblVals[iS]);
		++iS;
	}

	alpha = dblVals[iS];
	++iS;

	coevScaling = dblVals[iS];
	++iS;

	lambdaPoisson = dblVals[iS];
	++iS;

	priorParamValues.resize(N_PRIOR_PARAM_VALUES);
	for(size_t iP=0; iP<N_PRIOR_PARAM_VALUES; ++iP) {
		priorParamValues[iP] = dblVals[iS];
		++iS;
	}

	// [branches remainder]
	branchLength.resize(N_BL);
	for(size_t iB=0; iB<N_BL; ++iB) {
		branchLength[iB] = dblVals[iS];
		++iS;
	}
	assert(iS == dblVals.size());
}

BaseSampleWrapper::~BaseSampleWrapper() {
}

std::string BaseSampleWrapper::toString() const {
	std::stringstream ss;
	ss.precision(4);
	ss << std::fixed;

	ss << "StationaryFreq :\t";
	for(size_t iT=0; iT<3; ++iT) {
		ss << stFreq[iT] << "\t";
	}
	ss << std::endl;

	ss << "Thetas :\t";
	for(size_t iT=0; iT<N_THETA; ++iT) {
		ss << thetas[iT] << "\t";
	}
	ss << std::endl;

	ss << "GTR + GAMMA - Alpha = " << alpha << "\t Coev scaling = " << coevScaling << std::endl;

	if(N_PRIOR_PARAM_VALUES > 0) {
		ss << "Prior param values :\t";
		for(size_t iP=0; iP<N_PRIOR_PARAM_VALUES; ++iP) {
			ss << priorParamValues[iP];
		}
	}

	ss << "Branch lengths :\t";
	for(size_t i=0; i<N_BL; ++i) {
		ss << branchLength[i] << "\t";
	}

	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
