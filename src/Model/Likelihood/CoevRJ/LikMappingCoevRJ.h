/*
 * LikMappingCoevRJ.h
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#ifndef MODEL_LIKMAPPINGCOEVRJ_H_
#define MODEL_LIKMAPPINGCOEVRJ_H_

#include <boost/shared_ptr.hpp>

#include "Nodes/IncNodes.h"
#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/LikelihoodMapping.h"


namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

namespace TR = ::MolecularEvolution::TreeReconstruction;

class LikMappingCoevRJ: public TR::LikelihoodMapper::LikelihoodMapping {
public:
	typedef boost::shared_ptr<LikMappingCoevRJ> sharedPtr_t;

public:
	LikMappingCoevRJ(const size_t aNGamma, const std::string aName);
	~LikMappingCoevRJ();

	// Coev nodes
	void addCoevEdgeNode(EdgeCoevNode *node);
	std::vector<EdgeCoevNode *>& getCoevEdgeNodes();
	bool removeCoevEdgeNode(EdgeCoevNode *node);
	bool hasCoevEdgeNode(DAG::BaseNode  *node);

	void addCoevBranchMatrixNode(BranchMatrixCoevNode *node);
	std::vector<BranchMatrixCoevNode *>& getCoevBranchMatrixNodes();
	bool removeCoevBranchMatrixNode(BranchMatrixCoevNode *node);

	// Perm coev nodes
	// Coev nodes
	void addPermCoevEdgeNode(PermEdgeCoevNode *node);
	std::vector<PermEdgeCoevNode *>& getPermCoevEdgeNodes();

	void addPermCoevBranchMatrixNode(PermBranchMatrixCoevNode *node);
	std::vector<PermBranchMatrixCoevNode *>& getPermCoevBranchMatrixNodes();

	// GTRG nodes
	void addGTRGEdgeNode(EdgeGTRGNode *node);
	std::vector<EdgeGTRGNode *>& getGTRGEdgeNodes();

	void addGTRGBranchMatrixNode(BranchMatrixGTRGNode *node);
	std::vector<BranchMatrixGTRGNode *>& getGTRGBranchMatrixNodes();

	// Other
	void setCoevScaling(const double aCoevScaling, std::set<DAG::BaseNode*> &updatedNodes);

	void setBranchLength(const double aBL, std::set<DAG::BaseNode*> &updatedNodes);
	double getBranchLength() const;

	const std::string& getName() const;

	void doAddChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping);
	void doRemoveChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping);

	void doOnApplyChange();
	void doOnUpdate(std::set<DAG::BaseNode*> &updatedNodes);

	std::string getNewickSubstring() const;
	std::string toString() const;

private:

	typedef std::vector<EdgeCoevNode *> treeStructCoev_t;
	typedef std::vector<BranchMatrixCoevNode *> branchMCoev_t;

	typedef std::vector<PermEdgeCoevNode *> treeStructPermCoev_t;
	typedef std::vector<PermBranchMatrixCoevNode *> branchMPermCoev_t;

	typedef std::vector<EdgeGTRGNode *> treeStructGTRG_t;
	typedef std::vector<BranchMatrixGTRGNode *> branchMGTRG_t;

	/* Default data */
	const size_t N_GAMMA;
	std::string name;
	double branchLength;

	/* Linkage with DAG elements */
	treeStructCoev_t treeStructCoev;
	branchMCoev_t branchMCoev;

	treeStructPermCoev_t treeStructPermCoev;
	branchMPermCoev_t branchMPermCoev;

	treeStructGTRG_t treeStructGTRG;
	branchMGTRG_t branchMGTRG;

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MODEL_LIKMAPPINGCOEVRJ_H_ */
