//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelilhoodFactory.cpp
 *
 * @date May 13, 2015
 * @author meyerx
 * @brief
 */
#include "LikelihoodFactory.h"

#include "Model/Likelihood/LikelihoodInterface.h"


namespace MolecularEvolution { namespace DataLoader { class NewickParser; } }

namespace StatisticalModel {
namespace Likelihood {

LikelihoodFactory::LikelihoodFactory() {
}

LikelihoodFactory::~LikelihoodFactory() {
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createCoevRJ(const bool useFixedCluster, bool aUseStationaryFrequencyGTR, bool aCachePairProfileInfo,
																 const size_t aNGamma, const size_t aNThread,
																 MolecularEvolution::DataLoader::NewickParser *aNP,
																 const std::string &aFileAlign,  const std::string &aCoevClusterFile,
																 CoevRJ::Base::treeUpdateStrategy_t aStrat) {
	if(useFixedCluster) {
		return LikelihoodInterface::sharedPtr_t (new CoevRJ::Base(aNGamma, aNThread,
				aUseStationaryFrequencyGTR, aCachePairProfileInfo, aNP, aFileAlign, aCoevClusterFile, aStrat));
	} else {
		return LikelihoodInterface::sharedPtr_t (new CoevRJ::Base(aNGamma, aNThread,
				aUseStationaryFrequencyGTR, aCachePairProfileInfo, aNP, aFileAlign, aStrat));
	}


}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createTIGamma(const size_t aNThread, const size_t aNGamma,
		   	   	   	   	   	   	   	   	   	   	   	   	    const TIGamma::nuclModel_t aNuclModel,
		   	   	   	   	   	   	   	   	   	   	   	   	    MolecularEvolution::DataLoader::NewickParser *aNP, const std::string &aFileAlign,
															TIGamma::Base::treeUpdateStrategy_t aStrat,
															double aEpsilonFreq, double aPowerFreq) {

	return LikelihoodInterface::sharedPtr_t (new TIGamma::Base(aNThread, aNuclModel, aNGamma, aNP, aFileAlign, aStrat, aEpsilonFreq, aPowerFreq));
}


} /* namespace Likelihood */
} /* namespace StatisticalModel */
