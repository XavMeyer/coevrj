//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Model.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <boost/shared_ptr.hpp>
#include <iostream>

#include "Likelihood/LikelihoodInterface.h"
#include "Model/Parameter/Parameters.h"
#include "Sampler/Samples/Sample.h"
#include "Utils/Uncopyable.h"

namespace StatisticalModel {

using Likelihood::LikelihoodInterface;


class Model : private Uncopyable {
public:
	typedef boost::shared_ptr<Model> sharedPtr_t;

public:
	Model(LikelihoodInterface::sharedPtr_t aProc);
	~Model();

	Sampler::Sample getRandomSample() const;

	double processPriorValue(const Sampler::Sample &sample) const;
	double processPosterior(const Sampler::Sample &sample) const;
	void processSample(std::vector<Sampler::Sample> &proposedSamples);
	void processSample(const std::vector<size_t> &pInd, std::vector<Sampler::Sample> &proposedSamples);

	bool isPriorPossible(Sampler::Sample &sample) const;

	Parameters& getParams();
	const Parameters& getParams() const;
	LikelihoodInterface::sharedPtr_t getLikelihood();
	const LikelihoodInterface::sharedPtr_t getLikelihood() const;

private:
	Parameters params;
	LikelihoodInterface::sharedPtr_t proc;

};

} // namespace StatisticalModel


#endif /* MODEL_H_ */
