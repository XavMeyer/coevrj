//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseSampler.h
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#ifndef BASESAMPLER_H_
#define BASESAMPLER_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <sys/types.h>
#include <fstream>
#include <sstream>
#include <vector>

#include "Sampler/Samples/Sample.h"
#include "Sampler/Samples/SampleVector.h"
#include "Sampler/TraceWriter/BaseWriter.h"
#include "Checkpoint/CheckpointManager.h"
#include "Information/IncInformations.h"
#include "Information/InformationDispatcher.h"
#include "Model/Model.h"
#include "ParameterStatsAccessor/AccessorsManager.h"
#include "Proposal/Handler/Manager.h"
#include "Proposal/Proposals.h"
#include "Proposal/Selector/Selector.h"
#include "Sampler/SamplerState.h"
#include "Samples/Samples.h"
#include "TraceWriter/TextWriter.h"
#include "Utils/Uncopyable.h"

namespace Parallel { class MCMCManager; }
namespace StatisticalModel { class Model; }

namespace Sampler {

namespace Proposal { class Proposals; }

using StatisticalModel::Model;
using TraceWriter::BaseWriter;

class BaseSampler : private Uncopyable {
public:
	typedef boost::shared_ptr<BaseSampler> sharedPtr_t;

public:
	//! Constructor
	BaseSampler(Model &aModel, Proposal::Proposals &aProposals,
			BaseWriter::sharedPtr_t aTraceWriter);
	//! Constructor
	BaseSampler(Sample &initSample, Model &aModel,
			Proposal::Proposals &aProposals,
			BaseWriter::sharedPtr_t aTraceWriter);
	//! Destructor
	virtual ~BaseSampler();

	//! Generate samples during N iterations
	void generateNIterations(uint aN);
	//! Return the last sample generated
	Sample getLastSample() const;

	//! Print the times taken for each operations (perfomance report)
	virtual void printTimes();

	std::string getOutputFileBaseName() const;
	std::string getOutputFullFileName() const;
	size_t getVerbose() const;

	Checkpoint::CheckpointManager& getCheckpointManager();
	BaseWriter::sharedPtr_t getTraceWriter();

	std::string getPerformanceReport();
	std::string getBlockStatusReport();

protected:

	static const bool DEFAULT_ISBINARY;
	static const unsigned int DEFAULT_THINNING, STEP_WRITE;

	const Parallel::MCMCManager &mcmcMgr;

	size_t iT;
	//serializedSampleSize;
	double timeTotal, timeSampling, timeWrite;

	Model &model;
	Proposal::Proposals &proposals;
	SampleVector samples;
	Sample curSample;

	Checkpoint::CheckpointManager ckpManager;
	BaseWriter::sharedPtr_t ptrWriter;

	Sampler::State::SamplerState samplerState;
	PStatsAccessor::AccessorsManager accessorsManager;

	Proposal::Selector proposalSelector;
	Proposal::Handler::Manager handlerManager;
	Information::InformationDispatcher infoDispatcher;

	//! Process the next MCMC iteration
	virtual void processNextIteration() = 0;
	//! Process the posterior score given the prior and the likelihood
	double processPosterior(const double aPrior, const double aLikelihood) const;
	//! Check if the prior is possible, if not doesn't process the likelihood
	//double isPriorPossible(Sample &sample) const;
	//! Initialise the first sample
	void initSample(Sample &sample);

	//! Status report
	virtual std::string reportStatus(std::string prefix) = 0;

	//! Checkpoint function
	void saveCheckpoint();
	void loadCheckpoint();

	virtual void doSerializeToFile(const std::string &aFileName) const = 0;
	virtual void doSerializeFromFile(const std::string &aFileName) = 0;

private:
	bool isWriter;

	//! Default init
	void init(Sample &sample);

	//! Write the header
	void writeHeaderToTraceFile();
	//! Write samples
	void writeSamplesToTraceFile(bool isFinalWrite);

	//! Write output information
	void writeOutput();

	//! Handle stop file
	bool catchEndFileSignal();
	void removeEndFileSignal();

	// Boost serialization
	friend class boost::serialization::access;

    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace Sampler */

#endif /* BASESAMPLER_H_ */
