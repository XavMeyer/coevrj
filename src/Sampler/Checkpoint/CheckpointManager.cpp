//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CheckpointManager.cpp
 *
 * @date Jan 28, 2016
 * @author meyerx
 * @brief
 */
#include "CheckpointManager.h"

#include "Parallel/Parallel.h" // IWYU pragma: keep

namespace Sampler {
namespace Checkpoint {

CheckpointManager::CheckpointManager() {
	active = false;

	iCKP = 0;
	iIter = 1;
	iTime = 1;
	iterFrequency = 0;
	timeFrequency = 0.;

	nextIteration = 0.;
	nextTime = 0.;

	timeInterval = 0.;
	nbCkpPerInterval = 5;

	logFileOffset = 0;
	MC3FileOffset = 0;

	nCheckpointsKept = -1;
}

CheckpointManager::~CheckpointManager() {
}

bool CheckpointManager::isActive() const {
	return active;
}

bool CheckpointManager::isCheckpointRequired(size_t iT) const {
	if(!active) return false;

	// Check if there is checkpoint asked (function of # iterations)
	if(iterFrequency > 0) {
		if(iT >= iIter*iterFrequency) {
			iIter = (iT/iterFrequency) + 1; //iIter++;
			return true;
		}
	}

	// Check if there is checkpoint asked (function of time)
	if(timeFrequency > 0.) {
		double timeOffset = 0.15*timeFrequency;
		if(Parallel::mpiMgr().getElapsedTime() >= ((double)iTime*timeFrequency - timeOffset)) {
			iTime++;
			return true;
		}
	}

	// Check if there is checkpoint asked (function of time intervals)
	if(!intervals.empty()) {
		double timeOffset = 0.15*(timeInterval/(double)nbCkpPerInterval);
		if(Parallel::mpiMgr().getElapsedTime() >= (intervals.front()-timeOffset)) {
			intervals.pop_front();
			return true;
		}
	}

	return false;
}

void CheckpointManager::signalDone() {
	iCKP++;
}

void CheckpointManager::setActive() {
	active =  true;
}

void CheckpointManager::setTimeFrequency(double aTimeInSecond) {
	active =  true;
	timeFrequency = aTimeInSecond;
	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[CHECKPOINT] Time frequency (in seconds) for checkpoints set to " << timeFrequency << std::endl;
	}
}

void CheckpointManager::setTimeInterval(double aTimeInSecond, size_t aNumberOfCKP) {
	active =  true;
	intervals.clear();
	timeInterval = aTimeInSecond;
	nbCkpPerInterval = aNumberOfCKP;

	double timeFreq = timeInterval/(double)nbCkpPerInterval;
	for(size_t iT=1; iT<=nbCkpPerInterval; ++iT) {
		intervals.push_back((double)iT*timeFreq);
	}

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[CHECKPOINT] " << nbCkpPerInterval << " checkpoints will be made during the next " << timeInterval << " seconds." << std::endl;
	}

}

void CheckpointManager::setIterationFrequency(size_t aNumberOfIter) {
	active =  true;
	iterFrequency = aNumberOfIter;
	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[CHECKPOINT] Iteration frequency for checkpoints set to " << aNumberOfIter << std::endl;
	}
}

double CheckpointManager::getTimeFrequency() const {
	return timeFrequency;
}

double CheckpointManager::getTimeInterval() const {
	return timeInterval;
}

size_t CheckpointManager::getNbCkpPerInterval() const {
	return nbCkpPerInterval;
}

size_t CheckpointManager::getIterationFrequency() const {
	return iterFrequency;
}


void CheckpointManager::setLogFileOffset(std::streamoff offset) {
	logFileOffset = offset;
}

void CheckpointManager::setCustomLogFileOffsets(std::vector<std::streamoff> &offsets) {
	customLogFileOffsets = offsets;
}

void CheckpointManager::setMC3FileOffset(std::streamoff offset) {
	MC3FileOffset = offset;
}

bool CheckpointManager::areAllCheckpointKept() const {
	return nCheckpointsKept < 0;
}

void CheckpointManager::setNCheckpointKept(int aKeepNCP) {
	nCheckpointsKept = aKeepNCP;
}

int CheckpointManager::getNCheckpointKept() const {
	return nCheckpointsKept;
}

void CheckpointManager::setICKP(size_t aICPK) {
	iCKP = aICPK;
}

size_t CheckpointManager::getICKP() const {
	return iCKP;
}

std::streamoff CheckpointManager::getLogFileOffset() const {
	return logFileOffset;
}

const std::vector<std::streamoff>& CheckpointManager::getCustomLogFileOffsets() const {
	return customLogFileOffsets;
}

std::streamoff CheckpointManager::getMC3FileOffset() const {
	return MC3FileOffset;
}

} /* namespace Checkpoint */
} /* namespace Sampler */
