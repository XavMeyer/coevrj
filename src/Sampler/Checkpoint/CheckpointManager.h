//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CheckpointManager.h
 *
 * @date Jan 28, 2016
 * @author meyerx
 * @brief
 */
#ifndef CHECKPOINTMANAGER_H_
#define CHECKPOINTMANAGER_H_

#include <algorithm>
#include <ios>
#include <list>
#include <vector>

namespace Sampler {
namespace Checkpoint {

class CheckpointManager {
public:
	CheckpointManager();
	~CheckpointManager();

	bool isActive() const;
	bool isCheckpointRequired(size_t iT) const;
	void signalDone();

	void setActive();
	void setTimeFrequency(double aTimeInSecond);
	void setTimeInterval(double aTimeInSecond, size_t aNumberOfCKP=5);
	void setIterationFrequency(size_t aNumberOfIter);

	double getTimeFrequency() const;
	double getTimeInterval() const;
	size_t getNbCkpPerInterval() const;
	size_t getIterationFrequency() const;

	void setICKP(size_t aICPK);
	size_t getICKP() const;

	std::streamoff getLogFileOffset() const;
	const std::vector<std::streamoff>& getCustomLogFileOffsets() const;
	std::streamoff getMC3FileOffset() const;

	void setLogFileOffset(std::streamoff offset);
	void setCustomLogFileOffsets(std::vector<std::streamoff> &offsets);
	void setMC3FileOffset(std::streamoff offset);

	bool areAllCheckpointKept() const;
	void setNCheckpointKept(int aKeepNCP);
	int getNCheckpointKept() const;

private:
	bool active;

	int nCheckpointsKept;
	mutable size_t iCKP, iIter, iTime;
	size_t iterFrequency, nextIteration, nbCkpPerInterval;
	double timeFrequency, nextTime, timeInterval;

	mutable std::list<double> intervals;

	std::streamoff logFileOffset, MC3FileOffset;
	std::vector<std::streamoff> customLogFileOffsets;

};

} /* namespace Checkpoint */
} /* namespace Sampler */

#endif /* CHECKPOINTMANAGER_H_ */
