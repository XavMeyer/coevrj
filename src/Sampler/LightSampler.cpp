//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LightSampler.cpp
 *
 * @date Dec 9, 2016
 * @author meyerx
 * @brief
 */
#include "LightSampler.h"

#include "Sampler/BaseSampler.h"
#include "Parallel/Manager/MpiManager.h"
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/nvp.hpp>
#include "mpi.h"

namespace StatisticalModel { class Model; }

namespace Sampler {

class Sample;
namespace Proposal { class Proposals; }

LightSampler::LightSampler(Model &aModel, Proposal::Proposals &aProposals,
							   BaseWriter::sharedPtr_t aTraceWriter) :
									   BaseSampler(aModel, aProposals, aTraceWriter){
}

LightSampler::LightSampler(Sampler::Sample &initSample, Model &aModel,
		Proposal::Proposals &aProposals, BaseWriter::sharedPtr_t aTraceWriter) :
									   BaseSampler(initSample, aModel, aProposals, aTraceWriter){
}

LightSampler::~LightSampler() {
}


void LightSampler::processNextIteration() {
	BaseSampler::processNextIteration();
}

std::string LightSampler::reportStatus(std::string prefix) {
	return BaseSampler::reportStatus(prefix);
}


void LightSampler::doSerializeToFile(const std::string &aFileName) const {

	double startTime = MPI_Wtime();

	// Write to file
	std::ofstream ofs(aFileName.c_str());
	boost::archive::xml_oarchive oa(ofs);
	oa << boost::serialization::make_nvp( "LightSampler", *this );

	double endTime = MPI_Wtime();

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[ SAVED CHECKPOINT ID=" << ckpManager.getICKP() << " in " << endTime - startTime << " seconds ]" << std::endl;
	}
}

void LightSampler::doSerializeFromFile(const std::string &aFileName) {
	// Read from file
	std::ifstream ifs(aFileName.c_str());
	if(ifs.good()) {
		boost::archive::xml_iarchive ia(ifs);
		ia >> boost::serialization::make_nvp( "LightSampler", *this );
	}
}

template<class Archive>
void LightSampler::save(Archive & ar, const unsigned int version) const {
	ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseSampler);
}

template<class Archive>
void LightSampler::load(Archive & ar, const unsigned int version) {
	ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseSampler);
}

template void LightSampler::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void LightSampler::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void LightSampler::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void LightSampler::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Sampler */
