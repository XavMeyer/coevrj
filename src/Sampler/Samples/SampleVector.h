//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SampleVector.h
 *
 *  Created on: 2 oct. 2013
 *      Author: meyerx
 */

#ifndef SAMPLEVECTOR_H_
#define SAMPLEVECTOR_H_

#include <stddef.h>
#include <iostream>
#include <vector>

#include "Sample.h"

namespace Sampler { class Sample; }

using namespace std;

namespace Sampler {

class SampleVector {
public:

public:
	SampleVector();
	SampleVector(int size, Sample defValue);
	~SampleVector();

	inline Sample& back() { return samples.back();}
	inline const Sample& back() const { return samples.back();}

	inline Sample& previousSample() { return *(samples.rbegin()+1);}
	inline const Sample& previousSample() const { return *(samples.rbegin()+1);}

	inline Sample& front() { return samples.front();}
	inline const Sample& front() const { return samples.front();}

	inline size_t size() const { return samples.size();}

	inline void push_back(Sample& aSample) { return samples.push_back(aSample);}
	inline void push_back(const Sample& aSample) { return samples.push_back(aSample);}

	inline void clear() {samples.clear();}

	inline void pop_front() {samples.erase(samples.begin());}

	SampleUtils::vecPairItSample_t write(size_t &cntAccepted, std::ostream &oFile, const size_t offset=0,
			const char sep='\t', const size_t thinning=1) const;
	SampleUtils::vecPairItSample_t writeAllButFront(size_t &cntAccepted, std::ostream &oFile, const size_t offset=0,
			const char sep='\t', const size_t thinning=1) const;
	SampleUtils::vecPairItSample_t writeBin(size_t &cntAccepted, std::ostream &oFile, const size_t offset=0,
			const char sep='\t', const size_t thinning=1, const bool writeFloat=false) const;
	SampleUtils::vecPairItSample_t writeBinAllButFront(size_t &cntAccepted, std::ostream &oFile, const size_t offset=0,
			const char sep='\t', const size_t thinning=1, const bool writeFloat=false) const;
	string toString() const;

private:

	typedef std::vector<Sample> sampleV_t;
	typedef sampleV_t::iterator itSampleV_t;

	sampleV_t samples;

};

} // namespace Sampler

#endif /* SAMPLEVECTOR_H_ */
