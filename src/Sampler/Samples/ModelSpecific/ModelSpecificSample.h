//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ModelSpecificModelSpecificSample.h
 *
 *  Created on: 30 March 2017
 *      Author: meyerx
 */

#ifndef MODEL_SPECIFIC_SAMPLE_H_
#define MODEL_SPECIFIC_SAMPLE_H_

#include <stddef.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>

#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceUID.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

namespace Sampler {

class ModelSpecificSample {
public:
	ModelSpecificSample();
	ModelSpecificSample(Sampler::RJMCMC::RJSubSpaceUID::sharedPtr_t aUidSubSpace,
			            const size_t nInt, const std::vector<size_t> &pInd);
	ModelSpecificSample(const ModelSpecificSample &toCopy);
	ModelSpecificSample(const Utils::Serialize::buffer_t &aBuffer);
	~ModelSpecificSample();

	ModelSpecificSample& operator=(const ModelSpecificSample& toCopy);

	bool isInteger(int aInd) const;
	bool isDouble(int aInd) const;

	bool hasSameContinuousParameter(const ModelSpecificSample &other) const;

	void incParameter(int aInd, double aIncr);
	void multParameter(int aInd, double aMult);
	double getDoubleParameter(int aInd) const;
	void setDoubleParameter(int aInd, double aVal);
	long int getIntParameter(int aInd) const;
	void setIntParameter(int aInd, long int aVal);

	size_t getNbParameters() const;

	size_t getIdSubSpace() const;
	std::string getLabelSubSpaceUID() const;

	void setMarkers(const std::vector<double> &aMarkers);

	inline const std::vector<long int>& getIntValues() const {return intValues;}
	inline const std::vector<double>& getDblValues() const {return dblValues;}
	inline const std::vector<double>& getMarkers() const {return markers;}

	void write(std::ostream &oFile, char sep='\t') const;
	void writeBin(std::ostream &oFile) const;
	void writeBinFloat(std::ostream &oFile) const;

	std::string toString(char sep='\t') const;

	void load(const Utils::Serialize::buffer_t &buffer);
	Utils::Serialize::buffer_t save();

private:
	std::string labelSubSpaceUID;

	std::vector<size_t> pInd;

	std::vector<long int> intValues;
	std::vector<double> dblValues;
	std::vector<double> markers;


	void copy(const ModelSpecificSample &toCopy);

	// Serialization
	friend class Sample;
	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);
	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} // namespace Sampler

#endif /* MODEL_SPECIFIC_SAMPLE_H_ */
