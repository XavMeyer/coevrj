//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SingleCyclicPS.h
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#ifndef SINGLECYCLICPS_H_
#define SINGLECYCLICPS_H_

#include <stddef.h>

#include "BlockSelector.h"
#include "ParameterBlock/Block.h"

namespace ParameterBlock { class Blocks; }
namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

using ParameterBlock::Block;

class SingleCyclicBS : public BlockSelector {
public:
	SingleCyclicBS(const Parameters &aParams, const Blocks &aBlocks);
	~SingleCyclicBS();

	const Block::sharedPtr_t selectBlock(Sample &sample);
	const std::vector<size_t> selectBlocks(Sample & sample);
	void rewind(const int acceptId);

private:
	size_t lastBlockID;
};

} // namespace Strategies
} // namespace Sampler


#endif /* SINGLECYCLICPS_H_ */
