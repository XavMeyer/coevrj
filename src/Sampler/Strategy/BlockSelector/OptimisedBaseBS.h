//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimisedBaseBS.h
 *
 * @date Apr 12, 2017
 * @author meyerx
 * @brief
 */
#ifndef OPTIMISEDBASEBS_H_
#define OPTIMISEDBASEBS_H_

#include <stddef.h>
#include <list>

#include "BlockSelector.h"

namespace ParameterBlock { class Block; }
namespace ParameterBlock { class Blocks; }
namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

using ParameterBlock::Block;

class OptimisedBaseBS: public BlockSelector {
public:
	OptimisedBaseBS(const Parameters &aParams, const Blocks &aBlocks, const size_t aNTimePerBlock);
	virtual ~OptimisedBaseBS();

protected:

	static const size_t NB_IDBLOCK_THRESHOLD, NB_IDBLOCK_BUFFERED;
	const size_t N_TIME_PER_BLOCK;

	std::list<size_t> iBlock, nSelected;

	void checkPendingBlocks();
	virtual void fillPendingBlocks() = 0;

	std::vector<size_t> selectSomeBlocks();
	void rewindSomeSteps(size_t nKept);

private:
	size_t lastBlocksVersion;

};

} /* namespace Strategies */
} /* namespace Sampler */

#endif /* OPTIMISEDBASEBS_H_ */
