//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RandomTreeInferenceBS.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include "RandomTreeInferenceBS.h"

#include <assert.h>

#include "Sampler/Strategy/BlockSelector/BlockSelector.h"
#include "Parallel/Manager/MCMCManager.h"
#include "Parallel/Manager/MpiManager.h"
#include "Parallel/RNG/RNG.h"
#include "ParameterBlock/Blocks.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

const double RandomTreeInferenceBS::RATE_FREQUENCY = 0.02;

RandomTreeInferenceBS::RandomTreeInferenceBS(const Parameters &aParams, const Blocks &aBlocks,
											 const double aFreqTreeMove, const uint aSeed) :
		BlockSelector(aParams, aBlocks), MOVE_FREQUENCY(aFreqTreeMove),
		BRANCH_FREQUENCY(1-(RATE_FREQUENCY+MOVE_FREQUENCY)),
		OTHER_FREQUENCY(BRANCH_FREQUENCY/2.),
		nBlocks(Parallel::mcmcMgr().getNProposal()), lastBlocksVersion(blocks.getVersion()),
		blockCatHelper(boost::assign::list_of(MOVE_FREQUENCY)(RATE_FREQUENCY)(BRANCH_FREQUENCY)(OTHER_FREQUENCY).convert_to_container<std::vector<double> >()) {
	m_rng = RNG::createSitmo11RNG(aSeed);

	catFB = blockCatHelper.defineCategForTreeInf(params, blocks);

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "**************************************" << std::endl;
		std::cout << "<Block selection frequency> move = " << catFB[0].categoryFreq;
		std::cout << " - rate = " << catFB[1].categoryFreq;
		std::cout << " - branch = " << catFB[2].categoryFreq << std::endl;
	}
}

RandomTreeInferenceBS::~RandomTreeInferenceBS() {
}

const ParameterBlock::Block::sharedPtr_t RandomTreeInferenceBS::selectBlock(Sample &sample) {
	assert(blocks.getNBaseBlocks() >= 1 && "There are no base blocks.");
	if(blocks.getNBaseBlocks() == 1) return blocks.getBlock(0);

	ParameterBlock::Block::sharedPtr_t ptrBlock;
	if(blocks.getVersion() != lastBlocksVersion) {
		catFB = blockCatHelper.defineCategForTreeInf(params, blocks);
		lastBlocksVersion = blocks.getVersion();
	}

	double selType = m_rng->genUniformDbl();
	for(size_t iC=0; iC<catFB.size(); ++iC) {
		if(selType < catFB[iC].categoryFreq) {
			size_t iBlock = m_rng->genUniformInt(0, catFB[iC].blocks.size()-1);
			ptrBlock = catFB[iC].blocks[iBlock];
			break;
		} else {
			selType -= catFB[iC].categoryFreq;
		}
	}

	assert(ptrBlock != NULL);
	return ptrBlock;
}

const std::vector<size_t> RandomTreeInferenceBS::selectBlocks(Sample & sample) {
	std::vector<size_t> blockIDs;
	if(blocks.getNTotalBlocks() == 1) {
		blockIDs.assign(nBlocks, blocks.getFirstBlock()->getId());
	} else {
		if(blocks.getVersion() != lastBlocksVersion) {
			catFB = blockCatHelper.defineCategForTreeInf(params, blocks);
			lastBlocksVersion = blocks.getVersion();
		}
		double selType = m_rng->genUniformDbl();
		for(uint iB=0; iB<nBlocks; ++iB) {

			ParameterBlock::Block::sharedPtr_t ptrBlock;
			for(size_t iC=0; iC<catFB.size(); ++iC) {
				if(selType < catFB[iC].categoryFreq) {
					size_t iBlock = m_rng->genUniformInt(0, catFB[iC].blocks.size()-1);
					ptrBlock = catFB[iC].blocks[iBlock];
					break;
				} else {
					selType -= catFB[iC].categoryFreq;
				}
			}
			blockIDs.push_back(ptrBlock->getId());
		}
	}

	/*if(Parallel::mcmcMgr().checkRoleProposal(Parallel::MCMCManager::Manager)) {
		double maxTime = 0;
		double sumTime = 0;
		std::cout << "Blocks time : ";
		for(size_t iB=0 ; iB<nBlocks; ++iB) {
			std::cout << "[" << blocks.getBlock(blockIDs[iB]).getName() << "]" ;
			std::cout << std::scientific << blocks.getBlock(blockIDs[iB]).getTotalTime() << "\t";
			maxTime = std::max(maxTime, blocks.getBlock(blockIDs[iB]).getTotalTime());
			sumTime += blocks.getBlock(blockIDs[iB]).getTotalTime();
		}
		std::cout << std::endl;
		std::cout << "Max time : " << maxTime << "\t meanTime : " << sumTime/nBlocks << std::endl;
	}*/

	return blockIDs;
}

void RandomTreeInferenceBS::rewind(const int acceptId) {

	const size_t nBlock = Parallel::mcmcMgr().getNProposal();
	size_t nSteps = 0;
	if(acceptId >= 0) {
		nSteps = nBlock-(acceptId+1);

		// 2 draws per selected blocks
		size_t nNewRand = 2*nSteps;

		m_rng->rewind(nNewRand);
	}
}

size_t RandomTreeInferenceBS::getSeed() const {
	return m_rng->getSeed();
}

} /* namespace Strategies */
} /* namespace Sampler */
