//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SplitLoggerSampler.cpp
 *
 * @date Dec 9, 2016
 * @author meyerx
 * @brief
 */
#include "SplitLoggerSampler.h"

#include "Sampler/BaseSampler.h"
#include "Parallel/Manager/MpiManager.h"
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/nvp.hpp>
#include "mpi.h"

#include "Proposal/Outcome/OutcomeMC3.h"
#include "Proposal/Outcome/OutcomePFAMCMC.h"

namespace StatisticalModel { class Model; }

namespace Sampler {

class Sample;
namespace Proposal { class Proposals; }

SplitLoggerSampler::SplitLoggerSampler(Model &aModel, Proposal::Proposals &aProposals,
							   BaseWriter::sharedPtr_t aTraceWriter) :
									   BaseSampler(aModel, aProposals, aTraceWriter) {

	initCustomLogWriter();

}

SplitLoggerSampler::SplitLoggerSampler(Sampler::Sample &initSample, Model &aModel,
		Proposal::Proposals &aProposals, BaseWriter::sharedPtr_t aTraceWriter) :
									   BaseSampler(initSample, aModel, aProposals, aTraceWriter){

	initCustomLogWriter();

}

SplitLoggerSampler::~SplitLoggerSampler() {


	if(Parallel::mpiMgr().isMainProcessor()) {
		// Ugly
		oFileSplit->getOStream() << ptrBM->getSplitsString();

		long int treeHash = curSample.getIntParameter(0);
		oFile->getOStream() << iT << "\t" << treeHash << "\t" << -1 << "\t" << -1 << std::endl;


		treeSplitMap_t::iterator it;
		for(it = treeSplitMap.begin(); it != treeSplitMap.end(); ++it) {
			oFileTreeSplit->getOStream() << it->first << "\t";
			for(size_t iS=0; iS<it->second.size(); ++iS) oFileTreeSplit->getOStream() << it->second[iS] << "\t";
			oFileTreeSplit->getOStream() << std::endl;
		}

		model.getLikelihood()->getBipartitionMonitor()->saveSplitsAndStats(Sampler::Information::outputManager().getBaseFileName());
	}

}

void SplitLoggerSampler::initCustomLogWriter() {

	assert(( model.getLikelihood()->getBipartitionMonitor())  && "SplitLoggerSampler only works with TreeInference models.");

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::stringstream fName, fnameSplit, fnameTreeSplit;
		fName << Sampler::Information::outputManager().getBaseFileName() << ".allTrees";
		oFile.reset(new Utils::Checkpoint::File(fName.str(), ckpManager.getLogFileOffset(), false));

		fnameSplit << Sampler::Information::outputManager().getBaseFileName() << ".allSplits";
		oFileSplit.reset(new Utils::Checkpoint::File(fnameSplit.str(), ckpManager.getLogFileOffset(), false));

		fnameTreeSplit << Sampler::Information::outputManager().getBaseFileName() << ".allTreeSplits";
		oFileTreeSplit.reset(new Utils::Checkpoint::File(fnameTreeSplit.str(), ckpManager.getLogFileOffset(), false));

		ptrBM.reset(new TR::Bipartition::BipartitionMonitor(*(model.getLikelihood()->getBipartitionMonitor().get())));
	}


}

void SplitLoggerSampler::doCustomLogWrite(bool isTreeMove, int blockId, int accepted) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		long int treeHash = curSample.getIntParameter(0);

		// memorize tree hash for iteration at nline
		if(iT == 0) {
			oFile->getOStream() << iT << "\t" << treeHash << "\t" << -1 << "\t" << -1 << std::endl;
		} else if(isTreeMove) {
			oFile->getOStream() << iT << "\t" << treeHash << "\t" << blockId << "\t" << accepted << std::endl;
		}

		// Get the tree
		TR::Tree::sharedPtr_t tree;
		tree = model.getLikelihood()->getTreeManager()->getTree(treeHash);



		// Register the splits and get the split ID;
		bipartitionsId_t splitIds = ptrBM->registerTreeBipartitions(tree, true);

		// Check if the tree is already in the hashmap if not memorize the split
		treeSplitMap_t::iterator it = treeSplitMap.find(treeHash);
		if(it == treeSplitMap.end()) {

			// Insert that into the hasmap
			treeSplitMap.insert(std::pair< hashTree_t, bipartitionsId_t >(treeHash, splitIds) );
		}
	}

}


void SplitLoggerSampler::processNextIteration() {
	using namespace Proposal;
	using namespace Proposal::Handler;
	using namespace Proposal::Outcome;

	// Set the current sample as unchanged (by default)
	curSample.setChanged(false);

	// Select next proposal
	BaseProposal* nextProposal = proposalSelector.selectNext(iT);

	// Get handler
	BaseHandler::sharedPtr_t proposalHandler = handlerManager.getHandler(nextProposal);

	// Pre processing
	proposalHandler->doPreProcessing(curSample);

	// Apply
	proposalHandler->applyProposal(curSample);

	// Post processing
	proposalHandler->doPostProcessing(curSample);

	// Update samples
	proposalHandler->updateSamples(curSample, samples);

	// Enable likelihood to do custom operation during each iteration
	model.getLikelihood()->doCustomOperations(iT, curSample);

	// Process remaining informations
	std::list<Information::BaseInfo::sharedPtr_t>& infos = proposalHandler->getOutcome()->getInformations();
	typedef std::list<Information::BaseInfo::sharedPtr_t>::iterator itList_t;
	for(itList_t it = infos.begin(); it != infos.end(); ++it) {
		(*it)->submitToDispatcher(&infoDispatcher);
	}

	// Checking for block and acceptance
	Outcome::OutcomePFAMCMC* outcome = dynamic_cast<Outcome::OutcomePFAMCMC*>(proposalHandler->getOutcome().get());
	Outcome::OutcomeMC3* outcomeMC3 = dynamic_cast<Outcome::OutcomeMC3*>(proposalHandler->getOutcome().get());

	int blockId = -1;
	int accepted = -1;
	bool isTreeMove = false;
	if(outcome) {
		blockId = outcome->getBlockIds().front();
		accepted = outcome->getAcceptedSamples().size();
		isTreeMove = static_cast<ProposalPFAMCMC*>(nextProposal)->getBlocks().getBlock(blockId)->getPIndices().front() == 0;
	}
	if(outcomeMC3) {
		blockId = -5;
		accepted = outcomeMC3->getAcceptedSamples().size();
		isTreeMove = true;
	}

	doCustomLogWrite(isTreeMove, blockId, accepted);
}

std::string SplitLoggerSampler::reportStatus(std::string prefix) {
	return BaseSampler::reportStatus(prefix);
}


void SplitLoggerSampler::doSerializeToFile(const std::string &aFileName) const {

	double startTime = MPI_Wtime();

	// Write to file
	std::ofstream ofs(aFileName.c_str());
	boost::archive::xml_oarchive oa(ofs);
	oa << boost::serialization::make_nvp( "SplitLoggerSampler", *this );

	double endTime = MPI_Wtime();

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[ SAVED CHECKPOINT ID=" << ckpManager.getICKP() << " in " << endTime - startTime << " seconds ]" << std::endl;
	}
}

void SplitLoggerSampler::doSerializeFromFile(const std::string &aFileName) {
	// Read from file
	std::ifstream ifs(aFileName.c_str());
	if(ifs.good()) {
		boost::archive::xml_iarchive ia(ifs);
		ia >> boost::serialization::make_nvp( "SplitLoggerSampler", *this );
	}
}

template<class Archive>
void SplitLoggerSampler::save(Archive & ar, const unsigned int version) const {
	ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseSampler);
}

template<class Archive>
void SplitLoggerSampler::load(Archive & ar, const unsigned int version) {
	ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseSampler);
}

template void SplitLoggerSampler::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void SplitLoggerSampler::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void SplitLoggerSampler::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void SplitLoggerSampler::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Sampler */
