//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SplitLoggerRJSampler.h
 *
 * @date Oct 5, 2017
 * @author meyerx
 * @brief
 */
#ifndef SPLITLOGGERRJSAMPLER_H_
#define SPLITLOGGERRJSAMPLER_H_

#include "BaseSampler.h"
#include "Sampler/TraceWriter/BaseWriter.h"
#include "Sampler/Proposal/Handler/HandlerRJMCMC.h"

#include "Model/Likelihood/Helper/HelperInterface.h"

namespace StatisticalModel { class Model; }

namespace Sampler {

class Sample;
namespace TR = ::MolecularEvolution::TreeReconstruction;
namespace Proposal { class Proposals; }

class SplitLoggerRJSampler: public BaseSampler {
public:
	SplitLoggerRJSampler(Model &aModel, Proposal::Proposals &aProposals,
			  BaseWriter::sharedPtr_t aTraceWriter);
	SplitLoggerRJSampler(Sampler::Sample &initSample, Model &aModel,
			  Proposal::Proposals &aProposals, BaseWriter::sharedPtr_t aTraceWriter);
	~SplitLoggerRJSampler();

protected:

	//! Process the next MCMC iteration
	void processNextIteration();

	//! Status report
	std::string reportStatus(std::string prefix);

	void doSerializeToFile(const std::string &aFileName) const;
	void doSerializeFromFile(const std::string &aFileName);

	// Boost serialization
    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);
	BOOST_SERIALIZATION_SPLIT_MEMBER()

	friend class boost::serialization::access;

private:

	Proposal::Handler::HandlerRJMCMC* ptrHandlerRJMCMC;
	StatisticalModel::Likelihood::Helper::HelperInterface::sharedPtr_t ptrLikHelper;

	typedef long int hashTree_t;
	typedef std::vector<size_t> bipartitionsId_t;
	typedef std::map< hashTree_t, bipartitionsId_t > treeSplitMap_t;

	treeSplitMap_t treeSplitMap;
	Utils::Checkpoint::File::sharedPtr_t oFile, oFileSplit, oFileTreeSplit;
	TR::Bipartition::BipartitionMonitor::sharedPtr_t ptrBM;

	void init();

	void initCustomLogWriter();
	void doCustomLogWrite(bool isTreeMove, int blockId, int accepted);

};

} /* namespace Sampler */

#endif /* SPLITLOGGERSplitLoggerSplitLoggerRJSampler_H_ */
