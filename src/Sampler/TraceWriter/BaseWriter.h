//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseWriter.h
 *
 * @date Dec 6, 2016
 * @author meyerx
 * @brief
 */
#ifndef BASEWRITER_H_
#define BASEWRITER_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <sstream>
#include <string>

#include "Model/Model.h"
#include "Sampler/Checkpoint/CheckpointManager.h"
#include "Sampler/Samples/SampleVector.h"
#include "Utils/Code/CheckpointFile.h"

namespace Sampler { class SampleVector; }
namespace Sampler { namespace Checkpoint { class CheckpointManager; } }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace TraceWriter {

class BaseWriter {
public:
	typedef boost::shared_ptr< BaseWriter > sharedPtr_t;

public:
	BaseWriter(const std::string &aPrefix, const std::string &aSuffix,
			size_t aThinning);
	virtual ~BaseWriter();

	virtual void writeHeader(SampleVector &samples, Checkpoint::CheckpointManager &ckpManager, StatisticalModel::Model &model) = 0;
	virtual void writeSamples(const size_t offset, SampleVector &samples, StatisticalModel::Model &model) = 0;

	void setCntAccepted(size_t aCntAccepeted);
	void setNWSamples(size_t aNWSamples);

	size_t getCntAccepted() const;
	size_t getNWSamples() const;
	size_t getThinning() const;
	size_t getFileOffset() const;

	std::string getFNPrefix() const;
	std::string getFNSuffix() const;
	std::string getFileBaseName() const;
	std::string getFileName() const;

protected:

	size_t cntAccepted, nWSamples, thinning;
	std::string fnPrefix, fnSuffix, fileName, baseFName;
	Utils::Checkpoint::File::sharedPtr_t oFile;
};

} /* namespace TraceWriter */
} /* namespace Sampler */

#endif /* BASEWRITER_H_ */
