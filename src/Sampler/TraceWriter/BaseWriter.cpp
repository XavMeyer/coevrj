//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseWriter.cpp
 *
 * @date Dec 6, 2016
 * @author meyerx
 * @brief
 */
#include "BaseWriter.h"

#include <assert.h>

#include "Parallel/Parallel.h" // IWYU pragma: keep
#include "Sampler/Information/IncInformations.h"

namespace Sampler {
namespace TraceWriter {

BaseWriter::BaseWriter(const std::string &aPrefix, const std::string &aSuffix,
		size_t aThinning) :
		thinning(aThinning), fnPrefix(aPrefix), fnSuffix(aSuffix){
	cntAccepted = 0;
	nWSamples = 0;

	std::stringstream ssBaseName;
	ssBaseName << fnPrefix << "_" << Parallel::mpiMgr().getRank() << fnSuffix;
	baseFName = ssBaseName.str();

	Sampler::Information::outputManager().setPrefixFileName(fnPrefix);
	Sampler::Information::outputManager().setBaseFileName(baseFName);

	assert(thinning != 0);
}

BaseWriter::~BaseWriter() {
}

void BaseWriter::setCntAccepted(size_t aCntAccepeted) {
	cntAccepted = aCntAccepeted;
}

void BaseWriter::setNWSamples(size_t aNWSamples){
	nWSamples = aNWSamples;
}

size_t BaseWriter::getCntAccepted() const {
	return cntAccepted;
}

size_t BaseWriter::getNWSamples() const {
	return nWSamples;
}

size_t BaseWriter::getThinning() const {
	return thinning;
}

size_t BaseWriter::getFileOffset() const {
	return oFile->getSize();
}

std::string BaseWriter::getFNPrefix() const {
	return fnPrefix;
}

std::string BaseWriter::getFNSuffix() const {
	return fnSuffix;
}

std::string BaseWriter::getFileName() const {
	return fileName;
}

std::string BaseWriter::getFileBaseName() const {
	return baseFName;
}

} /* namespace TraceWriter */
} /* namespace Sampler */
