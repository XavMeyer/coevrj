//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TextWriter.cpp
 *
 * @date Dec 6, 2016
 * @author meyerx
 * @brief
 */
#include "TextWriter.h"

#include <assert.h>

#include "Sampler/Checkpoint/CheckpointManager.h"
#include "Sampler/Samples/SampleVector.h"

namespace StatisticalModel { class Model; }

namespace Sampler {
namespace TraceWriter {

TextWriter::TextWriter(const std::string &aPrefix, const std::string &aSuffix,
		size_t aThinning) :
			BaseWriter(aPrefix, aSuffix, aThinning) {

	std::stringstream ssFName;
	ssFName << baseFName << ".log";
	fileName = ssFName.str();
}

TextWriter::~TextWriter() {
}

void TextWriter::writeHeader(SampleVector &samples, Checkpoint::CheckpointManager &ckpManager, StatisticalModel::Model &model) {
	const char sep = '\t';

	// Create / recover output file
	if(oFile == NULL){
		oFile.reset(new Utils::Checkpoint::File(fileName, ckpManager.getLogFileOffset(), false));
		oFile->getOStream().precision(8);

		if(!oFile->getOStream().is_open()) {
			std::cerr << "[ERROR] void MCMC::writeHeader();" << std::endl;
			std::cerr << " Cannot open file : " << fileName << std::endl;
		}
	}

	// If there is no ckp or if we have ckp but its the first time : we write the header
	if(!ckpManager.isActive() || (ckpManager.isActive() && ckpManager.getICKP() == 0)) {
		oFile->getOStream() << "Iteration" << sep << "Prior"<< sep << "Likelihood" << sep << "Posterior" << sep;
		const vector<string> &markerNames = model.getLikelihood()->getMarkerNames();
		for(size_t iN=0; iN<markerNames.size(); ++iN){
			oFile->getOStream() << markerNames[iN] << sep;
		}
		vector<string> names(model.getParams().getNames());
		for(size_t iN=0; iN<names.size(); ++iN){
			oFile->getOStream() << names[iN] << sep;
		}
		oFile->getOStream() << std::endl;
	}
}

void TextWriter::writeSamples(const size_t offset, SampleVector &samples, StatisticalModel::Model &model) {
	const char sep = '\t';

	assert(oFile->getOStream().is_open());

	SampleUtils::vecPairItSample_t wrtSamples; // Keep track of written samples

	oFile->getOStream().precision(8);
	oFile->getOStream() << scientific;
	if(offset != 0) {
		wrtSamples = samples.writeAllButFront(cntAccepted, oFile->getOStream(), nWSamples, sep, thinning);
		nWSamples += samples.size()-1;
	} else {
		wrtSamples = samples.write(cntAccepted, oFile->getOStream(), nWSamples, sep, thinning);
		nWSamples += samples.size();
	}

	model.getLikelihood()->writeCustomizedLog(wrtSamples);

}

} /* namespace TraceWriter */
} /* namespace Sampler */
