//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SplitLoggerSampler.h
 *
 * @date Dec 9, 2016
 * @author meyerx
 * @brief
 */
#ifndef SPLITLOGGERSAMPLER_H_
#define SPLITLOGGERSAMPLER_H_

#include "BaseSampler.h"
#include "Sampler/TraceWriter/BaseWriter.h"
#include "Model/Likelihood/TIGamma/Base.h"

namespace StatisticalModel { class Model; }

namespace Sampler {

namespace TR = ::MolecularEvolution::TreeReconstruction;

class Sample;
namespace Proposal { class Proposals; }

class SplitLoggerSampler: public BaseSampler {
public:
	SplitLoggerSampler(Model &aModel, Proposal::Proposals &aProposals,
				   BaseWriter::sharedPtr_t aTraceWriter);
	SplitLoggerSampler(Sampler::Sample &initSample, Model &aModel,
			Proposal::Proposals &aProposals, BaseWriter::sharedPtr_t aTraceWriter);
	~SplitLoggerSampler();

protected:

	typedef long int hashTree_t;
	typedef std::vector<size_t> bipartitionsId_t;
	typedef std::map< hashTree_t, bipartitionsId_t > treeSplitMap_t;

	treeSplitMap_t treeSplitMap;
	Utils::Checkpoint::File::sharedPtr_t oFile, oFileSplit, oFileTreeSplit;
	TR::Bipartition::BipartitionMonitor::sharedPtr_t ptrBM;

	//! Process the next MCMC iteration
	void processNextIteration();

	//! Status report
	std::string reportStatus(std::string prefix);

	void doSerializeToFile(const std::string &aFileName) const;
	void doSerializeFromFile(const std::string &aFileName);

	// Boost serialization
    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);
	BOOST_SERIALIZATION_SPLIT_MEMBER()

	friend class boost::serialization::access;

	void initCustomLogWriter();
	void doCustomLogWrite(bool isTreeMove, int blockId, int accepted);

};

} /* namespace Sampler */

#endif /* SPLITLOGGERSAMPLER_H_ */
