//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RJSubSpaceUID.h
 *
 * @date Apr 18, 2017
 * @author meyerx
 * @brief
 */
#ifndef RJSUBSPACEUID_H_
#define RJSUBSPACEUID_H_

#include "Utils/Code/IDManagement/IDManager.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

#include <string>
#include <stddef.h>
#include <boost/shared_ptr.hpp>

namespace Sampler {
namespace RJMCMC {

class RJSubSpaceUID {
public:
	typedef boost::shared_ptr<RJSubSpaceUID> sharedPtr_t;

public:
	RJSubSpaceUID();
	virtual ~RJSubSpaceUID();

	virtual bool operator==(const RJSubSpaceUID &rhs) const = 0;

	virtual std::string getLabelUID() const = 0;

protected:

	static Utils::IDManagement::IDManager idManager;

	// Boost serialization
	friend class boost::serialization::access;

    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()

};

struct AreEqualSSUID {
	bool operator()(const RJSubSpaceUID::sharedPtr_t& left,
				    const RJSubSpaceUID::sharedPtr_t& right) const {
		return operator()( *left, *right );
	}

	bool operator()(const RJSubSpaceUID& left, const RJSubSpaceUID& right) const {
		return left == right;
	}
};

} /* namespace RJMCMC */
} /* namespace Sampler */

#endif /* RJSUBSPACEUID_H_ */
