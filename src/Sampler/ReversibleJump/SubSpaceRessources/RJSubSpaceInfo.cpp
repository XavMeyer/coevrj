//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RJSubSpaceInfo.h
 *
 * @date Apr 7, 2017
 * @author meyerx
 * @brief
 */
#include "RJSubSpaceInfo.h"

#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceUID.h"

//BOOST_CLASS_EXPORT_GUID(Sampler::RJMCMC::RJSubSpaceInfo, "Sampler_RJMCMC_RJSubSpaceInfo")

namespace Sampler {
namespace RJMCMC {

RJSubSpaceInfo::RJSubSpaceInfo(RJSubSpaceUID::sharedPtr_t aUIDSubSpace) :
		uidSubSpace(aUIDSubSpace) {
}

RJSubSpaceInfo::~RJSubSpaceInfo() {
}

void RJSubSpaceInfo::addParameterId(size_t aId) {
	paramsId.push_back(aId);
}

void RJSubSpaceInfo::setParametersId(const std::vector<size_t>& aParamsId) {
	paramsId = aParamsId;
}
const std::vector<size_t>& RJSubSpaceInfo::getParametersId() const {
	return paramsId;
}

void RJSubSpaceInfo::addBlockId(size_t aId) {
	blocksId.push_back(aId);
}

void RJSubSpaceInfo::setBlocksId(std::vector<size_t>& aBlocksId) {
	blocksId = aBlocksId;
}

const std::vector<size_t>& RJSubSpaceInfo::getBlocksId() const {
	return blocksId;
}

const RJSubSpaceUID::sharedPtr_t& RJSubSpaceInfo::getUIDSubSpace() const {
	return uidSubSpace;
}

void RJSubSpaceInfo::setUIDSubSpace(RJSubSpaceUID::sharedPtr_t aPtrUID) {
	uidSubSpace = aPtrUID;
}

bool RJSubSpaceInfo::operator==(const RJSubSpaceInfo &rhs) {
	return uidSubSpace->getLabelUID() == rhs.uidSubSpace->getLabelUID();
}

template<class Archive>
void RJSubSpaceInfo::save(Archive & ar, const unsigned int version) const {

	ar << uidSubSpace;

	ar << paramsId;
	ar << blocksId;
}

template<class Archive>
void RJSubSpaceInfo::load(Archive & ar, const unsigned int version) {

	ar >> uidSubSpace;

	ar >> paramsId;
	ar >> blocksId;

}

} /* namespace RJMCMC */
} /* namespace Sampler */
