//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RJSubSpaceInfo.h
 *
 * @date Apr 7, 2017
 * @author meyerx
 * @brief
 */
#ifndef RJSUBSPACEINFO_H_
#define RJSUBSPACEINFO_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <list>
#include <vector>

#include "RJSubSpaceUID.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep
#include "Utils/Uncopyable.h"

namespace Sampler {
namespace RJMCMC {


class RJSubSpaceInfo : public Uncopyable { // Unique ID thus, must be uncopyable
public:
	typedef boost::shared_ptr<RJSubSpaceInfo> sharedPtr_t;
	typedef std::list<RJSubSpaceInfo::sharedPtr_t> listSSInfo_t;

public:
	RJSubSpaceInfo(RJSubSpaceUID::sharedPtr_t aUIDSubSpace);
	~RJSubSpaceInfo();

	void addParameterId(size_t aId);
	void setParametersId(const std::vector<size_t>& aParamsId);
	const std::vector<size_t>& getParametersId() const;

	void addBlockId(size_t aId);
	void setBlocksId(std::vector<size_t>& aBlocksId);
	const std::vector<size_t>& getBlocksId() const;

	const RJSubSpaceUID::sharedPtr_t& getUIDSubSpace() const;
	void setUIDSubSpace(RJSubSpaceUID::sharedPtr_t aPtrUID);

	bool operator==(const RJSubSpaceInfo &rhs);

public:

	RJSubSpaceUID::sharedPtr_t uidSubSpace;
	std::vector<size_t> paramsId;
	std::vector<size_t> blocksId;

	// Serialization
	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);
	BOOST_SERIALIZATION_SPLIT_MEMBER()

};

struct HasSSInfoSpecificUID {
	RJSubSpaceUID::sharedPtr_t uidSubSpace;
	HasSSInfoSpecificUID(RJSubSpaceUID::sharedPtr_t aUidSubSpace) :
		uidSubSpace(aUidSubSpace){}
    bool operator() (const RJSubSpaceInfo::sharedPtr_t& ptrSSInfo) {
    	return ptrSSInfo->getUIDSubSpace()->getLabelUID() == uidSubSpace->getLabelUID();
    }
};

} /* namespace RJMCMC */
} /* namespace Sampler */

#endif /* RJSUBSPACEINFO_H_ */
