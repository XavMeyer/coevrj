//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RJMoveProposal.h
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#ifndef RJMOVEPROPOSAL_H_
#define RJMOVEPROPOSAL_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>

#include "Model/Model.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"
#include "Sampler/Samples/Sample.h"

class RNG;
namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace RJMCMC {

class RJMoveProposal {
public:
	typedef boost::shared_ptr<RJMoveProposal> sharedPtr_t;

public:
	RJMoveProposal(StatisticalModel::Model &aModel);
	RJMoveProposal(double aFreq, StatisticalModel::Model &aModel);
	virtual ~RJMoveProposal();

	RJMove::sharedPtr_t propose(Sampler::Sample &aSample);

	double getFrequency() const;

	void setRNG(RNG *aRng);

	virtual void signalMoveAccepted();
	virtual void signalMoveRejected();

	double getAcceptanceRatio() const;

	virtual std::string getStringStats(std::string &prefix) const;

protected:

	double freq;
	RNG* rng;
	size_t cntAccepted, cntRejected, cntProposed;
	StatisticalModel::Model &model;

	virtual RJMove::sharedPtr_t doPropose(Sampler::Sample &aSample) = 0;

};

} /* namespace RJMCMC */
} /* namespace Sampler */

#endif /* RJMOVEPROPOSAL_H_ */

