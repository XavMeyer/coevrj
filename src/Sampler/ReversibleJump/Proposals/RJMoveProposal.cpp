//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RJMoveProposal.cpp
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#include "RJMoveProposal.h"

#include <assert.h>

class RNG;
namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace RJMCMC {

RJMoveProposal::RJMoveProposal(StatisticalModel::Model &aModel) :
									   freq(1.0), model(aModel) {
	rng = NULL;
	cntAccepted = cntRejected = cntProposed = 0;
}

RJMoveProposal::RJMoveProposal(double aFreq, StatisticalModel::Model &aModel) :
									   freq(aFreq), model(aModel) {
	rng = NULL;
	cntAccepted = cntRejected = cntProposed = 0;
}

RJMoveProposal::~RJMoveProposal() {
}

RJMove::sharedPtr_t RJMoveProposal::propose(Sampler::Sample &aSample) {
	assert(rng != NULL);
	cntProposed++;
	return doPropose(aSample);
}

double RJMoveProposal::getFrequency() const {
	return freq;
}

void RJMoveProposal::setRNG(RNG *aRng) {
	rng = aRng;
}

void RJMoveProposal::signalMoveAccepted() {
	cntAccepted++;
}

void RJMoveProposal::signalMoveRejected() {
	cntRejected++;
}

double RJMoveProposal::getAcceptanceRatio() const {
	return (double)cntAccepted/(double)cntProposed;
}

std::string RJMoveProposal::getStringStats(std::string &prefix) const {
	std::stringstream ss;
	double ratio = cntProposed == 0 ? 0. : (double)cntAccepted/(double)cntProposed;
	ss << "[RJMoveProposal] Proposal acceptance ratio = " << ratio;
	ss << " ( " << cntAccepted << " / " << cntProposed << " " << std::endl;
	return ss.str();
}

} /* namespace RJMCMC */
} /* namespace Sampler */
