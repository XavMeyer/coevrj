//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveBlocks.cpp
 *
 * @date Feb 2, 2017
 * @author meyerx
 * @brief
 */
#include "AdaptiveBlocks.h"

#include "Sampler/Adaptive/AdaptiveBlock.h"
#include "Sampler/Adaptive/DefaultAdaptiveBlock.h"
#include "Sampler/Adaptive/FixedProposalBlock.h"
#include "Sampler/Adaptive/MixedAdaptiveBlock.h"
#include "Sampler/Adaptive/PCAAdaptiveBlock.h"

namespace Sampler { class Sample; }
namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }
namespace StatisticalModel { class Model; }


namespace Sampler {
namespace Adaptive {


AdaptiveBlock::sharedPtr_t createFixedProposal(const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel) {
	return AdaptiveBlock::sharedPtr_t(new FixedProposalBlock(aBlock, aModel));
}

AdaptiveBlock::sharedPtr_t createDefault(const Sample &initSample,
		const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		CF::ConfigFactory::sharedPtr_t aCfgFac) {
	return AdaptiveBlock::sharedPtr_t(new DefaultAdaptiveBlock(initSample, aBlock, aModel, aCfgFac));
}
AdaptiveBlock::sharedPtr_t createMixed(const Sample &initSample,
		const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		CF::ConfigFactory::sharedPtr_t aCfgFac) {
	return AdaptiveBlock::sharedPtr_t(new MixedAdaptiveBlock(initSample, aBlock, aModel, aCfgFac));
}

AdaptiveBlock::sharedPtr_t createPCA(const Sample &initSample,
		const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		CF::ConfigFactory::sharedPtr_t aCfgFac) {
	return AdaptiveBlock::sharedPtr_t(new PCA_AdaptiveBlock(initSample, aBlock, aModel, aCfgFac));
}

AdaptiveBlock::sharedPtr_t createDefault(
		const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		CF::ConfigFactory::sharedPtr_t aCfgFac) {
	return AdaptiveBlock::sharedPtr_t(new DefaultAdaptiveBlock(aBlock, aModel, aCfgFac));
}
AdaptiveBlock::sharedPtr_t createMixed(
		const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		CF::ConfigFactory::sharedPtr_t aCfgFac) {
	return AdaptiveBlock::sharedPtr_t(new MixedAdaptiveBlock(aBlock, aModel, aCfgFac));
}

AdaptiveBlock::sharedPtr_t createPCA(
		const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		CF::ConfigFactory::sharedPtr_t aCfgFac) {
	return AdaptiveBlock::sharedPtr_t(new PCA_AdaptiveBlock(aBlock, aModel, aCfgFac));
}

} /* namespace Adaptive */
} /* namespace Sampler */
