//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveProcess.cpp
 *
 * @date Dec 13, 2016
 * @author meyerx
 * @brief
 */
#include "AdaptiveProcess.h"

#include <assert.h>
#include <sys/types.h>

#include "Sampler/Adaptive/AdaptiveProcessState.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Model.h"
#include "Parallel/Manager/MCMCManager.h"
#include "ParameterBlock/Block.h"
#include "ParameterBlock/Blocks.h"
#include "Sampler/Information/OutputManager.h"
#include "Sampler/Samples/Sample.h"
#include "Sampler/Adaptive/AdaptiveBlocks.h"

namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }

namespace Sampler {
namespace Adaptive {

const size_t AdaptiveProcess::NB_BLOCK_RELAXATION_TH = 4;
const size_t AdaptiveProcess::MINIMAL_ADAPTIVE_PHASE = 2000;
const size_t AdaptiveProcess::CONVERGENCE_REPORTING_FREQUENCY = 10000;

AdaptiveProcess::AdaptiveProcess(Sampler::Sample initSample,
								 Config::ConfigFactory::sharedPtr_t aCfgFactory,
								 ParameterBlock::Blocks &aBlocks, StatisticalModel::Model &aModel,
								 Strategies::AcceptanceStrategy::sharedPtr_t aPtrAS,
								 Strategies::ModifierStrategy::sharedPtr_t aPtrMS,
								 Sampler::PStatsAccessor::AccessorsManager &aAccessorManager) :
		mcmcMgr(Parallel::mcmcMgr()), cfgFactory(aCfgFactory), blocks(aBlocks), model(aModel),
		ptrAS(aPtrAS), ptrMS(aPtrMS), pStatsAccessor(model.getParams()) {

	counter = 0;
	nAdaptiveBlocks = 0;
	convCnt = 0;
	fullConvergence = false;
	init(initSample, aAccessorManager);
}

AdaptiveProcess::~AdaptiveProcess() {
}

void AdaptiveProcess::init(Sample &initSample, Sampler::PStatsAccessor::AccessorsManager &aAccessorManager) {

	for(uint iB=0; iB<blocks.getNBaseBlocks(); ++iB){
		const ParameterBlock::Block::sharedPtr_t block = blocks.getBlock(iB);

		if(block->isNotAdaptive()) {
			adaptiveBlocks.push_back(Adaptive::createFixedProposal(block, model));
		} else if(block->isSingleDouble()) {
			adaptiveBlocks.push_back(Adaptive::createDefault(initSample, block, model, cfgFactory));
			nAdaptiveBlocks++;
		} else if(block->isMixedDouble()) {
			adaptiveBlocks.push_back(Adaptive::createMixed(initSample, block, model, cfgFactory));
			nAdaptiveBlocks++;
		} else if(block->isPCADouble()) {
			adaptiveBlocks.push_back(Adaptive::createPCA(initSample, block, model, cfgFactory));
			nAdaptiveBlocks++;
		} else {
			std::cerr << "[Error] in BlockUpdater::createNewAdaptiveBlock(...);" << std::endl;
			std::cerr << "Block type : " << static_cast<size_t>(block->getAdaptiveType()) << " is not supported." << std::endl;
			assert(false);
		}
	}

	pStatsAccessor.map(adaptiveBlocks);
	aAccessorManager.registerProposalAccessor(&pStatsAccessor);
}

void AdaptiveProcess::adaptBlock(bool accepted, Sample &oldSample, Sample &curSample,
							 	 size_t iBlock, double alpha) {

	// Adaptive blocks do not apply to dynamic blocks
	if(iBlock >= adaptiveBlocks.size()) {
		if(accepted) blocks.getBlock(iBlock)->accepted(true);
		else blocks.getBlock(iBlock)->rejected(true);
		return;
	}

	if(accepted) {
		blocks.getBlock(iBlock)->accepted(true);
		if(!hasConverged()) {
			adaptiveBlocks[iBlock]->updateSigma(accepted, oldSample, curSample);
			//std::cout << counter << "\t" << (accepted ? "true\t" : "false\t") << std::endl; // FIXME DELETE
			//std::cout << oldSample.toString() << std::endl; // FIXME DELETE
			//std::cout << curSample.toString() << std::endl; // FIXME DELETE
		}
	} else {
		blocks.getBlock(iBlock)->rejected(true);
		if(!hasConverged()) {
			adaptiveBlocks[iBlock]->updateSigma(accepted, oldSample, curSample);
			//std::cout << counter << "\t" << (accepted ? "true\t" : "false\t") << std::endl; // FIXME DELETE
			//std::cout << oldSample.toString() << std::endl; // FIXME DELETE
			//std::cout << curSample.toString() << std::endl; // FIXME DELETE
		}
	}
	if(!hasConverged()) {
		monitorBlockConvergence(iBlock, adaptiveBlocks[iBlock]->updateLambda(alpha));
		//std::cout << idBlock << " -- " << alphas[iB] << std::endl; // FIXME DELETE
		checkConvergence();
	}
	counter++;
}

void AdaptiveProcess::adaptBlocks(Sample &oldSample, Sample &curSample, const int acceptedId,
							 	  const vector<size_t> &iBlocks, const vector<double> &alphas) {

	const int myIdProposal = mcmcMgr.getRankProposal();

	for(uint iB=0; iB<iBlocks.size(); ++iB){ // for each block

		bool isMyBlock = myIdProposal == (int)iB;
		bool accepted = (acceptedId == (int)iB);
		bool rejected = (acceptedId == -1) || ((int)iB < acceptedId);

		// Adaptive blocks do not apply to dynamic blocks
		if(iBlocks[iB] >= adaptiveBlocks.size()) {
			if(accepted) blocks.getBlock(iBlocks[iB])->accepted(true);
			else blocks.getBlock(iBlocks[iB])->rejected(true);
			continue;
		}

		if(accepted) {
			blocks.getBlock(iBlocks[iB])->accepted(isMyBlock);
			if(!hasConverged()) {
				adaptiveBlocks[iBlocks[iB]]->updateSigma(accepted, oldSample, curSample);
				//std::cout << counter << "\t" << (accepted ? "true\t" : "false\t") << std::endl; // FIXME DELETE
				//std::cout << oldSample.toString() << std::endl; // FIXME DELETE
				//std::cout << curSample.toString() << std::endl; // FIXME DELETE
			}
		} else if(rejected) {
			blocks.getBlock(iBlocks[iB])->rejected(isMyBlock);
			if(!hasConverged()) {
				adaptiveBlocks[iBlocks[iB]]->updateSigma(accepted, oldSample, curSample);
				//std::cout << counter << "\t" << (accepted ? "true\t" : "false\t") << std::endl; // FIXME DELETE
				//std::cout << oldSample.toString() << std::endl; // FIXME DELETE
				//std::cout << curSample.toString() << std::endl; // FIXME DELETE
			}
		} else { // If not accepted or rejected : then its thrown away
			blocks.getBlock(iBlocks[iB])->thrown(isMyBlock);
		}
	}

	if(!hasConverged()) {
		for(uint iB=0; iB<iBlocks.size(); ++iB){
			size_t idBlock = iBlocks[iB];

			// Adaptive blocks do not apply to dynamic blocks
			if(idBlock >= adaptiveBlocks.size()) continue;

			monitorBlockConvergence(idBlock, adaptiveBlocks[idBlock]->updateLambda(alphas[iB]));
			//std::cout << idBlock << " -- " << alphas[iB] << std::endl; // FIXME DELETE
		}
		checkConvergence();
	}

	counter++;
}

bool AdaptiveProcess::hasConverged() const {
	return fullConvergence;
}

size_t AdaptiveProcess::getNAdaptiveBlocks() {
	return nAdaptiveBlocks;
}

size_t& AdaptiveProcess::getNAdaptiveBlocksByReference() {
	return nAdaptiveBlocks;
}

std::vector<Adaptive::AdaptiveBlock::sharedPtr_t>& AdaptiveProcess::getAdaptiveBlocks() {
	return adaptiveBlocks;
}

std::vector<Information::BaseInfo::sharedPtr_t>& AdaptiveProcess::getInformations() {
	return informations;
}

bool AdaptiveProcess::hasBlockToOptimize() const {
	bool hasBlockToOptimize = false;
	for(uint iB=0; iB<blocks.getNBaseBlocks(); ++iB){
		const Block::sharedPtr_t block = blocks.getBlock(iB);
		hasBlockToOptimize = hasBlockToOptimize || block->isSupportingBlockOptimisation();
	}
	return hasBlockToOptimize;
}

void AdaptiveProcess::refreshAccessor() {
	pStatsAccessor.map(adaptiveBlocks);
}

Sampler::PStatsAccessor::AdaptiveAccessor* AdaptiveProcess::getParametersStatsAccessor() {
	return &pStatsAccessor;
}

void AdaptiveProcess::saveState(AdaptiveProcessState &state) const {
	state.fullConvergence = fullConvergence;
	state.nAdaptiveBlocks = nAdaptiveBlocks;
	state.counter = counter;
	state.convCnt = convCnt;

	// AdaptiveBlockStats
	state.abStates.resize(adaptiveBlocks.size());
	for(size_t iAB=0; iAB<adaptiveBlocks.size(); ++iAB) {
		adaptiveBlocks[iAB]->saveState(state.abStates[iAB]);
	}
}

void AdaptiveProcess::loadState(AdaptiveProcessState &state) {
	fullConvergence = state.fullConvergence;
	counter = state.counter;
	convCnt = state.convCnt;

	// AdaptiveBlockStats
	assert(blocks.getNBaseBlocks() == state.abStates.size());
	nAdaptiveBlocks = 0;
	adaptiveBlocks.clear();

	for(uint iB=0; iB<blocks.getNBaseBlocks(); ++iB){
		const Block::sharedPtr_t block = blocks.getBlock(iB);

		if(block->isNotAdaptive()) {
			adaptiveBlocks.push_back(Adaptive::createFixedProposal(block, model));
		} else if(block->isSingleDouble()) {
			adaptiveBlocks.push_back(Adaptive::createDefault(block, model, cfgFactory));
			adaptiveBlocks[iB]->loadState(state.abStates[iB]);
			nAdaptiveBlocks++;
		} else if(block->isMixedDouble()) {
			adaptiveBlocks.push_back(Adaptive::createMixed(block, model, cfgFactory));
			adaptiveBlocks[iB]->loadState(state.abStates[iB]);
			nAdaptiveBlocks++;
		} else if(block->isPCADouble()) {
			adaptiveBlocks.push_back(Adaptive::createPCA(block, model, cfgFactory));
			adaptiveBlocks[iB]->loadState(state.abStates[iB]);
			nAdaptiveBlocks++;
		}
	}
}

void AdaptiveProcess::monitorBlockConvergence(const size_t iB, Adaptive::AdaptiveBlock::conv_signal_t convSignal) {
	using Adaptive::AdaptiveBlock;

	if(convSignal == AdaptiveBlock::NO_SIGNAL) return;

	if(!fullConvergence && Sampler::Information::outputManager().getVerbosityThreshold() >= Sampler::Information::MEDIUM_VERB ) {
		AdaptiveBlock::state_t state = adaptiveBlocks[iB]->getState();
		if(state == AdaptiveBlock::DEFAULT_ADAPT) {
			if(convSignal == AdaptiveBlock::SIGNAL_CONV) {
				std::string strInfo("Convergence for block : ");
				strInfo.append(blocks.getBlock(iB)->getName());
				informations.push_back(Information::TextInfo::createTextInfo(strInfo));
			}
		} else if(state == AdaptiveBlock::PCA_INDEP || state == AdaptiveBlock::PCA_INDEP_P2 ||
				  state == AdaptiveBlock::PCA_CORREL || state == AdaptiveBlock::CONVERGED) {
			if(convSignal == AdaptiveBlock::SIGNAL_INDEP) {
				std::stringstream ss;
				double meanScaling = 0.;
				for(size_t iS=0; iS<blocks.getBlock(iB)->size(); ++iS) {
					meanScaling += adaptiveBlocks[iB]->getScaling(iS);
				}
				ss << "No correlations detected in block : " << blocks.getBlock(iB)->getName();
				ss << " (lambda=" << meanScaling/blocks.getBlock(iB)->size() << ")";
				std::string strInfo = ss.str();
				informations.push_back(Information::TextInfo::createTextInfo(strInfo));
			} else if(convSignal == AdaptiveBlock::SIGNAL_CORREL) {
				std::stringstream ss;
				double meanScaling = 0.;
				for(size_t iS=0; iS<blocks.getBlock(iB)->size(); ++iS) {
					meanScaling += adaptiveBlocks[iB]->getScaling(iS);
				}
				ss << "Correlations detected in block : " << blocks.getBlock(iB)->getName();
				ss << " (lambda=" << adaptiveBlocks[iB]->getScaling(0)/blocks.getBlock(iB)->size() << ")";
				std::string strInfo = ss.str();
				informations.push_back(Information::TextInfo::createTextInfo(strInfo));
			} else if(convSignal == AdaptiveBlock::SIGNAL_CONV) {
				std::stringstream ss;
				double meanScaling = 0.;
				for(size_t iS=0; iS<blocks.getBlock(iB)->size(); ++iS) {
					meanScaling += adaptiveBlocks[iB]->getScaling(iS);
				}
				ss << "Convergence for block : " << blocks.getBlock(iB)->getName();
				ss << " (lambda=" << adaptiveBlocks[iB]->getScaling(0)/blocks.getBlock(iB)->size() << ")";
				std::string strInfo = ss.str();
				informations.push_back(Information::TextInfo::createTextInfo(strInfo));
			}
		}
	}
}

bool AdaptiveProcess::checkConvergence() {
	using Adaptive::AdaptiveBlock;

	if(counter % 10 == 0 && !fullConvergence){
		uint nSigmaP1=0, nLambdaP1=0;
		uint nSigmaP2=0, nLambdaP2=0;
		for(size_t iB=0; iB<adaptiveBlocks.size(); ++iB){

			bool hasSigConv = adaptiveBlocks[iB]->hasSigmaConverged();
			bool hasLambConv = adaptiveBlocks[iB]->hasLambdaConverged();

			AdaptiveBlock::state_t state = adaptiveBlocks[iB]->getState();
			bool isP1 = state == AdaptiveBlock::PCA_INDEP;
			bool isP2 = state == AdaptiveBlock::DEFAULT_ADAPT ||
					state == AdaptiveBlock::PCA_INDEP_P2 ||
					state == AdaptiveBlock::PCA_CORREL;
			bool hasConv = state == AdaptiveBlock::CONVERGED;

			if(isP1) {
				if(hasSigConv) nSigmaP1++;
				if(hasLambConv) nLambdaP1++;
			}

			if(isP2) {
				nSigmaP1++;
				nLambdaP1++;
				if(hasSigConv) nSigmaP2++;
				if(hasLambConv) nLambdaP2++;
			}

			if(hasConv) {
				nSigmaP1++;
				nLambdaP1++;
				nSigmaP2++;
				nLambdaP2++;
			}
		}

		std::vector<double> pSigma, pLambda;
		pSigma.push_back(100.*(double)nSigmaP1/(double)nAdaptiveBlocks);
		pSigma.push_back(100.*(double)nSigmaP2/(double)nAdaptiveBlocks);
		pLambda.push_back(100.*(double)nLambdaP1/(double)nAdaptiveBlocks);
		pLambda.push_back(100.*(double)nLambdaP2/(double)nAdaptiveBlocks);

		if(nAdaptiveBlocks >= NB_BLOCK_RELAXATION_TH || (nAdaptiveBlocks < NB_BLOCK_RELAXATION_TH && counter > MINIMAL_ADAPTIVE_PHASE)) {
			for(size_t iB=0; iB<adaptiveBlocks.size(); ++iB){
				adaptiveBlocks[iB]->updateRelaxationTolerances(pSigma, pLambda);
			}
		}

		if((nSigmaP2 != nAdaptiveBlocks || nLambdaP2 != nAdaptiveBlocks) && counter % CONVERGENCE_REPORTING_FREQUENCY == 0){
			if(Sampler::Information::outputManager().getVerbosityThreshold() >= Sampler::Information::MEDIUM_VERB) {
				std::stringstream ss;
				ss << "*** Phase 1 (" << nAdaptiveBlocks << ") : sigma=" << pSigma[0] <<"% (" << nSigmaP1;
				ss << ") :: lambda=" << pLambda[0] <<"% (" << nLambdaP1 << ") && ";
				ss << "Phase 2 : sigma=" << pSigma[1] <<"% (" << nSigmaP2 << ") :: lambda=";
				ss << pLambda[1] <<"% (" << nLambdaP2 << ") ***";
				informations.push_back(Information::TextInfo::createTextInfo(ss));
			}
		} else if ((nSigmaP2 == nAdaptiveBlocks && nLambdaP2 == nAdaptiveBlocks)){
			if(Sampler::Information::outputManager().getVerbosityThreshold() >= Sampler::Information::MEDIUM_VERB) {
				std::stringstream ss;
				ss << "*** Phase 1 (" << nAdaptiveBlocks << ") : sigma=" << pSigma[0] <<"% (" << nSigmaP1 << ") :: lambda=" << pLambda[0] <<"% (" << nLambdaP1 << ") && ";
				ss << "Phase 2 : sigma=" << pSigma[1] <<"% (" << nSigmaP2 << ") :: lambda=" << pLambda[1] <<"% (" << nLambdaP2 << ") ***" << std::endl;
				ss << "FULL CONVERGENCE REACHED";
				std::string tmpStr(ss.str());
				informations.push_back(Information::TextInfo::createTextInfo(ss));
			}
			convCnt = counter;
			fullConvergence = true;
		}
	}

	return fullConvergence;
}

void AdaptiveProcess::updateLocalAlphas(Sample &curSample, const std::vector<size_t> &iBlocks) {
	bool doUpdate = false;
	std::vector<long int> tmpIBlocks(iBlocks.size());
	for(size_t iB=0; iB<iBlocks.size(); ++iB) tmpIBlocks[iB] = iBlocks[iB];


	// If at least one block must be upated : then updated all
	std::set<int> toComputeBlock;
	for(uint iB=0; iB<iBlocks.size(); ++iB){

		// Do not apply to dynamic blocks
		if(iBlocks[iB] >= adaptiveBlocks.size()) continue;

		if(toComputeBlock.count(iBlocks[iB]) == 0) {
			toComputeBlock.insert(iBlocks[iB]);
			if(adaptiveBlocks[iBlocks[iB]]->isReadyForLocUpdate()){
				doUpdate = true;
				break;
			}
		}
	}

	// If the block has already converged we remove the computation request
	toComputeBlock.clear();
	size_t cntConv = 0;
	for(uint iB=0; iB<iBlocks.size(); ++iB){

		// Do not apply to dynamic blocks
		if(iBlocks[iB] >= adaptiveBlocks.size()) {
			tmpIBlocks[iB] = -1;
			continue;
		}

		// Base blocks
		if(adaptiveBlocks[iBlocks[iB]]->hasLocLambdaConverged()){
			tmpIBlocks[iB] = -1;
			cntConv++;
		} else {
			if (toComputeBlock.count(iBlocks[iB]) == 0) {
				toComputeBlock.insert(iBlocks[iB]);
			} else {
				tmpIBlocks[iB] = -1;
			}
		}
	}
	// Check that there are still some unconverged blocks
	doUpdate = doUpdate && (cntConv != iBlocks.size());

	// If there are some blocks to update
	if(doUpdate){
		// We call the dedicated method that update indep MVN and PCA proposals
		std::vector< std::vector<double> > lmAlpha;
		if(mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
			lmAlpha = this->updateLocalAlpha(curSample, tmpIBlocks);
		} else {
			lmAlpha = this->balancedLocalAlphaUpdate(curSample, tmpIBlocks);
			//lmAlpha = this->updateLocalAlpha(tmpIBlocks, propSamples);
		}

		for(uint iB=0; iB<iBlocks.size(); ++iB){
			if (tmpIBlocks[iB] >= 0) {
				adaptiveBlocks[iBlocks[iB]]->updateLocSD(lmAlpha[iB]);
			}
		}
	}
}

std::vector< std::vector<double> > AdaptiveProcess::updateLocalAlpha(Sample &curSample, const std::vector<long int> &iBlocks) {
	const int myIdProposal = mcmcMgr.getRankProposal();

	std::vector<double> tmpAlpha(1, 0);

	if(iBlocks[myIdProposal] >= 0) {
		const Block::sharedPtr_t block = blocks.getBlock(iBlocks[myIdProposal]);
		tmpAlpha.assign(block->getPIndices().size(), 0.);

		// ALL : Generate sample(s)
		std::vector<Sample> propSamples;
		ptrMS->proposeNextSamples(block, curSample, propSamples);

		// For each proposed sample
		for(size_t iPS=0; iPS<propSamples.size(); ++iPS){
			std::vector<Sample> uniqueDiff;
			// For each index in block
			adaptiveBlocks[iBlocks[myIdProposal]]->create1DMoves(curSample, propSamples[iPS], uniqueDiff);

			// Process samples for 1D moves
			if(model.getLikelihood()->isUpdatable()){
				model.processSample(block->getPIndices(), uniqueDiff);
			} else {
				model.processSample(uniqueDiff);
			}

			// Process scores for 1D moves
			std::vector<double> scores(ptrAS->getScores(curSample, block, uniqueDiff));

			// Update local mean
			for(size_t iS=0; iS<block->getPIndices().size(); ++iS){
				tmpAlpha[iS] += std::min(scores[iS], 1.0)/(double)propSamples.size();
			}
		}
		for(size_t iS=0; iS<block->getPIndices().size(); ++iS){
			if(model.getParams().isOverflowed(block->getPIndices()[iS])){
				tmpAlpha[iS] *= -1.;
			}
		}
	}

	// Compute total size and block size for each blocks
	int totalSize = 0;
	int blockSizes[iBlocks.size()];
	for(uint iB=0; iB<iBlocks.size(); ++iB){
		if(iBlocks[iB] >= 0) {
			blockSizes[iB] = blocks.getBlock(iBlocks[iB])->size();
			totalSize += blockSizes[iB];
		} else {
			blockSizes[iB] = 1;
			totalSize += 1;
		}
	}

	std::vector<double> locAlphas(totalSize, 0.);
	// Everybody share informations
	if(mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
		locAlphas = tmpAlpha;
	} else if(mcmcMgr.checkRoleProposal(mcmcMgr.Manager) || mcmcMgr.checkRoleProposal(mcmcMgr.Worker)) { // MASTER
		mcmcMgr.exchangeAllProposal(tmpAlpha.size(), &tmpAlpha[0], blockSizes, &locAlphas[0]);
	}

	uint displ = 0;
	std::vector< std::vector <double> > result;
	for(uint iB=0; iB<iBlocks.size(); ++iB){
		double *beg = &locAlphas[displ];
		double *end = beg + blockSizes[iB];
		result.push_back(std::vector<double>(beg, end));
		displ += blockSizes[iB];
	}

	return result;
}

std::vector< std::vector<double> > AdaptiveProcess::balancedLocalAlphaUpdate(Sample &curSample, const std::vector<long int> &iBlocks) {
	const int myIdProposal = mcmcMgr.getRankProposal();

	// Define jobs
	std::vector< std::pair<size_t, size_t> > jobs;
	for(size_t iB=0; iB<iBlocks.size(); ++iB) {
		if(iBlocks[iB] >= 0) {
			const Block::sharedPtr_t block = blocks.getBlock(iBlocks[iB]);
			for(size_t iP=0; iP<block->getPIndices().size(); ++iP) {
				jobs.push_back(std::pair<size_t, size_t>(iBlocks[iB], iP));
			}
		}
	}

	// Define job attributions
	div_t resDiv = std::div(jobs.size(), mcmcMgr.getNProposal());
	std::vector<size_t> chunkSize, displ;
	for(size_t iP=0; iP<(size_t)mcmcMgr.getNProposal(); ++iP) {
		// Define displacement
		if(iP==0) {
			displ.push_back(0);
		} else {
			displ.push_back(displ.back()+chunkSize.back());
		}

		// Define chunk size
		size_t aChunkSize = resDiv.quot;
		if(iP < (size_t)resDiv.rem) {
			aChunkSize += 1;
		}
		chunkSize.push_back(aChunkSize);
	}

	// Compute alphas
	std::vector<double> myLocAlphas;
	if(chunkSize[myIdProposal] > 0) {
		int iB = -1;
		std::vector<size_t> myPInd;
		for(size_t iJ=displ[myIdProposal]; iJ < displ[myIdProposal]+chunkSize[myIdProposal]; ++iJ) {
			bool hasBlockChanged = (int)jobs[iJ].first != iB;
			bool sameBlockOverAgain = !hasBlockChanged && !myPInd.empty() && jobs[iJ].second < myPInd.back();
			if( hasBlockChanged || sameBlockOverAgain ) { // we change block
				if(iB != -1) {
					// Call computation
					localAlphaComputation(iB, curSample, myPInd, myLocAlphas);
				}

				// Reset
				iB = jobs[iJ].first;
				myPInd.clear();
			}
			myPInd.push_back(jobs[iJ].second);
		}
		// Call computation
		localAlphaComputation(iB, curSample, myPInd, myLocAlphas);

	} else {
		myLocAlphas.push_back(-1.);
	}

	// Share results
	// Compute total size and block size for each blocks
	int totalSize = 0;
	int msgSizes[chunkSize.size()];
	for(size_t iP=0; iP<chunkSize.size(); ++iP){
		msgSizes[iP] = chunkSize[iP] > 0 ? chunkSize[iP] : 1;
		totalSize += msgSizes[iP];
	}

	std::vector<double> locAlphas(totalSize, 0.);
	// Everybody share informations
	mcmcMgr.exchangeAllProposal(myLocAlphas.size(), &myLocAlphas[0], msgSizes, &locAlphas[0]);

	size_t idx = 0;
	std::vector< std::vector <double> > result;
	for(size_t iB=0; iB<iBlocks.size(); ++iB){
		std::vector<double> blockRes;
		if(iBlocks[iB] >= 0) {
			for(size_t iP=0; iP<blocks.getBlock(iBlocks[iB])->size(); ++iP) {
				blockRes.push_back(locAlphas[idx]);
				idx++;
			}
		}
		result.push_back(blockRes);
	}

	return result;
}


void AdaptiveProcess::localAlphaComputation(const size_t iB, Sample &curSample, const std::vector<size_t> &pInd, std::vector<double> &locAlphas) {
	const Block::sharedPtr_t block = blocks.getBlock(iB);

	// ALL : Generate sample(s)
	std::vector<Sample> propSamples;
	ptrMS->proposeNextSamples(block, curSample, propSamples);

	// For each proposed sample
	std::vector<Sample> uniqueDiff, computeUD;
	// For each index in block
	adaptiveBlocks[iB]->create1DMoves(curSample, propSamples.back(), uniqueDiff);

	for(size_t iP=0; iP<pInd.size(); ++iP) {
		computeUD.push_back(uniqueDiff[pInd[iP]]);
	}

	// Process samples for 1D moves
	if(model.getLikelihood()->isUpdatable()){
		model.processSample(block->getPIndices(), computeUD);
	} else {
		model.processSample(computeUD);
	}

	// Process scores for 1D moves
	std::vector<double> scores(ptrAS->getScores(curSample, block, computeUD));

	// Update local mean
	for(size_t iS=0; iS<scores.size(); ++iS){
		double alpha = std::min(scores[iS], 1.0);
		if(model.getParams().isOverflowed(block->getPIndices()[pInd[iS]])){
			alpha *= -1.;
		}
		locAlphas.push_back(alpha);
	}
}

} /* namespace Adaptive */
} /* namespace Sampler */
