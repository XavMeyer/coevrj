//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveBlockState.h
 *
 * @date Jan 25, 2016
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVEBLOCKSTATE_H_
#define ADAPTIVEBLOCKSTATE_H_

#include <stddef.h>

#include "ParameterBlock/BlockStats/BlockStatsState.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

// Forward declaration
namespace Sampler {
	namespace Adaptive {
		class AdaptiveBlock;
		class DefaultAdaptiveBlock;
		class FixedProposalBlock;
		class MixedAdaptiveBlock;
		class PCA_AdaptiveBlock;
	}
}

namespace Sampler {
namespace BlockOptimizer {
	class BlockUpdater;
}
}

namespace Sampler {
namespace Adaptive {
namespace State {

class AdaptiveBlockState {
	friend class Sampler::Adaptive::AdaptiveBlock;
	friend class Sampler::Adaptive::DefaultAdaptiveBlock;
	friend class Sampler::Adaptive::FixedProposalBlock;
	friend class Sampler::Adaptive::MixedAdaptiveBlock;
	friend class Sampler::Adaptive::PCA_AdaptiveBlock;
	friend class Sampler::BlockOptimizer::BlockUpdater;
public:
	AdaptiveBlockState();
	~AdaptiveBlockState();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

private:

	size_t blockId;
	int state;
	double hashSigma;
	ParameterBlock::State::BlockStatsState bsState;

};

} /* namespace State */
} /* namespace Adaptive */
} /* namespace Sampler */

#endif /* ADAPTIVEBLOCKSTATE_H_ */
