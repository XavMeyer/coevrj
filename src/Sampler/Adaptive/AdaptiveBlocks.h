//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveBlocks.h
 *
 * @date Feb 2, 2017
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVEBLOCKS_H_
#define ADAPTIVEBLOCKS_H_

#include "AdaptiveBlock.h"
#include "ParameterBlock/Block.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "Sampler/ParameterStatsAccessor/AccessorsManager.h"

namespace Sampler { class Sample; }
namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace Adaptive {

// Factory
AdaptiveBlock::sharedPtr_t createFixedProposal(const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel);
AdaptiveBlock::sharedPtr_t createDefault(const Sample &initSample, const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel, CF::ConfigFactory::sharedPtr_t aCfgFac);
AdaptiveBlock::sharedPtr_t createMixed(const Sample &initSample, const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel, CF::ConfigFactory::sharedPtr_t aCfgFac);
AdaptiveBlock::sharedPtr_t createPCA(const Sample &initSample, const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel, CF::ConfigFactory::sharedPtr_t aCfgFac);

AdaptiveBlock::sharedPtr_t createDefault(const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel, CF::ConfigFactory::sharedPtr_t aCfgFac);
AdaptiveBlock::sharedPtr_t createMixed(const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel, CF::ConfigFactory::sharedPtr_t aCfgFac);
AdaptiveBlock::sharedPtr_t createPCA(const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel, CF::ConfigFactory::sharedPtr_t aCfgFac);


} /* namespace Adaptive */
} /* namespace Sampler */

#endif /* ADAPTIVEBLOCKS_H_ */
