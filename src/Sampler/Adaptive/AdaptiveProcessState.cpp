//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveProcessState.cpp
 *
 * @date Dec 23, 2016
 * @author meyerx
 * @brief
 */
#include "AdaptiveProcessState.h"

#include <boost/serialization/nvp.hpp>

namespace Sampler {
namespace Adaptive {

AdaptiveProcessState::AdaptiveProcessState() {
	fullConvergence = false;
	nAdaptiveBlocks = counter = convCnt = 0;
}

AdaptiveProcessState::~AdaptiveProcessState() {
}

template<class Archive>
void AdaptiveProcessState::save(Archive & ar, const unsigned int version) const {
	ar << BOOST_SERIALIZATION_NVP( fullConvergence );
	ar << BOOST_SERIALIZATION_NVP( nAdaptiveBlocks );
	ar << BOOST_SERIALIZATION_NVP( counter );
	ar << BOOST_SERIALIZATION_NVP( convCnt );

	// AdaptiveBlockStats
	ar << BOOST_SERIALIZATION_NVP( abStates );
}

template<class Archive>
void AdaptiveProcessState::load(Archive & ar, const unsigned int version) {

	ar >> BOOST_SERIALIZATION_NVP( fullConvergence );
	ar >> BOOST_SERIALIZATION_NVP( nAdaptiveBlocks );
	ar >> BOOST_SERIALIZATION_NVP( counter );
	ar >> BOOST_SERIALIZATION_NVP( convCnt );

	// Adaptive block
	ar >> BOOST_SERIALIZATION_NVP( abStates );
}

template void AdaptiveProcessState::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void AdaptiveProcessState::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void AdaptiveProcessState::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void AdaptiveProcessState::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Adaptive */
} /* namespace Sampler */
