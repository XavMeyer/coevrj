//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ParametersStatsManager.cpp
 *
 * @date Jan 10, 2017
 * @author meyerx
 * @brief
 */
#include "AccessorsManager.h"

#include <assert.h>
#include <stddef.h>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

namespace Sampler {
namespace PStatsAccessor {

AccessorsManager::AccessorsManager() : globalAccessor(NULL) {
}

AccessorsManager::~AccessorsManager() {
}

double AccessorsManager::getMean(size_t pId) {
	queryResult_t res = requestMean(pId);
	assert(res.first);
	return res.second;
}

double AccessorsManager::getVariance(size_t pId) {
	queryResult_t res = requestVariance(pId);
	assert(res.first);
	return res.second;
}

double AccessorsManager::getScaling(size_t pId) {
	queryResult_t res = requestScaling(pId);
	assert(res.first);
	return res.second;
}

double AccessorsManager::getCovariance(size_t pId1, size_t pId2) {
	queryResult_t res = requestCovariance(pId1, pId2);
	assert(res.first);
	return res.second;
}

queryResult_t AccessorsManager::requestMean(size_t pId) {
	using namespace boost::accumulators;
	accumulator_set<double, stats<tag::mean> > acc;

	if(globalAccessor != NULL) {
		queryResult_t res = globalAccessor->requestMean(pId);
		if(res.first) acc(res.second);
	}

	for(size_t iA=0; iA<proposalAccessors.size(); ++iA) {
		queryResult_t res = proposalAccessors[iA]->requestMean(pId);
		if(res.first) acc(res.second);
	}

	if(count(acc) > 0) {
		return queryResult_t(true, mean(acc));
	} else {
		return queryResult_t(false, 0.);
	}
}

queryResult_t AccessorsManager::requestVariance(size_t pId) {
	using namespace boost::accumulators;
	accumulator_set<double, stats<tag::mean> > acc;

	if(globalAccessor != NULL) {
		queryResult_t res = globalAccessor->requestVariance(pId);
		if(res.first) acc(res.second);
	}

	for(size_t iA=0; iA<proposalAccessors.size(); ++iA) {
		queryResult_t res = proposalAccessors[iA]->requestVariance(pId);
		if(res.first) acc(res.second);
	}

	if(count(acc) > 0) {
		return queryResult_t(true, mean(acc));
	} else {
		return queryResult_t(false, 0.);
	}
}

queryResult_t AccessorsManager::requestScaling(size_t pId) {
	using namespace boost::accumulators;
	accumulator_set<double, stats<tag::mean> > acc;

	if(globalAccessor != NULL) {
		queryResult_t res = globalAccessor->requestScaling(pId);
		if(res.first) acc(res.second);
	}

	for(size_t iA=0; iA<proposalAccessors.size(); ++iA) {
		queryResult_t res = proposalAccessors[iA]->requestScaling(pId);
		if(res.first) acc(res.second);
	}

	if(count(acc) > 0) {
		return queryResult_t(true, mean(acc));
	} else {
		return queryResult_t(false, 0.);
	}
}

queryResult_t AccessorsManager::requestCovariance(size_t pId1, size_t pId2) {
	using namespace boost::accumulators;
	accumulator_set<double, stats<tag::mean> > acc;

	if(globalAccessor != NULL) {
		queryResult_t res = globalAccessor->requestCovariance(pId1, pId2);
		if(res.first) acc(res.second);
	}

	for(size_t iA=0; iA<proposalAccessors.size(); ++iA) {
		queryResult_t res = proposalAccessors[iA]->requestCovariance(pId1, pId2);
		if(res.first) acc(res.second);
	}

	if(count(acc) > 0) {
		return queryResult_t(true, mean(acc));
	} else {
		return queryResult_t(false, 0.);
	}
}

void AccessorsManager::registerProposalAccessor(BaseAccessor* aAccessor) {
	proposalAccessors.push_back(aAccessor);
}

void AccessorsManager::registerGlobalAccessor(BaseAccessor* aAccessor) {
	globalAccessor = aAccessor;
}

} /* namespace PStatsAccessor */
} /* namespace Sampler */
