//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseAccessor.h
 *
 * @date Jan 10, 2017
 * @author meyerx
 * @brief
 */
#ifndef BASEACCESSOR_H_
#define BASEACCESSOR_H_

#include <utility>
#include <stdlib.h>

namespace Sampler {
namespace PStatsAccessor {

typedef std::pair< bool, double > queryResult_t;

class BaseAccessor {
public:
	BaseAccessor() {};
	virtual ~BaseAccessor() {};

	virtual queryResult_t requestMean(size_t pId) = 0;
	virtual queryResult_t requestVariance(size_t pId) = 0;
	virtual queryResult_t requestScaling(size_t pId) = 0;
	virtual queryResult_t requestCovariance(size_t pId1, size_t pId2) = 0;
};

} /* namespace PStatsAccessor */
} /* namespace Sampler */

#endif /* BASEACCESSOR_H_ */
