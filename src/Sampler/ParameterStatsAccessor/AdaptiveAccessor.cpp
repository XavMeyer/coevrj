//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveAccessor.cpp
 *
 * @date Jan 10, 2017
 * @author meyerx
 * @brief
 */
#include "AdaptiveAccessor.h"

#include <assert.h>
#include <stddef.h>

namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace PStatsAccessor {

AdaptiveAccessor::AdaptiveAccessor(StatisticalModel::Parameters &parameters) {

	init(parameters);
}

AdaptiveAccessor::~AdaptiveAccessor() {
}

void AdaptiveAccessor::init(StatisticalModel::Parameters &parameters) {
	mapping.assign(parameters.getNBaseParameters(), mapping_t(0, NULL)); // init
}

void AdaptiveAccessor::map(std::vector<Adaptive::AdaptiveBlock::sharedPtr_t> &adaptiveBlocks) {

	mapping.assign(mapping.size(), mapping_t(0, NULL)); // reset

	for(size_t iB=0; iB<adaptiveBlocks.size(); ++iB) {
		const ParameterBlock::Block::sharedPtr_t block = adaptiveBlocks[iB]->getBlock();

		for(size_t iP=0; iP<block->size(); ++iP) { 						// For each parameter in block
			size_t pId = block->getPIndice(iP);							// Get the parameter ID
			mapping[pId] = mapping_t(iP, adaptiveBlocks[iB].get());		// Map the pID to the internal pID=>iP and the adaptive block
		}
	}

}

queryResult_t AdaptiveAccessor::requestMean(size_t pId) {
	assert(pId < mapping.size() && mapping[pId].second != NULL);
	if(mapping[pId].second->isAdaptive()) {
		return queryResult_t(true, mapping[pId].second->getMean(mapping[pId].first));
	} else {
		return queryResult_t(false, 0.);
	}
}

queryResult_t AdaptiveAccessor::requestVariance(size_t pId) {
	assert(pId < mapping.size() && mapping[pId].second != NULL);
	if(mapping[pId].second->isAdaptive()) {
		return queryResult_t(true, mapping[pId].second->getVariance(mapping[pId].first));
	} else {
		return queryResult_t(false, 0.);
	}
}

queryResult_t AdaptiveAccessor::requestScaling(size_t pId) {
	assert(pId < mapping.size() && mapping[pId].second != NULL);
	if(mapping[pId].second->isAdaptive()) {
		return queryResult_t(true, mapping[pId].second->getScaling(mapping[pId].first));
	} else {
		return queryResult_t(false, 0.);
	}
}

queryResult_t AdaptiveAccessor::requestCovariance(size_t pId1, size_t pId2) {
	assert(pId1 < mapping.size() && pId2 < mapping.size());
	assert(mapping[pId1].second != NULL && mapping[pId2].second != NULL);

	 // Check if it is adaptive and in the same block
	if(mapping[pId1].second->isAdaptive() && mapping[pId1].second == mapping[pId2].second) {
			size_t intPID1 = mapping[pId1].first;
			size_t intPID2 = mapping[pId2].first;
		return queryResult_t(true, mapping[pId1].second->getCovariance(intPID1, intPID2));
	} else { // Is not estimated
		return queryResult_t(false, 0.);
	}
}

} /* namespace PStatsAccessor */
} /* namespace Sampler */
