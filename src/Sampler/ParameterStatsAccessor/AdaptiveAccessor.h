//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveAccessor.h
 *
 * @date Jan 10, 2017
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVEACCESSOR_H_
#define ADAPTIVEACCESSOR_H_

#include <assert.h>
#include <vector>

#include "BaseAccessor.h"
#include "Model/Parameter/Parameters.h"
#include "Sampler/Adaptive/AdaptiveBlock.h"

namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace PStatsAccessor {

class AdaptiveAccessor : public BaseAccessor {
public:
	AdaptiveAccessor(StatisticalModel::Parameters &parameters);
	~AdaptiveAccessor();

	void map(std::vector<Adaptive::AdaptiveBlock::sharedPtr_t> &adaptiveBlocks);

	queryResult_t requestMean(size_t pId);
	queryResult_t requestVariance(size_t pId);
	queryResult_t requestScaling(size_t pId);
	queryResult_t requestCovariance(size_t pId1, size_t pId2);

private:

	typedef std::pair< size_t, Adaptive::AdaptiveBlock* > mapping_t;
	std::vector< mapping_t > mapping;

	void init(StatisticalModel::Parameters &parameters);
};

} /* namespace PStatsAccessor */
} /* namespace Sampler */

#endif /* ADAPTIVEACCESSOR_H_ */
