//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PrefetchingState.h
 *
 * @date Dec 23, 2016
 * @author meyerx
 * @brief
 */
#ifndef PREFETCHINGSTATE_H_
#define PREFETCHINGSTATE_H_

#include "Parallel/RNG/StateRNG/StateRNG.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

namespace Sampler {

class Prefetching;

class PrefetchingState {
public:
	PrefetchingState();
	~PrefetchingState();
private:

	double timeP1, timeP2, timeI, timeC, timeW, timeTot, timeP1_1, timeP1_2, timeP1_3;
	StateRNG blockSelRNGState;

	// Boost serialization
	friend class Prefetching;
	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace Sampler */

#endif /* PREFETCHINGSTATE_H_ */
