//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Prefetching.cpp
 *
 * @date Dec 13, 2016
 * @author meyerx
 * @brief
 */
#include "Prefetching.h"

#include <stddef.h>

#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Model.h"
#include "Parallel/Manager/MCMCManager.h"
#include "ParameterBlock/Block.h"
#include "mpi.h"
#include "Sampler/Information/RemoteInfo.h"

namespace ParameterBlock { class Blocks; }
namespace Sampler { class Sample; }

// Define macro // to avoid time measures
#define TIME_MEASURE_PFMCMC

namespace Sampler {

Prefetching::Prefetching(ParameterBlock::Blocks &aBlocks, StatisticalModel::Model &aModel,
				 		 Strategies::BlockSelector::sharedPtr_t aPtrBS,
				 		 Strategies::AcceptanceStrategy::sharedPtr_t aPtrAS,
				 		 Strategies::ModifierStrategy::sharedPtr_t aPtrMS) :
								 mcmcMgr(Parallel::mcmcMgr()), blocks(aBlocks), model(aModel),
				 				 ptrBS(aPtrBS), ptrAS(aPtrAS), ptrMS(aPtrMS) {

	timeP1 = timeP2 = timeI = timeC = timeW = timeTot = timeP1_1 = timeP1_2 = timeP1_3 = 0.;

}

Prefetching::~Prefetching() {
}

void Prefetching::processNextIteration(Sampler::Sample &curSample, OutcomePFAMCMC::sharedPtr_t outcome) {

	using ParameterBlock::Block;

	int acceptedId = 0;
	int nReject = 0;
	std::vector<double> alphas;

	TIME_MEASURE_PFMCMC double s1 = 0., sP1 = 0., sP2 = 0., sP1_1 = 0., sP1_2 = 0., sP1_3 = 0.;
	TIME_MEASURE_PFMCMC double e1 = 0., eP1 = 0., eP2 = 0., eP1_1 = 0., eP1_2 = 0., eP1_3 = 0.;

	TIME_MEASURE_PFMCMC s1 = MPI_Wtime();

	// If no roles, return
	if(mcmcMgr.checkRoleProposal(mcmcMgr.None)) return;

	// Save curSample
	// curSample.setChanged(false);
	// oldSample = curSample;

	// ALL : Get block
	TIME_MEASURE_PFMCMC  double sI1 = MPI_Wtime();
	const int myIdProposal = mcmcMgr.getRankProposal();
	const std::vector<size_t> iBlocks(ptrBS->selectBlocks(curSample));
	const Block::sharedPtr_t block = blocks.getBlock(iBlocks[myIdProposal]);
	//std::cout << "PFMCMC - block name : " << block->getName() << std::endl; // FIXME DEBUG
	// Update outcome
 	outcome->setBlockIds(iBlocks);

	// ALL : Generate sample(s)
	std::vector<Sample> propSamples;
	ptrMS->proposeNextSamples(block, curSample, propSamples);
	TIME_MEASURE_PFMCMC  double eI1 = MPI_Wtime();
	TIME_MEASURE_PFMCMC timeI += eI1 - sI1;
 	//std::cout << "Current  : " << curSample.toString() << std::endl; // TODO REMOVE
 	//std::cout << "Proposed  : " << propSamples.front().toString() << std::endl; // TODO REMOVE


#ifdef DBG_PREDICTIVE
	DBG_logPredictive(block, iBlocks, propSamples);
#endif

	// ALL : Process sample(s)
	TIME_MEASURE_PFMCMC  double sC1 = MPI_Wtime();
	if(model.getLikelihood()->isUpdatable()){
		model.processSample(block->getPIndices(), propSamples);
	} else {
		model.processSample(propSamples);
	}

	// ALL Process score(s)
	std::vector<double> scores(ptrAS->getScores(curSample, block, propSamples));

	block->updateMeanTimes(eI1 - sI1, MPI_Wtime()-sC1); // Do not take into account the wasted time

	TIME_MEASURE_PFMCMC  double sW1 = MPI_Wtime();
	TIME_MEASURE_PFMCMC mcmcMgr.syncProposals(); // FIXME
	TIME_MEASURE_PFMCMC  double eW1 = MPI_Wtime();
	TIME_MEASURE_PFMCMC timeW += eW1 - sW1;

	TIME_MEASURE_PFMCMC  double eC1 = MPI_Wtime();
	TIME_MEASURE_PFMCMC timeC += eC1-sC1;

	if(mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
		// Do the acceptance check and update based on blocks, proposed samples and scores.
		doSequentialAcceptanceAndUpdate(curSample, propSamples, scores, outcome);

		TIME_MEASURE_PFMCMC e1 = MPI_Wtime();
		TIME_MEASURE_PFMCMC timeTot += e1-s1;

		return;
	} else if(mcmcMgr.checkRoleProposal(mcmcMgr.Manager)) { // MASTER
		// Gather scores
		size_t nScores = mcmcMgr.getNProposal()*propSamples.size();
		std::vector<double> globalScores(nScores, 0.);

		TIME_MEASURE_PFMCMC sP1 = MPI_Wtime();
		TIME_MEASURE_PFMCMC sP1_1 = MPI_Wtime();
		mcmcMgr.gatherScores(scores.size(), &scores[0], &globalScores[0]);
		TIME_MEASURE_PFMCMC eP1_1 = MPI_Wtime();
		TIME_MEASURE_PFMCMC timeP1_1 += eP1_1 - sP1_1;

		// Check acceptance
		TIME_MEASURE_PFMCMC sP1_2 = MPI_Wtime();
		acceptedId = acceptanceCheck(globalScores, nReject);

		// Send one mean alpha per block
		std::vector<double> buffer;
		buffer.assign(2+iBlocks.size(), 0.);
		prepareScores(propSamples.size(), globalScores, alphas);
		if(block->size() == 1 && model.getParams().isOverflowed(block->getPIndices()[0])){
			alphas[0] *= -1.; // In case of overflow : notify the block
		}

		buffer[0] = acceptedId;
		buffer[1] = nReject;
		for(uint iB=2; iB<buffer.size(); ++iB){
			buffer[iB] = alphas[iB-2];
		}
		TIME_MEASURE_PFMCMC eP1_2 = MPI_Wtime();
		TIME_MEASURE_PFMCMC timeP1_2 += eP1_2 - sP1_2;

		// Broadcast the acceptance check result
		TIME_MEASURE_PFMCMC sP1_3 = MPI_Wtime();
		int root = mcmcMgr.getManagerProposal();
		mcmcMgr.bcastProposal(buffer.size(), &buffer[0], root);
		TIME_MEASURE_PFMCMC eP1_3 = MPI_Wtime();
		TIME_MEASURE_PFMCMC timeP1_3 += eP1_3 - sP1_3;

		TIME_MEASURE_PFMCMC eP1 = MPI_Wtime();
		TIME_MEASURE_PFMCMC timeP1 += eP1 - sP1;
	} else if(mcmcMgr.checkRoleProposal(mcmcMgr.Worker)) { // SLAVE
		// Send Score(s)
		double *dummyPtr = NULL;
		mcmcMgr.gatherScores(scores.size(), &scores[0], dummyPtr);

		std::vector<double> buffer;
		buffer.assign(2+iBlocks.size(), 0.);


		// Get acceptance check result
		int dest = mcmcMgr.getManagerProposal();
		mcmcMgr.bcastProposal(buffer.size(), &buffer[0], dest);
		acceptedId = buffer[0];
		nReject = buffer[1];

		for(uint iB=2; iB<buffer.size(); ++iB){
			alphas.push_back(buffer[iB]);
		}

	}

	TIME_MEASURE_PFMCMC sP2 = MPI_Wtime();
	int aRank, aRow;
	defineSamplePosition(acceptedId, aRank, aRow, propSamples.size());

	// Update outcome
	outcome->setAcceptedId(acceptedId);
	outcome->setNRejected(nReject);
	outcome->setAlphaScore(alphas);

	// ALL : Exchange sample
	exchangeSample(aRank, aRow, propSamples, outcome);
	TIME_MEASURE_PFMCMC eP2 = MPI_Wtime();
	TIME_MEASURE_PFMCMC timeP2 += eP2 - sP2;

	TIME_MEASURE_PFMCMC e1 = MPI_Wtime();
	TIME_MEASURE_PFMCMC timeTot += e1-s1;

 	ptrBS->rewind(acceptedId);
}

void Prefetching::doSequentialAcceptanceAndUpdate(Sampler::Sample &curSample, std::vector<Sampler::Sample> &propSamples,
												  std::vector<double> &scores, OutcomePFAMCMC::sharedPtr_t outcome) {

	using ParameterBlock::Block;

	int nReject = 0;

	const int myIdProposal = mcmcMgr.getRankProposal();
	const Block::sharedPtr_t block = blocks.getBlock(outcome->getBlockIds()[myIdProposal]);

	TIME_MEASURE_PFMCMC double sAcc = MPI_Wtime();

	// Do acceptance check
	int acceptedId = acceptanceCheck(scores, nReject);
	outcome->setAcceptedId(acceptedId);
	outcome->setNRejected(nReject);
 	//std::cout << "Acceptance check  : " << scores.front() << " --> " << acceptedId << " / " << nReject << std::endl; // TODO REMOVE

	// If accepted update likelihood
	if(acceptedId >= 0){
		outcome->getAcceptedSamples().push_back(propSamples[acceptedId]);
		outcome->getAcceptedSamples().back().setChanged(true);
	}

	TIME_MEASURE_PFMCMC double eAcc = MPI_Wtime();
	TIME_MEASURE_PFMCMC timeP1 += eAcc-sAcc;

	// Prepare the score for the adaptive phase
	prepareScores(propSamples.size(), scores, outcome->getAlphaScore());

	if(block->size() == 1 && model.getParams().isOverflowed(block->getPIndices()[0])){
		outcome->getAlphaScore()[0] *= -1.; // In case of overflow : notify the block
	}

 	ptrBS->rewind(acceptedId);

 	/*************************** FIXME DEBUG *********************/
 	/*if(block->isDynamicBlock()) {
		if(acceptedId >= 0) {
			std::cout << "[ACCEPTED] : " << block->getName()  << std::endl;
		} else {
			std::cout << "[REJECTED] : " << block->getName()  << std::endl;
		}
		for(size_t iP=0; iP<block->getPIndices().size(); ++iP) {
			std::cout << curSample.getDoubleParameter(block->getPIndices()[iP]) << "\t";
		}
		std::cout << std::endl;
		for(size_t iP=0; iP<block->getPIndices().size(); ++iP) {
			std::cout << propSamples.front().getDoubleParameter(block->getPIndices()[iP]) << "\t";
		}
		std::cout << std::endl << "-----------------------------------------------------------------" << std::endl;
	}*/
}

void Prefetching::prepareScores(const uint nPropSample, const std::vector<double> &scores, std::vector<double> &alphas) const {
	uint nProp = mcmcMgr.getNProposal();
    alphas.assign(nProp, 0.);

    // Get the alphas score given the acceptance score
	for(size_t iB=0; iB<nProp; ++iB){
		// if multiple sample where proposed, alphas[i] define the average acceptance rate
		for(size_t iP=0; iP<nPropSample; ++iP){
			alphas[iB] += (std::min(scores[iB*nPropSample+iP], 1.0));
		}
		alphas[iB] /= (double) nPropSample;
	}

}


int Prefetching::acceptanceCheck(const std::vector<double> &scores, int &nReject) {
	std::vector<int> accepted = ptrAS->acceptanceCheck(scores);

	// Check for acceptance
	nReject = 0;
	if(!accepted.empty()){
		for(std::vector<int>::iterator itA=accepted.begin(); itA != accepted.end(); ++itA){
			if(*itA < 0) {
				nReject -= *itA;
			} else {
				return *itA;
			}
		}
	} else {
		nReject = scores.size();
	}
	return -1;
}

void Prefetching::defineSamplePosition(const int acceptId, int &aRank, int &aRow, const int propSize) {
	if(acceptId < 0) {
		aRank = -1;
		aRow = -1;
	} else {
		aRank = floor((float)acceptId / propSize); // Rank in Proposal communicator are 0...NProp-1
		aRow = acceptId % propSize;
	}
}

void Prefetching::exchangeSample(const int aRank, const int aRow, const std::vector<Sample> &propSamples,
								 OutcomePFAMCMC::sharedPtr_t outcome) {

	if(aRank < 0 || mcmcMgr.checkRoleProposal(mcmcMgr.Sequential) || mcmcMgr.checkRoleProposal(mcmcMgr.None)) {
		//pMCMC::mpiMgr().print("here");
		return;
	}

	// A sample has been accepted :
	Sample newSample;
	// aRank send the serialized sample
	if(mcmcMgr.getRankProposal() == aRank) {
		newSample = propSamples.front();
		Utils::Serialize::buffer_t buffer = newSample.save();
		Parallel::mcmcMgr().bcastSample(buffer, aRank);
		Information::RemoteInfo::sharedPtr_t ptrRInfo(new Information::RemoteInfo(false));
		outcome->addInformation(ptrRInfo);
	} else { // Other recieve it
		Utils::Serialize::buffer_t buffer;
		Parallel::mcmcMgr().bcastSample(buffer, aRank);
		newSample.load(buffer);
		Information::RemoteInfo::sharedPtr_t ptrRInfo(new Information::RemoteInfo(true));
		outcome->addInformation(ptrRInfo);
	}

	// Update the current state
	outcome->getAcceptedSamples().push_back(newSample);
	outcome->getAcceptedSamples().back().setChanged(true);
}

void Prefetching::saveState(PrefetchingState &state) const {
	state.timeP1 = timeP1 ;
	state.timeP2 = timeP2 ;
	state.timeI = timeI ;
	state.timeC = timeC ;
	state.timeW = timeW ;
	state.timeTot = timeTot ;
	state.timeP1_1 = timeP1_1 ;
	state.timeP1_2 = timeP1_2 ;
	state.timeP1_3 = timeP1_3 ;

	if(ptrBS->getRNG() != NULL) {
		ptrBS->getRNG()->saveState(state.blockSelRNGState);
	}
}

void Prefetching::loadState(PrefetchingState &state) {
	timeP1 = state.timeP1;
	timeP2 = state.timeP2;
	timeI = state.timeI;
	timeC = state.timeC;
	timeW = state.timeW;
	timeTot = state.timeTot;
	timeP1_1 = state.timeP1_1;
	timeP1_2 = state.timeP1_2;
	timeP1_3 = state.timeP1_3;

	if(ptrBS->getRNG() != NULL) {
		ptrBS->getRNG()->loadState(state.blockSelRNGState);
	}
}

std::string Prefetching::reportStatus(std::string &prefix, size_t iT, double timeTotal) const {
	std::stringstream sstr;
	sstr << prefix << "[PFMCMC] Time Block Sel + propose sample :\t" << timeI << "\t" << timeI/(double)iT << "\t" << 100*timeI/timeTotal << endl;
	if(mcmcMgr.isProposalSequential()) {
		sstr << prefix << "[PFMCMC] Time ACC :\t" << timeP1 << "\t" << timeP1/(double)iT << "\t" << 100*timeP1/timeTotal << endl;
	} else {
		sstr << prefix << "[PFMCMC] Time COM 1 :\t" << timeP1 << "\t" << timeP1/(double)iT << "\t" << 100*timeP1/timeTotal << endl;
		sstr << prefix << "[PFMCMC] Time COM 1_1 :\t" << timeP1_1 << "\t" << timeP1_1/(double)iT << "\t" << 100*timeP1_1/timeTotal << endl;
		sstr << prefix << "[PFMCMC] Time COM 1_2 :\t" << timeP1_2 << "\t" << timeP1_2/(double)iT << "\t" << 100*timeP1_2/timeTotal << endl;
		sstr << prefix << "[PFMCMC] Time COM 1_3 :\t" << timeP1_3 << "\t" << timeP1_3/(double)iT << "\t" << 100*timeP1_3/timeTotal << endl;
		sstr << prefix << "[PFMCMC] Time COM 2 :\t" << timeP2 << "\t" << timeP2/(double)iT << "\t" << 100*timeP2/timeTotal << endl;
	}
	sstr << prefix << "[PFMCMC] Time COMPUTE :\t" << timeC << "\t" << timeC/(double)iT << "\t" << 100*timeC/timeTotal << endl;
	if(!mcmcMgr.isProposalSequential()) {
		sstr << prefix << "[PFMCMC] >>Time WASTED :\t" << timeW << "\t" << timeW/(double)iT << "\t" << 100*timeW/timeC << endl;
	}
	return sstr.str();
}

} /* namespace Sampler */
