//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseProposal.h
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#ifndef BASEPROPOSAL_H_
#define BASEPROPOSAL_H_

#include <boost/shared_ptr.hpp>

#include "State/BaseProposalState.h"

namespace Sampler { class Sample; }

namespace Sampler {
namespace Proposal {

namespace Handler {
class Manager;
}
class Selector;


class BaseProposal {
public:
	typedef boost::shared_ptr<BaseProposal> sharedPtr_t;

public:
	BaseProposal();
	virtual ~BaseProposal();

	// Register the Proposals in the Sampler mechanism (selector and handler)
	virtual void registerToSelector(Selector *selector) = 0;
	virtual void registerToHandlerManager(Sampler::Sample &initSample, Handler::Manager *hm) = 0;

	// Must be override for proposals requiring checkpointing
	virtual State::BaseProposalState::sharedPtr_t saveState() const = 0;
	virtual void loadState(State::BaseProposalState::sharedPtr_t state) = 0;

	virtual std::string reportStatus() {return "";}

protected:

private:

};

} /* namespace Proposal */
} /* namespace Sampler */

#endif /* BASEPROPOSAL_H_ */
