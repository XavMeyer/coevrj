//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ProposalMC3.h
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#ifndef PROPOSALMC3_H_
#define PROPOSALMC3_H_

#include "BaseProposal.h"
#include "Model/Model.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "SelectionCriterion/FixedSelectionCriterion.h"

namespace Sampler {
namespace Proposal {

using namespace ParameterBlock::Config;

class ProposalMC3: public BaseProposal {
public:
	ProposalMC3(size_t aSeed, StatisticalModel::Model &aModel, ConfigFactory::sharedPtr_t aCfgFactory);
	ProposalMC3(size_t aFreq, size_t aSeed, StatisticalModel::Model &aModel, ConfigFactory::sharedPtr_t aCfgFactory);
	~ProposalMC3();

	void registerToSelector(Selector *selector);
	void registerToHandlerManager(Sampler::Sample &initSample, Handler::Manager *hm);

	State::BaseProposalState::sharedPtr_t saveState() const ;
	void loadState(State::BaseProposalState::sharedPtr_t state);

	RNG* getRNG();
	StatisticalModel::Model& getModel();
	BlockStatCfg::sharedPtr_t getConfig();

private:

	StatisticalModel::Model &model;
	BlockStatCfg::sharedPtr_t cfgMC3;
	RNG *rng;

	Utils::FixedSelectionCriterion fsc;

	size_t getTunedFreqMC3(size_t aFreq) const;
};

} /* namespace Proposal */
} /* namespace Sampler */

#endif /* PROPOSALMC3_H_ */
