//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockDispatcher.cpp
 *
 * @date Apr 14, 2017
 * @author meyerx
 * @brief
 */
#include "BlockDispatcher.h"

#include <assert.h>

#include "Sampler/Proposal/ProposalPFAMCMC.h"


namespace Sampler {
namespace Proposal {

BlockDispatcher::BlockDispatcher() {

}

BlockDispatcher::~BlockDispatcher() {
}

void BlockDispatcher::registerBlocks(ProposalPFAMCMC* ptrProposal) {
	BlocksAndSource bas;
	bas.source = PFAMCMC_SOURCE;
	bas.ptrBlocks = &ptrProposal->getBlocks();
	vecBlocksAndSource.push_back(bas);
}

size_t BlockDispatcher::addDynamicBlock(ParameterBlock::blockSharedPtr_t ptrBlock) {
	vecBlocksAndSource_t vecBS = getBlocksSource(ptrBlock);
	assert(!vecBS.empty());

	size_t nAdded = 0;
	for(size_t iB = 0; iB<vecBS.size(); ++iB) {
		vecBS[iB].ptrBlocks->addDynamicBlock(ptrBlock);
		nAdded++;
	}
	return nAdded;
}

size_t BlockDispatcher::removeDynamicBlock(ParameterBlock::blockSharedPtr_t ptrBlock) {
	vecBlocksAndSource_t vecBS = getBlocksSource(ptrBlock);
	assert(!vecBS.empty());

	size_t nDeleted = 0;
	for(size_t iB = 0; iB<vecBS.size(); ++iB) {
		bool found = vecBS[iB].ptrBlocks->tryToRemoveDynamicBlock(ptrBlock->getId());
		if(found) nDeleted++;
	}
	return nDeleted;
}

size_t BlockDispatcher::removeDynamicBlock(size_t idBlock) {
	size_t nDeleted = 0;
	for(vecBlocksAndSource_t::iterator it = vecBlocksAndSource.begin(); it != vecBlocksAndSource.end(); ++it) {
		bool found = it->ptrBlocks->tryToRemoveDynamicBlock(idBlock);
		if(found) nDeleted++;
	}
	return nDeleted;
}

BlockDispatcher::vecBlocksAndSource_t BlockDispatcher::getBlocksSources() const{
	return vecBlocksAndSource;
}

BlockDispatcher::vecBlocksAndSource_t BlockDispatcher::getBlocksSource(ParameterBlock::blockSharedPtr_t ptrBlock) {

	std::vector<blocksSource_t> vecSources = getBlockSources(ptrBlock);
	BlockDispatcher::vecBlocksAndSource_t vecFoundBS;

	vecBlocksAndSource_t::iterator it;
	for(size_t iS = 0; iS<vecSources.size(); ++iS) {
		for(size_t iB=0; iB<vecBlocksAndSource.size(); ++iB) {
			if(vecBlocksAndSource[iB].source == vecSources[iS]) {
				vecFoundBS.push_back(vecBlocksAndSource[iB]);
				break;
			}
		}
	}
	return vecFoundBS;
}

std::vector<BlockDispatcher::blocksSource_t> BlockDispatcher::getBlockSources(ParameterBlock::blockSharedPtr_t ptrBlock) const {

	std::vector<BlockDispatcher::blocksSource_t> blocksSources;

	if( ptrBlock->isNotAdaptive() ||
			ptrBlock->isSingleDouble() ||
			ptrBlock->isMixedDouble() ||
			ptrBlock->isPCADouble() ||
			ptrBlock->isLangevin() ||
			ptrBlock->isSMALA() ||
			ptrBlock->isTreeLangevin()) {
		blocksSources.push_back(PFAMCMC_SOURCE);
	}

	if( ptrBlock->isParallelLangevin() ||
		ptrBlock->isParallelTreeLangevin()) {
		blocksSources.push_back(GRADIENT_SOURCE);
	}

	return blocksSources;
}

} /* namespace Proposal */
} /* namespace Sampler */
