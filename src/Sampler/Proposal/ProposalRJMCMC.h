//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ProposalRJMCMC.h
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#ifndef PROPOSALRJMCMC_H_
#define PROPOSALRJMCMC_H_

#include <stddef.h>

#include "Sampler/Proposal/State/BaseProposalState.h"
#include "BaseProposal.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "ParameterBlock/Blocks.h"
#include "Sampler/ReversibleJump/RJMoveManager.h"
#include "SelectionCriterion/RandomSelectionCriterion.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"

class RNG;
namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace Proposal {

class Selector;
namespace Handler { class Manager; }

using namespace ParameterBlock;

class ProposalRJMCMC: public BaseProposal {
public:
	typedef boost::shared_ptr<ProposalRJMCMC> sharedPtr_t;
public:

	ProposalRJMCMC(size_t aSeed, size_t aBurnin,Config::ConfigFactory::sharedPtr_t aCfgFactory,
			       StatisticalModel::Model &aModel, RJMCMC::RJMoveManager::sharedPtr_t aRJMoveManager);
	ProposalRJMCMC(double aSelFreq, size_t aSeed, size_t aBurnin, Config::ConfigFactory::sharedPtr_t aCfgFactory,
				   StatisticalModel::Model &aModel, RJMCMC::RJMoveManager::sharedPtr_t aRJMoveManager);
	~ProposalRJMCMC();

	void registerToSelector(Selector *selector);
	void registerToHandlerManager(Sampler::Sample &initSample, Handler::Manager *hm);

	State::BaseProposalState::sharedPtr_t saveState() const ;
	void loadState(State::BaseProposalState::sharedPtr_t state);

	RNG* getRNG();
	size_t getBurnIn() const;

	StatisticalModel::Model& getModel();

	Config::ConfigFactory::sharedPtr_t getCfgFactory();

	RJMCMC::RJMoveManager::sharedPtr_t getRJMoveManager();

	void setInitMoves(std::vector< Sampler::RJMCMC::RJMove::sharedPtr_t > aMoves);
	std::vector< Sampler::RJMCMC::RJMove::sharedPtr_t >& getInitMoves();

private:

	RNG *rng;
	size_t burnin;
	Config::ConfigFactory::sharedPtr_t cfgFactory;
	Utils::RandomSelectionCriterion rsc;
	StatisticalModel::Model &model;
	RJMCMC::RJMoveManager::sharedPtr_t moveManagerRJ;

	std::vector< Sampler::RJMCMC::RJMove::sharedPtr_t > initMoves;
};

} /* namespace Proposal */
} /* namespace Sampler */

#endif /* PROPOSALRJMCMC_H_ */
