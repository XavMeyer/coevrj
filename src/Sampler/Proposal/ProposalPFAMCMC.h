//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ProposalPFAMCMC
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#ifndef PROPOSALPFAMCMC_H_
#define PROPOSALPFAMCMC_H_

#include <boost/shared_ptr.hpp>
#include <iostream>

#include "Sampler/Proposal/State/BaseProposalState.h"
#include "BaseProposal.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "ParameterBlock/Blocks.h"
#include "Sampler/Strategy/AcceptanceStrategy/AcceptanceStrategy.h"
#include "Sampler/Strategy/BlockSelector/BlockSelector.h"
#include "Sampler/Strategy/IncStrategy.h"
#include "Sampler/Strategy/ModifierStrategy/ModifierStrategy.h"
#include "SelectionCriterion/RandomSelectionCriterion.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace Proposal {

class Selector;
namespace Handler { class Manager; }

using namespace ParameterBlock;

class ProposalPFAMCMC: public BaseProposal {
public:
	typedef boost::shared_ptr<ProposalPFAMCMC> sharedPtr_t;

public:
	ProposalPFAMCMC(Config::ConfigFactory::sharedPtr_t aCfgFactory, Blocks &aBlocks, StatisticalModel::Model &aModel);
	ProposalPFAMCMC(double aSelFrequency, Config::ConfigFactory::sharedPtr_t aCfgFactory,
					Blocks &aBlocks, StatisticalModel::Model &aModel);
	~ProposalPFAMCMC();

	void registerToSelector(Selector *selector);
	void registerToHandlerManager(Sampler::Sample &initSample, Handler::Manager *hm);

	State::BaseProposalState::sharedPtr_t saveState() const ;
	void loadState(State::BaseProposalState::sharedPtr_t state);

	Blocks& getBlocks();
	const Blocks& getBlocks() const;

	StatisticalModel::Model& getModel();

	Config::ConfigFactory::sharedPtr_t getCfgFactory();

	Strategies::BlockSelector::sharedPtr_t getBlockSelector();
	Strategies::AcceptanceStrategy::sharedPtr_t getAccepanceStrategy();
	Strategies::ModifierStrategy::sharedPtr_t getModifierStrategy();

	void setBlockSelector(Strategies::BlockSelector::sharedPtr_t aPtrBS);

private:

	Config::ConfigFactory::sharedPtr_t cfgFactory;
	Utils::RandomSelectionCriterion rsc;
	Blocks blocks;
	StatisticalModel::Model &model;

	Strategies::BlockSelector::sharedPtr_t ptrBS;
	Strategies::AcceptanceStrategy::sharedPtr_t ptrAS;
	Strategies::ModifierStrategy::sharedPtr_t ptrMS;

	void initBlocks(Blocks &aBlocks);
	void initStrategies();
};

} /* namespace Proposal */
} /* namespace Sampler */

#endif /* PROPOSALPFAMCMC_H_ */
