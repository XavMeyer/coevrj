//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseOutcome.h
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#ifndef BASEOUTCOME_H_
#define BASEOUTCOME_H_

#include <boost/shared_ptr.hpp>
#include <list>

#include "Sampler/Information/BaseInfo.h"
#include "Sampler/Samples/Sample.h"

namespace Sampler {
namespace Proposal {
namespace Outcome {

class BaseOutcome {
public:
	typedef boost::shared_ptr<BaseOutcome> sharedPtr_t;

public:
	BaseOutcome();
	virtual ~BaseOutcome();

	std::vector<Sampler::Sample>& getAcceptedSamples();
	std::list<Information::BaseInfo::sharedPtr_t>& getInformations();

	void setAcceptedSamples(const std::vector<Sampler::Sample>& aSamples);
	void addInformation(Information::BaseInfo::sharedPtr_t aInfoPtr);

	virtual void reset();

protected:

	std::list<Information::BaseInfo::sharedPtr_t> informations;
	std::vector<Sampler::Sample> acceptedSamples;

};

} /* namespace Outcome */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* BASEOUTCOME_H_ */
