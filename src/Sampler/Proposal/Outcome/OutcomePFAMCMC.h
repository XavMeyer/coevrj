//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OutcomePFAMCMC.h
 *
 * @date Dec 13, 2016
 * @author meyerx
 * @brief
 */
#ifndef OUTCOMEPFAMCMC_H_
#define OUTCOMEPFAMCMC_H_

#include <vector>

#include "BaseOutcome.h"

namespace Sampler {
namespace Proposal {
namespace Outcome {

class OutcomePFAMCMC: public BaseOutcome {
public:
	typedef boost::shared_ptr<OutcomePFAMCMC> sharedPtr_t;

public:
	OutcomePFAMCMC();
	~OutcomePFAMCMC();

	int getAcceptedId() const;
	int getNRejected() const;
	std::vector<size_t>& getBlockIds();
	std::vector<double>& getAlphaScore();

	void setAcceptedId(int id);
	void setNRejected(int aN);
	void setBlockIds(const std::vector<size_t>& aBlockIds);
	void setAlphaScore(const std::vector<double>& aAlphaScore);

	void reset();

protected:
	int acceptedId, nRejected;
	std::vector<size_t> blockIds;
	std::vector<double> alphaScore;
};

} /* namespace Outcome */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* OUTCOMEPFAMCMC_H_ */
