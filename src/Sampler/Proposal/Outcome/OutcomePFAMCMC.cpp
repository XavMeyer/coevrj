//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OutcomePFAMCMC.cpp
 *
 * @date Dec 13, 2016
 * @author meyerx
 * @brief
 */
#include "OutcomePFAMCMC.h"

#include "Sampler/Proposal/Outcome/BaseOutcome.h"

namespace Sampler {
namespace Proposal {
namespace Outcome {

OutcomePFAMCMC::OutcomePFAMCMC() : BaseOutcome() {
	acceptedId = nRejected = -1;
}

OutcomePFAMCMC::~OutcomePFAMCMC() {
}

int OutcomePFAMCMC::getAcceptedId() const {
	return acceptedId;
}

int OutcomePFAMCMC::getNRejected() const {
	return nRejected;
}

std::vector<size_t>& OutcomePFAMCMC::getBlockIds() {
	return blockIds;
}

std::vector<double>& OutcomePFAMCMC::getAlphaScore() {
	return alphaScore;
}

void OutcomePFAMCMC::setAcceptedId(int id) {
	acceptedId = id;
}

void OutcomePFAMCMC::setNRejected(int aN) {
	nRejected = aN;
}

void OutcomePFAMCMC::setBlockIds(const std::vector<size_t>& aBlockIds) {
	blockIds = aBlockIds;
}

void OutcomePFAMCMC::setAlphaScore(const std::vector<double>& aAlphaScore) {
	alphaScore = aAlphaScore;
}

void OutcomePFAMCMC::reset() {
	BaseOutcome::reset();

	acceptedId = nRejected = -1;
	blockIds.clear();
	alphaScore.clear();
}

} /* namespace Outcome */
} /* namespace Proposal */
} /* namespace Sampler */
