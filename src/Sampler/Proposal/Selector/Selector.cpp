//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Selector.cpp
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#include "Selector.h"

namespace Sampler {
namespace Proposal {

Selector::Selector(size_t aSeed) {
	myRng = RNG::createSitmo11RNG(aSeed);
}

Selector::~Selector() {
	delete myRng;
}

void Selector::doRegister(Utils::FixedSelectionCriterion* fsc, BaseProposal* aProposal) {
	periods.push_back(fsc->getPeriod());
	periodicProp.push_back(aProposal);
}

void Selector::doRegister(Utils::RandomSelectionCriterion* rsc, BaseProposal* aProposal) {
	frequencies.push_back(rsc->getFrequency());
	randomProp.push_back(aProposal);
}

BaseProposal* Selector::selectNext(size_t iteration) {

	// Add periodic proposals
	for(size_t iP=0; iP<periodicProp.size(); ++iP) {
		if(iteration > 0 && iteration % periods[iP] == 0) {
			waitingProp.push(periodicProp[iP]);
		}
	}

	// If there are pending periodic proposal, select them first
	if(!waitingProp.empty()) {
		BaseProposal* tmpPtr = waitingProp.front();
		waitingProp.pop();
		return tmpPtr;
	}

	// Weighted random selection
	int draw = myRng->drawFrom(frequencies);

	return randomProp[draw];
}

} /* namespace Proposal */
} /* namespace Sampler */
