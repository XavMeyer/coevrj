//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ProposalRJMCMC.cpp
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#include "ProposalRJMCMC.h"

#include <assert.h>

#include "Handler/Manager.h"
#include "Parallel/RNG/RNG.h"
#include "State/StateProposalRJMCMC.h"
#include "new"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace Proposal {

class Selector;

ProposalRJMCMC::ProposalRJMCMC(size_t aSeed, size_t aBurnin,
							   Config::ConfigFactory::sharedPtr_t aCfgFactory,
							   StatisticalModel::Model &aModel,
							   RJMCMC::RJMoveManager::sharedPtr_t aRJMoveManager) :
									   rng(RNG::createSitmo11RNG(aSeed)), burnin(aBurnin),
									   cfgFactory(aCfgFactory), model(aModel), moveManagerRJ(aRJMoveManager) {
}

ProposalRJMCMC::ProposalRJMCMC(double aSelectionFreq, size_t aSeed, size_t aBurnin,
							   Config::ConfigFactory::sharedPtr_t aCfgFactory,
		 	 	 	 	 	   StatisticalModel::Model &aModel,
		 	 	 	 	 	   RJMCMC::RJMoveManager::sharedPtr_t aRJMoveManager) :
		 	 	 	 	 			   rng(RNG::createSitmo11RNG(aSeed)), burnin(aBurnin),
		 	 	 	 	 			   cfgFactory(aCfgFactory), rsc(aSelectionFreq), model(aModel),
		 	 	 	 	 			   moveManagerRJ(aRJMoveManager) {
}

ProposalRJMCMC::~ProposalRJMCMC() {
	delete rng;
}


void ProposalRJMCMC::registerToSelector(Selector *selector) {
	rsc.registerToSelector(selector, this);
}

void ProposalRJMCMC::registerToHandlerManager(Sampler::Sample &initSample, Handler::Manager *hm) {
	hm->doRegister(initSample, this);
}

State::BaseProposalState::sharedPtr_t ProposalRJMCMC::saveState() const {
	State::StateProposalRJMCMC::sharedPtr_t ptrState(new State::StateProposalRJMCMC());
	rng->saveState(ptrState->stateRNG);
	moveManagerRJ->getRng()->saveState(ptrState->stateRJMoveManagerRNG);
	return ptrState;
}
void ProposalRJMCMC::loadState(State::BaseProposalState::sharedPtr_t state) {
	State::StateProposalRJMCMC* ptrState = dynamic_cast<State::StateProposalRJMCMC*>(state.get());
	assert(ptrState != NULL);
	rng->loadState(ptrState->stateRNG);
	moveManagerRJ->getRng()->loadState(ptrState->stateRJMoveManagerRNG);

}

RNG* ProposalRJMCMC::getRNG() {
	return rng;
}

size_t ProposalRJMCMC::getBurnIn() const {
	return burnin;
}


StatisticalModel::Model& ProposalRJMCMC::getModel() {
	return model;
}

Config::ConfigFactory::sharedPtr_t ProposalRJMCMC::getCfgFactory() {
	return cfgFactory;
}

RJMCMC::RJMoveManager::sharedPtr_t ProposalRJMCMC::getRJMoveManager() {
	return moveManagerRJ;
}

void ProposalRJMCMC::setInitMoves(std::vector< Sampler::RJMCMC::RJMove::sharedPtr_t > aMoves) {
	initMoves = aMoves;
}

std::vector< Sampler::RJMCMC::RJMove::sharedPtr_t >& ProposalRJMCMC::getInitMoves() {
	return initMoves;
}

} /* namespace Proposal */
} /* namespace Sampler */
