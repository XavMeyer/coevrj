//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Manager.cpp
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#include "Manager.h"

#include <assert.h>

#include "Sampler/Proposal/Handler/BaseHandler.h"
#include "Sampler/Proposal/Handler/State/BaseHandlerState.h"
#include "HandlerMC3.h"
#include "HandlerPFAMCMC.h"
#include "HandlerRJMCMC.h"

namespace Sampler { class Sample; }
namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }
namespace Sampler { namespace Proposal { class ProposalGPMH; } }
namespace Sampler { namespace Proposal { class ProposalGradient; } }
namespace Sampler { namespace Proposal { class ProposalMC3; } }
namespace Sampler { namespace Proposal { class ProposalPFAMCMC; } }
namespace Sampler { namespace Proposal { class ProposalRJMCMC; } }
namespace Sampler { namespace State { class SamplerState; } }

namespace Sampler {
namespace Proposal {
namespace Handler {

Manager::Manager(Sampler::State::SamplerState &aSamplerState,
				 Sampler::PStatsAccessor::AccessorsManager &aAccessorsManager) :
						 samplerState(aSamplerState), accessorsManager(aAccessorsManager) {
}

Manager::~Manager() {
}

void Manager::doRegister(Sampler::Sample &initSample, ProposalPFAMCMC* aProposal) {
	HandlerPFAMCMC::sharedPtr_t newHandler( new HandlerPFAMCMC(initSample, aProposal, samplerState, accessorsManager));
	std::pair<BaseProposal*, BaseHandler::sharedPtr_t> aPair = std::make_pair(aProposal, newHandler);
	mapHandler.insert(aPair);

	blockDispatcher.registerBlocks(aProposal);
}

void Manager::doRegister(Sampler::Sample &initSample, ProposalMC3* aProposal) {
	HandlerMC3::sharedPtr_t newHandler(new HandlerMC3(aProposal, samplerState));
	std::pair<BaseProposal*, BaseHandler::sharedPtr_t> aPair = std::make_pair(aProposal, newHandler);
	mapHandler.insert(aPair);
}

void Manager::doRegister(Sampler::Sample &initSample, ProposalRJMCMC* aProposal) {
	HandlerRJMCMC::sharedPtr_t newHandler(new HandlerRJMCMC(initSample, aProposal, samplerState, blockDispatcher, accessorsManager));
	std::pair<BaseProposal*, BaseHandler::sharedPtr_t> aPair = std::make_pair(aProposal, newHandler);
	mapHandler.insert(aPair);
}

BaseHandler::sharedPtr_t Manager::getHandler(BaseProposal::sharedPtr_t aProp) {
	return getHandler(aProp.get());
}

BaseHandler::sharedPtr_t Manager::getHandler(BaseProposal* aProp) {

	itMapHandler_t it = mapHandler.find(aProp);
	assert(it != mapHandler.end());
	return it->second;
}

State::BaseHandlerState::sharedPtr_t Manager::getHandlerState(BaseProposal::sharedPtr_t aProp) const {
	return getHandlerState(aProp.get());
}

State::BaseHandlerState::sharedPtr_t Manager::getHandlerState(BaseProposal* aProp) const {
	cstItMapHandler_t it = mapHandler.find(aProp);
	assert(it != mapHandler.end());
	return it->second->saveState();
}

void Manager::setHandlerState(BaseProposal::sharedPtr_t aProp, State::BaseHandlerState::sharedPtr_t state) {
	setHandlerState(aProp.get(), state);
}

void Manager::setHandlerState(BaseProposal* aProp, State::BaseHandlerState::sharedPtr_t state) {
	BaseHandler::sharedPtr_t ptrHandler = getHandler(aProp);
	ptrHandler->loadState(state);
}

const BlockDispatcher& Manager::getBlockDispatcher() const {
	return blockDispatcher;
}

} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */
