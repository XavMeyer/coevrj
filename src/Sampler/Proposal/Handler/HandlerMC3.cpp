//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HandlerMC3.cpp
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#include "HandlerMC3.h"

#include <assert.h>

#include "Sampler/Proposal/Handler/BaseHandler.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Rho/BatchEvoRU.h"
#include "Sampler/Information/IncInformations.h"
#include "Sampler/SamplerState.h"
#include "Sampler/Samples/Sample.h"
#include "Sampler/Samples/SampleVector.h"
#include "State/StateHandlerMC3.h"
#include "mpi.h"

namespace Sampler {
namespace Proposal {
namespace Handler {

const double HandlerMC3::TARGET_ALPHA = 0.20; // 24% should be the optimum -> we want bolder move however to traverse valley

HandlerMC3::HandlerMC3(ProposalMC3 *aProposal, Sampler::State::SamplerState &aSamplerState) :
		BaseHandler(aSamplerState), mcmcMgr(Parallel::mcmcMgr()), proposal(aProposal),
		outcome(new Outcome::OutcomeMC3()) {

	counter = convIterMC3 = 0;
	nStepMC3 = nAccStepMC3 = 0;
	timeMC3 = 0.;

	nextWrite = Information::outputManager().getSamplerOutputFrequency();

	if(mcmcMgr.isActiveMC3()) {

		// Define temperature
		double invTemp = 1.0;
		samplerState.setInverseTemperature(invTemp);

		// Create adaptive MC3
		evoMC3 = new ParameterBlock::BatchEvoRU(TARGET_ALPHA, 1,  proposal->getConfig()->MC3,
												proposal->getConfig()->CONV_CHECKER);

		updateTemperatures();
	} else {
		if(Parallel::mpiMgr().isMainProcessor()) {
			std::cerr << "[ERROR] in constructor 'HandlerMC3::HandlerMC3(..)'." << std::endl;
			std::cerr << "You are trying to apply MC3 with only one heated chain : try with 'nChain>1'." << std::endl;
		}
		assert(mcmcMgr.isActiveMC3());
	}
}

HandlerMC3::~HandlerMC3() {
	if(mcmcMgr.isManagerMC3()) {
		delete evoMC3;
	}
}


State::BaseHandlerState::sharedPtr_t HandlerMC3::saveState() const {
	State::StateHandlerMC3::sharedPtr_t ptrState(new State::StateHandlerMC3());

	ptrState->counter = counter;
	ptrState->convIterMC3 = convIterMC3;
	ptrState->nStepMC3 = nStepMC3;
	ptrState->nAccStepMC3 = nAccStepMC3;
	ptrState->nextWrite = nextWrite;
	ptrState->timeMC3 = timeMC3;

	evoMC3->saveState(ptrState->evoMC3State);

	return ptrState;
}

void HandlerMC3::loadState(State::BaseHandlerState::sharedPtr_t state) {
	State::StateHandlerMC3* ptrState = dynamic_cast<State::StateHandlerMC3*>(state.get());

	counter = ptrState->counter;
	convIterMC3 = ptrState->convIterMC3;
	nStepMC3 = ptrState->nStepMC3;
	nAccStepMC3 = ptrState->nAccStepMC3;
	nextWrite = ptrState->nextWrite;
	timeMC3 = ptrState->timeMC3;

	evoMC3->loadState(ptrState->evoMC3State);
}

void HandlerMC3::doPreProcessing(Sampler::Sample &curSample) {
	outcome->reset();
}

void HandlerMC3::applyProposal(Sampler::Sample &curSample) {
	doMC3(curSample);
}

void HandlerMC3::doPostProcessing(Sampler::Sample &curSample) {
	updateTemperatures();
	writeOutputMC3(curSample);
}

void HandlerMC3::updateSamples(Sampler::Sample &curSample, Sampler::SampleVector &samples) {
	if(mcmcMgr.isOutputManager() && outcome->chainIsInvolved()) {
		if(!outcome->getAcceptedSamples().empty()) {
			samples.push_back(outcome->getAcceptedSamples().front());
			samples.back().setChanged(true);
		} else {
			samples.push_back(curSample);
			samples.back().setChanged(false);
		}
	}

	if(!outcome->getAcceptedSamples().empty() && outcome->chainIsInvolved()) {
		curSample = outcome->getAcceptedSamples().front();
		curSample.setChanged(true);
	}
}

Outcome::BaseOutcome::sharedPtr_t HandlerMC3::getOutcome() {
	return outcome;
}

std::string HandlerMC3::reportStatus(std::string &prefix, size_t iT, double timeTotal) const {
	std::stringstream sstr;
	sstr << prefix << "[MC3] Time MC3 :\t" << timeMC3 << "\t" << timeMC3/(double)iT << "\t" << 100*timeMC3/timeTotal << endl;
	sstr << prefix << "[MC3] >> Acceptance ratio MC3 :\t " << (double)nAccStepMC3/(double)nStepMC3 << "\t(" << nAccStepMC3  << " / " << nStepMC3 << ")" << endl;
	if(mcmcMgr.getMyChainMC3() > 0) {
		sstr << prefix << "[MC3] >> Temperature for MC3 :\t" << samplerState.getInverseTemperature() << " ( Rho= " << evoMC3->getRho() << ")" << endl;
		sstr << prefix << "[MC3] << MC3 convergence at iteration :\t" << convIterMC3 << std::endl;
	}
	return sstr.str();
}

void HandlerMC3::defineExchange(int &coldChain, int &hotChain, double &unifDbl) {
	const int nChain = mcmcMgr.getNChainMC3();
	// Who will exchange ?
	coldChain = proposal->getRNG()->genUniformInt(0, nChain-2);
	hotChain = coldChain + 1;

	// Generate random for test
	unifDbl = proposal->getRNG()->genUniformDbl();
}

double HandlerMC3::defineExchangeRatio(const int myChain, const int otherChain, ::Utils::Serialize::buffer_t &bufferRecv, Sample &curSample, Sample &newSample) {

	bool dummy = false;
	double myInvTemp = samplerState.getInverseTemperature();
	double hisInvTemp = 0.0;
	::Utils::Serialize::buffer_t bufferSend;

	packInfoMC3(bufferSend, dummy, myInvTemp, curSample);
	mcmcMgr.exchangeSample(bufferSend, bufferRecv, otherChain);
	unPackInfoMC3(bufferRecv, dummy, hisInvTemp, newSample);

	double ratio = 0.;
	if(proposal->getModel().getLikelihood()->isLog()) {
		ratio = myInvTemp*newSample.posterior + hisInvTemp*curSample.posterior;
		ratio -= hisInvTemp*newSample.posterior + myInvTemp*curSample.posterior;
		ratio = std::min(1., exp(ratio));
	} else {
		ratio = (pow(newSample.posterior, myInvTemp) * pow(curSample.posterior, hisInvTemp));
		ratio /= (pow(newSample.posterior, hisInvTemp) * pow(curSample.posterior, myInvTemp));
		ratio = std::min(1., ratio);
	}

	if(Sampler::Information::outputManager().check(Sampler::Information::HIGH_VERB)) {
		std::stringstream ss;
		ss << "[" << Parallel::mpiMgr().getRank() << "] it = " << samplerState.getIT() << " | " << "myChain = ";
		ss << mcmcMgr.getMyChainMC3() << " | nChain = " << mcmcMgr.getNChainMC3();
		ss << " | temperatures (my, his) = (" << myInvTemp << ", " << hisInvTemp << ") | ratios = " << ratio;
		outcome->getInformations().push_back(Information::MC3Info::createTextInfo(ss));
	}

	// FIXME There was here an additional convergence criterion on the whole chain
	// Adaptive (See "Adaptive Parallel Tempering Algorithm" Miasojedow et al. 2012)
	if(myChain > otherChain) { 											// If I'm the hot chain
		if(!evoMC3->hasConverged()) { 									// - If not converged then (wait for all adaptive process)
			evoMC3->addVal(ratio); 										// -- Adaptive learning of the temperature
			if(evoMC3->getTmpT() == proposal->getConfig()->MC3->MEAN_WINDOW-1) {
				if(Sampler::Information::outputManager().check(Sampler::Information::HIGH_VERB)) {
					std::stringstream ss;
					ss << "[" << Parallel::mpiMgr().getRank() << "] it = " << samplerState.getIT() << " | " << "myChain = ";
					ss << mcmcMgr.getMyChainMC3() << " | nChain = " << mcmcMgr.getNChainMC3();
					ss << " | " << evoMC3->toString() << "New temp = " <<  1./(exp(evoMC3->getRho())+1./hisInvTemp) << " | factor = " << 1./(exp(evoMC3->getRho()));
					outcome->getInformations().push_back(Information::MC3Info::createTextInfo(ss));
				}
			}
		} else if (convIterMC3 == 0) {		// - Else if convergence for the first time
			convIterMC3 = samplerState.getIT();							// -- Keep track of it
			if(Sampler::Information::outputManager().check(Sampler::Information::HIGH_VERB)) {
				std::stringstream ss;
				ss << "[" << Parallel::mpiMgr().getRank() << "] it = " << samplerState.getIT() << " | " << "myChain = ";
				ss << mcmcMgr.getMyChainMC3() << " | nChain = " << mcmcMgr.getNChainMC3();
				ss << " | Adaptive has converged at temperature (" << samplerState.getInverseTemperature() << ") ";
				outcome->getInformations().push_back(Information::MC3Info::createTextInfo(ss));
			}
		}
		// Update my temperature since Rho or other chain temperature might have changed
		double newInvTemp = 1./(exp(evoMC3->getRho())+1./hisInvTemp);
		samplerState.setInverseTemperature(newInvTemp);
	}

	return ratio;
}

void HandlerMC3::acceptExchangeManager(Sample &curSample, Sample &newSample, const int otherChain) {
	newSample.setChanged(true);
	outcome->getAcceptedSamples().push_back(newSample);

	// Call likelihood specialised exchange
	::Utils::Serialize::buffer_t bufferIn, bufferOut;
	proposal->getModel().getLikelihood()->saveInternalState(curSample, bufferIn);

	mcmcMgr.exchangeBufferMC3(bufferIn, bufferOut, otherChain);
	proposal->getModel().getLikelihood()->loadInternalState(curSample, bufferOut);
	// Share the information with our proposal workers
	mcmcMgr.shareWithWorkersBufferMC3(bufferOut);

	Information::RemoteInfo::sharedPtr_t ptrRInfo(new Information::RemoteInfo(true));
	outcome->addInformation(ptrRInfo);
}

void HandlerMC3::acceptExchangeWorker(Sample &curSample, Sample &newSample) {

	newSample.setChanged(true);
	outcome->getAcceptedSamples().push_back(newSample);

	/*curSample = newSample;
	curSample.setChanged(true);*/

	// Share the information with our proposal workers
	::Utils::Serialize::buffer_t bufferOut;
	mcmcMgr.receiveFromManagerBufferMC3(bufferOut);
	proposal->getModel().getLikelihood()->loadInternalState(curSample, bufferOut);

	Information::RemoteInfo::sharedPtr_t ptrRInfo(new Information::RemoteInfo(true));
	outcome->addInformation(ptrRInfo);
}


void HandlerMC3::updateTemperatures() {

	const int nChain = mcmcMgr.getNChainMC3();
	const int myChain = mcmcMgr.getMyChainMC3();

	double myChainTemp = 0;
	if(mcmcMgr.checkRoleProposal(mcmcMgr.Manager) || mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {

		if(myChain > 0) { // If I have a left neighbor : (myChain -1) exists
			// I wait for his temperature
			double hisInvT = mcmcMgr.recvTemperature(myChain-1);

			// I update my temperature
			double newInvTemp = 1./(exp(evoMC3->getRho())+1./hisInvT);
			samplerState.setInverseTemperature(newInvTemp);
		}

		myChainTemp = samplerState.getInverseTemperature();

		if(myChain+1 < nChain) { // if I have a right neighbor : (myChain + 1) exists
			// I send him my info
			mcmcMgr.sendTemperature(samplerState.getInverseTemperature(), myChain+1);
		}
	}

	// I update my proposals temp.
	mcmcMgr.bcastProposal(1, &myChainTemp, mcmcMgr.getManagerProposal());
	samplerState.setInverseTemperature(myChainTemp);
}


void HandlerMC3::doMC3(Sampler::Sample &curSample) {

	// Get my chain
	const int myChain = mcmcMgr.getMyChainMC3();

	// Who will exchange ?
	int coldChain, hotChain;
	double unifRand;
	defineExchange(coldChain, hotChain, unifRand);

	// If we are not part of this exchange we continue MCMC
	// Signal all chain are involved even if not selected
	outcome->setChainInvolved(true);
	if(myChain != coldChain && myChain != hotChain) return;
	nStepMC3++;
	double startMC3 = MPI_Wtime();

	// We add one character to signal if the sample is accepted
	::Utils::Serialize::buffer_t bufferRecv;
	if(mcmcMgr.checkRoleProposal(mcmcMgr.Manager) || mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) { // Managers do that
		// Swap on chain 2 the order to make operations "symmetric"
		int otherChain = myChain == coldChain ? hotChain : coldChain;

		Sample newSample;
		double ratio = defineExchangeRatio(myChain, otherChain, bufferRecv, curSample, newSample);

		if(unifRand < ratio){
			nAccStepMC3++;
			if(Sampler::Information::outputManager().check(Sampler::Information::HIGH_VERB)) {
				std::stringstream ss;
				ss << "Exchanging : " << myChain << " (" << curSample.posterior << ") and " << otherChain << " (" << newSample.posterior << ") with ratio=" << ratio << " > " << unifRand;
				outcome->getInformations().push_back(Information::MC3Info::createTextInfo(ss));
			}

			//dataRecv[serializedSampleSizeMC3] = 'Y';
			packPartialInfoMC3(bufferRecv, true, samplerState.getInverseTemperature());
			// Signal outcome to other member of parallel proposal
			mcmcMgr.bcastSampleProposal(bufferRecv, mcmcMgr.getManagerProposal());
			// Do stuff related with accepted MC3
			acceptExchangeManager(curSample, newSample, otherChain);
		} else {
			if(Sampler::Information::outputManager().check(Sampler::Information::HIGH_VERB)) {
				std::stringstream ss;
				ss << "Rejected : " << myChain << " (" << curSample.posterior << ") and " << otherChain << " (" << newSample.posterior << ") with ratio="  << ratio << " > " << unifRand;
				outcome->getInformations().push_back(Information::MC3Info::createTextInfo(ss));
			}
			//dataRecv[serializedSampleSizeMC3] = 'N';
			packPartialInfoMC3(bufferRecv, false, samplerState.getInverseTemperature());
			// Signal outcome to other member of parallel proposal
			mcmcMgr.bcastSampleProposal(bufferRecv, mcmcMgr.getManagerProposal());
		}
	} else if(mcmcMgr.checkRoleProposal(mcmcMgr.Worker)) { // Slaves
		bool accepted = false;
		double newInvTemp = 0.;
		Sample newSample;
		// Get message
		mcmcMgr.bcastSampleProposal(bufferRecv, mcmcMgr.getManagerProposal());
		unPackInfoMC3(bufferRecv, accepted, newInvTemp, newSample);
		// Set new temperature
		samplerState.setInverseTemperature(newInvTemp);
		// Do acceptance / rejection
		if(accepted) { // Sample has been exchanged
			acceptExchangeWorker(curSample, newSample);
		}
	}

	double endMC3 = MPI_Wtime();
	timeMC3 += endMC3 - startMC3;
}

void HandlerMC3::packInfoMC3(::Utils::Serialize::buffer_t &bufferSend, const bool accepted, const double invTemp, const Sample &sample) {
	// Prepare bufferSend and copy temperature and accepted boolean
	bufferSend.clear();
	bufferSend.resize(1+sizeof(double));

	// Accepted boolean
	bufferSend[0] = accepted ? 'Y' : 'N';

	// Temperature
	double *tmp = reinterpret_cast<double *>(&bufferSend[1]);
	*tmp = invTemp;

	// Serialize and concatenate sample
	::Utils::Serialize::buffer_t serializedSample = sample.save();
	bufferSend.insert(bufferSend.end(), serializedSample.begin(), serializedSample.end());
}

void HandlerMC3::unPackInfoMC3(const ::Utils::Serialize::buffer_t &bufferRecv, bool &accepted, double &invTemp, Sample &sample){
	// Accepted boolean
	accepted = bufferRecv[0] == 'Y' ? true : false;

	// Temperature
	const double *tmp = reinterpret_cast<const double *>(&bufferRecv[1]);
	invTemp = *tmp;

	// Get Sample
	::Utils::Serialize::buffer_t serializedSample(bufferRecv.data()+(1+sizeof(double)), bufferRecv.data()+bufferRecv.size());
	sample.load(serializedSample);
}

void HandlerMC3::packPartialInfoMC3(::Utils::Serialize::buffer_t &bufferSend, const bool accepted, const double invTemp) {
	// Check size and copy temperature and accepted boolean
	assert(bufferSend.size() >= 1+sizeof(double));

	// Accepted boolean
	bufferSend[0] = accepted ? 'Y' : 'N';

	// Temperature
	double *tmp = reinterpret_cast<double *>(&bufferSend[1]);
	*tmp = invTemp;
}

void HandlerMC3::unPackPartialInfoMC3(const ::Utils::Serialize::buffer_t &bufferRecv, bool &accepted, double &invTemp) {
	// Accepted boolean
	accepted = bufferRecv[0] == 'Y' ? true : false;

	// Temperature
	const double *tmp = reinterpret_cast<const double *>(&bufferRecv[1]);
	invTemp = *tmp;
}

void HandlerMC3::writeOutputMC3(const Sample &curSample) {
	if(samplerState.getIT() > nextWrite &&  Sampler::Information::outputManager().check(Sampler::Information::MEDIUM_VERB)) {
		std::stringstream ss;
		ss << "Iteration : " << samplerState.getIT() << " -- posterior = " << curSample.posterior;
		outcome->getInformations().push_back(Information::MC3Info::createTextInfo(ss));
		nextWrite += Information::outputManager().getSamplerOutputFrequency();
	}
}

} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */
