//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseHandler.h
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#ifndef BASEHANDLER_H_
#define BASEHANDLER_H_

#include <boost/shared_ptr.hpp>
#include <vector>

#include "../Outcome/BaseOutcome.h"
#include "State/BaseHandlerState.h"

namespace Sampler { class Sample; }
namespace Sampler { class SampleVector; }
namespace Sampler { namespace State { class SamplerState; } }

namespace Sampler {
namespace Proposal {
namespace Handler {

class BaseHandler {
public:
	typedef boost::shared_ptr<BaseHandler> sharedPtr_t;

public:
	BaseHandler(Sampler::State::SamplerState &aSamplerState);
	virtual ~BaseHandler();

	// Function to be called by the sampler
	virtual void doPreProcessing(Sampler::Sample &curSample) = 0;
	virtual void applyProposal(Sampler::Sample &curSample) = 0;
	virtual void doPostProcessing(Sampler::Sample &curSample) = 0;
	virtual void updateSamples(Sampler::Sample &curSample, Sampler::SampleVector &samples) = 0;

	// Must be override for proposals requiring checkpointing
	virtual State::BaseHandlerState::sharedPtr_t saveState() const = 0;
	virtual void loadState(State::BaseHandlerState::sharedPtr_t state) = 0;

	virtual Outcome::BaseOutcome::sharedPtr_t getOutcome() = 0;

	virtual std::string reportStatus(std::string &prefix, size_t iT, double timeTotal) const = 0;

protected:
	Sampler::State::SamplerState &samplerState;

};

} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* BASEHANDLER_H_ */
