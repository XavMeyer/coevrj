//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file StateHandlerRJMCMC.cpp
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#include "StateHandlerRJMCMC.h"

#include <boost/serialization/export.hpp>
#include <boost/serialization/nvp.hpp>

BOOST_CLASS_EXPORT_GUID(Sampler::Proposal::Handler::State::StateHandlerRJMCMC, "Sampler_Proposal_Handler_State_RJMCMC")

namespace Sampler {
namespace Proposal {
namespace Handler {
namespace State {

StateHandlerRJMCMC::StateHandlerRJMCMC() {
	counter = 0;
	sTimeRJMCMC = eTimeRJMCMC = 0.;
	timeApplyMove = timeComputeMove = 0.;
	timeAcceptRejectMove = timeRJMCMC = 0.;
}

StateHandlerRJMCMC::~StateHandlerRJMCMC() {
}


template<class Archive>
void StateHandlerRJMCMC::save(Archive & ar, const unsigned int version) const {
	ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseHandlerState);

	ar << BOOST_SERIALIZATION_NVP( counter );

	ar << BOOST_SERIALIZATION_NVP( sTimeRJMCMC );
	ar << BOOST_SERIALIZATION_NVP( eTimeRJMCMC );
	ar << BOOST_SERIALIZATION_NVP( timeApplyMove );
	ar << BOOST_SERIALIZATION_NVP( timeComputeMove );
	ar << BOOST_SERIALIZATION_NVP( timeAcceptRejectMove );
	ar << BOOST_SERIALIZATION_NVP( timeRJMCMC );
}

template<class Archive>
void StateHandlerRJMCMC::load(Archive & ar, const unsigned int version) {
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseHandlerState);

	ar >> BOOST_SERIALIZATION_NVP( counter );

	ar >> BOOST_SERIALIZATION_NVP( sTimeRJMCMC );
	ar >> BOOST_SERIALIZATION_NVP( eTimeRJMCMC );
	ar >> BOOST_SERIALIZATION_NVP( timeApplyMove );
	ar >> BOOST_SERIALIZATION_NVP( timeComputeMove );
	ar >> BOOST_SERIALIZATION_NVP( timeAcceptRejectMove );
	ar >> BOOST_SERIALIZATION_NVP( timeRJMCMC );
}

template void StateHandlerRJMCMC::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void StateHandlerRJMCMC::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void StateHandlerRJMCMC::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void StateHandlerRJMCMC::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);


} /* namespace State */
} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */
