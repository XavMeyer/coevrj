//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file StateHandlerMC3.h
 *
 * @date Dec 12, 2016
 * @author meyerx
 * @brief
 */
#ifndef STATEHANDLERMC3_H_
#define STATEHANDLERMC3_H_

#include <stddef.h>

#include "BaseHandlerState.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Rho/EvoRUState.h"

namespace Sampler {
namespace Proposal {
namespace Handler {
	class HandlerMC3;
}
}
}

namespace Sampler {
namespace Proposal {
namespace Handler {
namespace State {

class StateHandlerMC3: public BaseHandlerState {
public:
	typedef boost::shared_ptr<StateHandlerMC3> sharedPtr_t;

public:
	StateHandlerMC3();
	~StateHandlerMC3();

private:

	size_t counter, convIterMC3, nStepMC3, nAccStepMC3, nextWrite;
	double timeMC3;
	ParameterBlock::State::EvoRUState evoMC3State;

	// Boost serialization
	friend class Sampler::Proposal::Handler::HandlerMC3;
	friend class boost::serialization::access;

    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace State */
} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* STATEHANDLERMC3_H_ */
