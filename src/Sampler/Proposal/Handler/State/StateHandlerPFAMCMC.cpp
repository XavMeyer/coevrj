//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file StateHandlerPFAMCMC.cpp
 *
 * @date Dec 12, 2016
 * @author meyerx
 * @brief
 */
#include "StateHandlerPFAMCMC.h"

#include <boost/serialization/export.hpp>
#include <boost/serialization/nvp.hpp>

BOOST_CLASS_EXPORT_GUID(Sampler::Proposal::Handler::State::StateHandlerPFAMCMC, "Sampler_Proposal_Handler_State_PFAMCMC")

namespace Sampler {
namespace Proposal {
namespace Handler {
namespace State {

StateHandlerPFAMCMC::StateHandlerPFAMCMC() {

	counter = 0;
	timeB1 = timeB2 = timeO = timePFAMCMC = 0.;

}

StateHandlerPFAMCMC::~StateHandlerPFAMCMC() {
}

template<class Archive>
void StateHandlerPFAMCMC::save(Archive & ar, const unsigned int version) const {
	ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseHandlerState);

	ar << BOOST_SERIALIZATION_NVP( counter );

	ar << BOOST_SERIALIZATION_NVP( timeB1 );
	ar << BOOST_SERIALIZATION_NVP( timeB2 );
	ar << BOOST_SERIALIZATION_NVP( timeO );
	ar << BOOST_SERIALIZATION_NVP( timePFAMCMC );

	// Prefetching
	ar << BOOST_SERIALIZATION_NVP( pfState );

	// ADAPTIVE
	ar << BOOST_SERIALIZATION_NVP( apState );


}

template<class Archive>
void StateHandlerPFAMCMC::load(Archive & ar, const unsigned int version) {
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseHandlerState);

	ar >> BOOST_SERIALIZATION_NVP( counter );

	ar >> BOOST_SERIALIZATION_NVP( timeB1 );
	ar >> BOOST_SERIALIZATION_NVP( timeB2 );
	ar >> BOOST_SERIALIZATION_NVP( timeO );
	ar >> BOOST_SERIALIZATION_NVP( timePFAMCMC );

	// Prefetching
	ar >> BOOST_SERIALIZATION_NVP( pfState );

	// ADAPTIVE
	ar >> BOOST_SERIALIZATION_NVP( apState );


}

template void StateHandlerPFAMCMC::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void StateHandlerPFAMCMC::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void StateHandlerPFAMCMC::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void StateHandlerPFAMCMC::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace State */
} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */
