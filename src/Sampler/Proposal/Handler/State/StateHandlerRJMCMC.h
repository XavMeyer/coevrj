//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file StateHandlerRJMCMC.h
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#ifndef STATEHANDLERRJMCMC_H_
#define STATEHANDLERRJMCMC_H_

#include <stddef.h>

#include "BaseHandlerState.h"

namespace Sampler {
namespace Proposal {
namespace Handler {
	class HandlerRJMCMC;
}
}
}

namespace Sampler {
namespace Proposal {
namespace Handler {
namespace State {

class StateHandlerRJMCMC: public BaseHandlerState {
public:
	typedef boost::shared_ptr<StateHandlerRJMCMC> sharedPtr_t;

public:
	StateHandlerRJMCMC();
	~StateHandlerRJMCMC();

private:
	size_t counter;
	double sTimeRJMCMC, eTimeRJMCMC;
	double timeApplyMove, timeComputeMove, timeAcceptRejectMove, timeRJMCMC;

	// Boost serialization
	friend class Sampler::Proposal::Handler::HandlerRJMCMC;
	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace State */
} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* STATEHANDLERRJMCMC_H_ */
