//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HandlerRJMCMC.cpp
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#include "HandlerRJMCMC.h"

#include <assert.h>
#include <typeinfo>


#include "Sampler/SamplerState.h"
#include "Sampler/Samples/SampleVector.h"
#include "Sampler/Proposal/Handler/BaseHandler.h"
#include "Parallel/Manager/MCMCManager.h"
#include "Parallel/RNG/RNG.h"
#include "Sampler/Proposal/ProposalRJMCMC.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"
#include "Sampler/ReversibleJump/Proposals/RJMoveProposal.h"
#include "Sampler/ReversibleJump/RJMoveManager.h"
#include "State/StateHandlerRJMCMC.h"
#include "mpi.h"


namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }
namespace Sampler { namespace Proposal { class BlockDispatcher; } }
namespace StatisticalModel { class Model; }

// Define macro // to avoid time measures
#define TIME_MEASURE_RJMCMC

namespace Sampler {
namespace Proposal {
namespace Handler {

HandlerRJMCMC::HandlerRJMCMC(Sampler::Sample &initSample, ProposalRJMCMC* aProposal,
						 	 Sampler::State::SamplerState &aSamplerState,
						 	 BlockDispatcher &aBlockDispatcher,
						 	 Sampler::PStatsAccessor::AccessorsManager &accessorsManager) :
		BaseHandler(aSamplerState), mcmcMgr(Parallel::mcmcMgr()), proposal(aProposal),
		blockDispatcher(aBlockDispatcher), outcome(new Outcome::OutcomeRJMCMC()) {

	counter = 0;
	sTimeRJMCMC = eTimeRJMCMC = 0.;
	timeApplyMove = timeComputeMove = timeAcceptRejectMove = timeRJMCMC = 0.;

	initialize(initSample);
}

HandlerRJMCMC::~HandlerRJMCMC() {
}

void HandlerRJMCMC::initialize(Sampler::Sample &initSample) {

	std::vector< Sampler::RJMCMC::RJMove::sharedPtr_t > &moves = proposal->getInitMoves();
	StatisticalModel::Model &model = proposal->getModel();

	for(size_t iM=0; iM<moves.size(); ++iM) {

		// Update with the values of initSample
		moves[iM]->updateSamplesForInit(initSample);

		// Apply the move
		moves[iM]->applyMove(subSpaceRessources, model, blockDispatcher);

		std::vector<Sampler::Sample> proposedSamples;
		proposedSamples.push_back(moves[iM]->getProposedSample());

		// Update the likelihood
		if(model.getLikelihood()->isUpdatable()){
			model.processSample(moves[iM]->getUpdatedParametersID(), proposedSamples);
		} else {
			model.processSample(proposedSamples);
		}

		// Signal accepted and reset initSample
		moves[iM]->signalAccepted(subSpaceRessources, model, blockDispatcher);
		initSample = proposedSamples.front();
	}

	// Remove pending samples to be init'd
	moves.clear();
}

void HandlerRJMCMC::updateStateFromRemote(Sampler::Sample &curSample, std::vector< Sampler::RJMCMC::RJMove::sharedPtr_t > &moves) {

	StatisticalModel::Model &model = proposal->getModel();

	/// Apply moves
	std::set<size_t> updatedParametersID;
	for(size_t iM=0; iM<moves.size(); ++iM) {

		// Apply the move
		moves[iM]->applyMove(subSpaceRessources, model, blockDispatcher);
		updatedParametersID.insert(moves[iM]->getUpdatedParametersID().begin(), moves[iM]->getUpdatedParametersID().end());

		// Signal accepted and reset initSample
		moves[iM]->signalAccepted(subSpaceRessources, model, blockDispatcher);
	}

	/// Update MSS pInd
	updatePIndMSS(curSample);
	curSample.setChanged(true);
	curSample.setEvaluated(false);

	std::vector<Sampler::Sample> proposedSamples;
	proposedSamples.push_back(curSample);

	/// Update likelihood
	std::vector<size_t> vecUpdParams(updatedParametersID.begin(), updatedParametersID.end());
	if(model.getLikelihood()->isUpdatable()){
		model.processSample(vecUpdParams, proposedSamples);
	} else {
		model.processSample(proposedSamples);
	}

}

void HandlerRJMCMC::updatePIndMSS(Sampler::Sample &curSample) {

	typedef RJMCMC::RJSubSpaceInfo::listSSInfo_t::iterator itListSSInfo_t;

	std::vector< std::pair< Sampler::RJMCMC::RJSubSpaceUID::sharedPtr_t, std::vector<size_t> > > newPInd;
	for(itListSSInfo_t it = subSpaceRessources.begin(); it != subSpaceRessources.end(); ++it) {
		newPInd.push_back(std::make_pair((*it)->getUIDSubSpace(), (*it)->getParametersId()));
	}
	curSample.resetPIndMSS(newPInd);

}

State::BaseHandlerState::sharedPtr_t HandlerRJMCMC::saveState() const {
	//assert(false && "Not yet implemented"); //TODO IMPLEMENT
	State::StateHandlerRJMCMC::sharedPtr_t ptrState(new State::StateHandlerRJMCMC());

	ptrState->counter = counter;

	ptrState->sTimeRJMCMC = sTimeRJMCMC;
	ptrState->eTimeRJMCMC = eTimeRJMCMC;
	ptrState->timeApplyMove = timeApplyMove;
	ptrState->timeComputeMove = timeComputeMove;
	ptrState->timeAcceptRejectMove = timeAcceptRejectMove;
	ptrState->timeRJMCMC = timeRJMCMC;

	return ptrState;
}

void HandlerRJMCMC::loadState(State::BaseHandlerState::sharedPtr_t state) {
	//assert(false && "Not yet implemented"); //TODO IMPLEMENT
	State::StateHandlerRJMCMC* ptrState = dynamic_cast<State::StateHandlerRJMCMC*>(state.get());

	counter = ptrState->counter;

	sTimeRJMCMC = ptrState->sTimeRJMCMC;
	eTimeRJMCMC = ptrState->eTimeRJMCMC;
	timeApplyMove = ptrState->timeApplyMove;
	timeComputeMove = ptrState->timeComputeMove;
	timeAcceptRejectMove = ptrState->timeAcceptRejectMove;
	timeRJMCMC = ptrState->timeRJMCMC;

}

void HandlerRJMCMC::doPreProcessing(Sampler::Sample &curSample) {
	outcome->reset();
	TIME_MEASURE_RJMCMC sTimeRJMCMC = MPI_Wtime();
}

void HandlerRJMCMC::applyProposal(Sampler::Sample &curSample) {
	using namespace RJMCMC;

	//std::cout << "[HandlerRJMCMCM] Init sample : " << curSample.toString() << std::endl; // TODO DEBUG PRINT

	// Update outcome
	if(counter < proposal->getBurnIn()) return;

	//std::cout << "[HandlerRJMCMCM] Current sample : lik = " << curSample.likelihood; // TODO DEBUG PRINT
	//std::cout << "\t prior = " << curSample.prior ; // TODO DEBUG PRINT
	//std::cout << "\t posterior = " << curSample.posterior << std::endl; // TODO DEBUG PRINT

	// Get the move manager
	StatisticalModel::Model &model = proposal->getModel();
	RJMoveManager::sharedPtr_t managerRJ = proposal->getRJMoveManager();
	//std::cout << "[HandlerRJMCMCM] Got proposal manager" << std::endl; // TODO DEBUG PRINT

	// Everybody draw the same RJ move
	std::vector<Sampler::Sample> proposedSamples;
	RJMoveProposal::sharedPtr_t proposalRJ = managerRJ->drawProposal();
	//std::cout << "[HandlerRJMCMCM] Got proposal" << std::endl; // TODO DEBUG PRINT
	RJMove::sharedPtr_t moveRJ = proposalRJ->propose(curSample);
	//std::cout << "[HandlerRJMCMCM] Got move" << std::endl; // TODO DEBUG PRINT

	// If master
	//if(mcmcMgr.checkRoleProposal(mcmcMgr.Sequential) || mcmcMgr.checkRoleProposal(mcmcMgr.Manager)) {
	if(moveRJ->getMoveState() == RJMove::IMPOSSIBLE) {
		moveRJ->signalRejected(subSpaceRessources, model, blockDispatcher);
		return;
	}

	bool accepted = false; // Is the move accepted ?
	// Apply the move
	TIME_MEASURE_RJMCMC double sTimeApplyMove = MPI_Wtime();
	bool doMonitor_DBG = moveRJ->applyMove(subSpaceRessources, model, blockDispatcher);
	TIME_MEASURE_RJMCMC timeApplyMove += (MPI_Wtime()-sTimeApplyMove);

	proposedSamples.push_back(moveRJ->getProposedSample());
	//std::cout << "[HandlerRJMCMCM] Move applied" << std::endl; // TODO DEBUG PRINT

	// Compute posterior
	TIME_MEASURE_RJMCMC double sTimeComputeMove = MPI_Wtime();
	if(model.getLikelihood()->isUpdatable()){
		model.processSample(moveRJ->getUpdatedParametersID(), proposedSamples);
	} else {
		model.processSample(proposedSamples);
	}
	TIME_MEASURE_RJMCMC timeComputeMove += (MPI_Wtime()-sTimeComputeMove);
	//std::cout << "[HandlerRJMCMCM] Proposed sample : lik = " << proposedSamples.front().likelihood; // TODO DEBUG PRINT
	//std::cout << "\t prior = " << proposedSamples.front().prior ; // TODO DEBUG PRINT
	//std::cout << "\t posterior = " << proposedSamples.front().posterior << std::endl; // TODO DEBUG PRINT

	// Compute acceptance ratio
	double acceptanceRatio = 0.;
	double invTemp = samplerState.getInverseTemperature();
	if(model.getLikelihood()->isLog()) {
		// Posterior ratio
		acceptanceRatio = invTemp * proposedSamples.front().posterior - invTemp * curSample.posterior;
		// Probability of random number ratio (q(u')/q(u))
		acceptanceRatio += log(moveRJ->getRatioRandomProb());
		// Probability of moves ratio (q(M')/q(M))
		acceptanceRatio += log(moveRJ->getRatioMoveProb());
		// Probability of proposal ratio
		acceptanceRatio += log(moveRJ->getRatioProposalProb());
		// Jacobianproposal->getRJMoveManager()
		acceptanceRatio += log(moveRJ->getJacobian());
	} else {
		// Posterior ratio
		acceptanceRatio = pow(proposedSamples.front().posterior, invTemp) / pow(curSample.posterior, invTemp);
		// Random number generated ratio (q(u')/q(u))
		acceptanceRatio *= moveRJ->getRatioRandomProb();
		// Probability of moves ratio (q(M')/q(M))
		acceptanceRatio *= moveRJ->getRatioMoveProb();
		// Probability of proposal ratio
		acceptanceRatio *= moveRJ->getRatioProposalProb();
		// Jacobian
		acceptanceRatio *= moveRJ->getJacobian();
	}

	if(doMonitor_DBG) {
		std::cout << "[HandlerRJMCMCM] posterior prob = " << proposedSamples.front().posterior - curSample.posterior << std::endl;; // TODO DEBUG PRINT
		std::cout << "[HandlerRJMCMCM] likelihood prob = " << proposedSamples.front().likelihood - curSample.likelihood << std::endl;; // TODO DEBUG PRINT
		std::cout << "[HandlerRJMCMCM] prior prob = " << proposedSamples.front().prior - curSample.prior << std::endl;; // TODO DEBUG PRINT
		std::cout << "[HandlerRJMCMCM] proposal prob = " << log(moveRJ->getRatioProposalProb()) << std::endl;; // TODO DEBUG PRINT
		std::cout << "[HandlerRJMCMCM] move prob = " << log(moveRJ->getRatioMoveProb()) << std::endl;; // TODO DEBUG PRINT
		std::cout << "[HandlerRJMCMCM] rand prob = " << log(moveRJ->getRatioRandomProb()) << std::endl;; // TODO DEBUG PRINT
		std::cout << "[HandlerRJMCMCM] Acceptance ratio = " << acceptanceRatio << std::endl; // TODO DEBUG PRINT
		//std::cout << "[HandlerRJMCMCM] -----------------------------------------------------------------" << std::endl; // TODO DEBUG PRINT
	}

	// Acceptance check
	double u = proposal->getRNG()->genUniformDbl();
	// First check if better move, then if better acc. better than u
	if(model.getLikelihood()->isLog()) {
		accepted = (acceptanceRatio >= 0) || (acceptanceRatio > log(u));
	} else {
		accepted = (acceptanceRatio >= 1.0) || (acceptanceRatio > u);
	}

	// Broadcast result
	if(!mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
		int intAcc = accepted;
		mcmcMgr.bcastProposal(1, &intAcc, mcmcMgr.getManagerProposal());
	}

	// If not accepted, undo changes
	TIME_MEASURE_RJMCMC double timeARMove = MPI_Wtime();
	if(accepted) {
		//std::cout << "[HandlerRJMCMCM] Accepted" << std::endl; // TODO DEBUG PRINT
		proposalRJ->signalMoveAccepted();
		moveRJ->signalAccepted(subSpaceRessources, model, blockDispatcher);
	} else {
		//std::cout << "[HandlerRJMCMCM] Rejected" << std::endl; // TODO DEBUG PRINT
		proposalRJ->signalMoveRejected();
		moveRJ->signalRejected(subSpaceRessources, model, blockDispatcher);
	}
	TIME_MEASURE_RJMCMC timeAcceptRejectMove += (MPI_Wtime()-timeARMove);

	// Update outcome
	if(accepted) {
		outcome->getAcceptedSamples().push_back(proposedSamples.front());
		outcome->getAcceptedSamples().back().setChanged(true);
	}

	//std::cout << "[HandlerRJMCMCM] -----------------------------------------------------------------" << std::endl; // TODO DEBUG PRINT
}

void HandlerRJMCMC::doPostProcessing(Sampler::Sample &curSample) {
	TIME_MEASURE_RJMCMC eTimeRJMCMC = MPI_Wtime();
	timeRJMCMC += eTimeRJMCMC - sTimeRJMCMC;
	counter++;
}

void HandlerRJMCMC::updateSamples(Sampler::Sample &curSample, Sampler::SampleVector &samples) {
	if(mcmcMgr.isOutputManager()) {
		if(!outcome->getAcceptedSamples().empty()) {
			samples.push_back(outcome->getAcceptedSamples().front());
			samples.back().setChanged(true);
		} else {
			samples.push_back(curSample);
			samples.back().setChanged(false);
		}
	}

	if(!outcome->getAcceptedSamples().empty()) {
		curSample = outcome->getAcceptedSamples().front();
		curSample.setChanged(true);
	}
}

Outcome::BaseOutcome::sharedPtr_t HandlerRJMCMC::getOutcome() {
	return outcome;
}

std::string HandlerRJMCMC::reportStatus(std::string &prefix, size_t iT, double timeTotal) const {
	std::stringstream sstr;

	sstr << prefix << "[RJMCMC] Time apply move :\t" << timeApplyMove << "\t" << timeApplyMove/(double)counter << "\t" << 100.*timeApplyMove/timeTotal << std::endl;
	sstr << prefix << "[RJMCMC] Time compute move :\t" << timeComputeMove << "\t" << timeComputeMove/(double)counter << "\t" << 100.*timeComputeMove/timeTotal << std::endl;
	sstr << prefix << "[RJMCMC] Time acc/rej move :\t" << timeAcceptRejectMove << "\t" << timeAcceptRejectMove/(double)counter << "\t" << 100.*timeAcceptRejectMove/timeTotal << std::endl;
	sstr << prefix << "[RJMCMC] Time for RJCMCMC :\t" << timeRJMCMC << "\t" << timeRJMCMC/(double)counter << "\t" << 100.*timeRJMCMC/timeTotal << std::endl;
	sstr << prefix << proposal->getRJMoveManager()->getStringStats(prefix);

	return sstr.str();
}


} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */
