//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LightSampler.h
 *
 * @date Dec 9, 2016
 * @author meyerx
 * @brief
 */
#ifndef DEFAULTSAMPLER_H_
#define DEFAULTSAMPLER_H_

#include "BaseSampler.h"
#include "Sampler/TraceWriter/BaseWriter.h"

namespace StatisticalModel { class Model; }

namespace Sampler {

class Sample;
namespace Proposal { class Proposals; }

class LightSampler: public BaseSampler {
public:
	LightSampler(Model &aModel, Proposal::Proposals &aProposals,
				   BaseWriter::sharedPtr_t aTraceWriter);
	LightSampler(Sampler::Sample &initSample, Model &aModel,
			Proposal::Proposals &aProposals, BaseWriter::sharedPtr_t aTraceWriter);
	~LightSampler();

protected:

	//! Process the next MCMC iteration
	void processNextIteration();

	//! Status report
	std::string reportStatus(std::string prefix);

	void doSerializeToFile(const std::string &aFileName) const;
	void doSerializeFromFile(const std::string &aFileName);

	// Boost serialization
    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);
	BOOST_SERIALIZATION_SPLIT_MEMBER()

	friend class boost::serialization::access;

};

} /* namespace Sampler */

#endif /* DEFAULTSAMPLER_H_ */
