//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OutputManager.h
 *
 * @date Dec 13, 2016
 * @author meyerx
 * @brief
 */
#ifndef VERBOSITY_H_
#define VERBOSITY_H_

#include <stddef.h>
#include <string>

namespace Sampler {
namespace Information {

enum verboseLevel_t {
	NONE_VERB = 0, FEW_VERB = 1, MEDIUM_VERB = 2, HIGH_VERB = 3
};

class OutpoutManager {
public:

	void setVerbosityThreshold(verboseLevel_t aVerbLvl);
	verboseLevel_t getVerbosityThreshold();

	void setBaseFileName(const std::string& aBFName);
	const std::string& getBaseFileName() const;

	void setPrefixFileName(const std::string& aPFName);
	const std::string& getPrefixFileName() const;

	void setSamplerOutputFrequency(size_t aFreq);
	size_t getSamplerOutputFrequency() const;

	bool check(verboseLevel_t aVerbLvl) const;

private:

	std::string prefixFileName, baseFileName;
	verboseLevel_t verbosityThreshold;
	size_t samplerOutputFrequency;


	// Defined in the body
	OutpoutManager();
	~OutpoutManager();
	// Not defined to avoid call
	OutpoutManager(const OutpoutManager&);
	OutpoutManager& operator=(const OutpoutManager&);

	friend OutpoutManager& outputManager();
};

inline OutpoutManager& outputManager() {
    static OutpoutManager instance;
    return instance;
}

} /* namespace Information */
} /* namespace Sampler */
#endif /* VERBOSITY_H_ */

