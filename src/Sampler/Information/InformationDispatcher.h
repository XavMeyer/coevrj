//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file InformationDispatcher.h
 *
 * @date Dec 6, 2016
 * @author meyerx
 * @brief
 */
#ifndef INFORMATIONDISPATCHER_H_
#define INFORMATIONDISPATCHER_H_

#include <stddef.h>

#include "Sampler/TraceWriter/BaseWriter.h"
#include "Utils/Code/CheckpointFile.h"

namespace Sampler {
namespace Information {

class MC3Info;
class TextInfo;

class InformationDispatcher {
public:
	InformationDispatcher(bool aIsOutputManager, size_t &aIteration, TraceWriter::BaseWriter::sharedPtr_t aPtrWriter);
	virtual ~InformationDispatcher();

	//void dispatch(...);
	void dispatch(TextInfo* aTextInfo);
	void dispatch(MC3Info* aMC3Info);

private:
	bool isOutputManager;
	size_t &iteration;
	TraceWriter::BaseWriter::sharedPtr_t ptrWriter;

	std::streamoff offsetOutMC3File;
	::Utils::Checkpoint::File::sharedPtr_t outMC3File;
};

} /* namespace Information */
} /* namespace Sampler */

#endif /* INFORMATIONDISPATCHER_H_ */
