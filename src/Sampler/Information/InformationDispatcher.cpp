//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file InformationDispatcher.cpp
 *
 * @date Dec 6, 2016
 * @author meyerx
 * @brief
 */
#include "InformationDispatcher.h"

#include "Parallel/Parallel.h" // IWYU pragma: keep
#include "Sampler/Information/TextInfo.h"
#include "Sampler/Information/OutputManager.h"
#include "Sampler/Information/IncInformations.h"

namespace Sampler {
namespace Information {

InformationDispatcher::InformationDispatcher(bool aIsOutputManager, size_t &aIteration, TraceWriter::BaseWriter::sharedPtr_t aPtrWriter) :
		isOutputManager(aIsOutputManager), iteration(aIteration), ptrWriter(aPtrWriter) {
	offsetOutMC3File = 0;
}

InformationDispatcher::~InformationDispatcher() {
}

void InformationDispatcher::dispatch(TextInfo* aTextInfo) {
	if(isOutputManager) {
		std::cout << "[" << iteration << " :: " << ptrWriter->getNWSamples() << "] " << aTextInfo->getString() << std::endl;
	} else if(Parallel::mcmcMgr().isManagerMC3()) {
		if(outMC3File == NULL) {
			std::stringstream ssOutMC3FileName;
			ssOutMC3FileName << outputManager().getPrefixFileName() << Parallel::mcmcMgr().getMyChainMC3() << "_MC3.txt";
			outMC3File.reset(new ::Utils::Checkpoint::File(ssOutMC3FileName.str(), offsetOutMC3File));
		}
		outMC3File->getOStream() << "[" << iteration << " :: " << ptrWriter->getNWSamples() << "] " << aTextInfo->getString() << std::endl;
	}
}

void InformationDispatcher::dispatch(MC3Info* aMC3Info) {

	if (Parallel::mcmcMgr().isManagerMC3()) {
		if(outMC3File == NULL) {
			std::stringstream ssOutMC3FileName;
			ssOutMC3FileName << outputManager().getPrefixFileName() << Parallel::mcmcMgr().getMyChainMC3() << "_MC3.txt";
			outMC3File.reset(new ::Utils::Checkpoint::File(ssOutMC3FileName.str(), offsetOutMC3File));
		}
		outMC3File->getOStream() << "[" << iteration << " :: " << ptrWriter->getNWSamples() << "] " << aMC3Info->getString() << std::endl;
	}
}

} /* namespace Information */
} /* namespace Sampler */
