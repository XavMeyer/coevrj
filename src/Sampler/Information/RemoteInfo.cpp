//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RemoteInfo.cpp
 *
 * @date Oct 5, 2017
 * @author meyerx
 * @brief
 */
#include "Sampler/Information/RemoteInfo.h"

namespace Sampler {
namespace Information {

RemoteInfo::RemoteInfo(bool aIsSampleFromRemote) : sampleFromRemote(aIsSampleFromRemote) {

}

RemoteInfo::~RemoteInfo() {
}

bool RemoteInfo::isSampleFromRemote() const {
	return sampleFromRemote;
}


infoType_t RemoteInfo::getType() const {
	return REMOTE_INFO;
}

void RemoteInfo::submitToDispatcher(InformationDispatcher* dispatcher) {
	// Should not go through the dispatcher
}

} /* namespace Information */
} /* namespace Sampler */
