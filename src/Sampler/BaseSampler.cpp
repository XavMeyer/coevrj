//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseSampler.cpp
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#include "BaseSampler.h"

#include <assert.h>

#include "Sampler/Information/OutputManager.h"
#include "Sampler/Proposal/Proposals.h"
#include "Sampler/Proposal/Selector/Selector.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Bipartition/BipartitionMonitor.h"
#include "Model/Model.h"
#include "ParameterBlock/Block.h"
#include "Parallel/Manager/MCMCManager.h"
#include "Parallel/Manager/MpiManager.h"
#include "Parallel/RNG/RNG.h"
#include "Parallel/RNG/StateRNG/StateRNG.h"
#include "Proposal/Handler/BaseHandler.h"
#include <boost/serialization/nvp.hpp>

#include "mpi.h"

namespace Sampler {

namespace Checkpoint { class CheckpointManager; }
namespace Proposal { class BaseProposal; }

const unsigned int BaseSampler::STEP_WRITE = 200;


BaseSampler::BaseSampler(Model &aModel, Proposal::Proposals &aProposals,
		BaseWriter::sharedPtr_t aTraceWriter) :
		mcmcMgr(Parallel::mcmcMgr()), model(aModel), proposals(aProposals),
		ptrWriter(aTraceWriter), samplerState(iT), accessorsManager(),
		proposalSelector(12345), handlerManager(samplerState, accessorsManager),
		infoDispatcher(mcmcMgr.isOutputManager(), iT, ptrWriter){

	iT = 0;
	Sample sample(model.getRandomSample());
	init(sample);
}

BaseSampler::BaseSampler(Sample &initSample, Model &aModel,
		Proposal::Proposals &aProposals,
		BaseWriter::sharedPtr_t aTraceWriter) :
		mcmcMgr(Parallel::mcmcMgr()), model(aModel), proposals(aProposals),
		ptrWriter(aTraceWriter),  samplerState(iT), accessorsManager(),
		proposalSelector(12345), handlerManager(samplerState, accessorsManager),
		infoDispatcher(mcmcMgr.isOutputManager(), iT, ptrWriter){

	iT = 0;
	init(initSample);
}

BaseSampler::~BaseSampler() {

}

void BaseSampler::init(Sample &sample) {
	iT = 0;

	initSample(sample);
	isWriter = mcmcMgr.isOutputManager();
	timeTotal = timeSampling = timeWrite = 0.;

	if(proposals.empty()) {
		std::cerr << "[Error] in  BaseSampler::init(...)." << std::endl;
		std::cerr << "At least one proposal should be defined." << std::endl;
		assert(!proposals.empty());
	}

	for(size_t iP=0; iP<proposals.size(); ++iP) {
		proposals.getProposal(iP)->registerToSelector(&proposalSelector);
		proposals.getProposal(iP)->registerToHandlerManager(sample, &handlerManager);
	}

	// Add sample after Managers init -- since it can be changed by managers
	samples.push_back(sample);
	if(!mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
		curSample = sample;
		curSample.setChanged(true);
	} else {
		samples.back().setChanged(true);
		curSample = samples.front();
		curSample.setChanged(true);
	}

}

void BaseSampler::processNextIteration() {

	using namespace Proposal;
	using namespace Proposal::Handler;
	using namespace Proposal::Outcome;

	// Set the current sample as unchanged (by default)
	curSample.setChanged(false);

	// Select next proposal
	BaseProposal* nextProposal = proposalSelector.selectNext(iT);

	// Get handler
	BaseHandler::sharedPtr_t proposalHandler = handlerManager.getHandler(nextProposal);

	// Pre processing
	proposalHandler->doPreProcessing(curSample);

	// Apply
	proposalHandler->applyProposal(curSample);

	// Post processing
	proposalHandler->doPostProcessing(curSample);

	// Update samples
	proposalHandler->updateSamples(curSample, samples);

	// Enable likelihood to do custom operation during each iteration
	model.getLikelihood()->doCustomOperations(iT, curSample);

	// Process remaining informations
	std::list<Information::BaseInfo::sharedPtr_t>& infos = proposalHandler->getOutcome()->getInformations();
	typedef std::list<Information::BaseInfo::sharedPtr_t>::iterator itList_t;
	for(itList_t it = infos.begin(); it != infos.end(); ++it) {
		(*it)->submitToDispatcher(&infoDispatcher);
	}
}

void BaseSampler::initSample(Sample &sample) {
	sample.prior = model.processPriorValue(sample);
	sample.likelihood = model.getLikelihood()->processLikelihood(sample);
	sample.setMarkers(model.getLikelihood()->getMarkers());
	sample.setMSSMarkers(model.getLikelihood()->getSubSpaceMarkers());
	sample.posterior = processPosterior(sample.prior, sample.likelihood);

	if(!mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
		Utils::Serialize::buffer_t buffer;
		// aRank send the serialized sample
		if(mcmcMgr.getRankProposal() == mcmcMgr.getManagerProposal()) {
			buffer = sample.save();
			mcmcMgr.bcastSampleProposal(buffer, mcmcMgr.getManagerProposal());
		} else { // Other recieve it
			mcmcMgr.bcastSampleProposal(buffer, mcmcMgr.getManagerProposal());
			sample.load(buffer);
		}

		//curSample = sample;
		//curSample.setChanged(true);
	} else {
		//samples.back().setChanged(true);
		//curSample = samples.front();
		//curSample.setChanged(true);
	}
}

std::string BaseSampler::getPerformanceReport() {
	return reportStatus("[Final] ");
}

std::string BaseSampler::getBlockStatusReport() {

	using namespace Sampler::Proposal::Handler;

	Proposal::BlockDispatcher::vecBlocksAndSource_t vecBS = handlerManager.getBlockDispatcher().getBlocksSources();

	std::stringstream sstr;
	for(size_t iBS=0; iBS<vecBS.size(); ++iBS) {
		sstr << "[Final][Blocks]\t";
		if(vecBS[iBS].source == Proposal::BlockDispatcher::PFAMCMC_SOURCE) {
			sstr << "PFAMCMC" << std::endl;
		} else if(vecBS[iBS].source == Proposal::BlockDispatcher::GRADIENT_SOURCE) {
			sstr << "GRADIENT" << std::endl;
		} else {
			assert(false && "Block source not yet supported.");
		}

		std::vector<ParameterBlock::Block::sharedPtr_t> ptrBlocks = vecBS[iBS].ptrBlocks->getBlocks();
		for(uint i=0; i < ptrBlocks.size(); ++i){
			sstr << ptrBlocks[i]->toString() << endl;
			sstr << "**************************************" << endl;
		}

	}
	return sstr.str();
}

std::string BaseSampler::reportStatus(std::string prefix) {

	using namespace Sampler::Proposal::Handler;

	std::stringstream sstr;
	sstr << prefix << "[Sampler] Number of iteration :\t" << iT << endl;
	sstr << prefix << "[Sampler] Number of samples :\t" << ptrWriter->getNWSamples() << endl;
	sstr << prefix << "[Sampler] Mean sample per iteration :\t" << (float)ptrWriter->getNWSamples() / (float)iT << endl;

	for(size_t iP=0; iP<proposals.size(); ++iP) {
		BaseHandler::sharedPtr_t proposalHandler = handlerManager.getHandler(proposals.getProposal(iP));
		sstr << proposalHandler->reportStatus(prefix, iT, timeTotal);
	}

	sstr << prefix << "[Sampler] Time for write :\t" << timeWrite << "\t" << timeWrite/(double)iT << "\t" << 100*timeWrite/timeTotal << endl;
	sstr << prefix << "[Sampler] Time Total :\t" << timeTotal << "\t" << timeTotal/(double)iT  << endl;

	return sstr.str();
}


void BaseSampler::saveCheckpoint() {
	if(mcmcMgr.checkRoleProposal(mcmcMgr.Manager) || mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
		std::stringstream ckpSS;
		ckpSS << ptrWriter->getFNPrefix() << mcmcMgr.getMyChainMC3() << "MC3_" << ckpManager.getICKP() << "CKP.xml";

		// Base file name
		std::stringstream baseSS;
		baseSS << ptrWriter->getFNPrefix() << mcmcMgr.getMyChainMC3() << "MC3_CKP.xml";

		doSerializeToFile(ckpSS.str());

		{ // Copy to current file
			std::ifstream src(ckpSS.str().c_str(), std::ios::binary);
			std::ofstream dst(baseSS.str().c_str(), std::ios::binary);
			dst << src.rdbuf();
		}

		if(!ckpManager.areAllCheckpointKept()) { // If we do not keep all checkpoints
			int toRemId = ckpManager.getICKP()-ckpManager.getNCheckpointKept();
			if(toRemId > 0) { // The checkpoint should exist
				std::stringstream ckpRemSS;
				ckpRemSS << ptrWriter->getFNPrefix() << mcmcMgr.getMyChainMC3() << "MC3_" << toRemId << "CKP.xml";

				{
					std::ifstream ifs(ckpRemSS.str().c_str());
				}
				remove(ckpRemSS.str().c_str());
			}
		}
	}
}

void BaseSampler::loadCheckpoint() {
	std::stringstream ss;
	int myChainMC3 = mcmcMgr.getMyChainMC3();
	if(myChainMC3 >= 0) {
		ss << ptrWriter->getFNPrefix() << mcmcMgr.getMyChainMC3() << "MC3_CKP.xml";
		doSerializeFromFile(ss.str());
	}
}


void BaseSampler::generateNIterations(uint aN){

	// Checkpoints
	removeEndFileSignal();
	if(ckpManager.isActive() && iT == 0) {
		loadCheckpoint();
	}

	// Initial write for samples
	writeHeaderToTraceFile();

	size_t start = iT;
	for(iT=start; iT<aN; ++iT){
		double startTime = MPI_Wtime();

		// Write std output
		writeOutput();

		{	// Process iteration iT
			double sTimeSampling = MPI_Wtime();
			processNextIteration();
			timeSampling += MPI_Wtime() - sTimeSampling;
		}


		{	// Write samples (isFinalWrite=FALSE)
			double sTimeWriting = MPI_Wtime();
			writeSamplesToTraceFile(false);
			timeSampling += MPI_Wtime() - sTimeWriting;
		}

		// Checkpoints required ?
		if(ckpManager.isCheckpointRequired(iT) && (Parallel::mcmcMgr().isManagerMC3() ||
				Parallel::mcmcMgr().isOutputManager())) {
			ckpManager.signalDone();
			saveCheckpoint();
		}

		if(iT%50 == 0 && catchEndFileSignal()) {
			ckpManager.setActive();
			break;
		}

		timeTotal += MPI_Wtime() - startTime;
	}

	{	// Write samples (isFinalWrite=TRUE)
		double sTimeWriting = MPI_Wtime();
		writeSamplesToTraceFile(true);
		timeSampling += MPI_Wtime() - sTimeWriting;
	}

	// Checkpoints required ?
	if(ckpManager.isActive() && (Parallel::mcmcMgr().isManagerMC3() ||
			Parallel::mcmcMgr().isOutputManager())) {
		ckpManager.signalDone();
		saveCheckpoint();
	}

}

double BaseSampler::processPosterior(const double aPrior, const double aLikelihood) const {
	if(model.getLikelihood()->isLog()){
		return aPrior + aLikelihood;
	} else {
		return aPrior * aLikelihood;
	}
}

Sample BaseSampler::getLastSample() const {
	return curSample;
}

//! Print the times taken for each operations (perfomance report)
void BaseSampler::printTimes() {
	if(isWriter){
		cout << "Time W :\t" << timeWrite <<  endl;
	}
}

std::string BaseSampler::getOutputFileBaseName() const {
	return ptrWriter->getFileBaseName();
}

std::string BaseSampler::getOutputFullFileName() const {
	return ptrWriter->getFileName();
}

Checkpoint::CheckpointManager& BaseSampler::getCheckpointManager() {
	return ckpManager;
}

BaseWriter::sharedPtr_t BaseSampler::getTraceWriter() {
	return ptrWriter;
}

//! Write the header
void BaseSampler::writeHeaderToTraceFile() {
	if(isWriter){
		ptrWriter->writeHeader(samples, ckpManager, model); // Init header and output file
		if(ckpManager.isActive()) {
			// Init customized log file with checkpoint
			model.getLikelihood()->initCustomizedLog(ptrWriter->getFNPrefix(), ptrWriter->getFNSuffix(), ckpManager.getCustomLogFileOffsets());
		} else {
			// Init customized log file
			std::vector<std::streamoff> vecDummy;
			model.getLikelihood()->initCustomizedLog(ptrWriter->getFNPrefix(), ptrWriter->getFNSuffix(), vecDummy);
		}
	}
}

//! Write samples
void BaseSampler::writeSamplesToTraceFile(bool isFinalWrite) {
	if(!isWriter) return;

	double s = MPI_Wtime();
	if(iT%STEP_WRITE==0 || isFinalWrite) {
		ptrWriter->writeSamples(iT, samples, model);
		// Clean the sample vector
		Sample tmp(samples.back());
		samples.clear();
		samples.push_back(tmp);
	} else if(isFinalWrite) {
		// Insure that everybody is written
		bool changedState = samples.back().hasChanged();
		samples.back().setChanged(true);
		ptrWriter->writeSamples(iT, samples, model);
		// Copy last sample and insert it
		samples.back().setChanged(changedState);
		// Clean the sample vector
		Sample tmp(samples.back());
		samples.clear();
		samples.push_back(tmp);
	}
	// Measure time
	timeWrite += MPI_Wtime() - s;
}

void BaseSampler::writeOutput() {
	if(Parallel::mpiMgr().isMainProcessor() && Information::outputManager().getVerbosityThreshold() > 0){
		std::cout.precision(2);
		if(iT%Information::outputManager().getSamplerOutputFrequency()==0) {
			std::cout << "Iteration : " << iT << " -- posterior = " << std::fixed << samples.back().posterior << endl;
		}
		if((1+iT)%(100*Information::outputManager().getSamplerOutputFrequency())==0 && Information::outputManager().getVerbosityThreshold() > 0) {
			std::cout << reportStatus("[Partial] ") << std::endl;;
		}
	}
}

bool BaseSampler::catchEndFileSignal() {
	stringstream fName;
	fName << ptrWriter->getFNPrefix() << ".stop";
	//fName << fnPrefix << 0 << fnSuffix << ".stop";
	mcmcMgr.syncProposals();
	std::ifstream ifs(fName.str().c_str());
	if(ifs.good()) {
		ckpManager.setActive();
		return true;
	}

	return false;
}

void BaseSampler::removeEndFileSignal() {
	stringstream fName;
	fName << ptrWriter->getFileBaseName() << ".stop";
	remove(fName.str().c_str());
}


// Boost serialization
template<class Archive>
void BaseSampler::save(Archive & ar, const unsigned int version) const {

	using namespace Proposal::Handler;
	using namespace MolecularEvolution::TreeReconstruction;

	ar & BOOST_SERIALIZATION_NVP( iT );
	ar & BOOST_SERIALIZATION_NVP( timeSampling );
	ar & BOOST_SERIALIZATION_NVP( timeWrite );
	ar & BOOST_SERIALIZATION_NVP( timeTotal );

	// Sampler state
	ar & BOOST_SERIALIZATION_NVP( samplerState );

	// Samples
	Utils::Serialize::buffer_t serializedCurSample = curSample.save();
	ar & BOOST_SERIALIZATION_NVP( serializedCurSample );

	// Likelihood internal state (trees, etc.)
	Utils::Serialize::buffer_t internalState;
	model.getLikelihood()->saveInternalState(curSample, internalState);
	ar & BOOST_SERIALIZATION_NVP( internalState );

	Bipartition::BipartitionMonitor::sharedPtr_t ptrBM = model.getLikelihood()->getBipartitionMonitor();
	if(ptrBM) {
		Bipartition::BipartitionMonitorState bmState;
		ptrBM->saveState(bmState);
		ar & BOOST_SERIALIZATION_NVP( bmState );
	}

	// Serialize proposals
	ar & BOOST_SERIALIZATION_NVP(proposals);

	// Serialize managers
	std::vector<Proposal::Handler::State::BaseHandlerState::sharedPtr_t> vecHandlerState;
	const std::vector<Proposal::BaseProposal::sharedPtr_t>& vecP = proposals.getProposals();
	for(size_t iP=0; iP<vecP.size(); ++iP) {
		vecHandlerState.push_back(handlerManager.getHandlerState(vecP[iP]));
	}
	ar & BOOST_SERIALIZATION_NVP( vecHandlerState );

	// Checkpoint related
	size_t iCKP = ckpManager.getICKP();
	ar & BOOST_SERIALIZATION_NVP( iCKP );

	size_t logFileOffset = 0;
	std::vector< std::streamoff > customLogFileOffsets;
	if(mcmcMgr.isOutputManager()) {
		logFileOffset = ptrWriter->getFileOffset();
		customLogFileOffsets = model.getLikelihood()->getCustomizedLogSize();
	}
	ar & BOOST_SERIALIZATION_NVP( logFileOffset );
	ar & BOOST_SERIALIZATION_NVP( customLogFileOffsets );

	// RNG
	StateRNG prngState;
	Parallel::mpiMgr().getPRNG()->saveState(prngState);
	ar & BOOST_SERIALIZATION_NVP( prngState );

	// WRITER
	size_t cntAccepted = ptrWriter->getCntAccepted();
	ar & BOOST_SERIALIZATION_NVP( cntAccepted );
	size_t nWSamples = ptrWriter->getNWSamples();
	ar & BOOST_SERIALIZATION_NVP( nWSamples );
}

template<class Archive>
void BaseSampler::load(Archive & ar, const unsigned int version) {

	using namespace Proposal::Handler;
	using namespace MolecularEvolution::TreeReconstruction;

	ar & BOOST_SERIALIZATION_NVP( iT );
	ar & BOOST_SERIALIZATION_NVP( timeSampling );
	ar & BOOST_SERIALIZATION_NVP( timeWrite );
	ar & BOOST_SERIALIZATION_NVP( timeTotal );

	// Sampler state
	ar & BOOST_SERIALIZATION_NVP( samplerState );

	// Samples
	Utils::Serialize::buffer_t serializedCurSample;
	ar & BOOST_SERIALIZATION_NVP( serializedCurSample );
	curSample.load(serializedCurSample);

	samples.clear();
	samples.push_back(curSample);

	// Likelihood internal state (trees, etc.)
	Utils::Serialize::buffer_t internalState;
	ar & BOOST_SERIALIZATION_NVP( internalState );
	model.getLikelihood()->loadInternalState(curSample, internalState);

	Bipartition::BipartitionMonitor::sharedPtr_t ptrBM = model.getLikelihood()->getBipartitionMonitor();
	if(ptrBM) {
		Bipartition::BipartitionMonitorState bmState;
		ar & BOOST_SERIALIZATION_NVP( bmState );
		ptrBM->loadState(bmState);
	}

	// Serialize proposals
	ar & BOOST_SERIALIZATION_NVP(proposals);

	// Serialize managers
	std::vector<Proposal::Handler::State::BaseHandlerState::sharedPtr_t> vecHandlerState;
	ar & BOOST_SERIALIZATION_NVP( vecHandlerState );
	assert(vecHandlerState.size() == proposals.size());
	for(size_t iP=0; iP<proposals.size(); ++iP) {
		handlerManager.setHandlerState(proposals.getProposal(iP), vecHandlerState[iP]);
	}

	// Checkpoint related
	size_t iCKP = 0;
	ar & BOOST_SERIALIZATION_NVP( iCKP );
	ckpManager.setICKP(iCKP);

	size_t logFileOffset = 0;
	ar & BOOST_SERIALIZATION_NVP( logFileOffset );
	std::vector<std::streamoff> customLogFileOffsets;
	ar & BOOST_SERIALIZATION_NVP( customLogFileOffsets );

	if(mcmcMgr.isOutputManager()) {
		ckpManager.setLogFileOffset(logFileOffset);
		ckpManager.setCustomLogFileOffsets(customLogFileOffsets);
	}

	// RNG
	StateRNG prngState;
	ar & BOOST_SERIALIZATION_NVP( prngState );
	bool onlyCounter = true;
	Parallel::mpiMgr().getPRNG()->loadState(prngState, onlyCounter);

	// WRITER
	size_t cntAccepted = 0, nWSamples = 0;
	ar & BOOST_SERIALIZATION_NVP( cntAccepted );
	ptrWriter->setCntAccepted(cntAccepted);
	ar & BOOST_SERIALIZATION_NVP( nWSamples );
	ptrWriter->setNWSamples(nWSamples);

}


template void BaseSampler::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void BaseSampler::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void BaseSampler::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void BaseSampler::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Sampler */
