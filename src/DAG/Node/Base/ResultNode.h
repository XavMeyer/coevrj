//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ResultNode.h
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef RESULTNODE_H_
#define RESULTNODE_H_

#include "BaseNode.h"

namespace DAG {

template <typename T>
class ResultNode : public BaseNode {
public:
	ResultNode() : BaseNode() {};
	virtual ~ResultNode(){};

	const T& getResult() const {
		return result;
	}

	void setResult(const T aRes) {
		result = aRes;
	}

protected:

	T result;

};

} /* namespace DAG */

#endif /* RESULT_H_ */
