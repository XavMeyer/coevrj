//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelihoodNode.h
 *
 * @date Feb 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef LIKELIHOODNODE_H_
#define LIKELIHOODNODE_H_

namespace DAG {

class LikelihoodNode {
public:
	LikelihoodNode();
	virtual ~LikelihoodNode();

	double getLikelihood() const;

protected:
	double likelihood;
};

} /* namespace DAG */

#endif /* LIKELIHOODNODE_H_ */
