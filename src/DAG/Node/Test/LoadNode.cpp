//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LoadNode.cpp
 *
 * @date Nov 26, 2014
 * @author meyerx
 * @brief
 */
#include "LoadNode.h"

namespace DAG {

LoadNode::LoadNode() : BaseNode() {
	value = 0;
}

LoadNode::~LoadNode() {
}

void LoadNode::setValue(double aValue) {
	value = aValue;
	this->updated();
}

double LoadNode::getValue() const {
	return value;
}

void LoadNode::doProcessing() {
	//cout << "LoadNode [" << getId() << "] processing -> val=" << value << endl;
}

bool LoadNode::processSignal(BaseNode* aChild) {
	return true;
}



} /* namespace DAG */
