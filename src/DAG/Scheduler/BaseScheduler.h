//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseScheduler.h
 *
 * @date Dec 9, 2014
 * @author meyerx
 * @brief
 */
#ifndef BASESCHEDULER_H_
#define BASESCHEDULER_H_

#include <algorithm>
#include <list>
#include <map>
#include <set>
#include <vector>
#include <boost/dynamic_bitset_fwd.hpp>

namespace DAG { class BaseNode; }

namespace DAG {
namespace Scheduler {

class BaseScheduler {
public:
	// SOFT = minimal (leaves and state untouched), topoList may be recomputed
	// PARTIAL = Reset and search leaves, state are untouched, topoList is recomputed
	// HARD = Full reinit
	enum EnumReinitStrategy {SOFT_REINIT, PARTIAL_REINIT, HARD_REINIT};
	typedef EnumReinitStrategy reinitStrategy_t;
public:
	BaseScheduler(BaseNode* const aRoot);
	virtual ~BaseScheduler();

	void process(bool doFakeComputation=false);
	virtual void reinitialize(reinitStrategy_t reinitStrat) = 0;

	void resetDAG();
	bool resetPartial();
	bool resetPartial(const std::set<BaseNode*> &updatedNodes);

	BaseNode* getRoot() const {
		return root;
	}

	std::vector<double> getTimeStatistics() const;
	const std::list<BaseNode*>& getTopologicalList() const;

	bool findNodeToProcess(BaseNode *node, std::vector<BaseNode*> &mustBeProcessed);

protected:

	BaseNode* const root;

	typedef std::vector<BaseNode*> vecBNode_t;
	typedef std::list<BaseNode*> listBNode_t;

	vecBNode_t leaves;
	listBNode_t readyNodes;
	listBNode_t topoList;

	void initDAG();
	void initTopoList();
	bool findReady(BaseNode *node);
	void updateParentsState(BaseNode *node);
	void resetNode(BaseNode *node);

	void defaultReinitialize(reinitStrategy_t reinitStrat, bool reinitTopo);

	virtual void doProcessing(bool doFakeComputation) = 0;

private:
	void findLeaves(BaseNode *node, std::set<size_t> &exploredNodes, bool resetNodeState);
	void findLeavesFast(BaseNode *node, size_t depth, const size_t MAX_DEPTH);
};

} /* namespace Scheduler */
} /* namespace DAG */

#endif /* BASESCHEDULER_H_ */
