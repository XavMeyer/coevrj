//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CenteredDirichletBM.cpp
 *
 * @date May 24, 2017
 * @author meyerx
 * @brief
 */
#include "CenteredDirichletBM.h"
#include "Utils/Statistics/Dirichlet/DirichletPDF.h"

namespace ParameterBlock {

const double CenteredDirichletBM::BASE_CONCENTRATION = 5000.0;

CenteredDirichletBM::CenteredDirichletBM(bool aAreParamOnSimplex, Model &aModel) :
		BlockModifier(false, aModel), areParamOnSimplex(aAreParamOnSimplex) {

	scalingConcentration = 1.0;

}

CenteredDirichletBM::~CenteredDirichletBM() {
}

void CenteredDirichletBM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {

	std::vector<double> valSimplex;
	for(size_t iP=0; iP < pInd.size(); ++iP){
		double val = sample.getDoubleParameter(pInd[iP]);
		//std::cout << std::scientific << "pVal : " << pInd[iP] << " = " << val << std::endl;
		valSimplex.push_back(val);
	}

	if(!areParamOnSimplex) { // Normalize if needed
		valSimplex.push_back(1.0);

		double sumVals = 0.;
		for(size_t iA=0; iA<valSimplex.size(); ++iA) sumVals += valSimplex[iA];
		for(size_t iA=0; iA<valSimplex.size(); ++iA) valSimplex[iA] /= sumVals;
	}

	/*for(size_t iA=0; iA<valSimplex.size(); ++iA) {
		std::cout << "Simplex val : " << iA << " = " << valSimplex[iA] << std::endl;
	}*/

	// Set the alphas values
	bool useMean = true;
	std::vector<double> alphas(valSimplex);
	if(useMean) { // Default is mean = x val
		for(size_t iA=0; iA<alphas.size(); ++iA) {
			alphas[iA] *= BASE_CONCENTRATION*scalingConcentration;
			alphas[iA] += 1e-1;
		}

	} else { // Alternative is mode = xval
		for(size_t iA=0; iA<alphas.size(); ++iA) {
			alphas[iA] = alphas[iA] * (BASE_CONCENTRATION*scalingConcentration - (double)pInd.size()) + 1.;
		}
	}

	/*for(size_t iA=0; iA<alphas.size(); ++iA) {
		std::cout << "Alphas val : " << iA << " = " << alphas[iA] << std::endl;
	}*/

	// Draw new values
	fwAlphas = alphas;
	std::vector<double> newDrawSimplex(Parallel::mpiMgr().getPRNG()->genDirichlet(alphas));
	/*for(size_t iD=0; iD<newDrawSimplex.size(); ++iD) {
		std::cout << "Draw val : " << iD << " = " << newDrawSimplex[iD] << std::endl;
	}*/

	// Define Backward alphas
	bwAlphas = newDrawSimplex;
	if(useMean) { // Default is mean = x val
		for(size_t iA=0; iA<bwAlphas.size(); ++iA) {
			bwAlphas[iA] *= BASE_CONCENTRATION*scalingConcentration;
			bwAlphas[iA] += 1e-1;
		}

	} else { // Alternative is mode = x val
		for(size_t iA=0; iA<bwAlphas.size(); ++iA) {
			bwAlphas[iA] = bwAlphas[iA] * (BASE_CONCENTRATION*scalingConcentration - (double)pInd.size()) + 1.;
		}
	}


	if(areParamOnSimplex) { // Set params if on simplex
		for(size_t iP=0; iP < pInd.size(); ++iP){
			double val = newDrawSimplex[iP];
			sample.setDoubleParameter(pInd[iP], val);
		}
	} else { // Else transform, then set
		std::vector<double> transformedDraw;
		double sum = 1.0/newDrawSimplex.back();
		for(size_t iD=0; iD < newDrawSimplex.size()-1; ++iD){
			double transformedVal = newDrawSimplex[iD]*sum;
			transformedDraw.push_back(transformedVal);
		}

		/*for(size_t iD=0; iD<transformedDraw.size(); ++iD) {
			std::cout << "Transfo draw val : " << iD << " = " << transformedDraw[iD] << std::endl;
		}
		std::cout << "-----------------------------------------" << std::endl;*/

		assert(pInd.size() == transformedDraw.size());
		for(size_t iP=0; iP < pInd.size(); ++iP){
			double val = transformedDraw[iP];
			sample.setDoubleParameter(pInd[iP], val);
		}
	}

	sample.setEvaluated(false);

}

double CenteredDirichletBM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {

	assert(false); // FIXME Implement

	return 0.;
}

double CenteredDirichletBM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {

	Utils::Statistics::DirichletPDF fwDirichletPDF(fwAlphas);
	Utils::Statistics::DirichletPDF bwDirichletPDF(bwAlphas);

	std::vector<double> fromSampleValSimplex;
	{
		for(size_t iP=0; iP < pInd.size(); ++iP){
			double val = fromSample.getDoubleParameter(pInd[iP]);
			fromSampleValSimplex.push_back(val);
		}

		if(!areParamOnSimplex) { // Normalize if needed
			fromSampleValSimplex.push_back(1.0);

			double sumVals = 0.;
			for(size_t iA=0; iA<fromSampleValSimplex.size(); ++iA) sumVals += fromSampleValSimplex[iA];
			for(size_t iA=0; iA<fromSampleValSimplex.size(); ++iA) fromSampleValSimplex[iA] /= sumVals;
		}
	}

	std::vector<double> toSampleValSimplex;
	{
		for(size_t iP=0; iP < pInd.size(); ++iP){
			double val = fromSample.getDoubleParameter(pInd[iP]);
			toSampleValSimplex.push_back(val);
		}

		if(!areParamOnSimplex) { // Normalize if needed
			toSampleValSimplex.push_back(1.0);

			double sumVals = 0.;
			for(size_t iA=0; iA<toSampleValSimplex.size(); ++iA) sumVals += toSampleValSimplex[iA];
			for(size_t iA=0; iA<toSampleValSimplex.size(); ++iA) toSampleValSimplex[iA] /= sumVals;
		}
	}


	double moveProbFW = fwDirichletPDF.computePDF(toSampleValSimplex);
	double moveProbBW = bwDirichletPDF.computePDF(fromSampleValSimplex);

	return moveProbBW/moveProbFW;
}

void CenteredDirichletBM::setWindowSize(const vector<double> &aWinSize) {
	assert(false && "Deal with serialization");
	assert(aWinSize.size() > 0);
	scalingConcentration = aWinSize[0];
}

std::vector<double> CenteredDirichletBM::getWindowSize() const {
	std::vector<double> win(1, scalingConcentration);
	return win;
}


} /* namespace ParameterBlock */

