//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WeightedDiscreteCategoriesBM.cpp
 *
 * @date May 10, 2017
 * @author meyerx
 * @brief
 */
#include "ParameterBlock/BlockModifier/Discrete/WeightedDiscreteCategoriesBM.h"

namespace ParameterBlock {

WeightedDiscreteCategoriesBM::WeightedDiscreteCategoriesBM(const std::vector<double> &data, Model &aModel) :
		BlockModifier(false, aModel) {
	setWindowSize(data);
}

WeightedDiscreteCategoriesBM::WeightedDiscreteCategoriesBM(const std::vector<size_t> &aIdProfiles,
														   const std::vector<double> &aWeight,
														   Model &aModel) :
		BlockModifier(false, aModel), idProfiles(aIdProfiles), weight(aWeight) {

	// Normalize weights
	double sumW=0;
	for(size_t iW=0; iW<weight.size(); ++iW) sumW += weight[iW];
	for(size_t iW=0; iW<weight.size(); ++iW) weight[iW] /= sumW;
}

WeightedDiscreteCategoriesBM::~WeightedDiscreteCategoriesBM() {
}

void WeightedDiscreteCategoriesBM::updateParameters(const vector<size_t> &pInd, Sample &sample) const {

	assert(pInd.size() == 1);
	//size_t iProf = Parallel::mpiMgr().getPRNG()->genUniformInt(0, idProfiles.size()-1);
	size_t iProf = Parallel::mpiMgr().getPRNG()->drawFrom(weight);
	//std::cout << sample.getIntParameter(pInd.front()) << " to " << idProfiles[iProf] << " over all possible = " << idProfiles.size() << " { ";
	sample.setIntParameter(pInd.front(), idProfiles[iProf]);
	/*for(size_t i=0; i<idProfiles.size(); ++i) std::cout << idProfiles[i] << ", ";
	std::cout << " } " << std::endl;*/

	sample.setEvaluated(false);
}

double WeightedDiscreteCategoriesBM::getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	size_t val = toSample.getIntParameter(pInd.front());
	for(size_t iP=0; iP<idProfiles.size(); ++iP) {
		if(idProfiles[iP] == val) return weight[iP];
	}
	assert(false && "The category was not found.");
	return 0.;
}

double WeightedDiscreteCategoriesBM::getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {

	size_t valFrom = fromSample.getIntParameter(pInd.front());
	size_t valTo = toSample.getIntParameter(pInd.front());

	double probFrom = 0., probTo = 0.;
	for(size_t iP=0; iP<idProfiles.size(); ++iP) {
		if(idProfiles[iP] == valFrom) probFrom = weight[iP];
		if(idProfiles[iP] == valTo) probTo = weight[iP];
	}
	assert(probFrom != 0. && probTo != 0. && "One of the value was not found in the categories.");

	return probFrom/probTo;
}

void WeightedDiscreteCategoriesBM::setWindowSize(const std::vector<double> &aWinSize) {
	size_t vecSize = aWinSize.size()/2;

	idProfiles.clear();
	weight.clear();
	for(size_t iP=0; iP<vecSize; ++iP) {
		size_t idProf = static_cast<size_t>(aWinSize[iP]);
		idProfiles.push_back(idProf);
		weight.push_back(aWinSize[iP+vecSize]);
	}
}

std::vector<double> WeightedDiscreteCategoriesBM::getWindowSize() const {

	std::vector<double> winSize;
	for(size_t iP=0; iP<idProfiles.size(); ++iP) {
		double idProf = static_cast<double>(idProfiles[iP]);
		winSize.push_back(idProf);
	}

	for(size_t iW=0; iW<weight.size(); ++iW) {
		winSize.push_back(weight[iW]);
	}

	/*for(size_t iP=0; iP<winSize.size(); ++iP) {
		assert(static_cast<size_t>(winSize[iP]) == idProfiles[iP] &&
				"Find a better way to convert that!");
	}*/

	return winSize;
}

} /* namespace ParameterBlock */
