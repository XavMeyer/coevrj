//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DiscreteCategoriesBM.cpp
 *
 * @date Apr 21, 2017
 * @author meyerx
 * @brief
 */
#include "DiscreteCategoriesBM.h"

#include <assert.h>
#include <stddef.h>

namespace ParameterBlock {

DiscreteCategoriesBM::DiscreteCategoriesBM(const std::vector<double> &data, Model &aModel) :
		BlockModifier(true, aModel) {
	setWindowSize(data);
}

DiscreteCategoriesBM::DiscreteCategoriesBM(const std::vector<size_t> &aIdProfiles, Model &aModel) :
		BlockModifier(true, aModel), idProfiles(aIdProfiles) {
	moveProb = 1./(double)idProfiles.size();
}

DiscreteCategoriesBM::~DiscreteCategoriesBM() {
}

void DiscreteCategoriesBM::updateParameters(const vector<size_t> &pInd, Sample &sample) const {

	assert(pInd.size() == 1);
	size_t iProf = Parallel::mpiMgr().getPRNG()->genUniformInt(0, idProfiles.size()-1);
	//std::cout << sample.getIntParameter(pInd.front()) << " to " << idProfiles[iProf] << " over all possible = " << idProfiles.size() << " { ";
	sample.setIntParameter(pInd.front(), idProfiles[iProf]);
	/*for(size_t i=0; i<idProfiles.size(); ++i) std::cout << idProfiles[i] << ", ";
	std::cout << " } " << std::endl;*/

	sample.setEvaluated(false);
}

double DiscreteCategoriesBM::getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	return moveProb;
}

double DiscreteCategoriesBM::getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	return 1.;
}

void DiscreteCategoriesBM::setWindowSize(const std::vector<double> &aWinSize) {
	idProfiles.clear();
	for(size_t iP=0; iP<aWinSize.size(); ++iP) {
		size_t idProf = static_cast<size_t>(aWinSize[iP]);
		idProfiles.push_back(idProf);
	}
	moveProb = 1./(double)idProfiles.size();
}

std::vector<double> DiscreteCategoriesBM::getWindowSize() const {

	std::vector<double> winSize;
	for(size_t iP=0; iP<idProfiles.size(); ++iP) {
		double idProf = static_cast<double>(idProfiles[iP]);
		winSize.push_back(idProf);
	}

	for(size_t iP=0; iP<winSize.size(); ++iP) {
		assert(static_cast<size_t>(winSize[iP]) == idProfiles[iP] &&
				"Find a better way to convert that!");
	}

	return winSize;
}

} /* namespace ParameterBlock */
