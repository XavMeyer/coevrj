//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockModifierHelper.h
 *
 * @date Jan 28, 2016
 * @author meyerx
 * @brief
 */

#include "BlockModifierHelper.h"

#include <assert.h>


#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/TIGamma/Base.h"
#include "Model/Model.h"
#include "ParameterBlock/BlockModifier/BlockModifierState.h"

namespace ParameterBlock {
namespace Support {

BlockModifier::sharedPtr_t createBlockModifier( size_t N, StatisticalModel::Model& model, State::BlockModifierState &state) {

	BlockModifier::bm_name_t bmName = static_cast<BlockModifier::bm_name_t>(state.getBMType());
	BlockModifier::sharedPtr_t ptr;

	switch (bmName) {
		case BlockModifier::NONE:
			ptr = BlockModifier::sharedPtr_t();
			return ptr;
		case BlockModifier::UNIFORM:
			ptr = BlockModifier::createUniformWindow(state.getValues().front(), model);
			break;
		case BlockModifier::GAUSSIAN_1D:
			ptr = BlockModifier::createGaussianWindow(state.getValues().front(), model);
			break;
		case BlockModifier::GAUSSIAN_MVN:
			ptr = BlockModifier::createMVGaussianWindow(N, state.getValues().data(), model);
			break;
		case BlockModifier::PCA_MVN:
			ptr = BlockModifier::createPCAWindow(N, state.getValues(), model);
			break;
		case BlockModifier::MULTIPLIER:
			ptr = BlockModifier::createMultiplierWindow(state.getValues().front(), model);
			break;
		case BlockModifier::STNNI:
			assert(model.getLikelihood()->getTreeManager());
			ptr = BlockModifier::createSTNNIBM(model.getLikelihood()->getTreeManager(), model);
			break;
		case BlockModifier::ESPR:
			assert(model.getLikelihood()->getTreeManager());
			ptr = BlockModifier::createESPRBM(state.getValues().front(), model.getLikelihood()->getTreeManager(), model);
			break;
		case BlockModifier::ETBR:
			assert(model.getLikelihood()->getTreeManager());
			ptr = BlockModifier::createETBRBM(state.getValues().front(), model.getLikelihood()->getTreeManager(), model);
			break;
		case BlockModifier::ADAPTIVE_ESPR:
			{
				assert(model.getLikelihood()->getTreeManager() &&  model.getLikelihood()->getBipartitionMonitor());
				TR::AdaptiveESPR::stepNumber_t nStep = static_cast<TR::AdaptiveESPR::stepNumber_t>(state.getValues()[0]);
				TR::AdaptiveESPR::bias_t bias = static_cast<TR::AdaptiveESPR::bias_t>(state.getValues()[1]);
				double epsilonFreq = state.getValues()[2];
				double powerFreq = state.getValues()[3];
				ptr = BlockModifier::createAdaptiveESPRBM(nStep, bias, epsilonFreq, powerFreq, model.getLikelihood()->getTreeManager(), model.getLikelihood()->getBipartitionMonitor(), model);
			}
			break;
		case BlockModifier::ADAPTIVE_PBJ:
			{
				assert(model.getLikelihood()->getTreeManager()&&  model.getLikelihood()->getBipartitionMonitor());
				TR::AdaptivePBJ::moveType_t moveType = static_cast<TR::AdaptivePBJ::moveType_t>(state.getValues()[0]);
				double pe = state.getValues()[1];
				double epsilonFreq = state.getValues()[2];
				double powerFreq = state.getValues()[3];
				ptr = BlockModifier::createAdaptivePBJBM(moveType, pe, epsilonFreq, powerFreq, model.getLikelihood()->getTreeManager(), model.getLikelihood()->getBipartitionMonitor(), model);
			}
			break;
		case BlockModifier::GUIDED_ESPR:
			{
				assert(model.getLikelihood()->getTreeManager() &&  model.getLikelihood()->getBipartitionMonitor() && model.getLikelihood()->getFastFitchEvaluator());
				size_t nStepGESPR = state.getValues()[0];
				ptr = BlockModifier::createGuidedESPRBM(nStepGESPR, model.getLikelihood()->getTreeManager(), model.getLikelihood()->getBipartitionMonitor(), model.getLikelihood()->getFastFitchEvaluator(), model);

			}
			break;
		case BlockModifier::GUIDED_STNNI:
			{
				assert(model.getLikelihood()->getTreeManager() &&  model.getLikelihood()->getBipartitionMonitor() && model.getLikelihood()->getFastFitchEvaluator());
				size_t nStepGSTNNI= state.getValues()[0];
				ptr = BlockModifier::createGuidedSTNNIBM(nStepGSTNNI, model.getLikelihood()->getTreeManager(), model.getLikelihood()->getBipartitionMonitor(), model.getLikelihood()->getFastFitchEvaluator(), model);
			}
			break;
		case BlockModifier::DISCRETE_CATEGORIES:
			ptr = BlockModifier::createDiscreteCategoriesBM(state.getValues(), model);
			break;
		case BlockModifier::WEIGHTED_DISCRETE_CATEGORIES:
			ptr = BlockModifier::createWeightedDiscreteCategoriesBM(state.getValues(), model);
			break;
		case BlockModifier::INDEPENDENT_GAMMA:
			assert(state.getValues().size() == 2);
			ptr = BlockModifier::createGammaIndependentBM(state.getValues()[0], state.getValues()[1], model);
			break;
		case BlockModifier::INDEPENDENT_DIRICHLET:
			ptr = BlockModifier::createDirichletIndependentBM(state.getValues(), model);
			break;
		case BlockModifier::COEV_PROFILE_DIRICHLET:
			ptr = BlockModifier::createCoevProfDirichletBM(state.getValues(), model);
			break;
		case BlockModifier::INDEPENDENT_HP_DIRICHLET:
			ptr = BlockModifier::createHyperPriorDirichletIndepBM(state.getValues(), model);
			break;
		case BlockModifier::INDEPENDENT_NORMAL:
			ptr = BlockModifier::createNormalIndependentBM(state.getValues()[0], state.getValues()[1], model);
			break;
		case BlockModifier::COEV_POISSON_CONJUGATE_K:
			ptr = BlockModifier::createCoevPoissonConjugateBM(state.getValues()[0], state.getValues()[1], model);
			break;
		case BlockModifier::COEV_GAMMA_SHAPE_CONJUGATE_PRIOR:
			ptr = BlockModifier::createCoevGammaShapeConjugateBM(
					state.getValues()[0], state.getValues()[1], state.getValues()[2],
					state.getValues()[3], state.getValues()[4], state.getValues()[5],
					model);
			break;
		case BlockModifier::INDEPENDENT_BL:
			ptr = BlockModifier::createIndependentBranchLengthBM(state.getValues()[0], model.getLikelihood()->getTreeManager(), model.getLikelihood()->getBipartitionMonitor(), model);
			break;
		case BlockModifier::CENTERED_DIRICHLET:
			ptr = BlockModifier::createCenteredDirichletBM(false, model);
			break;
		default:
			std::cerr << "[Error] in BlockModifier::sharedPtr_t createBlockModifier(...);" << std::endl;
			std::cerr << "This BlockModifier (type=" << state.getBMType() << ") is not yet supported for serialization." << std::endl;
			assert(false);
	}

	return ptr;
}


void saveBMState ( BlockModifier::sharedPtr_t aBM,
				   State::BlockModifierState &state) {

	if(aBM == NULL) {
		state.setBMType(static_cast<size_t>(BlockModifier::NONE));
	} else {
		aBM->saveState(state);
	}
}




} /* namespace Support */
} /* namespace ParameterBlock */

