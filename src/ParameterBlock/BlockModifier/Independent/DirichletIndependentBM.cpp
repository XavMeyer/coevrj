//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DirichletIndependentBM.cpp
 *
 * @date May 24, 2017
 * @author meyerx
 * @brief
 */
#include "DirichletIndependentBM.h"

namespace ParameterBlock {

DirichletIndependentBM::DirichletIndependentBM(const std::vector<double> &aAlphas, Model &aModel) :
		BlockModifier(false, aModel), alphas(aAlphas) {

	dirichletPDF.reset(new Utils::Statistics::DirichletPDF(alphas));
}

DirichletIndependentBM::~DirichletIndependentBM() {
}

void DirichletIndependentBM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	assert(pInd.size() == alphas.size());
	std::vector<double> values(Parallel::mpiMgr().getPRNG()->genDirichlet(alphas));

	for(size_t iP=0; iP < pInd.size(); ++iP){
		double val = values[iP];
		sample.setDoubleParameter(pInd[iP], val);
	}
	BlockModifier::defaultReflection(pInd, sample);
	sample.setEvaluated(false);
}

double DirichletIndependentBM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	std::vector<double> values(alphas.size());
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		values[iP] =  toSample.getDoubleParameter(pInd[iP]);
	}
	double moveProb = dirichletPDF->computePDF(values);
	return moveProb;
}

double DirichletIndependentBM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {

	// Forward move (theta to theta')
	std::vector<double> values(alphas.size());
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		values[iP] = toSample.getDoubleParameter(pInd[iP]);
	}
	double moveProbFW = dirichletPDF->computePDF(values);

	// Backward move (theta' to theta)
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		values[iP] = fromSample.getDoubleParameter(pInd[iP]);
	}
	double moveProbBW = dirichletPDF->computePDF(values);

	return moveProbBW/moveProbFW;
}

void DirichletIndependentBM::setWindowSize(const vector<double> &aWinSize) {
	assert(aWinSize.size() > 0);
	alphas = aWinSize;
	dirichletPDF.reset(new Utils::Statistics::DirichletPDF(alphas));
}

std::vector<double> DirichletIndependentBM::getWindowSize() const {
	return alphas;
}

} /* namespace ParameterBlock */
