//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GammaIndependentBM.cpp
 *
 * @date May 20, 2017
 * @author meyerx
 * @brief
 */
#include "ParameterBlock/BlockModifier/Independent/GammaIndependentBM.h"

namespace ParameterBlock {

GammaIndependentBM::GammaIndependentBM(double aShape, double aScale, Model &aModel) :
		BlockModifier(false, aModel), shape(aShape), scale(aScale), distribution(shape, scale) {
}

GammaIndependentBM::~GammaIndependentBM() {
}

void GammaIndependentBM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	for(size_t iP=0; iP < pInd.size(); ++iP){
		double val = Parallel::mpiMgr().getPRNG()->genGamma(shape, scale);
		sample.setDoubleParameter(pInd[iP], val);
	}
	BlockModifier::defaultReflection(pInd, sample);
	sample.setEvaluated(false);
}

double GammaIndependentBM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	double moveProb = 1.0;
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		moveProb *= boost::math::pdf(distribution, toSample.getDoubleParameter(pInd[iP]));
	}

	return moveProb;
}

double GammaIndependentBM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {

	// Forward move (theta to theta')
	double moveProbFW = 1.0;
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		moveProbFW *= boost::math::pdf(distribution, toSample.getDoubleParameter(pInd[iP]));
	}

	// Backward move (theta' to theta)
	double moveProbBW = 1.0;
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		moveProbBW *= boost::math::pdf(distribution, fromSample.getDoubleParameter(pInd[iP]));
	}

	return moveProbBW/moveProbFW;
}

void GammaIndependentBM::setWindowSize(const vector<double> &aWinSize) {
	assert(aWinSize.size() == 2);
	shape = aWinSize[0];
	scale = aWinSize[1];

	boost::math::gamma_distribution<> newDist(shape, scale);
	distribution = newDist;
}

std::vector<double> GammaIndependentBM::getWindowSize() const {
	std::vector<double> windSize(2);
	windSize[0] = shape;
	windSize[1] = scale;
	return windSize;
}

} /* namespace ParameterBlock */
