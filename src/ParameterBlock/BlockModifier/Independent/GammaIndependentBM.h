//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GammaIndependentBM.h
 *
 * @date May 20, 2017
 * @author meyerx
 * @brief
 */
#ifndef GAMMAINDEPENDENTBM_H_
#define GAMMAINDEPENDENTBM_H_

#include <boost/math/distributions/gamma.hpp>
#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"

namespace ParameterBlock {

class GammaIndependentBM: public BlockModifier {
public:
	GammaIndependentBM(double aShape, double aScale, Model &aModel);
	~GammaIndependentBM();

	void updateParameters(const std::vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const std::vector<double> &aWinSize);
	std::vector<double> getWindowSize() const;

	bm_name_t getName() const {return INDEPENDENT_GAMMA;}

private:
	double shape, scale;
	boost::math::gamma_distribution<> distribution;

};

} /* namespace ParameterBlock */

#endif /* GAMMAINDEPENDENTBM_H_ */
