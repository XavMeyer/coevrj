//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchLengthIndepBM.h
 *
 * @date May 20, 2017
 * @author meyerx
 * @brief
 */
#ifndef BRANCHLENGTHINDEPBM_H_
#define BRANCHLENGTHINDEPBM_H_

#include <boost/math/distributions/gamma.hpp>
#include <boost/random/gamma_distribution.hpp>
#include <map>
#include <vector>

#include "BranchHelper.h"
#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"

namespace ParameterBlock {

class BranchLengthIndepBM: public BlockModifier {
public:
	BranchLengthIndepBM(size_t aN, TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBP, Model &aModel);
	~BranchLengthIndepBM();

	void updateParameters(const std::vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const std::vector<double> &aWinSize);
	std::vector<double> getWindowSize() const;

	bm_name_t getName() const {return INDEPENDENT_BL;}

	void moveAccepted(const bool isLocalProposal);
	void moveRejected(const bool isLocalProposal);
	void moveThrown(const bool isLocalProposal);

private:

	static const bool USE_MULTIPLIER_AS_DEFAULT;
	static const double DEFAULT_DISTRIBUTION_SHAPE, DEFAULT_DISTRIBUTION_SCALE;
	size_t N;

	mutable std::vector<size_t> nodesId;
	mutable std::vector<double> fwProb, bwProb;

	double lambda;
	boost::math::gamma_distribution<> defaultBLDistribution;

	TR::TreeManager::sharedPtr_t treeManager;
	TR::Bipartition::BipartitionMonitor::sharedPtr_t bipartitionMonitor;
	BranchHelper bh;

	/*struct info_t {
		TR::split_t split;
		std::vector<bool> history;
		std::vector<double> curVal, propVal;
	};


	mutable std::vector< info_t > lastSplits;

	typedef std::map<size_t, info_t> map_t;
	mutable map_t myMap;

	size_t getHash(TR::split_t &aSplit);*/

};

} /* namespace ParameterBlock */

#endif /* BRANCHLENGTHINDEPBM_H_ */
