//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GuidedESPRPM.cpp
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#include "GuidedESPRPM.h"

#include <assert.h>
#include <math.h>
#include <stddef.h>

#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/Move.h"
#include "cmath"

namespace StatisticalModel { class Model; }

namespace ParameterBlock {

const bool GuidedESPR_PM::SCALE_BRANCHES = false;
const double GuidedESPR_PM::LAMBDA = 2.*log(1.3);

GuidedESPR_PM::GuidedESPR_PM(const size_t aNStep, TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBP,
		MolecularEvolution::Parsimony::FastFitchEvaluator::sharedPtr_t aFFE, Model &aModel) :
		BlockModifier(false, aModel), nStep(aNStep), treeManager(aTM), bipartitionMonitor(aBP), bh(treeManager->getCurrentTree()),
		guidedESPR(bipartitionMonitor, &bh, aFFE) {
	m = 1.;
}

GuidedESPR_PM::~GuidedESPR_PM() {
}

void GuidedESPR_PM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	assert(pInd.size() == 1 && model.getParams().isInt(pInd.front()));

	long int cTreeH = sample.getIntParameter(pInd.front());
	TR::Tree::sharedPtr_t tree = treeManager->getTree(cTreeH);
	TR::Move::sharedPtr_t lastMove = guidedESPR.proposeNewTree(nStep, tree, sample);
	treeManager->setProposedMove(lastMove);
	sample.setIntParameter(pInd.front(), lastMove->getToH());

	if(SCALE_BRANCHES) {
		m = 1.0;
		 // Branch to explore and to graft
		m *= bh.scaleBranch(LAMBDA, lastMove->getInvolvedNode()[TR::ESPR::NODE_M1],
				lastMove->getInvolvedNode()[TR::ESPR::NODE_M2], tree, sample);
		// Branch to explore and "internal" (direction of movment) grafted branch
		m *= bh.scaleBranch(LAMBDA, lastMove->getInvolvedNode()[TR::ESPR::NODE_M2],
				lastMove->getInvolvedNode()[TR::ESPR::NODE_E1], tree, sample);
	}

	sample.setEvaluated(false);
}

double GuidedESPR_PM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && model.getParams().isInt(pInd.front()));

	assert(!SCALE_BRANCHES); // FIXME UNSURE HOW TO DEAL WITH BRANCH SCALING

	double prob = 0.0;

	assert(false);
	return prob;
}

double GuidedESPR_PM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && model.getParams().isInt(pInd.front()));

	double prob = treeManager->getProposedMove()->getProbability();

	if(SCALE_BRANCHES) {
		prob *= m;
	}

	return prob;
}

void GuidedESPR_PM::setWindowSize(const std::vector<double> &aWinSize) {
	nStep = aWinSize[0];
}

std::vector<double> GuidedESPR_PM::getWindowSize() const {
	return std::vector<double>(1, nStep);
}

void GuidedESPR_PM::moveAccepted(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->acceptProposedMove();
	} else {
		treeManager->acceptRemoteMove();
	}
}

void GuidedESPR_PM::moveRejected(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

void GuidedESPR_PM::moveThrown(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

} /* namespace ParameterBlock */
