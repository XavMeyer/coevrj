//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchLengthIndepBM.cpp
 *
 * @date May 20, 2017
 * @author meyerx
 * @brief
 */
#include "ParameterBlock/BlockModifier/TreeModifier/BranchLengthIndepBM.h"

#include <algorithm>

namespace ParameterBlock {

const bool BranchLengthIndepBM::USE_MULTIPLIER_AS_DEFAULT = true;
const double BranchLengthIndepBM::DEFAULT_DISTRIBUTION_SHAPE = 1.5;
const double BranchLengthIndepBM::DEFAULT_DISTRIBUTION_SCALE = 0.25;


BranchLengthIndepBM::BranchLengthIndepBM(size_t aN, TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBP, Model &aModel) :
		BlockModifier(false, aModel), N(aN), defaultBLDistribution(DEFAULT_DISTRIBUTION_SHAPE, DEFAULT_DISTRIBUTION_SCALE),
		treeManager(aTM), bipartitionMonitor(aBP), bh(treeManager->getCurrentTree()) {


	TR::Tree::sharedPtr_t tree = treeManager->getCurrentTree();

	std::vector<TR::TreeNode*>& terminals = tree->getTerminals();
	for(size_t i=0; i<terminals.size(); ++i) {
		nodesId.push_back(terminals[i]->getId());
	}

	std::vector<TR::TreeNode*>& internals = tree->getInternals();
	for(size_t i=0; i<internals.size(); ++i) {
		if(internals[i] != tree->getRoot()) {
			nodesId.push_back(internals[i]->getId());
		}
	}
	assert(N <= nodesId.size());

	fwProb.resize(N, 0.);
	bwProb.resize(N, 0.);

	lambda = 1.3;

}

BranchLengthIndepBM::~BranchLengthIndepBM() {

	/*map_t::iterator itMap;
	for(itMap = myMap.begin(); itMap != myMap.end(); ++itMap) {
		size_t count = itMap->second.history.size();
		size_t countTrue = 0;
		for(size_t i=0; i<count; ++i) countTrue = itMap->second.history[i] ? countTrue+1 : countTrue;

		double acceptance = (double)countTrue/(double)count;
		//std::cout << bipartitionMonitor->getSplitAsString(itMap->second.split) << "\t" << count << "\t" << acceptance << std::endl;

	}*/

}

void BranchLengthIndepBM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {

	TR::Tree::sharedPtr_t tree = treeManager->getCurrentTree();

	std::random_shuffle(nodesId.begin(), nodesId.end());

	//lastSplits.clear();

	for(size_t iP=0; iP < N; ++iP){

		// Chose a branch
		TR::TreeNode* node1 = tree->getNode(nodesId[iP]);
		TR::TreeNode* node2 = node1->getParent();

		double oldVal = bh.getBranchLength(node1, node2, sample);

		TR::split_t split = tree->findBipartitions(node1, node2);
		if(split[0]) split.flip();

		size_t count;
		double meanBL, varBL;
		double hasStats = bipartitionMonitor->getSplitsStatsBL(split, count, meanBL, varBL);

		double newVal = 0.;
		if(hasStats) {
			/*if(count < 500) {
				std::cout << (1.+(500.-count)/1000.) << std::endl;
				varBL = (1.+(500.-count)/1000.)*varBL;
			}*/

			double shape = meanBL*meanBL/varBL;
			double scale = varBL/meanBL;
			newVal = Parallel::mpiMgr().getPRNG()->genGamma(shape, scale);

			/*if(bipartitionMonitor->getSplitAsString(split) == "........................**......" && false) {
				std::cout << std::scientific << oldVal << "\t" << newVal << "\t" << shape << "\t" << scale << std::endl;
			}*/

			try {
				boost::math::gamma_distribution<> distr(shape, scale);
				fwProb[iP] = boost::math::pdf(distr, newVal);
				bwProb[iP] = boost::math::pdf(distr, oldVal);
			} catch(std::overflow_error &err) {
				std::cout << "Gamma overflow error (1) in BranchLengthIndepBM : shape = " << shape << ", scale = " << scale << ", oldVal = " << oldVal << ", newVal = " << newVal << std::endl;
				std::cerr << "Gamma overflow error (1) in BranchLengthIndepBM : shape = " << shape << ", scale = " << scale << ", oldVal = " << oldVal << ", newVal = " << newVal << std::endl;
				fwProb[iP] = 0.;
				bwProb[iP] = 0.;
			}

		} else { // Do multiplier

			if(!USE_MULTIPLIER_AS_DEFAULT) {
				newVal = Parallel::mpiMgr().getPRNG()->genGamma(DEFAULT_DISTRIBUTION_SHAPE, DEFAULT_DISTRIBUTION_SCALE);

				try {
					fwProb[iP] = boost::math::pdf(defaultBLDistribution, newVal);
					bwProb[iP] = boost::math::pdf(defaultBLDistribution, oldVal);
				} catch(std::overflow_error &err) {
					std::cout << "Gamma overflow error (2) in BranchLengthIndepBM : shape = " << DEFAULT_DISTRIBUTION_SHAPE << ", scale = " << DEFAULT_DISTRIBUTION_SCALE << ", oldVal = " << oldVal << ", newVal = " << newVal << std::endl;
					std::cerr << "Gamma overflow error (2) in BranchLengthIndepBM : shape = " << DEFAULT_DISTRIBUTION_SHAPE << ", scale = " << DEFAULT_DISTRIBUTION_SCALE << ", oldVal = " << oldVal << ", newVal = " << newVal << std::endl;
					fwProb[iP] = 0.;
					bwProb[iP] = 0.;
				}
			} else {


				double u = Parallel::mpiMgr().getPRNG()->genUniformDbl()-0.5;
				double m = exp(lambda*u);

				newVal = m*bh.getBranchLength(node1, node2, sample);

				fwProb[iP] = 1.;
				bwProb[iP] = m;

			}
			/*if(bipartitionMonitor->getSplitAsString(split) == "........................**......" && false) {
				std::cout << "Default distr" << std::endl;
			}*/
		}

		bh.setBranchLength(newVal, node1, node2, sample);


		/*info_t info;
		info.split = split;
		info.curVal.push_back(oldVal);
		info.propVal.push_back(newVal);

		lastSplits.push_back(info);*/


	}

	sample.setEvaluated(false);

}

double BranchLengthIndepBM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	double moveProb = 1.0;

	assert(!USE_MULTIPLIER_AS_DEFAULT);

	TR::Tree::sharedPtr_t tree = treeManager->getCurrentTree();

	for(size_t iP=0; iP < pInd.size(); ++iP){

		// Chose a branch
		TR::TreeNode* node1 = tree->getNode(nodesId[iP]);
		TR::TreeNode* node2 = node1->getParent();

		double oldVal = bh.getBranchLength(node1, node2, fromSample);
		double newVal = bh.getBranchLength(node1, node2, toSample);
		if(oldVal == newVal) continue;


		TR::split_t split = tree->findBipartitions(node1, node2);
		if(split[0]) split.flip();

		size_t count;
		double meanBL, varBL;
		double hasStats = bipartitionMonitor->getSplitsStatsBL(split, count, meanBL, varBL);

		if(hasStats) {

			/*if(count < 500) {
				std::cout << (1.+(500.-count)/1000.) << std::endl;
				varBL = (1.+(500.-count)/1000.)*varBL;
			}*/

			double shape = meanBL*meanBL/varBL;
			double scale = varBL/meanBL;

			boost::math::gamma_distribution<> distr(shape, scale);
			moveProb *= boost::math::pdf(distr, newVal);

		} else {
			moveProb *= boost::math::pdf(defaultBLDistribution, newVal);
		}
	}

	return moveProb;
}

double BranchLengthIndepBM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {

	// Forward move (theta to theta')
	double moveRatio = 1.0;

	for(size_t iP=0; iP < N; ++iP) {
		moveRatio *= (bwProb[iP]/fwProb[iP]);
	}

	return moveRatio;
}

void BranchLengthIndepBM::setWindowSize(const vector<double> &aWinSize) {
	N = aWinSize.front();
	assert(N <= nodesId.size());

	fwProb.resize(N, 0.);
	bwProb.resize(N, 0.);

	// Randomize the ordering of the branches
	//std::random_shuffle(nodesId.begin(), nodesId.end());
}

std::vector<double> BranchLengthIndepBM::getWindowSize() const {
	std::vector<double> windSize(1, N);
	return windSize;
}



/*size_t BranchLengthIndepBM::getHash(TR::split_t &aSplit) {
	assert(aSplit.size() < 64);
	size_t val = 0;
	for(size_t i=0; i<aSplit.size(); ++i) {
		if(aSplit[i]) val += 1<< i;
	}

	return val;
}*/

void BranchLengthIndepBM::moveAccepted(const bool isLocalProposal) {

	/*for(size_t i=0; i<lastSplits.size(); ++i) {
		size_t hash = getHash(lastSplits[i].split);
		map_t::iterator itMap = myMap.find(hash);
		if(itMap == myMap.end()) {
			lastSplits[i].history.push_back(true);
			myMap.insert(std::make_pair(hash, lastSplits[i]));
		} else {
			itMap->second.history.push_back(true);
			itMap->second.curVal.push_back(lastSplits[i].curVal.back());
			itMap->second.propVal.push_back(lastSplits[i].propVal.back());
		}
	}*/

}

void BranchLengthIndepBM::moveRejected(const bool isLocalProposal) {

	/*for(size_t i=0; i<lastSplits.size(); ++i) {
		size_t hash = getHash(lastSplits[i].split);
		map_t::iterator itMap = myMap.find(hash);
		if(itMap == myMap.end()) {
			lastSplits[i].history.push_back(false);
			myMap.insert(std::make_pair(hash, lastSplits[i]));
		} else {
			itMap->second.history.push_back(false);
			itMap->second.curVal.push_back(lastSplits[i].curVal.back());
			itMap->second.propVal.push_back(lastSplits[i].propVal.back());
		}
	}*/

}


void BranchLengthIndepBM::moveThrown(const bool isLocalProposal) {

}


} /* namespace ParameterBlock */
