//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ESPRPM.cpp
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#include "ETBRBM.h"

#include <assert.h>
#include <math.h>
#include <stddef.h>

#include "ParameterBlock/BlockModifier//BlockModifierInterface.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/Move.h"
#include "cmath"

namespace StatisticalModel { class Model; }

namespace ParameterBlock {

const bool ETBR_BM::SCALE_BRANCHES = false;
const double ETBR_BM::LAMBDA = 2.*log(1.3);

ETBR_BM::ETBR_BM(const double aP, TR::TreeManager::sharedPtr_t aTM, Model &aModel) :
		BlockModifier(false, aModel), p(aP), treeManager(aTM), bh(treeManager->getCurrentTree()), etbr(&bh) {
	m = 1.;
}

ETBR_BM::~ETBR_BM() {
}

void ETBR_BM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	assert(pInd.size() == 1 && model.getParams().isInt(pInd.front()));

	long int cTreeH = sample.getIntParameter(pInd.front());
	TR::Tree::sharedPtr_t tree = treeManager->getTree(cTreeH);
	TR::Move::sharedPtr_t lastMove = etbr.proposeNewTree(p, tree, sample);
	treeManager->setProposedMove(lastMove);
	//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] ESPR propose -> : " << lastMove->getFromH() << " to " << lastMove->getToH() << std::endl; // FIXME
	//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] ESPR curTree -> : " << tree->getHashKey() << std::endl; // FIXME
	sample.setIntParameter(pInd.front(), lastMove->getToH());

	if(SCALE_BRANCHES) {

		TR::ETBR::unpackedMoveId_t moveNodesId = TR::ETBR::unpackMove(lastMove->getInvolvedNode());

		m = 1.0;
		 // Branch to explore and to graft
		m *= bh.scaleBranch(LAMBDA, moveNodesId.edgeN1, moveNodesId.edgeN2, tree, sample);
		// Branch to explore and "internal" (direction of movment) grafted branch
		//m *= bh.scaleBranch(LAMBDA, moveNodesId.edgeN2, moveNodesId.graftE1N1, tree, sample);
		//m *= bh.scaleBranch(LAMBDA, moveNodesId.edgeN1, moveNodesId.graftE2N1, tree, sample);

	}

	sample.setEvaluated(false);
}

double ETBR_BM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && model.getParams().isInt(pInd.front()));

	//size_t nEdge = treeManager->getProposedMove()->getNEdgeDistance();
	//TR::ESPR::moveInfoESPR_t moveInfo = static_cast<TR::ESPR::moveInfoESPR_t>(treeManager->getProposedMove()->getMoveInfo());
	assert(!SCALE_BRANCHES); // FIXME UNSURE HOW TO DEAL WITH BRANCH SCALING

	double prob = 0.;
	return prob;
}

double ETBR_BM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && model.getParams().isInt(pInd.front()));

	double prob = treeManager->getProposedMove()->getProbability();

	if(SCALE_BRANCHES) {
		prob *= m;
	}

	return prob;
}

void ETBR_BM::setWindowSize(const std::vector<double> &aWinSize) {
	p = aWinSize[0];
}

std::vector<double> ETBR_BM::getWindowSize() const {
	return std::vector<double>(1, p);
}

void ETBR_BM::moveAccepted(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->acceptProposedMove();
	} else {
		treeManager->acceptRemoteMove();
	}
}

void ETBR_BM::moveRejected(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

void ETBR_BM::moveThrown(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

} /* namespace ParameterBlock */
