//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveTBRBM.cpp
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#include "AdaptivePBJBM.h"

#include <assert.h>
#include <math.h>
#include <stddef.h>

#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/Move.h"
#include "cmath"

namespace StatisticalModel { class Model; }

namespace ParameterBlock {

const bool AdaptivePBJ_BM::SCALE_BRANCHES = false;
const double AdaptivePBJ_BM::LAMBDA = 2.*log(1.3);

AdaptivePBJ_BM::AdaptivePBJ_BM(TR::AdaptivePBJ::moveType_t aMoveType, const double aP, double aEpsilonFreq, double aPowerFreq,
		TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBP, Model &aModel) :
		BlockModifier(false, aModel), treeManager(aTM), bipartitionMonitor(aBP), moveType(aMoveType),
		pe(aP), epsilonFreq(aEpsilonFreq), powerFreq(aPowerFreq),
		bh(treeManager->getCurrentTree()), etbr(aMoveType, bipartitionMonitor, &bh) {
	m = 1.;
	//espr.setBipartitionMonitor(bipartitionMonitor);
}

AdaptivePBJ_BM::~AdaptivePBJ_BM() {
}

void AdaptivePBJ_BM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	assert(pInd.size() == 1 && model.getParams().isInt(pInd.front()));


	/*std::cout << std::scientific;
	double initialTotalBL = bh.getTotalBranchLength(sample);*/

	long int cTreeH = sample.getIntParameter(pInd.front());
	TR::Tree::sharedPtr_t tree = treeManager->getTree(cTreeH);

	//size_t nBL = tree->getInternals().size() + tree->getTerminals().size() - 1;
	//size_t offset = sample.getDblValues().size() - nBL;
	//std::vector<double> BL(sample.getDblValues().begin()+offset, sample.getDblValues().end());
	//std::cout << tree->getNameAndBranchLengthString(treeManager->getOrderedNames(), BL) << std::endl;

	TR::Move::sharedPtr_t lastMove = etbr.proposeNewTree(pe, epsilonFreq, powerFreq, tree, sample);
	treeManager->setProposedMove(lastMove);
	//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] ESPR propose -> : " << lastMove->getFromH() << " to " << lastMove->getToH() << std::endl; // FIXME
	//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] ESPR curTree -> : " << tree->getHashKey() << std::endl; // FIXME
	sample.setIntParameter(pInd.front(), lastMove->getToH());



	/*if(lastMove->getProbability() < 0.) {

		std::cout << tree->getNameAndBranchLengthString(treeManager->getOrderedNames(), BL) << std::endl;
		getchar();
	}
	std::cout <<"---------------------------------------" << std::endl;*/


	/*double updatedTotalBL = bh.getTotalBranchLength(sample);
	if(fabs(updatedTotalBL-initialTotalBL) > 0 ) {
		std::cout << initialTotalBL << "\t" << updatedTotalBL << "\t" << updatedTotalBL-initialTotalBL << std::endl;
	}
	assert(fabs(initialTotalBL - updatedTotalBL) < 1e-7);*/


	if(SCALE_BRANCHES) {
		m = 1.0;
		 // Branch to explore and to graft
		m *= bh.scaleBranch(LAMBDA, lastMove->getInvolvedNode()[TR::ESPR::NODE_M1],
				lastMove->getInvolvedNode()[TR::ESPR::NODE_M2], tree, sample);
		// Branch to explore and "internal" (direction of movment) grafted branch
		m *= bh.scaleBranch(LAMBDA, lastMove->getInvolvedNode()[TR::ESPR::NODE_M2],
				lastMove->getInvolvedNode()[TR::ESPR::NODE_E1], tree, sample);
	}

	sample.setEvaluated(false);
}

double AdaptivePBJ_BM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && model.getParams().isInt(pInd.front()));

	size_t nEdge = treeManager->getProposedMove()->getNEdgeDistance();
	TR::ESPR::moveInfoESPR_t moveInfo = static_cast<TR::ESPR::moveInfoESPR_t>(treeManager->getProposedMove()->getMoveInfo());
	assert(!SCALE_BRANCHES); // FIXME UNSURE HOW TO DEAL WITH BRANCH SCALING

	double prob = 0.0;
	/*if(moveInfo == TR::ESPR::BOTH_CONSTRAINED) {
		prob = pow(0.5*p, (double)nEdge);
	} else if(moveInfo == TR::ESPR::BOTH_UNCONSTRAINED) {
		prob = pow(0.5*p, (double)nEdge)*(1-p);
	} else if(moveInfo == TR::ESPR::CONSTRAINED_TO_UNCONSTRAINED) {
		prob = pow(0.5*p, (double)nEdge);
	} else if(moveInfo == TR::ESPR::UNCONSTRAINED_TO_CONSTRAINED) {
		prob = pow(0.5*p, (double)nEdge)*(1-p);
	}*/
	assert(false);
	return prob;
}

double AdaptivePBJ_BM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && model.getParams().isInt(pInd.front()));

	double prob = treeManager->getProposedMove()->getProbability();
	//std::cout << prob << std::endl;


	if(SCALE_BRANCHES) {
		prob *= m;
	}

	return prob;
}

void AdaptivePBJ_BM::setWindowSize(const std::vector<double> &aWinSize) {
	moveType = static_cast<TR::AdaptivePBJ::moveType_t>(aWinSize[0]);
	pe = aWinSize[1];
	epsilonFreq = aWinSize[2];
	powerFreq = aWinSize[3];
}

std::vector<double> AdaptivePBJ_BM::getWindowSize() const {
	std::vector<double> params;
	params.push_back(moveType);
	params.push_back(pe);
	params.push_back(epsilonFreq);
	params.push_back(powerFreq);

	return params;
}

void AdaptivePBJ_BM::moveAccepted(const bool isLocalProposal) {
	//std::cout << ", 1" << std::endl;
	/*std::vector<size_t> edgesDist = treeManager->getProposedMove()->getNEdgeDistances();
	std::cout << "Acc edges : ";
	for(size_t i=0; i<edgesDist.size(); ++i) std::cout << edgesDist[i] << "\t";
	std::cout << std::endl;*/

	if(isLocalProposal) { // I created this move
		treeManager->acceptProposedMove();
	} else {
		treeManager->acceptRemoteMove();
	}
}

void AdaptivePBJ_BM::moveRejected(const bool isLocalProposal) {
	//std::cout << ", 0" << std::endl;
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

void AdaptivePBJ_BM::moveThrown(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

} /* namespace ParameterBlock */
