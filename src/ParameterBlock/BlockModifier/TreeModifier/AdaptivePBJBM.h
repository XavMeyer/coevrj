//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ESPRPM.h
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVE_ETBRBM_H_
#define ADAPTIVE_ETBRBM_H_

#include "../BlockModifierInterface.h"
#include "BranchHelper.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/AdaptivePBJ.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeManager.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Bipartition/BipartitionMonitor.h"

namespace StatisticalModel { class Model; }

namespace ParameterBlock {


namespace TR = ::MolecularEvolution::TreeReconstruction;


class AdaptivePBJ_BM: public BlockModifier {
public:
	AdaptivePBJ_BM(TR::AdaptivePBJ::moveType_t aMoveType, const double aP, double aEpsilonFreq, double aPowerFreq,
			TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBP,  Model &aModel);
	~AdaptivePBJ_BM();

	void updateParameters(const std::vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const std::vector<double> &aWinSize);
	std::vector<double> getWindowSize() const;

	void moveAccepted(const bool isLocalProposal);
	void moveRejected(const bool isLocalProposal);
	void moveThrown(const bool isLocalProposal);

	bm_name_t getName() const {return ADAPTIVE_PBJ;}

private:
	static const bool SCALE_BRANCHES;
	static const double LAMBDA;

	mutable double m;
	TR::TreeManager::sharedPtr_t treeManager;
	TR::Bipartition::BipartitionMonitor::sharedPtr_t bipartitionMonitor;

	TR::AdaptivePBJ::moveType_t moveType;
	double pe;
	double epsilonFreq;
	double powerFreq;
	BranchHelper bh;
	mutable TR::AdaptivePBJ etbr;


};

} /* namespace ParameterBlock */

#endif /* ADAPTIVE_ETBRBM_H_ */
