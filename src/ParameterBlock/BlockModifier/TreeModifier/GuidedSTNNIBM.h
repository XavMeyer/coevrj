//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GuidedESPRPM.h
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#ifndef GuidedSTNNI_H_
#define GuidedSTNNI_H_

#include "../../../Utils/MolecularEvolution/TreeReconstruction/Move/AdaptiveGuidedSTNNI.h"
#include "../BlockModifierInterface.h"
#include "BranchHelper.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/GuidedSTNNI.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeManager.h"

namespace StatisticalModel { class Model; }

namespace ParameterBlock {

class GuidedSTNNI_PM: public BlockModifier {
public:
	GuidedSTNNI_PM(const size_t aNStep, TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBP,
				  MolecularEvolution::Parsimony::FastFitchEvaluator::sharedPtr_t aFFE, Model &aModel);
	~GuidedSTNNI_PM();

	void updateParameters(const std::vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const std::vector<double> &aWinSize);
	std::vector<double> getWindowSize() const;

	void moveAccepted(const bool isLocalProposal);
	void moveRejected(const bool isLocalProposal);
	void moveThrown(const bool isLocalProposal);

	bm_name_t getName() const {return GUIDED_STNNI;}

private:
	static const bool SCALE_BRANCHES;
	static const double LAMBDA;

	size_t nStep;
	mutable double m;
	TR::TreeManager::sharedPtr_t treeManager;
	TR::Bipartition::BipartitionMonitor::sharedPtr_t bipartitionMonitor;

	BranchHelper bh;
	mutable TR::GuidedSTNNI guidedSTNNI;
};

} /* namespace ParameterBlock */

#endif /* GuidedSTNNI_H_ */
