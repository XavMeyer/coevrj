//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevPoissonConjugatePriorKBM.cpp
 *
 * @date May 29, 2017
 * @author meyerx
 * @brief
 */

#include "ParameterBlock/BlockModifier/ModelSpecific/CoevPoissonConjugatePriorKBM.h"

#include <limits>
#include <boost/assign/list_of.hpp>
#include <boost/math/distributions/gamma.hpp>

#include "Model/Likelihood/CoevRJ/Base.h"

namespace ParameterBlock {

CoevPoissonConjugatePriorKBM::CoevPoissonConjugatePriorKBM(double aAlpha, double aBeta, Model &aModel) :
		BlockModifier(false, aModel), alpha(aAlpha), beta(aBeta) {

	ptrLik = dynamic_cast<StatisticalModel::Likelihood::CoevRJ::Base*>(aModel.getLikelihood().get());
	assert(ptrLik != NULL && "This move only works with CoevRJ likelihood.");
	assert(!ptrLik->isUsingFixedPairsStrategy() && "This move only works with CoevRJ likelihood without the fixed pairs strategy.");
}

CoevPoissonConjugatePriorKBM::~CoevPoissonConjugatePriorKBM() {
}

void CoevPoissonConjugatePriorKBM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	double k = ptrLik->getNActiveCoevLikelihood();
	assert(pInd.size() == 1);

	// See Conjugate prior of poisson in "Compendium of Conjugate priors" by Daniel Fink
	double alphaPrime = alpha + k;
	double betaPrime = beta+1.;
	double lambdaFromPosterior = Parallel::mpiMgr().getPRNG()->genGamma(alphaPrime, 1./betaPrime);
	sample.setDoubleParameter(pInd.front(), lambdaFromPosterior);
	sample.setEvaluated(false);
}

double CoevPoissonConjugatePriorKBM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	boost::math::gamma_distribution<> gammaDistr(alpha, 1./beta);
	return boost::math::pdf(gammaDistr, toSample.getDoubleParameter(pInd.front()));
}

double CoevPoissonConjugatePriorKBM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	// This is a Gibbs sampling move. This trick force the acceptance check to go through
	return std::numeric_limits<double>::infinity(); // TODO Implement special Gibbs proposal
}

void CoevPoissonConjugatePriorKBM::setWindowSize(const std::vector<double> &aWinSize) {
	assert(aWinSize.size() == 2);
	alpha =	aWinSize[0];
	beta = aWinSize[1];
}

std::vector<double> CoevPoissonConjugatePriorKBM::getWindowSize() const {
	std::vector<double> winSize = boost::assign::list_of(alpha)(beta);
	return winSize;
}


} /* namespace ParameterBlock */
