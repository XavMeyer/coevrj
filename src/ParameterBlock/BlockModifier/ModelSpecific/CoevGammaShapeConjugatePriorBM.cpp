//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevGammaShapeConjugatePriorBM.cpp
 *
 * @date Jun 2, 2017
 * @author meyerx
 * @brief
 */
#include "CoevGammaShapeConjugatePriorBM.h"

#include "Model/Likelihood/CoevRJ/Base.h"
#include "Parallel/Manager/MpiManager.h"

namespace ParameterBlock {

CoevGammaShapeConjugatePriorBM::CoevGammaShapeConjugatePriorBM(double aKnownAlpha_R, double aAlpha0_R, double aBeta0_R,
															   double aKnownAlpha_D, double aAlpha0_D, double aBeta0_D,
															   Model &aModel) :
	BlockModifier(false, aModel),
	knownAlpha_R(aKnownAlpha_R), alpha0_R(aAlpha0_R), beta0_R(aBeta0_R),
	knownAlpha_D(aKnownAlpha_D), alpha0_D(aAlpha0_D), beta0_D(aBeta0_D),
	gammaCJ_R(new Utils::Statistics::GammaKnownShapeConjugatePrior(knownAlpha_R, alpha0_R, beta0_R)),
	gammaCJ_D(new Utils::Statistics::GammaKnownShapeConjugatePrior(knownAlpha_D, alpha0_D, beta0_D)) {

	ptrLik = dynamic_cast<StatisticalModel::Likelihood::CoevRJ::Base*>(aModel.getLikelihood().get());
	assert(ptrLik != NULL && "This move only works with CoevRJ likelihood.");
	assert((ptrLik->isUsingCoevScaling() && ptrLik->getCoevScalingPriorType() == StatisticalModel::Likelihood::CoevRJ::GAMMA2_HP)
			&& "This move only works with CoevRJ likelihood with coev scaling and GAMMA2_HP (coevPriorType=5).");
}

CoevGammaShapeConjugatePriorBM::~CoevGammaShapeConjugatePriorBM() {
}

void CoevGammaShapeConjugatePriorBM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {

	std::vector<double> valuesR, valuesD;
	typedef StatisticalModel::Likelihood::CoevRJ::itListPCLI_t itLCLI_t;
	for(itLCLI_t it=ptrLik->getActivePermCoevLikelihoods().begin(); it != ptrLik->getActivePermCoevLikelihoods().end(); ++it) {
		// Getting the model specific sample
		const Sampler::ModelSpecificSample& mss = sample.getMSS((*it)->getUIDSubSpace());

		// Parameters are r1, r2, s, d
		assert(mss.getDblValues().size() == 4);
		double r1 = mss.getDblValues()[0];
		double r2 = mss.getDblValues()[1];
		//double s = mss.getDblValues()[2];
		double d = mss.getDblValues()[3];

		valuesR.push_back(r1);
		valuesR.push_back(r2);

		valuesD.push_back(d);
	}

	double betaRFromPosterior = 1./gammaCJ_R->drawFromPosterior(Parallel::mpiMgr().getPRNG(), valuesR); // rate to scale
	double betaDFromPosterior = 1./gammaCJ_D->drawFromPosterior(Parallel::mpiMgr().getPRNG(), valuesD); // rate to scale

	assert(pInd.size() == 2);
	sample.setDoubleParameter(pInd[0], betaRFromPosterior);
	sample.setDoubleParameter(pInd[1], betaDFromPosterior);
	sample.setEvaluated(false);
}

double CoevGammaShapeConjugatePriorBM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 2);
	double pdfR = gammaCJ_R->computePDF(1./toSample.getDoubleParameter(pInd[0]));
	double pdfD = gammaCJ_D->computePDF(1./toSample.getDoubleParameter(pInd[1]));

	return pdfR * pdfD;
}

double CoevGammaShapeConjugatePriorBM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	// This is a Gibbs sampling move. This trick force the acceptance check to go through
	return std::numeric_limits<double>::infinity(); // TODO Implement special Gibbs proposal
}

void CoevGammaShapeConjugatePriorBM::setWindowSize(const std::vector<double> &aWinSize) {
	assert(aWinSize.size() == 6);
	knownAlpha_R = aWinSize[0];
	alpha0_R = aWinSize[1];
	beta0_R = aWinSize[2];
	knownAlpha_D = aWinSize[3];
	alpha0_D = aWinSize[4];
	beta0_D = aWinSize[5];

	gammaCJ_R.reset(new Utils::Statistics::GammaKnownShapeConjugatePrior(knownAlpha_R, alpha0_R, beta0_R));
	gammaCJ_D.reset(new Utils::Statistics::GammaKnownShapeConjugatePrior(knownAlpha_D, alpha0_D, beta0_D));

}


std::vector<double> CoevGammaShapeConjugatePriorBM::getWindowSize() const {
	std::vector<double> winSize = boost::assign::list_of(knownAlpha_R)(alpha0_R)(beta0_R)(knownAlpha_D)(alpha0_D)(beta0_D);
	return winSize;
}


} /* namespace ParameterBlock */
