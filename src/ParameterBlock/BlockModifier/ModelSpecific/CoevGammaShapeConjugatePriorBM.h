//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevGammaShapeConjugatePriorBM.h
 *
 * @date Jun 2, 2017
 * @author meyerx
 * @brief Conjugate prior for the gamma distribution with known shape
 */
#ifndef COEVGAMMASHAPECONJUGATEPRIORBM_H_
#define COEVGAMMASHAPECONJUGATEPRIORBM_H_

#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"
#include "Utils/Statistics/ConjugatePrior/GammaKnownShapeConjugatePrior.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {
	class Base;
}
}
}

namespace ParameterBlock {

class CoevGammaShapeConjugatePriorBM: public ParameterBlock::BlockModifier {
public:
	CoevGammaShapeConjugatePriorBM(double aKnownAlpha_R, double aAlpha0_R, double aBeta0_R,
								   double aKnownAlpha_D, double aAlpha0_D, double aBeta0_D,
								   Model &aModel);
	~CoevGammaShapeConjugatePriorBM();

	void updateParameters(const std::vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const std::vector<double> &aWinSize);
	std::vector<double> getWindowSize() const;

	bm_name_t getName() const {return COEV_GAMMA_SHAPE_CONJUGATE_PRIOR;}

private:
	double knownAlpha_R, alpha0_R, beta0_R, knownAlpha_D, alpha0_D, beta0_D;
	StatisticalModel::Likelihood::CoevRJ::Base* ptrLik;
	Utils::Statistics::GammaKnownShapeConjugatePrior::sharedPtr_t gammaCJ_R, gammaCJ_D;
};

} /* namespace ParameterBlock */

#endif /* COEVGAMMASHAPECONJUGATEPRIORBM_H_ */
