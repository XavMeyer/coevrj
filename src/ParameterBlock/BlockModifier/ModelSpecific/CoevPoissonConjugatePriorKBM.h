//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevPoissonConjugatePriorKBM.h
 *
 * @date May 29, 2017
 * @author meyerx
 * @brief Conjugate prior for the poisson distribution
 *  See Conjugate prior of poisson in "Compendium of Conjugate priors" by Daniel Fink
 */
#ifndef COEVPOISSONCONJUGATEPRIORKBM_H_
#define COEVPOISSONCONJUGATEPRIORKBM_H_

#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {
	class Base;
}
}
}

namespace ParameterBlock {

class CoevPoissonConjugatePriorKBM: public BlockModifier {
public:
	CoevPoissonConjugatePriorKBM(double aAlpha, double aBeta, Model &aModel);
	~CoevPoissonConjugatePriorKBM();

	void updateParameters(const std::vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const std::vector<double> &aWinSize);
	std::vector<double> getWindowSize() const;

	bm_name_t getName() const {return COEV_POISSON_CONJUGATE_K;}

private:
	double alpha, beta;
	StatisticalModel::Likelihood::CoevRJ::Base* ptrLik;

};

} /* namespace ParameterBlock */

#endif /* COEVPOISSONCONJUGATEPRIORKBM_H_ */
