//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BlockModificator.cpp
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */


#include "BlockModifierInterface.h"

#include <sys/types.h>

#include "Model/Model.h"
#include "Model/Parameter/Parameters.h"
#include "MultiplierWindowPM.h"
#include "Sampler/Samples/Sample.h"

#include "ParameterBlock/BlockModifier/BlockModifierState.h"
#include "ParameterBlock/BlockModifier/EllipsoidReflection.h"

#include "GaussianWindowPM.h"
#include "MVGaussianWindowPM.h"
#include "UniformWindowPM.h"
#include "PCAWindowPM.h"

#include "TreeModifier/ESPRPM.h"
#include "TreeModifier/ETBRBM.h"
#include "TreeModifier/AdaptiveESPRPM.h"
#include "TreeModifier/AdaptivePBJBM.h"
#include "TreeModifier/GuidedESPRPM.h"
#include "TreeModifier/STNNIPM.h"
#include "TreeModifier/BranchLengthIndepBM.h"

#include "Discrete/DiscreteCategoriesBM.h"
#include "Discrete/WeightedDiscreteCategoriesBM.h"

#include "Independent/GammaIndependentBM.h"
#include "Independent/DirichletIndependentBM.h"
#include "Independent/HyperPriorDirichletIndepBM.h"
#include "Independent/NormalIndependentBM.h"

#include "ModelSpecific/CoevProfDirichletBM.h"
#include "ModelSpecific/CoevPoissonConjugatePriorKBM.h"
#include "ModelSpecific/CoevGammaShapeConjugatePriorBM.h"
#include "Simplex/CenteredDirichletBM.h"

#include "TreeModifier/GuidedESPRPM.h"
#include "TreeModifier/GuidedSTNNIBM.h"


namespace ParameterBlock {

BlockModifier::BlockModifier(const bool aSymmetric, Model &aModel) : symmetric(aSymmetric), model(aModel) {

}

BlockModifier::~BlockModifier() {
}

void BlockModifier::saveState(State::BlockModifierState &state) const {
	state.bmType = static_cast<size_t>(getName());
	state.values = getWindowSize();
}

BlockModifier::sharedPtr_t BlockModifier::createUniformWindow(const double aWinSize, Model &aModel){
	return sharedPtr_t(new UniformWindowPM(aWinSize, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createGaussianWindow(const double aSigma, Model &aModel){
	return sharedPtr_t(new GaussianWindowPM(aSigma, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createMVGaussianWindow(const size_t n, const double aSigma, Model &aModel){
	return sharedPtr_t(new MVGaussianWindowPM(n, aSigma, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createMVGaussianWindow(const size_t n, const double *aSigma, Model &aModel){
	return sharedPtr_t(new MVGaussianWindowPM(n, aSigma, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createPCAWindow(const size_t n, const vector<double> &aSigma, Model &aModel){
	return sharedPtr_t(new PCAWindowPM(n, aSigma, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createIndependentBranchLengthBM(const size_t N, TR::TreeManager::sharedPtr_t aTM,
								TR::Bipartition::BipartitionMonitor::sharedPtr_t aBM, Model &aModel) {
	return sharedPtr_t(new BranchLengthIndepBM(N, aTM, aBM, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createCenteredDirichletBM(bool aAreParamSimplex, Model &aModel) {
	return sharedPtr_t(new CenteredDirichletBM(aAreParamSimplex, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createDefaultBM(Model &aModel){
	return createGaussianWindow(0.5, aModel);
}

BlockModifier::sharedPtr_t BlockModifier::createMultiplierWindow(const double aB, Model &aModel) {
	return sharedPtr_t(new MultiplierWindowPM(aB, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createESPRBM(const double aP,
		TR::TreeManager::sharedPtr_t aTM, Model &aModel) {
	return sharedPtr_t(new ESPR_PM(aP, aTM, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createETBRBM(const double aP,
		TR::TreeManager::sharedPtr_t aTM, Model &aModel) {
	return sharedPtr_t(new ETBR_BM(aP, aTM, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createAdaptivePBJBM(TR::AdaptivePBJ::moveType_t aMoveType, double aPe, double aEpsilon, double aPower,
		TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBM, Model &aModel) {
	return sharedPtr_t(new AdaptivePBJ_BM(aMoveType, aPe, aEpsilon, aPower, aTM, aBM, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createAdaptiveESPRBM(TR::AdaptiveESPR::stepNumber_t aNStep, TR::AdaptiveESPR::bias_t aBias, double aEpsilon, double aPower,
		TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBM, Model &aModel) {
		return sharedPtr_t(new AdaptiveESPR_PM(aNStep, aBias, aEpsilon, aPower, aTM, aBM, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createGuidedESPRBM(size_t aNStep,
		TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBM,
		MolecularEvolution::Parsimony::FastFitchEvaluator::sharedPtr_t aFFE, Model &aModel) {
		return sharedPtr_t(new GuidedESPR_PM(aNStep, aTM, aBM, aFFE, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createSTNNIBM(TR::TreeManager::sharedPtr_t aTM,
		Model &aModel) {
	return sharedPtr_t(new STNNI_PM(aTM, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createGuidedSTNNIBM(size_t aNStep,
		TR::TreeManager::sharedPtr_t aTM, TR::Bipartition::BipartitionMonitor::sharedPtr_t aBM,
		MolecularEvolution::Parsimony::FastFitchEvaluator::sharedPtr_t aFFE, Model &aModel) {
		return sharedPtr_t(new GuidedSTNNI_PM(aNStep, aTM, aBM, aFFE, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createDiscreteCategoriesBM(
		const std::vector<unsigned short int> &aIdProfile, Model &aModel) {

	std::vector<size_t> idProfile(aIdProfile.size());
	for(size_t iP=0; iP<idProfile.size(); ++iP) idProfile[iP] = aIdProfile[iP];

	return sharedPtr_t(new DiscreteCategoriesBM(idProfile, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createDiscreteCategoriesBM(
		const std::vector<size_t> &aIdProfile, Model &aModel) {
	return sharedPtr_t(new DiscreteCategoriesBM(aIdProfile, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createDiscreteCategoriesBM(
		const std::vector<double> &data, Model &aModel) {
	return sharedPtr_t(new DiscreteCategoriesBM(data, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createWeightedDiscreteCategoriesBM(
		const std::vector<unsigned short int> &aIdProfile, const std::vector<float> &aWeight, Model &aModel) {

	std::vector<size_t> idProfile(aIdProfile.size());
	for(size_t iP=0; iP<aIdProfile.size(); ++iP) idProfile[iP] = aIdProfile[iP];

	std::vector<double> weight(aWeight.size());
	for(size_t iW=0; iW<aWeight.size(); ++iW) weight[iW] = aWeight[iW];

	return sharedPtr_t(new WeightedDiscreteCategoriesBM(idProfile, weight, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createWeightedDiscreteCategoriesBM(
		const std::vector<size_t> &aIdProfile, const std::vector<double> &aWeight, Model &aModel) {
	return sharedPtr_t(new WeightedDiscreteCategoriesBM(aIdProfile, aWeight, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createWeightedDiscreteCategoriesBM(
		const std::vector<double> &data, Model &aModel) {
	return sharedPtr_t(new WeightedDiscreteCategoriesBM(data, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createNormalIndependentBM(
		double aMu, double aStd, Model &aModel) {
	return sharedPtr_t(new NormalIndependentBM(aMu, aStd, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createGammaIndependentBM(
		double aShape, double aScale, Model &aModel) {
	return sharedPtr_t(new GammaIndependentBM(aShape, aScale, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createDirichletIndependentBM(
		const std::vector<double> &aAlphas, Model &aModel) {
	return sharedPtr_t(new DirichletIndependentBM(aAlphas, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createHyperPriorDirichletIndepBM(
		double aAlpha0, const std::vector<double> &aAlphas, Model &aModel) {
	return sharedPtr_t(new HyperPriorDirichletIndepBM(aAlpha0, aAlphas, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createHyperPriorDirichletIndepBM(
		const std::vector<double> &aData, Model &aModel) {
	std::vector<double> tmp(aData.begin()+1, aData.end());
	return sharedPtr_t(new HyperPriorDirichletIndepBM(aData.front(), tmp, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createCoevProfDirichletBM(const std::vector<double> &aData,
	   	   	  	  	  	  	  	  	  	  	  	 	 	 	 	 	Model &aModel) {
	return sharedPtr_t(new CoevProfDirichletBM(aData, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createCoevProfDirichletBM(const std::vector<unsigned short int> &aIdProfile,
																	const std::vector<float> &aWeight,
	   	   	  	  	  	  	  	  	  	  	  	 	 	 	 	 	const std::vector<double> &aAlphas, Model &aModel) {
	std::vector<size_t> idProfile(aIdProfile.size());
	for(size_t iP=0; iP<aIdProfile.size(); ++iP) idProfile[iP] = aIdProfile[iP];

	std::vector<double> weight(aWeight.size());
	for(size_t iW=0; iW<aWeight.size(); ++iW) weight[iW] = aWeight[iW];

	 return sharedPtr_t(new CoevProfDirichletBM(idProfile, weight, aAlphas, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createCoevProfDirichletBM(const std::vector<size_t> &aIdProfile,
	   	   	  	  	  	  	  	  	  	  	  	 	 	 	 	 	const std::vector<double> &aWeight,
	   	   	  	  	  	  	  	  	  	  	  	 	 	 	 	 	const std::vector<double> &aAlphas, Model &aModel) {
	return sharedPtr_t(new CoevProfDirichletBM(aIdProfile, aWeight, aAlphas, aModel));
}


BlockModifier::sharedPtr_t BlockModifier::createCoevPoissonConjugateBM(double aAlpha, double aBeta, Model &aModel) {
	return sharedPtr_t(new CoevPoissonConjugatePriorKBM(aAlpha, aBeta, aModel));
}

BlockModifier::sharedPtr_t BlockModifier::createCoevGammaShapeConjugateBM(double aKnownAlpha_R, double aAlpha0_R, double aBeta0_R,
		   	   	   	   	   	   	   	   	   	   	   	   	   	   	   	   	  double aKnownAlpha_D, double aAlpha0_D, double aBeta0_D,
		   	   	   	   	   	   	   	   	   	   	   	   	   	   	   	   	  Model &aModel) {
	return sharedPtr_t(new CoevGammaShapeConjugatePriorBM(
			aKnownAlpha_R, aAlpha0_R, aBeta0_R,
			aKnownAlpha_D, aAlpha0_D, aBeta0_D,
			aModel));
}

void BlockModifier::defaultReflection(const vector<size_t> &pInd, Sample &sample) const {

	for(size_t i=0; i<pInd.size(); ++i){ 			// For each parameter
		if(model.getParams().requireReflection(pInd[i])) { 	// Check if require reflection
			defDblReflection(pInd[i], sample);
		}
	}
}

void BlockModifier::defDblReflection(const size_t pId, Sample &sample) const {
	uint reflectCnt = 0;
	double min = model.getParams().getBoundMin(pId);
	double max = model.getParams().getBoundMax(pId);
	double value = sample.getDoubleParameter(pId);

	if(min == max) {
		sample.setDoubleParameter(pId, min);
	} else {
		while(value < min || value > max){
			if(value < min){
				value = 2.*min-value;
			} else if(value > max){
				value = 2.*max-value;
			}
			++reflectCnt;
			if(reflectCnt > 50) {
				value = min+(max-min)/2.;
			}
		}
		model.getParams().signalReflections(pId, reflectCnt);
		sample.setDoubleParameter(pId, value);
	}
}

} // namespace ParameterBlock

