//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchEvoRU.h
 *
 * @date Jan 14, 2016
 * @author meyerx
 * @brief
 */
#ifndef BATCHEVORU_H_
#define BATCHEVORU_H_

#include <sys/types.h>

#include "../../CC/DynMultiLvlCC.h"
#include "../../CC/MeanVarCC.h"
#include "LightRhoUpdater.h"

namespace ParameterBlock {

namespace State { class EvoRUState; }

class BatchEvoRU: public LightRhoUpdater {
public:
	BatchEvoRU(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg);
	virtual ~BatchEvoRU();

	void reset(const bool keepLambda=false);
	void resetMVCC();

	bool hasConverged() const;
	void addVal(const double meanAlpha);
	void setAverageSd();

	uint getTmpT() const;

	string toString() const;

	void relaxTolerance(const double aFactorTol){
		mcc.relaxTolerance(aFactorTol);
	}

	void saveState(State::EvoRUState &state) const;
	void loadState(const State::EvoRUState &state);

private:
	unsigned int tmpT;
	double batchAlpha;
	DynMultiLvlCC mcc;
};

} // namespace ParameterBlock

#endif /* BATCHEVORU_H_ */
