//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MCCState.cpp
 *
 * @date Jan 23, 2016
 * @author meyerx
 * @brief
 */
#include "MCCState.h"

#include <boost/serialization/nvp.hpp>

namespace ParameterBlock {
namespace State {

MCCState::MCCState() {
	t = sum = sumSq = factorTol = 0.0;
}

MCCState::~MCCState() {
}

template<class Archive>
void MCCState::serialize(Archive & ar, const unsigned int version) {
	ar & BOOST_SERIALIZATION_NVP( t );
	ar & BOOST_SERIALIZATION_NVP( sum );
	ar & BOOST_SERIALIZATION_NVP( sumSq );
	ar & BOOST_SERIALIZATION_NVP( factorTol );
	ar & BOOST_SERIALIZATION_NVP( vals );
	ar & BOOST_SERIALIZATION_NVP( bMeans );
	ar & BOOST_SERIALIZATION_NVP( bRSDs );
}

template void MCCState::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version);
template void MCCState::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void MCCState::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version);
template void MCCState::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace State */
} /* namespace ParameterBlock */
