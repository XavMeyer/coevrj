//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MCCState.h
 *
 * @date Jan 23, 2016
 * @author meyerx
 * @brief
 */
#ifndef MCCSTATE_H_
#define MCCSTATE_H_

#include <list>

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

// Forward declaration
namespace ParameterBlock {
	class ConvergenceChecker;
	class DynMultiLvlCC;
	class MeanVarCC;
}

namespace Sampler {
namespace BlockOptimizer {
	class BlockUpdater;
}
}

namespace ParameterBlock {
namespace State {

class MCCState {
	friend class ParameterBlock::ConvergenceChecker;
	friend class ParameterBlock::MeanVarCC;
	friend class ParameterBlock::DynMultiLvlCC;
	friend class Sampler::BlockOptimizer::BlockUpdater;
public:
	MCCState();
	~MCCState();

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version);
private:

    double t, sum, sumSq, factorTol;
    std::list<double> vals, bMeans, bRSDs;

	friend class boost::serialization::access;

};

} /* namespace State */
} /* namespace ParameterBlock */

#endif /* MCCSTATE_H_ */
