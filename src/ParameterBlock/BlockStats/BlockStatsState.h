//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockStatsState.h
 *
 * @date Jan 23, 2016
 * @author meyerx
 * @brief
 */
#ifndef BLOCKSTATSSTATE_H_
#define BLOCKSTATSSTATE_H_

#include <stddef.h>

#include "ParameterBlock/BlockStats/ConvUtils/Updater/CovMatrix/../../CC/MCCState.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/CovMatrix/EvoSUState.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/EvoLUState.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Mean/EvoMUState.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

// Forward declaration
namespace ParameterBlock {
	class BlockStats;
	class EvoBS;
	class EvoPCABS;
}

namespace Sampler {
namespace BlockOptimizer {
	class BlockUpdater;
}
}

namespace ParameterBlock {
namespace State {

class BlockStatsState {
	friend class ParameterBlock::BlockStats;
	friend class ParameterBlock::EvoBS;
	friend class ParameterBlock::EvoPCABS;
	friend class Sampler::BlockOptimizer::BlockUpdater;

public:
	BlockStatsState();
	~BlockStatsState();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

private:

	size_t N, locSdT, nLocSDDisabled;
	double targetAlpha;
	bool sigmaConverged, lambdaConverged, locLambdaConverged;

	std::vector<bool> locSDEnabled;
	std::vector<uint> divergeCnt;
	std::vector<double> factors;
	std::vector<double> eigVal, eigVec; // Only for PCA

	EvoSUState suState;
	std::vector<EvoMUState> muState;
	EvoLUState luState;
	std::vector<EvoLUState> localLuState;
	MCCState localMCCState;

	friend class boost::serialization::access;

};

} /* namespace State */
} /* namespace ParameterBlock */

#endif /* BLOCKSTATSSTATE_H_ */


