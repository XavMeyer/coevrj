//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockStatsState.cpp
 *
 * @date Jan 23, 2016
 * @author meyerx
 * @brief
 */
#include "BlockStatsState.h"

#include <boost/serialization/nvp.hpp>

namespace ParameterBlock {
namespace State {

BlockStatsState::BlockStatsState() {

	N = 0;
	locSdT = 0;
	nLocSDDisabled = 0;

	targetAlpha = 0.;

	sigmaConverged = false;
	lambdaConverged = false;
	locLambdaConverged = false;

}

BlockStatsState::~BlockStatsState() {
}

template<class Archive>
void BlockStatsState::serialize(Archive & ar, const unsigned int version) {

	ar & BOOST_SERIALIZATION_NVP( N );
	ar & BOOST_SERIALIZATION_NVP( locSdT );
	ar & BOOST_SERIALIZATION_NVP( nLocSDDisabled );

	ar & BOOST_SERIALIZATION_NVP( targetAlpha );

	ar & BOOST_SERIALIZATION_NVP( sigmaConverged );
	ar & BOOST_SERIALIZATION_NVP( lambdaConverged );
	ar & BOOST_SERIALIZATION_NVP( locLambdaConverged );

	ar & BOOST_SERIALIZATION_NVP( locSDEnabled );
	ar & BOOST_SERIALIZATION_NVP( divergeCnt );
	ar & BOOST_SERIALIZATION_NVP( factors );
	ar & BOOST_SERIALIZATION_NVP( eigVal ); // Only for PCA
	ar & BOOST_SERIALIZATION_NVP( eigVec ); // Only for PCA

	ar & BOOST_SERIALIZATION_NVP( suState );
	ar & BOOST_SERIALIZATION_NVP( muState );
	ar & BOOST_SERIALIZATION_NVP( luState );
	ar & BOOST_SERIALIZATION_NVP( localLuState );
	ar & BOOST_SERIALIZATION_NVP( localMCCState );

}

template void BlockStatsState::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version);
template void BlockStatsState::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void BlockStatsState::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version);
template void BlockStatsState::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace State */
} /* namespace ParameterBlock */
