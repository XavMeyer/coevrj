//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SlowFactory.h
 *
 * @date May 7, 2015
 * @author meyerx
 * @brief
 */
#ifndef SLOWFACTORY_H_
#define SLOWFACTORY_H_

#include <ParameterBlock/BlockStats/Config/ConfigFactory.h>

namespace ParameterBlock {
namespace Config {

class SlowFactory: public ConfigFactory {
public:
	SlowFactory() : ConfigFactory() {}
	~SlowFactory(){}

	BlockStatCfg::sharedPtr_t createConfig(const size_t N) const {
		return BlockStatCfg::getSlow(N);
	}
};

} /* namespace Config */
} /* namespace ParameterBlock */

#endif /* SLOWFACTORY_H_ */
