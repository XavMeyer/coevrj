//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file IDManager.h
 *
 * @date Apr 7, 2017
 * @author meyerx
 * @brief Inspired by http://stackoverflow.com/questions/2620218/fastest-container-or-algorithm-for-unique-reusable-ids-in-c
 */
#ifndef IDMANAGER_H_
#define IDMANAGER_H_

#include <stddef.h>

#include "IDInterval.h"

namespace Utils {
namespace IDManagement {

class IDManager {
public:
	IDManager();
	IDManager(size_t minValue, size_t maxValue);
	~IDManager();

    size_t allocateId();        		// Allocates an id
    void freeId(size_t id);      		// Frees an id so it can be used again
    bool markAsUsed(size_t id);   		// Let's the user register an id.

    size_t getBiggestAllocatedId();

private:

    typedef std::set<IDInterval> idIntervals_t;
    idIntervals_t freeIntervals;

};

} /* namespace IDManagement */
} /* namespace Utils */

#endif /* IDMANAGER_H_ */
