//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file IDInterval.h
 *
 * @date Apr 7, 2017
 * @author meyerx
 * @brief Inspired by http://stackoverflow.com/questions/2620218/fastest-container-or-algorithm-for-unique-reusable-ids-in-c
 */
#ifndef IDINTERVAL_H_
#define IDINTERVAL_H_

#include <boost/numeric/interval.hpp>
#include <stddef.h>
#include <iostream>
#include <limits>
#include <set>

#include <boost/numeric/interval/interval.hpp>
#include <boost/numeric/interval/policies.hpp>

namespace Utils {
namespace IDManagement {

class IDInterval {
public:
	IDInterval(size_t aLower, size_t aUpper);
	~IDInterval();

	bool operator<(const IDInterval& other) const;
	size_t getLowerValue() const;
	size_t getUpperValue() const;
private:
	boost::numeric::interval<size_t> invervalRange;
};

struct IDIntervalContains {
	const size_t id;
	IDIntervalContains(size_t aId) : id(aId) {}
	~IDIntervalContains() {}

	bool operator()(const IDInterval &rhs) {
		return id >= rhs.getLowerValue() && id <= rhs.getUpperValue();
	}

};

} /* namespace IDManagement */
} /* namespace Utils */

#endif /* IDINTERVAL_H_ */
