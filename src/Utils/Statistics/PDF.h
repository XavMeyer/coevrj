//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PDF.hpp
 *
 * @date Jan 13, 2017
 * @author meyerx
 * @brief
 */
#ifndef PDF_HPP_
#define PDF_HPP_

#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/Cholesky>

namespace Utils {
namespace Statistics {

namespace MultivariateNormal {

double singleCholeskyPDF(const Eigen::VectorXd &x, const Eigen::VectorXd &mu, const Eigen::LLT<Eigen::MatrixXd> &choleskyDecomp);
Eigen::VectorXd multiCholeskyPDF(const Eigen::MatrixXd &x, const Eigen::VectorXd &mu, const Eigen::LLT<Eigen::MatrixXd> &choleskyDecomp);
double singleSigmaPDF(const Eigen::VectorXd &x, const Eigen::VectorXd &mu, const Eigen::MatrixXd &sigma);
Eigen::VectorXd multiSigmaPDF(const Eigen::MatrixXd &x, const Eigen::VectorXd &mu, const Eigen::MatrixXd &sigma);

} /* namespace MultivariateNormal */


}
}

#endif /* PDF_HPP_ */
