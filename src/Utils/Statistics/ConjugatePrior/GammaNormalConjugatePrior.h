//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GammaNormalConjugatePrior.h
 *
 * @date May 28, 2017
 * @author meyerx
 * @brief Implementation for the Gamma Normal distribution.
 * This distribution is the conjugate to the Normal distribution with unknown mean and variance.
 * See (saved in zotero) :
 * - Conjugate analysis of the Gaussian Distribution by Murphy Kevin
 * - Conjugate prior of the normal distribution by Jordan Michael
 * - or wikipedia
 */
#ifndef GAMMANORMALCONJUGATEPRIOR_H_
#define GAMMANORMALCONJUGATEPRIOR_H_

#include <boost/math/distributions/gamma.hpp>
#include <boost/math/distributions/normal.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

#include <utility>
#include <vector>

class RNG;

namespace Utils {
namespace Statistics {

class GammaNormalConjugatePrior {
public:
	typedef boost::shared_ptr<GammaNormalConjugatePrior> sharedPtr_t;
public:
	GammaNormalConjugatePrior(double aMu0, double aN0, double aAlpha, double aBeta);
	~GammaNormalConjugatePrior();

	std::pair<double, double> drawFromPrior(RNG *aRNG) const;
	std::pair<double, double> drawFromPosterior(RNG *aRNG, const std::vector<double> &data) const;

	double computePDF(const std::pair<double, double> &coupledMeanVar) const;

private:

	/// Assuming a distribution normal(mu, var)
	/// MU0 represents the observed mu
	/// N0 represents the number of sample on which this observation is based
	/// 2*ALPHA represents the number of sample on which the observation variance is based
	/// 2*BETA represents the Sum of the Squared Deviations from the mean (SSD or SSE)
	/// --> SSD = SUM [mean - x]^2 ==> Sample variance sigma^2 = SSD/(n-1)
	/// Therefore 2*BETA=SSD, n=2*ALPHA, (2*BETA)/(2*ALPHA)=BETA/ALPHA=SSD/n~= sigma^2
	const double MU0, N0, ALPHA, BETA;

};

} /* namespace Statistics */
} /* namespace Utils */

#endif /* GAMMANORMALCONJUGATEPRIOR_H_ */
