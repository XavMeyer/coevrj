//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file InverseGammaConjugatePrior.cpp
 *
 * @date May 28, 2017
 * @author meyerx
 * @brief
 */
#include "InverseGammaConjugatePrior.h"

#include <utility>
#include <vector>

#include "Parallel/RNG/RNG.h"

namespace Utils {
namespace Statistics {

InverseGammaConjugatePrior::InverseGammaConjugatePrior(double aAlpha, double aBeta) :
		ALPHA(aAlpha), BETA(aBeta) {
}

InverseGammaConjugatePrior::~InverseGammaConjugatePrior() {
}

double InverseGammaConjugatePrior::drawFromPrior(RNG *aRNG) const {
	double randIG = aRNG->genGamma(ALPHA, 1./BETA);
	double x = 1./randIG;
	return x;
}

double InverseGammaConjugatePrior::drawFromPosterior(RNG *aRNG, const std::vector<double> &data) const {
	const double N = data.size();
	// Compute the mean of data
	double mean = 0.;
	for(size_t iD=0; iD<data.size(); ++iD) mean += data[iD];
	mean /= N;

	// Precision is T|x ~ GAMMA(paramHP1, paramHP2)
	// paramHP1 = alpha + n/2
	double paramHP1 = ALPHA + N/2.;
	// paramHP2 = beta + 0.5*sum[(x_i-mean(x))^2]+(mean(x)-mu0)^2 * (n*n0)/[2*(n+n0)])
	double SSD = 0.;
	for(size_t iD=0; iD<data.size(); ++iD) SSD += (data[iD]-mean)*(data[iD]-mean);
	double paramHP2 = BETA + 0.5*SSD;

	double precision = aRNG->genGamma(paramHP1, 1./paramHP2); // paramHP2 is the rate

	//std::cout << "DATA:\t"; // FIXME DEBUG
	//for(size_t iD=0; iD<data.size();++iD) std::cout << data[iD] << ", "; // FIXME DEBUG
	//std::cout << std::endl << "Var = " << 1./precision << std::endl;; // FIXME DEBUG

	return 1./precision;
}

double InverseGammaConjugatePrior::computePDF(double variance) const {
	//double logPdf = ALPHA*log(BETA)-boost::math::lgamma(ALPHA)+(-ALPHA-1.)*log(variance)-BETA/variance;
	boost::math::inverse_gamma_distribution<> iGammaDistr(ALPHA, BETA);
	//std::cout << exp(logPdf) << "\t vs \t" << boost::math::pdf(iGammaDistr, variance) << std::endl;
	return boost::math::pdf(iGammaDistr, variance);
}

} /* namespace Statistics */
} /* namespace Utils */
