//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GammaKnownShapeConjugatePrior.cpp
 *
 * @date Jun 2, 2017
 * @author meyerx
 * @brief
 */
#include "GammaKnownShapeConjugatePrior.h"

#include "Parallel/RNG/RNG.h"

namespace Utils {
namespace Statistics {

GammaKnownShapeConjugatePrior::GammaKnownShapeConjugatePrior(double aKnownAlpha, double aAlpha0, double aBeta0) :
	KNOWN_ALPHA(aKnownAlpha), ALPHA0(aAlpha0), BETA0(aBeta0) {
}

GammaKnownShapeConjugatePrior::~GammaKnownShapeConjugatePrior() {
}

double GammaKnownShapeConjugatePrior::drawFromPrior(RNG *aRNG) const {
	return aRNG->genGamma(ALPHA0, BETA0);
}

double GammaKnownShapeConjugatePrior::drawFromPosterior(RNG *aRNG, const std::vector<double> &data) const {
	double sumData = 0.;
	const double N_DATA = data.size();
	for(size_t iD=0; iD<data.size(); ++iD) sumData += data[iD];

	double term1 = ALPHA0 + N_DATA*KNOWN_ALPHA;
	double term2 = 1./BETA0 + sumData; // 1./BETA0 ==> From scale to rate

	return aRNG->genGamma(term1, 1./term2); // genGamma use scale so => 1./term2 from rate to scale
}

double GammaKnownShapeConjugatePrior::computePDF(double aBeta) const {
	boost::math::gamma_distribution<> gammaDistr(ALPHA0, BETA0);
	return boost::math::pdf(gammaDistr, aBeta);
}


} /* namespace Statistics */
} /* namespace Utils */
