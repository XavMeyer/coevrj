//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GammaKnownShapeConjugatePrior.h
 *
 * @date Jun 2, 2017
 * @author meyerx
 * @brief
 */
#ifndef GAMMAKNOWNSHAPECONJUGATEPRIOR_H_
#define GAMMAKNOWNSHAPECONJUGATEPRIOR_H_

#include <boost/math/distributions/gamma.hpp>
#include <boost/math/distributions/normal.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

#include <utility>
#include <vector>

class RNG;

namespace Utils {
namespace Statistics {

class GammaKnownShapeConjugatePrior {
public:
	typedef boost::shared_ptr<GammaKnownShapeConjugatePrior> sharedPtr_t;
public:
	GammaKnownShapeConjugatePrior(double aKnownAlpha, double aAlpha0, double aBeta0);
	~GammaKnownShapeConjugatePrior();

	double drawFromPrior(RNG *aRNG) const;
	double drawFromPosterior(RNG *aRNG, const std::vector<double> &data) const;

	double computePDF(double aBeta) const;

private:

	/// Assuming a distribution gamma(alpha, beta)
	/// KNOWN_ALPHA represents the known shape parameter
	/// ALPHA0 represents the number of sample on which this observation is based
	/// BETA0 represents the sum of the observations
	const double KNOWN_ALPHA, ALPHA0, BETA0;


};

} /* namespace Statistics */
} /* namespace Utils */

#endif /* GAMMAKNOWNSHAPECONJUGATEPRIOR_H_ */
