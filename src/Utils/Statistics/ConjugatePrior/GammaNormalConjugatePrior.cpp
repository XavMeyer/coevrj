//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GammaNormalConjugatePrior.cpp
 *
 * @date May 28, 2017
 * @author meyerx
 * @brief
 */
#include "GammaNormalConjugatePrior.h"

#include <math.h>
#include <boost/math/special_functions/gamma.hpp>

#include "Parallel/RNG/RNG.h"

namespace Utils {
namespace Statistics {

GammaNormalConjugatePrior::GammaNormalConjugatePrior(double aMu0, double aN0, double aAlpha, double aBeta) :
	MU0(aMu0), N0(aN0), ALPHA(aAlpha), BETA(aBeta) {
}

GammaNormalConjugatePrior::~GammaNormalConjugatePrior() {
}

std::pair<double, double> GammaNormalConjugatePrior::drawFromPrior(RNG *aRNG) const {

	std::pair<double, double> coupledMeanVar;

	// Draw precision T and define var=1/T
	double precision = aRNG->genGamma(ALPHA, 1./BETA);
	coupledMeanVar.second = 1./precision;

	// Draw mean (normal(mu, sigma) and precision is 1/sigma^2
	coupledMeanVar.first = aRNG->genNormal(MU0, sqrt(1./(N0*precision)));

	return coupledMeanVar;
}

std::pair<double, double> GammaNormalConjugatePrior::drawFromPosterior(RNG *aRNG, const std::vector<double> &data) const {

	std::pair<double, double> coupledMeanVar;

	const double N = data.size();
	// Compute the mean of data
	double mean = 0.;
	for(size_t iD=0; iD<data.size(); ++iD) mean += data[iD];
	mean /= N;

	// Precision is T|x ~ GAMMA(paramHP1, paramHP2)
	// paramHP1 = alpha + n/2
	double paramHP1 = ALPHA + N/2.;
	// paramHP2 = beta + 0.5*sum[(x_i-mean(x))^2]+(mean(x)-mu0)^2 * (n*n0)/[2*(n+n0)])
	double SSD = 0.;
	for(size_t iD=0; iD<data.size(); ++iD) SSD += (data[iD]-mean)*(data[iD]-mean);
	double paramHP2 = BETA + 0.5*SSD + 0.5 * (mean-MU0)*(mean-MU0) * (N*N0) /(N+N0);

	double precision = aRNG->genGamma(paramHP1, 1./paramHP2); // paramHP2 is the rate
	coupledMeanVar.second = 1./precision;

	// Mean is mu|T, x ~ NORMAL(paramHP3, paramHP4)
	// paramHP3 = n*T*meanX/(n*T+N0*T) + n0*T*mu0/(n*T+N0*T)
	double paramHP3 = (N*precision*mean + N0*precision*MU0)/(N*precision+N0*precision);
	// paramHP4 = n*T+N0*T
	double paramH4 = N*precision+N0*precision; // "Precision"
	coupledMeanVar.first = aRNG->genNormal(paramHP3, sqrt(1./paramH4)); // Get the std as sqrt(1/precision)=sqrt(var)=std

	//std::cout << "DATA:\t"; // FIXME DEBUG
	//for(size_t iD=0; iD<data.size();++iD) std::cout << data[iD] << ", "; // FIXME DEBUG
	//std::cout << std::endl << "Mean = " << coupledMeanVar.first << "\t Var = " << coupledMeanVar.second << std::endl;; // FIXME DEBUG

	return coupledMeanVar;

}

double GammaNormalConjugatePrior::computePDF(const std::pair<double, double> &coupledMeanVar) const {
	// PDF is BETA^ALPHA*sqrt(N0)/[GAMMA(ALPHA)*sqrt(2*pi)] * T^(ALPHA-1/2) * exp(-BETA*T) * exp(-0.5*N0*T*(X-MU0)^2)
	// Compute in log space :
	// log(PDF) = ALPHA*log(BETA) + log(sqrtN0) - log(GAMMA(ALPHA)) - log(sqrt(2*pi) + (ALPHA-0.5)*log(T) -BETA*T -0.5*NO*T*(X-MU0)^2
	double precision = 1./coupledMeanVar.second;
	double logPDF = ALPHA*log(BETA);
	logPDF += 0.5*log(N0);
	logPDF -= boost::math::lgamma(ALPHA);
	logPDF -= 0.5*log(2.*M_PI);
	logPDF += (ALPHA-0.5)*log(precision);
	logPDF += -BETA*precision;
	logPDF += -0.5*N0*precision*(coupledMeanVar.first-MU0)*(coupledMeanVar.first-MU0);

	return exp(logPDF);
}

} /* namespace Statistics */
} /* namespace Utils */
