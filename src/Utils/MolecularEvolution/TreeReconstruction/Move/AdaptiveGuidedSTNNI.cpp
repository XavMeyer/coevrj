//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GuidedSTNNI_CPP.cpp
 *
 * @date Nov 4, 2018
 * @author meyerx
 * @brief
 */
#include <assert.h>
#include <boost/assign/list_of.hpp>

#include "AdaptiveGuidedSTNNI.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Parallel/RNG/RNG.h"

#include "Utils/MolecularEvolution/Parsimony/Cache/CacheLRU.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

const double AdaptiveGuidedSTNNI::EPSILON_FREQ = 1e-7;
const double AdaptiveGuidedSTNNI::POWER_FREQ = 1.3;

const bool AdaptiveGuidedSTNNI::EXPLORING_OUTSIDE_ADAPTIVE = false;

AdaptiveGuidedSTNNI::AdaptiveGuidedSTNNI(Bipartition::BipartitionMonitor::sharedPtr_t aBM,
		ParameterBlock::BranchHelper *aBH,
		Parsimony::FastFitchEvaluator::sharedPtr_t aFFE) :
				   TreeProposal(aBM, aBH), fastFitchEvaluator(aFFE) {

	lastFinalH = lastInitH = 0;

}

AdaptiveGuidedSTNNI::~AdaptiveGuidedSTNNI() {
}

AdaptiveGuidedSTNNI::minimalMoveInfo_t AdaptiveGuidedSTNNI::createMove(size_t iStep, long int h, TreeNode* movingNodeParent, TreeNode* movingNode,
													  Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {

	minimalMoveInfo_t newMove;

	newMove.nStep = iStep;
	newMove.h = h;
	newMove.movingEdge.n1 = movingNodeParent;
	newMove.movingEdge.n2 = movingNode;
	newMove.graftEdge = graftEdge;
	newMove.path = path;

	return newMove;

}

double AdaptiveGuidedSTNNI::queryParsimonyScore(Tree::sharedPtr_t tree) {

	double score = -1.;

	bool found = false;

	if(Parsimony::getCache().isEnabled()) {
		Parsimony::getCache().getElement(tree->getHashKey(), score);
	}

	if(found) {
		assert(score >= 0.);
	} else {

		score = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();

		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addNewElement(tree->getHashKey(), score);
		}
	}

	return score;
}

std::vector<double> AdaptiveGuidedSTNNI::computeMovesProbability(double currentScore, std::vector<double> &scores) {
	const double TRUNCATION_FACTOR = 0.1;
	const double EPSILON = 1.e-3;
	std::vector<double> rescaledScores(scores);

	// Trying with a rescaling type 1 - max[(new parsimony / old parsimony)/C, 1-epsilon]
	double sum = 0.;
	for(size_t i=0; i<scores.size(); ++i) {
		rescaledScores[i] = (currentScore - rescaledScores[i]) / ((double)fastFitchEvaluator->getNSite());

		rescaledScores[i] = std::min(TRUNCATION_FACTOR, rescaledScores[i]);
		rescaledScores[i] = std::max(-TRUNCATION_FACTOR, rescaledScores[i]);

		rescaledScores[i] += TRUNCATION_FACTOR;
		rescaledScores[i] = EPSILON + pow(rescaledScores[i]/TRUNCATION_FACTOR, 8.0);

		sum += rescaledScores[i];
	}

	for(size_t i=0; i<rescaledScores.size(); ++i) rescaledScores[i] /= sum;

	return rescaledScores;
}

std::vector<double> AdaptiveGuidedSTNNI::computeMovesJointProbability(const std::vector<double> &vecSplitProb, const std::vector<double> &prob) {

	std::vector<double> factorCCP, jointProb;
	/*for(size_t iS=0; iS<vecCCP.size(); ++iS) {
		if(vecCCP[iS] != vecCCP[iS]) {
			factorCCP.push_back(1.0);
		} else {
			factorCCP.push_back(0.01);
		}
	}*/

	double sumJointProb = 0.;
	for(size_t iS=0; iS<vecSplitProb.size(); ++iS) {
		if(EXPLORING_OUTSIDE_ADAPTIVE) {
			double factor = vecSplitProb[iS] < (1./vecSplitProb.size()) ? 1.0 : 0.01; // Down-weight moves that are over mean under uniform distr.
			jointProb.push_back(factor*prob[iS]);
		} else {
			jointProb.push_back(vecSplitProb[iS]*prob[iS]);
		}
		sumJointProb += jointProb.back();
	}

	for(size_t iS=0; iS<jointProb.size(); ++iS) {
		jointProb[iS] /= sumJointProb;
	}

	assert(vecSplitProb.size() == prob.size());
	/*for(size_t iS=0; iS<vecSplitProb.size(); ++iS) {
		std::cout << std::scientific << prob[iS] << " | " << vecSplitProb[iS] << " | " << jointProb[iS] << " | " << prob[iS] - jointProb[iS] <<std::endl;
	}
	std::cout << std::endl << "---------------------------------------------" << std::endl;*/

	return jointProb;

	//return rescaledScores;
}


Move::sharedPtr_t AdaptiveGuidedSTNNI::proposeNewTree(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	if(N_STEP > 0) {
		return proposeLimitedSTNNIMoves(N_STEP, tree, sample);
	} else {
		return proposeAllSTNNIMoves(tree, sample);
	}
}


split_t AdaptiveGuidedSTNNI::findBipartitions(TreeNode* ptr1, TreeNode* ptr2, std::vector< Tree::edge_t > &dirEdges, std::vector< split_t > &dirSplits) {

	bool reversed = false;
	if(ptr1->getId() > ptr2->getId()) {
		reversed = true;
		std::swap(ptr1, ptr2);
	}

	size_t iFound = 0;
	bool found = false;
	for(size_t iD=0; iD<dirEdges.size(); ++iD) {
		if(ptr1 == dirEdges[iD].n1 && ptr2 == dirEdges[iD].n2) {
			found = true;
			iFound = iD;
			break;
		}
	}
	assert(found && "Edge not found.");

	split_t aSplit(dirSplits[iFound]);
	if(reversed) aSplit.flip();
	return aSplit;
}


std::vector<double> AdaptiveGuidedSTNNI::defineAdaptiveMoveProbability(Tree::sharedPtr_t tree,
													  double edgeProb, Tree::edge_t edge,
													  TreeNode* ptrA, TreeNode* ptrB, TreeNode* ptrC, TreeNode* ptrD,
													  std::vector< Tree::edge_t > &dirEdges, std::vector< split_t > &dirSplits) {

	std::vector<double> moveProbs(2,0.);

	Bipartition::split_t splitA = this->findBipartitions(edge.n1, ptrA, dirEdges, dirSplits);
	Bipartition::split_t splitB = this->findBipartitions(edge.n1, ptrB, dirEdges, dirSplits);
	Bipartition::split_t splitC = this->findBipartitions(edge.n2, ptrC, dirEdges, dirSplits);
	Bipartition::split_t splitD = this->findBipartitions(edge.n2, ptrD, dirEdges, dirSplits);

	// Old split
	// AB
	Bipartition::split_t newSplitAB = splitA;
	newSplitAB |= splitB;
	/*for(size_t i=0; i<newSplitAB.size(); ++i) {
		newSplitAB[i] = splitB[i] || splitA[i];
	}*/
	if(newSplitAB[0]) newSplitAB.flip();

	// Possible permutation and frequencies
	// A to C
	Bipartition::split_t newSplitAC = splitA;
	newSplitAC |= splitC;
	/*for(size_t i=0; i<newSplitAC.size(); ++i) {
		newSplitAC[i] = splitC[i] || splitA[i];
	}*/
	if(newSplitAC[0]) newSplitAC.flip();
	double newSplitFreqAC = bipartitionMonitor->getSplitFrequency(newSplitAC);

	// A to D
	Bipartition::split_t newSplitAD = splitA;
	newSplitAD |= splitD;
	/*for(size_t i=0; i<newSplitAD.size(); ++i) {
		newSplitAD[i] = splitD[i] || splitA[i];
	}*/
	if(newSplitAD[0]) newSplitAD.flip();
	double newSplitFreqAD = bipartitionMonitor->getSplitFrequency(newSplitAD);

	double probAC = (EPSILON_FREQ+newSplitFreqAC)/(2.*EPSILON_FREQ+newSplitFreqAC+newSplitFreqAD);
	double probAD = (EPSILON_FREQ+newSplitFreqAD)/(2.*EPSILON_FREQ+newSplitFreqAC+newSplitFreqAD);

	moveProbs[0] = edgeProb*probAC;
	moveProbs[1] = edgeProb*probAD;

	return moveProbs;
}

std::vector<double> AdaptiveGuidedSTNNI::getEdgeProbabilities(double epsilonFreq, double powerFreq, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF) {

	std::vector<double> probabilities(vecBF.size(), epsilonFreq);

	double sumFreq = epsilonFreq*vecBF.size();
	for(size_t iF=0; iF<vecBF.size(); ++iF) {
		sumFreq += pow((1.-vecBF[iF].freq), powerFreq);
	}

	for(size_t iF=0; iF<vecBF.size(); ++iF) {
		probabilities[iF] += pow((1.-vecBF[iF].freq), powerFreq);
		probabilities[iF] /= sumFreq;
	}

	return probabilities;
}

void AdaptiveGuidedSTNNI::defineAllMovesSTNNI(Tree::sharedPtr_t tree, vecMoveInfo_t &moves, std::vector<double> &scores, std::vector<double> &vecSplitProb) {


	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> eProbability = getEdgeProbabilities(EPSILON_FREQ, POWER_FREQ, vecBF);

	std::vector< Tree::edge_t > dirEdges = tree->getDirectedEdgeForBipartitions();
	std::vector< split_t > dirSplits = tree->getDirectedSplitsForBipartitions();

	for(size_t iE=0; iE<vecBF.size(); ++iE) {
		Tree::edge_t stableEdge = vecBF[iE].edge;
		if(stableEdge.n1->getId() < stableEdge.n2->getId()) std::swap(stableEdge.n1, stableEdge.n2);

		vecTN_t childrenN1 = stableEdge.n1->getChildren(stableEdge.n2); // {A,B}
		vecTN_t childrenN2 = stableEdge.n2->getChildren(stableEdge.n1); // {C,D
		assert(childrenN1.size()==2 && childrenN2.size()==2);


		std::vector<double> moveProbs = defineAdaptiveMoveProbability(tree, eProbability[iE], stableEdge,
				childrenN1[0], childrenN1[1], childrenN2[0], childrenN2[1], dirEdges, dirSplits);


		for(size_t iC=0; iC<2; ++iC) {

			bool cross = (iC == 0);

			// Define who are swapped
			TreeNode *ptrA = childrenN1[0]; // Get A
			TreeNode *ptrB = childrenN1[1]; // Get B
			TreeNode *ptrOther, *ptrOther2;
			if(cross) {
				ptrOther = childrenN2[0]; // Get C
				ptrOther2 = childrenN2[1];
			} else {
				ptrOther = childrenN2[1]; // Get D
				ptrOther2 = childrenN2[0];
			}

			// Swap the subtrees
			tm.swapSubtrees(tree, ptrB, ptrOther, stableEdge);

			// Create and memorize node
			// Compute parsimony
			double score = queryParsimonyScore(tree);
			double splitProb = moveProbs[iC];

			// Creating node
			std::vector<TreeNode*> tmpPath;
			tmpPath.push_back(ptrA);
			tmpPath.push_back(ptrOther2);
			minimalMoveInfo_t newMove = createMove(1, tree->getHashKey(), ptrB, ptrOther, stableEdge, tmpPath);

			// Register the move and the parsimony score
			moves.push_back(newMove);
			scores.push_back(score);
			vecSplitProb.push_back(splitProb);

			// Swap the subtrees back
			tm.swapSubtrees(tree, ptrOther, ptrB, stableEdge);
		}
	}
}

Move::sharedPtr_t AdaptiveGuidedSTNNI::proposeAllSTNNIMoves(Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	// Memorize pending change on the tree:
	std::vector<TreeNode*> originalUpdatedNodes(tree->getUpdatedNodes());

	// Get current tree hash
	long int currentH = tree->getHashKey();
	bool reuseNew = (lastFinalH == currentH);
	bool reuseOld = (lastInitH == currentH);
	lastInitH = currentH;

	// First we want to make sure that the parsimony score are starting from a safe point
	double currentScore = 0.;
	if(reuseNew) {
		currentScore = queryParsimonyScore(tree);
		tree->clearUpdatedNodes();
	} else {
		currentScore = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();
		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addElement(currentH, currentScore);
		}
	}

	// Compute all 1 step forward moves
	vecMoveInfo_t fwMoves;
	std::vector<double> fwScores;
	std::vector<double> fwSplitProb;
	if(reuseOld) {
		fwMoves = cacheFwMoves;
		fwScores = cacheFwScores;
		fwSplitProb = cacheFwSplitProb;
	} else if(reuseNew) {
		fwMoves = cacheBwMoves;
		fwScores = cacheBwScores;
		fwSplitProb = cacheBwSplitProb;
	} else {
		defineAllMovesSTNNI(tree, fwMoves, fwScores, fwSplitProb);
	}


	// Draw based on parsimony scores
	std::vector<double> fwMovesProbability(computeMovesProbability(currentScore, fwScores));
	std::vector<double> fwTunedMovesProb(computeMovesJointProbability(fwSplitProb, fwMovesProbability));

	size_t iDraw = Parallel::mpiMgr().getPRNG()->drawFrom(fwTunedMovesProb);

	double fwProb = fwTunedMovesProb[iDraw];
	//std::cout << fwMovesProbability[iDraw] << " vs " << fwTunedMovesProb[iDraw] << std::endl;
	double nextScore = fwScores[iDraw];
	minimalMoveInfo_t fwMove = fwMoves[iDraw];

	Tree::edge_t fwStableEdge = fwMove.graftEdge;
	TreeNode *fwPtrB = fwMove.movingEdge.n1;
	TreeNode *fwPtrOther = fwMove.movingEdge.n2;

	// ----- re-apply move  ------- //
	tm.swapSubtrees(tree, fwPtrB, fwPtrOther, fwStableEdge);

	// enumerate backward
	long int newH = tree->getHashKey();
	assert(newH == fwMove.h);

	// Compute all 1 step forward moves
	vecMoveInfo_t bwMoves;
	std::vector<double> bwScores;
	std::vector<double> bwSplitProb;
	defineAllMovesSTNNI(tree, bwMoves, bwScores, bwSplitProb);

	// Resetting the Parsimony evalutator DAG to the proposed tree
	// This is required for us to reuse the DAG without full init
	fastFitchEvaluator->update(tree);

	// identify reverse move
	std::vector<double> bwMovesProbability(computeMovesProbability(nextScore, bwScores));
	std::vector<double> bwTunedMovesProb(computeMovesJointProbability(bwSplitProb, bwMovesProbability));

	bool found = false;
	size_t iMove = 0;
	for(size_t i=0; i<bwMoves.size(); ++i) {
		bool sameStableEdge = (bwMoves[i].graftEdge.n1 == fwStableEdge.n1) && (bwMoves[i].graftEdge.n2 == fwStableEdge.n2);
		bool sameMove1 = (fwMove.movingEdge.n1 == bwMoves[i].movingEdge.n2)  && (fwMove.movingEdge.n2 == bwMoves[i].movingEdge.n1);
		bool sameMove2 = (fwMove.movingEdge.n1 == bwMoves[i].path[1])  && (fwMove.movingEdge.n2 == bwMoves[i].path[0]);

		if(sameStableEdge && (sameMove1 || sameMove2)) {
			found = true;
			iMove = i;

			break;
		}
	}
	assert(found);

	// get probability
	minimalMoveInfo_t bwMove = bwMoves[iMove];
	double bwProb = bwTunedMovesProb[iMove];

	// Define the move
	// Register movement
	Move::sharedPtr_t newMove(new Move(currentH, newH, Move::GuidedSTNNI));

	// Set probability
	newMove->setForwardProbability(fwProb);
	newMove->setBackwardProbability(bwProb);
	newMove->setProbability(bwProb/fwProb);

	// Register involved nodes (edges)
	// Keep track of nodes <n1, n2, B, C/D>
	// n1 <--> n2 selected edge
	newMove->addInvolvedNode(fwStableEdge.n1->getId());
	newMove->addInvolvedNode(fwStableEdge.n2->getId());
	// node B
	newMove->addInvolvedNode(fwPtrB->getId());
	// node C or D
	newMove->addInvolvedNode(fwPtrOther->getId());

	newMove->addInvolvedNode(fwMove.path[0]->getId());
	newMove->addInvolvedNode(fwMove.path[1]->getId());

	// Reverse move
	tm.swapSubtrees(tree, fwPtrOther, fwPtrB, fwStableEdge);
	tree->clearUpdatedNodes();

	// Trying to prepare things for mapping
	split_info_t b1 = createSplitInfo(tree, fwPtrB, fwStableEdge.n1, sample);
	split_info_t b2 = createSplitInfo(tree, fwStableEdge.n1, fwStableEdge.n2, sample);
	split_info_t b3 = createSplitInfo(tree, fwStableEdge.n2, fwPtrOther, sample);
	std::vector<split_info_t> vecBefore = boost::assign::list_of(b1)(b2)(b3);

	// Swap the subtrees
	tm.swapSubtrees(tree, fwPtrB, fwPtrOther, fwStableEdge);

	// Trying to prepare things for mapping
	split_info_t a1 = createSplitInfo(tree, fwPtrOther, fwStableEdge.n1, sample);
	split_info_t a2 = createSplitInfo(tree, fwStableEdge.n1, fwStableEdge.n2, sample);
	split_info_t a3 = createSplitInfo(tree, fwStableEdge.n2, fwPtrB, sample);
	std::vector<split_info_t> vecAfter = boost::assign::list_of(a1)(a2)(a3);

	mapBranchLengths(vecBefore, vecAfter, sample);

	// Add nodes that where pending to be updated on the tree updated to the list:
	tree->addUpdatedNodes(originalUpdatedNodes);

	// Keep track of last tree hash key to avoid useless recomputation
	lastFinalH = tree->getHashKey();
	cacheFwMoves = fwMoves;
	cacheFwScores = fwScores;
	cacheFwSplitProb = fwSplitProb;

	cacheBwMoves = bwMoves;
	cacheBwScores = bwScores;
	cacheBwSplitProb = bwSplitProb;

	return newMove;
}

void AdaptiveGuidedSTNNI::extendRadius(size_t iStep, Tree::edge_t currentEdge, std::vector<Tree::edge_t> &edges, const size_t N_STEP) {

	iStep++;
	if(iStep > N_STEP) return;

	vecTN_t children = currentEdge.n1->getNonTerminalChildren(currentEdge.n2);

	for(size_t iC=0; iC<children.size(); ++iC) {
		Tree::edge_t edge = {currentEdge.n1, children[iC]};
		edges.push_back(edge);
		extendRadius(iStep, edge, edges, N_STEP);
	}

}


std::vector<Tree::edge_t> AdaptiveGuidedSTNNI::selectEdgesInRadius(const size_t N_STEP, Tree::sharedPtr_t tree, TreeNode* startingNode) {

	std::vector<Tree::edge_t> edges;

	vecTN_t nodes = startingNode->getNonTerminalChildren(NULL);

	for(size_t iN=0; iN<nodes.size(); ++iN) {
		Tree::edge_t edge = {startingNode, nodes[iN]};
		edges.push_back(edge);
		extendRadius(0, edge, edges, N_STEP);
	}

	return edges;
}


void AdaptiveGuidedSTNNI::defineMoveInRadiusSTNNI(Tree::sharedPtr_t tree, std::vector<Tree::edge_t> &edges, vecMoveInfo_t &moves, std::vector<double> &scores) {
	for(size_t iE=0; iE<edges.size(); ++iE) {
		Tree::edge_t stableEdge = edges[iE];
		if(stableEdge.n1->getId() < stableEdge.n2->getId()) std::swap(stableEdge.n1, stableEdge.n2);

		vecTN_t childrenN1 = stableEdge.n1->getChildren(stableEdge.n2); // {A,B}
		vecTN_t childrenN2 = stableEdge.n2->getChildren(stableEdge.n1); // {C,D
		assert(childrenN1.size()==2 && childrenN2.size()==2);

		for(size_t iC=0; iC<2; ++iC) {

			bool cross = (iC == 0);

			// Define who are swapped
			TreeNode *ptrA = childrenN1[0]; // Get A
			TreeNode *ptrB = childrenN1[1]; // Get B
			TreeNode *ptrOther, *ptrOther2;
			if(cross) {
				ptrOther = childrenN2[0]; // Get C
				ptrOther2 = childrenN2[1];
			} else {
				ptrOther = childrenN2[1]; // Get D
				ptrOther2 = childrenN2[0];
			}

			// Swap the subtrees
			tm.swapSubtrees(tree, ptrB, ptrOther, stableEdge);

			// Create and memorize node
			// Compute parsimony
			double score = queryParsimonyScore(tree);

			// Creating node
			std::vector<TreeNode*> tmpPath;
			tmpPath.push_back(ptrA);
			tmpPath.push_back(ptrOther2);
			minimalMoveInfo_t newMove = createMove(1, tree->getHashKey(), ptrB, ptrOther, stableEdge, tmpPath);

			// Register the move and the parsimony score
			moves.push_back(newMove);
			scores.push_back(score);

			// Swap the subtrees back
			tm.swapSubtrees(tree, ptrOther, ptrB, stableEdge);
		}
	}
}

Move::sharedPtr_t AdaptiveGuidedSTNNI::proposeLimitedSTNNIMoves(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	// Memorize pending change on the tree:
	std::vector<TreeNode*> originalUpdatedNodes(tree->getUpdatedNodes());

	// Get current tree hash
	long int currentH = tree->getHashKey();

	// First we want to make sure that the parsimony score are starting from a safe point
	double currentScore = 0.;
	if(lastFinalH == currentH && tree->getUpdatedNodes().empty()) {
		currentScore = queryParsimonyScore(tree);
	} else {
		currentScore = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();
		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addElement(currentH, currentScore);
		}
	}

	// Compute all 1 step forward moves
	size_t iStartingNode = aRNG->genUniformInt(0, tree->getInternals().size()-1);
	TreeNode *startingNode = tree->getInternals()[iStartingNode];

	std::vector<Tree::edge_t> fwEdges = selectEdgesInRadius(N_STEP, tree, startingNode);

	vecMoveInfo_t fwMoves;
	std::vector<double> fwScores;
	//std::vector<double> fwCCP;

	defineMoveInRadiusSTNNI(tree, fwEdges, fwMoves, fwScores);

	// Draw based on parsimony scores
	std::vector<double> fwMovesProbability(computeMovesProbability(currentScore, fwScores));
	size_t iDraw = Parallel::mpiMgr().getPRNG()->drawFrom(fwMovesProbability);

	double fwProb = fwMovesProbability[iDraw];
	double nextScore = fwScores[iDraw];
	minimalMoveInfo_t fwMove = fwMoves[iDraw];

	Tree::edge_t fwStableEdge = fwMove.graftEdge;
	TreeNode *fwPtrB = fwMove.movingEdge.n1;
	TreeNode *fwPtrOther = fwMove.movingEdge.n2;

	// ----- re-apply move  ------- //
	tm.swapSubtrees(tree, fwPtrB, fwPtrOther, fwStableEdge);

	// enumerate backward
	long int newH = tree->getHashKey();
	assert(newH == fwMove.h);

	// Compute all 1 step forward moves
	vecMoveInfo_t bwMoves;
	std::vector<double> bwScores;
	std::vector<double> bwCCP;
	std::vector<Tree::edge_t> bwEdges = selectEdgesInRadius(N_STEP, tree, startingNode);

	defineMoveInRadiusSTNNI(tree, bwEdges, bwMoves, bwScores);


	// Resetting the Parsimony evalutator DAG to the proposed tree
	// This is required for us to reuse the DAG without full init
	fastFitchEvaluator->update(tree);

	// identify reverse move
	std::vector<double> bwMovesProbability(computeMovesProbability(nextScore, bwScores));

	bool found = false;
	size_t iMove = 0;
	for(size_t i=0; i<bwMoves.size(); ++i) {
		bool sameStableEdge = (bwMoves[i].graftEdge.n1 == fwStableEdge.n1) && (bwMoves[i].graftEdge.n2 == fwStableEdge.n2);
		bool sameMove1 = (fwMove.movingEdge.n1 == bwMoves[i].movingEdge.n2)  && (fwMove.movingEdge.n2 == bwMoves[i].movingEdge.n1);
		bool sameMove2 = (fwMove.movingEdge.n1 == bwMoves[i].path[1])  && (fwMove.movingEdge.n2 == bwMoves[i].path[0]);

		if(sameStableEdge && (sameMove1 || sameMove2)) {
			found = true;
			iMove = i;

			break;
		}
	}
	assert(found);

	// get probability
	minimalMoveInfo_t bwMove = bwMoves[iMove];
	double bwProb = bwMovesProbability[iMove];

	// Define the move
	// Register movement
	Move::sharedPtr_t newMove(new Move(currentH, newH, Move::GuidedSTNNI));

	/*Tree::edge_t bwStableEdge = bwMove.graftEdge;
	TreeNode *bwPtrB = bwMove.movingEdge.n1;
	TreeNode *bwPtrOther = bwMove.movingEdge.n2;*/

	// Set probability
	newMove->setForwardProbability(fwProb);
	newMove->setBackwardProbability(bwProb);
	newMove->setProbability(bwProb/fwProb);

	// Register involved nodes (edges)
	// Keep track of nodes <n1, n2, B, C/D>
	// n1 <--> n2 selected edge
	newMove->addInvolvedNode(fwStableEdge.n1->getId());
	newMove->addInvolvedNode(fwStableEdge.n2->getId());
	// node B
	newMove->addInvolvedNode(fwPtrB->getId());
	// node C or D
	newMove->addInvolvedNode(fwPtrOther->getId());

	newMove->addInvolvedNode(fwMove.path[0]->getId());
	newMove->addInvolvedNode(fwMove.path[1]->getId());

	// Reverse move
	tm.swapSubtrees(tree, fwPtrOther, fwPtrB, fwStableEdge);
	tree->clearUpdatedNodes();

	// Trying to prepare things for mapping
	split_info_t b1 = createSplitInfo(tree, fwPtrB, fwStableEdge.n1, sample);
	split_info_t b2 = createSplitInfo(tree, fwStableEdge.n1, fwStableEdge.n2, sample);
	split_info_t b3 = createSplitInfo(tree, fwStableEdge.n2, fwPtrOther, sample);
	std::vector<split_info_t> vecBefore = boost::assign::list_of(b1)(b2)(b3);

	// Swap the subtrees
	tm.swapSubtrees(tree, fwPtrB, fwPtrOther, fwStableEdge);

	// Trying to prepare things for mapping
	split_info_t a1 = createSplitInfo(tree, fwPtrOther, fwStableEdge.n1, sample);
	split_info_t a2 = createSplitInfo(tree, fwStableEdge.n1, fwStableEdge.n2, sample);
	split_info_t a3 = createSplitInfo(tree, fwStableEdge.n2, fwPtrB, sample);
	std::vector<split_info_t> vecAfter = boost::assign::list_of(a1)(a2)(a3);

	mapBranchLengths(vecBefore, vecAfter, sample);

	// Add nodes that where pending to be updated on the tree updated to the list:
	tree->addUpdatedNodes(originalUpdatedNodes);

	// Keep track of last tree hash key to avoid useless recomputation
	lastFinalH = tree->getHashKey();

	return newMove;
}


} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
