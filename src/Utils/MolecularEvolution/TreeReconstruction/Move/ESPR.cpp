//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ESPR.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "ESPR.h"

#include <assert.h>
#include <boost/assign/list_of.hpp>

#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Parallel/RNG/RNG.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

ESPR::ESPR(ParameterBlock::BranchHelper *aBH) : TreeProposal(aBH){
}

ESPR::~ESPR() {
}

/**
 * Propose a new tree by doing an eSPR move.
 *
 * @param h Hash key of the current tree
 * @return The move produced
 */
Move::sharedPtr_t ESPR::proposeNewTree(const double pe, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	assert( pe >= 0 && pe <= 1.);

	long int h = tree->getHashKey();

	bool canChange = false;
	Tree::edge_t edgeToPrune;
	TreeNode *toGraft, *toExplore, *parent;
	while(!canChange) {
		// Randomly select an internal edge
		edgeToPrune = getRndInternalBranch(tree);

		// Label the subtrees
		canChange = tm.labelSubTrees(edgeToPrune, toGraft, toExplore);
		if(!canChange) {
			std::cout << "Cannot change!" << std::endl;
		}
	}

	// The tree may change, we then search for the direction
	parent = toGraft;
	TreeNode* toExploreClad;
	bool isBackwardConstrained = tm.findExploreDirection(toExplore, parent, toExploreClad);

	// We now have to find a grafting point
	parent = toExplore;
	Tree::edge_t graftEdge;
	std::vector<TreeNode*> path;
	size_t nEdge = tm.findGraftPoint(pe, toExploreClad, parent, graftEdge, path);
	bool isForwardConstrained = graftEdge.n2->isTerminal();

	// We keep track of old "toExplore neighbor" that will form the collapsed edge
	vecTN_t vecTN = toExplore->getChildren(toGraft);
	assert(vecTN.size() == 2);
	// Keep track of collapsed edge --> s1 is always equal to "toExploreClad"
	Tree::edge_t collapseEdge = {vecTN[0], vecTN[1]};
	if(collapseEdge.n1 != toExploreClad) std::swap(collapseEdge.n1, collapseEdge.n2);


	// Trying to prepare things for mapping
	split_info_t beforeMovedBL = createSplitInfo(tree, toGraft, toExplore, sample);
	split_info_t beforeLeftBL = createSplitInfo(tree, toExplore, collapseEdge.n2, sample);
	split_info_t beforeRightBL = createSplitInfo(tree, graftEdge.n1, graftEdge.n2, sample);
	split_info_t centralBL = createSplitInfo(tree, toExplore, toExploreClad, sample);
	assert(toExploreClad == path[0]);
	std::vector<split_info_t> vecBefore = boost::assign::list_of(beforeMovedBL)(beforeLeftBL)(beforeRightBL)(centralBL);
	//std::cout << path.size() << std::endl;
	//std::cout << "N Step : " << nEdge << std::endl;
	for(size_t i=1; i<path.size(); ++i) {
		split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
		vecBefore.push_back(tmpBL);
		//std::cout << "Push before." << std::endl;
	}

	// We now have to prune and graft
	//std::cout << "espr move " << std::endl;
	tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path);

	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	Move::sharedPtr_t newMove(new Move(h, newH, Move::ESPR));

	// Register involved nodes (edges)
	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	// m1 <--> m2 represents the edge to move (m1=toGraft, m2=toExplore)
	newMove->addInvolvedNode(toGraft->getId());
	newMove->addInvolvedNode(toExplore->getId());
	// s1 <--> s2 is the collapsed starting edge
	newMove->addInvolvedNode(collapseEdge.n1->getId());
	newMove->addInvolvedNode(collapseEdge.n2->getId());
	// e1 <--> e2 is the augmented ending edge
	newMove->addInvolvedNode(graftEdge.n1->getId());
	newMove->addInvolvedNode(graftEdge.n2->getId());

	// Keep track of path
	for(size_t iP=0; iP<path.size(); ++iP) {
		newMove->addInvolvedNode(path[iP]->getId());
	}

	// Trying to prepare things for mapping
	split_info_t afterMovedBL = createSplitInfo(tree, toGraft, toExplore, sample);
	split_info_t afterLeftBL = createSplitInfo(tree, collapseEdge.n1, collapseEdge.n2, sample);
	split_info_t afterRightBL = createSplitInfo(tree, toExplore, graftEdge.n2, sample);
	split_info_t afterCentralBL = createSplitInfo(tree, toExplore, graftEdge.n1, sample);
	std::vector<split_info_t> vecAfter = boost::assign::list_of(afterMovedBL)(afterLeftBL)(afterRightBL)(afterCentralBL);
	assert(graftEdge.n1 == path.back());
	for(size_t i=path.size()-1; i>0; --i) {
		split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
		vecAfter.push_back(tmpBL);
		//std::cout << "Push after." << std::endl;
	}

	mapBranchLengths(vecBefore, vecAfter, sample);

	// Set probability and define move info
	moveInfoESPR_t moveInfoESPR = BOTH_UNCONSTRAINED;
	if(isForwardConstrained == isBackwardConstrained) {
		newMove->setProbability(1.);
		moveInfoESPR = isForwardConstrained ? BOTH_CONSTRAINED : BOTH_UNCONSTRAINED;
	} else if(isForwardConstrained) {
		newMove->setProbability((1.-pe)/0.5);
		moveInfoESPR = UNCONSTRAINED_TO_CONSTRAINED;
	} else if(isBackwardConstrained) {
		newMove->setProbability(0.5/(1.-pe));
		moveInfoESPR = CONSTRAINED_TO_UNCONSTRAINED;
	}

	// Add some info to move
	newMove->setNEdgeDistance(nEdge);
	newMove->setMoveInfo(static_cast<int>(moveInfoESPR));

	return newMove;
}

std::string ESPR::toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;
	ss << "toGraft : " << toGraft->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "toExplore : " << toExplore->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s1 : " << collapseEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s2 : " << collapseEdge.n2->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e1 : " << graftEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e2 : " << graftEdge.n2->toString() << std::endl;
	for(size_t i=0; i<path.size(); ++i) {
		ss << "**" << std::endl;
		ss << "p" << i << " : " << path[i]->toString() << std::endl;
	}
	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
