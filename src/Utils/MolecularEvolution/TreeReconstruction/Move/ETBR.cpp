//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ETBR.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "ETBR.h"

#include <assert.h>
#include <boost/assign/list_of.hpp>

#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Parallel/RNG/RNG.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

ETBR::ETBR(ParameterBlock::BranchHelper *aBH) : TreeProposal(aBH){
}

ETBR::~ETBR() {
}


/**
 * Propose a new tree by doing an eTBR move.
 *
 * @param h Hash key of the current tree
 * @return The move produced
 */
Move::sharedPtr_t ETBR::proposeNewTree(const double pe, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	assert( pe >= 0 && pe <= 1.);

	long int h = tree->getHashKey();

	Tree::edge_t stableEdge = getRndInternalBranch(tree);

	double moveProb = 1.;
	std::vector<size_t> nEdges;
	std::vector<size_t> involvedNodes;

	involvedNodes.push_back(stableEdge.n1->getId());
	involvedNodes.push_back(stableEdge.n2->getId());

	// Draw move for each side.
	for(size_t iSPR=0; iSPR<2; ++iSPR) {

		// First "direction"
		size_t iDir = aRNG->genUniformInt(0, 1);
		TreeNode* toGraft = stableEdge.n1;
		TreeNode* toGraftParent = stableEdge.n2;
		vecTN_t directions = toGraftParent->getChildren(toGraft);

		if(directions[iDir]->isTerminal()) iDir = 1-iDir;

		// Find path and graft point
		Tree::edge_t graftEdge;
		std::vector<TreeNode*> path;
		tm.findGraftPoint(pe, directions[iDir], toGraftParent, graftEdge, path);
		nEdges.push_back(path.size());

		// Check constraints for fw move
		bool isForwardConstrained = graftEdge.n2->isTerminal();

		// Get collapse edge
		Tree::edge_t collapseEdge = {directions[0], directions[1]};
		if(collapseEdge.n1 != directions[iDir]) std::swap(collapseEdge.n1, collapseEdge.n2);

		// Check constraint for backward move
		bool isBackwardConstrained = collapseEdge.n2->isTerminal();

		//std::cout << path.size() << " ------ " << (isForwardConstrained ? "1" : "0") << " -- " << (isBackwardConstrained ? "1" : "0") << std::endl;

		if(path.size() > 0) {

			// Map branches (fw
			split_info_t beforeMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t beforeLeftBL = createSplitInfo(tree, toGraftParent, collapseEdge.n2, sample);
			split_info_t beforeRightBL = createSplitInfo(tree, graftEdge.n1, graftEdge.n2, sample);
			split_info_t centralBL = createSplitInfo(tree, toGraftParent, path.front(), sample);
			std::vector<split_info_t> vecBefore = boost::assign::list_of(beforeMovedBL)(beforeLeftBL)(beforeRightBL)(centralBL);
			for(size_t i=1; i<path.size(); ++i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecBefore.push_back(tmpBL);
			}

			// We now have to prune and graft
			tm.pruneAndGraft(tree, toGraft, toGraftParent, collapseEdge, graftEdge, path);

			// Trying to prepare things for mapping
			split_info_t afterMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t afterLeftBL = createSplitInfo(tree, collapseEdge.n1, collapseEdge.n2, sample);
			split_info_t afterRightBL = createSplitInfo(tree, toGraftParent, graftEdge.n2, sample);
			split_info_t afterCentralBL = createSplitInfo(tree, toGraftParent, graftEdge.n1, sample);
			std::vector<split_info_t> vecAfter = boost::assign::list_of(afterMovedBL)(afterLeftBL)(afterRightBL)(afterCentralBL);
			assert(graftEdge.n1 == path.back());
			for(size_t i=path.size()-1; i>0; --i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecAfter.push_back(tmpBL);
			}

			mapBranchLengths(vecBefore, vecAfter, sample);
		}

		// Pack first part of the move
		involvedNodes.push_back(collapseEdge.n1->getId());
		involvedNodes.push_back(collapseEdge.n2->getId());
		involvedNodes.push_back(graftEdge.n1->getId());
		involvedNodes.push_back(graftEdge.n2->getId());
		involvedNodes.push_back(path.size()); // Trick to keep track of the variable number of node in the path
		// Keep track of path
		for(size_t iP=0; iP<path.size(); ++iP) {
			involvedNodes.push_back(path[iP]->getId());
		}

		std::swap(stableEdge.n1, stableEdge.n2);

		double prob = 1.0;
		if(isForwardConstrained == isBackwardConstrained) {
			prob = 1.0;
		} else if(isForwardConstrained) {
			prob = (1.-pe)/0.5;
		} else if(isBackwardConstrained) {
			prob = 0.5/(1.-pe);
		}
		moveProb *= prob;
	}

	assert(nEdges.size() == 2);
	if(nEdges[0] == 0 && nEdges[1] == 0) {
		moveProb = 0.;
	}

	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	Move::sharedPtr_t newMove(new Move(h, newH, Move::ETBR));
	newMove->setInvolvedNodes(involvedNodes);
	newMove->setNEdgeDistances(nEdges);
	newMove->setProbability(moveProb);

	//std::cout << moveProb << std::endl;

	return newMove;
}


ETBR::unpackedMoveId_t ETBR::unpackMove(const std::vector<size_t>& invNodes ) {

	ETBR::unpackedMoveId_t unpackedMove;

	//for(size_t i=0; i<invNodes.size(); ++i) std::cout << invNodes[i] << std::endl;

	size_t iN=0;
	unpackedMove.edgeN1 = invNodes[iN++];
	unpackedMove.edgeN2 = invNodes[iN++];

	unpackedMove.collapseE1N1 = invNodes[iN++];
	unpackedMove.collapseE1N2 = invNodes[iN++];

	unpackedMove.graftE1N1 = invNodes[iN++];
	unpackedMove.graftE1N2 = invNodes[iN++];

	size_t nEdge1 = invNodes[iN++];
	for(size_t i=0; i<nEdge1; ++i) unpackedMove.path1.push_back(invNodes[iN++]);

	unpackedMove.collapseE2N1 = invNodes[iN++];
	unpackedMove.collapseE2N2 = invNodes[iN++];

	unpackedMove.graftE2N1 = invNodes[iN++];
	unpackedMove.graftE2N2 = invNodes[iN++];

	size_t nEdge2 = invNodes[iN++];
	for(size_t i=0; i<nEdge2; ++i) unpackedMove.path2.push_back(invNodes[iN++]);
	assert(iN == invNodes.size());

	return unpackedMove;
}


std::string ETBR::toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;
	ss << "toGraft : " << toGraft->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "toExplore : " << toExplore->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s1 : " << collapseEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s2 : " << collapseEdge.n2->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e1 : " << graftEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e2 : " << graftEdge.n2->toString() << std::endl;
	for(size_t i=0; i<path.size(); ++i) {
		ss << "**" << std::endl;
		ss << "p" << i << " : " << path[i]->toString() << std::endl;
	}
	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
