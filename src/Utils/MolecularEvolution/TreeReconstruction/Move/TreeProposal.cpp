//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeProposal.cpp
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#include "TreeProposal.h"

#include <assert.h>
#include <stddef.h>

#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "ParameterBlock/BlockModifier/TreeModifier/BranchHelper.h"
#include "Parallel/Manager/MpiManager.h"

#include <algorithm>

namespace MolecularEvolution {
namespace TreeReconstruction {


TreeProposal::TreeProposal(ParameterBlock::BranchHelper *aBH) :
		aRNG(Parallel::mpiMgr().getPRNG()), branchHelper(aBH) {

	assert(aBH);

}

TreeProposal::TreeProposal(Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH) :
		aRNG(Parallel::mpiMgr().getPRNG()), bipartitionMonitor(aBM), branchHelper(aBH) {

	assert(aBH);

}

TreeProposal::~TreeProposal() {
}

/**
 * Get a random internal edge of a tree.
 *
 * @param tree The tree
 * @return The randomly selected edge of the tree
 */
Tree::edge_t TreeProposal::getRndInternalBranch(Tree::sharedPtr_t tree) {
	TreeNode *intNode = tree->getRoot();
	while(intNode == tree->getRoot()) {
		size_t id = aRNG->genUniformInt(0, tree->getInternals().size()-1);
		intNode = tree->getInternals()[id];
	}

	Tree::edge_t edge = {intNode->getParent(), intNode};
	return edge;
}

TreeProposal::split_info_t TreeProposal::createSplitInfo(Tree::sharedPtr_t tree, TreeNode *node1, TreeNode *node2, Sampler::Sample &sample) {

	AdaptiveESPR::split_info_t splitInfo;
	splitInfo.n1 = node1;
	splitInfo.n2 = node2;
	//if(node1->getParent() == node1->getParent()) std::swap(splitInfo.n1, splitInfo.n2);
	splitInfo.bl = branchHelper->getBranchLength(node1, node2, sample);
	//splitInfo.split = bipartitionMonitor->getSplit(tree, splitInfo.n1, splitInfo.n2);
	splitInfo.split = tree->findBipartitions(splitInfo.n1, splitInfo.n2);
	//assert(splitInfo.split == splitIF && "split A not ok");

	if(splitInfo.split[0]) splitInfo.split.flip();

	if(bipartitionMonitor) {
		splitInfo.hasStats = bipartitionMonitor->getSplitsStatsBL(splitInfo.split, splitInfo.count, splitInfo.meanBL, splitInfo.varBL);
	} else {
		splitInfo.hasStats = false;
	}

	return splitInfo;
}

std::string TreeProposal::displaySplitInfo(split_info_t &aSplit) {
	std::stringstream ss;
	ss << aSplit.n1->getId() << "\t" << aSplit.n2->getId() << "\t" << aSplit.bl << "\t";
	for(size_t i=0; i<aSplit.split.size(); ++i) ss << (aSplit.split[i] ? "*" : ".");
	return ss.str();
}

void TreeProposal::mapBranchLengths(std::vector<split_info_t> &beforeSplit, std::vector<split_info_t> &afterSplit, Sampler::Sample &sample) {

	std::vector<int> mapping(beforeSplit.size(), -1);
	std::vector<double> tempBLs;

	/*std::cout << "Before : " << std::endl;
	for(size_t i=0; i<beforeSplit.size(); ++i) {
		std::cout << displaySplitInfo(beforeSplit[i]) << std::endl;
		std::cout << displaySplitInfo(afterSplit[i]) << std::endl;
		std::cout << "-----------------------------------------" << std::endl;
	}*/

	// Locate identical split
	for(size_t i=0; i<beforeSplit.size(); ++i) {
		bool found = false;
		for(size_t j=0; j<afterSplit.size(); ++j) {
			if(beforeSplit[i].split == afterSplit[j].split) { // If split found keep BL
				mapping[j] = i;
				afterSplit[j].bl = beforeSplit[i].bl;
				found = true;
				break;
			}
		}

		if(!found) {
			tempBLs.push_back(beforeSplit[i].bl); // If not keep track of BL
		}
	}

	std::random_shuffle(tempBLs.begin(), tempBLs.end() );

	// Randomly affect BL
	for(size_t i=0; i<mapping.size(); ++i) {
		if(mapping[i] >= 0) continue; // This split has already a new BL

		afterSplit[i].bl = tempBLs.back();
		tempBLs.pop_back();
	}

	// Set BLs
	for(size_t i=0; i<afterSplit.size(); ++i) {
		branchHelper->setBranchLength(afterSplit[i].bl, afterSplit[i].n1, afterSplit[i].n2, sample);
	}

	/*std::cout << "After : " << std::endl;
	for(size_t i=0; i<beforeSplit.size(); ++i) {
		std::cout << displaySplitInfo(beforeSplit[i]) << std::endl;
		std::cout << displaySplitInfo(afterSplit[i]) << std::endl;
		std::cout << "-----------------------------------------" << std::endl;
	}*/
}



} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
