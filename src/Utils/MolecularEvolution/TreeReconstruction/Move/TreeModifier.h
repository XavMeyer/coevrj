//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeModifier.h
 *
 * @date Dec 13 2018
 * @author meyerx
 * @brief
 */
#ifndef TREEMODIFIER_H_
#define TREEMODIFIER_H_

#include "Parallel/Parallel.h" // IWYU pragma: keep

#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Bipartition/BipartitionMonitor.h"

#include "Utils/Code/CustomProfiling.h"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

class RNG;

namespace ParameterBlock {
	class BranchHelper;
}

namespace MolecularEvolution {
namespace TreeReconstruction {

class TreeModifier {
public:
	enum edge_direction_t {FROM_N1_TO_N2, FROM_N2_TO_N1};
public:

	TreeModifier();
	~TreeModifier();

	Tree::edge_t getRndInternalBranch(Tree::sharedPtr_t tree);

	edge_direction_t getEdgeDirection(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2);

	bool containsRootNode(Tree::sharedPtr_t tree, TreeNode* inNode, TreeNode* outNode);
	void reverseDirection(Tree::sharedPtr_t tree, std::vector<TreeNode*> path);

	void addEdge(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2, edge_direction_t direction);
	TreeModifier::edge_direction_t removeEdge(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2);

	void swapSubtrees(Tree::sharedPtr_t tree, TreeNode* ptrAdjN1, TreeNode* ptrAdjN2, Tree::edge_t edge);

	bool labelSubTrees(Tree::edge_t &edge, TreeNode* &toGraft, TreeNode* &toExplore);
	bool canBeExplored(TreeNode *toExplore, TreeNode *parent);
	bool findExploreDirection(TreeNode *toExplore, TreeNode *parent, TreeNode* &toExploreClad);
	size_t findGraftPoint(const double pe, TreeNode *toExplore, TreeNode *parent, Tree::edge_t &graftPoint, std::vector<TreeNode*> &path);
	bool pruneAndGraft(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
			Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path);


	struct path_cost_t {
		double remEdgeSplitFreq, addPathSplitFreq, remPathSplitFreq;
	};

	struct info_move_ESPR_t {
		TreeNode *toGraft, *toExplore;
		Tree::edge_t collapseEdge, graftEdge;
		Tree::edge_path_t directedPath;
		double probability, remEdgeSplitFreq, addEdgeSplitFreq, addPathSplitFreq, remPathSplitFreq;

		bool isReverseMove(const info_move_ESPR_t& revIts) {
			bool reverse = true;
			reverse = reverse && (toGraft->getId() == revIts.toGraft->getId());
			reverse = reverse && (toExplore->getId() == revIts.toExplore->getId());
			reverse = reverse && ((collapseEdge.n1 == revIts.graftEdge.n1 && collapseEdge.n2== revIts.graftEdge.n2) || (collapseEdge.n1 == revIts.graftEdge.n2 && collapseEdge.n2== revIts.graftEdge.n1));
			reverse = reverse && ((graftEdge.n1 == revIts.collapseEdge.n1 && graftEdge.n2== revIts.collapseEdge.n2) || (graftEdge.n1 == revIts.collapseEdge.n2 && graftEdge.n2== revIts.collapseEdge.n1));
			return reverse;
		}
	};

	path_cost_t computePruneAndMoveCost(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
										Tree::edge_path_t directedPath, Bipartition::BipartitionMonitor::sharedPtr_t bipartitionMonitor);
	std::vector<info_move_ESPR_t> createGraftMoves(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
												   Tree::edge_path_t directedPath, Bipartition::BipartitionMonitor::sharedPtr_t bipartitionMonitor,
												   path_cost_t pathCost);


protected:

	CustomProfiling cp;
	boost::accumulators::accumulator_set< double, boost::accumulators::features< boost::accumulators::tag::mean > > accSwap;
	boost::accumulators::accumulator_set< double, boost::accumulators::features< boost::accumulators::tag::mean > > accPrune;

	const RNG *aRNG;

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* TREEMODIFIER_H_ */
