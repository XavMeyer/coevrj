//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file STNNI.cpp
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#include "STNNI.h"

#include <assert.h>

#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Parallel/Manager/../RNG/RNG.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

STNNI::STNNI(ParameterBlock::BranchHelper *aBH) :
		TreeProposal(aBH) {
}

STNNI::~STNNI() {
}

Move::sharedPtr_t STNNI::proposeNewTree(const double pe, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	long int h = tree->getHashKey();

	// Randomly select an internal edge
	Tree::edge_t edge = getRndInternalBranch(tree);

	// Chose which kind of exchanges
	//		{A, B} --> n1 <--edge--> n2 <-- {C, D}
	// If cross exchange :
	//		{A, C} --> n1 <--edge--> n2 <-- {B, D}
	// Else
	//		{A, D} --> n1 <--edge--> n2 <-- {C, B}

	//bool swapEdge = (aRNG->genUniformInt(0, 1) == 0);
	//if(swapEdge) std::swap(edge.n1, edge.n2);

	bool cross = (aRNG->genUniformInt(0, 1) == 0);
	vecTN_t childrenN1 = edge.n1->getChildren(edge.n2); // {A,B}
	vecTN_t childrenN2 = edge.n2->getChildren(edge.n1); // {C,D
	assert(childrenN1.size()==2 && childrenN2.size()==2);

	// Define who are swapped
	TreeNode *ptrA = childrenN1[0]; // Get A
	TreeNode *ptrB = childrenN1[1]; // Get B
	TreeNode *ptrOther, *ptrOther2;
	if(cross) {
		ptrOther = childrenN2[0]; // Get C
		ptrOther2 = childrenN2[1];
	} else {
		ptrOther = childrenN2[1]; // Get D
		ptrOther2 = childrenN2[0];
	}

	// Trying to prepare things for mapping
	split_info_t b1 = createSplitInfo(tree, ptrB, edge.n1, sample);
	split_info_t b2 = createSplitInfo(tree, edge.n1, edge.n2, sample);
	split_info_t b3 = createSplitInfo(tree, edge.n2, ptrOther, sample);
	std::vector<split_info_t> vecBefore = boost::assign::list_of(b1)(b2)(b3);

	// Swap the subtrees
	tm.swapSubtrees(tree, ptrB, ptrOther, edge);

	// Trying to prepare things for mapping
	split_info_t a1 = createSplitInfo(tree, ptrOther, edge.n1, sample);
	split_info_t a2 = createSplitInfo(tree, edge.n1, edge.n2, sample);
	split_info_t a3 = createSplitInfo(tree, edge.n2, ptrB, sample);
	std::vector<split_info_t> vecAfter = boost::assign::list_of(a1)(a2)(a3);

	mapBranchLengths(vecBefore, vecAfter, sample);


	long int newH = tree->getHashKey();

	// Register movement
	Move::sharedPtr_t newMove(new Move(h, newH, Move::STNNI));

	// Set probability
	newMove->setProbability(1.);

	// Register involved nodes (edges)
	// Keep track of nodes <n1, n2, B, C/D>
	// n1 <--> n2 selected edge
	newMove->addInvolvedNode(edge.n1->getId());
	newMove->addInvolvedNode(edge.n2->getId());
	// node B
	newMove->addInvolvedNode(ptrB->getId());
	// node C or D
	newMove->addInvolvedNode(ptrOther->getId());

	newMove->addInvolvedNode(ptrA->getId());
	newMove->addInvolvedNode(ptrOther2->getId());

	//std::cout << newMove->toString() << std::endl;

	return newMove;
}

std::string STNNI::toString(Tree::sharedPtr_t tree, TreeNode *ptrB, TreeNode *ptrOther, TreeNode *ptrN1, TreeNode *ptrN2) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;
	ss << "ptrB : " << ptrB->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "prtOther : " << ptrOther->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "n1 : " << ptrN1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "n2 : " << ptrN2->toString() << std::endl;
	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
