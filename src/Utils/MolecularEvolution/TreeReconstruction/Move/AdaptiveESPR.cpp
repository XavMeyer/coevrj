//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveESPR.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "AdaptiveESPR.h"

#include <assert.h>

#include "Utils/Code/CustomProfiling.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Parallel/RNG/RNG.h"
#include "ParameterBlock/BlockModifier/TreeModifier/BranchHelper.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "ParameterBlock/BlockStats/Config/Container/BlockStatCfg.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Mean/BatchEvoMU.h"


#include <boost/math/distributions/gamma.hpp>
#include <boost/math/policies/policy.hpp>
#include <boost/math/special_functions/fpclassify.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {

/*const double AdaptiveESPR::EPSILON_FREQ = 0.0001;
const double AdaptiveESPR::POWER_FREQ = 1.8;
const AdaptiveESPR::bias_t AdaptiveESPR::BIAS_TYPE = BOTH_BIASED;*/

const bool AdaptiveESPR::SCALE_BRANCHES = true;
const bool AdaptiveESPR::MAPPING_BRANCHES = true;

AdaptiveESPR::AdaptiveESPR(Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH) :
		TreeProposal(aBM, aBH){
	cntIt=0;
	assert(aBM);

	ParameterBlock::Config::ConfigFactory::sharedPtr_t configFactory = ParameterBlock::Config::ConfigFactory::createDefaultFactory();
	ParameterBlock::Config::BlockStatCfg::sharedPtr_t blockCfg = configFactory->createConfig(1);
	evoMu = new ParameterBlock::BatchEvoMU(blockCfg->MU, blockCfg->CONV_CHECKER);
	evoMu->setInitialMean(5.);

}

AdaptiveESPR::~AdaptiveESPR() {
}



double AdaptiveESPR::getEpsilon(double minEpsilon) const {

	// Decrease epsilon from 100 to 0.0001 over approx 50k iterations
	//double epsilon = 100.*exp(-(double)cntIt/4000.);
	//double scale = 10.*exp(-(double)cntIt/4000.);
	//scale = std::max(minEpsilon, scale);
	double scale = bipartitionMonitor->getEpsilon(minEpsilon, 250.*minEpsilon);
	evoMu->addVal(scale);
	//std::cout << "Scale = " << evoMu->getMean() << std::endl;
	//std::cout << "Evo Mu : " << std::scientific << evoMu->getMean() << std::endl; // FIXME DEBUG OUTPUT
	double epsilon = Parallel::mpiMgr().getPRNG()->genGamma(1., evoMu->getMean());

	//std::cout << std::scientific << "Scale = " << evoMu->getMean() << "\t" << epsilon << std::endl;

	return epsilon;
}


/**
 * Propose a new tree by doing an AdaptiveESPR move.
 *
 * @param h Hash key of the current tree
 * @return The move produced
 */
Move::sharedPtr_t AdaptiveESPR::proposeNewTree(stepNumber_t nStep, bias_t aBias, double aEpsilon, double aPower, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	Move::sharedPtr_t newMove;

	double epsilon = getEpsilon(aEpsilon) / tree->getInternalEdges().size();
	double power = bipartitionMonitor->getPower(aPower);

	if(nStep == ONE_STEP) {
		newMove = proposeOneEdgeMove(aBias, epsilon, power, tree, sample);
	} else {
		newMove = proposeTwoEdgesMove(aBias, epsilon, power, tree, sample);
	}

	cntIt++;

	return newMove;
}

void AdaptiveESPR::signalMoveHistory(bool accepted) {
#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bipartitionMonitor->registerESPR(accepted, bpHistory);
	bpHistory.added.clear();
	bpHistory.removed.clear();
	bpHistory.revAdded.clear();
	bpHistory.revRemoved.clear();
	bpHistory.revDelta.clear();
	bpHistory.stable.clear();
	bpHistory.delta.clear();
	bpHistory.moveDelta.clear();
	bpHistory.nStep.clear();
	bpHistory.probRatio.clear();
#endif
}

/**
 * Get a random internal edge of a tree.
 *
 * @param tree The tree
 * @return The randomly selected edge of the tree
 */
double AdaptiveESPR::getBiasedMove(bias_t biasType, double epsilonFreq, double powerFreq,
								   Tree::sharedPtr_t tree, TreeNode* &toGraft, TreeNode* &toCollapse,
								   TreeNode* &toExplore, TreeNode* &toExtend) {

	double forwardProb = 1.0;

	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> eProbability = getEdgeProbabilities(epsilonFreq, powerFreq, vecBF);

	assert(eProbability.size() == vecBF.size());

	Tree::edge_t edge;
	if(biasType == BOTH_BIASED || biasType == REM_EDGE_BIASED) {
		size_t id = aRNG->drawFrom(eProbability);
		edge = vecBF[id].edge;
		forwardProb *= eProbability[id];
		//std::cout << vecBF.size() << "\t" << eProbability.size() << "\t" << id << std::endl;
		assert(id < vecBF.size());
	} else {
		size_t id = aRNG->genUniformInt(0, eProbability.size()-1);
		assert(id < vecBF.size());
		edge = vecBF[id].edge;
		forwardProb *= 1./((double)eProbability.size());
	}

	if(aRNG->genUniformDbl() >0.5) std::swap(edge.n1, edge.n2);


	// Get A, B, C ,D
	assert(edge.n1);
	assert(edge.n2);
	vecTN_t childrenAB(edge.n1->getChildren(edge.n2));
	if(aRNG->genUniformDbl() >0.5) std::swap(childrenAB[0], childrenAB[1]);
	vecTN_t childrenCD(edge.n2->getChildren(edge.n1));
	if(aRNG->genUniformDbl() >0.5) std::swap(childrenCD[0], childrenCD[1]);

	Bipartition::split_t splitA = tree->findBipartitions(edge.n1, childrenAB[0]);
	Bipartition::split_t splitB = tree->findBipartitions(edge.n1, childrenAB[1]);
	Bipartition::split_t splitC = tree->findBipartitions(edge.n2, childrenCD[0]);
	Bipartition::split_t splitD = tree->findBipartitions(edge.n2, childrenCD[1]);

	// Old split
	// AB
	Bipartition::split_t newSplitAB = splitA;
	newSplitAB |= splitB;
	/*for(size_t i=0; i<newSplitAB.size(); ++i) {
		newSplitAB[i] = splitB[i] || splitA[i];
	}*/
	if(newSplitAB[0]) newSplitAB.flip();

	// Possible permutation and frequencies
	// A to C
	Bipartition::split_t newSplitAC = splitA;
	newSplitAC |= splitC;
	/*for(size_t i=0; i<newSplitAC.size(); ++i) {
		newSplitAC[i] = splitC[i] || splitA[i];
	}*/
	if(newSplitAC[0]) newSplitAC.flip();
	double newSplitFreqAC = bipartitionMonitor->getSplitFrequency(newSplitAC);

	// A to D
	Bipartition::split_t newSplitAD = splitA;
	newSplitAD |= splitD;
	/*for(size_t i=0; i<newSplitAD.size(); ++i) {
		newSplitAD[i] = splitD[i] || splitA[i];
	}*/
	if(newSplitAD[0]) newSplitAD.flip();
	double newSplitFreqAD = bipartitionMonitor->getSplitFrequency(newSplitAD);

	double randUnif = aRNG->genUniformDbl();

	double probAC = (epsilonFreq+newSplitFreqAC)/(2.*epsilonFreq+newSplitFreqAC+newSplitFreqAD);
	double probAD = (epsilonFreq+newSplitFreqAD)/(2.*epsilonFreq+newSplitFreqAC+newSplitFreqAD);

	//double freq = 0.;
	if(biasType == BOTH_BIASED || biasType == ADD_EDGE_BIASED) {
		if(randUnif < probAC) {
			toGraft = childrenAB[0];
			toCollapse = edge.n1;
			toExplore = edge.n2;
			toExtend = childrenCD[0];
			//freq = newSplitFreqAC;
			forwardProb *= probAC;
		} else {
			toGraft = childrenAB[0];
			toCollapse = edge.n1;
			toExplore = edge.n2;
			toExtend = childrenCD[1];
			// = newSplitFreqAD;
			forwardProb *= probAD;
		}
	} else {
		if(randUnif < 0.5) {
			toGraft = childrenAB[0];
			toCollapse = edge.n1;
			toExplore = edge.n2;
			toExtend = childrenCD[0];
			//freq = newSplitFreqAC;
			forwardProb *= 0.5;
		} else {
			toGraft = childrenAB[0];
			toCollapse = edge.n1;
			toExplore = edge.n2;
			toExtend = childrenCD[1];
			//freq = newSplitFreqAD;
			forwardProb *= 0.5;
		}
	}
	assert(fabs(1.-(probAC+probAD)) < 1.e-7);


#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	double stableFreq = bipartitionMonitor->getSplitFrequency(splitA);
	double newSplitFreqAB = bipartitionMonitor->getSplitFrequency(newSplitAB);
	bpHistory.stable.push_back(stableFreq);
	bpHistory.added.push_back(freq);
	bpHistory.removed.push_back(newSplitFreqAB);
	bpHistory.delta.push_back(freq-newSplitFreqAB);
	bpHistory.moveDelta.push_back(freq-newSplitFreqAB);
	bpHistory.nStep.push_back(1);
#endif

	assert(toGraft);
	assert(toCollapse);
	assert(toExplore);
	assert(toExtend);

	assert(!toCollapse->isTerminal());
	assert(!toExplore->isTerminal());

	return forwardProb;
}

double AdaptiveESPR::getBiasedMoveReverseProbability(bias_t biasType, double epsilonFreq, double powerFreq,
													 Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
													 Tree::edge_t collapseEdge, Tree::edge_t graftEdge) {

	double backwardProb = 1.0;

	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> eProbability = getEdgeProbabilities(epsilonFreq, powerFreq, vecBF);

	int iEdge = -1;
	for(size_t iE=0; iE<vecBF.size(); ++iE) {
		if((vecBF[iE].edge.n1 == graftEdge.n1 && vecBF[iE].edge.n2 == toExplore) ||
		   (vecBF[iE].edge.n1 == toExplore && vecBF[iE].edge.n2 == graftEdge.n1)) {
				 iEdge = iE;
				 break;
			 }
	}
	assert((iEdge >= 0) && (iEdge < eProbability.size()));

	if(biasType == BOTH_BIASED || biasType == REM_EDGE_BIASED) {
		backwardProb *= eProbability[iEdge];
	} else {
		backwardProb *= 1./((double)eProbability.size());
	}

	//std::cout << "Prob to select edge Extended : " << eProbability[iEdge] << std::endl;

	//std::cout << "Branch to remove : < " << edge.n1->getId() << ", " << edge.n2->getId() << " >" << std::endl;

	// Get A, B, C ,D
	vecTN_t childrenAX(toExplore->getChildren(graftEdge.n1));
	vecTN_t childrenOther(graftEdge.n1->getChildren(toExplore));
	assert(childrenAX.size() == 2);
	assert(childrenOther.size() == 2);

	// Displaced clad and new neighbour (C or D)
	//Bipartition::split_t splitA = bipartitionMonitor->getSplit(tree, toExplore, toGraft);
	Bipartition::split_t splitA = tree->findBipartitions(toExplore, toGraft);
	//assert(splitA == splitA2 && "split A not ok");


	TreeNode* nodeNN = (childrenAX[0] == toGraft) ? childrenAX[1] : childrenAX[0];
	//Bipartition::split_t splitNN = bipartitionMonitor->getSplit(tree, toExplore, nodeNN);
	Bipartition::split_t splitNN = tree->findBipartitions(toExplore, nodeNN);
	//assert(splitNN == splitNN2 && "split NN not ok");


	// Old B and old neighbour of new neighbour (if C then D otherwise C)s
	assert(collapseEdge.n1 == graftEdge.n1);
	TreeNode* nodeB = collapseEdge.n2;
	//Bipartition::split_t splitB = bipartitionMonitor->getSplit(tree, collapseEdge.n1, nodeB);
	Bipartition::split_t splitB = tree->findBipartitions(collapseEdge.n1, nodeB);
	//assert(splitB == splitB2 && "split B2 not ok");


	TreeNode* nodeON = (childrenOther[0] == nodeB) ? childrenOther[1] : childrenOther[0];
	//Bipartition::split_t splitON = bipartitionMonitor->getSplit(tree, collapseEdge.n1, nodeON);
	Bipartition::split_t splitON = tree->findBipartitions(collapseEdge.n1, nodeON);
	//assert(splitON == splitON2 && "split ON not ok");

	/*std::cout << "Node A : " << toGraft->getId() << std::endl;
	std::cout << "Node B : " << nodeB->getId() << std::endl;
	std::cout << "Node C/D : " << nodeNN->getId() << std::endl;
	std::cout << "Node C/D : " << nodeON->getId() << std::endl;
	std::cout << "Node Graft : " << toGraft->getId() << std::endl;
	std::cout << "Node Collapse : " << toExplore->getId() << std::endl;
	std::cout << "Node Explore : " << graftEdge.n1->getId() << std::endl;
	std::cout << "Node Extend : " << graftEdge.n2->getId() << std::endl;*/

	// Old split
	// AB
	Bipartition::split_t oldSplitAB = splitA;
	oldSplitAB |= splitB;
	/*for(size_t i=0; i<oldSplitAB.size(); ++i) {
		oldSplitAB[i] = splitB[i] || splitA[i];
	}*/
	if(oldSplitAB[0]) oldSplitAB.flip();
	double oldSplitFreqAB = bipartitionMonitor->getSplitFrequency(oldSplitAB);
	//std::cout << "AB freq : " << oldSplitFreqAB << std::endl << " --- " << std::endl;

	// A to other (C or D)
	Bipartition::split_t otherSplit = splitA;
	otherSplit |= splitON;
	/*for(size_t i=0; i<otherSplit.size(); ++i) {
		otherSplit[i] = splitA[i] || splitON[i];
	}*/
	if(otherSplit[0]) otherSplit.flip();
	double otherSplitFreq = bipartitionMonitor->getSplitFrequency(otherSplit);
	//std::cout << bipartitionMonitor->getSplitAsString(otherSplit) << std::endl;

	/*for(size_t i=0; i<otherSplit.size(); ++i) {
		if(otherSplit[i]) std::cout << "1";
		else std::cout << "0";
	}
	std::cout << std::endl;*/



	if(biasType == BOTH_BIASED || biasType == ADD_EDGE_BIASED) {
		double probAB = (epsilonFreq+oldSplitFreqAB)/(2*epsilonFreq+oldSplitFreqAB+otherSplitFreq);
		double probOther = (epsilonFreq+otherSplitFreq)/(2*epsilonFreq+oldSplitFreqAB+otherSplitFreq);
		assert(fabs(1.-(probAB+probOther)) < 1.e-7);
		//std::cout << probAB << " vs " << probOther << std::endl;
		//std::cout << "Prob to select edge collapsed : " << probAB << std::endl;

		backwardProb *= probAB;
	} else {
		backwardProb *= 0.5;
	}

#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.revAdded.push_back(oldSplitFreqAB);
	bpHistory.revRemoved.push_back(otherSplitFreq);
	bpHistory.revDelta.push_back(oldSplitFreqAB-otherSplitFreq);
#endif

	return backwardProb;
}


std::vector<AdaptiveESPR::involved_tree_structures_t> AdaptiveESPR::defineContiguousEdgeMoves(
	double epsilonFreq, double powerFreq,
	Tree::contiguous_edge_pair_t cep,
	TreeNode* nodeX, Bipartition::split_t splitX,
	vecTN_t children1, std::vector<Bipartition::split_t> split1,
	vecTN_t children2, std::vector<Bipartition::split_t> split2) {

	const double MOVE_SIZE = 2.0;

	std::vector<involved_tree_structures_t> vecMove;
	for(size_t i1=0; i1<children1.size(); ++i1) {
		for(size_t i2=0; i2<children2.size(); ++i2) {
			// Sufficient info to create the move -- path is missing but kept in directed cep
			involved_tree_structures_t move;
			move.toGraft = children1[i1];
			move.toExplore = cep.n1;
			move.collapseEdge.n1 = cep.n2;
			move.collapseEdge.n2 = children1[1-i1];
			move.graftEdge.n1 = cep.n3;
			move.graftEdge.n2 = children2[i2];
			move.directedCEP = cep;

			// Define the probability
			Bipartition::split_t movingSplit = split1[i1];
			Bipartition::split_t stayingSplit = split1[1-i1];
			Bipartition::split_t extendedSplit = split2[i2];
			Bipartition::split_t untouchedSplit = split2[1-i2];

			// Removed split 1
			Bipartition::split_t removedSplit_1 = movingSplit;
			removedSplit_1 |= stayingSplit;
			removedSplit_1 |= splitX;
			/*for(size_t iS=0; iS<removedSplit_1.size(); ++iS) {
				removedSplit_1[iS] = movingSplit[iS] || stayingSplit[iS] || splitX[iS];
			}*/
			double freqRem1 = bipartitionMonitor->getSplitFrequency(removedSplit_1);

			// Removed split 2
			Bipartition::split_t removedSplit_2 = movingSplit;
			removedSplit_2 |= stayingSplit;
			/*for(size_t iS=0; iS<removedSplit_2.size(); ++iS) {
				removedSplit_2[iS] = movingSplit[iS] || stayingSplit[iS];
			}*/
			double freqRem2 = bipartitionMonitor->getSplitFrequency(removedSplit_2);

			// Added split 1
			Bipartition::split_t addedSplit_1 = movingSplit;
			addedSplit_1 |= extendedSplit;
			/*for(size_t iS=0; iS<addedSplit_1.size(); ++iS) {
				addedSplit_1[iS] = movingSplit[iS] || extendedSplit[iS];
			}*/
			double freqAdd1 = bipartitionMonitor->getSplitFrequency(addedSplit_1);

			// Added split 2
			Bipartition::split_t addedSplit_2 = stayingSplit;
			addedSplit_2 |= splitX;
			/*for(size_t iS=0; iS<addedSplit_2.size(); ++iS) {
				addedSplit_2[iS] = stayingSplit[iS] || splitX[iS];
			}*/
			double freqAdd2 = bipartitionMonitor->getSplitFrequency(addedSplit_2);


			// Method 1 - just the gain from the move with regard to the new bipartitions
			//double unormProbability1 = epsilonFreq+(freqAdd1+freqAdd2)/MOVE_SIZE; // Original
			double unormProbability1 = epsilonFreq+(freqAdd1*freqAdd2); // Conditional
			//double unormProbability1 = epsilonFreq+(freqAdd1*freqAdd2); // Overall ?
			move.probability = unormProbability1;

			move.stableFreq = bipartitionMonitor->getSplitFrequency(movingSplit);
			move.addFreq = freqAdd1 + freqAdd2;
			move.remFreq = freqRem1 + freqRem2;

			// Method 2 - favorize delta close to 0 and penalize the other ones
			//double deltaMove = ((freqAdd1+freqAdd2)-(freqRem1+freqRem2))/MOVE_SIZE;
			//double unormProbability2 = EPSILON_FREQ + pow(1.0 - fabs(deltaMove), 1.8);

			//std::cout << move.toGraft->getId() << " -- " << move.toExplore->getId()  << " -- " << move.collapseEdge.n1->getId() << " -- " << move.collapseEdge.n2->getId()  << " -- " << move.graftEdge.n1->getId()  << " -- " << move.graftEdge.n2->getId();
			//std::cout << " -- Pos Move = " << freqAdd1+freqAdd2 << " -- Neg Move = " << freqRem1+freqRem2 << " -- prob = " << unormProbability1 << std::endl;

			vecMove.push_back(move);
		}
	}

	return vecMove;
}

AdaptiveESPR::involved_tree_structures_t AdaptiveESPR::getBiasedPairEdgeMove(bias_t biasType, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree) {

	double forwardProb = 1.0;

	// Get vector of contiguous pair of internal edges
	std::vector< Tree::contiguous_edge_pair_t > vecCEP = tree->getContiguousInternalEdges();
	//std::cout << "[FW] vecCEP size : " << vecCEP.size() << std::endl;

	// Get their probability
	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> vecProb = getContiguousEdgePairProbabilities(epsilonFreq, powerFreq, tree, vecBF);

	Tree::contiguous_edge_pair_t cep;
	if(biasType == BOTH_BIASED || biasType == REM_EDGE_BIASED) {
		size_t id = aRNG->drawFrom(vecProb);
		cep = vecCEP[id];
		forwardProb *= vecProb[id];
	} else {
		size_t id = aRNG->genUniformInt(0, vecCEP.size()-1.);
		cep = vecCEP[id];
		forwardProb *= 1./((double)vecCEP.size());
	}

	//std::cout << cep.n1->getId() << " -- " << cep.n2->getId() << " -- "<< cep.n3->getId() << " : " << std::scientific << vecProb[id] << std::endl;

	// Build list of potential moves and define their probabililty
	vecTN_t childrenAB = cep.n1->getChildren(cep.n2);
	vecTN_t childrenCD = cep.n3->getChildren(cep.n2);
	vecTN_t childrenX = cep.n2->getChildren(cep.n3);

	// Node X is the first node of the clad placed inbetween both edges
	TreeNode* nodeX = (childrenX[0] == cep.n1) ? childrenX[1] : childrenX[0];
	//Bipartition::split_t splitX = bipartitionMonitor->getSplit(tree, cep.n2, nodeX);
	Bipartition::split_t splitX = tree->findBipartitions(cep.n2, nodeX);
	//assert(splitX == splitX2 && "split X not ok");


	std::vector<Bipartition::split_t> splitAB, splitCD;
	assert(childrenAB.size() == childrenCD.size());
	for(size_t iC=0; iC<childrenAB.size(); ++iC) {
		splitAB.push_back(tree->findBipartitions(cep.n1, childrenAB[iC]));
		//splitAB.push_back(bipartitionMonitor->getSplit(tree, cep.n1, childrenAB[iC]));
		//Bipartition::split_t splitAB2 = tree->findBiparitions(cep.n1, childrenAB[iC]);
		//assert(splitAB.back() == splitAB2 && "split AB not ok");
		splitCD.push_back(tree->findBipartitions(cep.n3, childrenCD[iC]));
		//splitCD.push_back(bipartitionMonitor->getSplit(tree, cep.n3, childrenCD[iC]));
		//Bipartition::split_t splitCD2 = tree->findBiparitions(cep.n3, childrenCD[iC]);
		//assert(splitCD.back() == splitCD2 && "split CD not ok");
	}

	// From AB to CD
	std::vector<involved_tree_structures_t> vecMove1 = defineContiguousEdgeMoves(epsilonFreq, powerFreq, cep, nodeX, splitX, childrenAB, splitAB, childrenCD, splitCD);
	// From CD to AB
	Tree::contiguous_edge_pair_t revCEP = cep;
	std::swap(revCEP.n1, revCEP.n3);
	std::vector<involved_tree_structures_t> vecMove2 = defineContiguousEdgeMoves(epsilonFreq, powerFreq, revCEP, nodeX, splitX, childrenCD, splitCD, childrenAB, splitAB);

	//std::cerr << vecMove1.size() << " vs " << vecMove2.size() << std::endl;

	double nrmCst = 0.;
	std::vector<involved_tree_structures_t> vecMove;
	for(size_t i=0; i<vecMove1.size(); ++i) {
		vecMove.push_back(vecMove1[i]);
		nrmCst += vecMove1[i].probability;
	}

	for(size_t i=0; i<vecMove2.size(); ++i) {
		vecMove.push_back(vecMove2[i]);
		nrmCst += vecMove2[i].probability;
	}

	std::vector<double> vecProb2;
	for(size_t i=0; i<vecMove.size(); ++i) {
		//std::cout << vecMove[i].toGraft->getId() << " -- " << vecMove[i].toExplore->getId()  << " -- " << vecMove[i].collapseEdge.n1->getId() << " -- " << vecMove[i].collapseEdge.n2->getId()  << " -- " << vecMove[i].graftEdge.n1->getId()  << " -- " << vecMove[i].graftEdge.n2->getId();
		//std::cout << " -- prob " << vecMove[i].probability << " -- nrmCst = " << nrmCst << " -- norm prob prob = " << vecMove[i].probability/nrmCst << std::endl;
		vecProb2.push_back(vecMove[i].probability/nrmCst);
	}

	// Draw one move from the list
	involved_tree_structures_t its;
	if(biasType == BOTH_BIASED || biasType == ADD_EDGE_BIASED) {
		size_t id2 = aRNG->drawFrom(vecProb2);
		its = vecMove[id2];
		forwardProb *= vecProb2[id2];
		its.probability = forwardProb;
	} else {
		size_t id2 = aRNG->genUniformInt(0, vecMove.size()-1.);
		its = vecMove[id2];
		forwardProb *= 1./(vecMove.size()-1.);
		its.probability = forwardProb;
	}

#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.stable.push_back(its.stableFreq);
	bpHistory.added.push_back(its.addFreq);
	bpHistory.removed.push_back(its.remFreq);
	bpHistory.delta.push_back(its.addFreq-its.remFreq);
	bpHistory.moveDelta.push_back(its.addFreq-its.remFreq);
	bpHistory.nStep.push_back(2);
#endif

	//size_t id2 = aRNG->drawFrom(vecProb2);
	//involved_tree_structures_t its = vecMove[id2];

	//std::cout << its.toGraft->getId() << " -- " << its.toExplore->getId()  << " -- " << its.collapseEdge.n1->getId() << " -- " << its.collapseEdge.n2->getId()  << " -- " << its.graftEdge.n1->getId()  << " -- " << its.graftEdge.n2->getId() << std::endl;
	//std::cout << " Prob = " << vecProb2[id2] << std::endl;

	return its;
}

double AdaptiveESPR::getBiasedPairEdgeMoveReverseProbability(bias_t biasType, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, involved_tree_structures_t its) {

	double backwardProb = 1.0;

	// Get vector of contiguous pair of internal edges
	std::vector< Tree::contiguous_edge_pair_t > vecCEP = tree->getContiguousInternalEdges();

	//std::cout << "[BW] vecCEP size : " << vecCEP.size() << std::endl;

	// Get their probability
	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> vecProb = getContiguousEdgePairProbabilities(epsilonFreq, powerFreq, tree, vecBF);

	// Define the reverse CEP
	Tree::contiguous_edge_pair_t revCEP = its.directedCEP;
	//std::cout << its.directedCEP.n1->getId() << " -- " << its.directedCEP.n2->getId() << " -- " << its.directedCEP.n3->getId() << std::endl;
	std::swap(revCEP.n2, revCEP.n3);
	//std::cout << revCEP.n1->getId() << " -- " << revCEP.n2->getId() << " -- " << revCEP.n3->getId() << std::endl;

	int id = -1;
	for(size_t iC=0; iC<vecCEP.size(); ++iC) {
	//std::cout << "[" << iC << "] " << vecCEP[iC].n1->getId() << " -- " << vecCEP[iC].n2->getId() << " -- " << vecCEP[iC].n3->getId() << std::endl;
		if(vecCEP[iC] == revCEP) {
			id = iC;
			break;
		}
	}
	assert(id >= 0);

	if(biasType == BOTH_BIASED || biasType == REM_EDGE_BIASED) {
		backwardProb *= vecProb[id];
	} else {
		backwardProb *= 1./((double)vecCEP.size());
	}

	// Get probability of moves
	vecTN_t childrenAB = revCEP.n1->getChildren(revCEP.n2);
	vecTN_t childrenCD = revCEP.n3->getChildren(revCEP.n2);
	vecTN_t childrenX = revCEP.n2->getChildren(revCEP.n3);

	// Node X is the first node of the clad placed inbetween both edges
	TreeNode* nodeX = (childrenX[0] == revCEP.n1) ? childrenX[1] : childrenX[0];
	//Bipartition::split_t splitX = bipartitionMonitor->getSplit(tree, revCEP.n2, nodeX);
	Bipartition::split_t splitX = tree->findBipartitions(revCEP.n2, nodeX);
	//assert(splitX == splitX2 && "split X not ok");

	std::vector<Bipartition::split_t> splitAB, splitCD;
	assert(childrenAB.size() == childrenCD.size());
	for(size_t iC=0; iC<childrenAB.size(); ++iC) {
		splitAB.push_back(tree->findBipartitions(revCEP.n1, childrenAB[iC]));
		//splitAB.push_back(bipartitionMonitor->getSplit(tree, revCEP.n1, childrenAB[iC]));
		//Bipartition::split_t splitAB2 = tree->findBiparitions(revCEP.n1, childrenAB[iC]);
		//assert(splitAB.back() == splitAB2 && "split AB not ok");
		splitCD.push_back(tree->findBipartitions(revCEP.n3, childrenCD[iC]));
		//splitCD.push_back(bipartitionMonitor->getSplit(tree, revCEP.n3, childrenCD[iC]));
		//Bipartition::split_t splitCD2 = tree->findBiparitions(revCEP.n3, childrenCD[iC]);
		//assert(splitCD.back() == splitCD2 && "split CD not ok");
	}

	// From AB to CD
	std::vector<involved_tree_structures_t> vecMove1 = defineContiguousEdgeMoves(epsilonFreq, powerFreq, revCEP, nodeX, splitX, childrenAB, splitAB, childrenCD, splitCD);
	// From CD to AB
	Tree::contiguous_edge_pair_t revCEP2 = revCEP;
	std::swap(revCEP2.n1, revCEP2.n3);
	std::vector<involved_tree_structures_t> vecMove2 = defineContiguousEdgeMoves(epsilonFreq, powerFreq, revCEP2, nodeX, splitX, childrenCD, splitCD, childrenAB, splitAB);

	double nrmCst = 0.;
	std::vector<involved_tree_structures_t> vecMove;
	for(size_t i=0; i<vecMove1.size(); ++i) {
		vecMove.push_back(vecMove1[i]);
		nrmCst += vecMove1[i].probability;
	}

	for(size_t i=0; i<vecMove2.size(); ++i) {
		vecMove.push_back(vecMove2[i]);
		nrmCst += vecMove2[i].probability;
	}

	std::vector<double> vecProb2;
	int moveId = -1;
	for(size_t i=0; i<vecMove.size(); ++i) {
		//std::cout << vecMove[i].toGraft->getId() << " -- " << vecMove[i].toExplore->getId()  << " -- " << vecMove[i].collapseEdge.n1->getId() << " -- " << vecMove[i].collapseEdge.n2->getId()  << " -- " << vecMove[i].graftEdge.n1->getId()  << " -- " << vecMove[i].graftEdge.n2->getId();
		//std::cout << " -- prob " << vecMove[i].probability << " -- nrmCst = " << nrmCst << " -- norm prob prob = " << vecMove[i].probability/nrmCst << std::endl;
		vecProb2.push_back(vecMove[i].probability/nrmCst);
		if(vecMove[i].isReverseMove(its)) {
			moveId = i;
		}
	}
	assert(moveId >= 0);


	if(biasType == BOTH_BIASED || biasType == ADD_EDGE_BIASED) {
		backwardProb *= vecProb2[moveId];
	} else {
		backwardProb *= 1./(vecMove.size()-1.);
	}

	//backwardProb *= vecProb2[moveId];
#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.revAdded.push_back(vecMove[moveId].addFreq);
	bpHistory.revRemoved.push_back(vecMove[moveId].remFreq);
	bpHistory.revDelta.push_back(vecMove[moveId].addFreq-vecMove[moveId].remFreq);
#endif

	return backwardProb;
}


std::string AdaptiveESPR::displayBranchInfo(Tree::sharedPtr_t tree, TreeNode *n1, TreeNode *n2, Sampler::Sample &sample) {


	TreeNode *pNode, *cNode;

	if(n1->getParent() == n2) {
		pNode = n1;
		cNode = n2;
	} else if (n2->getParent() == n1) {
		pNode = n2;
		cNode = n1;
	} else {
		assert(false);
	}


	//Bipartition::split_t aSplit = bipartitionMonitor->getSplit(tree, pNode, cNode);
	Bipartition::split_t aSplit = tree->findBipartitions(pNode, cNode);
	//assert(aSplit == split2 && "split2 not ok");

	std::string aStrSplit = bipartitionMonitor->getSplitAsString(aSplit);
	double aBL = branchHelper->getBranchLength(pNode, cNode, sample);

	std::stringstream ss;
	ss << n1->getId() << "\t" << n2->getId() << "\t" << aBL << "\t" << aStrSplit;
	return ss.str();

}


std::pair<double, double> AdaptiveESPR::applyBranchModifier(split_info_t beforeBL, split_info_t afterBL, Sampler::Sample &sample) {

	//double probBL = 1.0;
	std::pair<double, double> probabilities(1.,1.);

	// Automate that for all branches
	if(beforeBL.hasStats && afterBL.hasStats) {

		double bfrShape, bfrScale;
		double afrShape, afrScale;
		beforeBL.defineGammaParameters(bfrShape, bfrScale);
		afterBL.defineGammaParameters(afrShape, afrScale);

		double newVal = Parallel::mpiMgr().getPRNG()->genGamma(afrShape, afrScale);
		//std::cout << std::scientific << newVal << "\t" << afrShape << "\t" << afrScale << "\t" << beforeBL.meanBL << "\t" << beforeBL.varBL << "\t" << beforeBL.skewnessBL << "\t" << beforeBL.kurtosisBL << std::endl;
		double oldVal = beforeBL.bl;
		//std::cout << std::scientific << oldVal << "\t" << bfrShape << "\t" << bfrScale << "\t" << afterBL.meanBL << "\t" << afterBL.varBL << "\t" << afterBL.skewnessBL << "\t" << afterBL.kurtosisBL << std::endl;

		boost::math::gamma_distribution<> bwDistr(bfrShape, bfrScale);
		boost::math::gamma_distribution<> fwDistr(afrShape, afrScale);
		//std::cout << "Done distr" << std::endl;

		double fwProb = boost::math::pdf(fwDistr, newVal);
		//std::cout << "Fw ok" << std::endl;
		double bwProb = boost::math::pdf(bwDistr, oldVal);
		//std::cout << "Bw ok" << std::endl;

		//probBL *= (bwProb/fwProb);

		branchHelper->setBranchLength(newVal, afterBL.n1, afterBL.n2, sample);

		//std::cout << "New branch length from full indep : " << newVal << " : move prob = " << probBL << std::endl;
		probabilities.first = fwProb;
		probabilities.second = bwProb;

	} else {
		// only swap
		// do nothing
		//std::cout << "No change" << std::endl;
	}

	return probabilities;
}




Move::sharedPtr_t AdaptiveESPR::proposeOneEdgeMove(bias_t biasType, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	size_t nEdge  = 0;
	long int h = tree->getHashKey();
	moveInfo_t moveInfoESPR = DEFAULT;
	Move::sharedPtr_t newMove;

	//CustomProfiling cp;

	TreeNode *toGraft, *toCollapse, *toExplore, *toExtend;
	//cp.startTime();
	std::vector<split_info_t> fwSplitInfo;
	double forwardProb = getBiasedMove(biasType, epsilonFreq, powerFreq, tree, toGraft, toCollapse, toExplore, toExtend);

	//double ccp1 = bipartitionMonitor->computeTreeCCP(tree);
	//std::cout << "[1] Moving node : " << toCollapse->getId() << " -- before freq : " << bipartitionMonitor->defineNodeFrequency(tree, toCollapse) << " -- tree prob = " << ccp1;

	Tree::edge_t graftEdge = {toExplore, toExtend};
	//assert(toExplore->isParent(toExtent));
	// We keep track of old "toExplore neighbor" that will form the collapsed edge
	vecTN_t vecTN = toCollapse->getChildren(toGraft);
	assert(vecTN.size() == 2);
	// Keep track of collapsed edge --> s1 is always equal to "toExploreClad"

	Tree::edge_t collapseEdge = {vecTN[0], vecTN[1]};

	if(collapseEdge.n1 != toExplore) std::swap(collapseEdge.n1, collapseEdge.n2);
	assert(collapseEdge.n1 != collapseEdge.n2);

	split_info_t beforeCentralBL = createSplitInfo(tree, toCollapse, toExplore, sample);
	split_info_t beforeMovedBL = createSplitInfo(tree, toGraft, toCollapse, sample);
	split_info_t beforeLeftBL = createSplitInfo(tree, toCollapse, collapseEdge.n2, sample);
	split_info_t beforeRightBL = createSplitInfo(tree, toExplore, toExtend, sample);

	/* DISPLAY BEFORE STATE */
	/*std::cout << "--------------------------------------------------------" << std::endl;
	std::cout << "1-Before: Moved branch - " << displayBranchInfo(tree, toGraft, toCollapse, sample, bh) << std::endl;
	std::cout << "1-Before: Traversed branch - " << displayBranchInfo(tree, toCollapse, toExplore, sample, bh) << std::endl;
	std::cout << "1-Before: Extended branch - " << displayBranchInfo(tree, toExplore, toExtend, sample, bh) << std::endl;
	std::cout << "1-Before: Collapsed branch - " << displayBranchInfo(tree, toCollapse, collapseEdge.n2, sample, bh) << std::endl;*/
	double beforeBridgeBranchLength = branchHelper->getBranchLength(toCollapse, toExplore, sample);
	//cp.endTime();
	//double timeBiasedMove = cp.duration();
	/* DISPLAY BEFORE STATE */



	std::vector<TreeNode*> path(1, toExplore);
	// We now have to prune and graft
	//cp.startTime();
	bool reversed = tm.pruneAndGraft(tree, toGraft, toCollapse, collapseEdge, graftEdge, path);
	//cp.endTime();
	//double timeApplyMove = cp.duration();

	//cp.startTime();
	double backwardProb = getBiasedMoveReverseProbability(biasType, epsilonFreq, powerFreq, tree, toGraft, toCollapse, collapseEdge, graftEdge);


	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	newMove.reset(new Move(h, newH, Move::AdaptiveESPR));

	// Register involved nodes (edges)
	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	// m1 <--> m2 represents the edge to move (m1=toGraft, m2=toExplore)
	newMove->addInvolvedNode(toGraft->getId());
	newMove->addInvolvedNode(toCollapse->getId());
	// s1 <--> s2 is the collapsed starting edge
	newMove->addInvolvedNode(collapseEdge.n1->getId());
	newMove->addInvolvedNode(collapseEdge.n2->getId());
	// e1 <--> e2 is the augmented ending edge
	newMove->addInvolvedNode(graftEdge.n1->getId());
	newMove->addInvolvedNode(graftEdge.n2->getId());

	// Keep track of path
	for(size_t iP=0; iP<path.size(); ++iP) {
		newMove->addInvolvedNode(path[iP]->getId());
	}

	//std::cout << std::scientific << "Forward prob = " << forwardProb << " -- Backward prob = " << backwardProb  << " -- Ratio = " << backwardProb/forwardProb << std::endl;
#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.probRatio.push_back(backwardProb/forwardProb);
#endif

	/*if(forwardProb < 0.0) {
		std::cerr << newH << std::endl;
		forwardProb *= -1.0;
	}*/

	//std::cerr << backwardProb/forwardProb << std::endl;

	// Add some info to move
	newMove->setNEdgeDistance(nEdge);
	newMove->setMoveInfo(static_cast<int>(moveInfoESPR));

	double afterBridgeBranchLength = branchHelper->getBranchLength(toExplore, toCollapse, sample);

	if(beforeBridgeBranchLength != afterBridgeBranchLength && MAPPING_BRANCHES) {
		double aBL1 = branchHelper->getBranchLength(toCollapse, toExtend, sample);
		double aBL2 = branchHelper->getBranchLength(toExplore, collapseEdge.n2, sample);

		if(aBL1 == beforeBridgeBranchLength) {
			//std::cout << "Swapping1 : " << afterBridgeBranchLength << "\t" << beforeBridgeBranchLength << std::endl;
			branchHelper->setBranchLength(afterBridgeBranchLength, toCollapse, toExtend, sample);
			branchHelper->setBranchLength(beforeBridgeBranchLength, toExplore, toCollapse, sample);
		} else if(aBL2 == beforeBridgeBranchLength) {
			//std::cout << "Swapping2 : " << afterBridgeBranchLength << "\t" << beforeBridgeBranchLength << std::endl;
			branchHelper->setBranchLength(afterBridgeBranchLength, toExplore, collapseEdge.n2, sample);
			branchHelper->setBranchLength(beforeBridgeBranchLength, toExplore, toCollapse, sample);
		} else {
			assert(false);
		}
		//std::cout << "is reversed - swapping : " << beforeBridgeBranchLength << " vs " << afterBridgeBranchLength << std::endl;
	}

	split_info_t afterCentralBL = createSplitInfo(tree, toExplore, toCollapse, sample);
	split_info_t afterMovedBL = createSplitInfo(tree, toGraft, toCollapse, sample);
	split_info_t afterLeftBL = createSplitInfo(tree, toExplore, collapseEdge.n2, sample);
	split_info_t afterRightBL = createSplitInfo(tree, toCollapse, toExtend, sample);

	double fwProbBL = 1.0;
	double bwProbBL = 1.0;
	if(SCALE_BRANCHES) {
		std::pair<double, double> probs = applyBranchModifier(beforeCentralBL, afterCentralBL, sample);
		fwProbBL *= probs.first;
		bwProbBL *= probs.second;
		//bwProbBL *= probs.first; // FIXME FOR TESTING WITH "WRONG" PROBABILITY
		//fwProbBL *= probs.second; // FIXME FOR TESTING WITH "WRONG" PROBABILITY
	}

	//cp.endTime();
	//double timeRevBiasedMove = cp.duration();

	/* DISPLAY AFTER STATE */
	/*std::cout << "********************************************************" << std::endl;
	std::cout << "1-after: Grafted branch - " << displayBranchInfo(tree, toGraft, toCollapse, sample, bh) << std::endl;
	std::cout << "1-after: New central branch - " << displayBranchInfo(tree, toExplore, toCollapse, sample, bh) << std::endl;
	std::cout << "1-after: Extended branch - " << displayBranchInfo(tree, toCollapse, toExtend, sample, bh) << std::endl;
	std::cout << "1-after: Collapsed branch - " << displayBranchInfo(tree, toExplore, collapseEdge.n2, sample, bh) << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl;*/
	/* DISPLAY AFTER STATE */

	double finalFwProb = forwardProb*fwProbBL;
	double finalBwProb = backwardProb*bwProbBL;
	newMove->setForwardProbability(finalFwProb);
	newMove->setBackwardProbability(finalBwProb);

	newMove->setProbability(finalBwProb/finalFwProb);

	/** DEEEEEEEBUUUUUUUUUUGGGGGGGGGGG *********/
	/*Bipartition::split_t split;
	std::string splitStr(".............*.**..........*.............................*.........");
	for(size_t i=0; i<splitStr.size(); ++i) {
		bool bit = (splitStr[i] == '*');
		split.push_back(bit);
	}
	if(beforeCentralBL.split == split) {
		std::cout << finalFwProb << "\t" << finalBwProb << "\t" << bipartitionMonitor->getSplitFrequency(split) << std::endl;
	}*/
	/** DEEEEEEEBUUUUUUUUUUGGGGGGGGGGG *********/


	//double ccp2 = bipartitionMonitor->computeTreeCCP(tree);
	//std::cout << " -- after freq : " << bipartitionMonitor->defineNodeFrequency(tree, toCollapse) << " -- tree prob = " << ccp2;
	//std::cout << " -- ratio = " << ccp2/ccp1 << " -- ratio2 = " << (ccp2/ccp1)*(finalBwProb/finalFwProb);
	//std::cout << "M1, " << ccp2/ccp1 << ", " << (finalBwProb/finalFwProb) << ", " << (ccp2/ccp1)*(finalBwProb/finalFwProb);

	//double sum = timeBiasedMove + timeApplyMove + timeRevBiasedMove;
	//std::cout << std::scientific << timeBiasedMove << "\t" << timeApplyMove << "\t" << timeRevBiasedMove << std::endl;
	//std::cout << std::fixed << 100.*timeBiasedMove/sum << "\t" << 100.*timeApplyMove/sum << "\t" << 100.*timeRevBiasedMove/sum << std::endl;

	return newMove;
}

Move::sharedPtr_t AdaptiveESPR::proposeTwoEdgesMove(bias_t biasType, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	size_t nEdge  = 0;
	long int h = tree->getHashKey();
	moveInfo_t moveInfoESPR = DEFAULT;
	Move::sharedPtr_t newMove;

	//std::cout << tree->getIdString() << std::endl;
	involved_tree_structures_t its = getBiasedPairEdgeMove(biasType, epsilonFreq, powerFreq, tree);
	double forwardProb = its.probability;

	//std::cout << "[2] Before frequencies : " << bipartitionMonitor->defineNodeFrequency(tree, its.toExplore);
	//std::cout << ",\t" << bipartitionMonitor->defineNodeFrequency(tree, its.directedCEP.n2);
	//std::cout << ",\t" << bipartitionMonitor->defineNodeFrequency(tree, its.directedCEP.n3);
	//double ccp1 = bipartitionMonitor->computeTreeCCP(tree);
	//std::cout << " -- tree prob = " << ccp1 << std::endl;

	/*std::cout << "Node Graft : " << its.toGraft->toString() << std::endl;
	std::cout << "Node Explore : " << its.toExplore->toString() << std::endl;
	std::cout << "Node Graft.n1 : " << its.graftEdge.n1->toString() << std::endl;
	std::cout << "Node Graft.n2 : " << its.graftEdge.n2->toString() << std::endl;
	std::cout << "Node Collapse.n1 : " << its.collapseEdge.n1->toString() << std::endl;
	std::cout << "Node Collapse.n2 : " << its.collapseEdge.n2->toString() << std::endl;
	std::cout << "Node CEP.n1 : " << its.directedCEP.n1->toString() << std::endl;
	std::cout << "Node CEP.n2 : " << its.directedCEP.n2->toString() << std::endl;
	std::cout << "Node CEP.n3 : " << its.directedCEP.n3->toString() << std::endl;*/

	/* DISPLAY BEFORE STATE */
	/*std::cout << "--------------------------------------------------------" << std::endl;
	std::cout << "2-Before: Moved branch - " << displayBranchInfo(tree, its.toGraft, its.toExplore, sample, bh) << std::endl;
	std::cout << "2-Before: Traversed branch1 - " << displayBranchInfo(tree, its.directedCEP.n1, its.directedCEP.n2, sample, bh) << std::endl;
	std::cout << "2-Before: Traversed branch2 - " << displayBranchInfo(tree, its.directedCEP.n2, its.directedCEP.n3, sample, bh) << std::endl;
	std::cout << "2-Before: Extended branch - " << displayBranchInfo(tree, its.graftEdge.n1, its.graftEdge.n2, sample, bh) << std::endl;
	std::cout << "2-Before: Collapsed branch - " << displayBranchInfo(tree, its.toExplore, its.collapseEdge.n2, sample, bh) << std::endl;*/
	/* DISPLAY BEFORE STATE */

	split_info_t beforeMoved = createSplitInfo(tree, its.toGraft, its.toExplore, sample);
	split_info_t beforeTrav1 = createSplitInfo(tree, its.directedCEP.n1, its.directedCEP.n2, sample);
	split_info_t beforeTrav2 = createSplitInfo(tree, its.directedCEP.n2, its.directedCEP.n3, sample);
	split_info_t beforeRight = createSplitInfo(tree, its.graftEdge.n1, its.graftEdge.n2, sample);
	split_info_t beforeLeft = createSplitInfo(tree, its.toExplore, its.collapseEdge.n2, sample);
	std::vector<split_info_t> vecBefore = boost::assign::list_of(beforeMoved)(beforeTrav1)(beforeTrav2)(beforeRight)(beforeLeft);

    //std::string initialTree = tree->getIdString();

	std::vector<TreeNode*> path;
	path.push_back(its.directedCEP.n2);
	path.push_back(its.directedCEP.n3);


	Tree::edge_t collapseEdge = its.collapseEdge;
	if(collapseEdge.n1 != its.directedCEP.n2) std::swap(collapseEdge.n1, collapseEdge.n2);
	assert(collapseEdge.n1 == its.directedCEP.n2);
	assert(collapseEdge.n1 != collapseEdge.n2);
	bool reverse = tm.pruneAndGraft(tree, its.toGraft, its.toExplore, its.collapseEdge, its.graftEdge, path);

	double backwardProb = getBiasedPairEdgeMoveReverseProbability(biasType, epsilonFreq, powerFreq, tree, its);

	if(reverse && MAPPING_BRANCHES) {
		//std::cout << "ESPR2-Reversed" << std::endl;
		double aBL1 = branchHelper->getBranchLength(its.directedCEP.n2, its.directedCEP.n3, sample);
		double aBL2 = branchHelper->getBranchLength(its.directedCEP.n3, its.toExplore, sample);

		branchHelper->setBranchLength(aBL1, its.directedCEP.n3, its.toExplore, sample);
		branchHelper->setBranchLength(aBL2, its.directedCEP.n2, its.directedCEP.n3, sample);
	}

	split_info_t afterMoved = createSplitInfo(tree, its.toGraft, its.toExplore, sample);
	split_info_t afterTrav1 = createSplitInfo(tree, its.directedCEP.n2, its.directedCEP.n3, sample);
	split_info_t afterTrav2 = createSplitInfo(tree, its.directedCEP.n3, its.toExplore, sample);
	split_info_t afterRight = createSplitInfo(tree, its.toExplore, its.graftEdge.n2, sample);
	split_info_t afterLeft = createSplitInfo(tree, its.directedCEP.n2, its.collapseEdge.n2, sample);
	std::vector<split_info_t> vecAfter = boost::assign::list_of(afterMoved)(afterTrav1)(afterTrav2)(afterRight)(afterLeft);

	if(MAPPING_BRANCHES) {
		mapBranchLengths(vecBefore, vecAfter, sample);
	}

	/*if(beforeLeft.bl != afterLeft.bl && MAPPING_BRANCHES) {
		if(beforeLeft.bl == afterTrav2.bl) {
			std::swap(afterLeft.bl, afterTrav2.bl);
			branchHelper->setBranchLength(afterLeft.bl, afterLeft.n1, afterLeft.n2, sample);
			branchHelper->setBranchLength(afterTrav2.bl, afterTrav2.n1, afterTrav2.n2, sample);
			//std::cout << "Swaped" << std::endl;
		} else {
			assert(false);
		}
	}

	if(beforeRight.bl != afterRight.bl && MAPPING_BRANCHES) {
		if(beforeRight.bl == afterTrav2.bl) {
			std::swap(afterRight.bl, afterTrav2.bl);
			branchHelper->setBranchLength(afterRight.bl, afterRight.n1, afterRight.n2, sample);
			branchHelper->setBranchLength(afterTrav2.bl, afterTrav2.n1, afterTrav2.n2, sample);
			//std::cout << "Swaped2.2" << std::endl;
		} else if(beforeRight.bl == afterTrav1.bl) {
			std::swap(afterRight.bl, afterTrav1.bl);
			branchHelper->setBranchLength(afterRight.bl, afterRight.n1, afterRight.n2, sample);
			branchHelper->setBranchLength(afterTrav1.bl, afterTrav1.n1, afterTrav1.n2, sample);
			//std::cout << "Swaped2.1" << std::endl;
		} else {
			assert(false);
		}
	}*/

	/*for(size_t i = 0; i < vecBefore.size(); ++i) {
		for(size_t j = 0; j < vecAfter.size(); ++j) {
			if(vecBefore[i].split == vecAfter[j].split && vecBefore[i].bl != vecAfter[j].bl) {
				std::cout << "ERROR : " << i << ". " << j << std::endl;
				assert(false);
			}
		}
	}*/

	double fwProbBL = 1.0;
	double bwProbBL = 1.0;
	if(SCALE_BRANCHES) {
		std::pair<double, double> probs1 = applyBranchModifier(beforeTrav1, afterTrav1, sample);
		std::pair<double, double> probs2 = applyBranchModifier(beforeTrav2, afterTrav2, sample);

		fwProbBL *= probs1.first * probs2.first;
		bwProbBL *= probs1.second * probs2.second;
	}

	/* DISPLAY AFTER STATE */
	/*std::cout << "********************************************************" << std::endl;
	std::cout << "2-After: Grafted branch - " << displayBranchInfo(tree, its.toGraft, its.toExplore, sample, bh) << std::endl;
	std::cout << "2-After: Rev Traversed branch2 - " << displayBranchInfo(tree, its.directedCEP.n2, its.directedCEP.n3, sample, bh) << std::endl;
	std::cout << "2-After: Rev Traversed branch1 - " << displayBranchInfo(tree, its.directedCEP.n3, its.toExplore, sample, bh) << std::endl;
	std::cout << "2-After: Extended branch - " << displayBranchInfo(tree, its.toExplore, its.graftEdge.n2, sample, bh) << std::endl;
	std::cout << "2-After: Collapsed branch - " << displayBranchInfo(tree, its.directedCEP.n2, its.collapseEdge.n2, sample, bh) << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl;*/
	/* DISPLAY AFTER STATE */

	/*std::cout << "------------------------" << std::endl;
	std::cout << "Node Graft : " << its.toGraft->toString() << std::endl;
	std::cout << "Node Explore : " << its.toExplore->toString() << std::endl;
	std::cout << "Node Graft.n1 : " << its.graftEdge.n1->toString() << std::endl;
	std::cout << "Node Graft.n2 : " << its.graftEdge.n2->toString() << std::endl;
	std::cout << "Node Collapse.n1 : " << its.collapseEdge.n1->toString() << std::endl;
	std::cout << "Node Collapse.n2 : " << its.collapseEdge.n2->toString() << std::endl;
	std::cout << "Node CEP.n1 : " << its.directedCEP.n1->toString() << std::endl;
	std::cout << "Node CEP.n2 : " << its.directedCEP.n2->toString() << std::endl;
	std::cout << "Node CEP.n3 : " << its.directedCEP.n3->toString() << std::endl;
	std::cout << "************************************************" << std::endl;
	getchar();*/

	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	newMove.reset(new Move(h, newH, Move::AdaptiveESPR));

	// Register involved nodes (edges)
	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	// m1 <--> m2 represents the edge to move (m1=toGraft, m2=toExplore)
	newMove->addInvolvedNode(its.toGraft->getId());
	newMove->addInvolvedNode(its.toExplore->getId());
	// s1 <--> s2 is the collapsed starting edge
	newMove->addInvolvedNode(collapseEdge.n1->getId());
	newMove->addInvolvedNode(collapseEdge.n2->getId());
	// e1 <--> e2 is the augmented ending edge
	newMove->addInvolvedNode(its.graftEdge.n1->getId());
	newMove->addInvolvedNode(its.graftEdge.n2->getId());

	// Keep track of path
	for(size_t iP=0; iP<path.size(); ++iP) {
		newMove->addInvolvedNode(path[iP]->getId());
	}

	/*if(forwardProb != backwardProb) {
		std::cout << initialTree << std::endl;
		std::cout << tree->getIdString() << std::endl;
		assert(false);
	}*/


	//std::cout << "[2] After frequencies : " << bipartitionMonitor->defineNodeFrequency(tree, its.toExplore);
	//for(size_t iP=0; iP<path.size(); ++iP) {
	//	std::cout << ",\t" << bipartitionMonitor->defineNodeFrequency(tree, path[iP]);
	//}
	//double ccp2 = bipartitionMonitor->computeTreeCCP(tree);
	//std::cout << " -- tree prob = " << ccp2 << std::endl << "-- ratio = " << ccp2/ccp1;

	//std::cout << std::scientific << "Forward prob = " << forwardProb << " -- Backward prob = " << backwardProb  << " -- Ratio = " << backwardProb/forwardProb << std::endl;
#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.probRatio.push_back(backwardProb/forwardProb);
#endif
	//newMove->setProbability(probBL*(backwardProb/forwardProb));

	double finalFwProb = forwardProb*fwProbBL;
	double finalBwProb = backwardProb*bwProbBL;
	newMove->setForwardProbability(finalFwProb);
	newMove->setBackwardProbability(finalBwProb);

	newMove->setProbability(finalBwProb/finalFwProb);

	//std::cout << "M2, " << ccp2/ccp1 << ", " << (finalBwProb/finalFwProb) << ", " << (ccp2/ccp1)*(finalBwProb/finalFwProb);

	// Add some info to move
	newMove->setNEdgeDistance(nEdge);
	newMove->setMoveInfo(static_cast<int>(moveInfoESPR));


	return newMove;
}

std::vector<double> AdaptiveESPR::getEdgeProbabilities(double epsilonFreq, double powerFreq, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF) {

	std::vector<double> probabilities(vecBF.size());
	//std::cout << epsilonFreq << "\t" << powerFreq << std::endl;

	double sumFreq = 0.;
	for(size_t iF=0; iF<vecBF.size(); ++iF) {
		probabilities[iF] = epsilonFreq + pow((1.-vecBF[iF].freq), powerFreq);
		sumFreq += probabilities[iF];
		//std::cout << (1.-vecBF[iF].freq) << "\t";
	}
	//std::cout << std::endl;
	//std::cout << sumFreq << std::endl;

	for(size_t iF=0; iF<probabilities.size(); ++iF) {
		probabilities[iF] /= sumFreq;
		//std::cout << probabilities[iF] << "\t";
	}
	//std::cout << std::endl;

	return probabilities;
}


std::vector<double> AdaptiveESPR::getContiguousEdgePairProbabilities(double epsilonFreq, double powerFreq, Tree::sharedPtr_t aTree,
		std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF) {

	// Build a map of both with edge having the node with smaller ID first
	std::map<Tree::edge_t, double> mapEdgeSplitFreq;

	for(size_t iE=0; iE<vecBF.size(); ++iE) {
		Tree::edge_t edge = vecBF[iE].edge;
		if(edge.n2->getId() < edge.n1->getId()) std::swap(edge.n1, edge.n2);
		mapEdgeSplitFreq[edge] = vecBF[iE].freq;
	}

	// Decompose the contiguous edges in two edges and get their split frequencies
	// Build the contiguous edges unnormalised probability
	double normCst = 0.;
	std::vector<double> prob;
	std::vector< Tree::contiguous_edge_pair_t > cep = aTree->getContiguousInternalEdges();
	for(size_t iC=0; iC<cep.size(); ++iC) {
		Tree::edge_t edge1 = {cep[iC].n1, cep[iC].n2};
		if(edge1.n2->getId() < edge1.n1->getId()) std::swap(edge1.n1, edge1.n2);
		Tree::edge_t edge2 = {cep[iC].n2, cep[iC].n3};
		if(edge2.n2->getId() < edge2.n1->getId()) std::swap(edge2.n1, edge2.n2);

		double e1Freq = mapEdgeSplitFreq[edge1];
		double e2Freq = mapEdgeSplitFreq[edge2];

		double conditionalProb = epsilonFreq;
		conditionalProb += pow(1.-e1Freq, powerFreq) * pow(1.-e2Freq, powerFreq);
		prob.push_back(conditionalProb);
		normCst += conditionalProb;
	}

	//std::cout << "Contiguous edge probability : " << std::endl;
	for(size_t iC=0; iC<cep.size(); ++iC) {
		prob[iC] = prob[iC]/normCst;

		/*Tree::edge_t edge1 = {cep[iC].n1, cep[iC].n2};
		if(edge1.n2->getId() < edge1.n1->getId()) std::swap(edge1.n1, edge1.n2);
		Tree::edge_t edge2 = {cep[iC].n2, cep[iC].n3};
		if(edge2.n2->getId() < edge2.n1->getId()) std::swap(edge2.n1, edge2.n2);
		std::cout << cep[iC].n1->getId() << " ( " << mapEdgeSplitFreq[edge1] << " ) -- " << cep[iC].n2->getId() << " -- " << cep[iC].n3->getId() << " ( " << mapEdgeSplitFreq[edge2] << " )-- " << std::scientific << prob[iC] << std::endl;*/
	}

	return prob;
}




std::string AdaptiveESPR::toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;
	ss << "toGraft : " << toGraft->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "toExplore : " << toExplore->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s1 : " << collapseEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s2 : " << collapseEdge.n2->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e1 : " << graftEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e2 : " << graftEdge.n2->toString() << std::endl;
	for(size_t i=0; i<path.size(); ++i) {
		ss << "**" << std::endl;
		ss << "p" << i << " : " << path[i]->toString() << std::endl;
	}
	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
