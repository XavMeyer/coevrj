//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ETBR.h
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef ETBR_H_
#define ETBR_H_

#include <stddef.h>

#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "TreeProposal.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

class ETBR : public TreeProposal {
public:

	typedef struct {
		size_t edgeN1, edgeN2;
		size_t collapseE1N1, collapseE1N2, graftE1N1, graftE1N2;
		size_t collapseE2N1, collapseE2N2, graftE2N1, graftE2N2;
		std::vector<size_t> path1, path2;
	} unpackedMoveId_t;

public:
	ETBR(ParameterBlock::BranchHelper *aBH);
	~ETBR();

	Move::sharedPtr_t proposeNewTree(const double pe, Tree::sharedPtr_t tree, Sampler::Sample &sample);

	static unpackedMoveId_t unpackMove(const std::vector<size_t>& invNodes);

private:

	std::string toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
			Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path);

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* ETBR_H_ */
