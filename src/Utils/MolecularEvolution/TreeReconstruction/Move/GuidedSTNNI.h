//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ESPR.h
 *
 * @date Nov 4, 2018
 * @author meyerx
 * @brief
 */
#ifndef GUIDED_STNNI_H_
#define GUIDED_STNNI_H_

#include <stddef.h>

#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/Parsimony/FastFitchEvaluator.h"

#include "TreeProposal.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

class GuidedSTNNI : public TreeProposal {
public:
	enum branchLabel_t {
		NODE_N1=0, NODE_N2=1, NODE_B=2, NODE_OTHER=3, NODE_A=4, NODE_OTHER2=5
	};

public:
	GuidedSTNNI(Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH,
			   Parsimony::FastFitchEvaluator::sharedPtr_t aFFE);
	~GuidedSTNNI();

	Move::sharedPtr_t proposeNewTree(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample);

private:

	typedef struct {
		size_t nStep;
		long int h;
		Tree::edge_t movingEdge;
		Tree::edge_t graftEdge;
		std::vector<TreeNode*> path;
	} minimalMoveInfo_t;

	typedef std::vector< minimalMoveInfo_t > vecMoveInfo_t;

	long int lastInitH, lastFinalH;

	vecMoveInfo_t cacheFwMoves, cacheBwMoves;
	std::vector<double> cacheFwScores, cacheBwScores;

	Parsimony::FastFitchEvaluator::sharedPtr_t fastFitchEvaluator;


	double queryParsimonyScore(Tree::sharedPtr_t tree);
	std::vector<double> computeMovesProbability(double currentScore, std::vector<double> &scores);

	minimalMoveInfo_t createMove(size_t iStep, long int h, TreeNode* movingNodeParent, TreeNode* movingNode,
								 Tree::edge_t graftEdge, std::vector<TreeNode*> &path);

	void defineAllMovesSTNNI(Tree::sharedPtr_t tree, vecMoveInfo_t &moves, std::vector<double> &scores);

	std::vector<Tree::edge_t> selectEdgesInRadius(const size_t N_STEP, Tree::sharedPtr_t tree, TreeNode* startingNode);
	void extendRadius(size_t iStep, Tree::edge_t currentEdge, std::vector<Tree::edge_t> &edges, const size_t N_STEP);
	void defineMoveInRadiusSTNNI(Tree::sharedPtr_t tree, std::vector<Tree::edge_t> &edges, vecMoveInfo_t &moves, std::vector<double> &scores);

	Move::sharedPtr_t proposeAllSTNNIMoves(Tree::sharedPtr_t tree, Sampler::Sample &sample);
	Move::sharedPtr_t proposeLimitedSTNNIMoves(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample);
};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* GUIDED_STNNI_H_ */
