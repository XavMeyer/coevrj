//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GuidedESPR.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "GuidedESPR.h"

#include <assert.h>
#include <boost/assign/list_of.hpp>

#include "Parallel/RNG/RNG.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Utils/MolecularEvolution/Parsimony/Cache/CacheLRU.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

GuidedESPR::GuidedESPR(Bipartition::BipartitionMonitor::sharedPtr_t aBM,
		ParameterBlock::BranchHelper *aBH,
		Parsimony::FastFitchEvaluator::sharedPtr_t aFFE) :
				   TreeProposal(aBM, aBH), fastFitchEvaluator(aFFE) {

	lastFinalH = lastInitH = 0;

}

GuidedESPR::~GuidedESPR() {
}



GuidedESPR::minimalMoveInfo_t GuidedESPR::createMove(size_t iStep, long int h, TreeNode* movingNodeParent, TreeNode* movingNode,
													 Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {

	minimalMoveInfo_t newMove;

	newMove.nStep = iStep;
	newMove.h = h;
	newMove.movingEdge.n1 = movingNodeParent;
	newMove.movingEdge.n2 = movingNode;
	newMove.graftEdge = graftEdge;
	newMove.path = path;

	return newMove;

}

void GuidedESPR::applyMove(size_t iStep, Tree::sharedPtr_t tree,
		TreeNode* movingNodeParent, TreeNode* movingNode, Tree::edge_t collapseEdge,
		Tree::edge_t graftEdge, std::vector<TreeNode*> &path, vecMoveInfo_t &moves,
		std::vector<double> &scores) {

	// We prune and graft
	std::vector<TreeNode*> tmpPath(1, graftEdge.n1);
	tm.pruneAndGraft(tree, movingNodeParent, movingNode, collapseEdge, graftEdge, tmpPath);

	// Compute parsimony
	//double score = fastFitchEvaluator->update(tree);
	//tree->clearUpdatedNodes();
	double score = queryParsimonyScore(tree);

	// Creating node
	minimalMoveInfo_t newMove = createMove(iStep, tree->getHashKey(), movingNodeParent, movingNode, graftEdge, path);

	// Register the move and the parsimony score
	moves.push_back(newMove);
	scores.push_back(score);
}

void GuidedESPR::enumerateMoves(size_t iStep, const long int originalH, Tree::sharedPtr_t tree,
								Tree::edge_t &movingEdge, Tree::edge_t &pathEdge,
								std::vector<TreeNode*> &path, vecMoveInfo_t &moves,
								std::vector<double> &scores, const size_t N_STEP) {

	iStep++;

	// Recover nodes
	TreeNode* movingNodeParent = movingEdge.n1;
	TreeNode* movingNode = movingEdge.n2;

	assert(movingNode == pathEdge.n1);
	TreeNode* pathNode = pathEdge.n2;

	// If the node to visit is terminal or if the depth is reached: we are done.
	if(pathNode->isTerminal() || iStep > N_STEP) return;

	// Update  full path
	path.push_back(pathNode);

	// First, we recover the edge to collapse
	vecTN_t vecTN = movingNode->getChildren(movingNodeParent);
	// Keep track of collapsed edge --> s1 is always equal to "pathNode"
	Tree::edge_t initialCollapseEdge = {vecTN[0], vecTN[1]};
	if(initialCollapseEdge.n1 != pathNode) std::swap(initialCollapseEdge.n1, initialCollapseEdge.n2);

	// Recover the two edges for the graft
	vecTN.clear();
	vecTN = pathNode->getChildren(movingNode);
	assert(vecTN.size() == 2);

	// ------  NODE A ------ //
	// Visit to first node
	Tree::edge_t graftEdge1 = {pathNode, vecTN[0]};
	Tree::edge_t collapseEdge1 = initialCollapseEdge;

	// Apply the move
	applyMove(iStep, tree, movingNodeParent, movingNode,
			  collapseEdge1, graftEdge1, path, moves, scores);

	// Recursively go down one step
	Tree::edge_t newPathEdge1 = {movingNode, graftEdge1.n2};
	enumerateMoves(iStep, originalH, tree, movingEdge, newPathEdge1, path, moves, scores, N_STEP);

	// ------  NODE B ------ //
	// Visit to second node
	Tree::edge_t graftEdge2 = {pathNode, vecTN[1]};
	Tree::edge_t collapseEdge2 = graftEdge1;

	// Apply the move
	applyMove(iStep, tree, movingNodeParent, movingNode,
			  collapseEdge2, graftEdge2, path, moves, scores);

	// Recursively go down one step
	Tree::edge_t newPathEdge2 = {movingNode, graftEdge2.n2};
	enumerateMoves(iStep, originalH, tree, movingEdge, newPathEdge2, path, moves, scores, N_STEP);

	// ------ Reset changes ------- //
	// We go down one step, remove the last "path node"
	path.pop_back();

	// Undo the last move
	Tree::edge_t graftEdge3 = initialCollapseEdge;
	// Update collapsed edge
	Tree::edge_t collapseEdge3 = graftEdge2;

	// We now have to prune and graft
	std::vector<TreeNode*> tmpPath(1, graftEdge3.n1);
	tm.pruneAndGraft(tree, movingNodeParent, movingNode, collapseEdge3, graftEdge3, tmpPath);
}

double GuidedESPR::queryParsimonyScore(Tree::sharedPtr_t tree) {

	double score = -1.;

	bool found = false;
	if(Parsimony::getCache().isEnabled()) {
		Parsimony::getCache().getElement(tree->getHashKey(), score);
	}


	if(found) {
		assert(score >= 0.);
	} else {

		score = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();

		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addNewElement(tree->getHashKey(), score);
		}
	}

	return score;
}


std::vector<double> GuidedESPR::computeMovesProbability(double currentScore, std::vector<double> &scores) {
	const double TRUNCATION_FACTOR = 0.1;
	const double EPSILON = 1.e-3;
	std::vector<double> rescaledScores(scores);

	// Trying with a rescaling type 1 - max[(new parsimony / old parsimony)/C, 1-epsilon]
	double sum = 0.;
	for(size_t i=0; i<scores.size(); ++i) {
		rescaledScores[i] = (currentScore - rescaledScores[i]) / ((double)fastFitchEvaluator->getNSite());

		rescaledScores[i] = std::min(TRUNCATION_FACTOR, rescaledScores[i]);
		rescaledScores[i] = std::max(-TRUNCATION_FACTOR, rescaledScores[i]);

		rescaledScores[i] += TRUNCATION_FACTOR;
		rescaledScores[i] = EPSILON + pow(rescaledScores[i]/TRUNCATION_FACTOR, 8.0);

		sum += rescaledScores[i];
	}

	for(size_t i=0; i<rescaledScores.size(); ++i) rescaledScores[i] /= sum;



	/*std::cout << "Current score : " << currentScore << std::endl;

	for(size_t i=0; i<scores.size(); ++i) {
		std::cout << scores[i] << ", ";
	}
	std::cout << std::endl;

	for(size_t i=0; i<scores.size(); ++i) {
		std::cout << currentScore-scores[i] << ", ";
	}
	std::cout << std::endl;

	for(size_t i=0; i<rescaledScores.size(); ++i) {
		std::cout << rescaledScores[i] << ", ";
	}
	std::cout << std::endl;
	getchar();*/



	return rescaledScores;
}


void GuidedESPR::selectEdgeToMove(Tree::sharedPtr_t tree, TreeNode* &movingNodeParent, TreeNode* &movingNode) {

	bool canChange = false;
	Tree::edge_t edgeToPrune;
	while(!canChange) {
		// Randomly select an internal edge
		edgeToPrune = getRndInternalBranch(tree);

		// Label the subtrees
		canChange = tm.labelSubTrees(edgeToPrune, movingNodeParent, movingNode);
		if(!canChange) {
			std::cout << "Cannot change!" << std::endl;
		}
	}
}


void GuidedESPR::defineMoves(const size_t N_STEP, Tree::sharedPtr_t tree, TreeNode* movingNodeParent, TreeNode* movingNode,
							 vecMoveInfo_t &moves, std::vector<double> &scores) {

	// Get current tree hash
	long int currentH = tree->getHashKey();


	//std::cout << "Moving node parent : " << movingNodeParent->toString() << std::endl;
	//std::cout << "Moving node : " << movingNode->toString() << std::endl;
	//std::cout << "-------------------------------------------" << std::endl;

	// Get first node(s) to visit
	vecTN_t set = movingNode->getNonTerminalChildren(movingNodeParent);
	if(set.empty()) return;
	assert(set.size() <= 2 && set.size() > 0);

	std::vector<TreeNode*> path;

	// enumerate possible moves for each of those nodes
	for(size_t i=0; i<set.size(); ++i) {
		Tree::edge_t movingEdge = {movingNodeParent, movingNode};
		Tree::edge_t pathEdge = {movingNode, set[i]};
		enumerateMoves(0, currentH, tree, movingEdge, pathEdge, path, moves, scores, N_STEP);
	}
	assert(path.empty());
	assert(tree->getHashKey() == currentH);
}

std::vector<TreeProposal::split_info_t> GuidedESPR::defineMoveSplits(Tree::sharedPtr_t tree, TreeNode* movingNodeParent, TreeNode* movingNode,
								 	 	 	 	 	   Tree::edge_t &graftEdge, Tree::edge_t &collapseEdge, std::vector<TreeNode*> &path,
													   Sampler::Sample &sample) {

	// Preparing mapping fw move
	split_info_t beforeMovedBL = createSplitInfo(tree, movingNodeParent, movingNode, sample);
	split_info_t beforeLeftBL = createSplitInfo(tree, graftEdge.n1, graftEdge.n2, sample);
	split_info_t beforeRightBL = createSplitInfo(tree, movingNode, collapseEdge.n2, sample);
	split_info_t beforeCentralBL = createSplitInfo(tree, movingNode, collapseEdge.n1, sample);
	std::vector<split_info_t> vecSplits = boost::assign::list_of(beforeMovedBL)(beforeLeftBL)(beforeRightBL)(beforeCentralBL);
	assert(graftEdge.n1 == path.back());
	for(size_t i=1; i<path.size(); ++i) {
		split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
		vecSplits.push_back(tmpBL);
	}

	return vecSplits;
}


/**
 * Propose a new tree by doing an eSPR move.
 *
 * @param h Hash key of the current tree
 * @return The move produced
 */
Move::sharedPtr_t GuidedESPR::proposeNewTree(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	if(N_STEP > 0) {
		return proposeRandomOneEdgeMoves(N_STEP, tree, sample);
	} else {
		return proposeAllOneStepMoves(tree, sample);
	}

}




Move::sharedPtr_t GuidedESPR::proposeRandomOneEdgeMoves(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample) {


	//CustomProfiling cp;
	//cp.startTime(2);

	// Memorize pending change on the tree:
	std::vector<TreeNode*> originalUpdatedNodes(tree->getUpdatedNodes());

	// Get current tree hash
	long int currentH = tree->getHashKey();

	// Select the edge to move (clad)
	TreeNode *movingNodeParent, *movingNode;
	selectEdgeToMove(tree, movingNodeParent, movingNode);

	//cp.startTime();

	// First we want to make sure that the parsimony score are starting from a safe point
	double currentScore = 0.;
	if(lastFinalH == currentH && tree->getUpdatedNodes().empty()) {
		currentScore = queryParsimonyScore(tree);
	} else {
		currentScore = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();
		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addElement(currentH, currentScore);
		}
	}
	//cp.endTime();
	//double timeInit = cp.duration();

	//cp.startTime();
	// defines forward moves
	vecMoveInfo_t fwMoves;
	std::vector<double> fwScores;
	defineMoves(N_STEP, tree, movingNodeParent, movingNode, fwMoves, fwScores);

	//std::cout << "N moves = " << fwMoves.size() << std::endl;

	// Draw based on parsimony scores
	std::vector<double> fwMovesProbability(computeMovesProbability(currentScore, fwScores));
	size_t iDraw = Parallel::mpiMgr().getPRNG()->drawFrom(fwMovesProbability);

	double fwProb = fwMovesProbability[iDraw];
	double nextScore = fwScores[iDraw];
	minimalMoveInfo_t fwMove = fwMoves[iDraw];

	//cp.endTime();
	//double timeFW = cp.duration();


	// ----- re-apply move  ------- //

	// First, we recover the edge to collapse
	vecTN_t vecTN = movingNode->getChildren(movingNodeParent);
	// Keep track of collapsed edge --> s1 is always equal to "pathNode"
	Tree::edge_t initialCollapseEdge = {vecTN[0], vecTN[1]};
	if(initialCollapseEdge.n1 != fwMove.path.front()) std::swap(initialCollapseEdge.n1, initialCollapseEdge.n2);

	// We now have to prune and graft
	tm.pruneAndGraft(tree, movingNodeParent, movingNode, initialCollapseEdge, fwMove.graftEdge, fwMove.path);
	//assert(false && "Something has to be done with the updatedNodes for the tree so the likelihood computations knows what it has to recompute.");
	// A possibility is to memorize the nodes affected and to set them later.

	// enumerate backward
	long int newH = tree->getHashKey();
	assert(newH == fwMove.h);

	//cp.startTime();
	vecMoveInfo_t bwMoves;
	std::vector<double> bwScores;
	defineMoves(N_STEP, tree, movingNodeParent, movingNode, bwMoves, bwScores);

	// Resetting the Parsimony evalutator DAG to the proposed tree
	// This is required for us to reuse the DAG without full init
	fastFitchEvaluator->update(tree);

	// identify reverse move
	std::vector<double> bwMovesProbability(computeMovesProbability(nextScore, bwScores));

	bool found = false;
	size_t iMove = 0;
	for(size_t i=0; i<bwMoves.size(); ++i) {
		if(((bwMoves[i].graftEdge.n1 == initialCollapseEdge.n1) && (bwMoves[i].graftEdge.n2 == initialCollapseEdge.n2))) {
			found = true;
			iMove = i;
			break;
		}
	}
	assert(found);

	// get probability
	minimalMoveInfo_t bwMove = bwMoves[iMove];
	double bwProb = bwMovesProbability[iMove];


	//cp.endTime();
	//double timeBW = cp.duration();

	// Define the move
	// Register movement
	Move::sharedPtr_t newMove(new Move(currentH, newH, Move::GuidedESPR));

	// Register involved nodes (edges)
	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	// m1 <--> m2 represents the edge to move (m1=toGraft, m2=toExplore)
	newMove->addInvolvedNode(movingNodeParent->getId());
	newMove->addInvolvedNode(movingNode->getId());
	// s1 <--> s2 is the collapsed starting edge
	newMove->addInvolvedNode(initialCollapseEdge.n1->getId());
	newMove->addInvolvedNode(initialCollapseEdge.n2->getId());
	// e1 <--> e2 is the augmented ending edge
	newMove->addInvolvedNode(fwMove.graftEdge.n1->getId());
	newMove->addInvolvedNode(fwMove.graftEdge.n2->getId());

	// Keep track of path
	for(size_t iP=0; iP<fwMove.path.size(); ++iP) {
		newMove->addInvolvedNode(fwMove.path[iP]->getId());
	}

	// Probabilities
	newMove->setForwardProbability(fwProb);
	newMove->setBackwardProbability(bwProb);
	newMove->setProbability(bwProb/fwProb);
	newMove->setNEdgeDistance(fwMove.nStep);

	// Trick to inform properly the likelihood processor
	// The collapse edge of the forward move is the graft edge of the backward move and vice versa

	// Reverse move
	tm.pruneAndGraft(tree, movingNodeParent, movingNode, fwMove.graftEdge, bwMove.graftEdge, bwMove.path);
	tree->clearUpdatedNodes();

	// Get move split info
	std::vector<split_info_t> vecBefore = defineMoveSplits(tree, movingNodeParent, movingNode, fwMove.graftEdge, bwMove.graftEdge, fwMove.path, sample);

	// Re apply the move
	tm.pruneAndGraft(tree, movingNodeParent, movingNode, bwMove.graftEdge, fwMove.graftEdge, fwMove.path);

	// Add nodes that where pending to be updated on the tree updated to the list:
	tree->addUpdatedNodes(originalUpdatedNodes);

	// Get move split info
	std::vector<split_info_t> vecAfter = defineMoveSplits(tree, movingNodeParent, movingNode, bwMove.graftEdge, fwMove.graftEdge, bwMove.path, sample);

	mapBranchLengths(vecBefore, vecAfter, sample);

	// Keep track of last tree hash key to avoid useless recomputation
	lastFinalH = tree->getHashKey();

	//cp.endTime(2);
	//double timeTotal = cp.duration(2);

	//std::cout << "Time : init = " << std::scientific <<  timeInit << "  fw = " << timeFW << "  bw  = " << timeBW  << "  total = " << timeTotal << std::endl;

	return newMove;
}

/**
 * Propose a new tree by doing an eSPR move.
 *
 * @param h Hash key of the current tree
 * @return The move produced
 */
Move::sharedPtr_t GuidedESPR::proposeAllOneStepMoves(Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	const size_t N_STEP = 1;

	// Memorize pending change on the tree:
	std::vector<TreeNode*> originalUpdatedNodes(tree->getUpdatedNodes());

	// Get current tree hash
	long int currentH = tree->getHashKey();

	// First we want to make sure that the parsimony score are starting from a safe point
	double currentScore = 0.;
	if(lastFinalH == currentH && tree->getUpdatedNodes().empty()) {
		currentScore = queryParsimonyScore(tree);
	} else {
		currentScore = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();
		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addElement(currentH, currentScore);
		}
	}

	// Compute all 1 step forward moves
	vecMoveInfo_t fwMoves;
	std::vector<double> fwScores;
	std::vector<Tree::edge_t> fwEdges = tree->getAllEdgeForBipartitions();//tree->getInternalEdges();

	for(size_t iE=0; iE<fwEdges.size(); ++iE) {
		Tree::edge_t movingEdge = fwEdges[iE];

		/*if(movingEdge.n1 == tree->getRoot() || movingEdge.n2 == tree->getRoot()) {
			std::cout << "Here1" << std::endl;
			continue;
		}*/

		if(!movingEdge.n2->isTerminal()) {
			defineMoves(N_STEP, tree, movingEdge.n1, movingEdge.n2, fwMoves, fwScores);
		}

		std::swap(movingEdge.n1, movingEdge.n2);
		if(!movingEdge.n2->isTerminal()) {
			defineMoves(N_STEP, tree, movingEdge.n1, movingEdge.n2, fwMoves, fwScores);
		}
	}

	//std::cout << "Here2" << std::endl;

	// Draw based on parsimony scores
	std::vector<double> fwMovesProbability(computeMovesProbability(currentScore, fwScores));
	size_t iDraw = Parallel::mpiMgr().getPRNG()->drawFrom(fwMovesProbability);

	double fwProb = fwMovesProbability[iDraw];
	double nextScore = fwScores[iDraw];
	minimalMoveInfo_t fwMove = fwMoves[iDraw];

	TreeNode* fwMovingNodeParent = fwMove.movingEdge.n1;
	TreeNode* fwMovingNode = fwMove.movingEdge.n2;

	// ----- re-apply move  ------- //

	// First, we recover the edge to collapse
	vecTN_t vecTN = fwMovingNode->getChildren(fwMovingNodeParent);
	// Keep track of collapsed edge --> s1 is always equal to "pathNode"
	Tree::edge_t initialCollapseEdge = {vecTN[0], vecTN[1]};
	if(initialCollapseEdge.n1 != fwMove.path.front()) std::swap(initialCollapseEdge.n1, initialCollapseEdge.n2);

	// We now have to prune and graft
	tm.pruneAndGraft(tree, fwMovingNodeParent, fwMovingNode, initialCollapseEdge, fwMove.graftEdge, fwMove.path);
	//assert(false && "Something has to be done with the updatedNodes for the tree so the likelihood computations knows what it has to recompute.");
	// A possibility is to memorize the nodes affected and to set them later.

	// enumerate backward
	long int newH = tree->getHashKey();
	assert(newH == fwMove.h);

	// Compute all 1 step forward moves
	vecMoveInfo_t bwMoves;
	std::vector<double> bwScores;
	std::vector<Tree::edge_t> bwEdges = tree->getAllEdgeForBipartitions();

	for(size_t iE=0; iE<bwEdges.size(); ++iE) {
		Tree::edge_t movingEdge = bwEdges[iE];

		//if(movingEdge.n1 == tree->getRoot() || movingEdge.n2 == tree->getRoot()) continue;

		if(!movingEdge.n2->isTerminal()) {
			defineMoves(N_STEP, tree, movingEdge.n1, movingEdge.n2, bwMoves, bwScores);
		}

		std::swap(movingEdge.n1, movingEdge.n2);
		if(!movingEdge.n2->isTerminal()) {
			defineMoves(N_STEP, tree, movingEdge.n1, movingEdge.n2, bwMoves, bwScores);
		}
	}

	// Resetting the Parsimony evalutator DAG to the proposed tree
	// This is required for us to reuse the DAG without full init
	fastFitchEvaluator->update(tree);

	// identify reverse move
	std::vector<double> bwMovesProbability(computeMovesProbability(nextScore, bwScores));

	bool found = false;
	size_t iMove = 0;
	for(size_t i=0; i<bwMoves.size(); ++i) {
		if(((bwMoves[i].graftEdge.n1 == initialCollapseEdge.n1) && (bwMoves[i].graftEdge.n2 == initialCollapseEdge.n2)) &&
		   ((fwMove.movingEdge.n1 == bwMoves[i].movingEdge.n1) && (fwMove.movingEdge.n2 == bwMoves[i].movingEdge.n2)) ) {
			found = true;
			iMove = i;
			break;
		}
	}
	assert(found);

	// get probability
	minimalMoveInfo_t bwMove = bwMoves[iMove];
	double bwProb = bwMovesProbability[iMove];

	/*std::cout << "N moves : " << fwMoves.size() << std::endl;
	for(size_t i=0; i< fwMoves.size(); ++i) {
		std::cout << fwMoves[i].h << "\t" << fwScores[i] << "\t" << 100.*fwMovesProbability[i]  << std::endl;
	}*/

	// Define the move
	// Register movement
	Move::sharedPtr_t newMove(new Move(currentH, newH, Move::GuidedESPR));

	TreeNode* movingNodeParent = fwMove.movingEdge.n1;
	TreeNode* movingNode = fwMove.movingEdge.n2;

	// Register involved nodes (edges)
	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	// m1 <--> m2 represents the edge to move (m1=toGraft, m2=toExplore)
	newMove->addInvolvedNode(movingNodeParent->getId());
	newMove->addInvolvedNode(movingNode->getId());
	// s1 <--> s2 is the collapsed starting edge
	newMove->addInvolvedNode(initialCollapseEdge.n1->getId());
	newMove->addInvolvedNode(initialCollapseEdge.n2->getId());
	// e1 <--> e2 is the augmented ending edge
	newMove->addInvolvedNode(fwMove.graftEdge.n1->getId());
	newMove->addInvolvedNode(fwMove.graftEdge.n2->getId());

	// Keep track of path
	for(size_t iP=0; iP<fwMove.path.size(); ++iP) {
		newMove->addInvolvedNode(fwMove.path[iP]->getId());
	}

	// Probabilities
	newMove->setForwardProbability(fwProb);
	newMove->setBackwardProbability(bwProb);
	newMove->setProbability(bwProb/fwProb);
	newMove->setNEdgeDistance(fwMove.nStep);

	//std::cout << "Move fw prob = " << 100.*fwProb << "\tMove bw prob = " << 100.*bwProb << "\tMove ratio = " << bwProb/fwProb << std::endl;
	//if(fwProb > 0.1) getchar();

	// Trick to inform properly the likelihood processor
	// The collapse edge of the forward move is the graft edge of the backward move and vice versa

	// Reverse move
	tm.pruneAndGraft(tree, movingNodeParent, movingNode, fwMove.graftEdge, bwMove.graftEdge, bwMove.path);
	tree->clearUpdatedNodes();

	// Get move split info
	std::vector<split_info_t> vecBefore = defineMoveSplits(tree, movingNodeParent, movingNode, fwMove.graftEdge, bwMove.graftEdge, fwMove.path, sample);

	// Re apply the move
	tm.pruneAndGraft(tree, movingNodeParent, movingNode, bwMove.graftEdge, fwMove.graftEdge, fwMove.path);

	// Add nodes that where pending to be updated on the tree updated to the list:
	tree->addUpdatedNodes(originalUpdatedNodes);

	// Get move split info
	std::vector<split_info_t> vecAfter = defineMoveSplits(tree, movingNodeParent, movingNode, bwMove.graftEdge, fwMove.graftEdge, bwMove.path, sample);

	mapBranchLengths(vecBefore, vecAfter, sample);

	// Keep track of last tree hash key to avoid useless recomputation
	lastFinalH = tree->getHashKey();

	//std::cout << "Reuse = " << 100.*(double)parsimonyReuse/(double)parsimonyQuery << std::endl;

	return newMove;
}

std::string GuidedESPR::toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;
	ss << "toGraft : " << toGraft->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "toExplore : " << toExplore->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s1 : " << collapseEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s2 : " << collapseEdge.n2->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e1 : " << graftEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e2 : " << graftEdge.n2->toString() << std::endl;
	for(size_t i=0; i<path.size(); ++i) {
		ss << "**" << std::endl;
		ss << "p" << i << " : " << path[i]->toString() << std::endl;
	}
	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
