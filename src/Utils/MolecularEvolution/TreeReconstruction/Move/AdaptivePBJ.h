//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ETBR.h
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVE_ETBR_H_
#define ADAPTIVE_ETBR_H_

#include <stddef.h>

#include "ParameterBlock/BlockStats/ConvUtils/Updater/Mean/BatchEvoMU.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "TreeProposal.h"

namespace TR_BP = ::MolecularEvolution::TreeReconstruction::Bipartition;

namespace MolecularEvolution {
namespace TreeReconstruction {

class AdaptivePBJ : public TreeProposal {
public:

	typedef struct {
		size_t edgeN1, edgeN2;
		std::vector<size_t> stablePath;
		size_t collapseE1N1, collapseE1N2, graftE1N1, graftE1N2;
		size_t collapseE2N1, collapseE2N2, graftE2N1, graftE2N2;
		std::vector<size_t> path1, path2;
	} unpackedMoveId_t;

	enum moveType_t {ONE_EDGE=0, PATH=1, PATH_AND_ADAPTIVE_SPR=2, N_STEP_SPR=3};

	enum adaptiveType_t {REM_ADAPTIVE=0, UPDATED_ADAPTIVE=1};

public:
	AdaptivePBJ(moveType_t aMoveType, Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH);
	~AdaptivePBJ();

	Move::sharedPtr_t proposeNewTree(const double pe, double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample);

	static unpackedMoveId_t unpackMove(const std::vector<size_t>& invNodes);

private:

	static const double BASELINE_STOCHASTIC_EFFECTS;
	static const adaptiveType_t ADAPTIVE_TYPE;

	//size_t nWasted, nProposal;

	moveType_t moveType;
	ParameterBlock::BatchEvoMU *evoMu;

	double getEpsilon(double minEpsilon) const;

	std::vector<double> getStableEdgeProbabilities(double epsilonFreq, double powerFreq,
					std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF);


	Move::sharedPtr_t oneEdgeMove(const double pe, double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample);

	std::vector<double> computeMinCladFreq(Tree::edge_t &startingEdge, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF);
	double computeMinEdgeFreqInCladRec(TreeNode *parent, TreeNode *child,
			std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF,
			std::vector<double> &minCladFreq);

	void getNextStepProbsForStableEdge(double epsilon, TreeNode *parent, vecTN_t &ntChildren,
						  std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<double> &minCladFreq,
						  double &probStop, std::vector<double> &freqs, std::vector<double> &probs);
	std::pair<double, double> getEdgeFreqAndProb(TreeNode *n1, TreeNode *n2, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<double> &eProbability);
	std::pair<double, double> getEdgeFreqAndMCF(TreeNode *n1, TreeNode *n2, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<double> &minCladFreq);
	double defineStablePathProb(double epsilon, double power, Tree::sharedPtr_t tree, Tree::edge_t &startingEdge, std::vector<TreeNode*> &path);
	void buildStablePath(double epsilon, double power, Tree::sharedPtr_t tree, double &probability, Tree::edge_t &startingEdge, std::vector<TreeNode*> &path);


	void getNextStepProbsForUnstableEdge(double epsilon, TreeNode *parent, vecTN_t &ntChildren,
										 std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<double> &eProbability,
										 double &probStop, std::vector<double> &freqs, std::vector<double> &probs);

	void choseGraftPoint(double epsilon, Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
						 TreeNode *curNode, vecTN_t &children, Tree::edge_t &graftPoint, double &moveProb);
	double defineGraftPointProb(double epsilon, Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
								TreeNode *curNode, int index, vecTN_t &children);

	void findAdaptiveGraftPoint(double epsilon, Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
								Tree::edge_t &graftPoint, double &pathProb, std::vector<TreeNode*> &path);
	void defineAdaptiveGraftPointProb(double epsilon, Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
									  Tree::edge_t &collapsePoint, double &pathProb, std::vector<TreeNode*> &path);


	Move::sharedPtr_t stablePathMove(const double pe, double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample);
	Move::sharedPtr_t stablePathMove2(double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample);


	std::pair<double, split_t > getEdgeFreqAndSplit(TreeNode *n1, TreeNode *n2, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF);
	void getNextStepProbsForUnstableEdge(bool firstCall, double epsilon, TreeNode *parent, vecTN_t &ntChildren,
										 Tree::sharedPtr_t tree, split_t &movingSplit,
										 std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF,
										 double &probStop, std::vector<double> &freqs, std::vector<double> &probs);


	Move::sharedPtr_t eSPRPathMove(double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample);

	std::string toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
			Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path);

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* ADAPTIVE_ETBR_H_ */
