//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ETBR.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "AdaptivePBJ.h"

#include <assert.h>
#include <boost/assign/list_of.hpp>

#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "ParameterBlock/BlockStats/Config/Container/BlockStatCfg.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Parallel/RNG/RNG.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

const double AdaptivePBJ::BASELINE_STOCHASTIC_EFFECTS = 0.005;
const AdaptivePBJ::adaptiveType_t AdaptivePBJ::ADAPTIVE_TYPE = UPDATED_ADAPTIVE;

AdaptivePBJ::AdaptivePBJ(moveType_t aMoveType, Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH) :
						   TreeProposal(aBM, aBH), moveType(aMoveType) {

	ParameterBlock::Config::ConfigFactory::sharedPtr_t configFactory = ParameterBlock::Config::ConfigFactory::createDefaultFactory();
	ParameterBlock::Config::BlockStatCfg::sharedPtr_t blockCfg = configFactory->createConfig(1);
	evoMu = new ParameterBlock::BatchEvoMU(blockCfg->MU, blockCfg->CONV_CHECKER);
	evoMu->setInitialMean(5.);


	//nWasted = nProposal = 0;

}

AdaptivePBJ::~AdaptivePBJ() {

	/*if(nProposal > 0) {
		std::cout << (size_t)moveType << "\t" << nWasted << "\t" << nProposal << "\t" << 100.*((double)nWasted/(double)nProposal) << std::endl;
	}*/

}

double AdaptivePBJ::getEpsilon(double minEpsilon) const {

	// Decrease epsilon from 100 to 0.0001 over approx 50k iterations
	//double epsilon = 100.*exp(-(double)cntIt/4000.);
	//double scale = 10.*exp(-(double)cntIt/4000.);
	//scale = std::max(minEpsilon, scale);
	double scale = bipartitionMonitor->getEpsilon(minEpsilon, 250.*minEpsilon);
	evoMu->addVal(scale);
	double epsilon = Parallel::mpiMgr().getPRNG()->genGamma(1., evoMu->getMean());

	//std::cout << "Scale = " << evoMu->getMean() << "\t" << std::scientific << epsilon << std::endl;

	return epsilon;
}

std::vector<double> AdaptivePBJ::getStableEdgeProbabilities(double epsilonFreq, double powerFreq,
													 std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF) {

	std::vector<double> probabilities(vecBF.size(), 0.);

	double sumFreq = 0.;
	for(size_t iF=0; iF<vecBF.size(); ++iF) {

		/*bool isPotentialPathEdge = !constrainedInternals || (
								   !vecBF[iF].edge.n1->getNonTerminalChildren(vecBF[iF].edge.n2).empty() ||
								   !vecBF[iF].edge.n2->getNonTerminalChildren(vecBF[iF].edge.n1).empty());*/

		if(vecBF[iF].freq == -1) vecBF[iF].freq = 0.0; // If the split is new, we give it a .1 freq

		//if(isPotentialPathEdge) {
			probabilities[iF] = epsilonFreq + pow((vecBF[iF].freq), powerFreq);
			sumFreq += probabilities[iF];
		/*} else {
			probabilities[iF] = 0.;
		}*/
	}

	for(size_t iF=0; iF<vecBF.size(); ++iF) {
		probabilities[iF] /= sumFreq;
	}


	return probabilities;
}

/**
 * Propose a new tree by doing an AdaptiveETBR move.
 *
 * @param h Hash key of the current tree
 * @return The move produced
 */
Move::sharedPtr_t AdaptivePBJ::proposeNewTree(const double pe, double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	double power = bipartitionMonitor->getPower(aPowerFreq);

	if(moveType == ONE_EDGE) {
		return oneEdgeMove(pe, aEpsilonFreq, power, tree, sample);
	} else if(moveType == PATH) {
		return stablePathMove(pe, aEpsilonFreq, power, tree, sample);
	} else if(moveType == PATH_AND_ADAPTIVE_SPR) {
		return stablePathMove2(aEpsilonFreq, power, tree, sample);
	} else {
		return eSPRPathMove(aEpsilonFreq, power, tree, sample);
	}

}



Move::sharedPtr_t AdaptivePBJ::oneEdgeMove(const double pe, double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	double epsilon = getEpsilon(aEpsilonFreq) / tree->getInternalEdges().size();

	assert( pe >= 0 && pe <= 1.);

	long int h = tree->getHashKey();

	double fwProb = 1.0;
	Tree::edge_t stableEdge;
	{
		std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
		vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
		std::vector<double> eProbability = getStableEdgeProbabilities(epsilon, aPowerFreq, vecBF);

		size_t id = aRNG->drawFrom(eProbability);
		stableEdge = vecBF[id].edge;
		fwProb = eProbability[id];
	}


	double moveProb = 1.;
	std::vector<size_t> nEdges;
	std::vector<size_t> involvedNodes;

	// Stable edge
	involvedNodes.push_back(stableEdge.n2->getId());
	involvedNodes.push_back(stableEdge.n1->getId());

	// Stable path
	involvedNodes.push_back(2);
	involvedNodes.push_back(stableEdge.n2->getId());
	involvedNodes.push_back(stableEdge.n1->getId());

	// Draw move for each side.
	for(size_t iSPR=0; iSPR<2; ++iSPR) {

		// First "direction"
		size_t iDir = aRNG->genUniformInt(0, 1);
		TreeNode* toGraft = stableEdge.n1;
		TreeNode* toGraftParent = stableEdge.n2;
		vecTN_t directions = toGraftParent->getChildren(toGraft);

		if(directions[iDir]->isTerminal()) iDir = 1-iDir;

		// Find path and graft point
		Tree::edge_t graftEdge;
		std::vector<TreeNode*> path;
		tm.findGraftPoint(pe, directions[iDir], toGraftParent, graftEdge, path);
		nEdges.push_back(path.size());

		// Check constraints for fw move
		bool isForwardConstrained = graftEdge.n2->isTerminal();

		// Get collapse edge
		Tree::edge_t collapseEdge = {directions[0], directions[1]};
		if(collapseEdge.n1 != directions[iDir]) std::swap(collapseEdge.n1, collapseEdge.n2);

		// Check constraint for backward move
		bool isBackwardConstrained = collapseEdge.n2->isTerminal();

		//std::cout << path.size() << " ------ " << (isForwardConstrained ? "1" : "0") << " -- " << (isBackwardConstrained ? "1" : "0") << std::endl;

		if(path.size() > 0) {

			// Map branches (fw
			split_info_t beforeMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t beforeLeftBL = createSplitInfo(tree, toGraftParent, collapseEdge.n2, sample);
			split_info_t beforeRightBL = createSplitInfo(tree, graftEdge.n1, graftEdge.n2, sample);
			split_info_t centralBL = createSplitInfo(tree, toGraftParent, path.front(), sample);
			std::vector<split_info_t> vecBefore = boost::assign::list_of(beforeMovedBL)(beforeLeftBL)(beforeRightBL)(centralBL);
			for(size_t i=1; i<path.size(); ++i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecBefore.push_back(tmpBL);
			}

			// We now have to prune and graft
			tm.pruneAndGraft(tree, toGraft, toGraftParent, collapseEdge, graftEdge, path);

			// Trying to prepare things for mapping
			split_info_t afterMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t afterLeftBL = createSplitInfo(tree, collapseEdge.n1, collapseEdge.n2, sample);
			split_info_t afterRightBL = createSplitInfo(tree, toGraftParent, graftEdge.n2, sample);
			split_info_t afterCentralBL = createSplitInfo(tree, toGraftParent, graftEdge.n1, sample);
			std::vector<split_info_t> vecAfter = boost::assign::list_of(afterMovedBL)(afterLeftBL)(afterRightBL)(afterCentralBL);
			assert(graftEdge.n1 == path.back());
			for(size_t i=path.size()-1; i>0; --i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecAfter.push_back(tmpBL);
			}

			mapBranchLengths(vecBefore, vecAfter, sample);
		}

		// Pack first part of the move
		involvedNodes.push_back(collapseEdge.n1->getId());
		involvedNodes.push_back(collapseEdge.n2->getId());
		involvedNodes.push_back(graftEdge.n1->getId());
		involvedNodes.push_back(graftEdge.n2->getId());
		involvedNodes.push_back(path.size()); // Trick to keep track of the variable number of node in the path
		// Keep track of path
		for(size_t iP=0; iP<path.size(); ++iP) {
			involvedNodes.push_back(path[iP]->getId());
		}

		std::swap(stableEdge.n1, stableEdge.n2);

		double prob = 1.0;
		if(isForwardConstrained == isBackwardConstrained) {
			prob = 1.0;
		} else if(isForwardConstrained) {
			prob = (1.-pe)/0.5;
		} else if(isBackwardConstrained) {
			prob = 0.5/(1.-pe);
		}
		moveProb *= prob;

	}

	double bwProb = 1.0;
	{
		std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
		vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
		std::vector<double> eProbability = getStableEdgeProbabilities(epsilon, aPowerFreq, vecBF);

		int iEdge = -1;
		for(size_t iE=0; iE<vecBF.size(); ++iE) {
			if((stableEdge.n1 == vecBF[iE].edge.n1 && stableEdge.n2 == vecBF[iE].edge.n2) ||
			   (stableEdge.n1 == vecBF[iE].edge.n2 && stableEdge.n2 == vecBF[iE].edge.n1)) {
				iEdge = iE;
				break;
			}
		}
		assert(iEdge>=0);
		bwProb = eProbability[iEdge];
	}

	assert(nEdges.size() == 2);
	if(nEdges[0] == 0 && nEdges[1] == 0) {
		moveProb = 0.;
	}
	moveProb *= (bwProb/fwProb);

	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	Move::sharedPtr_t newMove(new Move(h, newH, Move::AdaptivePBJ));
	newMove->setInvolvedNodes(involvedNodes);
	newMove->setNEdgeDistances(nEdges);
	newMove->setProbability(moveProb);

	//std::cout << moveProb << std::endl;

	return newMove;

}

AdaptivePBJ::unpackedMoveId_t AdaptivePBJ::unpackMove(const std::vector<size_t>& invNodes ) {

	AdaptivePBJ::unpackedMoveId_t unpackedMove;

	//for(size_t i=0; i<invNodes.size(); ++i) std::cout << invNodes[i] << std::endl;
	size_t iN=0;
	unpackedMove.edgeN1 = invNodes[iN++];
	unpackedMove.edgeN2 = invNodes[iN++];

	size_t nEdgeStable = invNodes[iN++];
	for(size_t i=0; i<nEdgeStable; ++i) unpackedMove.stablePath.push_back(invNodes[iN++]);

	unpackedMove.collapseE1N1 = invNodes[iN++];
	unpackedMove.collapseE1N2 = invNodes[iN++];

	unpackedMove.graftE1N1 = invNodes[iN++];
	unpackedMove.graftE1N2 = invNodes[iN++];

	size_t nEdge1 = invNodes[iN++];
	for(size_t i=0; i<nEdge1; ++i) unpackedMove.path1.push_back(invNodes[iN++]);

	unpackedMove.collapseE2N1 = invNodes[iN++];
	unpackedMove.collapseE2N2 = invNodes[iN++];

	unpackedMove.graftE2N1 = invNodes[iN++];
	unpackedMove.graftE2N2 = invNodes[iN++];

	size_t nEdge2 = invNodes[iN++];
	for(size_t i=0; i<nEdge2; ++i) unpackedMove.path2.push_back(invNodes[iN++]);
	assert(iN == invNodes.size());

	return unpackedMove;
}


std::pair<double, double> AdaptivePBJ::getEdgeFreqAndProb(TreeNode *n1, TreeNode *n2, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<double> &eProbability) {

	std::pair<double, double> prob = std::make_pair(0., 0.);
	for(size_t i=0; i<vecBF.size(); ++i) {
		if((vecBF[i].edge.n1->getId() == n1->getId() && vecBF[i].edge.n2->getId() == n2->getId()) ||
				(vecBF[i].edge.n1->getId() == n2->getId() && vecBF[i].edge.n2->getId() == n1->getId())) {
			return std::make_pair(vecBF[i].freq, eProbability[i]);
		}
	}

	if(n1->isTerminal() || n2->isTerminal()) {
		//assert(false);
		return std::make_pair(1.0, -1.0);
	}
	assert(false);

	return prob;
}

std::pair<double, double> AdaptivePBJ::getEdgeFreqAndMCF(TreeNode *n1, TreeNode *n2, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<double> &minCladFreq) {

	std::pair<double, double> prob = std::make_pair(0., 0.);
	for(size_t i=0; i<vecBF.size(); ++i) {
		if((vecBF[i].edge.n1->getId() == n1->getId() && vecBF[i].edge.n2->getId() == n2->getId()) ||
				(vecBF[i].edge.n1->getId() == n2->getId() && vecBF[i].edge.n2->getId() == n1->getId())) {
			return std::make_pair(vecBF[i].freq, minCladFreq[i]);
		}
	}

	if(n1->isTerminal() || n2->isTerminal()) {
		//assert(false);
		return std::make_pair(1.0, 1.0);
	}
	assert(false);

	return prob;
}

std::pair<double, split_t > AdaptivePBJ::getEdgeFreqAndSplit(TreeNode *n1, TreeNode *n2, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF) {

	for(size_t i=0; i<vecBF.size(); ++i) {
		if((vecBF[i].edge.n1->getId() == n1->getId() && vecBF[i].edge.n2->getId() == n2->getId()) ||
				(vecBF[i].edge.n1->getId() == n2->getId() && vecBF[i].edge.n2->getId() == n1->getId())) {
			return std::make_pair(vecBF[i].freq, vecBF[i].split);
		}
	}

	split_t emptySplit;

	if(n1->isTerminal() || n2->isTerminal()) {
		return std::make_pair(1.0, emptySplit);
	}
	assert(false);

	std::pair<double, split_t> prob = std::make_pair(0., emptySplit);
	return prob;
}


double AdaptivePBJ::computeMinEdgeFreqInCladRec(TreeNode *parent, TreeNode *child,
		std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF,
		std::vector<double> &minCladFreq) {

	int iE = -1;
	double myFreq = 1.0;
	for(size_t i=0; i<vecBF.size(); ++i) {
		if((vecBF[i].edge.n1->getId() == parent->getId() && vecBF[i].edge.n2->getId() == child->getId()) ||
				(vecBF[i].edge.n1->getId() == child->getId() && vecBF[i].edge.n2->getId() == parent->getId())) {
			iE = i;
			myFreq = vecBF[i].freq;
			break;
		}
	}

	double minFreq = myFreq;
	vecTN_t ntChildren = child->getNonTerminalChildren(parent);
	for(size_t iC=0; iC < ntChildren.size(); ++iC) {
		double minFreqChild = computeMinEdgeFreqInCladRec(child, ntChildren[iC], vecBF, minCladFreq);
		minFreq = std::min(minFreqChild, minFreq);
	}
	assert(iE >= 0);
	minCladFreq[iE] = minFreq;

	return minFreq;
}

std::vector<double> AdaptivePBJ::computeMinCladFreq(Tree::edge_t &startingEdge,
		std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF) {

	std::vector<double> minCladFreq(vecBF.size(), -1.);

	computeMinEdgeFreqInCladRec(startingEdge.n1, startingEdge.n2, vecBF, minCladFreq);
	computeMinEdgeFreqInCladRec(startingEdge.n2, startingEdge.n1, vecBF, minCladFreq);

	return minCladFreq;
}


void AdaptivePBJ::getNextStepProbsForStableEdge(double epsilon, TreeNode *parent, vecTN_t &ntChildren,
		std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<double> &minCladFreq,
				double &probStop, std::vector<double> &freqs, std::vector<double> &probs) {

	double sumProb = 0.;
	for(size_t i=0; i<ntChildren.size(); ++i) {
		std::pair<double, double> freqAndMCF = getEdgeFreqAndMCF(parent, ntChildren[i], vecBF, minCladFreq);
		freqs[i] = freqAndMCF.first;

		//if(constrainedInternals && ntChildren[i]->getNonTerminalChildren(parent).empty()) probs[i] = 0.;

		//if(true) { // Trying to use minCladFreq too
		double minCladF = freqAndMCF.second;
			//double minCladF = getMinEdgeFreqInClad(parent, ntChildren[i], vecBF, minCladFreq);
			//assert(minCladF == freqAndMCF.second);

		probs[i] = (1.-minCladF)*freqs[i]+epsilon;
			//std::cout << i << std::scientific << "\t" << (1.-minCladFreq) << "\t" << probs[i] << std::endl;
		//} else {
			//probs[i] = freqs[i]+epsilon;
		//}

		sumProb += probs[i];
	}

	// Define if we continue
	probStop = 0.;
	if(sumProb > 0.) {
		double maxStopProb = 0.0;
		for(size_t i=0; i<ntChildren.size(); ++i) {
			probs[i]/=sumProb;
			//probStop += (1.0-probs[i])*(1.0-freqs[i]) ;
			maxStopProb = std::max(maxStopProb, (1.0-freqs[i]));
		}
		probStop = maxStopProb;
		probStop = pow(probStop, 0.85);
		// We add a baseline chance of stopping of 0.2
		probStop = (BASELINE_STOCHASTIC_EFFECTS + probStop)/(1.+BASELINE_STOCHASTIC_EFFECTS);
		//probStop = (epsilon + probStop)/(1.+epsilon);

	} else {
		probStop = 1.0;
	}
}


void AdaptivePBJ::buildStablePath(double epsilon, double power, Tree::sharedPtr_t tree, double &probability, Tree::edge_t &startingEdge, std::vector<TreeNode*> &path) {

	// Starting edge
	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> eProbability = getStableEdgeProbabilities(epsilon, power, vecBF);

	double edgeProb = 0.;
	std::vector< double > pathProbs;
	std::vector< std::vector<TreeNode*> > paths(2);

	pathProbs.assign(2, 1.);
	paths[0].clear();
	paths[1].clear();
	path.clear();

	size_t id = aRNG->drawFrom(eProbability);
	startingEdge = vecBF[id].edge;
	edgeProb = eProbability[id];
	double uEdgeOrder = Parallel::mpiMgr().getPRNG()->genUniformDbl();
	if(uEdgeOrder < 0.5) std::swap(startingEdge.n1, startingEdge.n2);

	std::vector<double> minCladFreq = computeMinCladFreq(startingEdge, vecBF);

	for(size_t iDir=0; iDir<2; ++iDir) {

		TreeNode* cNode = startingEdge.n1;
		TreeNode* nNode = startingEdge.n2;
		vecTN_t ntChildren = nNode->getNonTerminalChildren(cNode);

		while(!ntChildren.empty()) {

			double probStop = 0.;
			std::vector<double> freqs(ntChildren.size()), probs(ntChildren.size());
			getNextStepProbsForStableEdge(epsilon, nNode, ntChildren, vecBF, minCladFreq, probStop, freqs, probs);

			double uRand1 = Parallel::mpiMgr().getPRNG()->genUniformDbl();
			bool doStop = uRand1 < probStop;

			if(doStop) { // we stop there with prob 1-freq
				pathProbs[iDir] *= probStop;
				break;
			}

			// We continue
			pathProbs[iDir] *= (1.-probStop);

			double uRand2 = Parallel::mpiMgr().getPRNG()->genUniformDbl();
			// Define next edge
			size_t index = uRand2 < probs[0] ? 0 : 1;
			pathProbs[iDir] *= probs[index]; // probability of choosing this edge

			cNode = nNode;
			nNode = ntChildren[index];
			ntChildren = nNode->getNonTerminalChildren(cNode);

			paths[iDir].push_back(nNode);
		}
		std::swap(startingEdge.n1, startingEdge.n2);
	}

	path.insert(path.end(), paths[1].rbegin(), paths[1].rend());
	path.push_back(startingEdge.n1);
	path.push_back(startingEdge.n2);
	path.insert(path.end(), paths[0].begin(), paths[0].end());

	vecTN_t childrenEndA = path[0]->getNonTerminalChildren(path[1]);
	vecTN_t childrenEndB = path[path.size()-1]->getNonTerminalChildren(path[path.size()-2]);

	/*if(requireTwoMoves) {
		doEnableTreeMove = !childrenEndA.empty() && !childrenEndB.empty();
	} else {
		doEnableTreeMove = !childrenEndA.empty() || !childrenEndB.empty();
	}*/
	//std::cout << "Path size : " << path.size() << std::endl;


	probability = edgeProb * pathProbs[0] * pathProbs[1];

}

double AdaptivePBJ::defineStablePathProb(double epsilon, double power, Tree::sharedPtr_t tree, Tree::edge_t &startingEdge, std::vector<TreeNode*> &path) {

	// Starting edge
	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> eProbability = getStableEdgeProbabilities(epsilon, power, vecBF);

	std::vector<double> minCladFreq = computeMinCladFreq(startingEdge, vecBF);

	size_t iN1 = path.size();
	for(size_t i=0; i<path.size(); ++i) {
		if(path[i] == startingEdge.n1) {
			iN1 = i;
			break;
		}
	}
	assert(iN1 != path.size());

	std::pair<double, double> freqAndProbStartingEdge = getEdgeFreqAndProb(startingEdge.n1, startingEdge.n2, vecBF, eProbability);
	double edgeProb = freqAndProbStartingEdge.second;

	std::vector< double > pathProbs(2, 1.);
	// Defines ranges for paths
	std::vector< std::vector<int> > ranges(2);
	for(size_t i=iN1+2; i<path.size(); ++i) ranges[0].push_back(i);
	for(int i=iN1-1; i>=0; --i) ranges[1].push_back(i);

	for(size_t iSPR=0; iSPR<2; ++iSPR) {

		TreeNode* cNode = startingEdge.n1;
		TreeNode* nNode = startingEdge.n2;
		if(iSPR==1) std::swap(cNode, nNode);

		vecTN_t ntChildren = nNode->getNonTerminalChildren(cNode);

		for(size_t i=0; i<ranges[iSPR].size(); ++i) {
			int iP = ranges[iSPR][i];

			int index = ntChildren[0]->getId() == path[iP]->getId() ? 0 : 1;
			assert( path[iP]->getId() == ntChildren[index]->getId() );

			// Compute direction prob
			double probStop = 0.;
			std::vector<double> freqs(ntChildren.size()), probs(ntChildren.size());
			getNextStepProbsForStableEdge(epsilon, nNode, ntChildren, vecBF, minCladFreq, probStop, freqs, probs);

			// Prob continuing
			pathProbs[iSPR] *= (1.-probStop);
			// Prob selecting this edge
			pathProbs[iSPR] *= probs[index];

			cNode = nNode;
			nNode = path[iP];

			ntChildren = nNode->getNonTerminalChildren(cNode);
		}

		if(!ntChildren.empty()) { // The probability of stopping given that we could have continued
			// Compute direction prob
			double probStop = 0.;
			std::vector<double> freqs(ntChildren.size()), probs(ntChildren.size());
			getNextStepProbsForStableEdge(epsilon, nNode, ntChildren, vecBF, minCladFreq, probStop, freqs, probs);
			pathProbs[iSPR] *= probStop;

		}
	}

	return edgeProb*pathProbs[0]*pathProbs[1];
}


void AdaptivePBJ::getNextStepProbsForUnstableEdge(double epsilon, TreeNode *parent, vecTN_t &ntChildren,
		std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<double> &eProbability,
				double &probStop, std::vector<double> &freqs, std::vector<double> &probs) {

	double sumProb = 0.;
	for(size_t i=0; i<ntChildren.size(); ++i) {
		std::pair<double, double> freqAndProb = getEdgeFreqAndProb(parent, ntChildren[i], vecBF, eProbability);
		freqs[i] = freqAndProb.first;
		probs[i] = (1.-freqs[i])+epsilon; // We extend the ESPR move along low freq edge
		sumProb += probs[i];
	}

	// Define if we continue
	probStop = 0.;
	for(size_t i=0; i<ntChildren.size(); ++i) {
		probs[i]/=sumProb;

		double tmpFreq = freqs[i] == 0.0 ? 0.2 : freqs[i]; // remove 0 freqs present at start
		probStop += probs[i]*tmpFreq; // We stop when we reach a high freq edge
	}
	// We add a baseline chance of stopping of 0.2
	probStop = (BASELINE_STOCHASTIC_EFFECTS + probStop)/(1.+BASELINE_STOCHASTIC_EFFECTS);
}

void AdaptivePBJ::getNextStepProbsForUnstableEdge(bool firstCall, double epsilon, TreeNode *parent, vecTN_t &ntChildren,
												   Tree::sharedPtr_t tree, split_t &movingSplit,
												   std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF,
												   double &probStop, std::vector<double> &freqs, std::vector<double> &probs) {

	double sumProb = 0.;
	for(size_t i=0; i<ntChildren.size(); ++i) {
		std::pair<double, split_t> freqAndSplit = getEdgeFreqAndSplit(parent, ntChildren[i], vecBF); // The split might not be directed
		split_t currentSplit = tree->findBipartitions(parent, ntChildren[i]); // Directed split
		split_t updatedSplit = currentSplit | movingSplit;
		double updatedSplitFreq = bipartitionMonitor->getSplitFrequency(updatedSplit);
		freqs[i] = freqAndSplit.first;
		if(firstCall) {
			probs[i] = (1.-freqs[i])+epsilon; // We extend the ESPR move along low freq edge
		} else {
			probs[i] = (1.-freqs[i])*updatedSplitFreq+epsilon; // We extend the ESPR move along low freq edge
		}
		sumProb += probs[i];
	}

	// Define if we continue
	probStop = 0.;
	for(size_t i=0; i<ntChildren.size(); ++i) {
		probs[i] /= sumProb;

		double tmpFreq = freqs[i] == 0.0 ? 0.2 : freqs[i]; // remove 0 freqs present at start
		probStop += probs[i]*tmpFreq; // We stop when we reach a high freq edge
	}
	// We add a baseline chance of stopping of 0.2
	probStop = (BASELINE_STOCHASTIC_EFFECTS + probStop)/(1.+BASELINE_STOCHASTIC_EFFECTS);
}

void AdaptivePBJ::choseGraftPoint(double epsilon, Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
								   TreeNode *curNode, vecTN_t &children, Tree::edge_t &graftPoint, double &moveProb) {

	split_t movingSplit = tree->findBipartitions(toGraftParent, toGraft);

	std::vector<double> probs(children.size(), 0.);

	double sumProb = 0.;
	for(size_t i=0; i<children.size(); ++i) {
		split_t newSplit = tree->findBipartitions(curNode, children[i]);
		newSplit |= movingSplit;
		double newSplitFreq = bipartitionMonitor->getSplitFrequency(newSplit);
		probs[i] = epsilon + newSplitFreq;
		sumProb += probs[i];
	}

	for(size_t i=0; i<probs.size(); ++i) {
		probs[i] /= sumProb;
	}

	double uRand = Parallel::mpiMgr().getPRNG()->genUniformDbl();
	size_t index = uRand < probs[0] ? 0 : 1;

	graftPoint.n1 = curNode;
	graftPoint.n2 = children[index];
	moveProb = probs[index];
}

double AdaptivePBJ::defineGraftPointProb(double epsilon, Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
								          TreeNode *curNode, int index, vecTN_t &children) {

	split_t movingSplit = tree->findBipartitions(toGraftParent, toGraft);

	std::vector<double> probs(children.size(), 0.);

	double sumProb = 0.;
	for(size_t i=0; i<children.size(); ++i) {
		split_t newSplit = tree->findBipartitions(curNode, children[i]);
		newSplit |= movingSplit;
		double newSplitFreq = bipartitionMonitor->getSplitFrequency(newSplit);
		probs[i] = epsilon + newSplitFreq;
		sumProb += probs[i];
	}

	for(size_t i=0; i<probs.size(); ++i) {
		probs[i] /= sumProb;
	}

	return probs[index];
}

void AdaptivePBJ::findAdaptiveGraftPoint(double epsilon, Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
										  Tree::edge_t &graftPoint, double &pathProb, std::vector<TreeNode*> &path) {

	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> eProbability(vecBF.size(), 0.);

	split_t movingSplit;
	if(ADAPTIVE_TYPE == UPDATED_ADAPTIVE) {
		movingSplit = tree->findBipartitions(toGraftParent, toGraft);
	}

	TreeNode* cNode = toGraft;
	TreeNode* nNode = toGraftParent;
	vecTN_t ntChildren = nNode->getNonTerminalChildren(cNode);

	bool graftEdgeDefined = false;

	//std::cout << "Starting edge : " << cNode->getId() << " -- " << nNode->getId() << std::endl;

	pathProb = 1.0;
	while(!ntChildren.empty()) {

		double probStop = 0.;
		std::vector<double> freqs(ntChildren.size()), probs(ntChildren.size());
		if(ADAPTIVE_TYPE == REM_ADAPTIVE) {
			getNextStepProbsForUnstableEdge(epsilon, nNode, ntChildren, vecBF, eProbability, probStop, freqs, probs);
		} else {
			getNextStepProbsForUnstableEdge(path.empty(), epsilon, nNode, ntChildren, tree, movingSplit, vecBF, probStop, freqs, probs);
		}

		if(!path.empty()) { // This enables to force a first move
			double uRand1 = Parallel::mpiMgr().getPRNG()->genUniformDbl();
			bool doStop = uRand1 < probStop;

			//std::cout << "Stop prob " << probStop << std::endl;

			if(doStop) { // we stop and define graft edge as an edge with high freq (since it will preserve its bipartion)
				pathProb *= probStop;

				//std::cout << "Stoping with prob " << probStop << std::endl;

				graftEdgeDefined = true;
				// we now draw an edge with high stopping prob (proportionel to its freq -- including terminal nodes)
				/*ntChildren = nNode->getChildren(cNode);
				getNextStepProbsForStableEdge(epsilon, nNode, ntChildren, vecBF, eProbability, probStop, freqs, probs);
				double uRand3 = Parallel::mpiMgr().getPRNG()->genUniformDbl();
				size_t index = uRand3 < probs[0] ? 0 : 1;

				graftPoint.n1 = nNode;
				graftPoint.n2 = ntChildren[index];
				pathProb *= probs[index];*/

				////////////////////////////////////////////////////////////
				Tree::edge_t graftPoint2;
				double moveProb2 = 0.;
				vecTN_t children2 = nNode->getChildren(cNode);
				choseGraftPoint(epsilon, tree, toGraft, toGraftParent, nNode, children2, graftPoint2, moveProb2);
				//std::cout << "Initial choice : " << graftPoint.n2->getId() << " with prob : " << probs[index] << " -- new : " << graftPoint2.n2->getId() << " with prob : " << moveProb2 << std::endl;
				graftPoint.n1 = graftPoint2.n1;
				graftPoint.n2 = graftPoint2.n2;
				pathProb *= moveProb2;
				////////////////////////////////////////////////////////////

				break;
			}

			// We continue
			pathProb *= (1.-probStop);
		}

		double uRand2 = Parallel::mpiMgr().getPRNG()->genUniformDbl();
		// Define next edge
		size_t index = uRand2 < probs[0] ? 0 : 1;
		pathProb *= probs[index]; // probability of choosing this edge

		cNode = nNode;
		nNode = ntChildren[index];
		ntChildren = nNode->getNonTerminalChildren(cNode);

		//std::cout << "Continuing and selecting node : " << nNode->getId() << " with prob " << probs[index] << std::endl;

		path.push_back(nNode);
	}

	if(!graftEdgeDefined && !path.empty()) { // We arrive here if we reach an endpoint (two terminal nodes)
		/*graftPoint.n1 = nNode;
		ntChildren = nNode->getChildren(cNode);
		double uRand4 = Parallel::mpiMgr().getPRNG()->genUniformDbl();
		size_t index = uRand4 < 0.5 ? 0 : 1;
		graftPoint.n2 = ntChildren[index];
		pathProb *= 0.5;*/

		////////////////////////////////////////////////////////////
		Tree::edge_t graftPoint2;
		double moveProb2 = 0.;
		vecTN_t children2 = nNode->getChildren(cNode);
		choseGraftPoint(epsilon, tree, toGraft, toGraftParent, nNode, children2, graftPoint2, moveProb2);
		//std::cout << "Initial choice 2 : " << graftPoint.n2->getId() << " with prob : " << 0.5 << " -- new : " << graftPoint2.n2->getId() << " with prob : " << moveProb2 << std::endl;
		graftPoint.n1 = graftPoint2.n1;
		graftPoint.n2 = graftPoint2.n2;
		pathProb *= moveProb2;
		////////////////////////////////////////////////////////////

		//std::cout << "Reached a no way out." << std::endl;
	}

	/*if(path.empty()) {
		std::cout << "No move" << std::endl;
	}*/

}

void AdaptivePBJ::defineAdaptiveGraftPointProb(double epsilon, Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
										  Tree::edge_t &collapsePoint, double &pathProb, std::vector<TreeNode*> &path) {

	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> eProbability(vecBF.size(), 0.);

	//std::cout << "collapse Edge : " << collapsePoint.n1->getId() << " -- "  << collapsePoint.n2->getId() << std::endl;

	split_t movingSplit;
	if(ADAPTIVE_TYPE == UPDATED_ADAPTIVE) {
		movingSplit = tree->findBipartitions(toGraftParent, toGraft);
	}

	TreeNode* cNode = toGraft;
	TreeNode* nNode = toGraftParent;
	vecTN_t ntChildren = nNode->getNonTerminalChildren(cNode);

	//std::cout << "Starting edge : " << cNode->getId() << " -- " << nNode->getId() << std::endl;

	for(int iP=path.size()-1; iP>=0; --iP) {

		int index = ntChildren[0]->getId() == path[iP]->getId() ? 0 : 1;
		assert( path[iP]->getId() == ntChildren[index]->getId() );

		double probStop = 0.;
		std::vector<double> freqs(ntChildren.size()), probs(ntChildren.size());
		if(ADAPTIVE_TYPE == REM_ADAPTIVE) {
			getNextStepProbsForUnstableEdge(epsilon, nNode, ntChildren, vecBF, eProbability, probStop, freqs, probs);
		} else {
			bool first = ((int)path.size()-1) == iP;
			getNextStepProbsForUnstableEdge(first, epsilon, nNode, ntChildren, tree, movingSplit, vecBF, probStop, freqs, probs);
		}

		//std::cout << "Continuing and selecting node : " << path[iP]->getId() << " with prob " << probs[index] << std::endl;

		// We continue
		if(iP < (int)path.size()-1) { // we only look to stop after the first element of the path
			pathProb *= (1.-probStop);
		}
		pathProb *= probs[index]; // probability of choosing this edge

		cNode = nNode;
		nNode = ntChildren[index];
		ntChildren = nNode->getNonTerminalChildren(cNode);

		//std::cout << "New edge : " << cNode->getId() << " -- " << nNode->getId() << " -- " << ntChildren.size() << std::endl;
	}

	if(ntChildren.empty()) {

		/////////////////////////////////////////////////////
		vecTN_t children = nNode->getChildren(cNode);
		int index = children[0]->getId() == collapsePoint.n2->getId() ? 0 : 1;
		double tmpProb = defineGraftPointProb(epsilon, tree, toGraft, toGraftParent, nNode, index, children);
		pathProb *= tmpProb;
		/////////////////////////////////////////////////////
		//pathProb *= 0.5; // only terminal, so 1 chance over 2
		//std::cout << "Dead end " << std::endl;
	} else {
		double probStop = 0.;
		std::vector<double> freqs(ntChildren.size()), probs(ntChildren.size());
		if(ADAPTIVE_TYPE == REM_ADAPTIVE) {
			getNextStepProbsForUnstableEdge(epsilon, nNode, ntChildren, vecBF, eProbability, probStop, freqs, probs);
		} else {
			getNextStepProbsForUnstableEdge(path.empty(), epsilon, nNode, ntChildren, tree, movingSplit, vecBF, probStop, freqs, probs);
		}
		pathProb *= probStop;


		//ntChildren = nNode->getChildren(cNode);
		//getNextStepProbsForStableEdge(epsilon, nNode, ntChildren, vecBF, eProbability, probStop, freqs, probs);

		assert(nNode == collapsePoint.n1);
		/*int index = ntChildren[0]->getId() == collapsePoint.n2->getId() ? 0 : 1;
		assert( ntChildren[index]->getId() == collapsePoint.n2->getId() );
		pathProb *= probs[index];*/

		/////////////////////////////////////////////////////
		vecTN_t children = nNode->getChildren(cNode);
		int index = children[0]->getId() == collapsePoint.n2->getId() ? 0 : 1;
		double tmpProb = defineGraftPointProb(epsilon, tree, toGraft, toGraftParent, nNode, index, children);
		pathProb *= tmpProb;
		/////////////////////////////////////////////////////

	}

}


Move::sharedPtr_t AdaptivePBJ::stablePathMove(const double pe, double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	double epsilon = getEpsilon(aEpsilonFreq) / tree->getInternalEdges().size();

	assert( pe >= 0 && pe <= 1.);

	long int h = tree->getHashKey();


	double fwProb = 1.0;
	Tree::edge_t startingEdge;
	std::vector<TreeNode*> stablePath;
	buildStablePath(epsilon, aPowerFreq, tree, fwProb, startingEdge, stablePath);

	double moveProb = 1.;
	std::vector<size_t> nEdges;
	std::vector<size_t> involvedNodes;

	involvedNodes.push_back(startingEdge.n1->getId());
	involvedNodes.push_back(startingEdge.n2->getId());

	involvedNodes.push_back(stablePath.size()); // Trick to keep track of the variable number of node in the path
	// Keep track of path
	for(size_t iP=0; iP<stablePath.size(); ++iP) {
		involvedNodes.push_back(stablePath[iP]->getId());
	}

	// Draw move for each side.
	for(size_t iSPR=0; iSPR<2; ++iSPR) {

		// First "direction"
		size_t iDir = aRNG->genUniformInt(0, 1);
		TreeNode *toGraft, *toGraftParent;
		if(iSPR == 1) {
			toGraft = stablePath[stablePath.size()-2];
			toGraftParent = stablePath[stablePath.size()-1];
		} else {
			toGraft = stablePath[1];
			toGraftParent = stablePath[0];
		}
		vecTN_t directions = toGraftParent->getChildren(toGraft);

		if(directions[iDir]->isTerminal()) iDir = 1-iDir;
		//assert(!directions[0]->isTerminal() && !directions[1]->isTerminal() ); // DEBUG

		// Find path and graft point
		Tree::edge_t graftEdge;
		std::vector<TreeNode*> path;
		tm.findGraftPoint(pe, directions[iDir], toGraftParent, graftEdge, path);
		nEdges.push_back(path.size());

		//if(directions[0]->isTerminal() && directions[1]->isTerminal()) {std::cout << path.size() << std::endl;}


		// Check constraints for fw move
		bool isForwardConstrained = graftEdge.n2->isTerminal();

		// Get collapse edge
		Tree::edge_t collapseEdge = {directions[0], directions[1]};
		if(collapseEdge.n1 != directions[iDir]) std::swap(collapseEdge.n1, collapseEdge.n2);

		// Check constraint for backward move
		bool isBackwardConstrained = collapseEdge.n2->isTerminal();

		//std::cout << path.size() << " ------ " << (isForwardConstrained ? "1" : "0") << " -- " << (isBackwardConstrained ? "1" : "0") << std::endl;

		if(path.size() > 0) {

			// Map branches (fw
			split_info_t beforeMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t beforeLeftBL = createSplitInfo(tree, toGraftParent, collapseEdge.n2, sample);
			split_info_t beforeRightBL = createSplitInfo(tree, graftEdge.n1, graftEdge.n2, sample);
			split_info_t centralBL = createSplitInfo(tree, toGraftParent, path.front(), sample);
			std::vector<split_info_t> vecBefore = boost::assign::list_of(beforeMovedBL)(beforeLeftBL)(beforeRightBL)(centralBL);
			for(size_t i=1; i<path.size(); ++i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecBefore.push_back(tmpBL);
			}

			// We now have to prune and graft
			tm.pruneAndGraft(tree, toGraft, toGraftParent, collapseEdge, graftEdge, path);

			// Trying to prepare things for mapping
			split_info_t afterMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t afterLeftBL = createSplitInfo(tree, collapseEdge.n1, collapseEdge.n2, sample);
			split_info_t afterRightBL = createSplitInfo(tree, toGraftParent, graftEdge.n2, sample);
			split_info_t afterCentralBL = createSplitInfo(tree, toGraftParent, graftEdge.n1, sample);
			std::vector<split_info_t> vecAfter = boost::assign::list_of(afterMovedBL)(afterLeftBL)(afterRightBL)(afterCentralBL);
			assert(graftEdge.n1 == path.back());
			for(size_t i=path.size()-1; i>0; --i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecAfter.push_back(tmpBL);
			}

			mapBranchLengths(vecBefore, vecAfter, sample);

			//std::cout << std::scientific << "TYPE 1 > " << iSPR << " -- " << path.size()  << " -- " << (isForwardConstrained ? "1" : "0") << " -- " << (isBackwardConstrained ? "1" : "0") << std::endl;
		}

		// Pack first part of the move
		involvedNodes.push_back(collapseEdge.n1->getId());
		involvedNodes.push_back(collapseEdge.n2->getId());
		involvedNodes.push_back(graftEdge.n1->getId());
		involvedNodes.push_back(graftEdge.n2->getId());
		involvedNodes.push_back(path.size()); // Trick to keep track of the variable number of node in the path
		// Keep track of path
		for(size_t iP=0; iP<path.size(); ++iP) {
			involvedNodes.push_back(path[iP]->getId());
		}

		double prob = 1.0;
		if(isForwardConstrained == isBackwardConstrained) {
			prob = 1.0;
		} else if(isForwardConstrained) {
			prob = (1.-pe)/0.5;
		} else if(isBackwardConstrained) {
			prob = 0.5/(1.-pe);
		}
		moveProb *= prob;

	}

	double bwProb = defineStablePathProb(epsilon, aPowerFreq, tree, startingEdge, stablePath);

	assert(nEdges.size() == 2);
	if(nEdges[0] == 0 && nEdges[1] == 0) {
		moveProb = 0.;
	}
	moveProb *= (bwProb/fwProb);

	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	Move::sharedPtr_t newMove(new Move(h, newH, Move::AdaptivePBJ));
	newMove->setInvolvedNodes(involvedNodes);
	newMove->setNEdgeDistances(nEdges);
	newMove->setProbability(moveProb);

	/*std::cout << stablePath.size() << std::scientific << "--" << fwProb << "--" << bwProb << std::endl;
	std::cout << moveProb << std::endl;
	std::cout << "TYPE 1 > " << stablePath.size() << "--" << fwProb << "--" << bwProb << std::endl;*/

	//std::cout << stablePath.size() << std::endl;
	//if(fabs(bwProb/fwProb - _bwProb/_fwProb) > 1e-5) {
	//std::cout << std::scientific << stablePath.size() << std::endl << fwProb << "\t" << bwProb << "\t" << bwProb/fwProb << std::endl;
	//std::cout << "-------------------------------------------------------" << std::endl;
	//}


	return newMove;

}



Move::sharedPtr_t AdaptivePBJ::stablePathMove2(double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	double epsilon = getEpsilon(aEpsilonFreq) / tree->getInternalEdges().size();

	long int h = tree->getHashKey();

	double fwProb = 1.0;
	Tree::edge_t startingEdge;
	std::vector<TreeNode*> stablePath;
	buildStablePath(epsilon, aPowerFreq, tree, fwProb, startingEdge, stablePath);

	if(false) {
		// Keep fwProb as such
	} else {
		fwProb = 0.;
		for(size_t i=1; i<stablePath.size(); ++i) {
			Tree::edge_t sE;
			sE.n1 = stablePath[i-1];
			sE.n2 = stablePath[i];
			double aProb = defineStablePathProb(epsilon, aPowerFreq, tree, sE, stablePath);
			//std::cout << aProb << "\t";
			fwProb += aProb;
		}
	}

	double moveProb = 1.;
	std::vector<size_t> nEdges;
	std::vector<size_t> involvedNodes;

	involvedNodes.push_back(startingEdge.n1->getId());
	involvedNodes.push_back(startingEdge.n2->getId());

	involvedNodes.push_back(stablePath.size()); // Trick to keep track of the variable number of node in the path
	// Keep track of path
	for(size_t iP=0; iP<stablePath.size(); ++iP) {
		involvedNodes.push_back(stablePath[iP]->getId());
	}

	// Draw move for each side.
	for(size_t iSPR=0; iSPR<2; ++iSPR) {

		//std::cout << "New move : " << iSPR << std::endl;

		// First "direction"
		TreeNode *toGraft, *toGraftParent;
		if(iSPR == 1) {
			toGraft = stablePath[stablePath.size()-2];
			toGraftParent = stablePath[stablePath.size()-1];
		} else {
			toGraft = stablePath[1];
			toGraftParent = stablePath[0];
		}

		// Find path and graft point
		double fwPathProb = 0.;
		Tree::edge_t graftEdge;
		std::vector<TreeNode*> path;
		//tm.findGraftPoint(pe, directions[iDir], toGraftParent, graftEdge, path);
		findAdaptiveGraftPoint(epsilon, tree, toGraft, toGraftParent, graftEdge, fwPathProb, path);
		nEdges.push_back(path.size());

		// Get collapse edge
		vecTN_t collapseNodes = toGraftParent->getChildren(toGraft);
		Tree::edge_t collapseEdge = {collapseNodes[0], collapseNodes[1]};

		if(path.empty()) graftEdge = collapseEdge;

		//std::cout << path.size() << " ------ " << (isForwardConstrained ? "1" : "0") << " -- " << (isBackwardConstrained ? "1" : "0") << std::endl;

		if(path.size() > 0) {

			if(collapseEdge.n1 != path.front()) std::swap(collapseEdge.n1, collapseEdge.n2);

			// Map branches (fw
			split_info_t beforeMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t beforeLeftBL = createSplitInfo(tree, toGraftParent, collapseEdge.n2, sample);
			split_info_t beforeRightBL = createSplitInfo(tree, graftEdge.n1, graftEdge.n2, sample);
			split_info_t centralBL = createSplitInfo(tree, toGraftParent, path.front(), sample);
			std::vector<split_info_t> vecBefore = boost::assign::list_of(beforeMovedBL)(beforeLeftBL)(beforeRightBL)(centralBL);
			for(size_t i=1; i<path.size(); ++i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecBefore.push_back(tmpBL);
			}

			// We now have to prune and graft
			tm.pruneAndGraft(tree, toGraft, toGraftParent, collapseEdge, graftEdge, path);

			double bwPathProb = 1.0;
			defineAdaptiveGraftPointProb(epsilon, tree, toGraft, toGraftParent, collapseEdge, bwPathProb, path);
			moveProb *= bwPathProb/fwPathProb;

			//std::cout << std::scientific << "TYPE 2 > " << iSPR << " -- " << path.size() << " -- " << fwPathProb << " -- " << bwPathProb << std::endl;

			// Trying to prepare things for mapping
			split_info_t afterMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t afterLeftBL = createSplitInfo(tree, collapseEdge.n1, collapseEdge.n2, sample);
			split_info_t afterRightBL = createSplitInfo(tree, toGraftParent, graftEdge.n2, sample);
			split_info_t afterCentralBL = createSplitInfo(tree, toGraftParent, graftEdge.n1, sample);
			std::vector<split_info_t> vecAfter = boost::assign::list_of(afterMovedBL)(afterLeftBL)(afterRightBL)(afterCentralBL);
			assert(graftEdge.n1 == path.back());
			for(size_t i=path.size()-1; i>0; --i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecAfter.push_back(tmpBL);
			}

			mapBranchLengths(vecBefore, vecAfter, sample);
		}

		// Pack first part of the move
		involvedNodes.push_back(collapseEdge.n1->getId());
		involvedNodes.push_back(collapseEdge.n2->getId());
		involvedNodes.push_back(graftEdge.n1->getId());
		involvedNodes.push_back(graftEdge.n2->getId());
		involvedNodes.push_back(path.size()); // Trick to keep track of the variable number of node in the path
		// Keep track of path
		for(size_t iP=0; iP<path.size(); ++iP) {
			involvedNodes.push_back(path[iP]->getId());
		}

	}

	double bwProb = 0.;
	if(false) {
		bwProb = defineStablePathProb(epsilon, aPowerFreq, tree, startingEdge, stablePath); // Current scenario probability
	} else {
		for(size_t i=1; i<stablePath.size(); ++i) {
			Tree::edge_t sE;
			sE.n1 = stablePath[i-1];
			sE.n2 = stablePath[i];
			double aProb = defineStablePathProb(epsilon, aPowerFreq, tree, sE, stablePath);
			bwProb += aProb;
		}
	}

	assert(nEdges.size() == 2);
	if(nEdges[0] == 0 && nEdges[1] == 0) {
		moveProb = 0.;
		//nWasted++;
		//std::cout << "Epsilon : " << epsilon << std::endl;
		//std::cout << "[2]Double terminal : " << stablePath.size() << std::endl;
	}
	//nProposal++;
	moveProb *= (bwProb/fwProb);

	//if(nProposal % 10000 == 0) std::cout << "Wasted ratio " << 100.*(double)nWasted/(double)nProposal << std::endl;

	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	Move::sharedPtr_t newMove(new Move(h, newH, Move::AdaptivePBJ));
	newMove->setInvolvedNodes(involvedNodes);
	newMove->setNEdgeDistances(nEdges);
	newMove->setProbability(moveProb);


	//std::cout << "TYPE 2 > " << stablePath.size() << "--" << fwProb << "--" << bwProb << std::endl;
	//std::cout << "-------------------------------------------------------" << std::endl;

	//std::cout << moveProb << std::endl;

	return newMove;

}




Move::sharedPtr_t AdaptivePBJ::eSPRPathMove(double aEpsilonFreq, double aPowerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	double epsilon = getEpsilon(aEpsilonFreq) / tree->getInternalEdges().size();

	long int h = tree->getHashKey();

	double fwProb = 1.0;
	Tree::edge_t startingEdge;
	std::vector<TreeNode*> stablePath;
	buildStablePath(epsilon, aPowerFreq, tree, fwProb, startingEdge, stablePath);

	if(false) {
		// Keep fwProb as such
	} else {
		fwProb = 0.;
		for(size_t i=1; i<stablePath.size(); ++i) {
			Tree::edge_t sE;
			sE.n1 = stablePath[i-1];
			sE.n2 = stablePath[i];
			double aProb = defineStablePathProb(epsilon, aPowerFreq, tree, sE, stablePath);
			//std::cout << aProb << "\t";
			fwProb += aProb;
		}
	}

	double moveProb = 1.;
	std::vector<size_t> nEdges;
	std::vector<size_t> involvedNodes;

	involvedNodes.push_back(startingEdge.n1->getId());
	involvedNodes.push_back(startingEdge.n2->getId());

	involvedNodes.push_back(stablePath.size()); // Trick to keep track of the variable number of node in the path
	// Keep track of path
	for(size_t iP=0; iP<stablePath.size(); ++iP) {
		involvedNodes.push_back(stablePath[iP]->getId());
	}

	// Check which moves are possible
	vecTN_t childrenEndA = stablePath[0]->getNonTerminalChildren(stablePath[1]);
	vecTN_t childrenEndB = stablePath[stablePath.size()-1]->getNonTerminalChildren(stablePath[stablePath.size()-2]);


	// Chose one of them
	size_t iMove = 0;
	if(!childrenEndA.empty() && childrenEndB.empty()) {
		iMove = 0;
	} else if(childrenEndA.empty() && !childrenEndB.empty()) {
		iMove = 1;
	} else {
		int uRand1 = Parallel::mpiMgr().getPRNG()->genUniformInt(0,1);
		iMove = uRand1;
	}

	// Draw move for each side.
	for(size_t iSPR=0; iSPR<2; ++iSPR) {

		//std::cout << "New move : " << iSPR << std::endl;

		// First "direction"
		TreeNode *toGraft, *toGraftParent;
		if(iSPR == 1) {
			toGraft = stablePath[stablePath.size()-2];
			toGraftParent = stablePath[stablePath.size()-1];
		} else {
			toGraft = stablePath[1];
			toGraftParent = stablePath[0];
		}

		// Find path and graft point
		double fwPathProb = 0.;
		Tree::edge_t graftEdge;
		std::vector<TreeNode*> path;
		//tm.findGraftPoint(pe, directions[iDir], toGraftParent, graftEdge, path);
		findAdaptiveGraftPoint(epsilon, tree, toGraft, toGraftParent, graftEdge, fwPathProb, path);
		if(iSPR != iMove) { // if this is not the selected move, we reset it.
			path.clear();
		}

		nEdges.push_back(path.size());

		// Get collapse edge
		vecTN_t collapseNodes = toGraftParent->getChildren(toGraft);
		Tree::edge_t collapseEdge = {collapseNodes[0], collapseNodes[1]};

		if(path.empty()) graftEdge = collapseEdge;

		//std::cout << path.size() << " ------ " << (isForwardConstrained ? "1" : "0") << " -- " << (isBackwardConstrained ? "1" : "0") << std::endl;

		if(path.size() > 0) {

			if(collapseEdge.n1 != path.front()) std::swap(collapseEdge.n1, collapseEdge.n2);

			// Map branches (fw
			split_info_t beforeMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t beforeLeftBL = createSplitInfo(tree, toGraftParent, collapseEdge.n2, sample);
			split_info_t beforeRightBL = createSplitInfo(tree, graftEdge.n1, graftEdge.n2, sample);
			split_info_t centralBL = createSplitInfo(tree, toGraftParent, path.front(), sample);
			std::vector<split_info_t> vecBefore = boost::assign::list_of(beforeMovedBL)(beforeLeftBL)(beforeRightBL)(centralBL);
			for(size_t i=1; i<path.size(); ++i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecBefore.push_back(tmpBL);
			}

			// We now have to prune and graft
			tm.pruneAndGraft(tree, toGraft, toGraftParent, collapseEdge, graftEdge, path);

			double bwPathProb = 1.0;
			defineAdaptiveGraftPointProb(epsilon, tree, toGraft, toGraftParent, collapseEdge, bwPathProb, path);
			moveProb *= bwPathProb/fwPathProb;

			//std::cout << std::scientific << "TYPE 2 > " << iSPR << " -- " << path.size() << " -- " << fwPathProb << " -- " << bwPathProb << std::endl;

			// Trying to prepare things for mapping
			split_info_t afterMovedBL = createSplitInfo(tree, toGraft, toGraftParent, sample);
			split_info_t afterLeftBL = createSplitInfo(tree, collapseEdge.n1, collapseEdge.n2, sample);
			split_info_t afterRightBL = createSplitInfo(tree, toGraftParent, graftEdge.n2, sample);
			split_info_t afterCentralBL = createSplitInfo(tree, toGraftParent, graftEdge.n1, sample);
			std::vector<split_info_t> vecAfter = boost::assign::list_of(afterMovedBL)(afterLeftBL)(afterRightBL)(afterCentralBL);
			assert(graftEdge.n1 == path.back());
			for(size_t i=path.size()-1; i>0; --i) {
				split_info_t tmpBL = createSplitInfo(tree, path[i-1], path[i], sample);
				vecAfter.push_back(tmpBL);
			}
			mapBranchLengths(vecBefore, vecAfter, sample);
		}

		// Pack first part of the move
		involvedNodes.push_back(collapseEdge.n1->getId());
		involvedNodes.push_back(collapseEdge.n2->getId());
		involvedNodes.push_back(graftEdge.n1->getId());
		involvedNodes.push_back(graftEdge.n2->getId());
		involvedNodes.push_back(path.size()); // Trick to keep track of the variable number of node in the path
		// Keep track of path
		for(size_t iP=0; iP<path.size(); ++iP) {
			involvedNodes.push_back(path[iP]->getId());
		}

	}

	double bwProb = 0.;
	if(false) {
		bwProb = defineStablePathProb(epsilon, aPowerFreq, tree, startingEdge, stablePath); // Current scenario probability
	} else {
		for(size_t i=1; i<stablePath.size(); ++i) {
			Tree::edge_t sE;
			sE.n1 = stablePath[i-1];
			sE.n2 = stablePath[i];
			double aProb = defineStablePathProb(epsilon, aPowerFreq, tree, sE, stablePath);
			bwProb += aProb;
		}
	}

	//std::cout << iMove << "\t" << nEdges[0] << "\t" << nEdges[1] << std::endl;
	assert(nEdges.size() == 2);
	if(nEdges[0] == 0 && nEdges[1] == 0) {
		moveProb = 0.;
		//nWasted++;
		//std::cout << "[3]Double terminal : " << stablePath.size() << std::endl;
	}
	//nProposal++;
	moveProb *= (bwProb/fwProb);

	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	Move::sharedPtr_t newMove(new Move(h, newH, Move::AdaptivePBJ));
	newMove->setInvolvedNodes(involvedNodes);
	newMove->setNEdgeDistances(nEdges);
	newMove->setProbability(moveProb);


	//std::cout << "TYPE 2 > " << stablePath.size() << "--" << fwProb << "--" << bwProb << std::endl;
	//std::cout << "-------------------------------------------------------" << std::endl;

	//std::cout << moveProb << std::endl;

	return newMove;

}

std::string AdaptivePBJ::toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;
	ss << "toGraft : " << toGraft->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "toExplore : " << toExplore->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s1 : " << collapseEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s2 : " << collapseEdge.n2->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e1 : " << graftEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e2 : " << graftEdge.n2->toString() << std::endl;
	for(size_t i=0; i<path.size(); ++i) {
		ss << "**" << std::endl;
		ss << "p" << i << " : " << path[i]->toString() << std::endl;
	}
	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
