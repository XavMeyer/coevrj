//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeProposal.h
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREEPROPOSAL_H_
#define TREEPROPOSAL_H_

#include "../Tree.h"
#include "Move.h"
#include "Parallel/Parallel.h" // IWYU pragma: keep


#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeModifier.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Bipartition/BipartitionMonitor.h"

class RNG;

namespace ParameterBlock {
	class BranchHelper;
}

namespace MolecularEvolution {
namespace TreeReconstruction {

class TreeProposal {
public:
	enum edge_direction_t {FROM_N1_TO_N2, FROM_N2_TO_N1};
public:

	TreeProposal(ParameterBlock::BranchHelper *aBH);
	TreeProposal(Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH);
	virtual ~TreeProposal();

	//virtual void applyMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move) = 0;
	//virtual void undoMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move) = 0;

protected:

	struct split_info_t {
		Bipartition::split_t split;
		bool hasStats;
		size_t count;
		double bl, meanBL, varBL;
		TreeNode *n1, *n2;

		void defineGammaParameters(double &shape, double &scale) {
			assert(hasStats);
			shape = meanBL*meanBL/varBL;
			scale = varBL/meanBL;
		}
	};

	const RNG *aRNG;


	TreeModifier tm;
	Bipartition::BipartitionMonitor::sharedPtr_t bipartitionMonitor;
	ParameterBlock::BranchHelper *branchHelper;

	Tree::edge_t getRndInternalBranch(Tree::sharedPtr_t tree);

	split_info_t createSplitInfo(Tree::sharedPtr_t tree, TreeNode *node1, TreeNode *node2, Sampler::Sample &sample);
	std::string displaySplitInfo(split_info_t &aSplit);

	void mapBranchLengths(std::vector<split_info_t> &beforeSplit, std::vector<split_info_t> &afterSplit, Sampler::Sample &sample);



};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* TREEPROPOSAL_H_ */
