//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveGuidedESPR.h
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVE_GUIDED_ESPR_H_
#define ADAPTIVE_GUIDED_ESPR_H_

#include <stddef.h>

#include "ParameterBlock/BlockStats/ConvUtils/Updater/Mean/BatchEvoMU.h"
#include "Utils/MolecularEvolution/Parsimony/FastFitchEvaluator.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Bipartition/BipartitionMonitor.h"
#include "TreeProposal.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

namespace TR_BP = ::MolecularEvolution::TreeReconstruction::Bipartition;


class AdaptiveGuidedESPR : public TreeProposal {
public:
	enum moveInfo_t {
		DEFAULT=0,
	};

	enum stepNumber_t {ONE_STEP=0, TWO_STEP=1};

public:
	AdaptiveGuidedESPR(Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH,
			   Parsimony::FastFitchEvaluator::sharedPtr_t aFFE);
	~AdaptiveGuidedESPR();

	Move::sharedPtr_t proposeNewTree(double aEpsilon, double aPower, bool exploreOutside, Tree::sharedPtr_t tree, Sampler::Sample &sample);

	void signalMoveHistory(bool accepted);

private:

	static const bool SCALE_BRANCHES, MAPPING_BRANCHES;
	size_t cntIt;

	Parsimony::FastFitchEvaluator::sharedPtr_t fastFitchEvaluator;

	ParameterBlock::BatchEvoMU *evoMu;

	struct involved_tree_structures_t {
		TreeNode *toGraft, *toExplore;
		Tree::edge_t collapseEdge, graftEdge;
		Tree::contiguous_edge_pair_t directedCEP;
		double probability, addFreq, remFreq, stableFreq, parsimonyScore;

		bool isReverseMove(const involved_tree_structures_t& revIts) {
			bool reverse = true;
			reverse = reverse && (toGraft->getId() == revIts.toGraft->getId());
			reverse = reverse && (toExplore->getId() == revIts.toExplore->getId());
			reverse = reverse && ((collapseEdge.n1 == revIts.graftEdge.n1 && collapseEdge.n2== revIts.graftEdge.n2) || (collapseEdge.n1 == revIts.graftEdge.n2 && collapseEdge.n2== revIts.graftEdge.n1));
			reverse = reverse && ((graftEdge.n1 == revIts.collapseEdge.n1 && graftEdge.n2== revIts.collapseEdge.n2) || (graftEdge.n1 == revIts.collapseEdge.n2 && graftEdge.n2== revIts.collapseEdge.n1));
			return reverse;
		}
	};


	double getBiasedMove(bool exploreOutside, double epsilonFreq, double powerFreq,
						Tree::sharedPtr_t tree, TreeNode* &toGraft, TreeNode* &toCollapse,
						TreeNode* &toExplore, TreeNode* &toExtend);
	double getBiasedMoveReverseProbability(bool exploreOutside, double epsilonFreq, double powerFreq,
										Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
										Tree::edge_t collapseEdge, Tree::edge_t graftEdge);

	involved_tree_structures_t getBiasedPairEdgeMove(bool exploreOutside, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree);
	double getBiasedPairEdgeMoveReverseProbability(bool exploreOutside, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, involved_tree_structures_t its);

	std::vector<involved_tree_structures_t> defineContiguousEdgeMoves(
		double epsilonFreq, double powerFreq,
		Tree::sharedPtr_t tree,
		Tree::contiguous_edge_pair_t cep,
		TreeNode* nodeX, Bipartition::split_t splitX,
		vecTN_t children1, std::vector<Bipartition::split_t> split1,
		vecTN_t children2, std::vector<Bipartition::split_t> split2);

	std::string toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
			Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path);

	double queryParsimonyScore(Tree::sharedPtr_t tree);
	std::vector<double> computeMovesProbability(double currentScore, std::vector<double> &scores);


	Move::sharedPtr_t proposeOneEdgeMove(bool exploreOutside, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample);
	Move::sharedPtr_t proposeTwoEdgesMove(bool exploreOutside, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample);

	std::vector<double> getEdgeProbabilities(double epsilonFreq, double powerFreq, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF);
	std::vector<double> getContiguousEdgePairProbabilities(double epsilonFreq, double powerFreq, Tree::sharedPtr_t aTree,
			std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF);


	double getEpsilon(double minEpsilon) const;

	std::string displayBranchInfo(Tree::sharedPtr_t tree, TreeNode *n1, TreeNode *n2, Sampler::Sample &sample);
	std::pair<double, double> applyBranchModifier(split_info_t beforeBL, split_info_t afterBL, Sampler::Sample &sample);

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* ADAPTIVE_GUIDED_ESPR_H_ */
