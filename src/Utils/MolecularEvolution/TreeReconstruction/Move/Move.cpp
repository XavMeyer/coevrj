//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Move.cpp
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#include "Move.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/ESPR.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/ETBR.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/AdaptivePBJ.h"

#include "Utils/Code/BinSerializationSupport.h" // IWYU pragma: keep

namespace MolecularEvolution {
namespace TreeReconstruction {

Move::Move(const Utils::Serialize::buffer_t &aBuffer) {
	load(aBuffer);
}

Move::Move(long int aFromH, long int aToH, moveType_t aMoveType) :
		fromH(aFromH), toH(aToH), moveType(aMoveType), probability(0.) {
	moveInfo = 0;
	fwProb = bwProb = -1.0;
}

Move::~Move() {
}

long int Move::getFromH() const {
	return fromH;
}

long int Move::getToH() const {
	return toH;
}

Move::moveType_t Move::getMoveType() const {
	return moveType;
}

void Move::setForwardProbability(double aP) {
	fwProb = aP;
}

double Move::getForwardProbability() const {
	return fwProb;
}

void Move::setBackwardProbability(double aP) {
	bwProb = aP;
}

double Move::getBackwardProbability() const {
	return bwProb;
}

void Move::setProbability(double aP) {
	probability = aP;
}

double Move::getProbability() const {
	return probability;
}

void Move::setMoveInfo(const int aMoveInfo) {
	moveInfo = aMoveInfo;
}

int Move::getMoveInfo() const {
	return moveInfo;
}

void Move::setNEdgeDistances(const std::vector<size_t> &aNEdges) {
	nEdges = aNEdges;
}

bool Move::equals(long int aFromH, long int aToH, Move::moveType_t aMoveType) const {
	return  moveType == aMoveType &&
			fromH == aFromH &&
			toH == aToH;

}


void Move::addInvolvedNode(size_t id) {
	involvedNodes.push_back(id);
}


void Move::setInvolvedNodes(const std::vector<size_t>& aInvNodes) {
	involvedNodes = aInvNodes;
}

const std::vector<size_t>& Move::getInvolvedNode() const {
	return involvedNodes;
}

std::vector<size_t> Move::getNEdgeDistances() const {
	return nEdges;
}

void Move::setNEdgeDistance(const size_t aNEdge) {
	nEdges.resize(1);
	nEdges[0] = aNEdge;
}

size_t Move::getNEdgeDistance() const {
	assert(nEdges.size() == 1);
	return nEdges.front();
}


const Utils::Serialize::buffer_t& Move::save() {
	serializedBuffer.clear();
	Utils::Serialize::save(*this, serializedBuffer);
	return serializedBuffer;
}

std::string Move::toString() const {
	std::stringstream ss;
	ss << "[" << static_cast<int>(moveType) << "]" << fromH << " -> " << toH << " :: p = " << probability ;

	return ss.str();
}


void Move::apply(Tree::sharedPtr_t tree) {
	if(moveType == Move::STNNI  || moveType == Move::GuidedSTNNI ) {
		assert(tree->getHashKey() == getFromH());

		std::vector<size_t> involvedNodes = getInvolvedNode();
		TreeNode *ptrN1 = tree->getNode(involvedNodes[0]); // n1
		TreeNode *ptrN2 = tree->getNode(involvedNodes[1]); // n2
		TreeNode *ptrB = tree->getNode(involvedNodes[2]); // B
		TreeNode *ptrOther = tree->getNode(involvedNodes[3]); // C/D

		Tree::edge_t edge = {ptrN1, ptrN2};
		// Swap the subtrees
		tm.swapSubtrees(tree, ptrB, ptrOther, edge);

	} else if(moveType == Move::ESPR || moveType == Move::AdaptiveESPR || moveType == Move::GuidedESPR) {
		assert(tree->getHashKey() == getFromH());

		// Keep track of nodes <m1, m2, s1, s2, e1, e2>
		TreeNode *toGraft = tree->getNode(involvedNodes[ESPR::NODE_M1]); // m1
		TreeNode *toExplore = tree->getNode(involvedNodes[ESPR::NODE_M2]); // m2
		TreeNode *ptrS1 = tree->getNode(involvedNodes[ESPR::NODE_S1]); // s1
		TreeNode *ptrS2 = tree->getNode(involvedNodes[ESPR::NODE_S2]); // s2
		TreeNode *ptrE1 = tree->getNode(involvedNodes[ESPR::NODE_E1]); // e1
		TreeNode *ptrE2 = tree->getNode(involvedNodes[ESPR::NODE_E2]); // e2

		// Get path
		std::vector<TreeNode *> path;
		for(size_t iN=6; iN<involvedNodes.size(); ++iN) {
			path.push_back(tree->getNode(involvedNodes[iN]));
		}

		// Move subtrees
		// We now have to prune and graft
		// forward move is : m1<-->m2 in between e1<-->e2
		// reverse move is : m1<-->m2 in between s1<-->s2
		Tree::edge_t graftEdge = {ptrE1, ptrE2};
		Tree::edge_t collapseEdge = {ptrS1, ptrS2};

		//std::cout << "Apply espr : " << moveType << " -- move size = " << nEdgeDistance << std::endl;
		tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path);

	} else if(moveType == Move::ETBR) {
		assert(tree->getHashKey() == getFromH());

		ETBR::unpackedMoveId_t nodesId = ETBR::unpackMove(getInvolvedNode());

		// Base nodes
		TreeNode *toGraft = tree->getNode(nodesId.edgeN1); // m1
		TreeNode *toExplore = tree->getNode(nodesId.edgeN2); // m2


		{ // SPR move 1
			TreeNode *ptrE1C1 = tree->getNode(nodesId.collapseE1N1); // s1
			TreeNode *ptrE1C2 = tree->getNode(nodesId.collapseE1N2); // s2
			TreeNode *ptrE1G1 = tree->getNode(nodesId.graftE1N1); // e1
			TreeNode *ptrE1G2 = tree->getNode(nodesId.graftE1N2); // e2
			Tree::edge_t graftEdge = {ptrE1G1, ptrE1G2};
			Tree::edge_t collapseEdge = {ptrE1C1, ptrE1C2};

			// Get path
			std::vector<TreeNode*> path1;
			for(size_t i=0; i<nodesId.path1.size(); ++i) {
				path1.push_back(tree->getNode(nodesId.path1[i]));
			}

			if(!nodesId.path1.empty())
				tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path1);
		}

		{ // SPR move 2
			TreeNode *ptrE2C1 = tree->getNode(nodesId.collapseE2N1); // s1
			TreeNode *ptrE2C2 = tree->getNode(nodesId.collapseE2N2); // s2
			TreeNode *ptrE2G1 = tree->getNode(nodesId.graftE2N1); // e1
			TreeNode *ptrE2G2 = tree->getNode(nodesId.graftE2N2); // e2
			Tree::edge_t graftEdge = {ptrE2G1, ptrE2G2};
			Tree::edge_t collapseEdge = {ptrE2C1, ptrE2C2};

			// Get path
			std::vector<TreeNode*> path2;
			for(size_t i=0; i<nodesId.path2.size(); ++i) {
				path2.push_back(tree->getNode(nodesId.path2[i]));
			}
			std::swap(toGraft, toExplore);
			if(!nodesId.path2.empty())
				tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path2);
		}

	} else if(moveType == Move::AdaptivePBJ) {
		assert(tree->getHashKey() == getFromH());

		AdaptivePBJ::unpackedMoveId_t nodesId = AdaptivePBJ::unpackMove(getInvolvedNode());

		std::vector<TreeNode*> stablePath;
		for(size_t i=0; i<nodesId.stablePath.size(); ++i) {
			stablePath.push_back(tree->getNode(nodesId.stablePath[i]));
		}


		{ // SPR move 1
			TreeNode *ptrE1C1 = tree->getNode(nodesId.collapseE1N1); // s1
			TreeNode *ptrE1C2 = tree->getNode(nodesId.collapseE1N2); // s2
			TreeNode *ptrE1G1 = tree->getNode(nodesId.graftE1N1); // e1
			TreeNode *ptrE1G2 = tree->getNode(nodesId.graftE1N2); // e2
			Tree::edge_t graftEdge = {ptrE1G1, ptrE1G2};
			Tree::edge_t collapseEdge = {ptrE1C1, ptrE1C2};

			// Get path
			std::vector<TreeNode*> path1;
			for(size_t i=0; i<nodesId.path1.size(); ++i) {
				path1.push_back(tree->getNode(nodesId.path1[i]));
			}

			// Base nodes
			TreeNode *toGraft = stablePath[1];
			TreeNode *toExplore = stablePath[0];

			if(!nodesId.path1.empty())
				tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path1);
		}

		{ // SPR move 2
			TreeNode *ptrE2C1 = tree->getNode(nodesId.collapseE2N1); // s1
			TreeNode *ptrE2C2 = tree->getNode(nodesId.collapseE2N2); // s2
			TreeNode *ptrE2G1 = tree->getNode(nodesId.graftE2N1); // e1
			TreeNode *ptrE2G2 = tree->getNode(nodesId.graftE2N2); // e2
			Tree::edge_t graftEdge = {ptrE2G1, ptrE2G2};
			Tree::edge_t collapseEdge = {ptrE2C1, ptrE2C2};

			// Get path
			std::vector<TreeNode*> path2;
			for(size_t i=0; i<nodesId.path2.size(); ++i) {
				path2.push_back(tree->getNode(nodesId.path2[i]));
			}

			TreeNode *toGraft = stablePath[stablePath.size()-2];
			TreeNode *toExplore = stablePath[stablePath.size()-1];
			if(!nodesId.path2.empty())
				tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path2);
		}

	} else if(moveType == Move::AdaptiveNStepsNNI) {
		assert(tree->getHashKey() == getFromH());

		size_t N_STEP = nEdges.front();
		assert(N_STEP*4 == involvedNodes.size());

		size_t iI=0;
		for(size_t iM=0; iM<N_STEP; ++iM) {

			TreeNode *ptrA = tree->getNode(involvedNodes[iI]); iI++;
			TreeNode *ptrC1 = tree->getNode(involvedNodes[iI]); iI++;
			TreeNode *ptrC2 = tree->getNode(involvedNodes[iI]); iI++;
			TreeNode *ptrB = tree->getNode(involvedNodes[iI]); iI++;

			Tree::edge_t edge;
			edge.n1 = ptrC1;
			edge.n2 = ptrC2;

			tm.swapSubtrees(tree, ptrA, ptrB, edge);
		}

	} else {
		assert(false && "Unknown move type for move");
	}
}

void Move::undo(Tree::sharedPtr_t tree) {
	if(moveType == Move::STNNI || moveType == Move::GuidedSTNNI) {
		assert(tree->getHashKey() == getToH());

		TreeNode *ptrN1 = tree->getNode(involvedNodes[0]); // n1
		TreeNode *ptrN2 = tree->getNode(involvedNodes[1]); // n2
		TreeNode *ptrB = tree->getNode(involvedNodes[2]); // B
		TreeNode *ptrOther = tree->getNode(involvedNodes[3]); // C/D

		Tree::edge_t edge = {ptrN1, ptrN2};

		// Swap the subtrees
		tm.swapSubtrees(tree, ptrOther, ptrB, edge);

	} else if(moveType == Move::ESPR || moveType == Move::AdaptiveESPR || moveType == Move::GuidedESPR) {
		assert(tree->getHashKey() == getToH());

		// Keep track of nodes <m1, m2, s1, s2, e1, e2>
		TreeNode *toGraft = tree->getNode(involvedNodes[ESPR::NODE_M1]); // m1
		TreeNode *toExplore = tree->getNode(involvedNodes[ESPR::NODE_M2]); // m2
		TreeNode *ptrS1 = tree->getNode(involvedNodes[ESPR::NODE_S1]); // s1
		TreeNode *ptrS2 = tree->getNode(involvedNodes[ESPR::NODE_S2]); // s2
		TreeNode *ptrE1 = tree->getNode(involvedNodes[ESPR::NODE_E1]); // e1
		TreeNode *ptrE2 = tree->getNode(involvedNodes[ESPR::NODE_E2]); // e2

		// Get path
		std::vector<TreeNode *> path;
		for(size_t iN=involvedNodes.size()-1; iN>=6; --iN) {
			path.push_back(tree->getNode(involvedNodes[iN]));
		}

		// Move subtrees
		// We now have to prune and graft
		// forward move was : m1<-->m2 in between e1<-->e2
		// reverse move is : m1<-->m2 in between s1<-->s2
		Tree::edge_t graftEdge = {ptrS1, ptrS2};
		Tree::edge_t collapseEdge = {ptrE1, ptrE2};

		//std::cout << "Reverse espr : " << moveType << " -- move size = " << nEdgeDistance << std::endl;
		tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path);

	} else if(moveType == Move::ETBR) {
		assert(tree->getHashKey() == getToH());

		ETBR::unpackedMoveId_t nodesId = ETBR::unpackMove(getInvolvedNode());

		// Base nodes
		TreeNode *toGraft = tree->getNode(nodesId.edgeN1); // m1
		TreeNode *toExplore = tree->getNode(nodesId.edgeN2); // m2


		{ // SPR move 1
			TreeNode *ptrE1C1 = tree->getNode(nodesId.collapseE1N1); // s1
			TreeNode *ptrE1C2 = tree->getNode(nodesId.collapseE1N2); // s2
			TreeNode *ptrE1G1 = tree->getNode(nodesId.graftE1N1); // e1
			TreeNode *ptrE1G2 = tree->getNode(nodesId.graftE1N2); // e2
			// Collapse edge becomes graft, and inversely graft edge becomes collapse
			Tree::edge_t graftEdge = {ptrE1C1, ptrE1C2};
			Tree::edge_t collapseEdge = {ptrE1G1, ptrE1G2};

			// Get path
			std::vector<TreeNode*> path1;
			for(int i=nodesId.path1.size()-1; i>=0; --i) {
				path1.push_back(tree->getNode(nodesId.path1[i]));
			}

			if(!nodesId.path1.empty())
				tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path1);
		}

		{ // SPR move 2
			TreeNode *ptrE2C1 = tree->getNode(nodesId.collapseE2N1); // s1
			TreeNode *ptrE2C2 = tree->getNode(nodesId.collapseE2N2); // s2
			TreeNode *ptrE2G1 = tree->getNode(nodesId.graftE2N1); // e1
			TreeNode *ptrE2G2 = tree->getNode(nodesId.graftE2N2); // e2
			// Collapse edge becomes graft, and inversely graft edge becomes collapse
			Tree::edge_t graftEdge = {ptrE2C1, ptrE2C2};
			Tree::edge_t collapseEdge = {ptrE2G1, ptrE2G2};

			// Get path
			std::vector<TreeNode*> path2;
			for(int i=nodesId.path2.size()-1; i>=0; --i) {
				path2.push_back(tree->getNode(nodesId.path2[i]));
			}
			std::swap(toGraft, toExplore);

			if(!nodesId.path2.empty())
				tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path2);
		}
	} else if(moveType == Move::AdaptivePBJ) {
		assert(tree->getHashKey() == getToH());

		AdaptivePBJ::unpackedMoveId_t nodesId = AdaptivePBJ::unpackMove(getInvolvedNode());
		//std::cout << "Undo : move unpacked." << std::endl;

		std::vector<TreeNode*> stablePath;
		for(size_t i=0; i<nodesId.stablePath.size(); ++i) {
			stablePath.push_back(tree->getNode(nodesId.stablePath[i]));
		}


		{ // SPR move 1
			TreeNode *ptrE1C1 = tree->getNode(nodesId.collapseE1N1); // s1
			TreeNode *ptrE1C2 = tree->getNode(nodesId.collapseE1N2); // s2
			TreeNode *ptrE1G1 = tree->getNode(nodesId.graftE1N1); // e1
			TreeNode *ptrE1G2 = tree->getNode(nodesId.graftE1N2); // e2
			Tree::edge_t graftEdge = {ptrE1C1, ptrE1C2};
			Tree::edge_t collapseEdge = {ptrE1G1, ptrE1G2};

			// Get path
			std::vector<TreeNode*> path1;
			for(size_t i=0; i<nodesId.path1.size(); ++i) {
				path1.push_back(tree->getNode(nodesId.path1[i]));
			}


			TreeNode *toGraft = stablePath[1];
			TreeNode *toExplore = stablePath[0];
			if(!nodesId.path1.empty())
				tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path1);
		}
		//std::cout << "Undo : first step." << std::endl;

		{ // SPR move 2
			TreeNode *ptrE2C1 = tree->getNode(nodesId.collapseE2N1); // s1
			TreeNode *ptrE2C2 = tree->getNode(nodesId.collapseE2N2); // s2
			TreeNode *ptrE2G1 = tree->getNode(nodesId.graftE2N1); // e1
			TreeNode *ptrE2G2 = tree->getNode(nodesId.graftE2N2); // e2
			Tree::edge_t graftEdge = {ptrE2C1, ptrE2C2};
			Tree::edge_t collapseEdge = {ptrE2G1, ptrE2G2};

			// Get path
			std::vector<TreeNode*> path2;
			for(size_t i=0; i<nodesId.path2.size(); ++i) {
				path2.push_back(tree->getNode(nodesId.path2[i]));
			}

			// Base nodes
			TreeNode *toGraft = stablePath[stablePath.size()-2];
			TreeNode *toExplore = stablePath[stablePath.size()-1];
			if(!nodesId.path2.empty())
				tm.pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path2);
		}
		//std::cout << "Undo : second step." << std::endl;
	} else if(moveType == Move::AdaptiveNStepsNNI) {
		assert(tree->getHashKey() == getToH());

		size_t N_STEP = nEdges.front();
		assert(N_STEP*4 == involvedNodes.size());


		for(int iM=N_STEP-1; iM>=0; --iM) {

			size_t offset = iM*4;
			size_t iI=0;
			TreeNode *ptrA = tree->getNode(involvedNodes[offset+iI]); iI++;
			TreeNode *ptrC1 = tree->getNode(involvedNodes[offset+iI]); iI++;
			TreeNode *ptrC2 = tree->getNode(involvedNodes[offset+iI]); iI++;
			TreeNode *ptrB = tree->getNode(involvedNodes[offset+iI]); iI++;

			Tree::edge_t edge;
			edge.n1 = ptrC1;
			edge.n2 = ptrC2;

			tm.swapSubtrees(tree, ptrB, ptrA, edge);
		}

		assert(tree->getHashKey() == getFromH());
	} else {
		assert(false && "Unknown move type for move");
	}
}

void Move::load(const Utils::Serialize::buffer_t& buffer) {
	Utils::Serialize::load(buffer, *this);
}

template<class Archive>
void Move::save(Archive & ar, const unsigned int version) const {
	int tmpMoveType = static_cast<int>(moveType);
	ar & fromH;
	ar & toH;
	ar & tmpMoveType;
	ar & moveInfo;
	ar & nEdges;
	ar & probability;
	ar & fwProb;
	ar & bwProb;
	ar & involvedNodes;
}

template<class Archive>
void Move::load(Archive & ar, const unsigned int version) {
	int tmpMoveType;
	ar & fromH;
	ar & toH;
	ar & tmpMoveType;
	moveType = static_cast<moveType_t>(tmpMoveType);
	ar & moveInfo;
	ar & nEdges;
	ar & probability;
	ar & fwProb;
	ar & bwProb;
	ar & involvedNodes;
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
