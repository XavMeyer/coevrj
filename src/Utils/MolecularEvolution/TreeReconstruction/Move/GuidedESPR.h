//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ESPR.h
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef GUIDED_ESPR_H_
#define GUIDED_ESPR_H_

#include <stddef.h>

#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/Parsimony/FastFitchEvaluator.h"

#include "TreeProposal.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

class GuidedESPR : public TreeProposal {
public:
	enum moveInfoESPR_t {
		BOTH_CONSTRAINED=0,
		BOTH_UNCONSTRAINED=1,
		UNCONSTRAINED_TO_CONSTRAINED=2,
		CONSTRAINED_TO_UNCONSTRAINED=3
	};

	enum branchLabel_t { NODE_M1 = 0, NODE_M2 = 1, NODE_S1 = 2, NODE_S2 = 3, NODE_E1 = 4, NODE_E2 = 5 };

public:
	GuidedESPR(Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH,
			   Parsimony::FastFitchEvaluator::sharedPtr_t aFFE);
	~GuidedESPR();

	Move::sharedPtr_t proposeNewTree(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample);

	//void applyMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move);
	//void undoMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move);

private:

	typedef struct {
		size_t nStep;
		long int h;
		Tree::edge_t movingEdge;
		Tree::edge_t graftEdge;
		std::vector<TreeNode*> path;
	} minimalMoveInfo_t;

	typedef std::vector< minimalMoveInfo_t > vecMoveInfo_t;

	long int lastInitH, lastFinalH;


	Parsimony::FastFitchEvaluator::sharedPtr_t fastFitchEvaluator;

	std::string toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
			Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path);

	void enumerateMoves(size_t iStep, const long int originalH, Tree::sharedPtr_t tree,
						Tree::edge_t &movingEdge, Tree::edge_t &pathEdge,
						std::vector<TreeNode*> &path, vecMoveInfo_t &moves,
						std::vector<double> &scores, const size_t N_STEP);

	minimalMoveInfo_t createMove(size_t iStep, long int h, TreeNode* movingNodeParent, TreeNode* movingNode,
								 Tree::edge_t graftEdge, std::vector<TreeNode*> &path);

	void applyMove(size_t iStep, Tree::sharedPtr_t tree,
			TreeNode* movingNodeParent, TreeNode* movingNode, Tree::edge_t collapseEdge,
			Tree::edge_t graftEdge, std::vector<TreeNode*> &path, vecMoveInfo_t &moves,
			std::vector<double> &scores);


	double queryParsimonyScore(Tree::sharedPtr_t tree);
	std::vector<double> computeMovesProbability(double currentScore, std::vector<double> &scores);

	void selectEdgeToMove(Tree::sharedPtr_t tree, TreeNode* &movingNodeParent, TreeNode* &movingNode);
	void defineMoves(const size_t N_STEP, Tree::sharedPtr_t tree, TreeNode* movingNodeParent, TreeNode* movingNode,
					 vecMoveInfo_t &moves, std::vector<double> &scores);
	std::vector<split_info_t> defineMoveSplits(Tree::sharedPtr_t tree, TreeNode* movingNodeParent, TreeNode* movingNode,
						 	 	 	 	 	   Tree::edge_t &graftEdge, Tree::edge_t &collapseEdge, std::vector<TreeNode*> &path,
											   Sampler::Sample &sample);

	Move::sharedPtr_t proposeRandomOneEdgeMoves(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample);
	Move::sharedPtr_t proposeAllOneStepMoves(Tree::sharedPtr_t tree, Sampler::Sample &sample);
};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* ESPR_H_ */
