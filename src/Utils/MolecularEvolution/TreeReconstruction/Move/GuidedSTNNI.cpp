//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GuidedSTNNI.cpp
 *
 * @date Nov 4, 2018
 * @author meyerx
 * @brief
 */
#include "GuidedSTNNI.h"

#include <assert.h>
#include <boost/assign/list_of.hpp>

#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Parallel/RNG/RNG.h"

#include "Utils/MolecularEvolution/Parsimony/Cache/CacheLRU.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

GuidedSTNNI::GuidedSTNNI(Bipartition::BipartitionMonitor::sharedPtr_t aBM,
		ParameterBlock::BranchHelper *aBH,
		Parsimony::FastFitchEvaluator::sharedPtr_t aFFE) :
				   TreeProposal(aBM, aBH), fastFitchEvaluator(aFFE) {

	lastFinalH = lastInitH = 0;

}

GuidedSTNNI::~GuidedSTNNI() {
}

GuidedSTNNI::minimalMoveInfo_t GuidedSTNNI::createMove(size_t iStep, long int h, TreeNode* movingNodeParent, TreeNode* movingNode,
													  Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {

	minimalMoveInfo_t newMove;

	newMove.nStep = iStep;
	newMove.h = h;
	newMove.movingEdge.n1 = movingNodeParent;
	newMove.movingEdge.n2 = movingNode;
	newMove.graftEdge = graftEdge;
	newMove.path = path;

	return newMove;

}

double GuidedSTNNI::queryParsimonyScore(Tree::sharedPtr_t tree) {

	double score = -1.;

	bool found = false;
	if(Parsimony::getCache().isEnabled()) {
		Parsimony::getCache().getElement(tree->getHashKey(), score);
	}


	if(found) {
		assert(score >= 0.);
	} else {

		score = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();

		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addNewElement(tree->getHashKey(), score);
		}
	}

	return score;
}

std::vector<double> GuidedSTNNI::computeMovesProbability(double currentScore, std::vector<double> &scores) {
	const double TRUNCATION_FACTOR = 0.1;
	const double EPSILON = 1.e-3;
	//const double TRUNCATION_FACTOR = 0.02;
	//const double EPSILON = 1.e-3/(double)scores.size();
	std::vector<double> rescaledScores(scores);

	// Trying with a rescaling type 1 - max[(new parsimony / old parsimony)/C, 1-epsilon]
	double sum = 0.;
	for(size_t i=0; i<scores.size(); ++i) {
		rescaledScores[i] = (currentScore - rescaledScores[i]) / ((double)fastFitchEvaluator->getNSite());

		rescaledScores[i] = std::min(TRUNCATION_FACTOR, rescaledScores[i]);
		rescaledScores[i] = std::max(-TRUNCATION_FACTOR, rescaledScores[i]);

		rescaledScores[i] += TRUNCATION_FACTOR;
		rescaledScores[i] = EPSILON + pow(rescaledScores[i]/TRUNCATION_FACTOR, 8.0); // 4.0

		sum += rescaledScores[i];
	}

	for(size_t i=0; i<rescaledScores.size(); ++i) rescaledScores[i] /= sum;

	return rescaledScores;
}


Move::sharedPtr_t GuidedSTNNI::proposeNewTree(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	if(N_STEP > 0) {
		return proposeLimitedSTNNIMoves(N_STEP, tree, sample);
	} else {
		return proposeAllSTNNIMoves(tree, sample);
	}
}

void GuidedSTNNI::defineAllMovesSTNNI(Tree::sharedPtr_t tree, vecMoveInfo_t &moves, std::vector<double> &scores) {

	std::vector<Tree::edge_t> edges = tree->getInternalEdges();
	for(size_t iE=0; iE<edges.size(); ++iE) {
		Tree::edge_t stableEdge = edges[iE];
		if(stableEdge.n1->getId() < stableEdge.n2->getId()) std::swap(stableEdge.n1, stableEdge.n2);

		vecTN_t childrenN1 = stableEdge.n1->getChildren(stableEdge.n2); // {A,B}
		vecTN_t childrenN2 = stableEdge.n2->getChildren(stableEdge.n1); // {C,D}
		assert(childrenN1.size()==2 && childrenN2.size()==2);

		for(size_t iC=0; iC<2; ++iC) {

			bool cross = (iC == 0);

			// Define who are swapped
			TreeNode *ptrA = childrenN1[0]; // Get A
			TreeNode *ptrB = childrenN1[1]; // Get B
			TreeNode *ptrOther, *ptrOther2;
			if(cross) {
				ptrOther = childrenN2[0]; // Get C
				ptrOther2 = childrenN2[1];
			} else {
				ptrOther = childrenN2[1]; // Get D
				ptrOther2 = childrenN2[0];
			}

			// Swap the subtrees
			tm.swapSubtrees(tree, ptrB, ptrOther, stableEdge);

			// Create and memorize node
			// Compute parsimony
			double score = queryParsimonyScore(tree);

			// Creating node
			std::vector<TreeNode*> tmpPath;
			tmpPath.push_back(ptrA);
			tmpPath.push_back(ptrOther2);
			minimalMoveInfo_t newMove = createMove(1, tree->getHashKey(), ptrB, ptrOther, stableEdge, tmpPath);

			// Register the move and the parsimony score
			moves.push_back(newMove);
			scores.push_back(score);

			// Swap the subtrees back
			tm.swapSubtrees(tree, ptrOther, ptrB, stableEdge);
		}
	}
}

Move::sharedPtr_t GuidedSTNNI::proposeAllSTNNIMoves(Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	// Memorize pending change on the tree:
	std::vector<TreeNode*> originalUpdatedNodes(tree->getUpdatedNodes());

	// Get current tree hash
	long int currentH = tree->getHashKey();
	bool reuseNew = (lastFinalH == currentH);
	bool reuseOld = (lastInitH == currentH);
	lastInitH = currentH;

	// First we want to make sure that the parsimony score are starting from a safe point
	double currentScore = 0.;
	if(reuseNew) {
		currentScore = queryParsimonyScore(tree);
		tree->clearUpdatedNodes();
	} else {
		currentScore = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();
		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addElement(currentH, currentScore);
		}
	}

	// Compute all 1 step forward moves
	vecMoveInfo_t fwMoves;
	std::vector<double> fwScores;
	if(reuseOld) {
		fwMoves = cacheFwMoves;
		fwScores = cacheFwScores;
	} else if(reuseNew) {
		fwMoves = cacheBwMoves;
		fwScores = cacheBwScores;
	} else {
		defineAllMovesSTNNI(tree, fwMoves, fwScores);
	}

	// Draw based on parsimony scores
	std::vector<double> fwMovesProbability(computeMovesProbability(currentScore, fwScores));
	size_t iDraw = Parallel::mpiMgr().getPRNG()->drawFrom(fwMovesProbability);

	double fwProb = fwMovesProbability[iDraw];
	double nextScore = fwScores[iDraw];
	minimalMoveInfo_t fwMove = fwMoves[iDraw];

	Tree::edge_t fwStableEdge = fwMove.graftEdge;
	TreeNode *fwPtrB = fwMove.movingEdge.n1;
	TreeNode *fwPtrOther = fwMove.movingEdge.n2;

	// ----- re-apply move  ------- //
	//std::cout << Parallel::mpiMgr().getRank() << "--" << fwStableEdge.n1->getId() << " -- " << fwStableEdge.n2->getId() << " -- " << fwPtrB->getId() << " -- " << fwPtrOther->getId() << std::endl;
	tm.swapSubtrees(tree, fwPtrB, fwPtrOther, fwStableEdge);

	// enumerate backward
	long int newH = tree->getHashKey();
	assert(newH == fwMove.h);

	// Compute all 1 step forward moves
	vecMoveInfo_t bwMoves;
	std::vector<double> bwScores;
	defineAllMovesSTNNI(tree, bwMoves, bwScores);

	// Resetting the Parsimony evalutator DAG to the proposed tree
	// This is required for us to reuse the DAG without full init
	fastFitchEvaluator->update(tree);

	// identify reverse move
	std::vector<double> bwMovesProbability(computeMovesProbability(nextScore, bwScores));

	bool found = false;
	size_t iMove = 0;
	for(size_t i=0; i<bwMoves.size(); ++i) {
		bool sameStableEdge = (bwMoves[i].graftEdge.n1 == fwStableEdge.n1) && (bwMoves[i].graftEdge.n2 == fwStableEdge.n2);
		bool sameMove1 = (fwMove.movingEdge.n1 == bwMoves[i].movingEdge.n2)  && (fwMove.movingEdge.n2 == bwMoves[i].movingEdge.n1);
		bool sameMove2 = (fwMove.movingEdge.n1 == bwMoves[i].path[1])  && (fwMove.movingEdge.n2 == bwMoves[i].path[0]);

		if(sameStableEdge && (sameMove1 || sameMove2)) {
			found = true;
			iMove = i;

			break;
		}
	}
	assert(found);

	// get probability
	minimalMoveInfo_t bwMove = bwMoves[iMove];
	double bwProb = bwMovesProbability[iMove];

	// Define the move
	// Register movement
	Move::sharedPtr_t newMove(new Move(currentH, newH, Move::GuidedSTNNI));

	// Set probability
	newMove->setForwardProbability(fwProb);
	newMove->setBackwardProbability(bwProb);
	newMove->setProbability(bwProb/fwProb);

	// Register involved nodes (edges)
	// Keep track of nodes <n1, n2, B, C/D>
	// n1 <--> n2 selected edge
	newMove->addInvolvedNode(fwStableEdge.n1->getId());
	newMove->addInvolvedNode(fwStableEdge.n2->getId());
	// node B
	newMove->addInvolvedNode(fwPtrB->getId());
	// node C or D
	newMove->addInvolvedNode(fwPtrOther->getId());

	newMove->addInvolvedNode(fwMove.path[0]->getId());
	newMove->addInvolvedNode(fwMove.path[1]->getId());

	// Reverse move
	tm.swapSubtrees(tree, fwPtrOther, fwPtrB, fwStableEdge);
	tree->clearUpdatedNodes();

	// Trying to prepare things for mapping
	split_info_t b1 = createSplitInfo(tree, fwPtrB, fwStableEdge.n1, sample);
	split_info_t b2 = createSplitInfo(tree, fwStableEdge.n1, fwStableEdge.n2, sample);
	split_info_t b3 = createSplitInfo(tree, fwStableEdge.n2, fwPtrOther, sample);
	std::vector<split_info_t> vecBefore = boost::assign::list_of(b1)(b2)(b3);

	// Swap the subtrees
	tm.swapSubtrees(tree, fwPtrB, fwPtrOther, fwStableEdge);

	// Trying to prepare things for mapping
	split_info_t a1 = createSplitInfo(tree, fwPtrOther, fwStableEdge.n1, sample);
	split_info_t a2 = createSplitInfo(tree, fwStableEdge.n1, fwStableEdge.n2, sample);
	split_info_t a3 = createSplitInfo(tree, fwStableEdge.n2, fwPtrB, sample);
	std::vector<split_info_t> vecAfter = boost::assign::list_of(a1)(a2)(a3);

	mapBranchLengths(vecBefore, vecAfter, sample);

	// Add nodes that where pending to be updated on the tree updated to the list:
	tree->addUpdatedNodes(originalUpdatedNodes);

	// Keep track of last tree hash key to avoid useless recomputation
	lastFinalH = tree->getHashKey();
	cacheFwMoves = fwMoves;
	cacheFwScores = fwScores;
	cacheBwMoves = bwMoves;
	cacheBwScores = bwScores;

	return newMove;
}

void GuidedSTNNI::extendRadius(size_t iStep, Tree::edge_t currentEdge, std::vector<Tree::edge_t> &edges, const size_t N_STEP) {

	iStep++;
	if(iStep > N_STEP) return;

	vecTN_t children = currentEdge.n1->getNonTerminalChildren(currentEdge.n2);

	for(size_t iC=0; iC<children.size(); ++iC) {
		Tree::edge_t edge = {currentEdge.n1, children[iC]};
		edges.push_back(edge);
		extendRadius(iStep, edge, edges, N_STEP);
	}

}


std::vector<Tree::edge_t> GuidedSTNNI::selectEdgesInRadius(const size_t N_STEP, Tree::sharedPtr_t tree, TreeNode* startingNode) {

	std::vector<Tree::edge_t> edges;

	vecTN_t nodes = startingNode->getNonTerminalChildren(NULL);

	for(size_t iN=0; iN<nodes.size(); ++iN) {
		Tree::edge_t edge = {startingNode, nodes[iN]};
		edges.push_back(edge);
		extendRadius(0, edge, edges, N_STEP);
	}

	return edges;
}


void GuidedSTNNI::defineMoveInRadiusSTNNI(Tree::sharedPtr_t tree, std::vector<Tree::edge_t> &edges, vecMoveInfo_t &moves, std::vector<double> &scores) {
	for(size_t iE=0; iE<edges.size(); ++iE) {
		Tree::edge_t stableEdge = edges[iE];
		if(stableEdge.n1->getId() < stableEdge.n2->getId()) std::swap(stableEdge.n1, stableEdge.n2);

		vecTN_t childrenN1 = stableEdge.n1->getChildren(stableEdge.n2); // {A,B}
		vecTN_t childrenN2 = stableEdge.n2->getChildren(stableEdge.n1); // {C,D
		assert(childrenN1.size()==2 && childrenN2.size()==2);

		for(size_t iC=0; iC<2; ++iC) {

			bool cross = (iC == 0);

			// Define who are swapped
			TreeNode *ptrA = childrenN1[0]; // Get A
			TreeNode *ptrB = childrenN1[1]; // Get B
			TreeNode *ptrOther, *ptrOther2;
			if(cross) {
				ptrOther = childrenN2[0]; // Get C
				ptrOther2 = childrenN2[1];
			} else {
				ptrOther = childrenN2[1]; // Get D
				ptrOther2 = childrenN2[0];
			}

			// Swap the subtrees
			tm.swapSubtrees(tree, ptrB, ptrOther, stableEdge);

			// Create and memorize node
			// Compute parsimony
			double score = queryParsimonyScore(tree);

			// Creating node
			std::vector<TreeNode*> tmpPath;
			tmpPath.push_back(ptrA);
			tmpPath.push_back(ptrOther2);
			minimalMoveInfo_t newMove = createMove(1, tree->getHashKey(), ptrB, ptrOther, stableEdge, tmpPath);

			// Register the move and the parsimony score
			moves.push_back(newMove);
			scores.push_back(score);

			// Swap the subtrees back
			tm.swapSubtrees(tree, ptrOther, ptrB, stableEdge);
		}
	}
}

Move::sharedPtr_t GuidedSTNNI::proposeLimitedSTNNIMoves(const size_t N_STEP, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	// Memorize pending change on the tree:
	std::vector<TreeNode*> originalUpdatedNodes(tree->getUpdatedNodes());

	// Get current tree hash
	long int currentH = tree->getHashKey();

	// First we want to make sure that the parsimony score are starting from a safe point
	double currentScore = 0.;
	if(lastFinalH == currentH && tree->getUpdatedNodes().empty()) {
		currentScore = queryParsimonyScore(tree);
	} else {
		currentScore = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();
		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addElement(currentH, currentScore);
		}
	}

	// Compute all 1 step forward moves
	size_t iStartingNode = aRNG->genUniformInt(0, tree->getInternals().size()-1);
	TreeNode *startingNode = tree->getInternals()[iStartingNode];

	std::vector<Tree::edge_t> fwEdges = selectEdgesInRadius(N_STEP, tree, startingNode);

	vecMoveInfo_t fwMoves;
	std::vector<double> fwScores;

	defineMoveInRadiusSTNNI(tree, fwEdges, fwMoves, fwScores);

	/*for(size_t iE=0; iE<fwEdges.size(); ++iE) {
		Tree::edge_t stableEdge = fwEdges[iE];
		if(stableEdge.n1->getId() < stableEdge.n2->getId()) std::swap(stableEdge.n1, stableEdge.n2);

		vecTN_t childrenN1 = stableEdge.n1->getChildren(stableEdge.n2); // {A,B}
		vecTN_t childrenN2 = stableEdge.n2->getChildren(stableEdge.n1); // {C,D
		assert(childrenN1.size()==2 && childrenN2.size()==2);

		for(size_t iC=0; iC<2; ++iC) {

			bool cross = (iC == 0);

			// Define who are swapped
			TreeNode *ptrA = childrenN1[0]; // Get A
			TreeNode *ptrB = childrenN1[1]; // Get B
			TreeNode *ptrOther, *ptrOther2;
			if(cross) {
				ptrOther = childrenN2[0]; // Get C
				ptrOther2 = childrenN2[1];
			} else {
				ptrOther = childrenN2[1]; // Get D
				ptrOther2 = childrenN2[0];
			}

			// Swap the subtrees
			tm.swapSubtrees(tree, ptrB, ptrOther, stableEdge);

			// Create and memorize node
			// Compute parsimony
			double score = queryParsimonyScore(tree);

			// Creating node
			std::vector<TreeNode*> tmpPath;
			tmpPath.push_back(ptrA);
			tmpPath.push_back(ptrOther2);
			minimalMoveInfo_t newMove = createMove(1, tree->getHashKey(), ptrB, ptrOther, stableEdge, tmpPath);

			// Register the move and the parsimony score
			fwMoves.push_back(newMove);
			fwScores.push_back(score);

			// Swap the subtrees back
			tm.swapSubtrees(tree, ptrOther, ptrB, stableEdge);
		}
	}*/

	// Draw based on parsimony scores
	std::vector<double> fwMovesProbability(computeMovesProbability(currentScore, fwScores));
	size_t iDraw = Parallel::mpiMgr().getPRNG()->drawFrom(fwMovesProbability);

	double fwProb = fwMovesProbability[iDraw];
	double nextScore = fwScores[iDraw];
	minimalMoveInfo_t fwMove = fwMoves[iDraw];

	Tree::edge_t fwStableEdge = fwMove.graftEdge;
	TreeNode *fwPtrB = fwMove.movingEdge.n1;
	TreeNode *fwPtrOther = fwMove.movingEdge.n2;

	// ----- re-apply move  ------- //
	tm.swapSubtrees(tree, fwPtrB, fwPtrOther, fwStableEdge);

	// enumerate backward
	long int newH = tree->getHashKey();
	assert(newH == fwMove.h);

	// Compute all 1 step forward moves
	vecMoveInfo_t bwMoves;
	std::vector<double> bwScores;
	std::vector<Tree::edge_t> bwEdges = selectEdgesInRadius(N_STEP, tree, startingNode);

	defineMoveInRadiusSTNNI(tree, bwEdges, bwMoves, bwScores);

	/*for(size_t iE=0; iE<bwEdges.size(); ++iE) {
		Tree::edge_t stableEdge = bwEdges[iE];
		if(stableEdge.n1->getId() < stableEdge.n2->getId()) std::swap(stableEdge.n1, stableEdge.n2);

		vecTN_t childrenN1 = stableEdge.n1->getChildren(stableEdge.n2); // {A,B}
		vecTN_t childrenN2 = stableEdge.n2->getChildren(stableEdge.n1); // {C,D
		assert(childrenN1.size()==2 && childrenN2.size()==2);

		for(size_t iC=0; iC<2; ++iC) {

			bool cross = (iC == 0);

			// Define who are swapped
			TreeNode *ptrA = childrenN1[0]; // Get A
			TreeNode *ptrB = childrenN1[1]; // Get B
			TreeNode *ptrOther, *ptrOther2;
			if(cross) {
				ptrOther = childrenN2[0]; // Get C
				ptrOther2 = childrenN2[1];
			} else {
				ptrOther = childrenN2[1]; // Get D
				ptrOther2 = childrenN2[0];
			}

			// Swap the subtrees
			tm.swapSubtrees(tree, ptrB, ptrOther, stableEdge);

			// Create and memorize node
			// Compute parsimony
			//double score = fastFitchEvaluator->update(tree);
			//tree->clearUpdatedNodes();
			double score = queryParsimonyScore(tree);

			// Creating node
			std::vector<TreeNode*> tmpPath;
			tmpPath.push_back(ptrA);
			tmpPath.push_back(ptrOther2);
			minimalMoveInfo_t newMove = createMove(1, tree->getHashKey(), ptrB, ptrOther, stableEdge, tmpPath);

			// Register the move and the parsimony score
			bwMoves.push_back(newMove);
			bwScores.push_back(score);

			// Swap the subtrees back
			tm.swapSubtrees(tree, ptrOther, ptrB, stableEdge);
		}
	}*/

	// Resetting the Parsimony evalutator DAG to the proposed tree
	// This is required for us to reuse the DAG without full init
	fastFitchEvaluator->update(tree);

	// identify reverse move
	std::vector<double> bwMovesProbability(computeMovesProbability(nextScore, bwScores));

	bool found = false;
	size_t iMove = 0;
	for(size_t i=0; i<bwMoves.size(); ++i) {
		bool sameStableEdge = (bwMoves[i].graftEdge.n1 == fwStableEdge.n1) && (bwMoves[i].graftEdge.n2 == fwStableEdge.n2);
		bool sameMove1 = (fwMove.movingEdge.n1 == bwMoves[i].movingEdge.n2)  && (fwMove.movingEdge.n2 == bwMoves[i].movingEdge.n1);
		bool sameMove2 = (fwMove.movingEdge.n1 == bwMoves[i].path[1])  && (fwMove.movingEdge.n2 == bwMoves[i].path[0]);

		if(sameStableEdge && (sameMove1 || sameMove2)) {
			found = true;
			iMove = i;

			break;
		}
	}
	assert(found);

	// get probability
	minimalMoveInfo_t bwMove = bwMoves[iMove];
	double bwProb = bwMovesProbability[iMove];

	// Define the move
	// Register movement
	Move::sharedPtr_t newMove(new Move(currentH, newH, Move::GuidedSTNNI));

	/*Tree::edge_t bwStableEdge = bwMove.graftEdge;
	TreeNode *bwPtrB = bwMove.movingEdge.n1;
	TreeNode *bwPtrOther = bwMove.movingEdge.n2;*/

	// Set probability
	newMove->setForwardProbability(fwProb);
	newMove->setBackwardProbability(bwProb);
	newMove->setProbability(bwProb/fwProb);

	// Register involved nodes (edges)
	// Keep track of nodes <n1, n2, B, C/D>
	// n1 <--> n2 selected edge
	newMove->addInvolvedNode(fwStableEdge.n1->getId());
	newMove->addInvolvedNode(fwStableEdge.n2->getId());
	// node B
	newMove->addInvolvedNode(fwPtrB->getId());
	// node C or D
	newMove->addInvolvedNode(fwPtrOther->getId());

	newMove->addInvolvedNode(fwMove.path[0]->getId());
	newMove->addInvolvedNode(fwMove.path[1]->getId());

	// Reverse move
	tm.swapSubtrees(tree, fwPtrOther, fwPtrB, fwStableEdge);
	tree->clearUpdatedNodes();

	// Trying to prepare things for mapping
	split_info_t b1 = createSplitInfo(tree, fwPtrB, fwStableEdge.n1, sample);
	split_info_t b2 = createSplitInfo(tree, fwStableEdge.n1, fwStableEdge.n2, sample);
	split_info_t b3 = createSplitInfo(tree, fwStableEdge.n2, fwPtrOther, sample);
	std::vector<split_info_t> vecBefore = boost::assign::list_of(b1)(b2)(b3);

	// Swap the subtrees
	tm.swapSubtrees(tree, fwPtrB, fwPtrOther, fwStableEdge);

	// Trying to prepare things for mapping
	split_info_t a1 = createSplitInfo(tree, fwPtrOther, fwStableEdge.n1, sample);
	split_info_t a2 = createSplitInfo(tree, fwStableEdge.n1, fwStableEdge.n2, sample);
	split_info_t a3 = createSplitInfo(tree, fwStableEdge.n2, fwPtrB, sample);
	std::vector<split_info_t> vecAfter = boost::assign::list_of(a1)(a2)(a3);

	mapBranchLengths(vecBefore, vecAfter, sample);

	// Add nodes that where pending to be updated on the tree updated to the list:
	tree->addUpdatedNodes(originalUpdatedNodes);

	// Keep track of last tree hash key to avoid useless recomputation
	lastFinalH = tree->getHashKey();

	return newMove;
}


} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
