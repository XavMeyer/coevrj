//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeModifier.cpp
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#include "TreeModifier.h"

#include <assert.h>
#include <stddef.h>

#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "ParameterBlock/BlockModifier/TreeModifier/BranchHelper.h"
#include "Parallel/Manager/MpiManager.h"

namespace MolecularEvolution {
namespace TreeReconstruction {


TreeModifier::TreeModifier() : aRNG(Parallel::mpiMgr().getPRNG()) {
}

TreeModifier::~TreeModifier() {
}

TreeModifier::edge_direction_t TreeModifier::getEdgeDirection(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2) {
	// If n1 is the root then direction is N2 to N1
	if(n1 == tree->getRoot()){
		return FROM_N2_TO_N1;
	}
	// If the parent is the root : its false
	if(n2 == tree->getRoot()){
		return FROM_N1_TO_N2;
	}

	// Get the the edge direction
	TreeNode* n1Parent = n1->getParent();
	if(n1Parent == n2) { // The root node is toward out node
		return FROM_N1_TO_N2;
	} else { // The root node point inside the subtree
		return FROM_N2_TO_N1;
	}
}

/**
 * Return true if the root node is in the clad : parent --> {child --> clad}
 * @param tree the tree in question
 * @param inNode The first node inside of the subtree (clad)
 * @param outNode the first node out of the subtree
 * @return True is the root node is in the subtree, false else
 */
bool TreeModifier::containsRootNode(Tree::sharedPtr_t tree, TreeNode* inNode, TreeNode* outNode) {
	edge_direction_t direction = getEdgeDirection(tree, inNode, outNode);
	return direction == FROM_N2_TO_N1;
}

void TreeModifier::reverseDirection(Tree::sharedPtr_t tree, std::vector<TreeNode*> path) {
	assert(path.size() > 1);
	// Get path direction
	edge_direction_t direction = getEdgeDirection(tree, path[0], path[1]);
	edge_direction_t oppositeDirection = direction == FROM_N1_TO_N2 ? FROM_N2_TO_N1 : FROM_N1_TO_N2;

	// Remove each edge of the path
	for(size_t iP=0; iP<path.size()-1; ++iP) {
		removeEdge(tree, path[iP], path[iP+1]);
	}

	// Remove each edge of the path
	for(size_t iP=0; iP<path.size()-1; ++iP) {
		addEdge(tree, path[iP], path[iP+1], oppositeDirection);
	}
}

void TreeModifier::addEdge(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2, edge_direction_t direction) {

	// Swap the last element of neighbours to be sure that the parent finish in the good spot
	TreeNode *cNode, *pNode;
	if(direction == FROM_N1_TO_N2) { // Parent is N2
		cNode = n1;
		pNode = n2;
	} else {
		cNode = n2;
		pNode = n1;
	}

	// Add edge
	cNode->addNeighbour(pNode, true);
	pNode->addNeighbour(cNode, false);

}

TreeModifier::edge_direction_t TreeModifier::removeEdge(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2) {

	edge_direction_t dir = getEdgeDirection(tree, n1, n2);
	n1->removeNeighbour(n2);
	n2->removeNeighbour(n1);

	return dir;
}

/**
 * Swap the subtree with first node ptrAdjN1 adjacent to edge.n1 with
 * the subtree with first node ptrAdjN2 adjacent to edge.n2.
 *
 * FROM : ptrAdjN1 <--> edge.n1 <--> edge.n2 <--> ptrAdjN2
 * TO : ptrAdjN2 <--> edge.n1 <--> edge.n2 <--> ptrAdjN1
 *
 * @param tree The tree that will be modified
 * @param ptrAdjN1 The first subtree node adjacent to edge.n1
 * @param ptrAdjN2 The first subtree node adjacent to edge.n1
 * @param edge The edge interconnecting both subtrees
 */
void TreeModifier::swapSubtrees(Tree::sharedPtr_t tree, TreeNode* ptrAdjN1, TreeNode* ptrAdjN2, Tree::edge_t edge) {

	// Track bipartitions required
	//split_t split1 = tree->findBipartitions(edge.n1, ptrAdjN1);
	//split_t split2 = tree->findBipartitions(edge.n2, ptrAdjN2);

	// check if the edge should change its direction
	bool doesBcontainsRoot = containsRootNode(tree, ptrAdjN1, edge.n1);
	bool doesOthercontainsRoot = containsRootNode(tree, ptrAdjN2, edge.n2);

	// Reverse edge if needed
	if(doesBcontainsRoot || doesOthercontainsRoot) {
		std::vector<TreeNode*> path;
		path.push_back(edge.n1);
		path.push_back(edge.n2);
		reverseDirection(tree, path);
	}

	// Move subtrees
	edge_direction_t dirN1 = removeEdge(tree, ptrAdjN1, edge.n1);
	edge_direction_t dirN2 = removeEdge(tree, ptrAdjN2, edge.n2);

	addEdge(tree, ptrAdjN1, edge.n2, dirN1);
	addEdge(tree, ptrAdjN2, edge.n1, dirN2);

	//Signal changed node to tree
	std::vector<TreeNode*> updatedNodes;
	updatedNodes.push_back(edge.n1);
	updatedNodes.push_back(edge.n2);
	updatedNodes.push_back(ptrAdjN1);
	updatedNodes.push_back(ptrAdjN2);

	// update bipartitions
	//std::cout << "Swap subtrees" << std::endl;
	/*cp.startTime();
	Tree::edge_t rem1 = {edge.n1, ptrAdjN1};
	tree->removeBipartition(rem1);
	Tree::edge_t rem2 = {edge.n2, ptrAdjN2};
	tree->removeBipartition(rem2);

	Tree::edge_t add1 = {edge.n2, ptrAdjN1};
	tree->addBiparition(add1, split1);
	Tree::edge_t add2 = {edge.n1, ptrAdjN2};
	tree->addBiparition(add2, split2);

	split_t updatedSplit = tree->findBipartitions(edge);
	split2.flip(); // Getting the complement
	updatedSplit &= split2; // Erasing split2
	updatedSplit |= split1; // Adding split1
	tree->updateBipartition(edge, updatedSplit);
	cp.endTime();
	accSwap(cp.duration());
	std::cout << std::scientific << "S : " << boost::accumulators::mean(accSwap)  << std::endl;
	*/

	// update newTree internals
	tree->update(updatedNodes);
}

/**
 * Randomly label two subtree, one can be explored the other as to be grafted somewhere
 * The subtree to explore must have at least one non-terminal edge
 *
 * @param edge The edge to prune (toGraftClad <- edge -> toExploreClad)
 * @param toGraft The first node of the clad to graft
 * @param toExplore The first node of the clad to explore
 * @return True if two subtree can be found, false else.
 */
bool TreeModifier::labelSubTrees(Tree::edge_t &edge, TreeNode* &toGraft, TreeNode* &toExplore) {

	bool canExploreN1 = canBeExplored(edge.n1, edge.n2);
	bool canExploreN2 = canBeExplored(edge.n2, edge.n1);

	if(canExploreN1 && canExploreN2) {
		bool draw = (aRNG->genUniformInt(0, 1) == 0);
		if(draw) {
			toGraft = edge.n1;
			toExplore = edge.n2;
		} else {
			toGraft = edge.n2;
			toExplore = edge.n1;
		}
	} else if(canExploreN1) {
		toGraft = edge.n2;
		toExplore = edge.n1;
	} else if(canExploreN2) {
		toGraft = edge.n1;
		toExplore = edge.n2;
	} else {
		return false;
	}

	return true;
}

/**
 * Check if a subtree can be explore
 * parentNode -edge-> toExplore(clad)
 *
 * @param toExplore The first node of the subtree to explore
 * @param parent The parent node that give the direction of the edge
 * @return True if it can be explored, false else.
 */
bool TreeModifier::canBeExplored(TreeNode *toExplore, TreeNode *parent) {

	bool areBothEdgeTerminal = true;

	for(size_t iN=0; iN<toExplore->getNeighbours().size(); ++iN) {
		TreeNode *nbr = toExplore->getNeighbour(iN);
		if(nbr != parent) {
			areBothEdgeTerminal = areBothEdgeTerminal && nbr->isTerminal();
		}
	}

	return !areBothEdgeTerminal;
}

/**
 * Randomly assign the first direction to take in the exploration
 * Only internal edges can be explored for this first step.
 *
 * @param toExplore The first node of the subtree to explore
 * @param parent The parent node that give the direction of the edge
 * @param toExploreClad The first node in the clad to explore (direction is : toExplore -edge-> toExploreClad)
 * @return True if the reverse movement is constrained (non-explored node is terminal), false else
 */
bool TreeModifier::findExploreDirection(TreeNode *toExplore, TreeNode *parent, TreeNode* &toExploreClad) {
	vecTN_t set = toExplore->getNonTerminalChildren(parent);
	assert(set.size() <= 2 && set.size() > 0);

	size_t direction = 0;
	if(set.size() > 1) {
		direction = aRNG->genUniformInt(0, set.size()-1);
	}
	toExploreClad = set[direction];
	return set.size() == 1;
}

/**
 *
 * @param pe The "extending" probability of the move
 * @param toExplore The first node of the subtree to explore
 * @param parent The parent node that give the direction of the edge
 * @param graftPoint edge with direction {parent -edge-> child}
 * @param path Path along which we move the clad to graft
 * @return The number of edge between the pruning and grafting point
 */
size_t TreeModifier::findGraftPoint(const double pe, TreeNode *toExplore, TreeNode *parent,
		Tree::edge_t &graftPoint, std::vector<TreeNode*> &path) {

	size_t nEdge = 1;
	TreeNode *pNode = parent;
	TreeNode *cNode = toExplore;

	while(!cNode->isTerminal()) { // While we have not reached a terminal edge
		// Add current node to path
		path.push_back(cNode);
		// Get nbr nodes
		vecTN_t set = cNode->getChildren(pNode);
		assert(set.size() == 2);
		// Move in one direction (chose one node)
		int direction = aRNG->genUniformInt(0, 1);
		pNode = cNode;
		cNode = set[direction];
		// Check if we have to continue
		bool doContinue = (aRNG->genUniformDbl() < pe);
		if(!doContinue) {
			break; // We have found an internal edge
		}
		nEdge++;
	}

	// We have found an edge (from parent -> child)
	graftPoint.n1 = pNode;
	graftPoint.n2 = cNode;

	return nEdge;
}

/**
 * Prune the subtree to graft an graft it on the graft point.
 * @param tree The tree
 * @param toGraft First node of the subtree to graft
 * @param toExplore Parent of the first node of the subtree to graft
 * @param graftPoint Edge on which we want to graft the subtree
 * @param path Path along which we move the clad to graft
 */
bool TreeModifier::pruneAndGraft(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {

	// (1) Prune subtree and clean edge
	//		toGraft   <--edge0-->  toExplore
	//		child1    <--edge1-->  toExplore  <--edge2--> child2
	// Transformed to
	//		toGraft   <--edge-->  toExplore
	//		child1    <--edge-->  child2

	// Keep track of bipartitions required
	//split_t splitA = tree->findBipartitions(toExplore, toGraft); // Moving clad
	//split_t splitB = tree->findBipartitions(toExplore, collapseEdge.n2); // Staying clad
	//split_t splitC = tree->findBipartitions(graftEdge); // disrupted edge

	/*std::cout << "Moving clad " << toGraft->getId() << " - with parent " << toExplore->getId() << std::endl;
	std::cout << "CollapseEdge n1 " << collapseEdge.n1->getId() << " - n2 " << collapseEdge.n2->getId() << std::endl;
	std::cout << "GraftEdge n1 " << graftEdge.n1->getId() << " - n2 " << graftEdge.n2->getId() << std::endl;
	std::cout << "Along path : ";
	for(size_t i=0; i<path.size(); ++i) std::cout << path[i]->getId() << ", ";
	std::cout << std::endl;*/

	// Change path direction if needed
	edge_direction_t directionRoot = getEdgeDirection(tree, toGraft, toExplore);
	bool rootIsInGraftClad = (directionRoot == FROM_N2_TO_N1) || (toExplore == tree->getRoot());
	if(path.size() > 1 && rootIsInGraftClad) { // n2 = toExplore --> n1 = toGraft
		//std::cout << "Direction reversed" << std::endl;
		reverseDirection(tree, path);
	}

	// Prune graft clad
	// Remove their edge to "toExplore"
	edge_direction_t dirTC2 = removeEdge(tree, collapseEdge.n2, toExplore);
	removeEdge(tree, toExplore, collapseEdge.n1);

	// Collapse edge
	addEdge(tree, collapseEdge.n2, collapseEdge.n1, dirTC2);

	// (2) Graft subtree and add edges
	//		graftPoint.n1   <--edge0--> graftPoint.n2
	// Transformed to
	//		graftPoint.n1   <--edge1--> toExplore <--edge2--> graftPoint.n2

	// (a) Break edge0
	// n1 is where toGraft is coming from (n1 is last element of path)
	edge_direction_t dirGraftPoint = removeEdge(tree, graftEdge.n1, graftEdge.n2);
	// (b) Create edge2
	addEdge(tree, toExplore, graftEdge.n2, dirGraftPoint);
	// (c) Create edge1
	if(rootIsInGraftClad) {
		addEdge(tree, graftEdge.n1, toExplore, FROM_N1_TO_N2);
	} else {
		addEdge(tree, graftEdge.n1, toExplore, dirGraftPoint);
	}

	//Signal changed node to tree
	std::vector<TreeNode*> updatedNodes;
	updatedNodes.push_back(toGraft);
	updatedNodes.push_back(toExplore);
	updatedNodes.push_back(collapseEdge.n1);
	updatedNodes.push_back(collapseEdge.n2);
	updatedNodes.push_back(graftEdge.n1);
	updatedNodes.push_back(graftEdge.n2);

	// Update bipartitions
	//std::cout << "Prune and graft subtrees : path = " << path.size() << std::endl;
	// Remove old
	/*cp.startTime();
	Tree::edge_t rem1 = {toExplore, collapseEdge.n2};
	tree->removeBipartition(rem1);
	Tree::edge_t rem2 = {toExplore, collapseEdge.n1};
	tree->removeBipartition(rem2);
	tree->removeBipartition(graftEdge);

	// Add new
	Tree::edge_t add1 = {collapseEdge.n1, collapseEdge.n2};
	tree->addBiparition(add1, splitB);
	Tree::edge_t add2 = {toExplore, graftEdge.n2};
	tree->addBiparition(add2, splitC);
	Tree::edge_t add3 = {graftEdge.n1, toExplore};
	split_t splitAC = splitA;
	splitAC |= splitC;
	tree->addBiparition(add3, splitAC);

	// Update path
	//splitA.flip(); // Getting the complement of splitA
	//std::cout << "Collapse : " << collapseEdge.n1->getId() << " -- " << collapseEdge.n2->getId() << std::endl;
	//std::cout << "graft : " << graftEdge.n1->getId() << " -- " << graftEdge.n2->getId() << std::endl;
	//std::cout << "Path front and end : " << path.front()->getId() << " -- " << path.back()->getId() << std::endl;
	assert((collapseEdge.n1->getId() == path.front()->getId()) && (graftEdge.n1->getId() == path.back()->getId()));
	for(size_t iP=0; iP<path.size()-1; ++iP) {
		Tree::edge_t pathEdge = {path[iP], path[iP+1]};
		split_t pathSplit = tree->findBipartitions(pathEdge);
		pathSplit |= splitA; // Add splitA
		tree->updateBipartition(pathEdge, pathSplit);
	}
	cp.endTime();
	accPrune(cp.duration());
	std::cout << std::scientific << boost::accumulators::mean(accPrune) << std::endl;
	*/

	// update newTree internals
	tree->update(updatedNodes);

	return rootIsInGraftClad;
}



TreeModifier::path_cost_t TreeModifier::computePruneAndMoveCost(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
																Tree::edge_path_t directedPath, Bipartition::BipartitionMonitor::sharedPtr_t bipartitionMonitor) {
	// The clad crowned by toGraft node will move along the path.
	// This function compute the predicted "cost" of 1) removing, 2) moving this clad along the edges as a function of the measured bipartitions frequencies.

	path_cost_t res;

	// The moving clad is identified as B
	split_t movingSplitB = tree->findBipartitions(toGraftParent, toGraft);
	//double freqSplitB = bipartitionMonitor->getSplitFrequency(movingSplitB);

	// First we compute the effect of removing AB|Others
	assert(directedPath.nodes[0] == toGraftParent);
	split_t remSplitAB = tree->findBipartitions(directedPath.nodes[1], directedPath.nodes[0]);
	double freqSplitAB = bipartitionMonitor->getSplitFrequency(remSplitAB);
	res.remEdgeSplitFreq = freqSplitAB;

	// Then we update the edges (e_i) along the path: for all i in path -> remove split(e_i) and add split(e_i - B)
	res.addPathSplitFreq = 0.;
	res.remPathSplitFreq = 0.;
	for(size_t i=1; i<directedPath.nodes.size()-1; ++i) {
		split_t edgeSplit = tree->findBipartitions(directedPath.nodes[i], directedPath.nodes[i+1]);
		double remSplitFreq = bipartitionMonitor->getSplitFrequency(edgeSplit);
		edgeSplit |= movingSplitB;
		double addSplitFreq = bipartitionMonitor->getSplitFrequency(edgeSplit);

		res.remPathSplitFreq += remSplitFreq;
		res.addPathSplitFreq += addSplitFreq;
	}


	return res;

}


std::vector<TreeModifier::info_move_ESPR_t> TreeModifier::createGraftMoves(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toGraftParent,
																		   Tree::edge_path_t directedPath, Bipartition::BipartitionMonitor::sharedPtr_t bipartitionMonitor,
																		   path_cost_t pathCost) {
	std::vector<TreeModifier::info_move_ESPR_t> moves;

	size_t iLast = directedPath.nodes.size()-1;
	vecTN_t potentialGraft = directedPath.nodes[iLast]->getChildren(directedPath.nodes[iLast-1]);
	assert(potentialGraft.size() == 2);

	vecTN_t toCollapseNodes = toGraftParent->getChildren(toGraft);
	assert(toCollapseNodes.size() == 2);

	Tree::edge_t toCollapseEdge;
	toCollapseEdge.n1 = toCollapseNodes[0];
	toCollapseEdge.n2 = toCollapseNodes[1];
	if(toCollapseEdge.n1 != directedPath.nodes[1]) std::swap(toCollapseEdge.n1, toCollapseEdge.n2);
	assert(toCollapseEdge.n1 == directedPath.nodes[1]);

	for(size_t iG=0; iG<potentialGraft.size(); ++iG) {
		TreeModifier::info_move_ESPR_t infoMove;
		infoMove.collapseEdge = toCollapseEdge;

		Tree::edge_t toGraftEdge;
		toGraftEdge.n1 = directedPath.nodes.back();
		toGraftEdge.n2 = potentialGraft[iG];
		infoMove.graftEdge = toGraftEdge;

		infoMove.toGraft = toGraft;
		infoMove.toExplore = toGraftParent;

		infoMove.directedPath = directedPath;

		infoMove.addPathSplitFreq = pathCost.addPathSplitFreq;
		infoMove.remPathSplitFreq = pathCost.remPathSplitFreq;

		infoMove.remEdgeSplitFreq = pathCost.remEdgeSplitFreq;

		// The moving clad is identified as B
		split_t movingSplitBAndCOrD = tree->findBipartitions(toGraftParent, toGraft); // Split of B
		movingSplitBAndCOrD |= tree->findBipartitions(toGraftEdge); // Split of C or D
		double addEdgeSplitFreq = bipartitionMonitor->getSplitFrequency(movingSplitBAndCOrD);

		infoMove.addEdgeSplitFreq = addEdgeSplitFreq;

		moves.push_back(infoMove);
	}

	return moves;
}



} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
