//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveNStepsNNI.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "AdaptiveNStepsNNI.h"

#include <assert.h>

#include "Utils/Code/CustomProfiling.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Parallel/RNG/RNG.h"
#include "ParameterBlock/BlockModifier/TreeModifier/BranchHelper.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "ParameterBlock/BlockStats/Config/Container/BlockStatCfg.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Mean/BatchEvoMU.h"


#include <boost/math/distributions/gamma.hpp>
#include <boost/math/policies/policy.hpp>
#include <boost/math/special_functions/fpclassify.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {

/*const double AdaptiveNStepsNNI::EPSILON_FREQ = 0.0001;
const double AdaptiveNStepsNNI::POWER_FREQ = 1.8;
const AdaptiveNStepsNNI::bias_t AdaptiveNStepsNNI::BIAS_TYPE = BOTH_BIASED;*/

const AdaptiveNStepsNNI::repeatPenalty_t AdaptiveNStepsNNI::REPEAT_PENALTY = TIMED_PEN;
const bool AdaptiveNStepsNNI::SCALE_BRANCHES = true;
const bool AdaptiveNStepsNNI::MAPPING_BRANCHES = true;
const double AdaptiveNStepsNNI::MAX_PENALTY = 4.;

AdaptiveNStepsNNI::AdaptiveNStepsNNI(Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH) :
		TreeProposal(aBM, aBH){
	cntIt=0;
	assert(aBM);

	ParameterBlock::Config::ConfigFactory::sharedPtr_t configFactory = ParameterBlock::Config::ConfigFactory::createDefaultFactory();
	ParameterBlock::Config::BlockStatCfg::sharedPtr_t blockCfg = configFactory->createConfig(1);
	evoMu = new ParameterBlock::BatchEvoMU(blockCfg->MU, blockCfg->CONV_CHECKER);
	evoMu->setInitialMean(5.);

}

AdaptiveNStepsNNI::~AdaptiveNStepsNNI() {
}



double AdaptiveNStepsNNI::getEpsilon(double minEpsilon) const {

	// Decrease epsilon from 100 to 0.0001 over approx 50k iterations
	//double epsilon = 100.*exp(-(double)cntIt/4000.);
	//double scale = 10.*exp(-(double)cntIt/4000.);
	//scale = std::max(minEpsilon, scale);
	double scale = bipartitionMonitor->getEpsilon(minEpsilon, 250.*minEpsilon);
	evoMu->addVal(scale);
	//std::cout << "Scale = " << evoMu->getMean() << std::endl;
	//std::cout << "Evo Mu : " << std::scientific << evoMu->getMean() << std::endl; // FIXME DEBUG OUTPUT
	double epsilon = Parallel::mpiMgr().getPRNG()->genGamma(1., evoMu->getMean());

	//std::cout << std::scientific << "Scale = " << evoMu->getMean() << "\t" << epsilon << std::endl;

	return epsilon;
}


/**
 * Propose a new tree by doing an AdaptiveNStepsNNI move.
 *
 * @param h Hash key of the current tree
 * @return The move produced
 */
Move::sharedPtr_t AdaptiveNStepsNNI::proposeNewTree(size_t N_STEP, double aEpsilon, double aPower, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	Move::sharedPtr_t newMove;

	double epsilon = getEpsilon(aEpsilon) / tree->getInternalEdges().size();

	//std::cout << "Propose new tree N_STEP : " << N_STEP << std::endl;
	newMove = proposeMove(N_STEP, epsilon, aPower, tree, sample);

	cntIt++;

	if(newMove->getFromH() == newMove->getToH()) std::cout << "N_STEP = " << N_STEP << " identique." << std::endl;

	return newMove;
}


/**
 * Get a random internal edge of a tree.
 *
 * @param tree The tree
 * @return The randomly selected edge of the tree
 */
double AdaptiveNStepsNNI::getBiasedMove(bias_t biasType, double epsilonFreq, double powerFreq,
								   Tree::sharedPtr_t tree, minimalMoveInfo_t &moveInfo, std::vector<prohibitedMove_t> &prohibitedMoves) {

	double forwardProb = 1.0;

	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> eProbability = getEdgeProbabilities(epsilonFreq, powerFreq, vecBF, prohibitedMoves);

	assert(eProbability.size() == vecBF.size());

	Tree::edge_t edge;
	if(biasType == BOTH_BIASED || biasType == REM_EDGE_BIASED) {
		size_t id = aRNG->drawFrom(eProbability);
		edge = vecBF[id].edge;
		forwardProb *= eProbability[id];
		//std::cout << "Fw edge prob : " << eProbability[id] << std::endl;
		//std::cout << vecBF.size() << "\t" << eProbability.size() << "\t" << id << std::endl;
		assert(id < vecBF.size());
	} else {
		std::vector<size_t> edgeIds;
		for(size_t iE=0; iE<eProbability.size(); ++iE) {
			if(eProbability[iE] > 0.0) edgeIds.push_back(iE);
		}
		size_t id = edgeIds[aRNG->genUniformInt(0, edgeIds.size()-1)];
		assert(id < vecBF.size());

		edge = vecBF[id].edge;
		forwardProb *= 1./((double)edgeIds.size());
	}

	if(aRNG->genUniformDbl() >0.5) std::swap(edge.n1, edge.n2);



	// Get A, B, C ,D
	assert(edge.n1);
	assert(edge.n2);
	vecTN_t childrenAB(edge.n1->getChildren(edge.n2));
	if(aRNG->genUniformDbl() >0.5) std::swap(childrenAB[0], childrenAB[1]);
	vecTN_t childrenCD(edge.n2->getChildren(edge.n1));
	if(aRNG->genUniformDbl() >0.5) std::swap(childrenCD[0], childrenCD[1]);

	moveInfo.centralEdge = edge;

	Bipartition::split_t splitA = tree->findBipartitions(edge.n1, childrenAB[0]);
	//std::cout << "Split A : " << splitA << std::endl;
	Bipartition::split_t splitB = tree->findBipartitions(edge.n1, childrenAB[1]);
	//std::cout << "Split B : " << splitB << std::endl;
	Bipartition::split_t splitC = tree->findBipartitions(edge.n2, childrenCD[0]);
	//std::cout << "Split C : " << splitC << std::endl;
	Bipartition::split_t splitD = tree->findBipartitions(edge.n2, childrenCD[1]);
	//std::cout << "Split D : " << splitD << std::endl;

	// Old split
	// AB
	Bipartition::split_t newSplitAB = splitA;
	newSplitAB |= splitB;
	/*for(size_t i=0; i<newSplitAB.size(); ++i) {
		newSplitAB[i] = splitB[i] || splitA[i];
	}*/
	if(newSplitAB[0]) newSplitAB.flip();

	// Possible permutation and frequencies
	// A to C
	Bipartition::split_t newSplitAC = splitA;
	newSplitAC |= splitC;
	/*for(size_t i=0; i<newSplitAC.size(); ++i) {
		newSplitAC[i] = splitC[i] || splitA[i];
	}*/
	if(newSplitAC[0]) newSplitAC.flip();
	double newSplitFreqAC = bipartitionMonitor->getSplitFrequency(newSplitAC);

	// A to D
	Bipartition::split_t newSplitAD = splitA;
	newSplitAD |= splitD;
	/*for(size_t i=0; i<newSplitAD.size(); ++i) {
		newSplitAD[i] = splitD[i] || splitA[i];
	}*/
	if(newSplitAD[0]) newSplitAD.flip();
	double newSplitFreqAD = bipartitionMonitor->getSplitFrequency(newSplitAD);

	double randUnif = aRNG->genUniformDbl();



	double probAC = 0.5;
	double probAD = 0.5;

	if(biasType == BOTH_BIASED || biasType == ADD_EDGE_BIASED) {
		probAC = (epsilonFreq+newSplitFreqAC)/(2.*epsilonFreq+newSplitFreqAC+newSplitFreqAD);
		probAD = (epsilonFreq+newSplitFreqAD)/(2.*epsilonFreq+newSplitFreqAC+newSplitFreqAD);
	}

	if(REPEAT_PENALTY == TIMED_PEN) {
		double penFactorAC = 1.0;
		double penFactorAD = 1.0;
		for(size_t iP=0; iP<prohibitedMoves.size(); ++iP) {
			double timeSinceAddition = prohibitedMoves.size()-iP; // Goes from 1 (for vector back) to vecSize (for vector front)
			double penaltyAsFunctionOfTime = MAX_PENALTY/std::min(timeSinceAddition, MAX_PENALTY); //
			if(newSplitAC == prohibitedMoves[iP].remSplit) { // Adding again an previously removed edge for removal is bad
				penFactorAC *= penaltyAsFunctionOfTime;
			}
			if(newSplitAD == prohibitedMoves[iP].remSplit) { // Adding again an previously removed edge for removal is bad
				penFactorAD *= penaltyAsFunctionOfTime;
			}
		}

		double oldPAC = probAC;
		double oldPAD = probAD;

		probAC = (probAC/penFactorAC);
		probAD = (probAD/penFactorAD);
		double normFactor = (probAC+probAD);
		probAC /= normFactor;
		probAD /= normFactor;
		/*if(penFactorAC != 1.0 || penFactorAD != 1.0 ) {
			std::cout << std::scientific << "[FW] pen : " << penFactorAC << " + " << penFactorAD << std::endl;
			std::cout << std::scientific <<  "[FW] Edge AC was recently removed : " << oldPAC << " to " << probAC << std::endl;
			std::cout << std::scientific <<  "[FW] Edge AD was recently removed : " << oldPAD << " to " << probAD << std::endl;
		}*/

	}

	//double freq = 0.;
	prohibitedMove_t prohibMove;
	prohibMove.remSplit = newSplitAB;
	assert(fabs(1.-(probAC+probAD)) < 1.e-7);
	if(biasType == BOTH_BIASED || biasType == ADD_EDGE_BIASED) {
		if(randUnif < probAC) {
			moveInfo.exchangeA = childrenAB[0];
			moveInfo.stayA = childrenAB[1];
			moveInfo.exchangeB = childrenCD[1];
			moveInfo.stayB = childrenCD[0];
			prohibMove.addSplit = newSplitAC;
			forwardProb *= probAC;
			//std::cout << "Fw AB prob : " << probAC << std::endl;
		} else {
			moveInfo.exchangeA = childrenAB[0];
			moveInfo.stayA = childrenAB[1];
			moveInfo.exchangeB = childrenCD[0];
			moveInfo.stayB = childrenCD[1];
			prohibMove.addSplit = newSplitAD;
			forwardProb *= probAD;
			//std::cout << "Fw AB prob : " << probAD << std::endl;
		}
	}


	//std::cout << "FW Prohib : " << prohibMove.remSplit << " \t " << prohibMove.addSplit << std::endl;
	prohibitedMoves.push_back(prohibMove);

	return forwardProb;
}

double AdaptiveNStepsNNI::getBiasedMoveReverseProbability(bias_t biasType, double epsilonFreq, double powerFreq,
													 Tree::sharedPtr_t tree, AdaptiveNStepsNNI::minimalMoveInfo_t &moveInfo,
													 std::vector<prohibitedMove_t> &prohibitedMoves) {

	double backwardProb = 1.0;

	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);
	std::vector<double> eProbability = getEdgeProbabilities(epsilonFreq, powerFreq, vecBF, prohibitedMoves);

	int iEdge = -1;
	for(size_t iE=0; iE<vecBF.size(); ++iE) {
		if((vecBF[iE].edge.n1 == moveInfo.centralEdge.n1 && vecBF[iE].edge.n2 == moveInfo.centralEdge.n2) ||
		   (vecBF[iE].edge.n1 == moveInfo.centralEdge.n2 && vecBF[iE].edge.n2 == moveInfo.centralEdge.n1)) {
				 iEdge = iE;
				 break;
			 }
	}
	assert((iEdge >= 0) && (iEdge < eProbability.size()));

	if(biasType == BOTH_BIASED || biasType == REM_EDGE_BIASED) {
		backwardProb *= eProbability[iEdge];
		//std::cout << "Edge prob : " << eProbability[iEdge] << std::endl;
	} else {
		std::vector<size_t> edgeIds;
		for(size_t iE=0; iE<eProbability.size(); ++iE) {
			if(eProbability[iE] > 0.0) edgeIds.push_back(iE);
		}
		backwardProb *= 1./((double)edgeIds.size());
	}

	// Displaced clad and new neighbour (C or D)
	Bipartition::split_t splitA = tree->findBipartitions(moveInfo.centralEdge.n2, moveInfo.exchangeA);
	//std::cout << "Split A : " << splitA << std::endl;
	Bipartition::split_t splitNN = tree->findBipartitions(moveInfo.centralEdge.n2, moveInfo.stayB);
	//std::cout << "Split X : " << splitNN << std::endl;

	Bipartition::split_t splitB = tree->findBipartitions(moveInfo.centralEdge.n1, moveInfo.stayA);
	//std::cout << "Split B : " << splitB << std::endl;
	Bipartition::split_t splitON = tree->findBipartitions(moveInfo.centralEdge.n1, moveInfo.exchangeB);
	//std::cout << "Split Y : " << splitON << std::endl;

	/*std::cout << "Node A : " << toGraft->getId() << std::endl;
	std::cout << "Node B : " << nodeB->getId() << std::endl;
	std::cout << "Node C/D : " << nodeNN->getId() << std::endl;
	std::cout << "Node C/D : " << nodeON->getId() << std::endl;
	std::cout << "Node Graft : " << toGraft->getId() << std::endl;
	std::cout << "Node Collapse : " << toExplore->getId() << std::endl;
	std::cout << "Node Explore : " << graftEdge.n1->getId() << std::endl;
	std::cout << "Node Extend : " << graftEdge.n2->getId() << std::endl;*/

	// Current split
	Bipartition::split_t curSplitAX = splitA;
	curSplitAX |= splitNN;
	if(curSplitAX[0]) curSplitAX.flip();

	// Old split
	// AB
	Bipartition::split_t oldSplitAB = splitA;
	oldSplitAB |= splitB;
	/*for(size_t i=0; i<oldSplitAB.size(); ++i) {
		oldSplitAB[i] = splitB[i] || splitA[i];
	}*/
	if(oldSplitAB[0]) oldSplitAB.flip();
	double oldSplitFreqAB = bipartitionMonitor->getSplitFrequency(oldSplitAB);
	//std::cout << "AB edge prob : " << oldSplitFreqAB << std::endl;
	//std::cout << "AB freq : " << oldSplitFreqAB << std::endl << " --- " << std::endl;

	// A to other (C or D)
	Bipartition::split_t otherSplit = splitA;
	otherSplit |= splitON;
	/*for(size_t i=0; i<otherSplit.size(); ++i) {
		otherSplit[i] = splitA[i] || splitON[i];
	}*/
	if(otherSplit[0]) otherSplit.flip();
	double otherSplitFreq = bipartitionMonitor->getSplitFrequency(otherSplit);
	//std::cout << "Other edge prob : " << oldSplitFreqAB << std::endl;
	//std::cout << bipartitionMonitor->getSplitAsString(otherSplit) << std::endl;

	/*for(size_t i=0; i<otherSplit.size(); ++i) {
		if(otherSplit[i]) std::cout << "1";
		else std::cout << "0";
	}
	std::cout << std::endl;*/

	double probAB = 0.5;
	double probOther = 0.5;

	if(biasType == BOTH_BIASED || biasType == ADD_EDGE_BIASED) {
		probAB = (epsilonFreq+oldSplitFreqAB)/(2*epsilonFreq+oldSplitFreqAB+otherSplitFreq);
		probOther = (epsilonFreq+otherSplitFreq)/(2*epsilonFreq+oldSplitFreqAB+otherSplitFreq);
	}

	if(REPEAT_PENALTY == TIMED_PEN) {
		double penFactorAB = 1.0;
		double penFactorOther = 1.0;
		for(size_t iP=0; iP<prohibitedMoves.size(); ++iP) {
			double timeSinceAddition = prohibitedMoves.size()-iP; // Goes from 1 (for vector back) to vecSize (for vector front)
			double penaltyAsFunctionOfTime = MAX_PENALTY/std::min(timeSinceAddition, MAX_PENALTY); //
			if(oldSplitAB == prohibitedMoves[iP].remSplit) { // Adding again an previously removed edge for removal is bad
				penFactorAB *= penaltyAsFunctionOfTime;
			}
			if(otherSplit == prohibitedMoves[iP].remSplit) { // Adding again an previously removed edge for removal is bad
				penFactorOther *= penaltyAsFunctionOfTime;
			}
		}

		double oldPAB = probAB;
		double oldPOther = probOther;

		probAB = (probAB/penFactorAB);
		probOther = (probOther/penFactorOther);
		double normFactor = (probAB+probOther);
		probAB /= normFactor;
		probOther /= normFactor;

		/*if(penFactorAB != 1.0 || penFactorOther != 1.0 ) {
			std::cout << std::scientific << "[BW] pen : " << penFactorAB << " + " << penFactorOther << std::endl;
			std::cout << std::scientific <<  "[BW] Edge AC was recently removed : " << oldPAB << " to " << probAB << std::endl;
			std::cout << std::scientific <<  "[BW] Edge AD was recently removed : " << oldPOther << " to " << probOther << std::endl;
		}*/
	}
	assert(fabs(1.-(probAB+probOther)) < 1.e-7);


	prohibitedMove_t prohibMove;
	prohibMove.remSplit = curSplitAX;
	prohibMove.addSplit = oldSplitAB;

	backwardProb *= probAB;

#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.revAdded.push_back(oldSplitFreqAB);
	bpHistory.revRemoved.push_back(otherSplitFreq);
	bpHistory.revDelta.push_back(oldSplitFreqAB-otherSplitFreq);
#endif

	//std::cout << "BW Prohib : " << prohibMove.remSplit << " \t " << prohibMove.addSplit << std::endl;
	prohibitedMoves.push_back(prohibMove);

	return backwardProb;
}

std::string AdaptiveNStepsNNI::displayBranchInfo(Tree::sharedPtr_t tree, TreeNode *n1, TreeNode *n2, Sampler::Sample &sample) {


	TreeNode *pNode, *cNode;

	if(n1->getParent() == n2) {
		pNode = n1;
		cNode = n2;
	} else if (n2->getParent() == n1) {
		pNode = n2;
		cNode = n1;
	} else {
		assert(false);
	}


	//Bipartition::split_t aSplit = bipartitionMonitor->getSplit(tree, pNode, cNode);
	Bipartition::split_t aSplit = tree->findBipartitions(pNode, cNode);
	//assert(aSplit == split2 && "split2 not ok");

	std::string aStrSplit = bipartitionMonitor->getSplitAsString(aSplit);
	double aBL = branchHelper->getBranchLength(pNode, cNode, sample);

	std::stringstream ss;
	ss << n1->getId() << "\t" << n2->getId() << "\t" << aBL << "\t" << aStrSplit;
	return ss.str();

}


std::pair<double, double> AdaptiveNStepsNNI::applyBranchModifier(split_info_t beforeBL, split_info_t afterBL, Sampler::Sample &sample) {

	//double probBL = 1.0;
	std::pair<double, double> probabilities(1.,1.);

	// Automate that for all branches
	if(beforeBL.hasStats && afterBL.hasStats) {

		double bfrShape, bfrScale;
		double afrShape, afrScale;
		beforeBL.defineGammaParameters(bfrShape, bfrScale);
		afterBL.defineGammaParameters(afrShape, afrScale);

		double newVal = Parallel::mpiMgr().getPRNG()->genGamma(afrShape, afrScale);
		//std::cout << std::scientific << newVal << "\t" << afrShape << "\t" << afrScale << "\t" << beforeBL.meanBL << "\t" << beforeBL.varBL << "\t" << beforeBL.skewnessBL << "\t" << beforeBL.kurtosisBL << std::endl;
		double oldVal = beforeBL.bl;
		//std::cout << std::scientific << oldVal << "\t" << bfrShape << "\t" << bfrScale << "\t" << afterBL.meanBL << "\t" << afterBL.varBL << "\t" << afterBL.skewnessBL << "\t" << afterBL.kurtosisBL << std::endl;

		boost::math::gamma_distribution<> bwDistr(bfrShape, bfrScale);
		boost::math::gamma_distribution<> fwDistr(afrShape, afrScale);
		//std::cout << "Done distr" << std::endl;

		double fwProb = boost::math::pdf(fwDistr, newVal);
		//std::cout << "Fw ok" << std::endl;
		double bwProb = boost::math::pdf(bwDistr, oldVal);
		//std::cout << "Bw ok" << std::endl;

		//probBL *= (bwProb/fwProb);

		branchHelper->setBranchLength(newVal, afterBL.n1, afterBL.n2, sample);

		//std::cout << "New branch length from full indep : " << newVal << " : move prob = " << probBL << std::endl;
		probabilities.first = fwProb;
		probabilities.second = bwProb;

	} else {
		// only swap
		// do nothing
		//std::cout << "No change" << std::endl;
	}

	return probabilities;
}


AdaptiveNStepsNNI::minimalMoveInfo_t AdaptiveNStepsNNI::proposeNextLimitedSTNNIMove(bias_t biasType, double epsilonFreq, double powerFreq,
		Tree::sharedPtr_t tree, std::vector<prohibitedMove_t> &prohibitedMoves) {

	minimalMoveInfo_t moveInfo;

	moveInfo.fwMoveProb = getBiasedMove(biasType, epsilonFreq, powerFreq, tree, moveInfo, prohibitedMoves);
	//std::cout << "[1] Moving node : " << toCollapse->getId() << " -- before freq : " << bipartitionMonitor->defineNodeFrequency(tree, toCollapse) << " -- tree prob = " << ccp1;

	// Swap the subtrees
	tm.swapSubtrees(tree, moveInfo.exchangeA, moveInfo.exchangeB, moveInfo.centralEdge);


	return moveInfo;
}

void AdaptiveNStepsNNI::defineReverseProbLimitedSTNNIMove(bias_t biasType, double epsilonFreq, double powerFreq,
		Tree::sharedPtr_t tree, AdaptiveNStepsNNI::minimalMoveInfo_t &moveInfo, std::vector<prohibitedMove_t> &prohibitedMoves) {

	moveInfo.bwMoveProb = getBiasedMoveReverseProbability(biasType, epsilonFreq, powerFreq, tree, moveInfo, prohibitedMoves);

	// Swap the subtrees -- cancel move
	tm.swapSubtrees(tree, moveInfo.exchangeB, moveInfo.exchangeA, moveInfo.centralEdge);
}




Move::sharedPtr_t AdaptiveNStepsNNI::proposeMove(size_t N_STEP, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	long int h = tree->getHashKey();
	moveInfo_t moveInfoESPR = DEFAULT;
	Move::sharedPtr_t newMove;

	//CustomProfiling cp;

	// Build multi-step move
	std::vector<minimalMoveInfo_t> fwMoves;
	std::vector<prohibitedMove_t> fwProhibitedMoves;

	bias_t biasType = BOTH_BIASED;
	for(size_t iM=0; iM<N_STEP; ++iM) {
		minimalMoveInfo_t moveInfo = proposeNextLimitedSTNNIMove(biasType, epsilonFreq, powerFreq, tree, fwProhibitedMoves);
		fwMoves.push_back(moveInfo);
	}

	// Compute backward probability
	std::vector<double> bwProbs;
	std::vector<prohibitedMove_t> bwProhibitedMoves;

	for(int iM=N_STEP-1; iM>=0; --iM) {
		defineReverseProbLimitedSTNNIMove(biasType, epsilonFreq, powerFreq, tree, fwMoves[iM], bwProhibitedMoves);
		/*if(iM < (int)fwProhibitedMoves.size()) {
			bwProhibitedMoves.push_back(fwProhibitedMoves[iM]);
			std::swap(bwProhibitedMoves.back().addSplit, bwProhibitedMoves.back().remSplit);
		}*/
	}


	// Re-apply move
	for(size_t iM=0; iM<N_STEP; ++iM) {

		minimalMoveInfo_t moveInfo = fwMoves[iM];

		// Trying to prepare things for mapping
		split_info_t b1 = createSplitInfo(tree, moveInfo.exchangeA, moveInfo.centralEdge.n1, sample);
		split_info_t b2 = createSplitInfo(tree, moveInfo.centralEdge.n1, moveInfo.centralEdge.n2, sample);
		split_info_t b3 = createSplitInfo(tree, moveInfo.centralEdge.n2, moveInfo.exchangeB, sample);
		std::vector<split_info_t> vecBefore = boost::assign::list_of(b1)(b2)(b3);

		// Swap the subtrees
		tm.swapSubtrees(tree, moveInfo.exchangeA, moveInfo.exchangeB, moveInfo.centralEdge);

		// Trying to prepare things for mapping
		split_info_t a1 = createSplitInfo(tree, moveInfo.exchangeB, moveInfo.centralEdge.n1, sample);
		split_info_t a2 = createSplitInfo(tree, moveInfo.centralEdge.n1, moveInfo.centralEdge.n2, sample);
		split_info_t a3 = createSplitInfo(tree, moveInfo.centralEdge.n2, moveInfo.exchangeA, sample);
		std::vector<split_info_t> vecAfter = boost::assign::list_of(a1)(a2)(a3);

		mapBranchLengths(vecBefore, vecAfter, sample);
	}



	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	newMove.reset(new Move(h, newH, Move::AdaptiveNStepsNNI));

	double finalFwProb = 1.0;
	double finalBwProb = 1.0;
	for(size_t iM=0; iM<N_STEP; ++iM) {

		minimalMoveInfo_t moveInfo = fwMoves[iM];

		// Register: nodeMoved (sideA), central edge (A<-->B), node moved (sideB)
		// As they were before the move
		newMove->addInvolvedNode(moveInfo.exchangeA->getId());
		newMove->addInvolvedNode(moveInfo.centralEdge.n1->getId());
		newMove->addInvolvedNode(moveInfo.centralEdge.n2->getId());
		newMove->addInvolvedNode(moveInfo.exchangeB->getId());

		//std::cout << moveInfo.fwMoveProb << "\t" << moveInfo.bwMoveProb << "\t";
		finalFwProb *= moveInfo.fwMoveProb;
		finalBwProb *= moveInfo.bwMoveProb;
	}

	// Add some info to move
	newMove->setNEdgeDistance(N_STEP);
	newMove->setMoveInfo(static_cast<int>(moveInfoESPR));

	/*double fwProbBL = 1.0;
	double bwProbBL = 1.0;
	if(SCALE_BRANCHES) {
		std::pair<double, double> probs = applyBranchModifier(beforeCentralBL, afterCentralBL, sample);
		fwProbBL *= probs.first;
		bwProbBL *= probs.second;
		//bwProbBL *= probs.first; // FIXME FOR TESTING WITH "WRONG" PROBABILITY
		//fwProbBL *= probs.second; // FIXME FOR TESTING WITH "WRONG" PROBABILITY
	}*/

	// DEBUG CANCELLED MOVE //
	size_t countCanceledMove = 0;
	std::vector<bool> checked(fwProhibitedMoves.size(), false);
	for(size_t i=0; i<fwProhibitedMoves.size()-1; ++i){
		if(checked[i]) continue;
		for(size_t j=i+1; j<fwProhibitedMoves.size(); ++j){
			if(!checked[j] && fwProhibitedMoves[i].addSplit == fwProhibitedMoves[j].remSplit &&
			   fwProhibitedMoves[i].remSplit == fwProhibitedMoves[j].addSplit) {
				countCanceledMove++;
				checked[i] = true;
				checked[j] = true;
				break;
			}
		}
	}

	//if(countCanceledMove > 0) std::cout << "NSTEP = " << N_STEP << " move with nCancel = " << countCanceledMove << std::endl;



	// END DEBUG CANCELLED MOVE //


	newMove->setForwardProbability(finalFwProb);
	newMove->setBackwardProbability(finalBwProb);

	newMove->setProbability(finalBwProb/finalFwProb);
	//std::cout << finalBwProb/finalFwProb << std::endl;

	return newMove;
}

std::vector<double> AdaptiveNStepsNNI::getEdgeProbabilities(double epsilonFreq, double powerFreq,
		std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<prohibitedMove_t> &prohibitedMoves) {

	std::vector<double> probabilities(vecBF.size());
	//std::cout << epsilonFreq << "\t" << powerFreq << std::endl;

	double sumFreq = 0.;
	for(size_t iF=0; iF<vecBF.size(); ++iF) {

		if(REPEAT_PENALTY == BAN_PREV_EDGE) {
			if(!prohibitedMoves.empty() && vecBF[iF].split == prohibitedMoves.back().addSplit) {
				assert(vecBF[iF].split[0] == prohibitedMoves.back().addSplit[0]);
				probabilities[iF] = 0.;
			} else {
				probabilities[iF] = epsilonFreq + pow((1.-vecBF[iF].freq), powerFreq);
			}
		} else if(REPEAT_PENALTY == TIMED_PEN) {

			double penFactor = 1.0;
			for(size_t iP=0; iP<prohibitedMoves.size(); ++iP) {
				if(vecBF[iF].split == prohibitedMoves[iP].addSplit) { // Selecting an newly added edge for removal is bad
					double timeSinceAddition = prohibitedMoves.size()-iP; // Goes from 1 (for vector back) to vecSize (for vector front)
					double penaltyAsFunctionOfTime = MAX_PENALTY/std::min(timeSinceAddition, MAX_PENALTY); //
					penFactor *= penaltyAsFunctionOfTime;
				}
			}
			probabilities[iF] = epsilonFreq + pow((1.-vecBF[iF].freq), powerFreq);
			//if(penFactor !=1.0) std::cout << std::scientific <<  "Edge was recently added : " << probabilities[iF] << " to " << probabilities[iF]/penFactor << std::endl;
			probabilities[iF] /= penFactor;


		} else {
			probabilities[iF] = epsilonFreq + pow((1.-vecBF[iF].freq), powerFreq);
		}

		sumFreq += probabilities[iF];

		//std::cout << (1.-vecBF[iF].freq) << "\t";
	}
	//std::cout << std::endl;
	//std::cout << sumFreq << std::endl;

	//std::cout << "Nprohibited = " << prohibitedMoves.size() << "\t";
	for(size_t iF=0; iF<probabilities.size(); ++iF) {
		probabilities[iF] /= sumFreq;
		//std::cout << probabilities[iF] << "\t";
	}
	//std::cout << std::endl;

	return probabilities;
}


std::string AdaptiveNStepsNNI::toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;

	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
