//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveNStepsNNI.h
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVE_N_STEPS_NNI_H_
#define ADAPTIVE_N_STEPS_NNI_H_

#include <stddef.h>

#include "ParameterBlock/BlockStats/ConvUtils/Updater/Mean/BatchEvoMU.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Bipartition/BipartitionMonitor.h"
#include "TreeProposal.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

namespace TR_BP = ::MolecularEvolution::TreeReconstruction::Bipartition;

class AdaptiveNStepsNNI : public TreeProposal {
public:
	enum moveInfo_t {
		DEFAULT=0,
	};

	enum bias_t {BOTH_BIASED=0, REM_EDGE_BIASED=1, ADD_EDGE_BIASED=2, NONE_BIASED=3};
	enum repeatPenalty_t {NONE=0, BAN_PREV_EDGE=1, TIMED_PEN=2};

public:
	AdaptiveNStepsNNI(Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH);
	~AdaptiveNStepsNNI();

	Move::sharedPtr_t proposeNewTree(size_t N_STEP, double aEpsilon, double aPower, Tree::sharedPtr_t tree, Sampler::Sample &sample);

private:


	typedef struct {
		long int h;
		TreeNode *exchangeA, *exchangeB, *stayA, *stayB;
		Tree::edge_t centralEdge;
		double fwMoveProb, bwMoveProb;
	} minimalMoveInfo_t;

	typedef struct {
		split_t addSplit, remSplit;
	} prohibitedMove_t;


	static const repeatPenalty_t REPEAT_PENALTY;
	static const bool SCALE_BRANCHES, MAPPING_BRANCHES;
	static const double MAX_PENALTY;
	size_t cntIt;

	ParameterBlock::BatchEvoMU *evoMu;


	double getBiasedMove(bias_t biasType, double epsilonFreq, double powerFreq,
						Tree::sharedPtr_t tree, minimalMoveInfo_t &moveInfo, std::vector<prohibitedMove_t> &prohibitedMoves);
	double getBiasedMoveReverseProbability(bias_t biasType, double epsilonFreq, double powerFreq,
										Tree::sharedPtr_t tree, AdaptiveNStepsNNI::minimalMoveInfo_t &moveInfo,
										 std::vector<prohibitedMove_t> &prohibitedMoves);


	std::string toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
			Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path);


	minimalMoveInfo_t proposeNextLimitedSTNNIMove(bias_t biasType, double epsilonFreq, double powerFreq,
			Tree::sharedPtr_t tree, std::vector<prohibitedMove_t> &prohibitedMoves);
	void defineReverseProbLimitedSTNNIMove(bias_t biasType, double epsilonFreq, double powerFreq,
			Tree::sharedPtr_t tree, AdaptiveNStepsNNI::minimalMoveInfo_t &moveInfo, std::vector<prohibitedMove_t> &prohibitedMoves);

	Move::sharedPtr_t proposeMove(size_t N_STEP, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample);

	std::vector<double> getEdgeProbabilities(double epsilonFreq, double powerFreq,
			std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF, std::vector<prohibitedMove_t> &prohibitedMoves);

	double getEpsilon(double minEpsilon) const;

	std::string displayBranchInfo(Tree::sharedPtr_t tree, TreeNode *n1, TreeNode *n2, Sampler::Sample &sample);
	std::pair<double, double> applyBranchModifier(split_info_t beforeBL, split_info_t afterBL, Sampler::Sample &sample);

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* ADAPTIVE_N_STEPS_NNI_H_ */
