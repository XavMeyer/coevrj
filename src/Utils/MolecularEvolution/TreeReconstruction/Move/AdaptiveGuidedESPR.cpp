//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveGuidedESPR.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "AdaptiveGuidedESPR.h"

#include <assert.h>

#include "Utils/Code/CustomProfiling.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.h"
#include "Parallel/RNG/RNG.h"
#include "ParameterBlock/BlockModifier/TreeModifier/BranchHelper.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"

#include "Utils/MolecularEvolution/Parsimony/Cache/CacheLRU.h"
#include "ParameterBlock/BlockStats/Config/Container/BlockStatCfg.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Mean/BatchEvoMU.h"


#include <boost/math/distributions/gamma.hpp>
#include <boost/math/policies/policy.hpp>
#include <boost/math/special_functions/fpclassify.hpp>



namespace MolecularEvolution {
namespace TreeReconstruction {

/*const double AdaptiveGuidedESPR::EPSILON_FREQ = 0.0001;
const double AdaptiveGuidedESPR::POWER_FREQ = 1.8;
const AdaptiveGuidedESPR::bias_t AdaptiveGuidedESPR::BIAS_TYPE = BOTH_BIASED;*/

const bool AdaptiveGuidedESPR::SCALE_BRANCHES = true;
const bool AdaptiveGuidedESPR::MAPPING_BRANCHES = true;

AdaptiveGuidedESPR::AdaptiveGuidedESPR(Bipartition::BipartitionMonitor::sharedPtr_t aBM, ParameterBlock::BranchHelper *aBH,
		   	   	   	   	   Parsimony::FastFitchEvaluator::sharedPtr_t aFFE) :
		TreeProposal(aBM, aBH), fastFitchEvaluator(aFFE) {
	cntIt=0;
	assert(aBM);



	ParameterBlock::Config::ConfigFactory::sharedPtr_t configFactory = ParameterBlock::Config::ConfigFactory::createDefaultFactory();
	ParameterBlock::Config::BlockStatCfg::sharedPtr_t blockCfg = configFactory->createConfig(1);
	evoMu = new ParameterBlock::BatchEvoMU(blockCfg->MU, blockCfg->CONV_CHECKER);
	evoMu->setInitialMean(5.);

}

AdaptiveGuidedESPR::~AdaptiveGuidedESPR() {
}



double AdaptiveGuidedESPR::getEpsilon(double minEpsilon) const {

	// Decrease epsilon from 100 to 0.0001 over approx 50k iterations
	//double epsilon = 100.*exp(-(double)cntIt/4000.);
	//double scale = 10.*exp(-(double)cntIt/4000.);
	//scale = std::max(minEpsilon, scale);
	double scale = bipartitionMonitor->getEpsilon(minEpsilon, 100.*minEpsilon);
	evoMu->addVal(scale);
	//std::cout << "Scale = " << evoMu->getMean() << std::endl;
	double epsilon = Parallel::mpiMgr().getPRNG()->genGamma(1., evoMu->getMean());

	//std::cout << std::scientific << epsilon << std::endl;

	return epsilon;
}


/**
 * Propose a new tree by doing an AdaptiveGuidedESPR move.
 *
 * @param h Hash key of the current tree
 * @return The move produced
 */
Move::sharedPtr_t AdaptiveGuidedESPR::proposeNewTree(double aEpsilon, double aPower, bool exploreOutside, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	stepNumber_t nStep = ONE_STEP;

	Move::sharedPtr_t newMove;

	double epsilon = getEpsilon(aEpsilon) / tree->getInternalEdges().size();
	//std::cout << std::scientific << epsilon << std::endl;

	if(nStep == ONE_STEP) {
		newMove = proposeOneEdgeMove(exploreOutside, epsilon, aPower, tree, sample);
	} else {
		newMove = proposeTwoEdgesMove(exploreOutside, epsilon, aPower, tree, sample);
	}

	cntIt++;

	return newMove;
}

void AdaptiveGuidedESPR::signalMoveHistory(bool accepted) {
#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bipartitionMonitor->registerESPR(accepted, bpHistory);
	bpHistory.added.clear();
	bpHistory.removed.clear();
	bpHistory.revAdded.clear();
	bpHistory.revRemoved.clear();
	bpHistory.revDelta.clear();
	bpHistory.stable.clear();
	bpHistory.delta.clear();
	bpHistory.moveDelta.clear();
	bpHistory.nStep.clear();
	bpHistory.probRatio.clear();
#endif
}

double AdaptiveGuidedESPR::queryParsimonyScore(Tree::sharedPtr_t tree) {

	double score = -1.;

	double found = false;
	if(Parsimony::getCache().isEnabled()) {
		found = Parsimony::getCache().getElement(tree->getHashKey(), score);
	}

	if(found) {
		assert(score >= 0.);
	} else {

		score = fastFitchEvaluator->update(tree);
		tree->clearUpdatedNodes();

		if(Parsimony::getCache().isEnabled()) {
			Parsimony::getCache().addNewElement(tree->getHashKey(), score);
		}
	}

	return score;
}

std::vector<double> AdaptiveGuidedESPR::computeMovesProbability(double currentScore, std::vector<double> &scores) {
	const double TRUNCATION_FACTOR = 0.1;
	const double EPSILON = 1.e-3;
	//const double TRUNCATION_FACTOR = 0.02;
	//const double EPSILON = 1.e-3/(double)scores.size();
	std::vector<double> rescaledScores(scores);

	// Trying with a rescaling type 1 - max[(new parsimony / old parsimony)/C, 1-epsilon]
	double sum = 0.;
	for(size_t i=0; i<scores.size(); ++i) {
		rescaledScores[i] = (currentScore - rescaledScores[i]) / ((double)fastFitchEvaluator->getNSite());

		rescaledScores[i] = std::min(TRUNCATION_FACTOR, rescaledScores[i]);
		rescaledScores[i] = std::max(-TRUNCATION_FACTOR, rescaledScores[i]);

		rescaledScores[i] += TRUNCATION_FACTOR;
		rescaledScores[i] = EPSILON + pow(rescaledScores[i]/TRUNCATION_FACTOR, 8.0); // 4.0

		sum += rescaledScores[i];
	}

	for(size_t i=0; i<rescaledScores.size(); ++i) rescaledScores[i] /= sum;

	return rescaledScores;
}

/**
 * Get a random internal edge of a tree.
 *
 * @param tree The tree
 * @return The randomly selected edge of the tree
 */
double AdaptiveGuidedESPR::getBiasedMove(bool exploreOutside, double epsilonFreq, double powerFreq,
								   Tree::sharedPtr_t tree, TreeNode* &toGraft, TreeNode* &toCollapse,
								   TreeNode* &toExplore, TreeNode* &toExtend) {

	double forwardProb = 1.0;

	//std::cout << "Before first parsimony." << std::endl;
	double currentScore = fastFitchEvaluator->update(tree);
	tree->clearUpdatedNodes();
	if(Parsimony::getCache().isEnabled()) {
		Parsimony::getCache().addElement(tree->getHashKey(), currentScore);
	}

	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);

	std::vector<double> eProbability;
	if(!exploreOutside) { // Normal adaptive edge probability
		eProbability = getEdgeProbabilities(epsilonFreq, powerFreq, vecBF);
	} else { // Invert these probability
		eProbability = getEdgeProbabilities(epsilonFreq, powerFreq, vecBF);
		double sum = 0.;
		for(size_t i=0; i<eProbability.size(); ++i) {
			eProbability[i] = 1.0/eProbability[i];
			sum += eProbability[i];
		}
		for(size_t i=0; i<eProbability.size(); ++i) {
			eProbability[i] /= sum;
		}
	}


	assert(eProbability.size() == vecBF.size());

	Tree::edge_t edge;
	size_t id = aRNG->drawFrom(eProbability);
	edge = vecBF[id].edge;
	forwardProb *= eProbability[id];

	if(edge.n1->getId() < edge.n2->getId()) std::swap(edge.n1, edge.n2);
	//std::cout << "Edge 1 : " << edge.n1->getId() << "\t" << edge.n2->getId() << std::endl;

	vecTN_t childrenN1 = edge.n1->getChildren(edge.n2); // {A,B}
	vecTN_t childrenN2 = edge.n2->getChildren(edge.n1); // {C,D
	assert(childrenN1.size()==2 && childrenN2.size()==2);

	std::vector<double> parsimonyScores;

	for(size_t iC=0; iC<2; ++iC) {

		bool cross = (iC == 0);

		// Define who are swapped
		TreeNode *ptrB = childrenN1[0]; // Get B
		TreeNode *ptrA = childrenN1[1]; // Get A
		TreeNode *ptrOther, *ptrOther2;
		if(cross) {
			ptrOther = childrenN2[0]; // Get C
			ptrOther2 = childrenN2[1];
		} else {
			ptrOther = childrenN2[1]; // Get D
			ptrOther2 = childrenN2[0];
		}

		// Swap the subtrees
		tm.swapSubtrees(tree, ptrB, ptrOther, edge);

		// Create and memorize node
		// Compute parsimony
		//std::cout << "Next parsimony." << std::endl;
		double score = queryParsimonyScore(tree);

		// Register the move and the parsimony score
		parsimonyScores.push_back(score);

		// Swap the subtrees back
		tm.swapSubtrees(tree, ptrOther, ptrB, edge);
	}

	std::vector<double> prob(computeMovesProbability(currentScore, parsimonyScores));

	double randUnif = aRNG->genUniformDbl();
	if(randUnif < prob[1]) {
		toGraft = childrenN1[0];
		toCollapse = edge.n1;
		toExplore = edge.n2;
		toExtend = childrenN2[0];
		forwardProb *= prob[1];
	} else {
		toGraft = childrenN1[0];
		toCollapse = edge.n1;
		toExplore = edge.n2;
		toExtend = childrenN2[1];
		forwardProb *= prob[0];
	}

	//std::cout << edge.n1->toString() << std::endl << edge.n2->toString() << std::endl << toGraft->getId() << std::endl;

	assert(fabs(1.-( prob[0]+prob[1]) ) < 1.e-7);


#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	double stableFreq = bipartitionMonitor->getSplitFrequency(splitA);
	double newSplitFreqAB = bipartitionMonitor->getSplitFrequency(newSplitAB);
	bpHistory.stable.push_back(stableFreq);
	bpHistory.added.push_back(freq);
	bpHistory.removed.push_back(newSplitFreqAB);
	bpHistory.delta.push_back(freq-newSplitFreqAB);
	bpHistory.moveDelta.push_back(freq-newSplitFreqAB);
	bpHistory.nStep.push_back(1);
#endif

	assert(toGraft);
	assert(toCollapse);
	assert(toExplore);
	assert(toExtend);

	assert(!toCollapse->isTerminal());
	assert(!toExplore->isTerminal());

	return forwardProb;
}

double AdaptiveGuidedESPR::getBiasedMoveReverseProbability(bool exploreOutside, double epsilonFreq, double powerFreq,
													 Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
													 Tree::edge_t collapseEdge, Tree::edge_t graftEdge) {

	double backwardProb = 1.0;

	//std::cout << "Before next parsimony." << std::endl;
	double currentScore = queryParsimonyScore(tree);/*fastFitchEvaluator->update(tree);
	tree->clearUpdatedNodes();
	Parsimony::getCache().addElement(tree->getHashKey(), currentScore);*/

	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);

	std::vector<double> eProbability;
	if(!exploreOutside) { // Normal adaptive edge probability
		eProbability = getEdgeProbabilities(epsilonFreq, powerFreq, vecBF);
	} else { // Invert these probability
		eProbability = getEdgeProbabilities(epsilonFreq, powerFreq, vecBF);
		double sum = 0.;
		for(size_t i=0; i<eProbability.size(); ++i) {
			eProbability[i] = 1.0/eProbability[i];
			sum += eProbability[i];
		}
		for(size_t i=0; i<eProbability.size(); ++i) {
			eProbability[i] /= sum;
		}
	}

	int iEdge = -1;
	for(size_t iE=0; iE<vecBF.size(); ++iE) {
		if((vecBF[iE].edge.n1 == graftEdge.n1 && vecBF[iE].edge.n2 == toExplore) ||
		   (vecBF[iE].edge.n1 == toExplore && vecBF[iE].edge.n2 == graftEdge.n1)) {
				 iEdge = iE;
				 break;
			 }
	}
	assert((iEdge >= 0) && (iEdge < eProbability.size()));

	backwardProb *= eProbability[iEdge];

	Tree::edge_t edge = vecBF[iEdge].edge;
	if(edge.n1->getId() < edge.n2->getId()) std::swap(edge.n1, edge.n2);
	//std::cout << "Edge 2 : " << edge.n1->getId() << "\t" << edge.n2->getId() << std::endl;
	//std::cout << edge.n1->toString() << std::endl << edge.n2->toString() << std::endl  << toGraft->getId() << std::endl;;


	vecTN_t childrenN1 = edge.n1->getChildren(edge.n2); // {A,B}
	vecTN_t childrenN2 = edge.n2->getChildren(edge.n1); // {C,D
	assert(childrenN1.size()==2 && childrenN2.size()==2);

	std::vector<double> parsimonyScores;

	for(size_t iC=0; iC<2; ++iC) {

		bool cross = (iC == 0);

		// Define who are swapped
		TreeNode *ptrB = toGraft; // Get B
		assert(toGraft->getId() == childrenN1[0]->getId() || toGraft->getId() == childrenN1[1]->getId());
		TreeNode *ptrOther, *ptrOther2;
		if(cross) {
			ptrOther = childrenN2[0]; // Get C
			ptrOther2 = childrenN2[1];
		} else {
			ptrOther = childrenN2[1]; // Get D
			ptrOther2 = childrenN2[0];
		}

		// Swap the subtrees
		//std::cout << "Reverse swap 1 " << std::endl;
		tm.swapSubtrees(tree, ptrB, ptrOther, edge);

		// Create and memorize node
		// Compute parsimony
		//std::cout << "Next parsimony." << std::endl;
		double score = queryParsimonyScore(tree);

		// Register the move and the parsimony score
		parsimonyScores.push_back(score);

		// Swap the subtrees back
		//std::cout << "Reverse swap 2 " << std::endl;
		tm.swapSubtrees(tree, ptrOther, ptrB, edge);
	}

	std::vector<double> prob(computeMovesProbability(currentScore, parsimonyScores));
	assert(fabs(1.-(prob[0]+prob[1])) < 1.e-7);
	//std::cout << probAB << " vs " << probOther << std::endl;
	//std::cout << "Prob to select edge collapsed : " << probAB << std::endl;

	double moveProb = 0.;
	if(childrenN2[0] != collapseEdge.n2) {
		moveProb = prob[0];
	} else {
		moveProb = prob[1];
	}


	backwardProb *= moveProb;

#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.revAdded.push_back(oldSplitFreqAB);
	bpHistory.revRemoved.push_back(otherSplitFreq);
	bpHistory.revDelta.push_back(oldSplitFreqAB-otherSplitFreq);
#endif

	return backwardProb;
}


std::vector<AdaptiveGuidedESPR::involved_tree_structures_t> AdaptiveGuidedESPR::defineContiguousEdgeMoves(
	double epsilonFreq, double powerFreq,
	Tree::sharedPtr_t tree,
	Tree::contiguous_edge_pair_t cep,
	TreeNode* nodeX, Bipartition::split_t splitX,
	vecTN_t children1, std::vector<Bipartition::split_t> split1,
	vecTN_t children2, std::vector<Bipartition::split_t> split2) {

	const double MOVE_SIZE = 2.0;

	std::vector<involved_tree_structures_t> vecMove;
	for(size_t i1=0; i1<children1.size(); ++i1) {
		for(size_t i2=0; i2<children2.size(); ++i2) {
			// Sufficient info to create the move -- path is missing but kept in directed cep
			involved_tree_structures_t move;
			move.toGraft = children1[i1];
			move.toExplore = cep.n1;
			move.collapseEdge.n1 = cep.n2;
			move.collapseEdge.n2 = children1[1-i1];
			move.graftEdge.n1 = cep.n3;
			move.graftEdge.n2 = children2[i2];
			move.directedCEP = cep;

			// Define the probability
			/*Bipartition::split_t movingSplit = split1[i1];
			Bipartition::split_t stayingSplit = split1[1-i1];
			Bipartition::split_t extendedSplit = split2[i2];
			Bipartition::split_t untouchedSplit = split2[1-i2];

			// Removed split 1
			Bipartition::split_t removedSplit_1 = movingSplit;
			removedSplit_1 |= stayingSplit;
			removedSplit_1 |= splitX;

			double freqRem1 = bipartitionMonitor->getSplitFrequency(removedSplit_1);

			// Removed split 2
			Bipartition::split_t removedSplit_2 = movingSplit;
			removedSplit_2 |= stayingSplit;

			double freqRem2 = bipartitionMonitor->getSplitFrequency(removedSplit_2);

			// Added split 1
			Bipartition::split_t addedSplit_1 = movingSplit;
			addedSplit_1 |= extendedSplit;

			double freqAdd1 = bipartitionMonitor->getSplitFrequency(addedSplit_1);

			// Added split 2
			Bipartition::split_t addedSplit_2 = stayingSplit;
			addedSplit_2 |= splitX;

			double freqAdd2 = bipartitionMonitor->getSplitFrequency(addedSplit_2);


			// Method 1 - just the gain from the move with regard to the new bipartitions
			double unormProbability1 = epsilonFreq+(freqAdd1+freqAdd2)/MOVE_SIZE; // 2 is the "size" of the move
			move.probability = unormProbability1;

			move.stableFreq = bipartitionMonitor->getSplitFrequency(movingSplit);
			move.addFreq = freqAdd1 + freqAdd2;
			move.remFreq = freqRem1 + freqRem2;*/
			move.probability = 0.0;

			Tree::edge_t collapseEdge = move.collapseEdge;
			if(collapseEdge.n1 != move.directedCEP.n2) std::swap(collapseEdge.n1, collapseEdge.n2);
			assert(collapseEdge.n1 == move.directedCEP.n2);
			assert(collapseEdge.n1 != collapseEdge.n2);
			assert(move.graftEdge.n1 == move.directedCEP.n3);

			long int initH = tree->getHashKey();

			std::vector<TreeNode*> path;
			path.push_back(move.directedCEP.n2);
			path.push_back(move.directedCEP.n3);
			tm.pruneAndGraft(tree, move.toGraft, move.toExplore, move.collapseEdge, move.graftEdge, path);

			move.parsimonyScore = queryParsimonyScore(tree);

			std::vector<TreeNode*> revPath;
			revPath.push_back(move.directedCEP.n3);
			revPath.push_back(move.directedCEP.n2);
			tm.pruneAndGraft(tree, move.toGraft, move.toExplore, move.graftEdge, move.collapseEdge, revPath);

			assert(tree->getHashKey() == initH);

			// Method 2 - favorize delta close to 0 and penalize the other ones
			//double deltaMove = ((freqAdd1+freqAdd2)-(freqRem1+freqRem2))/MOVE_SIZE;
			//double unormProbability2 = EPSILON_FREQ + pow(1.0 - fabs(deltaMove), 1.8);

			//std::cout << move.toGraft->getId() << " -- " << move.toExplore->getId()  << " -- " << move.collapseEdge.n1->getId() << " -- " << move.collapseEdge.n2->getId()  << " -- " << move.graftEdge.n1->getId()  << " -- " << move.graftEdge.n2->getId();
			//std::cout << " -- Pos Move = " << freqAdd1+freqAdd2 << " -- Neg Move = " << freqRem1+freqRem2 << " -- prob = " << unormProbability1 << std::endl;

			vecMove.push_back(move);
		}
	}

	return vecMove;
}




AdaptiveGuidedESPR::involved_tree_structures_t AdaptiveGuidedESPR::getBiasedPairEdgeMove(bool exploreOutside, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree) {

	double forwardProb = 1.0;

	double currentScore = fastFitchEvaluator->update(tree);
	tree->clearUpdatedNodes();
	if(Parsimony::getCache().isEnabled()) {
		Parsimony::getCache().addElement(tree->getHashKey(), currentScore);
	}

	// Get vector of contiguous pair of internal edges
	std::vector< Tree::contiguous_edge_pair_t > vecCEP = tree->getContiguousInternalEdges();
	//std::cout << "[FW] vecCEP size : " << vecCEP.size() << std::endl;

	// Get their probability
	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);

	std::vector<double> vecProb;
	if(!exploreOutside) { // Normal adaptive edge probability
		vecProb = getContiguousEdgePairProbabilities(epsilonFreq, powerFreq, tree, vecBF);
	} else { // Invert these probability
		vecProb = getContiguousEdgePairProbabilities(epsilonFreq, powerFreq, tree, vecBF);
		double sum = 0.;
		for(size_t i=0; i<vecProb.size(); ++i) {
			vecProb[i] = 1.0/vecProb[i];
			sum += vecProb[i];
		}
		for(size_t i=0; i<vecProb.size(); ++i) {
			vecProb[i] /= sum;
		}
	}

	Tree::contiguous_edge_pair_t cep;
	size_t id = aRNG->drawFrom(vecProb);
	cep = vecCEP[id];
	forwardProb *= vecProb[id];

	//std::cout << cep.n1->getId() << " -- " << cep.n2->getId() << " -- "<< cep.n3->getId() << " : " << std::scientific << vecProb[id] << std::endl;

	// Build list of potential moves and define their probabililty
	vecTN_t childrenAB = cep.n1->getChildren(cep.n2);
	vecTN_t childrenCD = cep.n3->getChildren(cep.n2);
	vecTN_t childrenX = cep.n2->getChildren(cep.n3);

	// Node X is the first node of the clad placed inbetween both edges
	TreeNode* nodeX = (childrenX[0] == cep.n1) ? childrenX[1] : childrenX[0];
	//Bipartition::split_t splitX = bipartitionMonitor->getSplit(tree, cep.n2, nodeX);
	Bipartition::split_t splitX = tree->findBipartitions(cep.n2, nodeX);
	//assert(splitX == splitX2 && "split X not ok");


	std::vector<Bipartition::split_t> splitAB, splitCD;
	assert(childrenAB.size() == childrenCD.size());
	for(size_t iC=0; iC<childrenAB.size(); ++iC) {
		splitAB.push_back(tree->findBipartitions(cep.n1, childrenAB[iC]));
		//splitAB.push_back(bipartitionMonitor->getSplit(tree, cep.n1, childrenAB[iC]));
		//Bipartition::split_t splitAB2 = tree->findBiparitions(cep.n1, childrenAB[iC]);
		//assert(splitAB.back() == splitAB2 && "split AB not ok");
		splitCD.push_back(tree->findBipartitions(cep.n3, childrenCD[iC]));
		//splitCD.push_back(bipartitionMonitor->getSplit(tree, cep.n3, childrenCD[iC]));
		//Bipartition::split_t splitCD2 = tree->findBiparitions(cep.n3, childrenCD[iC]);
		//assert(splitCD.back() == splitCD2 && "split CD not ok");
	}

	// From AB to CD
	std::vector<involved_tree_structures_t> vecMove1 = defineContiguousEdgeMoves(epsilonFreq, powerFreq, tree, cep, nodeX, splitX, childrenAB, splitAB, childrenCD, splitCD);
	// From CD to AB
	Tree::contiguous_edge_pair_t revCEP = cep;
	std::swap(revCEP.n1, revCEP.n3);
	std::vector<involved_tree_structures_t> vecMove2 = defineContiguousEdgeMoves(epsilonFreq, powerFreq, tree, revCEP, nodeX, splitX, childrenCD, splitCD, childrenAB, splitAB);

	//std::cerr << vecMove1.size() << " vs " << vecMove2.size() << std::endl;

	double nrmCst = 0.;
	std::vector<double> vecPScore;
	std::vector<involved_tree_structures_t> vecMove;
	for(size_t i=0; i<vecMove1.size(); ++i) {
		vecMove.push_back(vecMove1[i]);
		vecPScore.push_back(vecMove1[i].parsimonyScore);
		nrmCst += vecMove1[i].probability;
	}

	for(size_t i=0; i<vecMove2.size(); ++i) {
		vecMove.push_back(vecMove2[i]);
		vecPScore.push_back(vecMove2[i].parsimonyScore);
		nrmCst += vecMove2[i].probability;
	}

	std::vector<double> vecParsimonyProb = computeMovesProbability(currentScore, vecPScore);

	std::vector<double> vecProb2;
	for(size_t i=0; i<vecMove.size(); ++i) {
		//std::cout << vecMove[i].toGraft->getId() << " -- " << vecMove[i].toExplore->getId()  << " -- " << vecMove[i].collapseEdge.n1->getId() << " -- " << vecMove[i].collapseEdge.n2->getId()  << " -- " << vecMove[i].graftEdge.n1->getId()  << " -- " << vecMove[i].graftEdge.n2->getId();
		//std::cout << " -- prob " << vecMove[i].probability << " -- nrmCst = " << nrmCst << " -- norm prob prob = " << vecMove[i].probability/nrmCst << std::endl;
		vecProb2.push_back(vecMove[i].probability/nrmCst);
	}

	// Draw one move from the list
	involved_tree_structures_t its;
	size_t id2 = aRNG->drawFrom(vecParsimonyProb);
	its = vecMove[id2];
	forwardProb *= vecParsimonyProb[id2];//vecProb2[id2];
	its.probability = forwardProb;

#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.stable.push_back(its.stableFreq);
	bpHistory.added.push_back(its.addFreq);
	bpHistory.removed.push_back(its.remFreq);
	bpHistory.delta.push_back(its.addFreq-its.remFreq);
	bpHistory.moveDelta.push_back(its.addFreq-its.remFreq);
	bpHistory.nStep.push_back(2);
#endif

	//size_t id2 = aRNG->drawFrom(vecProb2);
	//involved_tree_structures_t its = vecMove[id2];

	//std::cout << its.toGraft->getId() << " -- " << its.toExplore->getId()  << " -- " << its.collapseEdge.n1->getId() << " -- " << its.collapseEdge.n2->getId()  << " -- " << its.graftEdge.n1->getId()  << " -- " << its.graftEdge.n2->getId() << std::endl;
	//std::cout << " Prob = " << vecProb2[id2] << std::endl;

	return its;
}

double AdaptiveGuidedESPR::getBiasedPairEdgeMoveReverseProbability(bool exploreOutside, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, involved_tree_structures_t its) {

	double currentScore = queryParsimonyScore(tree);
	/*tree->clearUpdatedNodes();
	Parsimony::getCache().addElement(tree->getHashKey(), currentScore);*/

	double backwardProb = 1.0;

	// Get vector of contiguous pair of internal edges
	std::vector< Tree::contiguous_edge_pair_t > vecCEP = tree->getContiguousInternalEdges();

	//std::cout << "[BW] vecCEP size : " << vecCEP.size() << std::endl;

	// Get their probability
	std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> vecBF;
	vecBF = bipartitionMonitor->defineBipartitionsFrequency(tree);

	std::vector<double> vecProb;
	if(!exploreOutside) { // Normal adaptive edge probability
		vecProb = getContiguousEdgePairProbabilities(epsilonFreq, powerFreq, tree, vecBF);
	} else { // Invert these probability
		vecProb = getContiguousEdgePairProbabilities(epsilonFreq, powerFreq, tree, vecBF);
		double sum = 0.;
		for(size_t i=0; i<vecProb.size(); ++i) {
			vecProb[i] = 1.0/vecProb[i];
			sum += vecProb[i];
		}
		for(size_t i=0; i<vecProb.size(); ++i) {
			vecProb[i] /= sum;
		}
	}

	// Define the reverse CEP
	Tree::contiguous_edge_pair_t revCEP = its.directedCEP;
	//std::cout << its.directedCEP.n1->getId() << " -- " << its.directedCEP.n2->getId() << " -- " << its.directedCEP.n3->getId() << std::endl;
	std::swap(revCEP.n2, revCEP.n3);
	//std::cout << revCEP.n1->getId() << " -- " << revCEP.n2->getId() << " -- " << revCEP.n3->getId() << std::endl;

	int id = -1;
	for(size_t iC=0; iC<vecCEP.size(); ++iC) {
	//std::cout << "[" << iC << "] " << vecCEP[iC].n1->getId() << " -- " << vecCEP[iC].n2->getId() << " -- " << vecCEP[iC].n3->getId() << std::endl;
		if(vecCEP[iC] == revCEP) {
			id = iC;
			break;
		}
	}
	assert(id >= 0);

	backwardProb *= vecProb[id];

	// Get probability of moves
	vecTN_t childrenAB = revCEP.n1->getChildren(revCEP.n2);
	vecTN_t childrenCD = revCEP.n3->getChildren(revCEP.n2);
	vecTN_t childrenX = revCEP.n2->getChildren(revCEP.n3);

	// Node X is the first node of the clad placed inbetween both edges
	TreeNode* nodeX = (childrenX[0] == revCEP.n1) ? childrenX[1] : childrenX[0];
	//Bipartition::split_t splitX = bipartitionMonitor->getSplit(tree, revCEP.n2, nodeX);
	Bipartition::split_t splitX = tree->findBipartitions(revCEP.n2, nodeX);
	//assert(splitX == splitX2 && "split X not ok");

	std::vector<Bipartition::split_t> splitAB, splitCD;
	assert(childrenAB.size() == childrenCD.size());
	for(size_t iC=0; iC<childrenAB.size(); ++iC) {
		splitAB.push_back(tree->findBipartitions(revCEP.n1, childrenAB[iC]));
		//splitAB.push_back(bipartitionMonitor->getSplit(tree, revCEP.n1, childrenAB[iC]));
		//Bipartition::split_t splitAB2 = tree->findBiparitions(revCEP.n1, childrenAB[iC]);
		//assert(splitAB.back() == splitAB2 && "split AB not ok");
		splitCD.push_back(tree->findBipartitions(revCEP.n3, childrenCD[iC]));
		//splitCD.push_back(bipartitionMonitor->getSplit(tree, revCEP.n3, childrenCD[iC]));
		//Bipartition::split_t splitCD2 = tree->findBiparitions(revCEP.n3, childrenCD[iC]);
		//assert(splitCD.back() == splitCD2 && "split CD not ok");
	}

	// From AB to CD
	std::vector<involved_tree_structures_t> vecMove1 = defineContiguousEdgeMoves(epsilonFreq, powerFreq, tree, revCEP, nodeX, splitX, childrenAB, splitAB, childrenCD, splitCD);
	// From CD to AB
	Tree::contiguous_edge_pair_t revCEP2 = revCEP;
	std::swap(revCEP2.n1, revCEP2.n3);
	std::vector<involved_tree_structures_t> vecMove2 = defineContiguousEdgeMoves(epsilonFreq, powerFreq, tree, revCEP2, nodeX, splitX, childrenCD, splitCD, childrenAB, splitAB);

	double nrmCst = 0.;
	std::vector<double> vecPScore;
	std::vector<involved_tree_structures_t> vecMove;
	for(size_t i=0; i<vecMove1.size(); ++i) {
		vecMove.push_back(vecMove1[i]);
		vecPScore.push_back(vecMove1[i].parsimonyScore);
		nrmCst += vecMove1[i].probability;
	}

	for(size_t i=0; i<vecMove2.size(); ++i) {
		vecMove.push_back(vecMove2[i]);
		vecPScore.push_back(vecMove2[i].parsimonyScore);
		nrmCst += vecMove2[i].probability;
	}

	std::vector<double> vecParsimonyProb = computeMovesProbability(currentScore, vecPScore);

	std::vector<double> vecProb2;
	int moveId = -1;
	for(size_t i=0; i<vecMove.size(); ++i) {
		//std::cout << vecMove[i].toGraft->getId() << " -- " << vecMove[i].toExplore->getId()  << " -- " << vecMove[i].collapseEdge.n1->getId() << " -- " << vecMove[i].collapseEdge.n2->getId()  << " -- " << vecMove[i].graftEdge.n1->getId()  << " -- " << vecMove[i].graftEdge.n2->getId();
		//std::cout << " -- prob " << vecMove[i].probability << " -- nrmCst = " << nrmCst << " -- norm prob prob = " << vecMove[i].probability/nrmCst << std::endl;
		vecProb2.push_back(vecMove[i].probability/nrmCst);
		if(vecMove[i].isReverseMove(its)) {
			moveId = i;
		}
	}
	assert(moveId >= 0);


	backwardProb *= vecParsimonyProb[moveId];//vecProb2[moveId];

	//backwardProb *= vecProb2[moveId];
#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.revAdded.push_back(vecMove[moveId].addFreq);
	bpHistory.revRemoved.push_back(vecMove[moveId].remFreq);
	bpHistory.revDelta.push_back(vecMove[moveId].addFreq-vecMove[moveId].remFreq);
#endif

	return backwardProb;
}


std::string AdaptiveGuidedESPR::displayBranchInfo(Tree::sharedPtr_t tree, TreeNode *n1, TreeNode *n2, Sampler::Sample &sample) {


	TreeNode *pNode, *cNode;

	if(n1->getParent() == n2) {
		pNode = n1;
		cNode = n2;
	} else if (n2->getParent() == n1) {
		pNode = n2;
		cNode = n1;
	} else {
		assert(false);
	}


	//Bipartition::split_t aSplit = bipartitionMonitor->getSplit(tree, pNode, cNode);
	Bipartition::split_t aSplit = tree->findBipartitions(pNode, cNode);
	//assert(aSplit == split2 && "split2 not ok");

	std::string aStrSplit = bipartitionMonitor->getSplitAsString(aSplit);
	double aBL = branchHelper->getBranchLength(pNode, cNode, sample);

	std::stringstream ss;
	ss << n1->getId() << "\t" << n2->getId() << "\t" << aBL << "\t" << aStrSplit;
	return ss.str();

}


std::pair<double, double> AdaptiveGuidedESPR::applyBranchModifier(split_info_t beforeBL, split_info_t afterBL, Sampler::Sample &sample) {

	//double probBL = 1.0;
	std::pair<double, double> probabilities(1.,1.);

	// Automate that for all branches
	if(beforeBL.hasStats && afterBL.hasStats) {

		double bfrShape, bfrScale;
		double afrShape, afrScale;
		beforeBL.defineGammaParameters(bfrShape, bfrScale);
		afterBL.defineGammaParameters(afrShape, afrScale);

		double newVal = Parallel::mpiMgr().getPRNG()->genGamma(afrShape, afrScale);
		//std::cout << std::scientific << newVal << "\t" << afrShape << "\t" << afrScale << "\t" << beforeBL.meanBL << "\t" << beforeBL.varBL << "\t" << beforeBL.skewnessBL << "\t" << beforeBL.kurtosisBL << std::endl;
		double oldVal = beforeBL.bl;
		//std::cout << std::scientific << oldVal << "\t" << bfrShape << "\t" << bfrScale << "\t" << afterBL.meanBL << "\t" << afterBL.varBL << "\t" << afterBL.skewnessBL << "\t" << afterBL.kurtosisBL << std::endl;

		boost::math::gamma_distribution<> bwDistr(bfrShape, bfrScale);
		boost::math::gamma_distribution<> fwDistr(afrShape, afrScale);
		//std::cout << "Done distr" << std::endl;

		double fwProb = boost::math::pdf(fwDistr, newVal);
		//std::cout << "Fw ok" << std::endl;
		double bwProb = boost::math::pdf(bwDistr, oldVal);
		//std::cout << "Bw ok" << std::endl;

		//probBL *= (bwProb/fwProb);

		branchHelper->setBranchLength(newVal, afterBL.n1, afterBL.n2, sample);

		//std::cout << "New branch length from full indep : " << newVal << " : move prob = " << probBL << std::endl;
		probabilities.first = fwProb;
		probabilities.second = bwProb;

	} else {
		// only swap
		// do nothing
		//std::cout << "No change" << std::endl;
	}

	return probabilities;
}




Move::sharedPtr_t AdaptiveGuidedESPR::proposeOneEdgeMove(bool exploreOutside, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	size_t nEdge  = 0;
	long int h = tree->getHashKey();
	moveInfo_t moveInfoESPR = DEFAULT;
	Move::sharedPtr_t newMove;

	// Memorize pending change on the tree:
	std::vector<TreeNode*> originalUpdatedNodes(tree->getUpdatedNodes());

	//CustomProfiling cp;

	TreeNode *toGraft, *toCollapse, *toExplore, *toExtend;
	//cp.startTime();
	std::vector<split_info_t> fwSplitInfo;
	double forwardProb = getBiasedMove(exploreOutside, epsilonFreq, powerFreq, tree, toGraft, toCollapse, toExplore, toExtend);

	//double ccp1 = bipartitionMonitor->computeTreeCCP(tree);
	//std::cout << "[1] Moving node : " << toCollapse->getId() << " -- before freq : " << bipartitionMonitor->defineNodeFrequency(tree, toCollapse) << " -- tree prob = " << ccp1;

	Tree::edge_t graftEdge = {toExplore, toExtend};
	//assert(toExplore->isParent(toExtent));
	// We keep track of old "toExplore neighbor" that will form the collapsed edge
	vecTN_t vecTN = toCollapse->getChildren(toGraft);
	assert(vecTN.size() == 2);
	// Keep track of collapsed edge --> s1 is always equal to "toExploreClad"

	Tree::edge_t collapseEdge = {vecTN[0], vecTN[1]};

	if(collapseEdge.n1 != toExplore) std::swap(collapseEdge.n1, collapseEdge.n2);
	assert(collapseEdge.n1 != collapseEdge.n2);

	split_info_t beforeCentralBL = createSplitInfo(tree, toCollapse, toExplore, sample);
	split_info_t beforeMovedBL = createSplitInfo(tree, toGraft, toCollapse, sample);
	split_info_t beforeLeftBL = createSplitInfo(tree, toCollapse, collapseEdge.n2, sample);
	split_info_t beforeRightBL = createSplitInfo(tree, toExplore, toExtend, sample);

	/* DISPLAY BEFORE STATE */
	/*std::cout << "--------------------------------------------------------" << std::endl;
	std::cout << "1-Before: Moved branch - " << displayBranchInfo(tree, toGraft, toCollapse, sample, bh) << std::endl;
	std::cout << "1-Before: Traversed branch - " << displayBranchInfo(tree, toCollapse, toExplore, sample, bh) << std::endl;
	std::cout << "1-Before: Extended branch - " << displayBranchInfo(tree, toExplore, toExtend, sample, bh) << std::endl;
	std::cout << "1-Before: Collapsed branch - " << displayBranchInfo(tree, toCollapse, collapseEdge.n2, sample, bh) << std::endl;*/
	double beforeBridgeBranchLength = branchHelper->getBranchLength(toCollapse, toExplore, sample);
	//cp.endTime();
	//double timeBiasedMove = cp.duration();
	/* DISPLAY BEFORE STATE */



	std::vector<TreeNode*> path(1, toExplore);
	// We now have to prune and graft
	//cp.startTime();
	bool reversed = tm.pruneAndGraft(tree, toGraft, toCollapse, collapseEdge, graftEdge, path);
	//cp.endTime();
	//double timeApplyMove = cp.duration();

	//cp.startTime();
	double backwardProb = getBiasedMoveReverseProbability(exploreOutside, epsilonFreq, powerFreq, tree, toGraft, toCollapse, collapseEdge, graftEdge);


	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	newMove.reset(new Move(h, newH, Move::GuidedESPR));

	// Register involved nodes (edges)
	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	// m1 <--> m2 represents the edge to move (m1=toGraft, m2=toExplore)
	newMove->addInvolvedNode(toGraft->getId());
	newMove->addInvolvedNode(toCollapse->getId());
	// s1 <--> s2 is the collapsed starting edge
	newMove->addInvolvedNode(collapseEdge.n1->getId());
	newMove->addInvolvedNode(collapseEdge.n2->getId());
	// e1 <--> e2 is the augmented ending edge
	newMove->addInvolvedNode(graftEdge.n1->getId());
	newMove->addInvolvedNode(graftEdge.n2->getId());

	// Keep track of path
	for(size_t iP=0; iP<path.size(); ++iP) {
		newMove->addInvolvedNode(path[iP]->getId());
	}

	//std::cout << std::scientific << "Forward prob = " << forwardProb << " -- Backward prob = " << backwardProb  << " -- Ratio = " << backwardProb/forwardProb << std::endl;
#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.probRatio.push_back(backwardProb/forwardProb);
#endif

	/*if(forwardProb < 0.0) {
		std::cerr << newH << std::endl;
		forwardProb *= -1.0;
	}*/

	//std::cerr << backwardProb/forwardProb << std::endl;

	// Add some info to move
	newMove->setNEdgeDistance(nEdge);
	newMove->setMoveInfo(static_cast<int>(moveInfoESPR));

	double afterBridgeBranchLength = branchHelper->getBranchLength(toExplore, toCollapse, sample);

	if(beforeBridgeBranchLength != afterBridgeBranchLength && MAPPING_BRANCHES) {
		double aBL1 = branchHelper->getBranchLength(toCollapse, toExtend, sample);
		double aBL2 = branchHelper->getBranchLength(toExplore, collapseEdge.n2, sample);

		if(aBL1 == beforeBridgeBranchLength) {
			//std::cout << "Swapping1 : " << afterBridgeBranchLength << "\t" << beforeBridgeBranchLength << std::endl;
			branchHelper->setBranchLength(afterBridgeBranchLength, toCollapse, toExtend, sample);
			branchHelper->setBranchLength(beforeBridgeBranchLength, toExplore, toCollapse, sample);
		} else if(aBL2 == beforeBridgeBranchLength) {
			//std::cout << "Swapping2 : " << afterBridgeBranchLength << "\t" << beforeBridgeBranchLength << std::endl;
			branchHelper->setBranchLength(afterBridgeBranchLength, toExplore, collapseEdge.n2, sample);
			branchHelper->setBranchLength(beforeBridgeBranchLength, toExplore, toCollapse, sample);
		} else {
			assert(false);
		}
		//std::cout << "is reversed - swapping : " << beforeBridgeBranchLength << " vs " << afterBridgeBranchLength << std::endl;
	}

	split_info_t afterCentralBL = createSplitInfo(tree, toExplore, toCollapse, sample);
	split_info_t afterMovedBL = createSplitInfo(tree, toGraft, toCollapse, sample);
	split_info_t afterLeftBL = createSplitInfo(tree, toExplore, collapseEdge.n2, sample);
	split_info_t afterRightBL = createSplitInfo(tree, toCollapse, toExtend, sample);

	double fwProbBL = 1.0;
	double bwProbBL = 1.0;
	if(SCALE_BRANCHES) {
		std::pair<double, double> probs = applyBranchModifier(beforeCentralBL, afterCentralBL, sample);
		fwProbBL *= probs.first;
		bwProbBL *= probs.second;
		//bwProbBL *= probs.first; // FIXME FOR TESTING WITH "WRONG" PROBABILITY
		//fwProbBL *= probs.second; // FIXME FOR TESTING WITH "WRONG" PROBABILITY
	}

	//cp.endTime();
	//double timeRevBiasedMove = cp.duration();

	/* DISPLAY AFTER STATE */
	/*std::cout << "********************************************************" << std::endl;
	std::cout << "1-after: Grafted branch - " << displayBranchInfo(tree, toGraft, toCollapse, sample, bh) << std::endl;
	std::cout << "1-after: New central branch - " << displayBranchInfo(tree, toExplore, toCollapse, sample, bh) << std::endl;
	std::cout << "1-after: Extended branch - " << displayBranchInfo(tree, toCollapse, toExtend, sample, bh) << std::endl;
	std::cout << "1-after: Collapsed branch - " << displayBranchInfo(tree, toExplore, collapseEdge.n2, sample, bh) << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl;*/
	/* DISPLAY AFTER STATE */

	double finalFwProb = forwardProb*fwProbBL;
	double finalBwProb = backwardProb*bwProbBL;
	newMove->setForwardProbability(finalFwProb);
	newMove->setBackwardProbability(finalBwProb);

	newMove->setProbability(finalBwProb/finalFwProb);

	// Add nodes that where pending to be updated on the tree updated to the list:
	tree->addUpdatedNodes(originalUpdatedNodes);

	//double ccp2 = bipartitionMonitor->computeTreeCCP(tree);
	//std::cout << " -- after freq : " << bipartitionMonitor->defineNodeFrequency(tree, toCollapse) << " -- tree prob = " << ccp2;
	//std::cout << " -- ratio = " << ccp2/ccp1 << " -- ratio2 = " << (ccp2/ccp1)*(finalBwProb/finalFwProb);
	//std::cout << "M1, " << ccp2/ccp1 << ", " << (finalBwProb/finalFwProb) << ", " << (ccp2/ccp1)*(finalBwProb/finalFwProb);

	//double sum = timeBiasedMove + timeApplyMove + timeRevBiasedMove;
	//std::cout << std::scientific << timeBiasedMove << "\t" << timeApplyMove << "\t" << timeRevBiasedMove << std::endl;
	//std::cout << std::fixed << 100.*timeBiasedMove/sum << "\t" << 100.*timeApplyMove/sum << "\t" << 100.*timeRevBiasedMove/sum << std::endl;

	return newMove;
}

Move::sharedPtr_t AdaptiveGuidedESPR::proposeTwoEdgesMove(bool exploreOutside, double epsilonFreq, double powerFreq, Tree::sharedPtr_t tree, Sampler::Sample &sample) {

	size_t nEdge  = 0;
	long int h = tree->getHashKey();
	moveInfo_t moveInfoESPR = DEFAULT;
	Move::sharedPtr_t newMove;

	std::vector<TreeNode*> originalUpdatedNodes(tree->getUpdatedNodes());

	//std::cout << tree->getIdString() << std::endl;
	involved_tree_structures_t its = getBiasedPairEdgeMove(exploreOutside, epsilonFreq, powerFreq, tree);
	double forwardProb = its.probability;

	//std::cout << "[2] Before frequencies : " << bipartitionMonitor->defineNodeFrequency(tree, its.toExplore);
	//std::cout << ",\t" << bipartitionMonitor->defineNodeFrequency(tree, its.directedCEP.n2);
	//std::cout << ",\t" << bipartitionMonitor->defineNodeFrequency(tree, its.directedCEP.n3);
	//double ccp1 = bipartitionMonitor->computeTreeCCP(tree);
	//std::cout << " -- tree prob = " << ccp1 << std::endl;

	/*std::cout << "Node Graft : " << its.toGraft->toString() << std::endl;
	std::cout << "Node Explore : " << its.toExplore->toString() << std::endl;
	std::cout << "Node Graft.n1 : " << its.graftEdge.n1->toString() << std::endl;
	std::cout << "Node Graft.n2 : " << its.graftEdge.n2->toString() << std::endl;
	std::cout << "Node Collapse.n1 : " << its.collapseEdge.n1->toString() << std::endl;
	std::cout << "Node Collapse.n2 : " << its.collapseEdge.n2->toString() << std::endl;
	std::cout << "Node CEP.n1 : " << its.directedCEP.n1->toString() << std::endl;
	std::cout << "Node CEP.n2 : " << its.directedCEP.n2->toString() << std::endl;
	std::cout << "Node CEP.n3 : " << its.directedCEP.n3->toString() << std::endl;*/

	/* DISPLAY BEFORE STATE */
	/*std::cout << "--------------------------------------------------------" << std::endl;
	std::cout << "2-Before: Moved branch - " << displayBranchInfo(tree, its.toGraft, its.toExplore, sample, bh) << std::endl;
	std::cout << "2-Before: Traversed branch1 - " << displayBranchInfo(tree, its.directedCEP.n1, its.directedCEP.n2, sample, bh) << std::endl;
	std::cout << "2-Before: Traversed branch2 - " << displayBranchInfo(tree, its.directedCEP.n2, its.directedCEP.n3, sample, bh) << std::endl;
	std::cout << "2-Before: Extended branch - " << displayBranchInfo(tree, its.graftEdge.n1, its.graftEdge.n2, sample, bh) << std::endl;
	std::cout << "2-Before: Collapsed branch - " << displayBranchInfo(tree, its.toExplore, its.collapseEdge.n2, sample, bh) << std::endl;*/
	/* DISPLAY BEFORE STATE */

	split_info_t beforeMoved = createSplitInfo(tree, its.toGraft, its.toExplore, sample);
	split_info_t beforeTrav1 = createSplitInfo(tree, its.directedCEP.n1, its.directedCEP.n2, sample);
	split_info_t beforeTrav2 = createSplitInfo(tree, its.directedCEP.n2, its.directedCEP.n3, sample);
	split_info_t beforeRight = createSplitInfo(tree, its.graftEdge.n1, its.graftEdge.n2, sample);
	split_info_t beforeLeft = createSplitInfo(tree, its.toExplore, its.collapseEdge.n2, sample);
	std::vector<split_info_t> vecBefore = boost::assign::list_of(beforeMoved)(beforeTrav1)(beforeTrav2)(beforeRight)(beforeLeft);

    std::string initialTree = tree->getIdString();

	std::vector<TreeNode*> path;
	path.push_back(its.directedCEP.n2);
	path.push_back(its.directedCEP.n3);


	Tree::edge_t collapseEdge = its.collapseEdge;
	if(collapseEdge.n1 != its.directedCEP.n2) std::swap(collapseEdge.n1, collapseEdge.n2);
	assert(collapseEdge.n1 == its.directedCEP.n2);
	assert(collapseEdge.n1 != collapseEdge.n2);
	bool reverse = tm.pruneAndGraft(tree, its.toGraft, its.toExplore, its.collapseEdge, its.graftEdge, path);

	double backwardProb = getBiasedPairEdgeMoveReverseProbability(exploreOutside, epsilonFreq, powerFreq, tree, its);

	if(reverse && MAPPING_BRANCHES) {
		//std::cout << "ESPR2-Reversed" << std::endl;
		double aBL1 = branchHelper->getBranchLength(its.directedCEP.n2, its.directedCEP.n3, sample);
		double aBL2 = branchHelper->getBranchLength(its.directedCEP.n3, its.toExplore, sample);

		branchHelper->setBranchLength(aBL1, its.directedCEP.n3, its.toExplore, sample);
		branchHelper->setBranchLength(aBL2, its.directedCEP.n2, its.directedCEP.n3, sample);
	}

	split_info_t afterMoved = createSplitInfo(tree, its.toGraft, its.toExplore, sample);
	split_info_t afterTrav1 = createSplitInfo(tree, its.directedCEP.n2, its.directedCEP.n3, sample);
	split_info_t afterTrav2 = createSplitInfo(tree, its.directedCEP.n3, its.toExplore, sample);
	split_info_t afterRight = createSplitInfo(tree, its.toExplore, its.graftEdge.n2, sample);
	split_info_t afterLeft = createSplitInfo(tree, its.directedCEP.n2, its.collapseEdge.n2, sample);

	if(beforeLeft.bl != afterLeft.bl && MAPPING_BRANCHES) {
		if(beforeLeft.bl == afterTrav2.bl) {
			std::swap(afterLeft.bl, afterTrav2.bl);
			branchHelper->setBranchLength(afterLeft.bl, afterLeft.n1, afterLeft.n2, sample);
			branchHelper->setBranchLength(afterTrav2.bl, afterTrav2.n1, afterTrav2.n2, sample);
			//std::cout << "Swaped" << std::endl;
		} else {
			assert(false);
		}
	}

	if(beforeRight.bl != afterRight.bl && MAPPING_BRANCHES) {
		if(beforeRight.bl == afterTrav2.bl) {
			std::swap(afterRight.bl, afterTrav2.bl);
			branchHelper->setBranchLength(afterRight.bl, afterRight.n1, afterRight.n2, sample);
			branchHelper->setBranchLength(afterTrav2.bl, afterTrav2.n1, afterTrav2.n2, sample);
			//std::cout << "Swaped2.2" << std::endl;
		} else if(beforeRight.bl == afterTrav1.bl) {
			std::swap(afterRight.bl, afterTrav1.bl);
			branchHelper->setBranchLength(afterRight.bl, afterRight.n1, afterRight.n2, sample);
			branchHelper->setBranchLength(afterTrav1.bl, afterTrav1.n1, afterTrav1.n2, sample);
			//std::cout << "Swaped2.1" << std::endl;
		} else {
			assert(false);
		}
	}

	std::vector<split_info_t> vecAfter = boost::assign::list_of(afterMoved)(afterTrav1)(afterTrav2)(afterRight)(afterLeft);

	/*for(size_t i = 0; i < vecBefore.size(); ++i) {
		for(size_t j = 0; j < vecAfter.size(); ++j) {
			if(vecBefore[i].split == vecAfter[j].split && vecBefore[i].bl != vecAfter[j].bl) {
				std::cout << "ERROR : " << i << ". " << j << std::endl;
				assert(false);
			}
		}
	}*/

	double fwProbBL = 1.0;
	double bwProbBL = 1.0;
	if(SCALE_BRANCHES) {
		std::pair<double, double> probs1 = applyBranchModifier(beforeTrav1, afterTrav1, sample);
		std::pair<double, double> probs2 = applyBranchModifier(beforeTrav2, afterTrav2, sample);

		fwProbBL *= probs1.first * probs2.first;
		bwProbBL *= probs1.second * probs2.second;
	}

	/* DISPLAY AFTER STATE */
	/*std::cout << "********************************************************" << std::endl;
	std::cout << "2-After: Grafted branch - " << displayBranchInfo(tree, its.toGraft, its.toExplore, sample, bh) << std::endl;
	std::cout << "2-After: Rev Traversed branch2 - " << displayBranchInfo(tree, its.directedCEP.n2, its.directedCEP.n3, sample, bh) << std::endl;
	std::cout << "2-After: Rev Traversed branch1 - " << displayBranchInfo(tree, its.directedCEP.n3, its.toExplore, sample, bh) << std::endl;
	std::cout << "2-After: Extended branch - " << displayBranchInfo(tree, its.toExplore, its.graftEdge.n2, sample, bh) << std::endl;
	std::cout << "2-After: Collapsed branch - " << displayBranchInfo(tree, its.directedCEP.n2, its.collapseEdge.n2, sample, bh) << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl;*/
	/* DISPLAY AFTER STATE */

	/*std::cout << "------------------------" << std::endl;
	std::cout << "Node Graft : " << its.toGraft->toString() << std::endl;
	std::cout << "Node Explore : " << its.toExplore->toString() << std::endl;
	std::cout << "Node Graft.n1 : " << its.graftEdge.n1->toString() << std::endl;
	std::cout << "Node Graft.n2 : " << its.graftEdge.n2->toString() << std::endl;
	std::cout << "Node Collapse.n1 : " << its.collapseEdge.n1->toString() << std::endl;
	std::cout << "Node Collapse.n2 : " << its.collapseEdge.n2->toString() << std::endl;
	std::cout << "Node CEP.n1 : " << its.directedCEP.n1->toString() << std::endl;
	std::cout << "Node CEP.n2 : " << its.directedCEP.n2->toString() << std::endl;
	std::cout << "Node CEP.n3 : " << its.directedCEP.n3->toString() << std::endl;
	std::cout << "************************************************" << std::endl;
	getchar();*/

	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	newMove.reset(new Move(h, newH, Move::GuidedESPR));

	// Register involved nodes (edges)
	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	// m1 <--> m2 represents the edge to move (m1=toGraft, m2=toExplore)
	newMove->addInvolvedNode(its.toGraft->getId());
	newMove->addInvolvedNode(its.toExplore->getId());
	// s1 <--> s2 is the collapsed starting edge
	newMove->addInvolvedNode(collapseEdge.n1->getId());
	newMove->addInvolvedNode(collapseEdge.n2->getId());
	// e1 <--> e2 is the augmented ending edge
	newMove->addInvolvedNode(its.graftEdge.n1->getId());
	newMove->addInvolvedNode(its.graftEdge.n2->getId());

	// Keep track of path
	for(size_t iP=0; iP<path.size(); ++iP) {
		newMove->addInvolvedNode(path[iP]->getId());
	}

	/*if(forwardProb != backwardProb) {
		std::cout << initialTree << std::endl;
		std::cout << tree->getIdString() << std::endl;
		assert(false);
	}*/


	//std::cout << "[2] After frequencies : " << bipartitionMonitor->defineNodeFrequency(tree, its.toExplore);
	//for(size_t iP=0; iP<path.size(); ++iP) {
	//	std::cout << ",\t" << bipartitionMonitor->defineNodeFrequency(tree, path[iP]);
	//}
	//double ccp2 = bipartitionMonitor->computeTreeCCP(tree);
	//std::cout << " -- tree prob = " << ccp2 << std::endl << "-- ratio = " << ccp2/ccp1;

	//std::cout << std::scientific << "Forward prob = " << forwardProb << " -- Backward prob = " << backwardProb  << " -- Ratio = " << backwardProb/forwardProb << std::endl;
#ifdef REPORT_STATS_ADAPTIVE_ESPR_MOVE
	bpHistory.probRatio.push_back(backwardProb/forwardProb);
#endif
	//newMove->setProbability(probBL*(backwardProb/forwardProb));

	double finalFwProb = forwardProb*fwProbBL;
	double finalBwProb = backwardProb*bwProbBL;
	newMove->setForwardProbability(finalFwProb);
	newMove->setBackwardProbability(finalBwProb);

	newMove->setProbability(finalBwProb/finalFwProb);

	//std::cout << "M2, " << ccp2/ccp1 << ", " << (finalBwProb/finalFwProb) << ", " << (ccp2/ccp1)*(finalBwProb/finalFwProb);

	// Add some info to move
	newMove->setNEdgeDistance(nEdge);
	newMove->setMoveInfo(static_cast<int>(moveInfoESPR));

	// Add nodes that where pending to be updated on the tree updated to the list:
	tree->addUpdatedNodes(originalUpdatedNodes);

	return newMove;
}

std::vector<double> AdaptiveGuidedESPR::getEdgeProbabilities(double epsilonFreq, double powerFreq, std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF) {

	std::vector<double> probabilities(vecBF.size(), epsilonFreq);

	double sumFreq = epsilonFreq*vecBF.size();
	for(size_t iF=0; iF<vecBF.size(); ++iF) {
		sumFreq += pow((1.-vecBF[iF].freq), powerFreq);
	}

	for(size_t iF=0; iF<vecBF.size(); ++iF) {
		probabilities[iF] += pow((1.-vecBF[iF].freq), powerFreq);
		probabilities[iF] /= sumFreq;
	}

	return probabilities;
}


std::vector<double> AdaptiveGuidedESPR::getContiguousEdgePairProbabilities(double epsilonFreq, double powerFreq, Tree::sharedPtr_t aTree,
		std::vector<TR_BP::BipartitionMonitor::bipartitionFrequency_t> &vecBF) {

	// Build a map of both with edge having the node with smaller ID first
	std::map<Tree::edge_t, double> mapEdgeSplitFreq;

	for(size_t iE=0; iE<vecBF.size(); ++iE) {
		Tree::edge_t edge = vecBF[iE].edge;
		if(edge.n2->getId() < edge.n1->getId()) std::swap(edge.n1, edge.n2);
		mapEdgeSplitFreq[edge] = vecBF[iE].freq;
	}

	// Decompose the contiguous edges in two edges and get their split frequencies
	// Build the contiguous edges unnormalised probability
	double normCst = 0.;
	std::vector<double> prob;
	std::vector< Tree::contiguous_edge_pair_t > cep = aTree->getContiguousInternalEdges();
	for(size_t iC=0; iC<cep.size(); ++iC) {
		Tree::edge_t edge1 = {cep[iC].n1, cep[iC].n2};
		if(edge1.n2->getId() < edge1.n1->getId()) std::swap(edge1.n1, edge1.n2);
		Tree::edge_t edge2 = {cep[iC].n2, cep[iC].n3};
		if(edge2.n2->getId() < edge2.n1->getId()) std::swap(edge2.n1, edge2.n2);

		double e1Freq = mapEdgeSplitFreq[edge1];
		double e2Freq = mapEdgeSplitFreq[edge2];

		double conditionalProb = epsilonFreq;
		conditionalProb += pow(1.-e1Freq, powerFreq) * pow(1.-e2Freq, powerFreq);
		prob.push_back(conditionalProb);
		normCst += conditionalProb;
	}

	//std::cout << "Contiguous edge probability : " << std::endl;
	for(size_t iC=0; iC<cep.size(); ++iC) {
		prob[iC] = prob[iC]/normCst;

		/*Tree::edge_t edge1 = {cep[iC].n1, cep[iC].n2};
		if(edge1.n2->getId() < edge1.n1->getId()) std::swap(edge1.n1, edge1.n2);
		Tree::edge_t edge2 = {cep[iC].n2, cep[iC].n3};
		if(edge2.n2->getId() < edge2.n1->getId()) std::swap(edge2.n1, edge2.n2);
		std::cout << cep[iC].n1->getId() << " ( " << mapEdgeSplitFreq[edge1] << " ) -- " << cep[iC].n2->getId() << " -- " << cep[iC].n3->getId() << " ( " << mapEdgeSplitFreq[edge2] << " )-- " << std::scientific << prob[iC] << std::endl;*/
	}

	return prob;
}




std::string AdaptiveGuidedESPR::toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;
	ss << "toGraft : " << toGraft->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "toExplore : " << toExplore->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s1 : " << collapseEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s2 : " << collapseEdge.n2->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e1 : " << graftEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e2 : " << graftEdge.n2->toString() << std::endl;
	for(size_t i=0; i<path.size(); ++i) {
		ss << "**" << std::endl;
		ss << "p" << i << " : " << path[i]->toString() << std::endl;
	}
	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
