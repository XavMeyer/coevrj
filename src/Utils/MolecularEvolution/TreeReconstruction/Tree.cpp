//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Tree.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "Tree.h"

#include <assert.h>
#include <utility>

#include "../../Code/CustomProfiling.h"
#include "Utils/Code/Algorithm.h" // IWYU pragma: keep
#include "Utils/Code/BinSerializationSupport.h" // IWYU pragma: keep
#include "Utils/MolecularEvolution/TreeReconstruction/TreeNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeParser/Parser.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"


namespace MolecularEvolution { namespace DataLoader { class MSA; } }

namespace MolecularEvolution {
namespace TreeReconstruction {

/*Tree::Tree(const Tree &other) {
	copy(other);
}*/

Tree::Tree(bool doShuffle, const DataLoader::MSA &msa, const std::map<std::string, int>& nameMapping) {
	// Create an initial tree from MSA
	TreeNode *tmpNode = createFromMSA(doShuffle, msa, nameMapping);

	if(tmpNode->getNeighbours().size() == 2) { // If tree is rooted
		// Create an unrooted tree
		// Define root and other
		size_t newRoot = tmpNode->getNeighbour(0)->isTerminal() ? 1 : 0;
		size_t other = 1-newRoot;

		// Get new root
		root = tmpNode->getNeighbour(newRoot);
		TreeNode *otherNode = tmpNode->getNeighbour(other);

		root->removeNeighbour(tmpNode); // Remove root node from treeRoot
		otherNode->removeNeighbour(tmpNode); // Remove root node from other
		root->addNeighbour(otherNode); // Create edges
		otherNode->addNeighbour(root);

		// Remove wrong root
		delete tmpNode;
	} else {
		root = tmpNode;
	}

	// define ids for all the nodes
	size_t id = 0;
	assignIdRecursively(id, root, NULL);

	defineNodesCategory();

	idString = root->buildOrderedString();
	createHashKey();

	resetBipartitions();
	//initBipartitions();
}



Tree::Tree(const DL::TreeNode &newickNode, const std::map<std::string, int> & nameMapping) {
	// Create tree
	TreeNode *tmpNode = new TreeNode(newickNode, nameMapping);
	createRecursive(newickNode, tmpNode, nameMapping);

	if(tmpNode->getNeighbours().size() == 2) { // If tree is rooted
		// Create an unrooted tree
		// Define root and other
		size_t newRoot = tmpNode->getNeighbour(0)->isTerminal() ? 1 : 0;
		size_t other = 1-newRoot;

		// Get new root
		root = tmpNode->getNeighbour(newRoot);
		TreeNode *otherNode = tmpNode->getNeighbour(other);

		root->removeNeighbour(tmpNode); // Remove root node from treeRoot
		otherNode->removeNeighbour(tmpNode); // Remove root node from other
		root->addNeighbour(otherNode); // Create edges
		otherNode->addNeighbour(root);

		// Remove wrong root
		delete tmpNode;
	} else {
		root = tmpNode;
	}

	// define ids for all the nodes
	size_t id = 0;

	nodeIdtoNewickMapping.clear();
	assignIdRecursively(id, root, NULL, nodeIdtoNewickMapping);

	defineNodesCategory();

	idString =  root->buildOrderedString();
	createHashKey();


	resetBipartitions();
	//initBipartitions();
}

Tree::Tree(const std::string &aTreeStr) {
	// Create from string
	Parser parser(aTreeStr);
	root = parser.getTreeRoot();

	defineNodesCategory();

	idString = aTreeStr;
	createHashKey();

	//initBipartitions();
	resetBipartitions();
}

Tree::Tree(const Utils::Serialize::buffer_t &buffer) {
	// Load from buffer (treeString and move type)
	load(buffer);

	// Create tree etc.
	defineNodesCategory();

	resetBipartitions();
	//initBipartitions();
}

Tree::~Tree() {
	root->deleteNeighbours();
	delete root;
}

bool Tree::isEqualTo(const Tree* other) const {
	return (getHashKey() == other->getHashKey()) &&
		   (getIdString() == other->getIdString());
}

std::string Tree::getIdString() const {
	return idString;
}

std::string Tree::getNameString(const std::vector<std::string> &names) const {
	TreeNode *child_0 = terminals.front();
	TreeNode *startingNode = child_0->getNeighbour(0);

	return startingNode->buildOrderedString(names);
}

std::string Tree::getNameAndBranchLengthString(const std::vector<std::string> &names,
											   const std::vector<double> &branchLength) const {
	//TreeNode *child_0 = terminals.front();
	//TreeNode *startingNode = child_0->getNeighbour(0);

	//return startingNode->buildOrderedString(names, branchLength);
	//std::cout << "Root : " << root->toString() << std::endl;
	return root->buildOrderedString(names, branchLength);
}


std::vector<size_t> Tree::getIdBLInTraversalOrder() {
	TreeNode *child_0 = terminals.front();
	//TreeNode *startingNode = child_0->getNeighbour(0);
	return root->defineIdBLInTraversalOrder();
}

std::string Tree::getNameAndBranchLengthStringForLogAdpt(const std::vector<std::string> &names,
						  	  	  	  	        const std::vector<double> &branchLength) {
	//TreeNode *child_0 = terminals.front();
	//TreeNode *startingNode = child_0->getNeighbour(0);
	//return startingNode->buildStringUsingBLInTraversalOrder(names, branchLength);

	return root->buildStringUsingBLInTraversalOrder(names, branchLength);
}

std::string Tree::getNameAndBranchLengthStringForLogConv(const std::vector<std::string> &names,
						  	  	  	  	        const std::vector<double> &branchLength) {
	TreeNode *child_0 = terminals.front();
	TreeNode *startingNode = child_0->getNeighbour(0);
	return startingNode->buildStringUsingBLInTraversalOrder(names, branchLength);
}



TreeNode* Tree::getRoot() {
	return root;
}

TreeNode* Tree::getNode(size_t id) {
	itNodesMap_t it = nodesMap.find(id);
	assert(it != nodesMap.end());
	return it->second;
}

std::vector<Tree::edge_t> Tree::getInternalEdges() const {
	std::vector<Tree::edge_t> intEdges;
	for(size_t iN=0; iN<internals.size(); ++iN) {
		if(root != internals[iN]) {
			edge_t edge = {internals[iN]->getParent(), internals[iN]};
			intEdges.push_back(edge);
		}
	}
	return intEdges;
}

std::vector<TreeNode*>& Tree::getInternals() {
	return internals;
}

std::vector<TreeNode*>& Tree::getTerminals() {
	return terminals;
}

const std::map<size_t, size_t>& Tree::getNodeIdtoNewickMapping() const {
	return nodeIdtoNewickMapping;
}

long int Tree::getHashKey() const {
	return myH;
}

void Tree::createHashKey() {
	boost::hash<std::string> string_hash;
	myH = string_hash(getIdString());
}

/*Tree* Tree::clone() const {
	Tree* newTree = new Tree(*this);
	return newTree;
}*/

void Tree::addUpdatedNodes(std::vector<TreeNode*>& aUpdTreeNodes) {
	updatedNodes.insert(updatedNodes.end(), aUpdTreeNodes.begin(), aUpdTreeNodes.end());
}

const std::vector<TreeNode*>& Tree::getUpdatedNodes() const {
	return updatedNodes;
}

void Tree::clearUpdatedNodes() {
	updatedNodes.clear();
}

void Tree::update(const std::vector<TreeNode*> &aUpdatedNodes) {

	// Add to updatedNodes vector
	updatedNodes.insert(updatedNodes.end(), aUpdatedNodes.begin(), aUpdatedNodes.end());

	// update the tree using updated nodes
	updateTree(aUpdatedNodes);

	// Get the tree
	idString = root->subString;
	idString.append(";");

	// Update the hash key
	createHashKey();

	//assert(areBipartitionsModified);
	//areBipartitionsModified = false; // We clean the flag since the tree has been notified has upated
	//initBipartitions();

	//assert(false && "Test for bipartitions correctness here.");
	//checkUpdatdeSplitCorrectness();

	areBipartitionsReady = false;

}

const Utils::Serialize::buffer_t& Tree::save() {
	serializedBuffer.clear();
	Utils::Serialize::save(*this, serializedBuffer);
	return serializedBuffer;
}


void Tree::DBG_checkChildenOrder(TreeNode *node, TreeNode *parent) {

	vecTN_t children = node->getChildren(parent);

	for(size_t iC=0; iC<children.size(); ++iC) {
		assert(children[iC]->neighbours.back() == node);
		DBG_checkChildenOrder(children[iC], node);
	}
}

void Tree::load(const Utils::Serialize::buffer_t &buffer) {
	Utils::Serialize::load(buffer, *this);
}

void Tree::defineNodesCategory() {
	// Clear before
	internals.clear();
	terminals.clear();
	nodesMap.clear();

	// Then populate vectors
	exploreTree(root, NULL);

	// Sort leaves by idNames
	std::sort(terminals.begin(), terminals.end(), SortTreeNodePtrByIdName());
}

void Tree::exploreTree(TreeNode *node, TreeNode *parent) {
	nodesMap[node->getId()] = node;

	if(node->isTerminal()) { // If terminal its easy
		terminals.push_back(node);
		// Init internal values
		node->setParent(parent);
		node->setMinId(node->getIdName());
		node->subString = node->getIdString();
	} else { // If internal : we memorize edges and we order nodes
		size_t minId = std::numeric_limits<size_t>::max();

		// Get children
		vecTN_t children = node->getChildren(parent);
		std::vector<size_t> minIdSubT(children.size());

		for(size_t iC=0; iC<children.size(); ++iC) {
			// Explore the subtree
			exploreTree(children[iC], node);
			minIdSubT[iC] = children[iC]->getMinId();
			// Keep min Id for local node
			minId = std::min(minId, minIdSubT[iC]);
		}
		node->setMinId(minId);

		// If we are not terminal we are internal then
		internals.push_back(node);

		// Order nodes neighbours
		node->neighbours.clear(); // Clear old neighbours
		bool ascending = false;
		std::vector<size_t> orderedId = Utils::Algorithm::getOrderedIndices(minIdSubT,ascending);

		// Add children ordered by min leaf ID
		for(size_t iC=0; iC<children.size(); ++iC) {
			node->neighbours.push_back(children[orderedId[iC]]);
		}
		// Then add parent
		if(parent != NULL) {
			node->setParent(parent);
			node->neighbours.push_back(parent);
		}
		// Add subtrees ordered by min leaf ID
		node->subString = "(";
		for(size_t iC=0; iC<children.size(); ++iC) {
			if(iC>0) {
				node->subString.push_back(',');
			}
			node->subString.append(children[orderedId[iC]]->subString);
		}
		node->subString.push_back(')');
		node->subString.append(node->getIdString());
	}
}

void Tree::updateTree(const std::vector<TreeNode*> &aUpdatedNodes) {

	std::list<TreeNode*> listNodes;

	// Initial nodes to look-up
	for(size_t iN=0; iN<aUpdatedNodes.size(); ++iN) {
		if(std::find(listNodes.begin(), listNodes.end(), aUpdatedNodes[iN]) == listNodes.end()) {
			listNodes.push_back(aUpdatedNodes[iN]);
		}
	}

	while(!listNodes.empty()) {
		// Get first node
		TreeNode *node = listNodes.front();
		listNodes.pop_front();

		if(node->isTerminal()) {
			continue;
		}

		// reset minId
		size_t minId = std::numeric_limits<size_t>::max();
		// Get children
		vecTN_t children = node->getChildren();
		std::vector<size_t> minIdSubT(children.size());
		for(size_t iC=0; iC<children.size(); ++iC) {
			minIdSubT[iC] = children[iC]->getMinId();
			// Keep min Id for local node
			minId = std::min(minId, minIdSubT[iC]);
		}
		node->setMinId(minId);

		// Order nodes neighbours
		node->neighbours.clear(); // Clear old neighbours
		bool ascending = false;
		std::vector<size_t> orderedId = Utils::Algorithm::getOrderedIndices(minIdSubT,ascending);

		// Add children ordered by min leaf ID
		for(size_t iC=0; iC<children.size(); ++iC) {
			node->neighbours.push_back(children[orderedId[iC]]);
		}
		// Then add parent
		if(node->getParent() != NULL) {
			node->neighbours.push_back(node->getParent());
			if(std::find(listNodes.begin(), listNodes.end(), node->getParent()) == listNodes.end()) {
				listNodes.push_back(node->getParent());
			}
		}

		// Add subtrees ordered by min leaf ID
		node->subString = "(";
		for(size_t iC=0; iC<children.size(); ++iC) {
			if(iC>0) {
				node->subString.push_back(',');
			}
			node->subString.append(children[orderedId[iC]]->subString);
		}
		node->subString.push_back(')');
		node->subString.append(node->getIdString());
	}
}

void Tree::createRecursive(const DL::TreeNode &newickNode, TreeNode *node,
		const std::map<std::string, int> & nameMapping) {
	if(!newickNode.isLeaf()) {  // If node has children, process
		for(size_t i=0; i<newickNode.getChildren().size(); ++i){
			TreeNode *childNode = new TreeNode(newickNode.getChildren()[i], nameMapping);
			createRecursive(newickNode.getChildren()[i], childNode, nameMapping);
			node->addNeighbour(childNode);
			childNode->addNeighbour(node);
		}
	}
}

TreeNode* Tree::createFromMSA(bool doShuffle, const DataLoader::MSA &msa,
		const std::map<std::string, int>& nameMapping) {
	const std::vector<DL::Alignment>& aligns = msa.getAlignments();
	std::vector<TreeNode*> curNodes, lastNodes;

	// Index vector
	std::vector<size_t> ind;
	for(size_t i=0; i<aligns.size(); ++i) {
		ind.push_back(i);
	}

	// shuffle index if requested
	if(doShuffle) {
		//Parallel::mpiMgr().getPRNG()->randomShuffle(ind);
		std::random_shuffle(ind.begin(), ind.end());
	}

	// Init leaves
	for(size_t iL=0; iL<aligns.size(); ++iL) {
		size_t indice = ind[iL];
		std::map<std::string, int>::const_iterator it;
		it = nameMapping.find(aligns[indice].getName());
		if(it != nameMapping.end()) {
			size_t idName = it->second;
			TreeNode *node = new TreeNode(idName);
			curNodes.push_back(node);
		} else {
			std::cerr << "[ERROR] TreeNode::TreeNode(const DL::TreeNode &aNode, const std::map<std::string, int> & nameMapping);" << std::endl;
			std::cerr << "Id name not found for name : '" << aligns[indice].getName() << "'." << std::endl;
			assert(it == nameMapping.end());
		}
	}

	// Create tree structure
	lastNodes = curNodes;
	curNodes.clear();
	while(lastNodes.size() > 1) { // While we have more than one node per level
		bool isOdd = (lastNodes.size() % 2) == 1;
		size_t nPair = (isOdd ? lastNodes.size()-1 : lastNodes.size()) / 2;
		for(size_t iL=0; iL<nPair; ++iL) { // Create internal node for each pair of nodes
			// Create internal nodes
			TreeNode *node = new TreeNode(-1);
			// Add edges
			node->addNeighbour(lastNodes[iL*2]);
			lastNodes[iL*2]->addNeighbour(node);
			node->addNeighbour(lastNodes[iL*2+1]);
			lastNodes[iL*2+1]->addNeighbour(node);
			// add node to current layer
			curNodes.push_back(node);
		}
		if(isOdd) { // If the number of nodes is odd, we add the last one as such
			curNodes.push_back(lastNodes.back());
		}

		// Prepare for next iteration
		lastNodes = curNodes;
		curNodes.clear();
	}
	// Get the new tree root
	assert(lastNodes.size() == 1);
	TreeNode *tmpRoot = lastNodes.back();

	// define ids for all the nodes
	size_t id = 0;
	assignIdRecursively(id, tmpRoot, NULL);

	return tmpRoot;
}

void Tree::assignIdRecursively(size_t &id, TreeNode *node, TreeNode *parent) {
	// Assign the id of current node
	node->setId(id);
	nodesMap[node->getId()] = node;
	id++;

	// For all children do the same
	vecTN_t children = node->getChildren(parent);
	for(size_t iC=0; iC<children.size(); ++iC) {
		assignIdRecursively(id, children[iC], node);
	}
}

void Tree::assignIdRecursively(size_t &id, TreeNode *node, TreeNode *parent, std::map<size_t, size_t> &oldIDs) {
	// keep oldIds
	oldIDs.insert(std::make_pair(id, node->getId()));
	// Assign the id of current node
	node->setId(id);
	nodesMap[node->getId()] = node;
	id++;

	// For all children do the same
	vecTN_t children = node->getChildren(parent);
	for(size_t iC=0; iC<children.size(); ++iC) {
		assignIdRecursively(id, children[iC], node, oldIDs);
	}
}


void Tree::defineBalancingRoot() {
	TreeNode *curNode = root;

	std::vector<size_t> sts = root->setSubTreesSize(NULL);
	std::cout << "node [" << root->getId() << "] -> Subtree sizes : ";
	std::copy(sts.begin(), sts.end(), std::ostream_iterator<size_t>(std::cout, ", "));
	std::cout << std::endl;

	bool improve = true;
	do {
		size_t iMax=0, max=0, sumOthers=0;
		for(size_t iS=0; iS<curNode->getNeighbours().size(); ++iS) {
			sumOthers += curNode->getNeighbour(iS)->getMySubtreeSize();
			if(curNode->getNeighbour(iS)->getMySubtreeSize() > max) {
				iMax = iS;
				max = curNode->getNeighbour(iS)->getMySubtreeSize();
			}
		}
		sumOthers -= max;

		improve = max > sumOthers+1;
		if(improve) {
			curNode->setMySubtreeSize(sumOthers+1);
			curNode = curNode->getNeighbour(iMax);
			curNode->setMySubtreeSize(max+sumOthers+1);
		}

	} while(improve);

	sts = curNode->setSubTreesSize(NULL);
	std::cout << "curNode [" << curNode->getId() << "] -> Subtree sizes : ";
	std::copy(sts.begin(), sts.end(), std::ostream_iterator<size_t>(std::cout, ", "));
	std::cout << std::endl;

}

template<class Archive>
void Tree::save(Archive & ar, const unsigned int version) const {
	ar & idString;
}

template<class Archive>
void Tree::load(Archive & ar, const unsigned int version) {
	//std::string treeString;
	ar & idString;

	Parser parser(idString);
	root = parser.getTreeRoot();
	createHashKey();

	//initBipartitions();
	resetBipartitions();
}


std::vector < Tree::edge_t > Tree::getNeighbouringInternalEdges(TreeNode *pNode, TreeNode *cNode) const {
	std::vector < Tree::edge_t > edges;
	vecTN_t nontTerminalChildren = cNode->getNonTerminalChildren(pNode);

	for(size_t iN=0; iN<nontTerminalChildren.size(); ++iN) {
		edge_t edge = {cNode, nontTerminalChildren[iN]};
		edges.push_back(edge);
	}

	return edges;
}


std::vector< Tree::contiguous_edge_pair_t > Tree::getContiguousInternalEdges() const {

	//CustomProfiling cp;
	//cp.startTime();

	std::vector<Tree::edge_t> internalEdges = getInternalEdges();
	std::set<contiguous_edge_pair_t> uniqueEdgePairs;

	for(size_t iE=0; iE<internalEdges.size(); ++iE) {
		edge_t edge = internalEdges[iE];
		//std::cout << "Edge : " << edge.n1->getId() << " -- " << edge.n2->getId() << std::endl;

		{
			std::vector<edge_t> edgesA = getNeighbouringInternalEdges(edge.n1, edge.n2);
			for(size_t iEA=0; iEA<edgesA.size(); ++iEA) {
				contiguous_edge_pair_t cep = {edge.n1, edge.n2, edgesA[iEA].n2};
				//std::cout << "Add1 : " << edge.n1->getId() << " -- " << edge.n2->getId() << " -- " << edgesA[iEA].n2->getId() << std::endl;
				cep.orderNodes();
				uniqueEdgePairs.insert(cep);
			}
		}

		{
			std::vector<edge_t> edgesB = getNeighbouringInternalEdges(edge.n2, edge.n1);
			for(size_t iEB=0; iEB<edgesB.size(); ++iEB) {
				contiguous_edge_pair_t cep = {edge.n2, edge.n1, edgesB[iEB].n2};
				//std::cout << "Add2 : " << edge.n2->getId() << " -- " << edge.n1->getId() << " -- " << edgesB[iEB].n2->getId() << std::endl;
				cep.orderNodes();
				uniqueEdgePairs.insert(cep);
			}
		}
	}

	std::vector<Tree::contiguous_edge_pair_t> vecCEP;
	for(std::set<Tree::contiguous_edge_pair_t>::iterator it=uniqueEdgePairs.begin(); it!=uniqueEdgePairs.end(); it++) {
		vecCEP.push_back(*it);
		//std::cout << (*it).n1->getId() << " -- " << (*it).n2->getId() << " -- " << (*it).n3->getId() << std::endl;
		assert((*it).n1 != (*it).n3);
	}
	//std::cout << "---------------------------------" << std::endl;
	/*cp.endTime();
	std::cout << std::scientific << cp.duration() << std::endl;

	size_t K = 3;
	cp.startTime();
	std::vector< Tree::edge_path_t > paths(computeKNodesPaths(K));
	cp.endTime();
	std::cout << std::scientific <<  cp.duration() << std::endl;

	std::cout << vecCEP.size() << " vs " << paths.size() << std::endl;
	assert(vecCEP.size() == paths.size());

	for(size_t i=0; i<vecCEP.size(); ++i) {

		Tree::edge_path_t ePath;
		ePath.nodes.push_back(vecCEP[i].n1);
		ePath.nodes.push_back(vecCEP[i].n2);
		ePath.nodes.push_back(vecCEP[i].n3);

		std::vector< Tree::edge_path_t >::iterator it = std::find(paths.begin(), paths.end(), ePath);
		assert(it != paths.end());
	}*/


	return vecCEP;

}

split_t Tree::createSplitsRecursive(TreeNode* cNode, TreeNode* pNode) {
	if(cNode->isTerminal()) {
		size_t leafId = cNode->getIdName();
		split_t split(terminals.size(), false);
		split[leafId] = true;

		Tree::edge_t edge = {pNode, cNode};

		allEdges.push_back(edge);
		allSplits.push_back(split);
		if(split[0]) allSplits.back().flip();

		dirSplits.push_back(split);
		if(edge.n1->getId() > edge.n2->getId()) {
			std::swap(edge.n1, edge.n2);
			dirSplits.back().flip();
		}
		dirEdges.push_back(edge);

		return split;
	} else {
		split_t split(terminals.size(), false);
		vecTN_t children = cNode->getChildren(pNode);
		for(size_t iC=0; iC<children.size(); ++iC) {
			split |= createSplitsRecursive(children[iC], cNode);
		}

		if(pNode != NULL) {
			Tree::edge_t edge = {pNode, cNode};

			allEdges.push_back(edge);
			allSplits.push_back(split);
			if(split[0]) allSplits.back().flip();

			internalEdges.push_back(allEdges.back());
			internalSplits.push_back(allSplits.back());

			dirSplits.push_back(split);
			if(edge.n1->getId() > edge.n2->getId()) {
				std::swap(edge.n1, edge.n2);
				dirSplits.back().flip();
			}
			dirEdges.push_back(edge);
		}

		return split;
	}
}




void Tree::resetBipartitions() {
	areBipartitionsReady = false;
	areBipartitionsModified = false;
}


size_t Tree::findEdgePositionInVector(edge_t aEdge, std::vector<edge_t> &aEdgeVector) {
	size_t iPos = 0;
	bool found = false;

	for(size_t iE=0; iE<aEdgeVector.size(); ++iE) {
		if((aEdgeVector[iE].n1->getId() == aEdge.n1->getId() && aEdgeVector[iE].n2->getId() == aEdge.n2->getId()) ||
		   (aEdgeVector[iE].n1->getId() == aEdge.n2->getId() && aEdgeVector[iE].n2->getId() == aEdge.n1->getId())) {
			iPos = iE;
			found = true;
			break;
		}
	}
	assert(found);
	return iPos;

}


void Tree::addBiparition(edge_t toAdd, split_t split) {

	assert(areBipartitionsReady);
	//std::cout << "Adding bipartition for edge " << toAdd.n1->getId() << " -- " << toAdd.n2->getId() << " -- " << split << std::endl;

	// Deal with the directed edge
	dirEdges.push_back(toAdd);
	dirSplits.push_back(split);

	if(dirEdges.back().n1->getId() > dirEdges.back().n2->getId()) {
		std::swap(dirEdges.back().n1, dirEdges.back().n2);
		dirSplits.back().flip();
	}

	// Deal with the undirected edge containers
	if(split[0]) split.flip();

	allEdges.push_back(toAdd);
	allSplits.push_back(split);

	if(!toAdd.n1->isTerminal() && !toAdd.n2->isTerminal()) {
		internalEdges.push_back(toAdd);
		internalSplits.push_back(split);
	}

	areBipartitionsModified = true;

}

void Tree::updateBipartition(edge_t toUpdate, split_t split) {

	assert(areBipartitionsReady);
	//std::cout << "Updating bipartition for edge " << toUpdate.n1->getId() << " -- " << toUpdate.n2->getId() << " -- " << split << std::endl;

	bool hasTerminalNode = toUpdate.n1->isTerminal() || toUpdate.n2->isTerminal();

	{ // This is the position in dirEdges and allEdges
	size_t iPosAll = findEdgePositionInVector(toUpdate, allEdges);

	dirSplits[iPosAll] = split;
	if(dirEdges[iPosAll].n1->getId() != toUpdate.n1->getId()) { // if the edge don't share the direction
		dirSplits[iPosAll].flip(); // flip it
	}

	if(split[0]) split.flip(); // Flip it if needed
	allSplits[iPosAll] = split; // Update it in all

	}

	if(!hasTerminalNode) {
		// This is the position in internalEdges
		size_t iPosInt = findEdgePositionInVector(toUpdate, internalEdges);
		internalSplits[iPosInt] = split; // Update it in int
	}

	areBipartitionsModified = true;;
}

void Tree::removeBipartition(edge_t toRemove) {

	assert(areBipartitionsReady);
	//std::cout << "Removing bipartition for edge " << toRemove.n1->getId() << " -- " << toRemove.n2->getId() << std::endl;

	bool hasTerminalNode = toRemove.n1->isTerminal() || toRemove.n2->isTerminal();

	{ // This is the position in dirEdges and allEdges
	size_t iPosAll = findEdgePositionInVector(toRemove, allEdges);

	size_t sizeMinusOneAll = allEdges.size() - 1;
	size_t iLastAll = sizeMinusOneAll;

	std::swap(allEdges[iPosAll], allEdges[iLastAll]);
	std::swap(allSplits[iPosAll], allSplits[iLastAll]);

	allEdges.resize(sizeMinusOneAll);
	allSplits.resize(sizeMinusOneAll);

	std::swap(dirEdges[iPosAll], dirEdges[iLastAll]);
	std::swap(dirSplits[iPosAll], dirSplits[iLastAll]);

	dirEdges.resize(sizeMinusOneAll);
	dirSplits.resize(sizeMinusOneAll);
	}

	if(!hasTerminalNode) {
		// This is the position in internalEdges
		size_t iPosInt = findEdgePositionInVector(toRemove, internalEdges);

		size_t sizeMinusOneInt = internalEdges.size() - 1;
		size_t iLastInt = sizeMinusOneInt;

		std::swap(internalEdges[iPosInt], internalEdges[iLastInt]);
		std::swap(internalSplits[iPosInt], internalSplits[iLastInt]);

		internalEdges.resize(sizeMinusOneInt);
		internalSplits.resize(sizeMinusOneInt);
	}

	areBipartitionsModified = true;
}


void Tree::initBipartitions() {

	allEdges.clear();
	allSplits.clear();

	internalEdges.clear();
	internalSplits.clear();

	dirEdges.clear();
	dirSplits.clear();

	createSplitsRecursive(getRoot(), NULL);

	areBipartitionsReady = true;
	areBipartitionsModified = false;

}

split_t Tree::findBipartitions(TreeNode *n1, TreeNode *n2) {
	edge_t edge = {n1, n2};
	return findBipartitions(edge);
}

split_t Tree::findBipartitions(edge_t edge) {

	if(!areBipartitionsReady) {
		initBipartitions();
	}

	bool reversed = false;
	if(edge.n1->getId() > edge.n2->getId()) {
		reversed = true;
		std::swap(edge.n1, edge.n2);
	}

	size_t iFound = 0;
	bool found = false;
	for(size_t iD=0; iD<dirEdges.size(); ++iD) {
		if(edge.n1 == dirEdges[iD].n1 && edge.n2 == dirEdges[iD].n2) {
			found = true;
			iFound = iD;
			break;
		}
	}
	assert(found && "Edge not found.");

	split_t aSplit(dirSplits[iFound]);
	if(reversed) aSplit.flip();
	return aSplit;
}

std::vector<Tree::edge_t> Tree::getInternalEdgeForBipartitions() {

	if(!areBipartitionsReady) {
		initBipartitions();
	}

	return internalEdges;
}

std::vector<split_t> Tree::getInternalSplitsForBipartitions() {

	if(!areBipartitionsReady) {
		initBipartitions();
	}

	return internalSplits;
}

std::vector<Tree::edge_t> Tree::getAllEdgeForBipartitions() {

	if(!areBipartitionsReady) {
		initBipartitions();
	}

	return allEdges;
}

std::vector<split_t> Tree::getAllSplitsForBipartitions() {

	if(!areBipartitionsReady) {
		initBipartitions();
	}

	return allSplits;
}

std::vector<Tree::edge_t> Tree::getDirectedEdgeForBipartitions() {

	if(!areBipartitionsReady) {
		initBipartitions();
	}

	return dirEdges;
}

std::vector<split_t> Tree::getDirectedSplitsForBipartitions() {

	if(!areBipartitionsReady) {
		initBipartitions();
	}

	return dirSplits;
}

void Tree::checkUpdatdeSplitCorrectness() {

	std::vector< edge_t > allEdges2, internalEdges2, dirEdges2;
	std::vector< split_t >  allSplits2, internalSplits2, dirSplits2;

	allEdges2 = allEdges;
	internalEdges2 = internalEdges;
	dirEdges2 = dirEdges;

	allSplits2 = allSplits;
	internalSplits2 = internalSplits;
	dirSplits2 = dirSplits;

	initBipartitions();

	{
		std::vector< edge_t > &vecE1 = allEdges;
		std::vector< split_t > &vecS1 = allSplits;

		std::vector< edge_t > &vecE2 = allEdges2;
		std::vector< split_t > &vecS2 = allSplits2;

		assert(vecE1.size() == vecE2.size());
		assert(vecS1.size() == vecS2.size());
		assert(vecE1.size() == vecS1.size());

		for(size_t i=0; i<vecE1.size(); ++i) {

			bool found = false;
			bool correctSplit = false;
			Tree::edge_t edge = vecE1[i];

			for(size_t j=0; j<vecE2.size(); ++j) {

				if(((edge.n1->getId() == vecE2[j].n1->getId()) && (edge.n2->getId() == vecE2[j].n2->getId())) ||
				   ((edge.n1->getId() == vecE2[j].n2->getId()) && (edge.n2->getId() == vecE2[j].n1->getId()))) {

					found = true;
					correctSplit = vecS1[i] == vecS2[j];

					break;
				}
			}
			if(!found || !correctSplit) {
				std::cout << "Problem : ";
				std::cout << vecE1[i].n1->getId() << " -- ";
				std::cout << vecE1[i].n2->getId() << " -- ";
				std::cout << vecS1[i] << std::endl;;
			}
			assert(found && "All found");
			assert(correctSplit && "All split");
		}
	}

	{
		std::vector< edge_t > &vecE1 = internalEdges;
		std::vector< split_t > &vecS1 = internalSplits;

		std::vector< edge_t > &vecE2 = internalEdges2;
		std::vector< split_t > &vecS2 = internalSplits2;

		assert(vecE1.size() == vecE2.size());
		assert(vecS1.size() == vecS2.size());
		assert(vecE1.size() == vecS1.size());

		for(size_t i=0; i<vecE1.size(); ++i) {

			bool found = false;
			bool correctSplit = false;
			Tree::edge_t edge = vecE1[i];

			for(size_t j=0; j<vecE2.size(); ++j) {

				if(((edge.n1->getId() == vecE2[j].n1->getId()) && (edge.n2->getId() == vecE2[j].n2->getId())) ||
				   ((edge.n1->getId() == vecE2[j].n2->getId()) && (edge.n2->getId() == vecE2[j].n1->getId()))) {

					found = true;
					correctSplit = vecS1[i] == vecS2[j];

					break;
				}
			}
			if(!found) {
				std::cout << "Problem : ";
				std::cout << vecE1[i].n1->getId() << " -- ";
				std::cout << vecE1[i].n2->getId() << " -- ";
				std::cout << vecS1[i] << std::endl;;
			}
			assert(found && "Internal found");
			assert(correctSplit && "Internal split");
		}
	}

	{
		std::vector< edge_t > &vecE1 = dirEdges;
		std::vector< split_t > &vecS1 = dirSplits;

		std::vector< edge_t > &vecE2 = dirEdges2;
		std::vector< split_t > &vecS2 = dirSplits2;

		assert(vecE1.size() == vecE2.size());
		assert(vecS1.size() == vecS2.size());
		assert(vecE1.size() == vecS1.size());

		for(size_t i=0; i<vecE1.size(); ++i) {

			bool found = false;
			bool correctSplit = false;
			Tree::edge_t edge = vecE1[i];

			for(size_t j=0; j<vecE2.size(); ++j) {

				if(((edge.n1->getId() == vecE2[j].n1->getId()) && (edge.n2->getId() == vecE2[j].n2->getId()))) {

					found = true;
					correctSplit = vecS1[i] == vecS2[j];

					break;
				}

				if(((edge.n1->getId() == vecE2[j].n2->getId()) && (edge.n2->getId() == vecE2[j].n1->getId()))) {
					found = true;
					split_t compSplit = vecS2[j];
					compSplit.flip();
					correctSplit = vecS1[i] == compSplit;

					break;
				}

			}
			if(!found) {
				std::cout << "Problem : ";
				std::cout << vecE1[i].n1->getId() << " -- ";
				std::cout << vecE1[i].n2->getId() << " -- ";
				std::cout << vecS1[i] << std::endl;;
			}
			assert(found && "Dir found");
			assert(correctSplit && "Dir split");
		}
	}
}

std::vector< Tree::edge_path_t > Tree::computeKNodesPaths(const size_t K, pathConstraint_t pathConstrType,
											  	  	  	  TreeNode* toGraft, TreeNode* toGraftParent) const {

	std::set< Tree::edge_path_t > uniquePaths;

	vecTN_t potentialDirections = toGraftParent->getNonTerminalChildren(toGraft);
	for(size_t iN=0; iN<potentialDirections.size(); ++iN) {
		edge_path_t start;
		start.nodes.push_back(toGraftParent);
		start.nodes.push_back(potentialDirections[iN]);

		recursiveFindKNodesPaths(2, K, start, uniquePaths, pathConstrType);
	}

	std::vector< Tree::edge_path_t > result;
	result.insert(result.end(), uniquePaths.begin(), uniquePaths.end());

	return result;
}

std::vector< Tree::edge_path_t > Tree::computeAllKNodesPaths(const size_t K, pathConstraint_t pathConstrType) const {

	std::set< Tree::edge_path_t > uniquePaths;

	for(size_t iN=0; iN<internals.size(); ++iN) {
		if(root != internals[iN]) {

			// First direction
			edge_path_t start;
			start.nodes.push_back(internals[iN]->getParent());
			start.nodes.push_back(internals[iN]);

			recursiveFindKNodesPaths(2, K, start, uniquePaths, pathConstrType);

			// Second direction
			std::swap(start.nodes.front(), start.nodes.back());

			recursiveFindKNodesPaths(2, K, start, uniquePaths, pathConstrType);
		}
	}

	std::vector< Tree::edge_path_t > result;
	result.insert(result.end(), uniquePaths.begin(), uniquePaths.end());

	return result;

}

void Tree::recursiveFindKNodesPaths(size_t iK, const size_t K, edge_path_t &current, std::set< edge_path_t > &paths, pathConstraint_t pathConstrType) const {

	//assert(current.nodes.size() >= 2);

	if(pathConstrType == WITHIN_K_NODES) {
		edge_path_t orderedPath = current;
		orderedPath.orderNodes();
		paths.insert(orderedPath);
	}

	if(iK == K) { // We reached the path size
		if(pathConstrType == EXACTLY_K_NODES) {
			edge_path_t orderedPath = current;
			orderedPath.orderNodes();
			paths.insert(orderedPath);
		}
		return;
	}

	// Next nodes that can be added to the path in this direction
	size_t iLast = current.nodes.size()-1;
	vecTN_t children = current.nodes[iLast]->getNonTerminalChildren(current.nodes[iLast-1]);

	// Try to add them
	for(size_t iC=0; iC<children.size(); ++iC) {
		current.nodes.push_back(children[iC]);
		recursiveFindKNodesPaths(iK+1, K, current, paths, pathConstrType);
		current.nodes.pop_back();
	}

}



/*void Tree::copy(const Tree &other) {
	myH = other.myH;
	idString = other.idString;
	root = cloneTree(other.root, NULL);
}*/

/*TreeNode* Tree::cloneTree(TreeNode *node, TreeNode *parent) {

	// Create the new node
	TreeNode *newNode = new TreeNode(node->getIdName());
	newNode->setId(node->getId());

	if(node->isTerminal()) { // If is terminal
		terminals.push_back(newNode);
	} else {
		// For all children of the node to copy
		vecTN_t children = node->getChildren(parent);
		for(size_t iC=0; iC<children.size(); ++iC) {
			TreeNode *childNode = cloneTree(children[iC], node);
			// Create links
			newNode->addNeighbour(childNode);
			childNode->addNeighbour(newNode);
		}
		internals.push_back(newNode);
	}

	return newNode;
}*/

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
