/*
 * LikelihoodMapping.cpp
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/LikelihoodMapping.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace LikelihoodMapper {

LikelihoodMapping::LikelihoodMapping() {

}

LikelihoodMapping::~LikelihoodMapping() {
}

std::string LikelihoodMapping::getNewickSubstring() const {
	return ":1.";
}


std::string LikelihoodMapping::toString() const {
	return "";
}

} /* namespace LikelihoodMapper */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
