/**
 * TreeNodeLikelihood.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: meyerx
 */

#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/TreeNode.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace LikelihoodMapper {

TreeNode::TreeNode(const TR::TreeNode *aTreeNodeTR, LikelihoodMapping::sharedPtr_t aLikMapping) :
	likelihoodMapping(aLikMapping) {
	id = aTreeNodeTR->getId();

	leaf = false;
	requireReinitDAG = false;
}

TreeNode::~TreeNode() {
}

void TreeNode::addChild(TreeNode *child) {
	children.push_back(child);

	// Add DAG link
	if(likelihoodMapping != NULL) {
		assert(child->getLikelihoodMapping());
		likelihoodMapping->doAddChild(child->getLikelihoodMapping());
	}
}

bool TreeNode::removeChild(TreeNode *child) {

	// Remove DAG link
	if(likelihoodMapping != NULL) {
		likelihoodMapping->doRemoveChild(child->getLikelihoodMapping());
	}

	std::vector<TreeNode*>::iterator position = std::find(children.begin(), children.end(), child);
	if (position != children.end()) {
		children.erase(position);
		//sortChildren();
		return true;
	}
	return false;
}

bool TreeNode::isParent(const TreeNode *aChild) const {
	std::vector<TreeNode*>::const_iterator position = std::find(children.begin(), children.end(), aChild);
	return position != children.end();
}

bool TreeNode::isChild(const TreeNode *aParent) const {
	return aParent->isParent(this);
}

TreeNode* TreeNode::getChild(size_t id) {
	return children[id];
}


childrenTreeNode_t& TreeNode::getChildren() {
	return children;
}

const childrenTreeNode_t& TreeNode::getChildren() const {
	return children;
}

size_t TreeNode::getId() const {
	return id;
}

void TreeNode::setLeaf(const bool aLeaf) {
	leaf = aLeaf;
}

bool TreeNode::isLeaf() const {
	return leaf;
}

void TreeNode::setRequireReinitDAG(const bool aRequire) {
	requireReinitDAG = aRequire;
}

bool TreeNode::doRequireReinitDAG() const {
	return requireReinitDAG;
}


std::string TreeNode::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << "Node [" << id << "]";
	if(leaf) ss << " - isLeaf";
	ss << std::endl;
	for(uint i=0; i<children.size(); ++i){
		ss << "[" << children[i]->id << "]";
		if(i < children.size()-1) ss << " :: ";
	}

	return ss.str();
}

void TreeNode::deleteChildren() {
	while(!children.empty()) {
		TreeNode *child = children.back();
		child->deleteChildren();
		children.pop_back();
		delete child;
	}
}

std::string TreeNode::buildNewick() const {
	std::string nwkStr;
	addNodeToNewick(nwkStr);
	return nwkStr;
}

void TreeNode::addNodeToNewick(std::string &nwk) const {
	if(!children.empty()) {
		nwk.push_back('(');
		children[0]->addNodeToNewick(nwk);

		for(size_t iC=1; iC<children.size(); ++iC) {
			nwk.push_back(',');
			nwk.push_back(' ');
			children[iC]->addNodeToNewick(nwk);
		}
		nwk.push_back(')');
	}

	nwk.append(likelihoodMapping->getNewickSubstring());
}

/*void TreeNode::setLikelihoodMapping(LikelihoodMapping::sharedPtr_t aLikMapping) {
	likelihoodMapping = aLikMapping;
}*/


LikelihoodMapping::sharedPtr_t TreeNode::getLikelihoodMapping() {
	return likelihoodMapping;
}

} /* namespace LikelihoodMapper */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
