/*
 * TreeMapper.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: meyerx
 */

#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/TreeMapper.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace LikelihoodMapper {

TreeMapper::TreeMapper(std::map<size_t, TreeNode*> &aBranchMap) : branchMap(aBranchMap) {
}

TreeMapper::~TreeMapper() {
}

void TreeMapper::addEdge(TreeNode *parent, TreeNode *child) {
	assert(parent != NULL); assert(child != NULL);

	// Add tree link
	parent->addChild(child);
	parent->setRequireReinitDAG(true);
}

void TreeMapper::removeEdge(TreeNode *parent, TreeNode *child) {
	assert(parent != NULL); assert(child != NULL);
	// Was only required for moves
	// orderNodes(parent, child);

	// Remove tree link
	parent->removeChild(child);
}

void TreeMapper::orderChildren(TreeNode *nodeInt, TR::vecTN_t &childrenTR) {
	childrenTreeNode_t &childrenInt = nodeInt->getChildren();
	assert(childrenTR.size() == childrenInt.size());

	// Sort childrenInt according to childrenTR
	for(size_t iC=0; iC<childrenTR.size()-1; ++iC) {
		if(childrenTR[iC]->getId() != childrenInt[iC]->getId()) {
			bool found = false;
			for(size_t jC=iC+1; jC<childrenInt.size(); ++jC) {
				found = childrenTR[iC]->getId() == childrenInt[jC]->getId();
				if(found) {
					std::swap(childrenInt[iC], childrenInt[jC]);
					break;
				}
			}
			assert(found);
		}
	}
}

void TreeMapper::getDifference(const TR::vecTN_t &childrenTR, const childrenTreeNode_t &children,
		std::vector<TreeNode*> &toAdd, std::vector<TreeNode*> &toRemove) {

	// We check if childrenTR are in children
	for(size_t iTR=0; iTR<childrenTR.size(); ++iTR) { // for each TR child
		bool found = false;
		for(size_t iInt=0; iInt<children.size(); ++iInt) { // for each INT child
			found = childrenTR[iTR]->getId() == children[iInt]->getId();
			if(found) { // If found : ok
				break;
			}
		}
		if(!found) { // if not found : add to toAdd
			toAdd.push_back(branchMap[childrenTR[iTR]->getId()]);
		}
	}

	// We check if children are in childrenTR
	for(size_t iInt=0; iInt<children.size(); ++iInt) {  // for each INT child
		bool found = false;
		for(size_t iTR=0; iTR<childrenTR.size(); ++iTR) { // for each TR child
			found = children[iInt]->getId() == childrenTR[iTR]->getId();
			if(found) { // If found : ok
				break;
			}
		}
		if(!found) { // if not found : add to toRemove
			toRemove.push_back(children[iInt]);
		}
	}
}


void TreeMapper::applyChangesToTreeAndDAG(TR::Tree::sharedPtr_t curTree, std::vector<TR::TreeNode *> &updTreeNodes) {
	std::vector<TR::TreeNode *> updTNodes = curTree->getUpdatedNodes();

	size_t iN=0;
	while(iN<updTNodes.size()) { // updTNodes.size() may change during the loop
		// Get TR children
		TR::TreeNode *nodeTR = updTNodes[iN];
		TR::TreeNode *parentNodeTR = nodeTR->getParent();
		TR::vecTN_t childrenTR = nodeTR->getChildren();

		// Get Internal children
		TreeNode *node = branchMap[nodeTR->getId()];
		const childrenTreeNode_t &children = node->getChildren();

		if(parentNodeTR != NULL) { // If we are not at the root node
			TreeNode *parentNode = branchMap[parentNodeTR->getId()];
			// If the edge with the parent has changed
			if(!parentNode->isParent(node)) {
				// We must update the parent too
				updTNodes.push_back(parentNodeTR);
			}
		}

		// Get difference
		std::vector<TreeNode*> toAdd, toRemove;
		getDifference(childrenTR, children, toAdd, toRemove);

		if(!toRemove.empty() || !toAdd.empty()) { // If there are differences
			// Remove edges
			for(size_t iR=0; iR<toRemove.size(); ++iR) {
				removeEdge(node, toRemove[iR]);
			}

			// Add edges
			for(size_t iA=0; iA<toAdd.size(); ++iA) {
				addEdge(node, toAdd[iA]);
			}

			node->getLikelihoodMapping()->doOnApplyChange();

			// This node has been updated
			updTreeNodes.push_back(nodeTR);
		}
		iN++;
	}

}

void TreeMapper::signalUpdatedTreeAndNodes(const std::vector<TR::TreeNode *> &updTreeNodes,
		std::set<DAG::BaseNode*> &updatedNodes) {
	std::set<size_t> markerNodes;
	for(size_t iN=0; iN<updTreeNodes.size(); ++iN) {
		// TR children
		TR::TreeNode *nodeTR = updTreeNodes[iN];
		TR::vecTN_t childrenTR;
		// Internal node
		TreeNode *node;

		while (nodeTR != NULL) { // While we are not at the root

			// Get children and node
			childrenTR = nodeTR->getChildren();
			node = branchMap[nodeTR->getId()];
			// If visited, we are done
			if(markerNodes.count(node->getId()) > 0) {
				break;
			}

			// Order nodes
			orderChildren(node, childrenTR);

			// Signal node updated
			node->getLikelihoodMapping()->doOnUpdate(updatedNodes);

			// Mark node
			markerNodes.insert(node->getId());
			// Next node
			nodeTR = nodeTR->getParent();
		}
	}
}

void TreeMapper::partialUpdateTreeAndDAG(TR::Tree::sharedPtr_t curTree, std::set<DAG::BaseNode*> &updatedNodes) {
	// Vector to keep track of changed tree nodes
	std::vector<TR::TreeNode *> updTreeNodes;
	// Change TreeNode and dag Topology
	applyChangesToTreeAndDAG(curTree, updTreeNodes);
	// Signal which node must be updated in DAG and order the ndoes
	signalUpdatedTreeAndNodes(updTreeNodes, updatedNodes);
}


bool TreeMapper::fullUpdateTreeAndDAG(TR::Tree::sharedPtr_t curTree, TreeNode *rootNode, std::set<DAG::BaseNode*> &updatedNodes) {
	return doFullUpdateTreeAndDAG(curTree->getRoot(), NULL,  rootNode, updatedNodes);
}

bool TreeMapper::doFullUpdateTreeAndDAG(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TreeNode *node,
		std::set<DAG::BaseNode*> &updatedNodes) {
	bool requireReinit = false;

	// Get TR children
	TR::vecTN_t childrenTR = nodeTR->getChildren(parentNodeTR);
	// Get Internal children
	const childrenTreeNode_t &children = node->getChildren();

	// Get difference
	std::vector<TreeNode*> toAdd, toRemove;
	getDifference(childrenTR, children, toAdd, toRemove);

	// Remove edges
	for(size_t iR=0; iR<toRemove.size(); ++iR) {
		removeEdge(node, toRemove[iR]);
	}

	// Add edges
	for(size_t iA=0; iA<toAdd.size(); ++iA) {
		addEdge(node, toAdd[iA]);
	}

	// If the node is a leaf, we don't have to reinit it
	if(node->isLeaf()) return false;

	// Order nodes
	orderChildren(node, childrenTR);

	// Apply the same for all children
	for(size_t iC=0; iC<childrenTR.size(); ++iC) {
		bool childRequireInit = doFullUpdateTreeAndDAG(childrenTR[iC], nodeTR, node->getChild(iC), updatedNodes);
		requireReinit = requireReinit || childRequireInit;
	}
	// From there my subtree is correct
	// If one of my children was reinit'd or if I require to be reinit'd
	if(requireReinit || node->doRequireReinitDAG()) {
		// reinit each internal DAG nodes
		// Signal node updated
		node->getLikelihoodMapping()->doOnUpdate(updatedNodes);
		node->setRequireReinitDAG(false); // I did it
		return true; // But by dad as to do it
	} else {
		return false;
	}
}


} /* namespace LikelihoodMapper */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
