/*
 * TreeMapper.h
 *
 *  Created on: Apr 23, 2018
 *      Author: meyerx
 */

#ifndef UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_LIKELIHOODMAPPER_TREEMAPPER_H_
#define UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_LIKELIHOODMAPPER_TREEMAPPER_H_

#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/TreeNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace LikelihoodMapper {

namespace TR = ::MolecularEvolution::TreeReconstruction;

class TreeMapper {
public:
	TreeMapper(std::map<size_t, TreeNode*> &aBranchMap);
	~TreeMapper();

	void partialUpdateTreeAndDAG(TR::Tree::sharedPtr_t curTree, std::set<DAG::BaseNode*> &updatedNodes);
	bool fullUpdateTreeAndDAG(TR::Tree::sharedPtr_t curTree, TreeNode *rootNode, std::set<DAG::BaseNode*> &updatedNodes);

private:

	typedef std::map<size_t, TreeNode*> branchMap_t;
	branchMap_t &branchMap;

	// Utilitarian functions for the tree moves
	void addEdge(TreeNode *parent, TreeNode *child);
	void removeEdge(TreeNode *parent, TreeNode *child);
	void orderChildren(TreeNode *nodeInt, TR::vecTN_t &childrenTR);

	void getDifference(const TR::vecTN_t &childrenTR, const childrenTreeNode_t &children,
			std::vector<TreeNode*> &toAdd, std::vector<TreeNode*> &toRemove);
	void applyChangesToTreeAndDAG(TR::Tree::sharedPtr_t curTree, std::vector<TR::TreeNode *> &updTreeNodes);
	void signalUpdatedTreeAndNodes(const std::vector<TR::TreeNode *> &updTreeNodes,
			std::set<DAG::BaseNode*> &updatedNodes);

	bool doFullUpdateTreeAndDAG(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR,
			TreeNode *node,	std::set<DAG::BaseNode*> &updatedNodes);

};

} /* namespace LikelihoodMapper */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_LIKELIHOODMAPPER_TREEMAPPER_H_ */
