/*
 * LikelihoodMapping.h
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#ifndef UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_LIKELIHOODMAPPER_LIKELIHOODMAPPING_H_
#define UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_LIKELIHOODMAPPER_LIKELIHOODMAPPING_H_

#include <set>
#include <string>
#include <boost/shared_ptr.hpp>

#include "DAG/Node/Base/BaseNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/LikelihoodMapping.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace LikelihoodMapper {

class LikelihoodMapping {
public:
	typedef boost::shared_ptr<LikelihoodMapping> sharedPtr_t;

public:
	LikelihoodMapping();
	virtual ~LikelihoodMapping();

	virtual void doAddChild(LikelihoodMapping::sharedPtr_t child) = 0;
	virtual void doRemoveChild(LikelihoodMapping::sharedPtr_t child) = 0;

	virtual void doOnApplyChange() = 0;
	virtual void doOnUpdate(std::set<DAG::BaseNode*> &updatedNodes) = 0;

	virtual std::string getNewickSubstring() const;
	virtual std::string toString() const;

};

} /* namespace LikelihoodMapper */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_LIKELIHOODMAPPER_LIKELIHOODMAPPING_H_ */
