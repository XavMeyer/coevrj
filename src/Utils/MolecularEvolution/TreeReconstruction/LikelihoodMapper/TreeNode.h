/*
 * TreeNodeLikelihood.h
 *
 *  Created on: Apr 23, 2018
 *      Author: meyerx
 */

#ifndef UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_LIKELIHOODMAPPER_TREENODE_H_
#define UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_LIKELIHOODMAPPER_TREENODE_H_


#include <set>
#include <stddef.h>
#include <algorithm>
#include <sstream>
#include <vector>

#include "DAG/Node/Base/BaseNode.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeParser/TmpNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/LikelihoodMapping.h"


namespace MolecularEvolution { namespace TreeReconstruction { class TreeNode; } }

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace LikelihoodMapper {

namespace TR = ::MolecularEvolution::TreeReconstruction;

class TreeNode;

typedef std::vector<TreeNode*> childrenTreeNode_t;

class TreeNode {
public:
	TreeNode(const TR::TreeNode *aTreeNodeTR, LikelihoodMapping::sharedPtr_t aLikMapping);
	~TreeNode();

	void addChild(TreeNode *child);
	bool removeChild(TreeNode *child);

	bool isParent(const TreeNode *aChild) const;
	bool isChild(const TreeNode *aParent) const;

	TreeNode* getChild(size_t id);
	childrenTreeNode_t& getChildren();
	const childrenTreeNode_t& getChildren() const;
	size_t getId() const;

	void setLeaf(const bool aLeaf);
	bool isLeaf() const;

	void setRequireReinitDAG(const bool aRequire);
	bool doRequireReinitDAG() const;

	std::string toString() const;

	void deleteChildren();

	std::string buildNewick() const;
	void addNodeToNewick(std::string &nwk) const;

	//void setLikelihoodMapping(LikelihoodMapping::sharedPtr_t aLikMapping);
	LikelihoodMapping::sharedPtr_t getLikelihoodMapping();

protected:

	//friend class TreeMapper;

	/* Default data */
	size_t id;

	/* state */
	bool leaf, requireReinitDAG;
	childrenTreeNode_t children;

	LikelihoodMapping::sharedPtr_t likelihoodMapping;

};


struct SortTreeNodePtrById
{
    bool operator()( const TreeNode* n1, const TreeNode* n2 ) const {
    	return n1->getId() < n2->getId();
    }
};

} /* namespace LikelihoodMapper */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_LIKELIHOODMAPPER_TREENODE_H_ */
