//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * DecisionTreeState.cpp
 *
 *  Created on: Aug 13, 2019
 *      Author: xaviermeyer
 */

#include "DecisionTreeState.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

DecisionTreeState::DecisionTreeState() {
	countNew = 0;

}

DecisionTreeState::~DecisionTreeState() {
}

template<class Archive>
void DecisionTreeState::serialize(Archive & ar, const unsigned int version)  {

	ar & BOOST_SERIALIZATION_NVP(countNew);
	ar & BOOST_SERIALIZATION_NVP(finalNodeState);

}

template void DecisionTreeState::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version);
template void DecisionTreeState::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void DecisionTreeState::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version);
template void DecisionTreeState::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
