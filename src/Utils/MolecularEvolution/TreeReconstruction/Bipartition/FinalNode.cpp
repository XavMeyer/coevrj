//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FinalNode.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include "Parallel/Parallel.h"
#include "FinalNode.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

Utils::IDManagement::IDManager FinalNode::idManager;

FinalNode::FinalNode() : accChange(tag::rolling_mean::window_size = 1000) {
	id = idManager.allocateId();
	count = 0.;
	stableFreq = 0.;
}

FinalNode::~FinalNode() {
}


double FinalNode::getFrequency(size_t currentIteration) const {
	return count/(double)currentIteration;
}


std::string FinalNode::getSplitAsString() const {
	std::stringstream ss;
	for(size_t iN=0; iN<partition.size(); ++iN) {
		if(partition[iN]) {
			ss << "*";
		} else {
			ss << ".";
		}
	}
	return ss.str();
}

std::string FinalNode::getStatsString(size_t currentIteration) const {

	std::stringstream ss;
	ss << id << "\t" << getFrequency(currentIteration) << "\t" << getSplitAsString() << "\t";
	ss << boost::accumulators::count(acc) << "\t" << mean(acc) << "\t" << variance(acc);

	return ss.str();

}

size_t FinalNode::getCountBL() const {
	return boost::accumulators::count(acc);

}

double FinalNode::getMeanBL() const {
	return mean(acc);

}

double FinalNode::getVarianceBL() const {
	return variance(acc);
}

void FinalNode::setStableFreq(size_t currentIteration) {
	double stableFreqBefore = stableFreq;
	stableFreq = count/(double)currentIteration;

	double changeAtTimeT = stableFreq-stableFreqBefore;
	double change = changeAtTimeT*currentIteration/10.;
	accChange(change);
}

double FinalNode::getChangeAmplitude(size_t currentIteration) {
	return boost::accumulators::rolling_mean(accChange);
}

double FinalNode::getStableFreq() const {
	return stableFreq;
}

void FinalNode::saveState(FinalNodeState &state) const {

	state.id = id;

	state.partitionStr.clear();
	for(size_t i=0; i<partition.size(); ++i) {
		if(partition[i]) {
			state.partitionStr.push_back('*');
		} else {
			state.partitionStr.push_back('.');
		}
	}

	state.count = count;
	state.stableFreq = stableFreq;

	state.countAcc = boost::accumulators::count(acc);
	state.meanAcc = boost::accumulators::mean(acc);
	state.varAcc = boost::accumulators::variance(acc);

	state.rollingMeanAcc = boost::accumulators::rolling_mean(accChange);


	//std::cout << id << " -- Save : Count : " << state.countAcc << "\tmean : " << state.meanAcc << " \t var : " << state.varAcc << std::endl;

}

void FinalNode::loadState(FinalNodeState &state) {
	idManager.freeId(id);
	id = state.id;
	idManager.markAsUsed(id);

	for(size_t i=0; i<state.partitionStr.size(); ++i) {
		bool isPresent = state.partitionStr.at(i) == '*';
		assert(isPresent == partition[i]);
	}

	count = state.count;
	stableFreq = state.stableFreq;

	//std::cout << id << " -- Count : " << state.countAcc << "\tmean : " << state.meanAcc << " \t var : " << state.varAcc << std::endl;

	double shape = state.meanAcc*state.meanAcc/state.varAcc;
	double scale = state.varAcc/state.meanAcc;

	//std::cout << "shape : " << shape << " \t scale : " << scale << std::endl;

	if(state.countAcc == 1 || state.varAcc == 0.) {
		acc(state.meanAcc);
	} else if(state.countAcc > 1 && state.varAcc > 0.) {
		for(size_t iC=0; iC < state.countAcc; ++iC) {
			acc(Parallel::mpiMgr().getPRNG()->genGamma(shape, scale));
		}
	}

	for(size_t iC=0; iC < 1000; ++ iC) {
		accChange(state.rollingMeanAcc);
	}
}

} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
