//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BipartitionMonitor.h
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#ifndef BIPARTITION_BIPARTITIONMONITOR_H_
#define BIPARTITION_BIPARTITIONMONITOR_H_

#include <stddef.h>
#include <algorithm>
#include <boost/shared_ptr.hpp>

#include "DecisionTree.h"
#include "BipartitionMonitorState.h"
#include "Sampler/Samples/Sample.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/rolling_count.hpp>
#include <boost/accumulators/statistics/rolling_sum.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>
#include <boost/accumulators/statistics/rolling_variance.hpp>

namespace ParameterBlock {
	class BranchHelper;
}

namespace MolecularEvolution {
namespace TreeReconstruction {

	class Tree;
	class TreeNode;

namespace Bipartition {

using namespace boost::accumulators;

class BipartitionMonitor {
public:
	typedef boost::shared_ptr<BipartitionMonitor> sharedPtr_t;


	struct bipartitionFrequency_t {
		Tree::edge_t edge;
		split_t split;
		double freq;
	};

public:

	BipartitionMonitor(const std::vector<std::string> &aOrderedTaxaNames);
	BipartitionMonitor(const BipartitionMonitor& aBM);
	~BipartitionMonitor();

	void init(Tree::sharedPtr_t aTree);

	std::vector<size_t> registerTreeBipartitions(Tree::sharedPtr_t aTree, bool forceRegister);
	std::vector<size_t> registerTreeBipartitions(Tree::sharedPtr_t aTree, const Sampler::Sample &aSample, bool forceRegister);
	std::vector<bipartitionFrequency_t> defineBipartitionsFrequency(Tree::sharedPtr_t aTree);

	double defineNodeFrequency(Tree::sharedPtr_t aTree, TreeNode* aNode);
	double defineBipartitionFrequency(Tree::sharedPtr_t aTree, Tree::edge_t aEdge);

	double getSplitFrequency(split_t aSplit);
	std::string getSplitAsString(split_t aSplit);

	double computeTreeCCP(Tree::sharedPtr_t aTree);

	double getEpsilon(double minEpsilon, double maxEpsilon);
	double getPower(double maxPower);

	std::vector<size_t> getSplitsID(Tree::sharedPtr_t aTree);
	std::string getSplitsString() const;

	bool getSplitsStatsBL(split_t aSplit, size_t &count, double &mean, double &var);
	void saveSplitsAndStats(std::string baseName) const;

	void saveState(BipartitionMonitorState &state) const;
	void loadState(BipartitionMonitorState &state);

private:

	static const bool WITH_CPP;
	static const size_t ROLLING_WINDOWS_SIZE, CONVERGENCE_LIMIT;
	static const size_t UPDATE_STABLE_FREQ_PERIOD, RELAXATION_OFFSET;

	size_t freqIteration, currentIteration;
	size_t newSplitCount, timeSinceLastRelaxation;
	accumulator_set<double, stats< tag::rolling_count, tag::rolling_sum, tag::rolling_mean, tag::rolling_variance > > accSplitCount;

	DecisionTree dt, dtNode;

	ParameterBlock::BranchHelper* ptrBH;

	struct leaf_t {
		int id;
		std::string name;
		bool operator<(leaf_t const& rhs) const{
			return id < rhs.id;
		}
	};

	std::vector<leaf_t> leaves;
	std::map<size_t, size_t> leavesMapping;

	std::vector<std::string> orderedTaxaNames;

	void defineLeaves(Tree *aTree);

	std::vector<size_t> registerSplits(Tree *aTreePtr, bool forceRegister);
	std::vector<size_t> registerSplits(Tree *aTreePtr, const Sampler::Sample &aSample, bool forceRegister);
	std::vector<bipartitionFrequency_t> reportSplitsFrequencies(Tree *aTreePtr);

	void cleanDecisionTree();

	void updateNewSplitCount();
	bool doRequireRelaxation();
	void relaxFrequencies();

};


} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* BIPARTITION_BIPARTITIONMONITOR_H_ */
