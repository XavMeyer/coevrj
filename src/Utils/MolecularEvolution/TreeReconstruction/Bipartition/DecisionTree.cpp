//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DecisionTree.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include "DecisionTree.h"

#include <assert.h>
#include <stddef.h>

#include <boost/accumulators/framework/accumulator_set.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

DecisionTree::DecisionTree() {

	countNew = 0;

}

DecisionTree::~DecisionTree() {
	finalNodes.clear();
}

size_t DecisionTree::registerPartition(size_t currentIteration, const split_t &split) {

	// Start at root
	DecisionNode *cNode = &rootNode;
	cNode->lastVisit = currentIteration;

	// Left partition should always have the min element
	assert(!split[0]);

	// Go down in the tree
	bool createMissingNodes = true;
	for(size_t iP=0; iP<split.size(); ++iP) {
		bool isLeft = split[iP];
		cNode = nextNode(isLeft, cNode, createMissingNodes);
		cNode->lastVisit = currentIteration;
	}

	// Create final nodes if not yet existing (new partition)
	if(!cNode->final) {
		cNode->final.reset(new FinalNode());
		cNode->final->partition = split;
		finalNodes.push_back(cNode->final);
		countNew += 1;
	}
	cNode->final->count += 1. ;
	//std::cout << cNode->final->count << " / " << currentIteration << "\t" << cNode->final->getSplitAsString() << std::endl;

	return cNode->final->id;
}

size_t DecisionTree::registerPartition(size_t currentIteration, const split_t &split, double bl) {

	// Start at root
	DecisionNode *cNode = &rootNode;
	cNode->lastVisit = currentIteration;

	// Left partition should always have the min element
	assert(!split[0]);

	// Go down in the tree
	bool createMissingNodes = true;
	for(size_t iP=0; iP<split.size(); ++iP) {
		bool isLeft = split[iP];
		cNode = nextNode(isLeft, cNode, createMissingNodes);
		cNode->lastVisit = currentIteration;
	}

	// Create final nodes if not yet existing (new partition)
	if(!cNode->final) {
		cNode->final.reset(new FinalNode());
		cNode->final->partition = split;
		finalNodes.push_back(cNode->final);
		countNew += 1;
	}
	cNode->final->count += 1. ;
	cNode->final->acc(bl);

	return cNode->final->id;
}

double DecisionTree::getBipartitionFrequency(const split_t &split) {
	// Start at root
	DecisionNode *cNode = &rootNode;

	// Left partition should always have the min element
	assert(!split[0]);

	// Go down in the tree
	bool createMissingNodes = false;
	for(size_t iP=0; iP<split.size(); ++iP) {
		bool isLeft = split[iP];
		cNode = nextNode(isLeft, cNode, createMissingNodes);
		if(cNode == NULL) return -1.;
	}

	assert(cNode != NULL);
	assert(cNode->final != NULL);

	return cNode->final->getStableFreq();
}

bool DecisionTree::getStatsBL(const split_t &split, size_t &count ,double &mean, double &var) {
	// Start at root
	DecisionNode *cNode = &rootNode;

	// Left partition should always have the min element
	assert(!split[0]);

	// Go down in the tree
	bool createMissingNodes = false;
	for(size_t iP=0; iP<split.size(); ++iP) {
		bool isLeft = split[iP];
		cNode = nextNode(isLeft, cNode, createMissingNodes);
		if(cNode == NULL) return false;
	}

	assert(cNode != NULL);
	assert(cNode->final != NULL);
	count = cNode->final->getCountBL();
	mean = cNode->final->getMeanBL();
	var = cNode->final->getVarianceBL();
	if(var < 1.e-15 || count < 100) return false;
	//var = std::max(var, 1.e-4);
	return true;
}

size_t DecisionTree::getCountNew() const {
	return countNew;
}

void DecisionTree::resetCountNew() {
	countNew = 0;
}


const std::vector<FinalNode::sharedPtr_t>& DecisionTree::getFinalNodes() const {
	return finalNodes;
}

DecisionNode* DecisionTree::nextNode(const bool isLeft, DecisionNode *cNode, bool createMissingNodes) {
	if(isLeft) {
		if(cNode->left == NULL ) {
			if(createMissingNodes) {
				cNode->left = new DecisionNode();
			} else {
				return NULL;
			}
		}
		return cNode->left;
	} else {
		if(cNode->right == NULL) {
			if(createMissingNodes) {
				cNode->right = new DecisionNode();
			} else {
				return NULL;
			}
		}
		return cNode->right;
	}
}


void DecisionTree::pruneOldNodes(size_t limitIteration, size_t currentIteration, double frequencyThreshold) {

	recursivelyPruneNodes(limitIteration, currentIteration, frequencyThreshold, &rootNode);

}

void DecisionTree::relaxFrequencies(double k, size_t freqIteration, size_t currentIteration) {
	//std::cout << "k = " << k << std::endl;
	for(size_t iF=0; iF<finalNodes.size(); ++iF) {
		//std::cout << finalNodes[iF]->getFrequency(freqIteration) << std::scientific << "\t" << finalNodes[iF]->count << "\t" << k;
		finalNodes[iF]->count /= k;
		//std::cout << "\t" << finalNodes[iF]->count << "\t" << freqIteration;
		if(finalNodes[iF]->count == 0) {
			finalNodes[iF]->count += 1.0;
		}
		finalNodes[iF]->count = std::min((size_t)finalNodes[iF]->count, freqIteration);
		//std::cout << "\t" << finalNodes[iF]->getFrequency(freqIteration) << std::endl;
	}
	//std::cout << "---------------------------------------" << std::endl;
}

bool DecisionTree::recursivelyPruneNodes(size_t limitIteration, size_t currentIteration, double frequencyThreshold, DecisionNode *aNode) {

	bool canPrune = true;

	if(aNode->left != NULL) {
		bool isPruned = recursivelyPruneNodes(limitIteration, currentIteration, frequencyThreshold, aNode->left);
		if(isPruned) aNode->left = NULL;

		canPrune = canPrune && isPruned;
	}

	if(aNode->right != NULL) {
		bool isPruned = recursivelyPruneNodes(limitIteration, currentIteration, frequencyThreshold, aNode->right);
		if(isPruned) aNode->right = NULL;

		canPrune = canPrune && isPruned;
	}

	// The node is old... should we prune it ?
	if(aNode->lastVisit < limitIteration && canPrune) {
		// The node is has a final node -> check the frequency
		if(aNode->final != NULL) {
			canPrune = canPrune && aNode->final->getFrequency(currentIteration) < frequencyThreshold;

			if(canPrune) { // Remove the final node

				// Find the node in finalNodes
				std::vector<FinalNode::sharedPtr_t>::iterator it;
				for( it = finalNodes.begin(); it != finalNodes.end(); it++) {
					if((*it)->id == aNode->final->id) {
						break;
					}
				}
				assert(it != finalNodes.end());

				// Remove node from finalNodes
				finalNodes.erase(it);

				// Reset smart pointer
				aNode->final.reset();
			}

		}

		if(canPrune) { // remove the current node
			delete aNode;
		}

	}

	return canPrune;

}

size_t DecisionTree::getPartitionsNumber() const {
	return finalNodes.size();
}

void DecisionTree::updateStableFrequencies(size_t freqIteration) {
	for(size_t iF=0; iF<finalNodes.size(); ++iF) {
		finalNodes[iF]->setStableFreq(freqIteration);
	}
}

bool DecisionTree::areFrequenciesStable(size_t freqIteration) {
	double max = 0.;
	boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > acc;
	for(size_t iC=0; iC<finalNodes.size(); ++iC) {

		if(finalNodes[iC]->getStableFreq() > 0.05 && finalNodes[iC]->getStableFreq() < 0.99) {
			acc( fabs(finalNodes[iC]->getChangeAmplitude(freqIteration) ));
			max = std::max(fabs(finalNodes[iC]->getChangeAmplitude(freqIteration) ), max);
		}
	}

	bool stableMean = boost::accumulators::mean(acc) < 0.12;
	bool stableMax = max < 0.3;

	return stableMean && stableMax;
}

void DecisionTree::saveState(DecisionTreeState &state) const {

	state.countNew = countNew;
	for(size_t iF=0; iF<finalNodes.size(); ++iF) {
		FinalNodeState fnState;
		finalNodes[iF]->saveState(fnState);
		state.finalNodeState.push_back(fnState);
	}

}

void DecisionTree::loadState(size_t currentIteration, DecisionTreeState &state) {

	countNew = state.countNew;
	for(size_t iF=0; iF<state.finalNodeState.size(); ++iF) {
		split_t bipartition;
		for(size_t iP=0; iP<state.finalNodeState[iF].partitionStr.size(); ++iP) {
			bool isPresent = state.finalNodeState[iF].partitionStr.at(iP) == '*';
			bipartition.push_back(isPresent);
		}

		registerPartition(currentIteration, bipartition);
		finalNodes.back()->loadState(state.finalNodeState[iF]);
	}

}

} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
