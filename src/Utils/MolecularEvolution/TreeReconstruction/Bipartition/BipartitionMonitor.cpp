//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BipartitionMonitor.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */

#include "BipartitionMonitor.h"

#include "DecisionTreeState.h"
#include "Utils/Code/Algorithm.h" // IWYU pragma: keep
#include "Utils/Code/CustomProfiling.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeNode.h"
#include "ParameterBlock/BlockModifier/TreeModifier/BranchHelper.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

const bool BipartitionMonitor::WITH_CPP = false;
const size_t BipartitionMonitor::ROLLING_WINDOWS_SIZE = 25;
const size_t BipartitionMonitor::UPDATE_STABLE_FREQ_PERIOD = 10;
const size_t BipartitionMonitor::RELAXATION_OFFSET = 2000;
const size_t BipartitionMonitor::CONVERGENCE_LIMIT = 14000;


BipartitionMonitor::BipartitionMonitor(const std::vector<std::string> &aOrderedTaxaNames) :
		accSplitCount(tag::rolling_window::window_size = ROLLING_WINDOWS_SIZE), orderedTaxaNames(aOrderedTaxaNames) {

	newSplitCount = timeSinceLastRelaxation = 0;
	currentIteration = freqIteration = 1;
	ptrBH = NULL;
}


BipartitionMonitor::BipartitionMonitor(const BipartitionMonitor& aBM) :
		accSplitCount(tag::rolling_window::window_size = ROLLING_WINDOWS_SIZE), orderedTaxaNames(aBM.orderedTaxaNames) {

	newSplitCount = timeSinceLastRelaxation = 0;

	currentIteration = aBM.currentIteration;
	freqIteration = aBM.freqIteration;


	leaves = aBM.leaves;
	leavesMapping = aBM.leavesMapping;

	ptrBH = NULL;

}


BipartitionMonitor::~BipartitionMonitor() {

	if(ptrBH != NULL) {
		delete ptrBH;
		ptrBH = NULL;
	}

}

std::vector<size_t> BipartitionMonitor::registerTreeBipartitions(Tree::sharedPtr_t aTree, bool forceRegister) {

	std::vector<size_t> splitsId;

	if(forceRegister) {
		splitsId = registerSplits(aTree.get(), forceRegister);
	} else {
		if(currentIteration*50 > 2000 && freqIteration < CONVERGENCE_LIMIT) { // Start monitoring after 5K iterations
			splitsId = registerSplits(aTree.get(), forceRegister);

			freqIteration++;
			if(freqIteration == CONVERGENCE_LIMIT && Parallel::mpiMgr().isMainProcessor()) std::cout << "[BipartitionMonitor] Adaptive bipartition frequency 'converged' after " << currentIteration << " steps." << std::endl;
		}
	}

	currentIteration++;
	return splitsId;
}

std::vector<size_t> BipartitionMonitor::registerTreeBipartitions(Tree::sharedPtr_t aTree,  const Sampler::Sample &aSample, bool forceRegister) {

	std::vector<size_t> splitsId;

	if(forceRegister) {
		splitsId = registerSplits(aTree.get(), aSample, forceRegister);
	} else {
		if(currentIteration*50 > 2000 && freqIteration < CONVERGENCE_LIMIT) { // && currentIteration*25 < 500000) { // Start monitoring after 10k iterations
			splitsId = registerSplits(aTree.get(), aSample, forceRegister);

			freqIteration++;
			if(freqIteration == CONVERGENCE_LIMIT  && Parallel::mpiMgr().isMainProcessor()) std::cout << "[BipartitionMonitor] Adaptive bipartition frequency 'converged' after " << currentIteration << " steps." << std::endl;
		}
	}

	currentIteration++;
	return splitsId;
}



void BipartitionMonitor::init(Tree::sharedPtr_t aTree) {
	if(leaves.empty()) {
		defineLeaves(aTree.get());
	}

	ptrBH = new ParameterBlock::BranchHelper(aTree);

}

std::vector<BipartitionMonitor::bipartitionFrequency_t> BipartitionMonitor::defineBipartitionsFrequency(Tree::sharedPtr_t aTree) {

	return reportSplitsFrequencies(aTree.get());

}


std::vector<size_t> BipartitionMonitor::getSplitsID(Tree::sharedPtr_t aTree) {

	std::vector<size_t> splitsIds;


	return splitsIds;
}


std::string BipartitionMonitor::getSplitsString() const {
	std::stringstream ss;

	for(size_t iF=0; iF<dt.getFinalNodes().size(); ++iF) {
		ss << dt.getFinalNodes()[iF]->id << "\t" << dt.getFinalNodes()[iF]->getFrequency(currentIteration) << "\t" << dt.getFinalNodes()[iF]->getSplitAsString() << std::endl;
	}

	return ss.str();
}


bool BipartitionMonitor::getSplitsStatsBL(split_t aSplit, size_t &count, double &mean, double &var) {
	bool hasStats = dt.getStatsBL(aSplit, count, mean, var);
	return hasStats;
}


void BipartitionMonitor::saveSplitsAndStats(std::string baseName) const {
	std::stringstream ss1;//, ss2;
	ss1 << baseName << ".splitsStats";


	std::ofstream oFileStats(ss1.str().c_str(), std::ios::out);

	for(size_t iF=0; iF<dt.getFinalNodes().size(); ++iF) {


		oFileStats << dt.getFinalNodes()[iF]->getStatsString(freqIteration) << std::endl;

	}
}




double BipartitionMonitor::defineNodeFrequency(Tree::sharedPtr_t aTree, TreeNode* aNode) {

	assert(WITH_CPP);

	std::vector<Tree::edge_t> edges = aTree->getAllEdgeForBipartitions();
	std::vector<split_t> splits = aTree->getAllSplitsForBipartitions();

	std::vector<split_t> tmpSplits;

	for(size_t iE=0; iE < edges.size(); ++iE) {
		if(edges[iE].n1 == aNode || edges[iE].n2 == aNode) {
			tmpSplits.push_back(splits[iE]);
			if(tmpSplits.size() == 3) break;
		}
	}
	assert(tmpSplits.size() == 3);

	std::sort(tmpSplits.begin(), tmpSplits.end());

	split_t tripletSplit;
	for(size_t iS=0; iS<tmpSplits.size(); ++iS) {
		//tripletSplit.insert(tripletSplit.end(), tmpSplits[iS].begin(), tmpSplits[iS].end());
		for(size_t iA=0; iA<tmpSplits[iS].size(); ++iA) tripletSplit.push_back(tmpSplits[iS][iA]);
	}

	return dtNode.getBipartitionFrequency(tripletSplit);

}

double BipartitionMonitor::defineBipartitionFrequency(Tree::sharedPtr_t aTree, Tree::edge_t aEdge) {

	std::vector<double> splitsFrequencies;

	std::vector<BipartitionMonitor::bipartitionFrequency_t> vecBF = reportSplitsFrequencies(aTree.get());

	for(size_t iE=0; iE<vecBF.size(); ++iE) {
		if((aEdge.n1 == vecBF[iE].edge.n1 && aEdge.n2 == vecBF[iE].edge.n2) ||
		   (aEdge.n1 == vecBF[iE].edge.n2 && aEdge.n2 == vecBF[iE].edge.n1)) {
				 return splitsFrequencies[iE];
			 }
	}

	//assert(false && "Split frequency not found.");
	return -1.;

}

void BipartitionMonitor::defineLeaves(Tree* aTreePtr) {

	std::vector<TreeNode*> &nodes = aTreePtr->getTerminals();

	for(size_t iL=0; iL< nodes.size(); ++iL) {
		std::string treeNodeName;
		if(!nodes[iL]->isTerminal()) {
			treeNodeName = "";
		} else {
			assert(nodes[iL]->getIdName() < (long int)orderedTaxaNames.size());
			treeNodeName = orderedTaxaNames[nodes[iL]->getIdName()];
		}

		leaf_t leaf = {nodes[iL]->getIdName(), treeNodeName};
		leaves.push_back(leaf);
	}

	std::sort(leaves.begin(), leaves.end());

	for(size_t iL=0; iL< leaves.size(); ++iL) {
		leavesMapping[leaves[iL].id] = iL;
	}
}

double BipartitionMonitor::getSplitFrequency(split_t aSplit) {
	if(aSplit[0]) {
		aSplit.flip();
	}

	double freq = dt.getBipartitionFrequency(aSplit);
	//if(freq != -1.0) std::cout << freq << std::endl;
	if(freq < 0.0) freq = 0.0; // If the freq has never
	return freq;
}

std::string BipartitionMonitor::getSplitAsString(split_t aSplit) {
	if(aSplit[0]) aSplit.flip();
	std::stringstream ss;
	for(size_t i=0; i<aSplit.size(); ++i) {
		if(aSplit[i]) ss << "*";
		else ss << ".";
	}
	return ss.str();
}



double BipartitionMonitor::computeTreeCCP(Tree::sharedPtr_t aTree) {

	assert(WITH_CPP);

	std::vector<Tree::edge_t> edges = aTree->getAllEdgeForBipartitions();
	std::vector<split_t> splits = aTree->getAllSplitsForBipartitions();

	// For each nodes: request the tri-bipartition frequencies
	std::vector<double> nodeFreq;
	const std::vector<TreeNode*>& internalNodes = aTree->getInternals();
	for(size_t iN=0; iN<internalNodes.size(); ++iN) {

		std::vector<split_t> tmpSplits;

		for(size_t iE=0; iE < edges.size(); ++iE) {
			if(edges[iE].n1 == internalNodes[iN] || edges[iE].n2 == internalNodes[iN]) {
				tmpSplits.push_back(splits[iE]);
				if(tmpSplits.size() == 3) break;
			}
		}
		assert(tmpSplits.size() == 3);

		std::sort(tmpSplits.begin(), tmpSplits.end());

		split_t tripletSplit;
		for(size_t iS=0; iS<tmpSplits.size(); ++iS) {
			//tripletSplit.insert(tripletSplit.end(), tmpSplits[iS].begin(), tmpSplits[iS].end());
			for(size_t iA=0; iA<tmpSplits[iS].size(); ++iA) tripletSplit.push_back(tmpSplits[iS][iA]);
		}

		nodeFreq.push_back(dtNode.getBipartitionFrequency(tripletSplit));
		//std::cout << "Node freq : " << nodeFreq.back() << std::endl;
		//dtNode.registerPartition(currentIteration, tripletSplit, 1.0);
	}


	// For each edges: request the bipartition frequencies
	std::vector<BipartitionMonitor::bipartitionFrequency_t> bipartitionFreq = reportSplitsFrequencies(aTree.get());

	// How to deal with unknown freq ?
	double logTreeCCP = 0.0;

	for(size_t i=0; i<nodeFreq.size(); ++i) logTreeCCP += log(nodeFreq[i]);
	for(size_t i=0; i<bipartitionFreq.size(); ++i) logTreeCCP -= log(bipartitionFreq[i].freq);

	return exp(logTreeCCP);

}

double BipartitionMonitor::getEpsilon(double minEpsilon, double maxEpsilon) {

	if(rolling_count(accSplitCount) == 0.) {
		return maxEpsilon;
	}
	//double meanNewSplitsPerCall = rolling_mean(accSplitCount);
	double nsd = std::max(rolling_sum(accSplitCount)-3., 0.);
	if(!dt.areFrequenciesStable(freqIteration)) nsd += 1.0;

	double factor = 1.0;
	if(freqIteration > 2000.0) factor = std::max(0., 1.-(freqIteration-2000.0)/8000.0);
	//double nsd = newSplitDiff;//std::max(newSplitDiff-5., 0.);

	//std::cout << "Factor = " << factor << "nsd = " << nsd << std::endl;
	double epsilon = minEpsilon + factor*(0.2+std::min(nsd, 0.8))*(maxEpsilon-minEpsilon);
	//std::cout << minEpsilon << "\t" << epsilon << "\t" << nsd << "\t" <<  factor << std::endl;
	return epsilon;
}

double BipartitionMonitor::getPower(double maxPower) {

	if(rolling_count(accSplitCount) == 0.) {
		return 0.1;
	}


	double factor = 0.;

	double c = 10.0;
	double b = c/1.01;
	double a = c-b;

	if(freqIteration > 2000.0 && freqIteration <= 7000) {
		factor = std::min(1., (freqIteration-2000.0)/6000.0);

		factor *= 3.0;
		factor -= 1.;
		factor = pow(10.0, factor);

		b = c/(factor+1.);
		a = c-b;
	} else {
		factor = std::min(1., (5000.0)/6000.0);

		factor *= 3.0;
		factor -= 1.;
		factor = pow(10.0, factor);


		b = c/(factor+1);
		a = c-b;
	}


	double power = Parallel::mpiMgr().getPRNG()->genBeta(a,b);
	return maxPower*power;
}


void BipartitionMonitor::updateNewSplitCount() {

	// Get and clear the new split counter
	size_t newCount = dt.getCountNew();
	dt.resetCountNew();

	// Overall counter
	newSplitCount += newCount;

	// update accumulator
	accSplitCount((double)newCount);

	timeSinceLastRelaxation += 1;

}

bool BipartitionMonitor::doRequireRelaxation() {

	if (timeSinceLastRelaxation < ROLLING_WINDOWS_SIZE) return false; // Wait 10 split registration before re-relaxing
	//std::cout << "Freq it = " << freqIteration << " - " << std::scientific;

	size_t totalSplits = dt.getPartitionsNumber();

	double percentageNewSplits = 100.*(double)newSplitCount/(double)totalSplits;

	if(percentageNewSplits > 10.0) { // 15% pct new split -> we relax
		//std::cout << "Relaxation with percentage new split : " << percentageNewSplits << std::endl;
		return true;
	}

	double meanNewSplitsPerCall = rolling_mean(accSplitCount);
	double varNewSplitsPerCall = rolling_variance(accSplitCount);
	double relativeStdNewSplits = 100.0*sqrt(varNewSplitsPerCall)/meanNewSplitsPerCall;
	double percentageMeanNewSplits = 100.*rolling_mean(accSplitCount)/(double)totalSplits;

	if(rolling_sum(accSplitCount) >= 5.0 && percentageMeanNewSplits < 1.0 && relativeStdNewSplits > 350.0) { // Low change during last 10 but high variance
		return true;
	}

	if(timeSinceLastRelaxation >= 20*ROLLING_WINDOWS_SIZE && (timeSinceLastRelaxation % 20*ROLLING_WINDOWS_SIZE) == 0) {
		bool areFreqStable = dt.areFrequenciesStable(freqIteration);
		if(!areFreqStable) {
			return true;
		}/* else {
			//std::cout << "Freq are stable." << std::endl;
		}*/
	}

	return false;
}

void BipartitionMonitor::relaxFrequencies() {

	double newVal = std::max(0.9*freqIteration, (double)RELAXATION_OFFSET);
	double k = std::max(1., freqIteration/newVal);

	if( k == 1.0 ) return;

	freqIteration = freqIteration / k;
	dt.relaxFrequencies(k, freqIteration, currentIteration);
	if(WITH_CPP) {
		dtNode.relaxFrequencies(k, freqIteration, currentIteration);
	}

	// Reset what need be
	timeSinceLastRelaxation = 0;
	newSplitCount = 0;
}


std::vector<size_t> BipartitionMonitor::registerSplits(Tree *aTreePtr, bool forceRegister) {


	std::vector<size_t> splitsId;
	std::vector<Tree::edge_t> edges = aTreePtr->getAllEdgeForBipartitions();
	std::vector<split_t> splits = aTreePtr->getAllSplitsForBipartitions();

	for(size_t iS=0; iS<splits.size(); ++iS) {
		splitsId.push_back(dt.registerPartition(currentIteration, splits[iS]));
	}

	if(freqIteration % UPDATE_STABLE_FREQ_PERIOD == 0 && freqIteration < CONVERGENCE_LIMIT) {
		dt.updateStableFrequencies(freqIteration); // Update the stable frequency counter
	}

	if(!forceRegister  && freqIteration > RELAXATION_OFFSET) {
		updateNewSplitCount();
		if(doRequireRelaxation()) relaxFrequencies();
	}

	return splitsId;
}

std::vector<size_t> BipartitionMonitor::registerSplits(Tree *aTreePtr, const Sampler::Sample &aSample, bool forceRegister) {

	std::vector<size_t> splitsId;
	std::vector<Tree::edge_t> edges = aTreePtr->getAllEdgeForBipartitions();
	std::vector<split_t> splits = aTreePtr->getAllSplitsForBipartitions();

	for(size_t iS=0; iS<splits.size(); ++iS) {
		double aBl = ptrBH->getBranchLength(edges[iS].n1, edges[iS].n2, aSample);
		splitsId.push_back(dt.registerPartition(currentIteration, splits[iS], aBl));
	}

	if(WITH_CPP) {
		/* This counts the frequencies of "nodes" (tri-biparitions) */
		const std::vector<TreeNode*>& internalNodes = aTreePtr->getInternals();
		for(size_t iN=0; iN<internalNodes.size(); ++iN) {

			std::vector<split_t> tmpSplits;

			for(size_t iE=0; iE < edges.size(); ++iE) {
				if(edges[iE].n1 == internalNodes[iN] || edges[iE].n2 == internalNodes[iN]) {
					tmpSplits.push_back(splits[iE]);
					if(tmpSplits.size() == 3) break;
				}
			}
			assert(tmpSplits.size() == 3);

			std::sort(tmpSplits.begin(), tmpSplits.end());

			split_t tripletSplit;
			for(size_t iS=0; iS<tmpSplits.size(); ++iS) {
				//tripletSplit.insert(tripletSplit.end(), tmpSplits[iS].begin(), tmpSplits[iS].end());
				for(size_t iA=0; iA<tmpSplits[iS].size(); ++iA) tripletSplit.push_back(tmpSplits[iS][iA]);
			}

			dtNode.registerPartition(currentIteration, tripletSplit, 1.0);
		}
	}

	if(freqIteration % UPDATE_STABLE_FREQ_PERIOD  == 0 && freqIteration < CONVERGENCE_LIMIT) {
		dt.updateStableFrequencies(freqIteration); // Update the stable frequency counter
	}

	if(!forceRegister && freqIteration > RELAXATION_OFFSET) {
		updateNewSplitCount();
		if(doRequireRelaxation()) relaxFrequencies();
	}

	return splitsId;
}



std::vector<BipartitionMonitor::bipartitionFrequency_t> BipartitionMonitor::reportSplitsFrequencies(Tree *aTreePtr) {

	std::vector<Tree::edge_t> intEdge = aTreePtr->getInternalEdgeForBipartitions();
	std::vector<split_t> splits = aTreePtr->getInternalSplitsForBipartitions();

	assert(intEdge.size() == aTreePtr->getInternalEdges().size());

	std::vector<BipartitionMonitor::bipartitionFrequency_t> vecBF;
	for(size_t iE = 0; iE<intEdge.size(); ++iE) {
		bipartitionFrequency_t bf;
		bf.edge = intEdge[iE];
		bf.split = splits[iE];
		if(currentIteration*50 > 2000) { // start informing after 10k samples
			bf.freq = getSplitFrequency(bf.split);//dt.getBipartitionFrequency(freqIteration, bf.split);
		} else {
			bf.freq = 0.;
		}

		vecBF.push_back(bf);
	}

	return vecBF;
}

void BipartitionMonitor::cleanDecisionTree() {

	size_t limitIteration = currentIteration > 25000 ? currentIteration - 10000 : 0;
	double limitFrequency = 0.05;


	dt.pruneOldNodes(limitIteration, currentIteration, limitFrequency);
}

void BipartitionMonitor::saveState(BipartitionMonitorState &state) const{

	state.freqIteration = freqIteration;
	state.currentIteration = currentIteration;
	state.newSplitCount = newSplitCount;
	state.timeSinceLastRelaxation = timeSinceLastRelaxation;

	state.count = boost::accumulators::rolling_count(accSplitCount);
	state.sum = boost::accumulators::rolling_sum(accSplitCount);
	state.mean = boost::accumulators::rolling_mean(accSplitCount);
	state.variance = boost::accumulators::rolling_variance(accSplitCount);

	//dt.pruneOldNodes(0.2*currentIteration, currentIteration, 5.e-3);
	dt.saveState(state.dtState);

	//dtNode.pruneOldNodes(0.2*currentIteration, currentIteration, 5.e-3);
	dtNode.saveState(state.dtNodeState);

}

void BipartitionMonitor::loadState(BipartitionMonitorState &state) {

	freqIteration = state.freqIteration;
	currentIteration = state.currentIteration;
	newSplitCount = state.newSplitCount;
	timeSinceLastRelaxation = state.timeSinceLastRelaxation;


	for(size_t iC=0; iC < state.count; ++iC) {
		double val = Parallel::mpiMgr().getPRNG()->genNormal(state.mean, state.variance);
		accSplitCount(val);
	}

	dt.loadState(currentIteration, state.dtState);
	dtNode.loadState(currentIteration, state.dtNodeState);

}


} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
