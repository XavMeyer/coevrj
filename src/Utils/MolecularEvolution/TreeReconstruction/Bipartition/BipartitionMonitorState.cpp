//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BipartitionMonitorState.cpp
 *
 *  Created on: Aug 13, 2019
 *      Author: xaviermeyer
 */

#include "BipartitionMonitorState.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

BipartitionMonitorState::BipartitionMonitorState() {
	freqIteration = currentIteration = 0;
	newSplitCount = timeSinceLastRelaxation = 0;

	count = 0.;
	sum = mean = variance = 0.0;
}

BipartitionMonitorState::~BipartitionMonitorState() {
}


template<class Archive>
void BipartitionMonitorState::serialize(Archive & ar, const unsigned int version)  {

	ar & BOOST_SERIALIZATION_NVP(freqIteration);
	ar & BOOST_SERIALIZATION_NVP(currentIteration);
	ar & BOOST_SERIALIZATION_NVP(newSplitCount);
	ar & BOOST_SERIALIZATION_NVP(timeSinceLastRelaxation);

	ar & BOOST_SERIALIZATION_NVP(count);
	ar & BOOST_SERIALIZATION_NVP(sum);
	ar & BOOST_SERIALIZATION_NVP(mean);
	ar & BOOST_SERIALIZATION_NVP(variance);

	ar & BOOST_SERIALIZATION_NVP(dtState);
	ar & BOOST_SERIALIZATION_NVP(dtNodeState);
}

template void BipartitionMonitorState::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version);
template void BipartitionMonitorState::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void BipartitionMonitorState::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version);
template void BipartitionMonitorState::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
