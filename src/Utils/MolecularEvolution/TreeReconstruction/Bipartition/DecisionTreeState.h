//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * DecisionTreeState.h
 *
 *  Created on: Aug 13, 2019
 *      Author: xaviermeyer
 */

#ifndef UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_BIPARTITION_DECISIONTREESTATE_H_
#define UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_BIPARTITION_DECISIONTREESTATE_H_

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep
#include "Utils/MolecularEvolution/TreeReconstruction/Bipartition/FinalNodeState.h"

#include <boost/dynamic_bitset/dynamic_bitset.hpp>
#include <vector>

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

class DecisionTreeState {
public:
	DecisionTreeState();
	~DecisionTreeState();

private:

	size_t countNew;
	std::vector<FinalNodeState> finalNodeState;

	// Boost serialization
	friend class DecisionTree;
	friend class boost::serialization::access;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

};

} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_BIPARTITION_DECISIONTREESTATE_H_ */
