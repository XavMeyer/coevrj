//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DecisionNode.h
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#ifndef BIPARTITION_DECISIONNODE_H_
#define BIPARTITION_DECISIONNODE_H_

#include "FinalNode.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

class DecisionNode {
public:
	DecisionNode() : lastVisit(0), left(NULL), right(NULL) {}
	~DecisionNode() {
		if(left != NULL) delete left;
		if(right != NULL) delete right;
	}
public:
	FinalNode::sharedPtr_t final;
	size_t lastVisit;
	DecisionNode *left, *right;
};

} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* BIPARTITION_DECISIONNODE_H_ */
