//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DecisionTree.h
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#ifndef BIPARTITION_DECISIONTREE_TREERECONSTRUCTION_H_
#define BIPARTITION_DECISIONTREE_TREERECONSTRUCTION_H_

#include <boost/functional/hash.hpp>
#include <boost/shared_ptr.hpp>

#include <algorithm>
#include <iostream>
#include <vector>

#include "DecisionNode.h"
#include "FinalNode.h"
#include "DecisionTreeState.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

class DecisionTree {
public:
	DecisionTree();
	~DecisionTree();

	size_t registerPartition(size_t currentIteration, const split_t &partition);
	size_t registerPartition(size_t currentIteration, const split_t &partition, double bl);
	double getBipartitionFrequency(const split_t &split);

	const std::vector<FinalNode::sharedPtr_t>& getFinalNodes() const;

	void pruneOldNodes(size_t limitIteration, size_t currentIteration, double frequencyThreshold);

	void relaxFrequencies(double k, size_t freqIteration, size_t currentIteration);

	bool getStatsBL(const split_t &split, size_t &count, double &mean, double &var);

	size_t getCountNew() const;
	void resetCountNew();

	size_t getPartitionsNumber() const;

	void updateStableFrequencies(size_t freqIteration);
	bool areFrequenciesStable(size_t freqIteration);

	void saveState(DecisionTreeState &state) const;
	void loadState(size_t currentIteration, DecisionTreeState &state);

private:

	size_t countNew;

	DecisionNode rootNode;
	std::vector<FinalNode::sharedPtr_t> finalNodes;

	DecisionNode* nextNode(const bool isLeft, DecisionNode *cNode, bool createMissingNodes);

	bool recursivelyPruneNodes(size_t limitIteration, size_t currentIteration, double frequencyThreshold, DecisionNode *aNode);

};


} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* BIPARTITION_DECISIONTREE_TREERECONSTRUCTION_H_ */
