//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BipartitionMonitorState.h
 *
 *  Created on: Aug 13, 2019
 *      Author: xaviermeyer
 */

#ifndef UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_BIPARTITION_BIPARTITIONMONITORSTATE_H_
#define UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_BIPARTITION_BIPARTITIONMONITORSTATE_H_

#include "DecisionTreeState.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

class BipartitionMonitorState {
public:
	BipartitionMonitorState();
	~BipartitionMonitorState();

private:

	size_t freqIteration, currentIteration;
	size_t newSplitCount, timeSinceLastRelaxation;

	size_t count;
	double sum, mean, variance;

	DecisionTreeState dtState, dtNodeState;

	// Boost serialization
	friend class BipartitionMonitor;
	friend class boost::serialization::access;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);



};

} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_TREERECONSTRUCTION_BIPARTITION_BIPARTITIONMONITORSTATE_H_ */
