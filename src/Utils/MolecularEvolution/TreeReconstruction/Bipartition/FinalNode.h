//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FinalNode.h
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#ifndef BIPARTITION_FINALNODE_H_
#define BIPARTITION_FINALNODE_H_

#include "Eigen/Core"
#include "Eigen/Dense"
#include "Utils/Code/IDManagement/IDManager.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Bipartition/FinalNodeState.h"

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <algorithm>
#include <vector>
#include <iostream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>
#include <boost/dynamic_bitset/dynamic_bitset.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

using namespace boost::accumulators;

typedef boost::dynamic_bitset<> split_t;

class FinalNode {
public:
	typedef boost::shared_ptr<FinalNode> sharedPtr_t;
public:
	FinalNode();
	~FinalNode();

	double getFrequency(size_t currentIteration) const;

	std::string getSplitAsString() const;

	std::string getStatsString(size_t currentIteration) const;

	size_t getCountBL() const;
	double getMeanBL() const;
	double getVarianceBL() const;

	void setStableFreq(size_t currentIteration);
	double getChangeAmplitude(size_t currentIteration);
	double getStableFreq() const;

public:

	static Utils::IDManagement::IDManager idManager;

	size_t id;
	split_t partition;
	double count, stableFreq;

	accumulator_set<double, stats< tag::count, tag::mean, tag::variance > > acc;
	accumulator_set<double, stats< tag::rolling_mean > > accChange;

	void saveState(FinalNodeState &state) const;
	void loadState(FinalNodeState &state);
};

} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* BIPARTITION_FINALNODE_H_ */
