//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * FinalNodeState.cpp
 *
 *  Created on: Aug 13, 2019
 *      Author: xaviermeyer
 */

#include "FinalNodeState.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Bipartition {

FinalNodeState::FinalNodeState() {
	id = 0;

	count = 0;
	stableFreq = 0;

	countAcc = meanAcc = varAcc = rollingMeanAcc = 0.;

}

FinalNodeState::~FinalNodeState() {
}

template<class Archive>
void FinalNodeState::serialize(Archive & ar, const unsigned int version)  {

	ar & BOOST_SERIALIZATION_NVP(id);
	ar & BOOST_SERIALIZATION_NVP(partitionStr);

	ar & BOOST_SERIALIZATION_NVP(count);
	ar & BOOST_SERIALIZATION_NVP(stableFreq);

	ar & BOOST_SERIALIZATION_NVP(countAcc);
	ar & BOOST_SERIALIZATION_NVP(meanAcc);
	ar & BOOST_SERIALIZATION_NVP(varAcc);

	ar & BOOST_SERIALIZATION_NVP(rollingMeanAcc);
}

template void FinalNodeState::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version);
template void FinalNodeState::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void FinalNodeState::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version);
template void FinalNodeState::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Bipartition */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
