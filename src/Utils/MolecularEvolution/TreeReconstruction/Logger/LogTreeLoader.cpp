//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogTreeLoader.cpp
 *
 * @date Mai 14, 2017
 * @author meyerx
 * @brief
 */

#include "LogTreeLoader.h"

#include <assert.h>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <utility>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/detail/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include "Parallel/Manager/MpiManager.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

LogTreeLoader::LogTreeLoader(const std::string &fnPrefix, const std::string &fnSuffix) {

	std::stringstream ssFName;
	ssFName << fnPrefix << "_" << Parallel::mpiMgr().getRank() << fnSuffix << ".logTrees";
	treeStringFN = ssFName.str();

	burninIteration = 0;
	burninRatio = 0.;
}

LogTreeLoader::~LogTreeLoader() {
}

const std::vector<std::string>& LogTreeLoader::getOrderedNames() const {
	return orderedTaxaNames;
}

std::string LogTreeLoader::getIdString(long int aTreeIdx) const {
	itConstTreeStringMap_t it = treeStringMap.find(aTreeIdx);
	assert(it != treeStringMap.end());
	return it->second;
}

void LogTreeLoader::setBurninIteration(size_t aBurninIteration) {
	burninIteration = aBurninIteration;
}

void LogTreeLoader::setBurninRatio(double aBurninRatio) {
	burninRatio = aBurninRatio;
}

bool LogTreeLoader::summarizeTrees(std::vector<std::string> &idStrSummary,
								   std::map<long int, size_t> &mapping) {
	assert(Parallel::mpiMgr().isMainProcessor());

	if(!loadTreeStringMap()) return false;

	size_t idx=0;
	std::map<std::string, size_t> stringToIdx;

	// For all existing trees
	for(itConstTreeStringMap_t itT = treeStringMap.begin(); itT != treeStringMap.end(); ++itT) {
		// Defines
		std::map<std::string, size_t>::iterator itS;
		size_t idMapping = 0;
		// Order the tree
		std::string idTreeString = itT->second;
		// Is this tree already summarized ?
		itS = stringToIdx.find(idTreeString);
		if(itS == stringToIdx.end()) { // No, we add it
			stringToIdx.insert(std::make_pair(idTreeString, idx));
			idStrSummary.push_back(idTreeString);
			idMapping = idx;
			idx++;
		} else { // Yes we get his id
			idMapping = itS->second;
		}

		// We map the hash and the string
		mapping[itT->first] = idMapping;
	}

	return true;
}


bool LogTreeLoader::loadTreeStringMap() {

	std::string line;
	std::ifstream iFile(treeStringFN.c_str());

	if(!iFile.is_open()) return false;

	// First line is the ordered taxa name
	std::getline(iFile, line);
	boost::split(orderedTaxaNames, line, boost::is_any_of("\t"));

	// For each line
	while(std::getline(iFile, line)) { // Get the ID
		long int tmpH = atol(line.c_str());
		// Get the tree string
		std::getline(iFile, line);
		treeStringMap[tmpH] = line;
	}
	iFile.close();

	return true;
}

const std::string& LogTreeLoader::getTreeStringFN() const {
	return treeStringFN;
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */




