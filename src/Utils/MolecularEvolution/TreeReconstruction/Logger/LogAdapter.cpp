//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogAdapter.cpp
 *
 * @date Nov 12, 2015
 * @author meyerx
 * @brief
 */

#include "LogAdapter.h"

#include "Sampler/Information/OutputManager.h"
#include "Utils/Code/Algorithm.h" // IWYU pragma: keep
#include "Utils/MolecularEvolution/TreeReconstruction/Logger/SplitLogger.h"
#include "Model/Likelihood/CoevRJ/Base.h"

#include <assert.h>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <boost/algorithm/string/split.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <string>


namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Logger {

LogAdapter::LogAdapter(Sampler::TraceWriter::BaseWriter::sharedPtr_t aPtrWriter) :
	ptrWriter(aPtrWriter), logTreeLoader(ptrWriter->getFNPrefix(), ptrWriter->getFNSuffix()) {
	burninIteration = 0;
	burninRatio = 0.;
	nLine = 0;

	std::stringstream ss;
	ss << Sampler::Information::outputManager().getBaseFileName() << ".trees";
	std::ofstream oFile(ss.str().c_str());
}

LogAdapter::~LogAdapter() {
}

void LogAdapter::summarizeUniqueTrees(const std::vector<std::string> &idStrTrees, const std::map<long int, size_t> &mapping,
									  std::vector<std::string> &uniqueStrTrees, std::map<long int, mappingBranch_t> &uniqueMapping,
									  std::vector<Tree::sharedPtr_t> &uniqueTrees) {

	// For all existing trees
	for(std::map<long int, size_t>::const_iterator itT = mapping.begin(); itT != mapping.end(); ++itT) {

		// Load tree to get unique newick string
		size_t idTree = itT->second;
		Tree::sharedPtr_t aTree(new Tree(idStrTrees[idTree]));
		std::string nameTreeString = aTree->getNameString(logTreeLoader.getOrderedNames());

		std::vector<size_t> orderedIdBL = aTree->getIdBLInTraversalOrder();
		mappingBranch_t mapB;
		mapB.mappingBranch.resize(orderedIdBL.size());
		for(size_t iB=0; iB < mapB.mappingBranch.size(); ++iB) {
			//std::cout << orderedIdBL[iB] << "\t" << iB << std::endl;
			mapB.mappingBranch[orderedIdBL[iB]] = iB;
		}

		// Search if this tree already exists
		size_t pos = std::numeric_limits<size_t>::max();
		for(size_t iS=0; iS<uniqueStrTrees.size(); ++iS) {
			if(uniqueStrTrees[iS] == nameTreeString) {
				pos = iS;
				break;
			}
		}

		if(pos < std::numeric_limits<size_t>::max()) { // it exists
			mapB.id = pos;
			uniqueMapping.insert(std::make_pair(itT->first, mapB));
		} else { // it doesn't
			uniqueStrTrees.push_back(nameTreeString);
			mapB.id = uniqueStrTrees.size()-1;
			uniqueMapping.insert(std::make_pair(itT->first, mapB));
			uniqueTrees.push_back(aTree);
		}
	}
}

LogAdapter::resultCount_t LogAdapter::countTreeAndBLengthTracerLog(std::vector<std::string> &idStrTrees, std::map<long int, size_t> &mapping,
																   std::vector<std::string> &uniqueStrTrees, std::map<long int, mappingBranch_t> &uniqueMapping) {
	LogAdapter::resultCount_t resCount;
	resCount.countTree.assign(uniqueStrTrees.size(), 0);
	resCount.branchLengths.resize(uniqueStrTrees.size());

	std::string line;
	std::stringstream ssIn;

	ssIn << Sampler::Information::outputManager().getBaseFileName() << ".log";
	std::ifstream iFile(ssIn.str().c_str());

	// Counting number of line in file for the burn-in
	std::getline(iFile, line); // Trash headers

	// For each line
	nLine = 0;
	while(std::getline(iFile, line)) {
		nLine++;
	}

	// Go back to the start of the file
	iFile.clear();
	iFile.seekg(0, ios::beg);

	// Copy header
	std::getline(iFile, line);

	// Get tree hash position
	size_t posTH = getTreeHashPosition(line);
	// Get the branch length positions and init the accumulators
	std::vector<size_t> posBL = getBranchLengthPosition(line);
	for(size_t iT=0; iT<uniqueStrTrees.size(); ++iT) resCount.branchLengths[iT].resize(posBL.size());

	// Define the burnin
	size_t burninRatioToIter = burninRatio*nLine;
	const size_t BURN_IN_OFFSET =  std::max(burninIteration, burninRatioToIter);
	size_t iLine=0;
	while (std::getline(iFile, line)) {
		if(iLine < BURN_IN_OFFSET) {
			iLine++;
			continue;
		}

		// Correct the tree thingy
		// Get words
		std::vector<std::string> words;
		boost::split(words, line, boost::is_any_of("\t"));

		// Convert from hash to idx
		assert(words.size() > posTH);
		long int hash = atol(words[posTH].c_str()); // get hash
		mappingBranch_t mapB = uniqueMapping[hash];
		size_t iTree = mapB.id;
		resCount.countTree[iTree]++;
		//std::cout << iTree << "\t" <<  uniqueStrTrees[iTree] << "\t" << resCount.countTree[iTree] << std::endl;

		//std::cout << "sizes : " << posBL.size() << "\t" << mapB.mappingBranch.size() << std::endl;
		for(size_t iB=0; iB<posBL.size(); ++iB) {
			size_t idBL = mapB.mappingBranch[iB];
			double val = atof(words[posBL[iB]].c_str());
			//std::cout << iB << "\t" << idBL << std::endl;
			resCount.branchLengths[iTree][idBL](val);
		}

		iLine++;
	}

	return resCount;
}


bool LogAdapter::adaptLog(bool withSplitFrequencies) {

	const bool isBinaryWriter = false;

	std::vector<std::string> idStrTrees;
	std::map<long int, size_t> mapping;
	// Summarize tree by idTree
	bool successful = logTreeLoader.summarizeTrees(idStrTrees, mapping);
	if(!successful) return false;

	// Summarize tree by uniqueTree
	std::vector<std::string> uniqueStTrees;
	std::map<long int, mappingBranch_t> uniqueMapping;
	std::vector<Tree::sharedPtr_t> uniqueTrees;
	summarizeUniqueTrees(idStrTrees, mapping, uniqueStTrees, uniqueMapping, uniqueTrees);
	//std::cout << "Id tree size = " << idStrTrees.size() << "\t -- \t Unique tree nb = " << uniqueStTrees.size() << std::endl;

	// Count tree appearance
	resultCount_t resCount;
	if(isBinaryWriter) {
		resCount = countTreeAndBLengthBinaryLog(idStrTrees, mapping);
	} else {
		resCount = countTreeAndBLengthTracerLog(idStrTrees, mapping, uniqueStTrees, uniqueMapping);
	}

	// Order trees in function of appearance
	bool ascending = false;
	std::vector<size_t> orderedId = Utils::Algorithm::getOrderedIndices(resCount.countTree, ascending);
	std::map<size_t, size_t> mappingOrderedId = Utils::Algorithm::getOrderedIndicesMapping(resCount.countTree, ascending);

	// Write trees (sorted by descending count)
	writeTrees(uniqueTrees, orderedId, resCount);

	// Adapt log file
	std::vector<long int> hashSeq;
	if(isBinaryWriter) {
		hashSeq = adaptBinaryLog();
	} else {
		hashSeq = adaptTracerLog(uniqueMapping, mappingOrderedId);
	}

	if(withSplitFrequencies) {
		assert(nLine != 0);
		double burninIterRatio = (double)burninIteration/(double)nLine;
		const double BURN_IN_RATIO = std::max(burninIterRatio, burninRatio);
		SplitLogger sl(BURN_IN_RATIO, 0.01, logTreeLoader, ptrWriter->getFileBaseName());
		sl.processTreeSeq(hashSeq);
	}

	return true;
}



void LogAdapter::setBurninIteration(size_t aBurninIteration) {
	burninIteration = aBurninIteration;
	logTreeLoader.setBurninIteration(aBurninIteration);
}

void LogAdapter::setBurninRatio(double aBurninRatio) {
	burninRatio = aBurninRatio;
	logTreeLoader.setBurninRatio(aBurninRatio);
}



LogAdapter::resultCount_t LogAdapter::countTreeAndBLengthBinaryLog(std::vector<std::string> &idStrTrees, std::map<long int, size_t> &mapping) {
	// TODO Implement.
	std::cerr << "Not yet implemented." << std::endl;
	abort();
}

std::vector<long int> LogAdapter::adaptTracerLog(std::map<long int, mappingBranch_t> &uniqueMapping, std::map<size_t, size_t> &mappingOrderedId) {
	std::string line;
	std::stringstream ssIn, ssOut;
	std::vector<long int> hashSeq;

	ssIn << Sampler::Information::outputManager().getBaseFileName() << ".log";
	std::ifstream iFile(ssIn.str().c_str());
	ssOut << Sampler::Information::outputManager().getBaseFileName() << "_Corr.log";
	std::ofstream oFile(ssOut.str().c_str());

	// Copy header
	std::getline(iFile, line);
	oFile << line << std::endl;

	// Get tree hash position
	size_t posTH = getTreeHashPosition(line);

	while (std::getline(iFile, line)) {
		// Correct the tree thingy
		// Get words
		std::vector<std::string> words;
		boost::split(words, line, boost::is_any_of("\t"));

		// Convert from hash to idx
		assert(words.size() > posTH);
		long int hash = atol(words[posTH].c_str()); // get hash
		hashSeq.push_back(hash);
		mappingBranch_t mapB =  uniqueMapping[hash]; // get idx
		size_t sortedIdx = mappingOrderedId[mapB.id];

		std::stringstream ss;
		ss << sortedIdx;
		words[posTH] = ss.str(); // set new value

		// Rewrite
		for(size_t iW=0; iW<words.size()-1; ++iW) {
			oFile << words[iW] << "\t";
		}
		oFile << words.back() << std::endl;
	}

	return hashSeq;
}

std::vector<long int> LogAdapter::adaptBinaryLog() {
	std::vector<long int> hashSeq;
	// TODO Implement.
	std::cerr << "Not yet implemented." << std::endl;
	abort();
	return hashSeq;
}

void LogAdapter::writeTrees(std::vector<Tree::sharedPtr_t> &uniqueTrees, std::vector<size_t> &orderedId, resultCount_t &resCount) const {
	std::stringstream ss;
	ss << Sampler::Information::outputManager().getBaseFileName() << ".trees";
	std::ofstream oFile(ss.str().c_str());
	//oFile.fill(' ');
	oFile.precision(3);

	// Get the total count
	size_t sum = 0;
	for(size_t i=0; i<resCount.countTree.size(); ++i) {
		sum += resCount.countTree[i];
	}

	// Summarize the average branch length
	std::vector< std::vector<double> > averageBranchLengths;
	for(size_t iT=0; iT<resCount.branchLengths.size(); ++iT) {
		std::vector<double> avgBL(resCount.branchLengths[iT].size());
		for(size_t iB=0; iB<resCount.branchLengths[iT].size(); ++iB) {
			avgBL[iB] = boost::accumulators::mean(resCount.branchLengths[iT][iB]);
		}
		averageBranchLengths.push_back(avgBL);
	}

	// For each tree, using the ordereding
	for(size_t iT=0; iT<orderedId.size(); ++iT) {
		size_t idTree = orderedId[iT];
		//std::cout << resCount.countTree[idTree] << "\t" << uniqueTrees[idTree]->getNameString(logTreeLoader.getOrderedNames()) << std::endl; // FIXME DEBUG
		if(resCount.countTree[idTree] == 0) break;

		// Create the tree string with taxa name and branch length
		//std::string nameTreeString = aTree->getNameString(logTreeLoader.getOrderedNames());
		//std::string niceTreeString = aTree->getNameAndBranchLengthString(logTreeLoader.getOrderedNames(), averageBranchLengths[idTree]);
		std::string theString = uniqueTrees[idTree]->getNameAndBranchLengthStringForLogAdpt(logTreeLoader.getOrderedNames(), averageBranchLengths[idTree]);

		oFile  << iT << "\t" << 100.*((double)resCount.countTree[idTree]/(double)sum) << "\t" << theString << std::endl;

		/*std::cout << "NB edge : " << averageBranchLengths[idTree].size() << std::endl; // FIXME DEBUG
		std::cout << idStrTrees[idTree] << std::endl; // FIXME DEBUG
		std::cout << niceTreeString << std::endl; // FIXME DEBUG
		std::cout << "----------------------------------------------------------" << std::endl; // FIXME DEBUG*/
	}
}

size_t LogAdapter::getTreeHashPosition(std::string &aLine) const {
	std::vector<std::string> words;
	boost::split(words, aLine, boost::is_any_of("\t"));

	std::vector<std::string>::iterator it = std::find(words.begin(), words.end(),
			StatisticalModel::Likelihood::CoevRJ::Base::NAME_TREE_PARAMETER);
	size_t index = std::distance(words.begin(), it);

	if(index == words.size()) {
		std::cerr << "[ERROR] in size_t LogAdapter::getTreeHashPosition(std::string &aLine) const;" << std::endl;
		std::cerr << "Could not find parameter '" << StatisticalModel::Likelihood::CoevRJ::Base::NAME_TREE_PARAMETER << "'" << std::endl;
		abort();
	}

	return index;
}

std::vector<size_t> LogAdapter::getBranchLengthPosition(std::string &aLine) const {
	std::vector<size_t> indices;

	std::vector<std::string> words;
	boost::split(words, aLine, boost::is_any_of("\t"));

	for(size_t iW=0; iW<words.size(); ++iW) {
		if(words[iW].size() > 3 && words[iW].substr(0, 3) == "BL_") {
			size_t idBL = atol(words[iW].substr(3, std::string::npos).c_str());
			if(indices.size() < idBL+1) indices.resize(idBL+1);
			indices[idBL] = iW;
		}
	}

	return indices;
}

} /* namespace Logger */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
