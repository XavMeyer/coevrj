//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogTreeLoader.h
 *
 * @date Mai 14, 2017
 * @author meyerx
 * @brief
 */
#ifndef LOGTREELOADER
#define LOGTREELOADER

#include <boost/shared_ptr.hpp>

#include <stddef.h>
#include <map>
#include <string>
#include <vector>

namespace Parallel { class MCMCManager; }

namespace MolecularEvolution {
namespace TreeReconstruction {

class LogTreeLoader {
public:
	typedef boost::shared_ptr<LogTreeLoader> sharedPtr_t;
	typedef std::map<std::string, int> nameMap_t;
public:
	LogTreeLoader(const std::string &fnPrefix, const std::string &fnSuffix);
	~LogTreeLoader();

	const std::vector<std::string>& getOrderedNames() const;
	std::string getIdString(long int aTreeIdx) const;

	void setBurninIteration(size_t aBurninIteration);
	void setBurninRatio(double aBurninRatio);

	bool summarizeTrees(std::vector<std::string> &idStrSummary,
			   	   	    std::map<long int, size_t> &mapping);

	const std::string& getTreeStringFN() const;

private:

	size_t burninIteration;
	double burninRatio;

	typedef std::map<long int, std::string> treeStringMap_t;
	typedef treeStringMap_t::iterator itTreeStringMap_t;
	typedef treeStringMap_t::const_iterator itConstTreeStringMap_t;
	treeStringMap_t treeStringMap;

	std::vector<std::string> orderedTaxaNames;

	std::string treeStringFN;

	bool loadTreeStringMap();
};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* LOGTREELOADER */
