//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SplitLogger.h
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#ifndef SPLITLOGGER_H_
#define SPLITLOGGER_H_

#include <stddef.h>
#include <algorithm>

#include "DecisionTree.h"
#include "LogTreeLoader.h"
#include "PartitionStatistics.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeManager.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

class Tree;
class TreeNode;

class SplitLogger {
public:
	SplitLogger(double aBurnin, double aFreqThres, LogTreeLoader &aLogTreeLoader, const std::string &aOutFName);
	~SplitLogger();

	void processTreeSeq(const std::vector<long int> &treeSeq);

private:

	const double BURNIN, FREQUENCY_THRESHOLD;
	const std::string outFName;
	LogTreeLoader &logTreeLoader;

	struct leaf_t {
		int id;
		std::string name;
		bool operator<(leaf_t const& rhs) const{
		    //return name < rhs.name;
			return id < rhs.id;
		}
	};

	std::vector<leaf_t> leaves;
	std::map<size_t, size_t> leavesMapping;

	void defineLeaves(Tree *aTree);

	void computeSplits(size_t iT, Tree *aTree, DecisionTree &dt);
	void createSplit(TreeNode* cNode, TreeNode* pNode, split_t &split);
	void writeTaxaOrder(const std::string &fName);

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* SPLITLOGGER_H_ */
