//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Tree.h
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREE_TREERECONSTRUCTION_H_
#define TREE_TREERECONSTRUCTION_H_

#include <stddef.h>
#include <algorithm>
#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/functional/hash.hpp>
#include <boost/dynamic_bitset/dynamic_bitset.hpp>

#include "TreeNode.h"
#include "TreeParser/Parser.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"

namespace MolecularEvolution { namespace DataLoader { class MSA; } }
namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }

namespace MolecularEvolution {
namespace TreeReconstruction {

class TreeNode;
class TreeModifier;
//typedef std::vector<bool> split_t;
typedef boost::dynamic_bitset<> split_t;

class Tree {
public:

	typedef boost::shared_ptr<Tree> sharedPtr_t;

	struct edge_t {
		TreeNode* n1;
		TreeNode* n2;

		bool operator<(const edge_t &other) const {
				return ((n1->getId() < other.n1->getId()) ||
					   ((n1->getId() == other.n1->getId()) && (n2->getId() < other.n2->getId())));
		}
	};

	struct contiguous_edge_pair_t {
		TreeNode* n1; // external node with smaller ID
		TreeNode* n2; // shared node (between 2 edges)
		TreeNode* n3; // external node with bigger ID

		void orderNodes() { // Ensure that n1.id < n3.id
			if (n3->getId() < n1->getId()) std::swap(n1, n3);
		}

		// contiguous edge are sorted by the node ids from n1 to n3
		bool operator<(const contiguous_edge_pair_t &other) const {
        return ((n1->getId() < other.n1->getId()) ||
				((n1->getId() == other.n1->getId()) && (n2->getId() < other.n2->getId())) ||
				((n1->getId() == other.n1->getId()) && (n2->getId() == other.n2->getId()) && (n3->getId() < other.n3->getId())));
		}

        bool operator==(const contiguous_edge_pair_t &other) const {
        	return ((n1->getId() == other.n1->getId())
        			&& (n2->getId() == other.n2->getId())
					&& (n3->getId() == other.n3->getId())) ||
        			((n1->getId() == other.n3->getId())
					&& (n2->getId() == other.n2->getId())
					&& (n3->getId() == other.n1->getId()));
        }
    };

	struct edge_path_t {
		std::vector<TreeNode*> nodes;

		void orderNodes() {
			if (nodes.back()->getId() < nodes.front()->getId()) {
				std::reverse(nodes.begin(), nodes.end());
			}
		}

		void reverseDirection() {
			std::reverse(nodes.begin(), nodes.end());
		}


		bool operator<(const edge_path_t &other) const {
			if(this->nodes.size() < other.nodes.size()) return true;
			else if(this->nodes.size() > other.nodes.size()) return false;

			for(size_t i=0; i<other.nodes.size(); ++i) {
				if(this->nodes[i]->getId() < other.nodes[i]->getId()) {
					return true;
				} else if(this->nodes[i]->getId() > other.nodes[i]->getId()) {
					return false;
				}
			}
			return false; // they are equal
		}

        bool operator==(const edge_path_t &other) const {
        	for(size_t i=0; i<other.nodes.size(); ++i) {
				if(this->nodes[i]->getId() != other.nodes[i]->getId()) {
					return false;
				}
			}
        	return true;
        }
    };

public:
	//Tree(const Tree &other);
	Tree(bool doShuffle, const DataLoader::MSA &, const std::map<std::string, int> & nameMapping);
	Tree(const DL::TreeNode &newickNode, const std::map<std::string, int> & nameMapping);
	Tree(const std::string &aTreeStr);
	Tree(const Utils::Serialize::buffer_t &buffer);
	~Tree();

	bool isEqualTo(const Tree* other) const;

	std::string getIdString() const;
	std::string getNameString(const std::vector<std::string> &names) const;
	std::string getNameAndBranchLengthString(const std::vector<std::string> &names,
							  	  	  	  	 const std::vector<double> &branchLength) const;

	/** These two functions must be used together and are called by LogAdapted */
	std::vector<size_t> getIdBLInTraversalOrder();
	std::string getNameAndBranchLengthStringForLogAdpt(const std::vector<std::string> &names,
							  	  	  	  	        const std::vector<double> &branchLength);
	std::string getNameAndBranchLengthStringForLogConv(const std::vector<std::string> &names,
							  	  	  	  	        const std::vector<double> &branchLength);

	TreeNode* getRoot();
	TreeNode* getNode(size_t id);
	std::vector<edge_t> getInternalEdges() const;
	std::vector<TreeNode*>& getInternals();
	std::vector<TreeNode*>& getTerminals();
	const std::map<size_t, size_t>& getNodeIdtoNewickMapping() const;

	std::vector< contiguous_edge_pair_t > getContiguousInternalEdges() const;
	std::vector < edge_t > getNeighbouringInternalEdges(TreeNode *pNode, TreeNode *cNode) const;

	long int getHashKey() const;

	void defineNodesCategory();

	//Tree* clone() const; // DEPRECATED AND NOT WORKING

	void update(const std::vector<TreeNode*> &aUpdatedNodes);
	void addUpdatedNodes(std::vector<TreeNode*>& aUpdTreeNodes);
	const std::vector<TreeNode*>& getUpdatedNodes() const;
	void clearUpdatedNodes();

	const Utils::Serialize::buffer_t& save();

	void DBG_checkChildenOrder(TreeNode *node, TreeNode *parent);

	void defineBalancingRoot();


	split_t findBipartitions(TreeNode *n1, TreeNode *n2);
	split_t findBipartitions(edge_t);

	std::vector<edge_t> getInternalEdgeForBipartitions();
	std::vector<split_t> getInternalSplitsForBipartitions();

	std::vector<edge_t> getAllEdgeForBipartitions();
	std::vector<split_t> getAllSplitsForBipartitions();

	std::vector<edge_t> getDirectedEdgeForBipartitions();
	std::vector<split_t> getDirectedSplitsForBipartitions();

	typedef enum {WITHIN_K_NODES=0, EXACTLY_K_NODES=1} pathConstraint_t;
	std::vector< edge_path_t > computeKNodesPaths(const size_t K, pathConstraint_t pathConstrType,
												  TreeNode* toGraft, TreeNode* toGraftParent) const;
	std::vector< edge_path_t > computeAllKNodesPaths(const size_t K, pathConstraint_t pathConstrType) const;

private:

	long int myH;
	TreeNode *root;
	std::string idString;

	bool areBipartitionsReady, areBipartitionsModified;

	std::vector<TreeNode*> internals;
	std::vector<TreeNode*> terminals;
	std::vector<TreeNode*> updatedNodes;

	typedef std::map<size_t, TreeNode*> nodesMap_t;
	typedef nodesMap_t::iterator itNodesMap_t;
	nodesMap_t nodesMap;

	std::map<size_t, size_t> nodeIdtoNewickMapping;

	std::vector< edge_t > allEdges, internalEdges, dirEdges;
	std::vector< split_t>  allSplits, internalSplits, dirSplits;

	void createHashKey();

	void exploreTree(TreeNode *node, TreeNode *parent);
	void updateTree(const std::vector<TreeNode*> &aUpdatedNodes);
	void createRecursive(const DL::TreeNode &newickNode, TreeNode *node,
			const std::map<std::string, int> & nameMapping);

	TreeNode* createFromMSA(bool doShuffle, const DataLoader::MSA &msa,
			const std::map<std::string, int>& nameMapping);
	void assignIdRecursively(size_t &id, TreeNode *node, TreeNode *parent);
	void assignIdRecursively(size_t &id, TreeNode *node, TreeNode *parent, std::map<size_t, size_t> &oldIDs);


	void load(const Utils::Serialize::buffer_t &buffer);

	// Serialization
	Utils::Serialize::buffer_t serializedBuffer;
    friend class boost::serialization::access;

    template<class Archive>
    void save(Archive & ar, const unsigned int version) const;

    template<class Archive>
    void load(Archive & ar, const unsigned int version);
    BOOST_SERIALIZATION_SPLIT_MEMBER()

    split_t createSplitsRecursive(TreeNode* cNode, TreeNode* pNode);

	void initBipartitions();
	void resetBipartitions();

	size_t findEdgePositionInVector(edge_t aEdge, std::vector<edge_t> &aEdgeVector);

	void addBiparition(edge_t toAdd, split_t split);
	void updateBipartition(edge_t toUpdate, split_t split);
	void removeBipartition(edge_t toRemove);

	void checkUpdatdeSplitCorrectness();

	void recursiveFindKNodesPaths(size_t iK, const size_t K,  edge_path_t &currentPath,
								  std::set< edge_path_t > &paths, pathConstraint_t pathConstrType) const;

	friend class TreeModifier;

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* TREE_TREERECONSTRUCTION_H_ */
