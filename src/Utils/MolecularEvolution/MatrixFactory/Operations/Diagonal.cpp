//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DiagonalOperation.cpp
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief
 */
#include "Diagonal.h"

namespace MolecularEvolution {
namespace MatrixUtils {

namespace Operations {

Diagonal::Diagonal(size_t aIdxFrom, size_t aIdxTo, const size_t N) : Base(aIdxFrom, aIdxTo, N) {
}

Diagonal::~Diagonal() {
}

void Diagonal::addOffDiagOperation(Base *offDiagOperation) {
	offDiagPos.push_back(offDiagOperation->getIdxMatrix());
	offDiagOperations.push_back(offDiagOperation);
}

std::vector<MultStore> Diagonal::toInstructions(double &scalingFactor,
												std::vector<double>& matrix,
												std::vector<double>& frequencies,
												std::vector<double>& freqDotFreq,
												std::vector<double>& coefficients) const {

	std::vector<MultStore> instrs;

	// matrix = 0*matrix + -1 * matrixij
	instrs.push_back(MultStore(MultStore::NEG_ONE, matrix[offDiagPos[0]], MultStore::ZERO, matrix[getIdxMatrix()]));
	for(size_t i=1; i<offDiagOperations.size(); ++i){
		// matrix = 1*matrix + -1 * matrixij
		instrs.push_back(MultStore(MultStore::NEG_ONE, matrix[offDiagPos[i]], MultStore::ONE, matrix[getIdxMatrix()]));
	}

	return instrs;
}


} /* namespace Operations */

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */
