//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NucleotidePairMF.h
 *
 * @date Mar 29, 2017
 * @author meyerx
 * @brief
 */
#include "NucleotidePairMF.h"

namespace MolecularEvolution {
namespace MatrixUtils {

NucleotidePairMF::NucleotidePairMF(const size_t aNB_COEFF) :
		MatrixFactory(MD::getNucleotides()->NB_BASE_NUCL*MD::getNucleotides()->NB_BASE_NUCL, aNB_COEFF) {
}

NucleotidePairMF::NucleotidePairMF(const size_t aNB_COEFF, const bool aDoRowsSumtoZero) :
		MatrixFactory(MD::getNucleotides()->NB_BASE_NUCL*MD::getNucleotides()->NB_BASE_NUCL, aNB_COEFF, aDoRowsSumtoZero) {
}

NucleotidePairMF::~NucleotidePairMF() {
}

void NucleotidePairMF::scanCombination() {
	const std::string NUC_BASE(MD::getNucleotides()->NUCL_BASE, MD::getNucleotides()->NB_BASE_NUCL);

	//Operations::Diagonal* diagOperations[N];

	// Off diagonal values
	for (size_t iFrom = 0; iFrom < N; ++iFrom) {

		if(doRowsSumtoZero) { // If we want rows to sum to 0, init a diagOperation
			diagonalOperations[iFrom] = new Operations::Diagonal(iFrom, iFrom, N);
		}

		// for each combination
		for (size_t iTo = 0; iTo < N; ++iTo) {
			if(iFrom != iTo) { // If we want rows to sum to 0, build the sum of term

				// Get nucleotide idx for each pair position
				div_t resDivFrom = div((int)iFrom, (int)MD::getNucleotides()->NB_BASE_NUCL);
				int iFrom1 = resDivFrom.quot;
				int iFrom2 = resDivFrom.rem;

				div_t resDivTo = div((int)iTo, (int)MD::getNucleotides()->NB_BASE_NUCL);
				int iTo1 = resDivTo.quot;
				int iTo2 = resDivTo.rem;

				if((iFrom1 != iTo1) != (iFrom2 != iTo2)) { // diff in pos 1 XOR diff in pos 2
					// add the operation
					Operations::Base *op = defineOperation(iFrom, iTo);
					operations.push_back(op);

					if(doRowsSumtoZero) {
						// If diagonal sum to 0 add the element to the diagonal
						diagonalOperations[iFrom]->addOffDiagOperation(op);
					}
				}
			}
		}
	}

	// Diagonal values
	if(!doRowsSumtoZero){
		for (size_t iDiag = 0; iDiag < N; ++iDiag) {
			Operations::Base *op = defineOperation(iDiag, iDiag);
			operations.push_back(op);
		}
	}
}


} /* namespace MatrixUtils */
} /* namespace MolecularEvolution */
