//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NucleotideMF.cpp
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief
 */
#include "NucleotideMF.h"

namespace MolecularEvolution {
namespace MatrixUtils {

NucleotideMF::NucleotideMF(const size_t aNB_COEFF) :
		MatrixFactory(MD::getNucleotides()->NB_BASE_NUCL, aNB_COEFF) {
}

NucleotideMF::NucleotideMF(const size_t aNB_COEFF, const bool aDoRowsSumtoZero) :
		MatrixFactory(MD::getNucleotides()->NB_BASE_NUCL, aNB_COEFF, aDoRowsSumtoZero) {
}

NucleotideMF::~NucleotideMF() {
}

void NucleotideMF::scanCombination() {
	const std::string NUC_BASE(MD::getNucleotides()->NUCL_BASE, MD::getNucleotides()->NB_BASE_NUCL);

	//Operations::Diagonal* diagOperations[N];

	// Off diagonal values
	for (size_t iFrom = 0; iFrom < N; ++iFrom) {
		if(doRowsSumtoZero) { // If we want rows to sum to 0, init a diagOperation
			diagonalOperations[iFrom] = new Operations::Diagonal(iFrom, iFrom, N);
		}

		// for each combination
		for (size_t iTo = 0; iTo < N; ++iTo) {

			if(iFrom != iTo) { // If we want rows to sum to 0, build the sum of term
				// add the operation
				Operations::Base *op = defineOperation(iFrom, iTo);
				operations.push_back(op);

				if(doRowsSumtoZero) {
					// If diagonal sum to 0 add the element to the diagonal
					diagonalOperations[iFrom]->addOffDiagOperation(op);
				}
			}
		}
	}

	// Diagonal values
	if(!doRowsSumtoZero){
		for (size_t iDiag = 0; iDiag < N; ++iDiag) {
			Operations::Base *op = defineOperation(iDiag, iDiag);
			operations.push_back(op);
		}
	}
}






} /* namespace MatrixUtils */
} /* namespace MolecularEvolution */
