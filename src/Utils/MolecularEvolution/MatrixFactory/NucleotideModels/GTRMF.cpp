//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HKY85MF.cpp
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief
 */
#include "GTRMF.h"

#include "Utils/MolecularEvolution/Definitions/MolecularDefIncl.h"
#include "Utils/MolecularEvolution/Definitions/Nucleotides.h"

namespace MolecularEvolution {
namespace MatrixUtils {

const size_t GTR_MF::GTR_NB_COEFF = 5;
const std::vector<std::string> GTR_MF::PARAMETERS_NAME = boost::assign::list_of("A<-->C")("A<-->G")("A<-->T")("C<-->G")("C<-->T")("G<-->T");

GTR_MF::GTR_MF() : NucleotideMF(GTR_NB_COEFF) {
	MatrixFactory::initOperations();
}

GTR_MF::GTR_MF(const bool aDoRowsSumtoZero) : NucleotideMF(GTR_NB_COEFF, aDoRowsSumtoZero) {
	MatrixFactory::initOperations();
}


GTR_MF::~GTR_MF() {
}

void GTR_MF::doInitOperations() {
	// For each combination, call defineOperation or defineOperationDiagonal
	super::scanCombination();
}

Operations::Base* GTR_MF::defineOperation(size_t iFrom, size_t iTo) const {

	std::vector<size_t> coeffId;

	// Get correct theta index
	size_t thetaIdx = defineThetaIdx(iFrom, iTo);
	if(thetaIdx < GTR_NB_COEFF) coeffId.push_back(thetaIdx); // Transition T<->G fixed at 1.
	//coeffId.push_back(thetaIdx); // Its not the transition T<->G fixed at 1.

	Operations::Base* myOp = new Operations::CoeffsDotFreq(iFrom, iTo, N, coeffId);

	return myOp;
}

int GTR_MF::defineThetaIdx(size_t iFrom, size_t iTo) const {
	const MD::Nucleotides *nucl = MD::getNucleotides();

	if((iFrom < 0) || (iTo < 0)) {
		std::cerr << "size_t BranchSiteREL_MF::defineThetaIdx(char from, char to) const;" << std::endl;
		std::cerr << "Error : character doesn't represent a non-ambiguous nucleotide." << std::endl;
		abort();
	}

	char from = nucl->NUCL_BASE[iFrom];
	char to = nucl->NUCL_BASE[iTo];


	// We want the nucleotide with smallest index to be in from
	if(iFrom > iTo) std::swap(from, to);

	// For each possible nucleotide transition return the index
	if(from == 'A' && to == 'C') {
		return 0;
	} else if(from == 'A' && to == 'G') {
		return 1;
	} else if(from == 'A' && to == 'T') {
		return 2;
	} else if(from == 'C' && to == 'G') {
		return 3;
	} else if(from == 'C' && to == 'T') {
		return 4;
	} else if(from == 'T' && to == 'G') {
		return 5;
	} else {
		std::cerr << "size_t BranchSiteREL_MF::defineThetaIdx(char from, char to) const;" << std::endl;
		std::cerr << "Error : character doesn't represent a non-ambiguous nucleotide." << std::endl;
		abort();
		return -1;
	}
}


} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */
