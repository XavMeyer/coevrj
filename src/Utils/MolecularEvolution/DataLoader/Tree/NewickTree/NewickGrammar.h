//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NewickGrammar.h
 *
 * @date Jan 21, 2015
 * @author meyerx
 * @brief
 */
#ifndef NEWICKGRAMMAR_H_
#define NEWICKGRAMMAR_H_

//#define BOOST_SPIRIT_DEBUG

// Custom


// Boost
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/support_adapt_adt_attributes.hpp>

// STD
#include <string>

namespace MolecularEvolution {
namespace DataLoader {

using std::string;
namespace qi = ::boost::spirit::qi;

template<typename Iterator>
class NewickGrammar: public qi::grammar<Iterator, TreeNode()> {
public:
	NewickGrammar() : NewickGrammar::base_type(tree) {

		// For label use %= to assign the result of the parse to the string
		label %= qi::lexeme[+(~qi::char_(';') - ':' - ')' - ',')];

        // For branch length use %= to assign the result of the parse to the double
		branch_length %= ':' >> qi::double_;

	    // When parsing the subtree just assign the elements that have been built in the subrules
		subtree = -descendant_list >> -label >> -branch_length;

        // Descendant list is a vector of TreeNode, we just push back the created TreeNode into the vector
		descendant_list = '(' >> subtree >> *(',' >> subtree) >> ')' ;

		 // The tree receive the whole subtree using %=
		tree %= subtree >> ';';

		BOOST_SPIRIT_DEBUG_NODE(label);
		BOOST_SPIRIT_DEBUG_NODE(branch_length);
		BOOST_SPIRIT_DEBUG_NODE(subtree);
		BOOST_SPIRIT_DEBUG_NODE(descendant_list);
		BOOST_SPIRIT_DEBUG_NODE(tree);
	}

private:

	/* grammar rules */
	qi::rule<Iterator, TreeNode()> tree, subtree;
	qi::rule<Iterator, TreeNode_children()> descendant_list;
	qi::rule<Iterator, double()> branch_length;
	qi::rule<Iterator, string()> label;
};

}
}

#endif /* NEWICKGRAMMAR_H_ */
