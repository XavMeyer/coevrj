//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Frequencies.cpp
 *
 * @date Feb 11, 2015
 * @author meyerx
 * @brief
 */
#include "Frequencies.h"

namespace MolecularEvolution {
namespace DataLoader {
namespace Utils {


Frequencies::Frequencies(const size_t aN) : N(aN), validFreq(N, true), frequencies(N, 1./(double)N){
	init(frequencies);
}

Frequencies::Frequencies(const size_t aN, const std::vector<double> &aFrequencies) : N(aN), validFreq(N, true), frequencies(N, 0.){
	init(aFrequencies);
}

Frequencies::~Frequencies() {
}

void Frequencies::set(const std::vector<double> &aFrequencies) {
	validFreq.assign(N, true);
	frequencies.assign(N, 0.);
	init(aFrequencies);
}

void Frequencies::init(const std::vector<double> &aFrequencies) {

	Eigen::Map<Eigen::VectorXd> mapedF(frequencies.data(), N);

	// Copy and normalise freq vector
	double sum = 0.;
	for(size_t i=0; i<N; ++i) {
		sum += aFrequencies[i];
		mapedF(i) = aFrequencies[i];
	}
	mapedF = mapedF/sum;
	freq = mapedF;
	freq.eval();
	freqFlt = freq.cast<float>();

	// Prepare sqrt
	sqrtFreq = freq.cwiseSqrt();
	sqrtFreq.eval();
	sqrtFreqFlt = sqrtFreq.cast<float>();

	// This is not mathematicaly right.
	// However since the frequency is 0, the probability of
	// seeing such event is 0 and thus should not be considered.
	// Forcing the sqrtInvFreq to 0 will lead to a 0 probability.
	invFreq = freq;
	sqrtInvFreq = sqrtFreq;
	for(size_t i=0; i<N; ++i) {
		if(freq(i) > 1e-50){
			invFreq(i) = 1./invFreq(i);
			sqrtInvFreq(i) = 1./sqrtInvFreq(i);
		} else {
			validFreq[i] = false;
			invFreq(i) = 0.;
			sqrtInvFreq(i) = 0.;
		}
	}

	invFreqFlt = invFreq.cast<float>();
	sqrtInvFreqFlt = sqrtInvFreq.cast<float>();
}

const size_t Frequencies::size() const {
	return N;
}

const std::vector<double>& Frequencies::getStd() const {
	return frequencies;
}

const Eigen::VectorXd& Frequencies::getEigen() const {
	return freq;
}

const Eigen::VectorXd& Frequencies::getInvEigen() const {
	return invFreq;
}

const Eigen::VectorXd& Frequencies::getSqrtEigen() const {
	return sqrtFreq;
}

const Eigen::VectorXd& Frequencies::getSqrtInvEigen() const {
	return sqrtInvFreq;
}

const Eigen::VectorXf& Frequencies::getEigenFlt() const {
	return freqFlt;
}

const Eigen::VectorXf& Frequencies::getInvEigenFlt() const {
	return invFreqFlt;
}

const Eigen::VectorXf& Frequencies::getSqrtEigenFlt() const {
	return sqrtFreqFlt;
}

const Eigen::VectorXf& Frequencies::getSqrtInvEigenFlt() const {
	return sqrtInvFreqFlt;
}


size_t Frequencies::getNValidFreq() const {
	return std::count(validFreq.begin(), validFreq.end(), true);
}

const std::vector<bool>& Frequencies::getValidFreq() const {
	return validFreq;
}



} /* namespace Utils */
} /* namespace DataLoader */
} /* namespace MolecularEvolution */
