//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NucleotidesPairProfiles.cpp
 *
 * @date Apr 20, 2017
 * @author meyerx
 * @brief
 */
#include "NucleotidesPairProfiles.h"

#include <assert.h>
#include <stdio.h>

#include "MolecularDefIncl.h"

namespace MolecularEvolution {
namespace Definition {

NucleotidesPairProfiles* NucleotidesPairProfiles::instance = NULL;

const NucleotidesPairProfiles* NucleotidesPairProfiles::getInstance() {
	if(!instance) {
		instance = new NucleotidesPairProfiles;
	}
	return instance;
}


NucleotidesPairProfiles::NucleotidesPairProfiles() :
		ALL_POSSIBLE_PROFILES(initAllPossibleProfile()),
		NB_POSSIBLE_PROFILES(ALL_POSSIBLE_PROFILES.size()),
		MAP_PROFILES_ID(initMapProfilesId(ALL_POSSIBLE_PROFILES)) {
}

NucleotidesPairProfiles::~NucleotidesPairProfiles() {
}

std::vector<idProfile_t> NucleotidesPairProfiles::getPossibleProfilesId(bitset16b_t observedPairs) const {

	std::vector<idProfile_t> profilesId;
	vecProfiles_t someProfiles = getPossibleProfiles(observedPairs);

	for(size_t iP=0; iP<someProfiles.size(); ++iP) {
		mapProfilesId_t::const_iterator it = MAP_PROFILES_ID.find(someProfiles[iP]);
		assert(it!=MAP_PROFILES_ID.end());
		profilesId.push_back(it->second);;
	}

	return profilesId;
}

vecProfiles_t NucleotidesPairProfiles::getPossibleProfiles(bitset16b_t observedPairs) const {
	size_t iObserved = 0; 					// None for now
	size_t iLevel = 0; 						// Starting at level 0
	size_t maxLevel = 4; 					// Max number of different pairs
	bitset16b_t inProfile;					// All false (empty)
	vecProfiles_t someProfiles;				// Empty vector of profiles

	recursiveProfileBuilding(iObserved, iLevel, maxLevel, observedPairs, inProfile, someProfiles);

	return someProfiles;
}

profile_t NucleotidesPairProfiles::idToProfile(idProfile_t idProfile) const {
	assert(idProfile < ALL_POSSIBLE_PROFILES.size());
	return ALL_POSSIBLE_PROFILES[idProfile];
}

std::string NucleotidesPairProfiles::profileToString(profile_t profile) const {
	std::stringstream ss;
	ss << "[";
	for(size_t iP=0; iP<profile.size(); ++iP) {
		ss << profile[iP];
		if(iP < profile.size()-1) ss << "|";
		else ss << "]";
	}
	return ss.str();
}

vecProfiles_t NucleotidesPairProfiles::initAllPossibleProfile() const {

	size_t iObserved = 0; 					// None for now
	size_t iLevel = 0; 						// Starting at level 0
	size_t maxLevel = 4; 					// Max number of different pairs
	bitset16b_t observedPairs(0xFFFF);		// All true (all observed)
	bitset16b_t inProfile;					// All false (empty)
	vecProfiles_t allProfiles;				// Empty vector of profiles

	recursiveProfileBuilding(iObserved, iLevel, maxLevel, observedPairs, inProfile, allProfiles);

	/*
	// For all possible combination of pairCode
	const size_t NB_PAIR_NUCL = getNucleotidesPairs()->NB_NUCL_PAIR;

	// Search for all combination (without replacement)
	for(size_t iPair1=0; iPair1<NB_PAIR_NUCL-1; ++iPair1) {
		for(size_t iPair2=iPair1+1; iPair2<NB_PAIR_NUCL; ++iPair2) {
			// Consider all possible combination!

			// First we have to make sure that there are two subs between paired nucl.

			// Nucleotide codes on two bits XX (00=>A, 01=>C, 10=>G, 11=>T)
			// Pair of nucl codes on 4 bits XXYY (e.g. 00|00=>A|A)
			// If the first and last two bits are different in each code, we have a profile.
			size_t maskPos1 = 0b1100; // XX
			size_t maskPos2 =  0b0011; // YY

			bool differPos1 = (iPair1 | maskPos1) != (iPair2 | maskPos1);
			bool differPos2 = (iPair1 | maskPos2) != (iPair2 | maskPos2);

			if(differPos1 && differPos2) { // We have a profile
				profile_t profile;
				profile.push_back(getNucleotidesPairs()->NUCL_PAIR[iPair1]);
				profile.push_back(getNucleotidesPairs()->NUCL_PAIR[iPair2]);
				allProfiles.push_back(profile);
			}
		}
	}
	*/
	return allProfiles;
}

NucleotidesPairProfiles::mapProfilesId_t NucleotidesPairProfiles::initMapProfilesId(const vecProfiles_t &vecProfiles) const {

	NucleotidesPairProfiles::mapProfilesId_t mapProfilesId;

	for(size_t iP=0; iP<ALL_POSSIBLE_PROFILES.size(); ++iP) {
		mapProfilesId.insert(std::make_pair(ALL_POSSIBLE_PROFILES[iP], iP));
	}

	return mapProfilesId;
}

void NucleotidesPairProfiles::recursiveProfileBuilding(size_t iObserved, size_t iLevel, size_t maxLevel,
													   bitset16b_t observedPairs, bitset16b_t inProfile,
													   vecProfiles_t &vecProfiles) const {

	// Nucleotide codes on two bits XX (00=>A, 01=>C, 10=>G, 11=>T)
	// Pair of nucl codes on 4 bits XXYY (e.g. iPair1= 0 ==> 00|00 => A|A )
	const size_t maskPos1 = 0b1100; // XX
	const size_t maskPos2 = 0b0011; // YY

	if(iLevel >= maxLevel) return; // We reached max level

	for(size_t iPair1=iObserved; iPair1<observedPairs.size(); ++iPair1) {
		if(!observedPairs[iPair1]) continue; // This pair is not observed
		//std::cout << "[" << iLevel << "] rec | OBSERVED - iPair1 = " << iPair1 << std::endl;

		// Check if there is a conflict
		bool conflict = false;
		if(iLevel > 0) { // Only check if the level is bigger than 0
			for(size_t iPair2=0; iPair2<inProfile.size(); ++iPair2) {
				//std::cout << "[" << iLevel << "] rec |  OBSERVED - iPair1 = " << iPair1 << "\t iPair2 = " << iPair2 << std::endl;
				if(!inProfile[iPair2]) continue; // This pair is not in profile
				// First we have to make sure that there are two substitutions between paired nucl.
				// Check that the masked XX YY values are different at both pairs
				bool differPos1 = (iPair1 | maskPos1) != (iPair2 | maskPos1);
				bool differPos2 = (iPair1 | maskPos2) != (iPair2 | maskPos2);
				//std::cout << "[" << iLevel << "] rec | Pair1 = " << getNucleotidesPairs()->getNuclPairString(iPair1) << "\t Pair2 = " << getNucleotidesPairs()->getNuclPairString(iPair2) << std::endl;
				//std::cout << "[" << iLevel << "] rec | differPos1 = "  << (differPos1 ? "true" : "false") << "| differPos2 = " << (differPos2 ? "true" : "false") << std::endl;
				// If it does not differ at both pos, then there is a conflict
				if(!(differPos1 && differPos2)) {
					conflict = true;
					break;
				}
			}
		}
		//getchar();

		if(!conflict) { // We have a profile
			bitset16b_t augmentedProfile(inProfile);
			augmentedProfile[iPair1] = true;
			if(iLevel>0) { // We only add if we have more than 1 pair
				//std::cout << "[" << iLevel << "] rec | ADD = " << getNucleotidesPairs()->getNuclPairString(iPair1) << std::endl;
				vecProfiles.push_back(getNucleotidesPairs()->getNuclPairsString(augmentedProfile));
			}
			recursiveProfileBuilding(iPair1+1, iLevel+1, maxLevel, observedPairs, augmentedProfile, vecProfiles);
		}

	}
}



} /* namespace Definition */
} /* namespace MolecularEvolution */
