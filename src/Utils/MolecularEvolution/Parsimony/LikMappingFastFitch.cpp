/*
 * LikMappingFastFitch.cpp
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#include "LikMappingFastFitch.h"

namespace MolecularEvolution {
namespace Parsimony {

LikMappingFastFitch::LikMappingFastFitch(const std::string aName) : TreeReconstruction::LikelihoodMapper::LikelihoodMapping() {
	name = aName;
	fastFitchNode = NULL;
}

LikMappingFastFitch::~LikMappingFastFitch() {
}

void LikMappingFastFitch::setFastFitchNode(FastFitchNode* aNode) {
	fastFitchNode = aNode;
}

FastFitchNode* LikMappingFastFitch::getFastFitchNode() {
	return fastFitchNode;
}

void LikMappingFastFitch::doAddChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping) {

	LikMappingFastFitch* childParsimony = dynamic_cast<LikMappingFastFitch*>(childLikMapping.get());
	assert(childParsimony);

	// Add DAG link
	if(fastFitchNode != NULL) {
		assert(childParsimony->getFastFitchNode() && "Make sure add is correct.");
		fastFitchNode->addChild(childParsimony->getFastFitchNode());
	}
}

void LikMappingFastFitch::doRemoveChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping) {

	LikMappingFastFitch* childParsimony = dynamic_cast<LikMappingFastFitch*>(childLikMapping.get());
	assert(childParsimony);

	// Remove DAG link
	if(fastFitchNode != NULL) {
		assert(childParsimony->getFastFitchNode() && "Make sure remove is correct.");
		fastFitchNode->removeChild(childParsimony->getFastFitchNode());
	}
}

void LikMappingFastFitch::doOnApplyChange() {
	fastFitchNode->setDone();
}

void LikMappingFastFitch::doOnUpdate(std::set<DAG::BaseNode*> &updatedNodes) {
	assert(fastFitchNode && "On update - no parsimony node are found.");
	if(!fastFitchNode->isLeafNode()) {
		fastFitchNode->updated();
		updatedNodes.insert(fastFitchNode);
	}
}

const std::string& LikMappingFastFitch::getName() const {
	return name;
}


} /* namespace Parsimony */
} /* namespace MolecularEvolution */
