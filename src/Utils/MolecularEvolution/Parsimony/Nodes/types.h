/*
 * types.h
 *
 *  Created on: Apr 19, 2018
 *      Author: meyerx
 */

#ifndef UTILS_MOLECULAREVOLUTION_PARSIMONY_NODES_TYPES_H_
#define UTILS_MOLECULAREVOLUTION_PARSIMONY_NODES_TYPES_H_

namespace MolecularEvolution {
namespace Parsimony {

typedef enum {FITCH=0, FITCH_FAST=1, FITCH_FAST2=2, FITCH_FAST3=3, SANKOFF=4} algorithm_t;


} /* namespace Parsimony */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_PARSIMONY_NODES_TYPES_H_ */
