/*
 * FastFitchNode.cpp
 *
 *  Created on: Apr 27, 2018
 *      Author: meyerx
 */

#include "FastFitchNode.h"

namespace MolecularEvolution {
namespace Parsimony {

char* initMapping() {
	char* ptr = new char[16*16];

	char valMask = 0b1111;
	char incMask = 0b1 << 4;

	for(char i=0; i<16; ++i) {
		for(char j=0; j<16; ++j) {
			char val = i & j;
			if((valMask & val) == 0) {
				ptr[i*16+j] = (((i | j) & valMask) | incMask);
			} else {
				ptr[i*16+j] = (val & valMask);
			}
		}
	}

	return ptr;
}

const char* FastFitchNode::mappingCA = initMapping();

FastFitchNode::FastFitchNode(const size_t aN, bool aIsRoot) :
		DAG::BaseNode(), N(aN), N_STATE(4), isLeaf(false), isRoot(aIsRoot) {
	charArray = NULL;
	rootCA = NULL;
	init();
}

FastFitchNode::FastFitchNode(const size_t aN, const std::string &aName, const DL::CompressedAlignements& ca) :
				DAG::BaseNode(), N(aN), N_STATE(4), isLeaf(true), isRoot(false) {
	charArray = NULL;
	rootCA = NULL;
	init(aName, ca);
}

FastFitchNode::~FastFitchNode() {
	delete [] charArray;
	if(isRoot) delete [] rootCA;
}

void FastFitchNode::init() {
	changeCount = 0;
	charArray = new char[N];
	if(isRoot) {
		rootCA = new char[N];
	}

}

void FastFitchNode::init(const std::string &aName, const DL::CompressedAlignements& ca) {
	changeCount = 0;
	charArray = new char[N];

	for	(size_t iS=0; iS<N; ++iS) {
		charArray[iS] = 0b0;

		char val = 0b0;
		DL::Alignment::vecCode_t vals = ca.getSiteCode(aName, iS);
		for(size_t iV=0; iV<vals.size(); ++iV) {
			val |= 0b1 << vals[iV];
		}
		charArray[iS] = val;
	}
}

void FastFitchNode::doProcessingFitch(size_t changeCountC1, size_t changeCountC2, const char* charArray1, const char* charArray2) {

	// Sum children change count
	changeCount = changeCountC1 + changeCountC2;

	char valMask = 0b1111;
	for	(unsigned int iS=0; iS<N; ++iS) {
		char res = mappingCA[charArray1[iS]*16+charArray2[iS]];
		charArray[iS] = res & valMask;
		changeCount += (res >> 4) & 0b1;
	}
}

void FastFitchNode::doProcessing() {

	if(isLeaf)  return;

	assert(children.size() > 1 && "[FastFitchNode] Error in the tree topology - there are less than 2 children.");

	doProcessingFitch(children[0]->getChangeCount(), children[1]->getChangeCount(), children[0]->getPossibleNucl(), children[1]->getPossibleNucl());

	// If we are at the top node of a unrooted tree
	if(isRoot) {
		size_t tmpChangeCnt = changeCount;
		std::swap(charArray, rootCA);
		doProcessingFitch(tmpChangeCnt, children[2]->getChangeCount(), rootCA, children[2]->getPossibleNucl());
	}
}

bool FastFitchNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void FastFitchNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(FastFitchNode)){
		children.push_back(dynamic_cast<FastFitchNode*>(aChild));
	} else {
		assert(false && "[FastFitchNode] Wrong type of DAG node.");
	}
}

void FastFitchNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
		if(childtype == typeid(FastFitchNode)){
			FastFitchNode* childNode = dynamic_cast<FastFitchNode*>(aChild);
			std::vector<FastFitchNode*>::iterator pos = std::find(children.begin(), children.end(), childNode);
			if (pos != children.end()) {
				children.erase(pos);
			}
			//assert(pos != children.end());
		} else {
			assert(false && "[FastFitchNode] Wrong type of DAG node.");
		}
}

size_t FastFitchNode::getChangeCount() const {
	return changeCount;
}

const char* FastFitchNode::getPossibleNucl() const {
	return charArray;
}

double FastFitchNode::getParsimonyScore() const {
	return changeCount;
}

bool FastFitchNode::isLeafNode() const {
	return isLeaf;
}



} /* namespace Parsimony */
} /* namespace MolecularEvolution */
