/*
 * ParsimonyNode.h
 *
 *  Created on: Apr 19, 2018
 *      Author: meyerx
 */

#ifndef UTILS_MOLECULAREVOLUTION_PARSIMONY_NODES_PARSIMONYNODE_H_
#define UTILS_MOLECULAREVOLUTION_PARSIMONY_NODES_PARSIMONYNODE_H_

#include "DAG/Node/Base/BaseNode.h"
#include "Utils/MolecularEvolution/Parsimony/Nodes/types.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Alignment.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"

#include <Eigen/Core>
#include <set>
#include <vector>

namespace MolecularEvolution {
namespace Parsimony {

namespace DL = ::MolecularEvolution::DataLoader;

class ParsimonyNode: public DAG::BaseNode {
public:
	typedef long long int boolx8_t;

public:
	ParsimonyNode(const size_t aN, algorithm_t aAlgo);
	ParsimonyNode(const size_t aN, algorithm_t aAlgo, const std::string &aName, const DL::CompressedAlignements& ca);

	~ParsimonyNode();

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
	void doRemoveChild(const size_t rowId, DAG::BaseNode* aChild);

	const std::vector< std::set<DL::Alignment::code_t> >& getPossibleNucl() const;
	const std::vector< std::bitset<4> >& getPossibleNuclBS() const;
	const boolx8_t* getPossibleNuclBoolArray() const;
	const char* getPossibleNuclCharArray() const;
	const Eigen::VectorXi& getChangeCount() const;
	const Eigen::MatrixXd& getChanges() const;
	size_t getChangeCountVar() const;

	std::vector<double> computeParsimonyVector() const;
	double computeParsimonyScore() const;

	bool isLeafNode() const;

private:

	const size_t N, N_STATE, N_BOOL8X;
	const algorithm_t algorithm;
	bool isLeaf;

	std::vector<ParsimonyNode*> children;

	unsigned int changeCountVar;
	Eigen::MatrixXd changes, costMatrix;
	Eigen::VectorXi changeCount;
	std::vector< DL::Alignment::vecCode_t > states;
	std::vector< std::set<DL::Alignment::code_t> > possibleNucl;
	std::vector< std::bitset<4> > possibleNuclBS;

	boolx8_t *boolArray, *rootBA;
	char *charArray, *rootCA;
	static const char *mappingCA;

	void init();
	void init(const std::string &aName, const DL::CompressedAlignements& ca);

	void doProcessingFitch(const Eigen::VectorXi& changeCountC1, const Eigen::VectorXi& changeCountC2,
			const std::vector<std::set< DL::Alignment::code_t > >& possibleNuclC1, const std::vector<std::set< DL::Alignment::code_t > >& possibleNuclC2);
	void doProcessingFitchFast(const Eigen::VectorXi& changeCountC1, const Eigen::VectorXi& changeCountC2,
			const std::vector<std::bitset< 4 > >& possibleNuclBSC1, const std::vector< std::bitset< 4 > >& possibleNuclBSC2);
	void doProcessingFitchFast2(size_t changeCountC1, size_t changeCountC2,
				const boolx8_t* boolArray1, const boolx8_t* boolArray2);
	void doProcessingFitchFast3(size_t changeCountC1, size_t changeCountC2,
			const char* charArray1, const char* charArray2);
	void doProcessingSankoff(const Eigen::MatrixXd& changesC1, const Eigen::MatrixXd& changesC2);


};

} /* namespace Parsimony */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_PARSIMONY_NODES_PARSIMONYNODE_H_ */
