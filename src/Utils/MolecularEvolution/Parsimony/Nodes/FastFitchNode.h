/*
 * FastFitchNode.h
 *
 *  Created on: Apr 27, 2018
 *      Author: meyerx
 */

#ifndef UTILS_MOLECULAREVOLUTION_PARSIMONY_NODES_FASTFITCHNODE_H_
#define UTILS_MOLECULAREVOLUTION_PARSIMONY_NODES_FASTFITCHNODE_H_

#include "DAG/Node/Base/BaseNode.h"
#include "Utils/MolecularEvolution/Parsimony/Nodes/types.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Alignment.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"

#include <Eigen/Core>
#include <set>
#include <vector>

namespace DL = ::MolecularEvolution::DataLoader;

namespace MolecularEvolution {
namespace Parsimony {

class FastFitchNode: public DAG::BaseNode {
public:
	FastFitchNode(const size_t aN, bool aIsRoot);
	FastFitchNode(const size_t aN, const std::string &aName, const DL::CompressedAlignements& ca);
	~FastFitchNode();

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
	void doRemoveChild(const size_t rowId, DAG::BaseNode* aChild);

	const char* getPossibleNucl() const;
	size_t getChangeCount() const;

	double getParsimonyScore() const;

	bool isLeafNode() const;

private:

	const size_t N, N_STATE;
	bool isLeaf, isRoot;

	std::vector<FastFitchNode*> children;

	unsigned int changeCount;

	char *charArray, *rootCA;
	static const char *mappingCA;

	void init();
	void init(const std::string &aName, const DL::CompressedAlignements& ca);

	void doProcessingFitch(size_t changeCountC1, size_t changeCountC2,
			const char* charArray1, const char* charArray2);
};

} /* namespace Parsimony */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_PARSIMONY_NODES_FASTFITCHNODE_H_ */
