/*
 * ParsimonyNode.cpp
 *
 *  Created on: Apr 19, 2018
 *      Author: meyerx
 */

#include "Utils/MolecularEvolution/Parsimony/Nodes/ParsimonyNode.h"

namespace MolecularEvolution {
namespace Parsimony {

char* initMappingCA() {
	char* ptr = new char[16*16];

	char valMask = 0b1111;
	char incMask = 0b1 << 4;

	for(char i=0; i<16; ++i) {
		for(char j=0; j<16; ++j) {
			char val = i & j;
			if((valMask & val) == 0) {
				ptr[i*16+j] = (((i | j) & valMask) | incMask);
			} else {
				ptr[i*16+j] = (val & valMask);
			}
		}
	}

	return ptr;
}


const char* ParsimonyNode::mappingCA = initMappingCA();


ParsimonyNode::ParsimonyNode(const size_t aN, algorithm_t aAlgo) : DAG::BaseNode(), N(aN), N_STATE(4), N_BOOL8X(ceil(N/8.)), algorithm(aAlgo), isLeaf(false) {
	boolArray = NULL;
	rootBA = NULL;
	rootCA = NULL;
	init();
}

ParsimonyNode::ParsimonyNode(const size_t aN, algorithm_t aAlgo, const std::string &aName, const DL::CompressedAlignements& ca) :
		DAG::BaseNode(), N(aN), N_STATE(4), N_BOOL8X(ceil(N/8.)), algorithm(aAlgo), isLeaf(true) {
	boolArray = NULL;
	rootBA = NULL;
	rootCA = NULL;
	init(aName, ca);
}

ParsimonyNode::~ParsimonyNode() {
}

void ParsimonyNode::init() {
	changeCountVar = 0;
	if(algorithm == FITCH) {
		changeCount.resize(N);
		changeCount.setZero();
		possibleNucl.resize(N);
	} else if(algorithm == FITCH_FAST) {
		changeCount.resize(N);
		changeCount.setZero();
		possibleNuclBS.resize(N);
	} else if(algorithm == FITCH_FAST2) {
		//changeCount.resize(N);
		//changeCount.setZero();
		boolArray = new boolx8_t[N_BOOL8X];
	} else if(algorithm == FITCH_FAST3) {
		//changeCount.resize(N);
		//changeCount.setZero();
		charArray = new char[N];
	} else {
		changes.resize(N, N_STATE);
		costMatrix.resize(N_STATE, N_STATE);
		costMatrix.setOnes();
		costMatrix = costMatrix - Eigen::MatrixXd::Identity(N_STATE, N_STATE);
	}
}

void ParsimonyNode::init(const std::string &aName, const DL::CompressedAlignements& ca) {
	changeCountVar = 0;
	if(algorithm == FITCH) {
		changeCount.resize(N);
		changeCount.setZero();
		possibleNucl.resize(N);

		for	(size_t iS=0; iS<N; ++iS) {
			DL::Alignment::vecCode_t vals = ca.getSiteCode(aName,iS);
			possibleNucl[iS].insert(vals.begin(), vals.end());
		}
	} else if(algorithm == FITCH_FAST) {
		changeCount.resize(N);
		changeCount.setZero();
		possibleNuclBS.resize(N);
		for	(size_t iS=0; iS<N; ++iS) {
			possibleNuclBS[iS].reset();
			DL::Alignment::vecCode_t vals = ca.getSiteCode(aName,iS);
			for(size_t iV=0; iV<vals.size(); ++iV) {
				possibleNuclBS[iS][vals[iV]] = true;
			}
		}
	} else if(algorithm == FITCH_FAST2) {
		//changeCount.resize(N);
		//changeCount.setZero();
		boolArray = new boolx8_t[N_BOOL8X];

		for	(size_t iS=0; iS<N_BOOL8X; ++iS) {
			boolArray[iS] = 0b0;
			for (size_t iB=0, iP=iS*8; iB<8 && iP<N; ++iB, ++iP) {
				boolx8_t val = 0b0;

				DL::Alignment::vecCode_t vals = ca.getSiteCode(aName, iP);
				for(size_t iV=0; iV<vals.size(); ++iV) {
					val |= 0b1 << vals[iV];
				}
				boolArray[iS] |= val << iB*4;
			}
		}
	} else if(algorithm == FITCH_FAST3) {
		//changeCount.resize(N);
		//changeCount.setZero();
		charArray = new char[N];

		for	(size_t iS=0; iS<N; ++iS) {
			charArray[iS] = 0b0;

			char val = 0b0;
			DL::Alignment::vecCode_t vals = ca.getSiteCode(aName, iS);
			for(size_t iV=0; iV<vals.size(); ++iV) {
				val |= 0b1 << vals[iV];
			}
			charArray[iS] = val;
		}
	} else {
		changes.resize(N, N_STATE);
		changes.setOnes();
		changes *= std::numeric_limits<double>::max();
		for	(size_t iS=0; iS<N; ++iS) {
			DL::Alignment::vecCode_t vals = ca.getSiteCode(aName,iS);
			for(size_t iCod=0; iCod < vals.size(); ++iCod) {
				changes(iS, vals[iCod]) = 0.0;
			}
		}

		costMatrix.resize(N_STATE, N_STATE);
		costMatrix.setOnes();
		costMatrix = costMatrix - Eigen::MatrixXd::Identity(N_STATE, N_STATE);
	}
}


void ParsimonyNode::doProcessingFitch(const Eigen::VectorXi& changeCountC1, const Eigen::VectorXi& changeCountC2,
		const std::vector<std::set< DL::Alignment::code_t > >& possibleNuclC1, const std::vector<std::set< DL::Alignment::code_t > >& possibleNuclC2) {

	// Sum children change count
	changeCount = changeCountC1 + changeCountC2;

	// Compute the intersection of possible nucl and count the number of changes according to Fitch algo.
	for	(size_t iS=0; iS<N; ++iS) {
		const std::set<DL::Alignment::code_t>& x1 = possibleNuclC1[iS];
		const std::set<DL::Alignment::code_t>& x2 = possibleNuclC2[iS];

		possibleNucl[iS].clear();
		std::set_intersection(x1.begin(), x1.end(),
							  x2.begin(), x2.end(),
							  std::inserter(possibleNucl[iS], possibleNucl[iS].begin()));

		if(possibleNucl[iS].empty()) {
			changeCount(iS) += 1;
			std::set_union(x1.begin(), x1.end(),
						   x2.begin(), x2.end(),
						   std::inserter(possibleNucl[iS], possibleNucl[iS].begin()));
		}
	}
}

void ParsimonyNode::doProcessingFitchFast(const Eigen::VectorXi& changeCountC1, const Eigen::VectorXi& changeCountC2,
		const std::vector<std::bitset< 4 > >& possibleNuclBSC1, const std::vector< std::bitset< 4 > >& possibleNuclBSC2) {

	// Sum children change count
	changeCount = changeCountC1 + changeCountC2;

	// Compute the intersection of possible nucl and count the number of changes according to Fitch algo.
	for	(size_t iS=0; iS<N; ++iS) {
		const std::bitset< 4 >& x1 = possibleNuclBSC1[iS];
		const std::bitset< 4 >& x2 = possibleNuclBSC2[iS];

		possibleNuclBS[iS] = x1 & x2;

		if(possibleNuclBS[iS].count() == 0) {
			changeCount(iS) += 1;
			possibleNuclBS[iS] = x1 | x2;
		}
	}
}

void ParsimonyNode::doProcessingFitchFast2(size_t changeCountC1, size_t changeCountC2, const boolx8_t* boolArray1, const boolx8_t* boolArray2) {

	// Sum children change count
	changeCountVar = changeCountC1 + changeCountC2;

	for	(size_t iS=0; iS<N_BOOL8X; ++iS) {
		boolArray[iS] = boolArray1[iS] & boolArray2[iS];
		boolx8_t tmp =  boolArray1[iS] | boolArray2[iS];
		boolx8_t mask = 0b1111;
		for (size_t iP=iS*8; iP < (iS+1)*8 && iP < N; ++iP) {
			if((boolArray[iS] & mask) == 0) {
				changeCountVar += 1;
				boolArray[iS] |= (tmp & mask);
			}
			mask = mask << 4;
		}

	}
}

void ParsimonyNode::doProcessingFitchFast3(size_t changeCountC1, size_t changeCountC2, const char* charArray1, const char* charArray2) {

	// Sum children change count
	changeCountVar = changeCountC1 + changeCountC2;

	char valMask = 0b1111;
	for	(unsigned int iS=0; iS<N; ++iS) {
		char res = mappingCA[charArray1[iS]*16+charArray2[iS]];
		charArray[iS] = res & valMask;
		changeCountVar += (res >> 4) & 0b1;
	}
}

void ParsimonyNode::doProcessingSankoff(const Eigen::MatrixXd& changesC1, const Eigen::MatrixXd& changesC2) {
	for	(size_t iS=0; iS<N; ++iS) {
		for(size_t i=0; i<N_STATE; ++i) {
			double minChildren1 = (costMatrix.row(i) + changesC1.row(iS)).minCoeff();
			double minChildren2 = (costMatrix.row(i) + changesC2.row(iS)).minCoeff();
			changes(iS, i) = minChildren1 + minChildren2;
		}
	}
}



void ParsimonyNode::doProcessing() {

	if(!isLeaf) {
		assert(children.size() > 1 && "[ParsimonyNode] Error in the tree topology - there are less than 2 children.");

		if(algorithm == FITCH_FAST2) {
			doProcessingFitchFast2(children[0]->getChangeCountVar(), children[1]->getChangeCountVar(), children[0]->getPossibleNuclBoolArray(), children[1]->getPossibleNuclBoolArray());

			// If we are at the top node of a unrooted tree
			if(children.size() == 3) {

				if(rootBA == NULL) rootBA = new boolx8_t[N_BOOL8X];

				size_t tmpChangeCnt = changeCountVar;
				for(size_t i=0; i<ceil(N/8.); ++i ) {
					rootBA[i] = boolArray[i];
				}
				doProcessingFitchFast2(tmpChangeCnt, children[2]->getChangeCountVar(), rootBA, children[2]->getPossibleNuclBoolArray());
			}
		} else if(algorithm == FITCH_FAST3) {
			doProcessingFitchFast3(children[0]->getChangeCountVar(), children[1]->getChangeCountVar(), children[0]->getPossibleNuclCharArray(), children[1]->getPossibleNuclCharArray());

			// If we are at the top node of a unrooted tree
			if(children.size() == 3) {

				if(rootCA == NULL) rootCA = new char[N];

				size_t tmpChangeCnt = changeCountVar;
				for(size_t i=0; i<ceil(N); ++i ) {
					rootCA[i] = charArray[i];
				}
				doProcessingFitchFast3(tmpChangeCnt, children[2]->getChangeCountVar(), rootCA, children[2]->getPossibleNuclCharArray());
			}
		} else if(algorithm == FITCH_FAST) {
			doProcessingFitchFast(children[0]->getChangeCount(), children[1]->getChangeCount(), children[0]->getPossibleNuclBS(), children[1]->getPossibleNuclBS());

			// If we are at the top node of a unrooted tree
			if(children.size() == 3) {
				Eigen::VectorXi tmpChangeCnt = changeCount;
				std::vector< std::bitset< 4 > > tmpPossibleNuclBS = possibleNuclBS;
				doProcessingFitchFast(tmpChangeCnt, children[2]->getChangeCount(), tmpPossibleNuclBS, children[2]->getPossibleNuclBS());
			}
		} else if(algorithm == FITCH) {
			doProcessingFitch(children[0]->getChangeCount(), children[1]->getChangeCount(), children[0]->getPossibleNucl(), children[1]->getPossibleNucl());

			// If we are at the top node of a unrooted tree
			if(children.size() == 3) {
				Eigen::VectorXi tmpChangeCnt = changeCount;
				std::vector< std::set<DL::Alignment::code_t> > tmpPossibleNucl = possibleNucl;
				doProcessingFitch(tmpChangeCnt, children[2]->getChangeCount(), tmpPossibleNucl, children[2]->getPossibleNucl());
			}

		} else {
			doProcessingSankoff(children[0]->getChanges(), children[1]->getChanges());

			// If we are at the top node of a unrooted tree
			if(children.size() == 3) {
				Eigen::MatrixXd tmpChanges = changes;
				doProcessingSankoff(tmpChanges, children[2]->getChanges());
			}
		}
	}
}

bool ParsimonyNode::processSignal(DAG::BaseNode* aChild) {
	return true;

}

void ParsimonyNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(ParsimonyNode)){
		children.push_back(dynamic_cast<ParsimonyNode*>(aChild));
	} else {
		assert(false && "[ParsimonyNode] Wrong type of DAG node.");
	}
}

void ParsimonyNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
		if(childtype == typeid(ParsimonyNode)){
			ParsimonyNode* childNode = dynamic_cast<ParsimonyNode*>(aChild);
			std::vector<ParsimonyNode*>::iterator pos = std::find(children.begin(), children.end(), childNode);
			if (pos != children.end()) {
				children.erase(pos);
			}
			//assert(pos != children.end());
		} else {
			assert(false && "[ParsimonyNode] Wrong type of DAG node.");
		}
}

const std::vector< std::set<DL::Alignment::code_t> >& ParsimonyNode::getPossibleNucl() const {
	return possibleNucl;
}

const std::vector< std::bitset < 4 > >& ParsimonyNode::getPossibleNuclBS() const {
	return possibleNuclBS;
}

const ParsimonyNode::boolx8_t* ParsimonyNode::getPossibleNuclBoolArray() const {
	return boolArray;
}

const Eigen::VectorXi& ParsimonyNode::getChangeCount() const {
	return changeCount;
}

const Eigen::MatrixXd& ParsimonyNode::getChanges() const {
	return changes;
}

size_t ParsimonyNode::getChangeCountVar() const {
	return changeCountVar;
}

const char* ParsimonyNode::getPossibleNuclCharArray() const {
	return charArray;
}

std::vector<double> ParsimonyNode::computeParsimonyVector() const {
	std::vector<double> parsimonyVector(N);
	if((algorithm == FITCH || algorithm == FITCH_FAST)) {
		for(size_t i=0; i<N; ++i) {
			parsimonyVector[i] = changeCount(i);
		}
	} else {
		for(size_t i=0; i<N; ++i) {
			parsimonyVector[i] = changes.row(i).minCoeff();
		}
	}

	return parsimonyVector;
}

double ParsimonyNode::computeParsimonyScore() const {
	double parsimonyScore = 0.;
	if(algorithm == FITCH_FAST2 || algorithm == FITCH_FAST3) {
		parsimonyScore = changeCountVar;
	} else if (algorithm == FITCH || algorithm == FITCH_FAST) {
		parsimonyScore = changeCount.sum();
	} else {
		for(size_t i=0; i<N; ++i) {
			parsimonyScore += changes.row(i).minCoeff();
		}
	}
	return parsimonyScore;
}

bool ParsimonyNode::isLeafNode() const {
	return isLeaf;
}

} /* namespace Parsimony */
} /* namespace MolecularEvolution */
