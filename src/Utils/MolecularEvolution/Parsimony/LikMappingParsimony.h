/*
 * LikMappingParsimony.h
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#ifndef UTILS_MOLECULAREVOLUTION_PARSIMONY_LIKMAPPINGPARSIMONY_H_
#define UTILS_MOLECULAREVOLUTION_PARSIMONY_LIKMAPPINGPARSIMONY_H_

#include "Utils/MolecularEvolution/Parsimony/Nodes/ParsimonyNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/LikelihoodMapping.h"

namespace MolecularEvolution {
namespace Parsimony {

namespace TR = ::MolecularEvolution::TreeReconstruction;

class LikMappingParsimony: public TreeReconstruction::LikelihoodMapper::LikelihoodMapping {
public:
	typedef boost::shared_ptr<LikMappingParsimony> sharedPtr_t;
public:
	LikMappingParsimony(const std::string aName);
	~LikMappingParsimony();

	void setParsimonyNode(ParsimonyNode* aNode);
	ParsimonyNode* getParsimonyNode();

	void doAddChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping);
	void doRemoveChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping);

	void doOnApplyChange();
	void doOnUpdate(std::set<DAG::BaseNode*> &updatedNodes);

	const std::string& getName() const;

private:

	typedef std::vector<ParsimonyNode *> treeStructDAG_t;

	/* Default data */
	std::string name;

	/* Linkage with DAG elements */
	ParsimonyNode *parsimonyNode;

};

} /* namespace Parsimony */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_PARSIMONY_LIKMAPPINGPARSIMONY_H_ */
