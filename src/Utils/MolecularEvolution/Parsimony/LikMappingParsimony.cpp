/*
 * LikMappingParsimony.cpp
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#include "LikMappingParsimony.h"

namespace MolecularEvolution {
namespace Parsimony {

LikMappingParsimony::LikMappingParsimony(const std::string aName) : TreeReconstruction::LikelihoodMapper::LikelihoodMapping() {
	name = aName;
	parsimonyNode = NULL;
}

LikMappingParsimony::~LikMappingParsimony() {
}

void LikMappingParsimony::setParsimonyNode(ParsimonyNode* aNode) {
	parsimonyNode = aNode;
}

ParsimonyNode* LikMappingParsimony::getParsimonyNode() {
	return parsimonyNode;
}

void LikMappingParsimony::doAddChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping) {

	LikMappingParsimony* childParsimony = dynamic_cast<LikMappingParsimony*>(childLikMapping.get());
	assert(childParsimony);

	// Add DAG link
	if(parsimonyNode != NULL) {
		assert(childParsimony->getParsimonyNode() && "Make sure add is correct.");
		parsimonyNode->addChild(childParsimony->getParsimonyNode());
	}
}

void LikMappingParsimony::doRemoveChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping) {

	LikMappingParsimony* childParsimony = dynamic_cast<LikMappingParsimony*>(childLikMapping.get());
	assert(childParsimony);

	// Remove DAG link
	if(parsimonyNode != NULL) {
		assert(childParsimony->getParsimonyNode() && "Make sure remove is correct.");
		parsimonyNode->removeChild(childParsimony->getParsimonyNode());
	}
}

void LikMappingParsimony::doOnApplyChange() {
	parsimonyNode->setDone();
}

void LikMappingParsimony::doOnUpdate(std::set<DAG::BaseNode*> &updatedNodes) {
	assert(parsimonyNode && "On update - no parsimony node are found.");
	if(!parsimonyNode->isLeafNode()) {
		parsimonyNode->updated();
		updatedNodes.insert(parsimonyNode);
	}
}

const std::string& LikMappingParsimony::getName() const {
	return name;
}


} /* namespace Parsimony */
} /* namespace MolecularEvolution */
