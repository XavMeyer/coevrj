/*
 * FastFitchEvaluator.h
 *
 *  Created on: Apr 19, 2018
 *      Author: meyerx
 */

#ifndef UTILS_MOLECULAREVOLUTION_PARSIMONY_FASTFITCHEVALUATOR_H_
#define UTILS_MOLECULAREVOLUTION_PARSIMONY_FASTFITCHEVALUATOR_H_

#include "DAG/Scheduler/BaseScheduler.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeManager.h"
#include "Utils/MolecularEvolution/Parsimony/Nodes/types.h"
#include "Utils/MolecularEvolution/Parsimony/Nodes/FastFitchNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/TreeMapper.h"

#include <map>
#include <boost/smart_ptr/shared_ptr.hpp>


namespace MolecularEvolution { namespace DataLoader { class NewickParser; } }

namespace MolecularEvolution { namespace TreeReconstruction { class Tree; } }

namespace MolecularEvolution {
namespace Parsimony {


namespace TR = ::MolecularEvolution::TreeReconstruction;
namespace DL = ::MolecularEvolution::DataLoader;


class FastFitchEvaluator {
public:
	typedef boost::shared_ptr<FastFitchEvaluator> sharedPtr_t;

public:
	FastFitchEvaluator(DL::MSA::SEQUENCE_TYPE aSeqType, const std::string &aFileAlign, TR::TreeManager::sharedPtr_t aTreeManager);
	~FastFitchEvaluator();

	double evaluate(TR::Tree::sharedPtr_t curTree);
	double update(TR::Tree::sharedPtr_t curTree);

	size_t getNSite() const;

	void signalUpdatedTreeNodes(const std::vector<TR::TreeNode *> &nodes);
	void signalTreeImport();

private:

	bool isTreeImported;
	std::size_t nSite;

	TR::TreeManager::sharedPtr_t treeManager;
	TR::LikelihoodMapper::TreeMapper treeMapper;

	TR::LikelihoodMapper::TreeNode *rootTree;
	FastFitchNode *rootDAG;

	std::vector<TR::TreeNode *> pendingUpdatedNodes;

	DAG::Scheduler::BaseScheduler *scheduler;

	typedef std::map<size_t, TR::LikelihoodMapper::TreeNode*> branchMap_t;
	branchMap_t branchMap;

	void createTreeAndDAG( DL::MSA::SEQUENCE_TYPE seqType, const std::string &aFileAlign);
	void createTree(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TR::LikelihoodMapper::TreeNode *node);
	void createSubDAG(TR::LikelihoodMapper::TreeNode *node, DAG::BaseNode *nodeDAG, DL::CompressedAlignements &compressedAligns);

};

} /* namespace Parsimony */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_PARSIMONY_FASTFITCHEVALUATOR_H_ */
