/*
 * ParsimonyEvaluator.cpp
 *
 *  Created on: Apr 19, 2018
 *      Author: meyerx
 */


#include <Utils/MolecularEvolution/Parsimony/LikMappingParsimony.h>
#include "ParsimonyEvaluator.h"
#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"

namespace MolecularEvolution {
namespace Parsimony {

ParsimonyEvaluator::ParsimonyEvaluator(algorithm_t aAlgo,  DL::MSA::SEQUENCE_TYPE aSeqType, const std::string &aFileAlign, TR::TreeManager::sharedPtr_t aTreeManager) :
		algorithm(aAlgo), treeManager(aTreeManager), treeMapper(branchMap){


	createTreeAndDAG(aSeqType, aFileAlign);

	scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
}


ParsimonyEvaluator::~ParsimonyEvaluator() {
}


double ParsimonyEvaluator::evaluate(TR::Tree::sharedPtr_t curTree) {
	std::set<DAG::BaseNode*> updatedNodes;
	treeMapper.fullUpdateTreeAndDAG(curTree, rootTree, updatedNodes);

	scheduler->resetDAG();
	scheduler->process();
	return rootDAG->computeParsimonyScore();
}

double ParsimonyEvaluator::update(TR::Tree::sharedPtr_t curTree) {
	std::set<DAG::BaseNode*> updatedNodes;

	treeMapper.partialUpdateTreeAndDAG(curTree, updatedNodes);

	scheduler->resetPartial(updatedNodes);

	scheduler->process();

	double score = rootDAG->computeParsimonyScore();

	return score;
}

void ParsimonyEvaluator::createTreeAndDAG(DL::MSA::SEQUENCE_TYPE aSeqType, const std::string &aFileAlign) {

	DL::FastaReader fr(aFileAlign);
	DL::MSA msa(aSeqType, fr.getAlignments(), false);
	DL::CompressedAlignements compressedAligns(msa, false, false);

	// Tree
	LikMappingParsimony::sharedPtr_t rootLikMapping(new LikMappingParsimony(treeManager->getTreeNodeName(treeManager->getCurrentTree()->getRoot())));
	rootTree = new TR::LikelihoodMapper::TreeNode(treeManager->getCurrentTree()->getRoot(), rootLikMapping);
	branchMap.insert(std::make_pair(rootTree->getId(), rootTree));
	createTree(treeManager->getCurrentTree()->getRoot(), NULL,  rootTree);

	// DAG
	rootDAG = new ParsimonyNode(compressedAligns.getNSite(), algorithm);
	rootLikMapping->setParsimonyNode(rootDAG);
	createSubDAG(rootTree, rootDAG, compressedAligns);
}

void ParsimonyEvaluator::createTree(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TR::LikelihoodMapper::TreeNode *node) {

	TR::vecTN_t childrenTR = nodeTR->getChildren(parentNodeTR);

	if(childrenTR.size() > 0) { // If node has children, process
		// Set boolean info.
		node->setLeaf(false);

		// For all children in newick, create in PosSel
		for(size_t iC=0; iC<childrenTR.size(); ++iC){
			LikMappingParsimony::sharedPtr_t likMapping(new LikMappingParsimony(treeManager->getTreeNodeName(childrenTR[iC])));
			TR::LikelihoodMapper::TreeNode *childNode = new TR::LikelihoodMapper::TreeNode(childrenTR[iC], likMapping);
			branchMap.insert(std::make_pair(childNode->getId(), childNode));

			createTree(childrenTR[iC], nodeTR, childNode);
			node->addChild(childNode);
		}
	} else { // Else it is a leaf node
		node->setLeaf(true);
	}
}

void ParsimonyEvaluator::createSubDAG(TR::LikelihoodMapper::TreeNode *node, DAG::BaseNode *nodeDAG, DL::CompressedAlignements &compressedAligns) {

	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		DAG::BaseNode *newNodeDAG;
		TR::LikelihoodMapper::TreeNode *childNode = node->getChild(i);
		LikMappingParsimony* likMapping = static_cast<LikMappingParsimony*>(childNode->getLikelihoodMapping().get());

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			ParsimonyNode *newNode;
			newNode = new ParsimonyNode(compressedAligns.getNSite(), algorithm, likMapping->getName(), compressedAligns);
			likMapping->setParsimonyNode(newNode);
			newNodeDAG = newNode;
		} else { // create CPV element
			ParsimonyNode *newNode;
			newNode = new ParsimonyNode(compressedAligns.getNSite(), algorithm);
			likMapping->setParsimonyNode(newNode);
			newNodeDAG = newNode;

			// create sub-DAG
			createSubDAG(childNode, newNode, compressedAligns);
		}

		// (2) We add the child node created to the current node (register to father).
		nodeDAG->addChild(newNodeDAG);
	}

}


} /* namespace Parsimony */
} /* namespace MolecularEvolution */
