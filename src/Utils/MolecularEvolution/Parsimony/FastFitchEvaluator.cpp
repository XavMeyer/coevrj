/*
 * FastFitchEvaluator.cpp
 *
 *  Created on: Apr 19, 2018
 *      Author: meyerx
 */


#include "Utils/MolecularEvolution/Parsimony/LikMappingFastFitch.h"
#include "FastFitchEvaluator.h"
#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"

namespace MolecularEvolution {
namespace Parsimony {

FastFitchEvaluator::FastFitchEvaluator(DL::MSA::SEQUENCE_TYPE aSeqType, const std::string &aFileAlign, TR::TreeManager::sharedPtr_t aTreeManager) :
		treeManager(aTreeManager), treeMapper(branchMap){


	isTreeImported = false;

	createTreeAndDAG(aSeqType, aFileAlign);

	scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
}


FastFitchEvaluator::~FastFitchEvaluator() {
}


double FastFitchEvaluator::evaluate(TR::Tree::sharedPtr_t curTree) {
	std::set<DAG::BaseNode*> updatedNodes;

	isTreeImported = false;
	pendingUpdatedNodes.clear();

	treeMapper.fullUpdateTreeAndDAG(curTree, rootTree, updatedNodes);

	scheduler->resetDAG();
	scheduler->process();
	return rootDAG->getParsimonyScore();
}

double FastFitchEvaluator::update(TR::Tree::sharedPtr_t curTree) {
	std::set<DAG::BaseNode*> updatedNodes;

	if(isTreeImported) {
		return evaluate(curTree);
	}

	curTree->addUpdatedNodes(pendingUpdatedNodes);
	pendingUpdatedNodes.clear();

	treeMapper.partialUpdateTreeAndDAG(curTree, updatedNodes);

	scheduler->resetPartial(updatedNodes);

	scheduler->process();

	double score = rootDAG->getParsimonyScore();

	return score;
}

size_t FastFitchEvaluator::getNSite() const {
	return nSite;
}


void FastFitchEvaluator::signalUpdatedTreeNodes(const std::vector<TR::TreeNode *> &nodes) {
	pendingUpdatedNodes.insert(pendingUpdatedNodes.end(), nodes.begin(), nodes.end());
}

void FastFitchEvaluator::signalTreeImport() {
	isTreeImported = true;
}


void FastFitchEvaluator::createTreeAndDAG(DL::MSA::SEQUENCE_TYPE aSeqType, const std::string &aFileAlign) {

	DL::FastaReader fr(aFileAlign);
	DL::MSA msa(aSeqType, fr.getAlignments(), false);
	DL::CompressedAlignements compressedAligns(msa, false, false);

	nSite = msa.getNSite();

	// Tree
	LikMappingFastFitch::sharedPtr_t rootLikMapping(new LikMappingFastFitch(treeManager->getTreeNodeName(treeManager->getCurrentTree()->getRoot())));
	rootTree = new TR::LikelihoodMapper::TreeNode(treeManager->getCurrentTree()->getRoot(), rootLikMapping);
	branchMap.insert(std::make_pair(rootTree->getId(), rootTree));
	createTree(treeManager->getCurrentTree()->getRoot(), NULL,  rootTree);

	// DAG
	rootDAG = new FastFitchNode(compressedAligns.getNSite(), true);
	rootLikMapping->setFastFitchNode(rootDAG);
	createSubDAG(rootTree, rootDAG, compressedAligns);
}

void FastFitchEvaluator::createTree(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TR::LikelihoodMapper::TreeNode *node) {

	TR::vecTN_t childrenTR = nodeTR->getChildren(parentNodeTR);

	if(childrenTR.size() > 0) { // If node has children, process
		// Set boolean info.
		node->setLeaf(false);

		// For all children in newick, create in PosSel
		for(size_t iC=0; iC<childrenTR.size(); ++iC){
			LikMappingFastFitch::sharedPtr_t likMapping(new LikMappingFastFitch(treeManager->getTreeNodeName(childrenTR[iC])));
			TR::LikelihoodMapper::TreeNode *childNode = new TR::LikelihoodMapper::TreeNode(childrenTR[iC], likMapping);
			branchMap.insert(std::make_pair(childNode->getId(), childNode));

			createTree(childrenTR[iC], nodeTR, childNode);
			node->addChild(childNode);
		}
	} else { // Else it is a leaf node
		node->setLeaf(true);
	}
}

void FastFitchEvaluator::createSubDAG(TR::LikelihoodMapper::TreeNode *node, DAG::BaseNode *nodeDAG, DL::CompressedAlignements &compressedAligns) {

	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		DAG::BaseNode *newNodeDAG;
		TR::LikelihoodMapper::TreeNode *childNode = node->getChild(i);
		LikMappingFastFitch* likMapping = static_cast<LikMappingFastFitch*>(childNode->getLikelihoodMapping().get());

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			FastFitchNode *newNode;
			newNode = new FastFitchNode(compressedAligns.getNSite(), likMapping->getName(), compressedAligns);
			likMapping->setFastFitchNode(newNode);
			newNodeDAG = newNode;
		} else { // create CPV element
			FastFitchNode *newNode;
			newNode = new FastFitchNode(compressedAligns.getNSite(), false);
			likMapping->setFastFitchNode(newNode);
			newNodeDAG = newNode;

			// create sub-DAG
			createSubDAG(childNode, newNode, compressedAligns);
		}

		// (2) We add the child node created to the current node (register to father).
		nodeDAG->addChild(newNodeDAG);
	}

}


} /* namespace Parsimony */
} /* namespace MolecularEvolution */
