/*
 * LikMappingFastFitch.h
 *
 *  Created on: Apr 24, 2018
 *      Author: meyerx
 */

#ifndef UTILS_MOLECULAREVOLUTION_PARSIMONY_LIKMAPPINGFASTFITCH_H_
#define UTILS_MOLECULAREVOLUTION_PARSIMONY_LIKMAPPINGFASTFITCH_H_

#include "Utils/MolecularEvolution/Parsimony/Nodes/FastFitchNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/LikelihoodMapper/LikelihoodMapping.h"

namespace MolecularEvolution {
namespace Parsimony {

namespace TR = ::MolecularEvolution::TreeReconstruction;

class LikMappingFastFitch: public TreeReconstruction::LikelihoodMapper::LikelihoodMapping {
public:
	typedef boost::shared_ptr<LikMappingFastFitch> sharedPtr_t;
public:
	LikMappingFastFitch(const std::string aName);
	~LikMappingFastFitch();

	void setFastFitchNode(FastFitchNode* aNode);
	FastFitchNode* getFastFitchNode();

	void doAddChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping);
	void doRemoveChild(TR::LikelihoodMapper::LikelihoodMapping::sharedPtr_t childLikMapping);

	void doOnApplyChange();
	void doOnUpdate(std::set<DAG::BaseNode*> &updatedNodes);

	const std::string& getName() const;

private:

	/* Default data */
	std::string name;

	/* Linkage with DAG elements */
	FastFitchNode *fastFitchNode;

};

} /* namespace Parsimony */
} /* namespace MolecularEvolution */

#endif /* UTILS_MOLECULAREVOLUTION_PARSIMONY_LIKMAPPINGFASTFITCH_H_ */
