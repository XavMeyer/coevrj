/*
 * CacheLRU.h
 *
 *  Created on: Feb 21, 2019
 *      Author: meyerx
 */

#ifndef UTILS_CODE_CACHELRU_H_
#define UTILS_CODE_CACHELRU_H_

#include <iostream>
#include <assert.h>

#include <map>
#include <vector>
#include <list>

namespace MolecularEvolution {
namespace Parsimony {

class CacheLRU {

public:

	friend CacheLRU& getCache();

	bool addElement(size_t key, double score);
	void addNewElement(size_t key, double score);
	bool getElement(size_t key, double &score);

	void setLimits(size_t aTimeLimit, size_t aMemorySize);

	std::string toString() const;

	void enable();
	void disable();
	bool isEnabled();

private:
	CacheLRU();
	~CacheLRU();

	// Not defined to avoid call
	CacheLRU(const CacheLRU&);
	CacheLRU& operator=(const CacheLRU&);

private:

	static const size_t DEFAULT_TIME_LIMIT, DEFAULT_MEMORY_SIZE;

	bool enabled;
	size_t TIME_LIMIT, MEMORY_SIZE;
	size_t clock;

	class ParsimonyScore {
	public:
		size_t key;
		double score;
		size_t time;

		ParsimonyScore(size_t aKey, double aScore, size_t aTime) : key(aKey), score(aScore), time(aTime) {}
		~ParsimonyScore(){}
	};

	typedef std::list< ParsimonyScore* > parsimonyScoreList_t;
	typedef parsimonyScoreList_t::iterator itPScoreList_t;

	typedef std::map< size_t, itPScoreList_t > parsimonyScoreMap_t;

	parsimonyScoreMap_t psMap;
	parsimonyScoreList_t psList;

	void cleanUp();
	void removeLast();

	ParsimonyScore* updateElement(size_t key, parsimonyScoreMap_t::iterator itMap);

};

inline CacheLRU& getCache() {
    static CacheLRU instance;
    return instance;
}

} /* namespace Parsimony */
} /* namespace MolecularEvolution */

#endif /* UTILS_CODE_CACHELRU_H_ */
