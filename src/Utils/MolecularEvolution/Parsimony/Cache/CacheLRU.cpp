/*
 * CacheLRU.cpp
 *
 *  Created on: Feb 21, 2019
 *      Author: meyerx
 */

#include "CacheLRU.h"

#include <limits>
#include <iostream>
#include <sstream>

namespace MolecularEvolution {
namespace Parsimony {

const size_t CacheLRU::DEFAULT_TIME_LIMIT = 5e5;
const size_t CacheLRU::DEFAULT_MEMORY_SIZE = 5e5;

CacheLRU::CacheLRU() : TIME_LIMIT(DEFAULT_TIME_LIMIT), MEMORY_SIZE(DEFAULT_MEMORY_SIZE) {
	clock = 0;
	enabled = false;
}


CacheLRU::~CacheLRU() {
}

void CacheLRU::setLimits(size_t aTimeLimit, size_t aMemorySize) {
	TIME_LIMIT = aTimeLimit;
	MEMORY_SIZE = aMemorySize;
}



bool CacheLRU::addElement(size_t key, double score) {

	assert(enabled);

	// --------------- INSERTION ------------------//
	parsimonyScoreMap_t::iterator itMap = psMap.find(key);

	bool isNew = (itMap == psMap.end());
	if(isNew) {
		addNewElement(key, score);
	} else {
		ParsimonyScore* psPtr = updateElement(key, itMap);
		assert(psPtr->score == score && "Updated element has a different parsimony score.");
	}

	return isNew;
}

void CacheLRU::addNewElement(size_t key, double score) {

	assert(enabled);

	// Create new element
	ParsimonyScore* pScorePtr = new ParsimonyScore(key, score, clock);
	// Add it at the beginning of the list
	psList.push_front(pScorePtr);
	// Recover the iterator (list iterator should not be affected by list alterations)
	itPScoreList_t newIt = psList.begin();
	// Memorize the list iterator in the map
	std::pair<size_t, itPScoreList_t> pair = std::make_pair(key, newIt);

	std::pair< parsimonyScoreMap_t::iterator , bool > ret;
	ret = psMap.insert(pair);
	assert(ret.second && "Forced insert failed - element was already existing.");

	clock++;

	// Clean-up: Remove old, oversized and rescale clocks
	cleanUp();
}

CacheLRU::ParsimonyScore* CacheLRU::updateElement(size_t key, parsimonyScoreMap_t::iterator itMap) {

	assert(enabled);

	// Recover the iterator to the element in the list
	itPScoreList_t oldIt = itMap->second;
	// Recover the element
	ParsimonyScore* pScorePtr = *oldIt;
	// Delete the iterator
	psList.erase(oldIt);
	// Update the time
	pScorePtr->time = clock;
	// Insert in the front of the list
	psList.push_front(pScorePtr);
	// Recover the iterator (list iterator should not be affected by list alterations)
	itPScoreList_t newIt = psList.begin();
	// Modify the map
	itMap->second = newIt;

	clock++;

	// Clean-up: Remove old, oversized and rescale clocks
	cleanUp();

	assert(pScorePtr && "Updating a NULL element.");
	return pScorePtr;
}


void CacheLRU::cleanUp() {

	assert(enabled);

	// 1) Check clock
	if(clock == std::numeric_limits<size_t>::max()) { // Reset time
		//std::cout << "Reset clock" << std::endl;
		// Get smallest time
		size_t offset = psList.back()->time;
		// Rescale based on this time
		for(itPScoreList_t it=psList.begin(); it != psList.end(); ++it) {
			assert((*it)->time >= offset);
			(*it)->time -= offset;
		}
		clock -= offset;
	}

	// 2) Check size
	int nElemOver = (long int)psList.size() - (long int)MEMORY_SIZE;
	for(long int i=0; i<nElemOver; ++i) {
		//std::cout << "Size cleanup" << std::endl;
		removeLast();
	}

	// 3) Check time
	while(!psList.empty() && (clock-psList.back()->time) > TIME_LIMIT) {
		//std::cout << "Time cleanup" << std::endl;
		//std::cout << "Last clock : " << psList.back()->time << std::endl;
		//std::cout << "Current clock : " << clock << std::endl;
		removeLast();
	}

}


void CacheLRU::removeLast() {
	// Recover element
	ParsimonyScore* pScore = psList.back();
	// Remove from list
	psList.pop_back();
	// Remove from map
	psMap.erase(pScore->key);
	// Erase memory
	delete pScore;
}


bool CacheLRU::getElement(size_t key, double &score) {

	assert(enabled);

	parsimonyScoreMap_t::iterator itMap = psMap.find(key);

	bool found = itMap != psMap.end();

	if(found) {
		ParsimonyScore* pScorePtr = updateElement(key, itMap);
		score = pScorePtr->score;
	} else {
		score = -1;
	}

	return found;
}


std::string CacheLRU::toString() const {

	std::stringstream ss;

	ss << "Size : " << psList.size() << std::endl;
	for(parsimonyScoreList_t::const_iterator it = psList.begin(); it != psList.end(); ++it) {
		ss << (*it)->key << "\t" << (*it)->score << "\t" << (*it)->time << std::endl;
	}
	ss << "------------------------------------" << std::endl;

	return ss.str();
}

void CacheLRU::enable() {
	enabled = true;
}

void CacheLRU::disable() {
	enabled = false;
}

bool CacheLRU::isEnabled() {
	return enabled;
}



} /* namespace Parsimony */
} /* namespace MolecularEvolution */
