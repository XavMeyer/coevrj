//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReaderXML.h
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef READERXML_H_
#define READERXML_H_

#include <stddef.h>
#include <iostream>
#include <string>

#include "Model/Model.h"
#include "Parallel/Parallel.h" // IWYU pragma: keep
#include "Tag.h"
#include "Utils/XML/HelpersXML.h"
#include "Utils/XML/Likelihood/ReaderLikelihoodXML.h"
#include "Utils/XML/Sampler/ReaderSamplerXML.h"
#include "Utils/XML/Analysis/ReaderAnalysisXML.h"
#include "Utils/XML/TinyXML/tinyxml.h"

namespace Sampler { class BaseSampler; }

namespace XML {

class ReaderXML {
private:
	typedef StatisticalModel::Model::sharedPtr_t modelPtr_t;
	typedef enum {UNKNOWN_RUN, MCMC_RUN, ML_RUN, BEB_RUN, HOLM_BONFERRONI_RUN, LOG_ANALYSIS_RUN} run_t;


public:
	ReaderXML(const std::string &fileName);
	ReaderXML(const std::string &fileName, const std::string &xmlContent);
	~ReaderXML();

	void run();

	// These pointers only during the readerXML scope
	ReaderLikelihoodXML* getPtrLikXML();
	ReaderSamplerXML* getPtrSamplerXML();
	StatisticalModel::Model* getPtrModel();
	Sampler::BaseSampler* getPtrSampler();

	size_t getNIteration();

private:
	static const size_t DEFAULT_SEED;

	run_t runType;
	size_t seed;
	size_t nProposal, nGradient, nChain, nIteration;

	std::ofstream redirectFile;
	std::streambuf *coutbuf;

	TiXmlDocument doc;
	TiXmlElement *pRoot;

	ReaderLikelihoodXML::sharedPtr_t ptrLikXML;
	ReaderSamplerXML::sharedPtr_t ptrSamplerXML;
	ReaderAnalysisXML::sharedPtr_t ptrLogAnalysisXML;

	void initXML();
	void initXML(const std::string &xmlContent);
	void readXML();

	void readXML_MCMC();
	void readXML_Analysis();

	void readLogFile();
	void readSeed();

	void readTopologyMCMC();

	void readNSample();
	void readMaxIteration();

	void readLikelihood();

	void readSampler();
	void readMaximizer();

	void reportStatusMCMC();

	void runLogAnalysis();
};

} /* namespace XML */

#endif /* READERXML_H_ */

