//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Tag.cpp
 *
 *  Created on: May 12, 2015
 *      Author: meyerx
 */

#include "Tag.h"

namespace XML {

// Base
const std::string Tag::ROOT_TAG = "config";
const std::string Tag::SEED_TAG = "seed";
const std::string Tag::N_TIME_PER_BLOCK_TAG = "nSuccinctProposalPerBlock";
const std::string Tag::FREQ_TREE_MOVE_TAG = "treeMoveProportion";
const std::string Tag::TOPOLOGY_TAG = "topology";
const std::string Tag::NPROPOSAL_TAG = "nProposal";
const std::string Tag::NGRADIENT_TAG = "nGradient";
const std::string Tag::NCHAIN_TAG = "nChain";
const std::string Tag::LOG_FILE_TAG = "logFile";
const std::string Tag::STDOUTPUT_FREQUENCY_TAG = "stdOutputFrequency";
// Likelihood
const std::string Tag::LIKELIHOOD_TAG = "likelihood";
// Sampler
const std::string Tag::SAMPLER_TAG = "sampler";
// Anaysis
const std::string Tag::ANALYSIS_TAG = "analysis";
// nSamples
const std::string Tag::NITERATION_TAG = "nIterations";

const std::string Tag::NAME_ATT = "name";
const std::string Tag::METHOD_ATT = "method";

// Likelihood
const std::string Tag::INDEPENDENT_NORMAL_NAME = "independentNormals";
const std::string Tag::CORRELATED_NORMAL_NAME = "correlatedNormals";
const std::string Tag::MULTI_PEAK_NORMAL_NAME = "multiPeakNormals";
const std::string Tag::BDRATE_NAME = "BDRate";
const std::string Tag::BDRATE_UPD_NAME = "BDRateUpd";
const std::string Tag::BDRATE_BIG_UPD_NAME = "BDRateBig";
const std::string Tag::BDRATE_ALPHA_UPD_NAME = "BDRateAlpha";
const std::string Tag::BDRATE_DAG_NAME = "BDRateDAG";
const std::string Tag::BDRATE_ALPHA_DAG_NAME = "BDRateAlphaDAG";
const std::string Tag::SEL_POS_BASE_NAME = "PositiveSelectionBase";
const std::string Tag::SEL_POS_LIGHT_NAME = "PositiveSelectionLight";
const std::string Tag::BRANCH_SITE_REL_NAME = "BranchSiteREL";
const std::string Tag::STOCHASTIC_BS_NAME = "StochasticBS";
const std::string Tag::CODONS_MODEL_NAME = "CodonModels";
const std::string Tag::TREE_INFERENCE_NAME = "TreeInference";
const std::string Tag::TIGAMMA_NAME = "TIGamma";
const std::string Tag::COEV_REVERSIBLE_JUMP_NAME = "CoevReversibleJump";

const std::string Tag::FILE_TAG = "filename";
const std::string Tag::NDIM_TAG = "nDim";
const std::string Tag::NPEAK_TAG = "nPeak";
const std::string Tag::NDATA_TAG = "nData";
const std::string Tag::NTHREAD_TAG = "nThread";
const std::string Tag::N_GAMMA = "nGamma";

const std::string Tag::PARAMETERS_TAG = "parameters";
const std::string Tag::BLOCKS_TAG = "blocks";
const std::string Tag::BLOCK_SIZE_TAG = "blockSize";
const std::string Tag::ADAPTIVE_TYPE_TAG = "adaptiveType";
const std::string Tag::NOT_ADAPTIVE_TAG = "NotAdaptive";
const std::string Tag::DEFAULT_ADAPTIVE_TAG = "DefaultAdaptive";
const std::string Tag::MIXED_ADAPTIVE_TAG = "MixedAdaptive";
const std::string Tag::PCA_ADAPTIVE_TAG = "PCA_Adaptive";
const std::string Tag::INDEPENDENT_GAMMA_TAG = "IndepGammaBL";
const std::string Tag::LANGEVIN_ADAPTIVE_TAG = "LangevinAdaptive";
const std::string Tag::SMALA_ADAPTIVE_TAG = "SMALAAdaptive";
const std::string Tag::PARALLEL_LANGEVIN_ADAPTIVE_TAG = "ParallelLangevin";


const std::string Tag::NQUANTILE_TAG = "nQuantile";
const std::string Tag::ISPLANT_TAG = "isPlant";

const std::string Tag::ALIGN_FILE_TAG = "alignFile";
const std::string Tag::TREE_FILE_TAG = "treeFile";
const std::string Tag::HYPOTHESIS_TAG = "hypothesis";
const std::string Tag::FG_BRANCH_TAG = "foregroundBranch";
const std::string Tag::USE_COMPRESSION_TAG = "isUsingCompression";
const std::string Tag::NUCL_MODEL_TAG = "NucleotideModel_ID";
const std::string Tag::CODON_FREQ_TYPE_TAG = "CodonFrequencyType_ID";
const std::string Tag::SCALING_TYPE_TAG = "scalingType";
const std::string Tag::MODEL_TYPE_TAG = "modelType";

const std::string Tag::TREE_SAMPLING_TAG = "sampleTreeTopology";
const std::string Tag::USE_FIXED_TREE_TOPOLOGY_TAG = "useFixedTree";
const std::string Tag::WITH_TREE_SPLIT_FREQUENCY_TAG = "withTreeSplitFreq";

const std::string Tag::N_CLASS_TAG = "nClass";

const std::string Tag::NGAMMA_TAG = "nGamma";
const std::string Tag::COEV_CLUSTER_FILE_TAG = "coevClusterFile";
const std::string Tag::COEV_RESTART_FILE_TAG = "coevRestartFile";
const std::string Tag::WITH_COEV_TRACE_TAG = "withCoevTrace";
const std::string Tag::USE_STATIONARY_FREQ_GTR_TAG = "sampleStationaryFreqGTR";
const std::string Tag::USE_CACHE_PPI_TAG = "useScoreCaching";
const std::string Tag::USE_COEV_SCALING_TAG = "useCoevScaling";
const std::string Tag::COEV_PRIOR_TYPE_TAG = "coevPriorType";

// Proposals
const std::string Tag::PROPOSAL_TAG = "proposal";
const std::string Tag::PFAMCMC_NAME = "PFAMCMC";
const std::string Tag::MC3_NAME = "MC3";
const std::string Tag::GRADIENT_NAME = "GradientMCMC";
const std::string Tag::RJMCMC_NAME = "ReversibleJumpMCMC";
const std::string Tag::FREQUENCY_PROPOSAL_TAG = "frequency";
const std::string Tag::PERIOD_PROPOSAL_TAG = "period";

// Sampler
const std::string Tag::LIGHT_SAMPLER_NAME = "LightSampler";
const std::string Tag::SL_SAMPLER_NAME = "SplitLoggerSampler";
const std::string Tag::RJMCMC_SAMPLER_NAME = "RJMCMCSampler";
const std::string Tag::SL_RJMCMC_SAMPLER_NAME = "SplitLoggerRJMCMCSampler";


const std::string Tag::BLOCK_SELECTOR_TAG = "blockSelector";
const std::string Tag::MODIFIER_STRAT_TAG = "modifierStrategy";
const std::string Tag::ACCEPTANCE_STRAT_TAG = "acceptanceStrategy";

const std::string Tag::CYCLYC_BS = "cyclic";
const std::string Tag::REVERSIBLE_BS = "reversible";
const std::string Tag::RANDOM_BS = "random";
const std::string Tag::OPTIMISED_BS = "optimised";
const std::string Tag::RANDOM_TREEINFERENCE_BS = "randomTreeInference";
const std::string Tag::OPTIMISED_TREEINFERENCE_BS = "optimisedTreeInference";

const std::string Tag::BLOCK_STAT_CFG_TAG = "blockStatCfg";

const std::string Tag::OUTPUT_TAG = "output";
const std::string Tag::NTHINNING_TAG = "nThinning";
const std::string Tag::BURNIN_TAG = "burnin";
const std::string Tag::BURNIN_PERCENT_TAG = "burninPercent";
const std::string Tag::BURNIN_ITERATION_TAG = "burninIteration";
const std::string Tag::VERBOSE_TAG = "verbose";
const std::string Tag::WRITE_BINARY_TAG = "writeBinary";
const std::string Tag::WRITE_TYPE_TAG = "writeType";

const std::string Tag::RJ_PROPOSAL_TYPE_TAG = "proposalType";

const std::string Tag::CHECKPOINT_TAG = "checkpoint";
const std::string Tag::CKP_N_CKP_KEPT = "keepNCheckpoints";
const std::string Tag::CKP_TIME_FREQUENCY_TAG = "timeFrequency";
const std::string Tag::CKP_ITER_FREQUENCY_TAG = "iterationFrequency";
const std::string Tag::CKP_INTERVAL_DURATION = "timeInterval";
const std::string Tag::CKP_NB_DURING_INTERVAL = "nbCheckpointDuringInterval";

// Maximizer
const std::string Tag::MAXIMIZER_TAG = "maximizer";
const std::string Tag::NLOPT = "NLOPT";
const std::string Tag::LBFGS_B = "LBFGSB";

// Analysis
const std::string Tag::ANALYZE_COEV_LOG ="AnalyzeCoevLog";
const std::string Tag::ANALYZE_TREE_LOG ="AnalyzeTreeLog";
const std::string Tag::ANALYZE_ALL_LOGS ="AnalyzeAllLogs";

// Values
const std::string Tag::DEFAULT = "default";
const std::string Tag::OPTIMIZED = "optimised";
const std::string Tag::RANDOM = "random";
const std::string Tag::SINGLE = "single";
const std::string Tag::MULTIPLIER = "multiplier";
const std::string Tag::LOG = "log";
const std::string Tag::WIDE = "wide";
const std::string Tag::PRIOR = "prior";
const std::string Tag::GOOD = "good";
const std::string Tag::ACCURATE = "accurate";
const std::string Tag::FAST = "fast";
const std::string Tag::EXTRAFAST = "extraFast";
const std::string Tag::CUSTOM = "custom";

const std::string Tag::TRUE = "true";
const std::string Tag::FALSE = "false";

const std::string Tag::H0 = "H0";
const std::string Tag::H1 = "H1";

const std::string Tag::ESPILON_FREQ = "epsilonFreq";
const std::string Tag::POWER_FREQ = "powerFreq";

const std::string Tag::FLOAT = "float";
const std::string Tag::DOUBLE = "double";

// ML
const std::string Tag::MCMC = "MCMC";
const std::string Tag::ML = "ML";
const std::string Tag::BEB = "BEB";
const std::string Tag::HOLM_BONFERRONI_RUN ="Holm-Bonferroni REL";
const std::string Tag::LOG_ANALYSIS ="LogAnalysis";



} /* namespace XML */


