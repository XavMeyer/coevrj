//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SamplerXML.h
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef SAMPLER_H_
#define SAMPLER_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>

#include "../Tag.h"
#include "Model/Model.h"
#include "Model/Likelihood/Helper/HelperInterface.h"
#include "Model/Likelihood/Helper/Helpers.h"
#include "ParameterBlock/Block.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "ParameterBlock/BlockStats/Config/Container/BlockStatCfg.h"
#include "Sampler/BaseSampler.h"
#include "Sampler/IncSamplers.h"
#include "Sampler/Proposal/Proposals.h"
#include "Sampler/Strategy/AcceptanceStrategy/AcceptanceStrategy.h"
#include "Sampler/Strategy/BlockSelector/BlockSelector.h"
#include "Sampler/Strategy/ModifierStrategy/ModifierStrategy.h"
#include "Sampler/TraceWriter/BaseWriter.h"
#include "Sampler/TraceWriter/TextWriter.h"
#include "Utils/XML/HelpersXML.h"
#include "Utils/XML/TinyXML/tinyxml.h"

class TiXmlElement;
namespace ParameterBlock { class Blocks; }
namespace StatisticalModel { class Model; }

using namespace Sampler::Strategies;
using namespace ParameterBlock::Config;
using namespace StatisticalModel::Likelihood;
using namespace StatisticalModel::Likelihood::Helper;

namespace XML {

class ReaderSamplerXML {
public:
	typedef boost::shared_ptr<ReaderSamplerXML> sharedPtr_t;
	typedef boost::shared_ptr<ParameterBlock::Blocks> blocksPtr_t;

public:
	ReaderSamplerXML(const std::string &aName, TiXmlElement *aPRoot, StatisticalModel::Model* aPtrModel);
	~ReaderSamplerXML();

	Sampler::BaseSampler* getSamplerPtr();
	ParameterBlock::Blocks* getBlocksPtr();

private:
	static const size_t DEFAULT_SEED;

	bool hasProposalRJMCMC;
	const std::string name;
	TiXmlElement *pRoot;
	blocksPtr_t ptrBlocks;
	StatisticalModel::Model* ptrModel;

	Sampler::Proposal::Proposals proposals;

	Sampler::TraceWriter::BaseWriter::sharedPtr_t ptrWriter;

	Sampler::BaseSampler::sharedPtr_t ptrSampler;

	void readBlocks(Helper::HelperInterface::sharedPtr_t hlp);
	ParameterBlock::Block::adaptiveType_t tagToAdaptiveType(const std::string &text) const;

	void readProposals(Helper::HelperInterface::sharedPtr_t hlp);
	void readProposalPFAMCMC(TiXmlElement *pProposal);
	void readProposalMC3(TiXmlElement *pProposal);
	void readProposalRJMCMC(TiXmlElement *pProposal, Helper::HelperInterface::sharedPtr_t hlp);

	void readSampler();

	// void readStrategies(); // Deprecated
	BlockSelector::sharedPtr_t readBlockSelector(const std::string &blockSel, TiXmlElement *pProposal, Blocks &blocks);
	ModifierStrategy::sharedPtr_t readModifierStrategy(const std::string &name);
	AcceptanceStrategy::sharedPtr_t readAcceptanceStrategy(const std::string &name, ModifierStrategy::sharedPtr_t ptrMS);

	ConfigFactory::sharedPtr_t readBlockStatConfig(TiXmlElement *pProposal);

	void readOutput();
	void readCheckpointConfiguration();

};

} /* namespace XML */

#endif /* SAMPLER_H_ */

