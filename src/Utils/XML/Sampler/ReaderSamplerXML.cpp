//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SamplerXML.cpp
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#include "ReaderSamplerXML.h"

#include "Model/Model.h"
#include "Parallel/Manager/MpiManager.h"
#include "Sampler/Proposal/BaseProposal.h"
#include "Sampler/Proposal/ProposalMC3.h"
#include "Sampler/Proposal/ProposalPFAMCMC.h"
#include "Sampler/Proposal/ProposalRJMCMC.h"
#include "Sampler/Samples/Sample.h"

class TiXmlElement;
namespace ParameterBlock { class Blocks; }

namespace XML {

const size_t ReaderSamplerXML::DEFAULT_SEED = 1234;

ReaderSamplerXML::ReaderSamplerXML(const std::string &aName, TiXmlElement *aPRoot, StatisticalModel::Model* aPtrModel) :
		name(aName), pRoot(aPRoot), ptrModel(aPtrModel) {

	hasProposalRJMCMC = false;

	ptrBlocks.reset(new ParameterBlock::Blocks());

	readSampler();
}

ReaderSamplerXML::~ReaderSamplerXML() {
}

Sampler::BaseSampler* ReaderSamplerXML::getSamplerPtr() {
	return ptrSampler.get();
}

ParameterBlock::Blocks* ReaderSamplerXML::getBlocksPtr() {
	return ptrBlocks.get();
}

void ReaderSamplerXML::readSampler() {
	using namespace StatisticalModel::Likelihood;
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrModel->getLikelihood().get());

	// Read blocks
	readBlocks(hlp);

	// Create proposals
	readProposals(hlp);

	// configure the OutputManager and the TraceFileWriter
	readOutput();

	// Create inital sample
	Sampler::Sample sample = hlp->defineInitSample(ptrModel->getParams());

	// Create the sampler
	if(name == Tag::RJMCMC_SAMPLER_NAME) {
		ptrSampler.reset(
						new Sampler::RJSampler(sample, *ptrModel, proposals, ptrWriter));
	} else if(name == Tag::LIGHT_SAMPLER_NAME && hasProposalRJMCMC) {
		if(Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "[ReaderSamplerXML] You requested the use of the [LightSampler] however, are using a model with Reversible Jumps." << std::endl;
			std::cout << "[ReaderSamplerXML] By default the sampler has been switched to [RJSampler]." << std::endl;
		}
		ptrSampler.reset(
						new Sampler::RJSampler(sample, *ptrModel, proposals, ptrWriter));
	} else if(name == Tag::SL_RJMCMC_SAMPLER_NAME) {
		ptrSampler.reset(
						new Sampler::SplitLoggerRJSampler(sample, *ptrModel, proposals, ptrWriter));
	} else if(name == Tag::LIGHT_SAMPLER_NAME && !hasProposalRJMCMC) {
		ptrSampler.reset(
				new Sampler::LightSampler(sample, *ptrModel, proposals, ptrWriter));
	} else if(name == Tag::SL_SAMPLER_NAME) {
		ptrSampler.reset(
				new Sampler::SplitLoggerSampler(sample, *ptrModel, proposals, ptrWriter));
	} else {
		std::stringstream ss;
		ss << "Sampler '" << name << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}

	// Configure the checkpointing
	readCheckpointConfiguration();
}

BlockSelector::sharedPtr_t ReaderSamplerXML::readBlockSelector(const std::string &blockSel, TiXmlElement *pProposal, Blocks &blocks) {

	BlockSelector::sharedPtr_t ptrBS;

	size_t seed = Parallel::mpiMgr().getPRNG()->getSeed();
	double freqTreeMove = 0.0;
	if(!XML::readElement(__func__, Tag::FREQ_TREE_MOVE_TAG, pProposal, freqTreeMove, OPTIONAL)){ // If value not correctly read
		freqTreeMove = 0.32; // set to default
	}
	ptrBS = Sampler::Strategies::BlockSelector::createOptimisedTreeInference(ptrModel->getParams(), blocks, freqTreeMove, seed);

	return ptrBS;
}

ModifierStrategy::sharedPtr_t ReaderSamplerXML::readModifierStrategy(const std::string &modStrat) {

	ModifierStrategy::sharedPtr_t ptrMS;

	if(modStrat == Tag::DEFAULT) {
		ptrMS = Sampler::Strategies::ModifierStrategy::createDefaultModifierStrategy(*ptrModel);
	} else {
		std::stringstream ss;
		ss << "ModifierStrategy '" << modStrat << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}

	return ptrMS;
}

AcceptanceStrategy::sharedPtr_t ReaderSamplerXML::readAcceptanceStrategy(const std::string &accStrat, ModifierStrategy::sharedPtr_t ptrMS) {

	AcceptanceStrategy::sharedPtr_t ptrAS;

	if(accStrat == Tag::DEFAULT) {
		ptrAS = Sampler::Strategies::AcceptanceStrategy::createDefaultAcceptanceStrategy(ptrMS, *ptrModel);
	} else {
		std::stringstream ss;
		ss << "AcceptanceStrategy '" << accStrat << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}

	return ptrAS;
}

ConfigFactory::sharedPtr_t ReaderSamplerXML::readBlockStatConfig(TiXmlElement *pProposal){

	std::string blockStatStr;
	if(!XML::readElement(__func__, Tag::BLOCK_STAT_CFG_TAG, pProposal, blockStatStr, OPTIONAL)){
		return ConfigFactory::createDefaultFactory();
	}

	if(blockStatStr == Tag::DEFAULT) {
		return ConfigFactory::createDefaultFactory();
	} else if (blockStatStr == Tag::FAST) {
		return ConfigFactory::createFastFactory();
	} else if (blockStatStr == Tag::EXTRAFAST) {
		return ConfigFactory::createExtraFastFactory();
	} else if (blockStatStr == Tag::ACCURATE) {
		return Config::ConfigFactory::createSlowFactory();
	} else {
		std::stringstream ss;
		ss << "BlockStatConfig '" << blockStatStr << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}

	return ConfigFactory::createDefaultFactory();
}

void ReaderSamplerXML::readOutput() {

	// If there is no output tag we use MCMC class default
	TiXmlElement *pOutput = pRoot->FirstChildElement(Tag::OUTPUT_TAG);
	if(!pOutput){
		warningMissingTag(__func__, Tag::OUTPUT_TAG);
		return;
	}

	// Output File
	std::string outputFile("output");
	XML::readElement(__func__, Tag::FILE_TAG, pOutput, outputFile, OPTIONAL);

	// Thinning
	size_t nThin = 1;
	XML::readElement(__func__, Tag::NTHINNING_TAG, pOutput, nThin, OPTIONAL);

	// Write binary
	std::string emptyString("");
	ptrWriter.reset(new Sampler::TraceWriter::TextWriter(outputFile, emptyString, nThin));
	/*std::string isBinary;
	if(XML::readElement(__func__, Tag::WRITE_BINARY_TAG, pOutput, isBinary, OPTIONAL)){
		std::string emptyString("");
		if(isBinary == Tag::TRUE) {
			ptrWriter.reset(new Sampler::TraceWriter::BinaryWriter(outputFile, emptyString, nThin));
		} else if(isBinary == Tag::FALSE) {
			ptrWriter.reset(new Sampler::TraceWriter::TextWriter(outputFile, emptyString, nThin));
		} else {
			std::stringstream ss;
			ss << "writeBinary must be (true/false) : '" << isBinary << "' is not a correct value.";
			errorMessage(__func__,  ss.str());
		}
	}*/

	// Verbosity
	size_t verbosity = 1;
	if(XML::readElement(__func__, Tag::VERBOSE_TAG, pOutput, verbosity, OPTIONAL)){
		Sampler::Information::verboseLevel_t verbLvl = static_cast<Sampler::Information::verboseLevel_t>(verbosity);
		Sampler::Information::outputManager().setVerbosityThreshold(verbLvl);
	}

	// Output Frequency
	size_t outFrequency;
	if(XML::readElement(__func__, Tag::STDOUTPUT_FREQUENCY_TAG, pOutput, outFrequency, OPTIONAL)) {
		Sampler::Information::outputManager().setSamplerOutputFrequency(outFrequency);
	}
}

void ReaderSamplerXML::readCheckpointConfiguration() {

	// Check if the checkpoint tag is present
	TiXmlElement *pCKP = pRoot->FirstChildElement(Tag::CHECKPOINT_TAG);
	if(!pCKP){
		if(Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "[XML Config] Warning 'missing tag : " << Tag::CHECKPOINT_TAG << "'. Checkpoints disabled." << std::endl;
		}
		return;
	}

	// Checkpoint manager is active but not configure
	bool isConfigured = false;
	ptrSampler->getCheckpointManager().setActive();

	// How much checkpoints should we keep ?
	int keepNCP=0;
	if(XML::readElement(__func__, Tag::CKP_N_CKP_KEPT, pCKP, keepNCP, NO_OUTPUT)){
		ptrSampler->getCheckpointManager().setNCheckpointKept(keepNCP);
	}

	// Is there a time frequency configuration ?
	double timeFrequency=0.;
	if(XML::readElement(__func__, Tag::CKP_TIME_FREQUENCY_TAG, pCKP, timeFrequency, NO_OUTPUT)){
		ptrSampler->getCheckpointManager().setTimeFrequency(timeFrequency);
		isConfigured = true;
	}

	// Is there an iteration frequency configuration ?
	size_t iterFrequency=0;
	if(XML::readElement(__func__, Tag::CKP_ITER_FREQUENCY_TAG, pCKP, iterFrequency, NO_OUTPUT)){
		ptrSampler->getCheckpointManager().setIterationFrequency(iterFrequency);
		isConfigured = true;
	}

	// Is there an interval configuration ?
	double timeInterval=0;
	if(XML::readElement(__func__, Tag::CKP_INTERVAL_DURATION, pCKP, timeInterval, NO_OUTPUT)){
		size_t nCKPperInterval = 0;
		if(XML::readElement(__func__, Tag::CKP_NB_DURING_INTERVAL, pCKP, nCKPperInterval, NO_OUTPUT)){
			ptrSampler->getCheckpointManager().setTimeInterval(timeInterval, nCKPperInterval);
		} else {
			ptrSampler->getCheckpointManager().setTimeInterval(timeInterval);
		}
		isConfigured = true;
	}

	// Is it configure ? If not set default config.
	// Is there a time frequency configuration ?
	if(!isConfigured){
		ptrSampler->getCheckpointManager().setTimeFrequency(3500.);
		if(Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "[XML Config] Warning tag : '" << Tag::CHECKPOINT_TAG << "' found without specific configuration." << std::endl;
			std::cout << "[XML Config] Default configuration of one checkpoint per hour will be used." << std::endl;
			std::cout << "[XML Config] Remove the '" << Tag::CHECKPOINT_TAG << "' if you wish to disable checkpoints." << std::endl;
		}
	}

}

void ReaderSamplerXML::readBlocks(Helper::HelperInterface::sharedPtr_t hlp) {

	TiXmlElement *pBlock = pRoot->FirstChildElement(Tag::BLOCKS_TAG);
	const std::string *pAttrib = pBlock->Attribute(Tag::NAME_ATT);

	std::vector<size_t> blockSize;
	XML::readElements(__func__, Tag::BLOCK_SIZE_TAG, pBlock, blockSize, MANDATORY);

	std::string adaptiveTypeTag;
	XML::readElement(__func__, Tag::ADAPTIVE_TYPE_TAG, pBlock, adaptiveTypeTag, MANDATORY);
	ParameterBlock::Block::adaptiveType_t adaptiveType = tagToAdaptiveType(adaptiveTypeTag);

	hlp->defineBlocks(adaptiveType, blockSize, *ptrModel, *ptrBlocks.get());
}

ParameterBlock::Block::adaptiveType_t ReaderSamplerXML::tagToAdaptiveType(const std::string &text) const {
	using ParameterBlock::Block;
	if(text == Tag::NOT_ADAPTIVE_TAG) {
		return Block::NOT_ADAPTIVE;
	} else if(text == Tag::DEFAULT_ADAPTIVE_TAG) {
		return Block::SINGLE_DOUBLE_VARIABLE;
	} else if(text == Tag::MIXED_ADAPTIVE_TAG) {
		return Block::MIXED_DOUBLE_VARIABLES;
	} else if(text == Tag::PCA_ADAPTIVE_TAG) {
		return Block::PCA_DOUBLE_VARIABLES;
	} else if(text == Tag::INDEPENDENT_GAMMA_TAG) {
		return Block::INDEPENDENT_GAMMA;
	} else {
		std::stringstream ss;
		ss << "Adaptive type is unknown : " << text;
		XML::errorMessage(__func__, ss.str());
	}
	return Block::NOT_ADAPTIVE;
}

void ReaderSamplerXML::readProposals(Helper::HelperInterface::sharedPtr_t hlp) {

	// Read the first element and check that it exists
	TiXmlElement *pProposal = pRoot->FirstChildElement(Tag::PROPOSAL_TAG);
	if(!pProposal) {
		XML::errorMissingTag(__func__, Tag::PROPOSAL_TAG);
	}

	while(pProposal != NULL) { // While there are proposals
		// Read the name of the proposal
		const std::string *pAttrib = pProposal->Attribute(Tag::NAME_ATT);
		if(!pAttrib){
			XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
		}

		// Dispatch proposals
		if(*pAttrib == Tag::PFAMCMC_NAME) {
			readProposalPFAMCMC(pProposal);
		} else if(*pAttrib == Tag::MC3_NAME) {
			readProposalMC3(pProposal);
		} else if(*pAttrib == Tag::RJMCMC_NAME) {
			readProposalRJMCMC(pProposal, hlp);
		}


		// Read the next proposal block
		pProposal = pProposal->NextSiblingElement(Tag::PROPOSAL_TAG);
	}
}


void ReaderSamplerXML::readProposalPFAMCMC(TiXmlElement *pProposal) {

	Sampler::Proposal::ProposalPFAMCMC::sharedPtr_t ptrProp;

	// Read config factory
	ConfigFactory::sharedPtr_t ptrCfg = ConfigFactory::createFastFactory();//readBlockStatConfig(pProposal);

	double freq = 0.;
	if(XML::readElement(__func__, Tag::FREQUENCY_PROPOSAL_TAG, pProposal, freq, OPTIONAL)){
		ptrProp.reset(new Sampler::Proposal::ProposalPFAMCMC(freq, ptrCfg, *ptrBlocks.get(), *ptrModel));
	} else {
		ptrProp.reset(new Sampler::Proposal::ProposalPFAMCMC(ptrCfg, *ptrBlocks.get(), *ptrModel));
	}

	// Read BlockSelector
	std::string blockSel;
	Sampler::Strategies::BlockSelector::sharedPtr_t ptrBS;
	//if(XML::readElement(__func__, Tag::BLOCK_SELECTOR_TAG, pProposal, blockSel, OPTIONAL)){
	ptrBS = readBlockSelector(blockSel, pProposal, ptrProp->getBlocks());
	//}

	if(ptrBS != NULL) {
		ptrProp->setBlockSelector(ptrBS);
	}

	proposals.addProposal(ptrProp);
}

void ReaderSamplerXML::readProposalMC3(TiXmlElement *pProposal) {
	Sampler::Proposal::ProposalMC3::sharedPtr_t ptrProp;

	// Read config factory
	ConfigFactory::sharedPtr_t ptrCfg = ConfigFactory::createFastFactory();//readBlockStatConfig(pProposal);

	// Read seed
	size_t seed = Parallel::mpiMgr().getPRNG()->getSeed();
	//Sampler::Strategies::BlockSelector::sharedPtr_t ptrBS;
	//XML::readElement(__func__, Tag::SEED_TAG, pProposal, seed, OPTIONAL);

	size_t period = 100;
	if(XML::readElement(__func__, Tag::PERIOD_PROPOSAL_TAG, pProposal, period, OPTIONAL)){
		ptrProp.reset(new Sampler::Proposal::ProposalMC3(period, seed, *ptrModel, ptrCfg));
	} else {
		ptrProp.reset(new Sampler::Proposal::ProposalMC3(seed, *ptrModel, ptrCfg));
	}

	proposals.addProposal(ptrProp);
}

void ReaderSamplerXML::readProposalRJMCMC(TiXmlElement *pProposal, Helper::HelperInterface::sharedPtr_t hlp) {
	Sampler::Proposal::ProposalRJMCMC::sharedPtr_t ptrProp;

	hasProposalRJMCMC = true;

	// Read config factory
	ConfigFactory::sharedPtr_t ptrCfg = ConfigFactory::createFastFactory();//readBlockStatConfig(pProposal);

	// Read seed
	size_t seed = Parallel::mpiMgr().getPRNG()->getSeed();
	//XML::readElement(__func__, Tag::SEED_TAG, pProposal, seed, OPTIONAL);

	// Read burnin
	size_t burnIn = 0;
	XML::readElement(__func__, Tag::BURNIN_TAG, pProposal, burnIn, OPTIONAL);

	// Read iType
	size_t iType = 2;
	//XML::readElement(__func__, Tag::RJ_PROPOSAL_TYPE_TAG, pProposal, iType, OPTIONAL);

	double freq = 0.05;
	if(XML::readElement(__func__, Tag::FREQUENCY_PROPOSAL_TAG, pProposal, freq, OPTIONAL)){
		ptrProp.reset(new Sampler::Proposal::ProposalRJMCMC(freq, seed, burnIn, ptrCfg, *ptrModel, hlp->createRJMoveManager(seed, iType, *ptrModel)));
	} else {
		ptrProp.reset(new Sampler::Proposal::ProposalRJMCMC(seed, burnIn, ptrCfg, *ptrModel, hlp->createRJMoveManager(seed, iType, *ptrModel)));
	}

	ptrProp->setInitMoves(hlp->createRJInitMoves(*ptrModel));

	proposals.addProposal(ptrProp);
}


} /* namespace XML */

