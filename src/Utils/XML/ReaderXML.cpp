//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReaderXML.cpp
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#include "ReaderXML.h"

#include <assert.h>

#include "Sampler/Samples/Sample.h"
#include "Parallel/Manager/MpiManager.h"
#include "Model/Likelihood/Helper/HelperInterface.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Logger/LogAdapter.h"

#include "Model/Likelihood/CoevRJ/Writer/CoevTraceAnalyser.h"

#include <boost/math/distributions/chi_squared.hpp>

namespace Sampler { class BaseSampler; }
namespace StatisticalModel { class Parameters; }

namespace XML {

const size_t ReaderXML::DEFAULT_SEED = 1337;

ReaderXML::ReaderXML(const std::string &fileName) : doc(fileName) {

	runType = UNKNOWN_RUN;
	seed = DEFAULT_SEED;
	nGradient = nProposal = 1;//Parallel::mpiMgr().getNProc();
	nChain = 1;

	coutbuf = NULL;

	initXML();
	readXML();
}


ReaderXML::ReaderXML(const std::string &fileName, const std::string &xmlContent) : doc() {

	runType = UNKNOWN_RUN;
	seed = DEFAULT_SEED;
	nGradient = nProposal = 1;//Parallel::mpiMgr().getNProc();
	nChain = 1;

	coutbuf = NULL;

	initXML(xmlContent);
	readXML();
}



ReaderXML::~ReaderXML() {

	if(coutbuf!=NULL) {
		std::cout.rdbuf(coutbuf);
		redirectFile.close();
	}

}

void ReaderXML::run() {

	if(runType == MCMC_RUN) {
		ptrSamplerXML->getSamplerPtr()->generateNIterations(nIteration);
		reportStatusMCMC();
	} else if(runType == LOG_ANALYSIS_RUN) {
		runLogAnalysis();
	}
}

ReaderLikelihoodXML* ReaderXML::getPtrLikXML() {
	return ptrLikXML.get();
}

ReaderSamplerXML* ReaderXML::getPtrSamplerXML() {
	return ptrSamplerXML.get();
}

StatisticalModel::Model* ReaderXML::getPtrModel() {
	return ptrLikXML->getModelPtr();
}

Sampler::BaseSampler* ReaderXML::getPtrSampler() {
	return ptrSamplerXML->getSamplerPtr();
}

size_t ReaderXML::getNIteration() {
	return nIteration;
}

void ReaderXML::initXML() {

	if (!doc.LoadFile()) {
		XML::errorMessage(__func__, "Unable to open XML file.");
	}

	pRoot = doc.FirstChildElement(Tag::ROOT_TAG);
	if(!pRoot){
		XML::errorMissingTag(__func__, Tag::ROOT_TAG);
	}
}

void ReaderXML::initXML(const std::string &xmlContent) {

	if (!doc.Parse((const char*)xmlContent.c_str(), 0, TIXML_ENCODING_UTF8)) {
		XML::errorMessage(__func__, "Unable to open XML file.");
	}

	pRoot = doc.FirstChildElement(Tag::ROOT_TAG);
	if(!pRoot){
		XML::errorMissingTag(__func__, Tag::ROOT_TAG);
	}
}

void ReaderXML::readXML() {

	const std::string *pAttrib = pRoot->Attribute(Tag::METHOD_ATT);
	if(!pAttrib){
		XML::errorMissingAttribute(__func__, Tag::METHOD_ATT);
	} else {
		if(*pAttrib == Tag::MCMC) {
			runType = MCMC_RUN;
			readXML_MCMC();
		} else if(*pAttrib == Tag::LOG_ANALYSIS) {
			runType = LOG_ANALYSIS_RUN;
			readXML_Analysis();
			// Other operations are done during the run command
		} else {
			std::stringstream ss;
			ss << "Method '" << *pAttrib << "' does not exists";
			XML::errorMessage(__func__, ss.str());
		}
	}

}

void ReaderXML::readXML_MCMC() {

	readLogFile();

	readTopologyMCMC();
	//Parallel::mpiMgr().defineTopology(nProposal, nChain);
	Parallel::mcmcMgr().init(false, 1, 1, nChain);

	readSeed();
	std::srand(seed); // Every thread as the same std rng
	// During init everybody has the same PRNG
	Parallel::mpiMgr().setSeedPRNG(seed);

	readNSample();

	readLikelihood();
	readSampler();

	// From there each thread as a different PRNG
	const int myRank = Parallel::mpiMgr().getRank();
	Parallel::mpiMgr().setSeedPRNG(myRank*5+seed);
}

void ReaderXML::readXML_Analysis() {
	readTopologyMCMC();
	//Parallel::mpiMgr().defineTopology(nProposal, nChain);
	assert(nChain == 1 && "Analysis of the Coev log file must be conducted with only 1 processor.");
	Parallel::mcmcMgr().init(false, nProposal, 1, 1);

	//readLikelihood();

	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::ANALYSIS_TAG);
	if(!pChild){
		XML::errorMissingTag(__func__, Tag::ANALYSIS_TAG);
	} else {
		const std::string *pAttrib = pChild->Attribute(Tag::NAME_ATT);
		if(!pAttrib){
			XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
		} else {
			ptrLogAnalysisXML.reset(new ReaderAnalysisXML(*pAttrib, pChild));
		}
	}
}


void ReaderXML::readLogFile() {
	// Validate if checkpoint are used
	bool isUsingCheckpoint = pRoot->FirstChildElement(Tag::SAMPLER_TAG) != NULL &&
			 pRoot->FirstChildElement(Tag::SAMPLER_TAG)->FirstChildElement(Tag::CHECKPOINT_TAG)!= NULL;

	std::string logFile;

	if(XML::readElement(__func__, Tag::LOG_FILE_TAG, pRoot, logFile, OPTIONAL)) {
		if(Parallel::mpiMgr().isMainProcessor()) {
			logFile.append(".out");
			if(!isUsingCheckpoint) {
				redirectFile.open(logFile.c_str());
			} else {
				redirectFile.open(logFile.c_str(), std::ios::app);
			}
			coutbuf = std::cout.rdbuf();
			std::cout.rdbuf(redirectFile.rdbuf());
		}
	}
}

void ReaderXML::readSeed() {
	XML::readElement(__func__, Tag::SEED_TAG, pRoot, seed, OPTIONAL);
}

void ReaderXML::readTopologyMCMC() {
	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::TOPOLOGY_TAG);
	if(!pChild){
		XML::warningMissingTag(__func__, Tag::TOPOLOGY_TAG);
		return;
	}

	nProposal = 1; //XML::readElement(__func__, Tag::NPROPOSAL_TAG, pChild, nProposal, OPTIONAL);
	XML::readElement(__func__, Tag::NCHAIN_TAG, pChild, nChain, OPTIONAL);

	if(nProposal*nChain != (size_t)Parallel::mpiMgr().getNProc()) {
		std::stringstream ssErr;
		ssErr << "MPI number of processor ( " << Parallel::mpiMgr().getNProc();
		ssErr << " ) doesn't correspond to the requested number of proposal times the number of chain ( ";
		ssErr << nProposal << " x " << nChain << " = " << nProposal*nChain << " ).";
		XML::errorMessage(__func__, ssErr.str());
	}
}


void ReaderXML::readNSample() {
	XML::readElement(__func__, Tag::NITERATION_TAG, pRoot, nIteration, OPTIONAL);
}

void ReaderXML::readMaxIteration() {
	nIteration = 0;
	XML::readElement(__func__, Tag::NITERATION_TAG, pRoot, nIteration, OPTIONAL);
}

void ReaderXML::readLikelihood() {
	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG);
	if(!pChild){
		XML::errorMissingTag(__func__, Tag::LIKELIHOOD_TAG);
	} else {
		const std::string *pAttrib = pChild->Attribute(Tag::NAME_ATT);
		if(!pAttrib){
			XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
		} else {
			ptrLikXML.reset(new ReaderLikelihoodXML(*pAttrib, pChild));
		}
	}
}

void ReaderXML::readSampler() {
	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::SAMPLER_TAG);
	if(!pChild){
		XML::errorMissingTag(__func__, Tag::SAMPLER_TAG);
	} else {
		const std::string *pAttrib = pChild->Attribute(Tag::NAME_ATT);
		if(!pAttrib){
			XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
		} else {
			ptrSamplerXML.reset(new ReaderSamplerXML(*pAttrib, pChild, ptrLikXML->getModelPtr()));
		}
	}
}


void ReaderXML::reportStatusMCMC() {
	if(Parallel::mcmcMgr().isOutputManager() &&
			Sampler::Information::outputManager().check(Sampler::Information::FEW_VERB)) {
		std::cout << getPtrSampler()->getPerformanceReport() << std::endl;
		std::cout << getPtrSampler()->getBlockStatusReport() << std::endl;

		StatisticalModel::Likelihood::TIGamma::Base* ptrLikG =
						dynamic_cast<StatisticalModel::Likelihood::TIGamma::Base*>(getPtrModel()->getLikelihood().get());


		if(ptrLikG != NULL) {
			MolecularEvolution::TreeReconstruction::Logger::LogAdapter logAdapter(getPtrSampler()->getTraceWriter());
			logAdapter.adaptLog();
		}


		StatisticalModel::Likelihood::CoevRJ::Base* ptrLikCoev =
					dynamic_cast<StatisticalModel::Likelihood::CoevRJ::Base*>(getPtrModel()->getLikelihood().get());
		if(ptrLikCoev != NULL) {
			MolecularEvolution::TreeReconstruction::Logger::LogAdapter logAdapter(getPtrSampler()->getTraceWriter());
			logAdapter.adaptLog(true);

			if(!ptrLikCoev->isUsingFixedPairsStrategy()) {
				typedef StatisticalModel::Likelihood::CoevRJ::CoevTraceAnalyser coevTraceAnalyise_t;
				coevTraceAnalyise_t coevTraceAnalyser(getPtrSampler()->getTraceWriter()->getFNPrefix(), getPtrSampler()->getTraceWriter()->getFNSuffix());
				coevTraceAnalyser.summarizeRJLog();
			}
		}
	}

	if(Sampler::Information::outputManager().check(Sampler::Information::HIGH_VERB) &&
			Parallel::mcmcMgr().isManagerMC3() && Parallel::mcmcMgr().getMyChainMC3() > 0) {
		std::stringstream ssDbgMC3;
		ssDbgMC3 << Sampler::Information::outputManager().getPrefixFileName();
		ssDbgMC3 << "MC3_" << Parallel::mcmcMgr().getMyChainMC3() << ".out";
		std::ofstream dbgFile(ssDbgMC3.str().c_str(), iostream::app);
		dbgFile << getPtrSampler()->getPerformanceReport() << std::endl;
		for(uint i=0; i< getPtrSamplerXML()->getBlocksPtr()->getNBaseBlocks(); ++i){
			dbgFile << getPtrSamplerXML()->getBlocksPtr()->getBlock(i)->toString() << endl;
		}
		dbgFile.close();
	}


}

void ReaderXML::runLogAnalysis() {

	ReaderAnalysisXML::analysisType_t analysisType = ptrLogAnalysisXML->getAnalysisType();

	if(analysisType == ReaderAnalysisXML::ANALYZE_COEV_LOG_RUN || analysisType == ReaderAnalysisXML::ANALYZE_ALL_LOGS_RUN) {
		typedef StatisticalModel::Likelihood::CoevRJ::CoevTraceAnalyser coevTraceAnalyse_t;
		coevTraceAnalyse_t coevTraceAnalyser(ptrLogAnalysisXML->getPtrWriter()->getFNPrefix(), ptrLogAnalysisXML->getPtrWriter()->getFNSuffix());
		coevTraceAnalyser.setBurninIteration(ptrLogAnalysisXML->getBurninIteration());
		coevTraceAnalyser.setBurninRatio(ptrLogAnalysisXML->getBurninRatio());

		bool isAnalyzed = coevTraceAnalyser.summarizeRJLog(ptrLogAnalysisXML->isCoevTraceEnabled());
		if(!isAnalyzed) {
			std::cout << "[ReaderXML] Coev log file <" << coevTraceAnalyser.getCoevStringFN() << "> not found  and thus not analyzed." << std::endl;
		} else {
			std::cout << "[ReaderXML] Coev log file analysed and <" << coevTraceAnalyser.getCoevStatStringFN() << "> created." << std::endl;
		}
	}

	if(analysisType == ReaderAnalysisXML::ANALYZE_TREE_LOG_RUN || analysisType == ReaderAnalysisXML::ANALYZE_ALL_LOGS_RUN) {
		MolecularEvolution::TreeReconstruction::Logger::LogAdapter logAdapter(ptrLogAnalysisXML->getPtrWriter());
		logAdapter.setBurninIteration(ptrLogAnalysisXML->getBurninIteration());
		logAdapter.setBurninRatio(ptrLogAnalysisXML->getBurninRatio());
		bool isAdapted = logAdapter.adaptLog(ptrLogAnalysisXML->isTreeSplitFrequencyEnabled());
		if(!isAdapted) {
			std::cout << "[ReaderXML] Tree log file not found  and thus not analyzed." << std::endl;
		} else {
			std::cout << "[ReaderXML] Tree log file analysed <" << ptrLogAnalysisXML->getPtrWriter()->getFileBaseName() << ".trees> created." << std::endl;
		}
	}
}

} /* namespace XML */
