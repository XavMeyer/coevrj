//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelihoodXML.h
 *
 * @date May 11, 2015
 * @author meyerx
 * @brief
 */
#ifndef READER_ANALYSIS_XML_H
#define READER_ANALYSIS_XML_H

#include <boost/shared_ptr.hpp>

#include "../Tag.h"

class TiXmlElement;

namespace XML {

class ReaderAnalysisXML {
public:
	typedef boost::shared_ptr<ReaderAnalysisXML> sharedPtr_t;

	typedef enum {ANALYZE_COEV_LOG_RUN, ANALYZE_TREE_LOG_RUN, ANALYZE_ALL_LOGS_RUN} analysisType_t;

public:
	ReaderAnalysisXML(const std::string &aName, TiXmlElement *aPRoot);
	~ReaderAnalysisXML();

	analysisType_t getAnalysisType() const;
	Sampler::TraceWriter::BaseWriter::sharedPtr_t getPtrWriter() const;
	size_t getBurninIteration() const;
	double getBurninRatio() const;

	bool isCoevTraceEnabled() const;
	bool isTreeSplitFrequencyEnabled() const;

private:

	TiXmlElement *pRoot;
	analysisType_t analysisType;
	Sampler::TraceWriter::BaseWriter::sharedPtr_t ptrWriter;
	size_t burninIteration;
	double burninRatio;
	bool coevTraceEnabled, treeSplitFrequencyEnabled;

	void readAnalysis(const std::string &aName);

};

} /* namespace XML */

#endif /* READER_ANALYSIS_XML_H */
