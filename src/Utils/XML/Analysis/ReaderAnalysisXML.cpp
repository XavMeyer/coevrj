//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelihoodXML.cpp
 *
 * @date May 11, 2015
 * @author meyerx
 * @brief
 */

#include <boost/smart_ptr/shared_ptr.hpp>


#include "Sampler/TraceWriter/TextWriter.h"
#include "Utils/XML/Analysis/ReaderAnalysisXML.h"
#include "Utils/XML/HelpersXML.h"

#include <stddef.h>
#include <iostream>
#include <sstream>
#include <string>

class TiXmlElement;

namespace XML {

ReaderAnalysisXML::ReaderAnalysisXML(const std::string &aName, TiXmlElement *aPRoot) :
	pRoot(aPRoot) {

	burninIteration = 0;
	burninRatio = 0;

	readAnalysis(aName);
}

ReaderAnalysisXML::~ReaderAnalysisXML() {
}

ReaderAnalysisXML::analysisType_t ReaderAnalysisXML::getAnalysisType() const {
	return analysisType;
}

Sampler::TraceWriter::BaseWriter::sharedPtr_t ReaderAnalysisXML::getPtrWriter() const {
	return ptrWriter;
}

size_t ReaderAnalysisXML::getBurninIteration() const {
	return burninIteration;
}

double ReaderAnalysisXML::getBurninRatio() const {
	return burninRatio;
}

bool ReaderAnalysisXML::isCoevTraceEnabled() const {
	return coevTraceEnabled;
}

bool ReaderAnalysisXML::isTreeSplitFrequencyEnabled() const {
	return treeSplitFrequencyEnabled;
}

void ReaderAnalysisXML::readAnalysis(const std::string &aName) {

	if (aName == Tag::ANALYZE_ALL_LOGS) {
		analysisType = ANALYZE_ALL_LOGS_RUN;
	} else if (aName == Tag::ANALYZE_COEV_LOG) {
		analysisType = ANALYZE_COEV_LOG_RUN;
	} else if (aName == Tag::ANALYZE_TREE_LOG) {
		analysisType = ANALYZE_TREE_LOG_RUN;
	} else {
		std::stringstream ss;
		ss << "Analysis '" << aName << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}

	// Output File
	std::string outputFile("output");
	XML::readElement(__func__, Tag::FILE_TAG, pRoot, outputFile, OPTIONAL);

	// Burnin
	burninIteration = 1;
	XML::readElement(__func__, Tag::BURNIN_ITERATION_TAG, pRoot, burninIteration, OPTIONAL);

	burninRatio = 0.;
	XML::readElement(__func__, Tag::BURNIN_PERCENT_TAG, pRoot, burninRatio, OPTIONAL);
	burninRatio /= 100.;
	assert(burninRatio >= 0. && burninRatio <= 1. && "Burnin percent must be in the range [0..100]");

	if(burninIteration != 0 && burninRatio != 0.) {
		std::cout << "[XML Config] - WARNING - burning iteration and percent were both set. Using the biggest." << std::endl;
	}

	// Thinning
	size_t nThin = 1;
	if(burninIteration != 1) {
		XML::readElement(__func__, Tag::NTHINNING_TAG, pRoot, nThin, MANDATORY);
		burninIteration /= nThin;
	} else {
		XML::readElement(__func__, Tag::NTHINNING_TAG, pRoot, nThin, OPTIONAL);

	}

	std::string emptyString("");
	ptrWriter.reset(new Sampler::TraceWriter::TextWriter(outputFile, emptyString, nThin));

	// Options
	coevTraceEnabled = readBool(pRoot, Tag::WITH_COEV_TRACE_TAG, OPTIONAL, false); // Default is false
	treeSplitFrequencyEnabled = readBool(pRoot, Tag::WITH_TREE_SPLIT_FREQUENCY_TAG, OPTIONAL, false); // Default is false

}

} /* namespace XML */
