# XML Configuration files
[Back to main page](../README.md)

This page detail the options available when using a XML configuration file.

	> mpirun -np <P> build/CoevRJ -x <myConfig.xml>


An example of XML configuration file for CoevRJ is available in the `config_files` folder.

## Running CoevRJ analysis with an XML file

### Content of an XML configuration file


```xml
<config method="MCMC">
    <topology>
        <nChain>2</nChain>
    </topology>
    <seed>1332</seed>
    <logFile>stdOutRedirection.txt</logFile>
    <nIterations>500000</nIterations>
    <likelihood name="CoevReversibleJump">
        <nGamma>4</nGamma>
        <sampleTreeTopology>1</sampleTreeTopology> <!-- None=0, Naive=1, Adaptive=2, Guided=3, Mixed=4, Best=5 -->
        <alignFile>myAlignment.fasta</alignFile>
      	 <treeFile>myTree.nwk</treeFile> <!-- optional if topology is sampled -->
		 <useScoreCaching>true</useScoreCaching> <!-- If multiple analysis are done on the same large dataset, this will speedup the init at the extent of a large file. -->
		 <!--<coevRestartFile>results/run1.restart</coevRestartFile>-->
    </likelihood>
    <sampler name="RJMCMCSampler">
		<!-- Proposal on base parameters :-->
        <blocks name="default">
            <blockSize>8</blockSize> <!-- Number of parameters per blocks -->
            <adaptiveType>PCA_Adaptive</adaptiveType> <!-- Fast but bad mixing : NotAdaptive | Slower but better mixing : PCA_Adaptive see Meyer et al. 2017-->
        </blocks>
         <proposal name="PFAMCMC">
            <treeMoveProportion>0.10</treeMoveProportion> <!-- Proportion of tree moves are proposed compared to GTR/branch length propsoals -->
            <frequency>0.6</frequency> <!-- Frequency of this type of moves compared to RJMCMC moves -->
        </proposal>
		<!-- Proposals on Coev parameters and the models space -->
       <proposal name="ReversibleJumpMCMC">
			<burnin>25000</burnin> <!-- Number of Reversible jump to skip. Enable fast convergence of GTR parameters. -->
            <frequency>0.4</frequency> <!-- Frequency of this type of moves compared to PFAMCMC moves -->
        </proposal>
		<!-- Configuraiton of the MC3 proposal -->
		<!-- A MC3 move will be proposed each xth iterations using the period parameter for x -->
        <proposal name="MC3">
            <period>20</period>
        </proposal>
       <checkpoint>
                <keepNCheckpoints>5</keepNCheckpoints>
                <timeFrequency>10</timeFrequency>
                <!-- <iterationFrequency>20000</iterationFrequency>
                <timeInterval>60</timeInterval>
                <nbCheckpointDuringInterval>5</nbCheckpointDuringInterval>-->
        </checkpoint>
        <output>
            <filename>pathAndOutputFileName</filename>
            <verbose>1</verbose>
            <stdOutputFrequency>500</stdOutputFrequency>
            <nThinning>100</nThinning>
        </output>
    </sampler>
</config>
```

---
### Parameters
#### Global
* **nChain** defines the number of MC3 chains. This must be equal to P in the the `-np <P>` parameter provided to mpirun.
* **seed** defines the random seed employed for this run.
* (Optional) **logFile** - If specified, will redirect the *standard output* (text in the console) to the file provided.
* **nIterations** defines the number of iterations for this analysis.

---
#### Likelihood
* **nGamma** defines the number of discrete categories for the Gamma distribution modeling the rate heterogeneity of independent site.
* **sampleTreeTopology** when set to 0 the tree topologies are not sampled. With 1, naive proposals are used (stNNI, eSPR, eTBR). Configuration 2 to 5 correspond to the : Adaptive, Guided, Mixed and Best settings.
It is advised to use the option 5 for faster analyses (manuscript in preparation).
* **alignFile** defines the path to the alignment file
* (Optional) **treeFile** - If specified, will be used as initial tree topology instead of a random one.
* **useScoreCaching** enable (*true*) or disable (*false*) the caching (in a file) of the score used for the RJMCMC moves (see article). If multiple analysis are done on the same dataset, it will significantly speedup the initialization of CoevRJMCMC. Datasets having a large amount of nucleotides may results in large files (several MB).
* (Optional) **coevRestartFile** - If specified, this [restart file](restart.md) will be used to intialize the core parameters, the number of coevolving pairs and their sites.

---
#### Sampler
##### Blocks
Configuration of the adaptive move as defined in Meyer et al 2017.

* **blockSize** defines the number of parameters per blocks
* **adaptiveType** can be set either to *NotAdaptive* for a stardard normally distributed move or *PCA_Adaptive* to use the method detailed in Meyer et al. 2017.

##### Proposal::PFAMCMC
Configuration of the proposals on the base parameters (GTR model, branches length, tree topology).

* **treeMoveProportion** - proportions of the moves that will be dedicated to exploring the tree space
* **frequency** - overall frequency of proposals on the base parameters

##### Proposal::ReversibleJumpMCMC
Configuration of the proposals on th parameters dedicated to coevolution (Coev model, model space).

* **burnin** - this parameter can be used to disable the model space exploration for the first X RJMCMC proposals. **We encourage to use this option.** It enables base parameters to first reach a *pseudo*-convergence under a pure GTR+Gamma model before enable the model space exploration. This helps significantly the convergence of the MCMC algorith.given that it enables the base parameters to get closer to their
* **frequency** - overall frequency of proposals on the coev parameters / model space

##### Proposal::MC3
This must only be defined when `nChain > 1`.

* **period** defines at which period MC3 proposals will be applied. For instance, a period of 100 will results in a MC3 proposal each 100th iterations.

##### Checkpoint
Configure the checkpointing of an analysis. Using previous checkpoints is done by launching the **exact same** XML files than for the previous run.

* **keepNCheckpoints** defines how many checkpoints are kept on the disk.
* *Chose on of these*
	* **timeFrequency** - will save a checkpoint each X seconds.
	* **iterationFrequency** - will save a checkpoint each K iterations.
	* **timeInterval** and **nbCheckpointDuringInterval** - will save K checkpoint during the next X seconds.

The two first options are convenient when running small job on normal desktop, while the last one may become useful when CoevRJ is run on cluster having a *wall time*.

##### Output

* **filename** - path and basename of the output log file
* **verbose** - provides more or less information on the MCMC algorithms.
* **stdOutputFrequency** - defines at which frequency the status of the MCMC run is reported.
* **nThinning** - defines how much samples are to be kept (each Kth sample will be kept).

## Running CoevRJ post-analysis with an XML file

### Content of an XML configuration file

```xml
<config method="LogAnalysis">
    <analysis name="AnalyzeCoevLog"> <!-- AnalyzeTreeLog, AnalyzeCoevLog, AnalyzeAllLogs -->
       <filename>^Results/TEST_TREE_RANDOM_50_0_</filename>
		<burninIteration>0</burninIteration>
		<withCoevTrace>false</withCoevTrace> <!-- [COEV] If set to true, will write one trace file per coev pair of position sampled -->
		<withTreeSplitFreq>true</withTreeSplitFreq> <!-- [TREE] If set to true, will process the statistics of the split frequencies -->
    </analysis>
</config>
```

### Parameters

* **analysis name** can be either *AnalyzeTreeLog, AnalyzeCoevLog* or *AnalyzeAllLogs*. The first only post-process the tree samples, the second the coevolving pairs log and the third post-process both of them.
* **filename** - path and base filename used as output filename for the analysis.
* **burninIteration** - number of iterations or sample to discard at the beggining of the MCMC run.

#### Optional

* **withCoevTrace** - If set to true, it will write one log file per coevolving pair. This log file can then be visualised with Tracer.
* **withTreeSplitFreq** - If set to true, it will summarize the bipartitions frequency.
