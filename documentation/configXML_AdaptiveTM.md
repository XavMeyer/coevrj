# XML Configuration files

[Back to main page](../README.md)

This page detail the options available when using a XML configuration file for the GTR+Gamma models.

	> mpirun -np <P> build/CoevRJ -x <myConfig.xml>


An example of XML configuration file is available in the `config_files` folder.

## Running GTR+Gamma analyses with an XML file

### Content of an XML configuration file


```xml
<config method="MCMC">
    <topology>
        <nChain>1</nChain>
    </topology>
    <seed>1332</seed>
    <logFile>Results/MY_FILE_NAME</logFile>
    <nIterations>100000</nIterations>
    <likelihood name="TIGamma">
        <nGamma>4</nGamma> <!-- Number of discrete rate categories for the Gamma distribution -->
        <NucleotideModel_ID>1</NucleotideModel_ID> <!-- GTR=1, HKY85=2 -->
        <sampleTreeTopology>5</sampleTreeTopology> <!-- None=0, Naive=1, Adaptive=2, Guided=3, Mixed=4, Best=5 -->
        <alignFile>data/simulations/alignment_50Pairs.fasta</alignFile>
        <treeFile>data/simulations/tree_50Pairs.nwk</treeFile> <!-- optional only if topology is sampled -->
        <parameters>default</parameters>
    </likelihood>
    <sampler name="SplitLoggerSampler"> <!-- Default : LightSampler / Extensive tree and bipartition logging : SplitLoggerSampler -->
        <blocks name="default"> <!-- optimised will search for better blocks -->
            <blockSize>4</blockSize>
            <!-- Chose the type of adaptive block -->
            <adaptiveType>IndepGammaBL</adaptiveType> <!-- Fast but bad mixing : NotAdaptive | Slower but better mixing : PCA_Adaptive see Meyer et al. 2017 | Prototype indep Gamma for BL : IndepGammaBL-->
        </blocks>
        <proposal name="PFAMCMC">
            <treeMoveProportion>0.20</treeMoveProportion> <!-- Proportion of tree moves are proposed compared to GTR/branch length propsoals -->
            <frequency>1.0</frequency> <!-- Frequency of this type of moves compared to RJMCMC moves -->
        </proposal>
        <!-- Configuraiton of the MC3 proposal -->
        <!-- A MC3 move will be proposed each xth iterations using the period parameter for x -->
        <!--<proposal name="MC3">
            <period>20</period>
        </proposal>-->
        <checkpoint>
           <keepNCheckpoints>2</keepNCheckpoints>
           <iterationFrequency>20000</iterationFrequency>
           <!--<timeFrequency>10</timeFrequency>
           <iterationFrequency>20000</iterationFrequency>
           <timeInterval>60</timeInterval>
           <nbCheckpointDuringInterval>5</nbCheckpointDuringInterval>-->
         </checkpoint>
        <output>
            <filename>Results/MY_FILE_NAME</filename>
            <verbose>1</verbose>
            <stdOutputFrequency>5000</stdOutputFrequency>
            <nThinning>100</nThinning>
        </output>
    </sampler>
</config>
```

---
### Parameters
#### Global
* **nChain** defines the number of MC3 chains. This must be equal to P in the the `-np <P>` parameter provided to mpirun.
* **seed** defines the random seed employed for this run.
* (Optional) **logFile** - If specified, will redirect the *standard output* (text in the console) to the file provided.
* **nIterations** defines the number of iterations for this analysis.

---
#### Likelihood (TIGamma)
* **nGamma** defines the number of discrete categories for the Gamma distribution modeling the rate heterogeneity among sites.
* **sampleTreeTopology** when set to 0 the tree topologies are not sampled. With 1, naive proposals are used (stNNI, eSPR, eTBR). Configuration 2 to 5 correspond to the : Adaptive, Guided, Mixed and Best settings (manuscript in preparation).
* **alignFile** defines the path to the alignment file
* (Optional) **treeFile** - If specified, will be used as initial tree topology instead of a random one.

---
#### Sampler
* **name** can be *LightSampler* for analyses with the GTR+Gamma model without monitoring of the tree proposals or *SplitLoggerSampler* with monitoring of the tree proposals.

##### Blocks
Configuration of the adaptive move as defined in Meyer et al 2017.

* **blockSize** defines the number of parameters per blocks
* **adaptiveType** can be set either to *NotAdaptive* for a stardard normally distributed move or *PCA_Adaptive* to use the method detailed in Meyer et al. 2017.

##### Proposal::PFAMCMC
Configuration of the proposals on the base parameters (GTR model, branches length, tree topology).

* **treeMoveProportion** - proportions of the moves that will be dedicated to exploring the tree space
* **frequency** - overall frequency of proposals on the base parameters

##### Proposal::MC3
This must only be defined when `nChain > 1`.

* **period** defines at which period MC3 proposals will be applied. For instance, a period of 100 will results in a MC3 proposal each 100th iterations.

##### Checkpoint
Configure the checkpointing of an analysis. Using previous checkpoints is done by launching the **exact same** XML files than for the previous run.

* **keepNCheckpoints** defines how many checkpoints are kept on the disk.
* *Chose on of these*
	* **timeFrequency** - will save a checkpoint each X seconds.
	* **iterationFrequency** - will save a checkpoint each K iterations.
	* **timeInterval** and **nbCheckpointDuringInterval** - will save K checkpoint during the next X seconds.

The two first options are convenient when running small job on normal desktop, while the last one may become useful when CoevRJ is run on cluster having a *wall time*.

##### Output

* **filename** - path and basename of the output log file
* **verbose** - provides more or less information on the MCMC algorithms.
* **stdOutputFrequency** - defines at which frequency the status of the MCMC run is reported.
* **nThinning** - defines how much samples are to be kept (each Kth sample will be kept).

## Running CoevRJ post-analysis with an XML file

### Content of an XML configuration file

```xml
<config method="LogAnalysis">
    <analysis name="AnalyzeTreeLog">
       <filename>Results/MY_FILE_NAME_0_</filename>
		<burninIteration>0</burninIteration>
		<withTreeSplitFreq>true</withTreeSplitFreq> <!-- [TREE] If set to true, will process the statistics of the split frequencies -->
    </analysis>
</config>
```

### Parameters

* **analysis name**: *AnalyzeTreeLog*. Post-process the tree samples.
* **filename** - path and base filename used as output filename for the analysis.
* **burninIteration** - number of iterations or sample to discard at the beggining of the MCMC run.

#### Optional

* **withCoevTrace** - If set to true, it will write one log file per coevolving pair. This log file can then be visualised with Tracer.
* **withTreeSplitFreq** - If set to true, it will summarize the bipartitions frequency.
